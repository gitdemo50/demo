
#include "logger/impl/gwmlogger.hpp"


namespace gwm {
namespace platform {
namespace impl {

const char* LogLevelString(GWMLogLevel level) {
    switch (level)
    {
    case LogOff:
        return "Off    ";
        break;
    case LogFatal:
        return "Fatal  ";
        break;
    case LogError:
        return "Error  ";
        break;
    case LogWarn:
        return "Warn   ";
        break;
    case LogInfo:
        return "Info   ";
        break;
    case LogDebug:
        return "Debug  ";
        break;
    case LogVerbose:
        return "Verbose";
        break;

    default:
        return "----";
        break;
    }
}

GWMLogger::GWMLogger(std::string name, GWMLogLevel logLevel) : name(name), logLevel(logLevel) {
    targets.push_back(new ConsoleTarget());
    nameLen = name.size();
    if(nameLen > 25) {
        (void) this->name.erase(25,name.size());
        filler = "";
    } else {
        filler = std::string(25-nameLen, ' ');
    }
}

GWMLogger::~GWMLogger() {
    for(auto target : targets) {
        delete target;
    }
}

bool GWMLogger::isLogLevelEnabled(GWMLogLevel level)
{
    return (level <= logLevel && logLevel != LogOff);
}

void GWMLogger::log(std::stringstream& stream, GWMLogLevel level) {
    std::string prefix = "[" + name + filler + "] ["+ LogLevelString(level)+"] ";

    for(auto target : targets) {
        target->log(prefix,stream);
    }
}

}
}
}