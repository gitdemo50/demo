#ifndef GWM_UTILS_SHIMS_HPP
#define GWM_UTILS_SHIMS_HPP

#include <vector>
#include <cctype>
#include <future>
#include <unordered_map>
#include <memory>
#include <functional>

#include "ara/core/future.h"
#include "ara/com/types.h"

#include "logger/logger.hpp"

#include <gtest/gtest.h>
#include <gmock/gmock.h>

using ::testing::_;
using ::testing::Return;

struct TestBridge {
    static TestBridge* getInstance() {
        static TestBridge* tb = new TestBridge();
        return tb;
    }

private:
    TestBridge() {}
};

namespace gwm {
namespace services {
    class ProxyBase {
        using Handle = ara::com::HandleType;
        using Handler = ara::com::FindServiceHandler<Handle>;

    public:
        class HandleType {

        };

        virtual ~ProxyBase() {}

        using HandleContainer = ara::com::ServiceHandleContainer<HandleType>;

        static ara::com::FindServiceHandle StartFindService(ara::com::FindServiceHandler<HandleType> ha, ara::core::InstanceSpecifier inst) {
            static int FindServiceHandleCount=0;
            HandleContainer c;
            c.push_back(HandleType());
            FindServiceHandleCount++;
            if(ha)
                ha(c);
            return {inst.ToString()};
        }

        static void StopFindService(ara::com::FindServiceHandle serviceHandle) {
            if(stopHandler)
            {
                std::async(std::launch::async,stopHandler,serviceHandle.instanceSpec);
            }
        }

        static void setStopHandler(std::function<void(std::string)> sh) {
            stopHandler = sh;
        }

    private:
        static std::function<void(std::string)> stopHandler;
    };

    class MethodBase {
    public:
        struct Output {

        };
    };

    template<typename T>
    class EventBase {
    private:
        ara::com::EventReceiveHandler handlerFn;
    public:
            using SampleType = T;

            void Subscribe(ara::com::EventCacheUpdatePolicy p, int m) {}
            void SetReceiveHandler(ara::com::EventReceiveHandler h) {
                handlerFn = h;
            }
            bool Update() { return true; }
            const ara::com::SampleContainer<ara::com::SamplePtrInternal<const SampleType>> &
                        GetCachedSamples() const {return samples;}

            ara::com::SampleContainer<ara::com::SamplePtrInternal<const SampleType>> samples;

            void raiseEvent(T val) {
                samples.clear();
                samples.push_back(std::make_shared<T>(val));
                if(handlerFn)
                    handlerFn();
            }
    };

    template<typename T>
    class FieldBase {
    public:
        using FieldType = T;

        using GetHandlerInterface = std::function<::ara::core::Future<FieldType>()>;

        void Update(const FieldType& data) {
            storedData = data;
        }

        void RegisterGetHandler(GetHandlerInterface get_handler) {
            getHandler = get_handler;
        }

        ara::core::Future<FieldType> Get() {
            if(getHandler) {
                return getHandler();
            }
            ara::core::Promise<FieldType> retPromise;

            retPromise.set_value(storedData);

            return retPromise.get_future();
        }
        
    private:
        FieldType storedData;
        GetHandlerInterface getHandler;
    };


    class SkeletonBase {
    public:
        class ConstructionToken {
        public:
            ConstructionToken() {}
            ConstructionToken(ConstructionToken&& o) {}
        };
        using ConstructionResult = ara::core::Result<ConstructionToken>;

        static ConstructionResult Preconstruct(ara::core::InstanceSpecifier inst) {
            return ConstructionResult(ConstructionToken());
        };

        virtual ~SkeletonBase() {}

        void OfferService() {}
    };

}
}

#endif
