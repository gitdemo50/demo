/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  TimerSrvExe/include/amsr/someip_protocol/internal/datatype_deserialization/datatypes/gs2s/timersrv_gs2s/deserializer_TimerSt_Struct.h
 *        \brief  SOME/IP protocol deserializer implementation for datatype 'TimerSt_Struct'
 *
 *      \details  /S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/TimerSt_Struct
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_someipprotocol
 *         Commit ID: 0e137b9e6356987a6d2839c8d79c286219b68d60
 *********************************************************************************************************************/

#ifndef TIMERSRVEXE_INCLUDE_AMSR_SOMEIP_PROTOCOL_INTERNAL_DATATYPE_DESERIALIZATION_DATATYPES_GS2S_TIMERSRV_GS2S_DESERIALIZER_TIMERST_STRUCT_H_
#define TIMERSRVEXE_INCLUDE_AMSR_SOMEIP_PROTOCOL_INTERNAL_DATATYPE_DESERIALIZATION_DATATYPES_GS2S_TIMERSRV_GS2S_DESERIALIZER_TIMERST_STRUCT_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "datatypes/gs2s/timersrv_gs2s/impl_type_timerst_struct.h"
#include "someip-protocol/internal/deserialization/deser_wrapper.h"

namespace datatypes {
namespace gs2s {
namespace timersrv_gs2s {

// VECTOR NC Metric-HIS.PATH: MD_SOMEIPPROTOCOL_Metric-HIS.PATH
/*!
 * \brief Deserializer for datatype /S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/TimerSt_Struct.
 * \details Data type class: DataTypeStruct (CppImplDataTypeStruct).
 *          is TLV struct: false
 *
 * \tparam TpPack Transformation properties to be used for deserialization.
 *
 * \param[in,out] reader Reference to byte stream reader.
 * \param[out]    data Reference to the data object of type /S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/TimerSt_Struct
 *                in which the deserialized value will be written.
 * \return        True if the deserialization is successful, false otherwise.
 * \pre           The static size provided by SomeIpProtocolGetStaticSize has been verified.
 * \context       Reactor|App
 * \threadsafe    FALSE
 * \reentrant     TRUE for different reader objects.
 * \synchronous   TRUE
 */
template <typename TpPack>
amsr::someip_protocol::internal::deserialization::Result SomeIpProtocolDeserialize(amsr::someip_protocol::internal::deserialization::Reader& reader, ::datatypes::gs2s::timersrv_gs2s::TimerSt_Struct &data) noexcept {
  // Namespace alias for static deserialization code
  namespace deserialization = amsr::someip_protocol::internal::deserialization;
  // Deserialize non-TLV struct
  deserialization::Result result{false};
  // Deserialize struct member 'WakeSrcexpired' of type /S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/TimerSt_WakeSrcexpired_Enum
  result = deserialization::SomeIpProtocolDeserialize<
          TpPack,
          // Byte-order of primitive datatype (/S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/TimerSt_WakeSrcexpired_Enum)
        typename deserialization::Tp<TpPack>::ByteOrder

          >(reader, data.WakeSrcexpired);


  // Deserialize struct member 'WakeSource' of type /S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/TimerSt_WakeSource_Enum
  // VECTOR NL AutosarC++17_10-M0.1.2: MD_SOMEIPPROTOCOL_AutosarC++17_10-M0.1.2_dead_branch
  if (result) {
    result = deserialization::SomeIpProtocolDeserialize<
        TpPack,
        // Byte-order of primitive datatype (/S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/TimerSt_WakeSource_Enum)
        typename deserialization::Tp<TpPack>::ByteOrder

        >(reader, data.WakeSource);
  }

  return result;
}

/*!
 * \brief Calculates the static size for datatype /S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/TimerSt_Struct.
 * \details Data type class: DataTypeStruct (CppImplDataTypeStruct).
 *          is TLV struct: false
 *
 * \tparam TpPack Transformation properties to be used for deserialization.
 * \return Returns the static size of the struct in bytes.
 * \pre -
 * \context Reactor|App
 * \threadsafe FALSE
 * \reentrant TRUE
 * \vprivate Vector component internal API.
 * \synchronous TRUE
 */
template <typename TpPack>
constexpr std::size_t SomeIpProtocolGetStaticSize(amsr::someip_protocol::internal::deserialization::SizeToken<::datatypes::gs2s::timersrv_gs2s::TimerSt_Struct>) noexcept {
  // Namespace alias for static deserialization code
  namespace deserialization = amsr::someip_protocol::internal::deserialization;

  // Sum of static size
 constexpr std::size_t static_size{
  // Accumulate the static size of struct member 'WakeSrcexpired' of type /S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/TimerSt_WakeSrcexpired_Enum
   SomeIpProtocolGetStaticSize<TpPack,
                                // Byte-order of primitive datatype (/S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/TimerSt_WakeSrcexpired_Enum)
                                typename deserialization::Tp<TpPack>::ByteOrder

                               >(deserialization::SizeToken<::datatypes::gs2s::timersrv_gs2s::TimerSt_WakeSrcexpired_Enum>{})  + 
  
  // Accumulate the static size of struct member 'WakeSource' of type /S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/TimerSt_WakeSource_Enum
   SomeIpProtocolGetStaticSize<TpPack,
                                // Byte-order of primitive datatype (/S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/TimerSt_WakeSource_Enum)
                                typename deserialization::Tp<TpPack>::ByteOrder

                               >(deserialization::SizeToken<::datatypes::gs2s::timersrv_gs2s::TimerSt_WakeSource_Enum>{}) 
  };
  return static_size;
}

}  // namespace timersrv_gs2s
}  // namespace gs2s
}  // namespace datatypes

#endif  // TIMERSRVEXE_INCLUDE_AMSR_SOMEIP_PROTOCOL_INTERNAL_DATATYPE_DESERIALIZATION_DATATYPES_GS2S_TIMERSRV_GS2S_DESERIALIZER_TIMERST_STRUCT_H_

