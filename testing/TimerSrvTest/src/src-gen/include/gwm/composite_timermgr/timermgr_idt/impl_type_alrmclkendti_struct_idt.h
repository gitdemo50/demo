/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  TiMgrSrvExe/include/gwm/composite_timermgr/timermgr_idt/impl_type_alrmclkendti_struct_idt.h
 *        \brief
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_modelleddatatypes_api
 *         Commit ID: d9332226824a4a6c4616e2d948d41c862d192343
 *********************************************************************************************************************/

#ifndef TIMGRSRVEXE_INCLUDE_GWM_COMPOSITE_TIMERMGR_TIMERMGR_IDT_IMPL_TYPE_ALRMCLKENDTI_STRUCT_IDT_H_
#define TIMGRSRVEXE_INCLUDE_GWM_COMPOSITE_TIMERMGR_TIMERMGR_IDT_IMPL_TYPE_ALRMCLKENDTI_STRUCT_IDT_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include <array>
#include <cstdint>
#include <map>
#include <string>
#include <vector>
#include "gwm/composite_timermgr/timermgr_idt/impl_type_alrmclk_hours_integer_idt.h"
#include "gwm/composite_timermgr/timermgr_idt/impl_type_alrmclk_minutes_integer_idt.h"
#include "gwm/composite_timermgr/timermgr_idt/impl_type_alrmclk_seconds_integer_idt.h"

namespace gwm {
namespace composite_timermgr {
namespace timermgr_idt {

// VECTOR Disable AutosarC++17_10-A12.6.1: MD_MDTG_A12.6.1_GeneratedStructUninitializedMembers
// VECTOR Disable AutosarC++17_10-M8.5.1: MD_MDTG_M8.5.1_GeneratedStructUninitializedMembers
/*!
 * \brief Type AlrmClkEndTi_Struct_Idt.
 * \remark generated
 * \trace SPEC-5951372
 *
 * IMPLEMENTATION-DATA-TYPE /DataTypes/ImplementationDataTypes/AlrmClkEndTi_Struct_Idt
 */
struct AlrmClkEndTi_Struct_Idt {
  using AlrmClk_Hours_Idt_generated_type = gwm::composite_timermgr::timermgr_idt::AlrmClk_Hours_Integer_Idt;
  using AlrmClk_Minutes_Idt_generated_type = gwm::composite_timermgr::timermgr_idt::AlrmClk_Minutes_Integer_Idt;
  using AlrmClk_Seconds_Idt_generated_type = gwm::composite_timermgr::timermgr_idt::AlrmClk_Seconds_Integer_Idt;

  AlrmClk_Hours_Idt_generated_type AlrmClk_Hours_Idt;
  AlrmClk_Minutes_Idt_generated_type AlrmClk_Minutes_Idt;
  AlrmClk_Seconds_Idt_generated_type AlrmClk_Seconds_Idt;
};
// VECTOR Enable AutosarC++17_10-A12.6.1
// VECTOR Enable AutosarC++17_10-M8.5.1

/*!
 * \brief Compare for equality with another AlrmClkEndTi_Struct_Idt instance.
 */
inline bool operator==(AlrmClkEndTi_Struct_Idt const& l,
                       AlrmClkEndTi_Struct_Idt const& r) noexcept {
  return (&l == &r) || ((l.AlrmClk_Hours_Idt == r.AlrmClk_Hours_Idt)
                         && (l.AlrmClk_Minutes_Idt == r.AlrmClk_Minutes_Idt)
                         && (l.AlrmClk_Seconds_Idt == r.AlrmClk_Seconds_Idt)
  );
}

/*!
 * \brief Compare for inequality with another AlrmClkEndTi_Struct_Idt instance.
 */
inline bool operator!=(AlrmClkEndTi_Struct_Idt const& l,
                       AlrmClkEndTi_Struct_Idt const& r) noexcept {
  return !(l == r);
}

}  // namespace timermgr_idt
}  // namespace composite_timermgr
}  // namespace gwm

#endif  // TIMGRSRVEXE_INCLUDE_GWM_COMPOSITE_TIMERMGR_TIMERMGR_IDT_IMPL_TYPE_ALRMCLKENDTI_STRUCT_IDT_H_
