
/*
*   COPY RIGHT NOTICE
*   
*
*
*/

#include<memory>

#include "TimerBasicManager.hpp"


namespace gwm {
namespace manager {

TimerBasicManager::TimerBasicManager() : 
        _srv_TimerS2SService(std::make_shared<TimerS2SService>()),
        _controller(std::make_shared<TimerBasicController>(_srv_TimerS2SService)),
        _srv_TimerSrvService(std::make_shared<TimerSrvService>(_controller)),
        logger(gwm::platform::Logger("TimerBasicManager")) {
    logger.LogInfo() << "Timer Basic manager ready";
}

void TimerBasicManager::start() {
    logger.LogInfo() << "Starting Timer Basic Manager";

    startControllers();
}

void TimerBasicManager::startControllers() {
    _srv_TimerS2SService->start();
    _srv_TimerSrvService->start();
}

}}