
/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/include/gwm/pt_powersys/obcstsrv_si/SI_OBCStSrv_types.h
 *        \brief  Input and output structures for methods, fields and application errors of service 'SI_OBCStSrv'
 *
 *      \details  Definition of common input-/output structs used for simplified argument / marshalling handling. For all elements like methods, events fields structs with the related in-/output arguments are generated.
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_socal
 *         Commit ID: 209b3c0234013e84e3170ee826c9a8c6022de194
 *********************************************************************************************************************/

#ifndef DENPENDENCYSRVEXE_INCLUDE_GWM_PT_POWERSYS_OBCSTSRV_SI_SI_OBCSTSRV_TYPES_H_
#define DENPENDENCYSRVEXE_INCLUDE_GWM_PT_POWERSYS_OBCSTSRV_SI_SI_OBCSTSRV_TYPES_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "amsr/socal/internal/handle_type.h"
#include "gwm/pt_powersys/obcstsrv_si/si_obcstsrv_common.h"

namespace gwm {
namespace pt_powersys {
namespace obcstsrv_si {

namespace internal {

/*!
 * \brief Proxy HandleType for the Service 'SI_OBCStSrv'.
 * \trace SPEC-4980259
 */
class SI_OBCStSrvHandleType final : public ::amsr::socal::internal::HandleType {
  public:
  /*!
   * \brief Inherit constructor.
   */
  using HandleType::HandleType;
};

namespace methods {

}  // namespace methods

namespace fields {

/*!
 * \brief Data class for service field 'OBCChrgGunSt'.
 * \remark generated
 */
class OBCChrgGunSt final {
 public:
  /*!
   * \brief Return/output parameters of service field 'OBCChrgGunSt'
   */
  using Output = ::gwm::pt_powersys::powersys_idt::OBCChrgGunSt_Struct_Idt;
};


  /*!
 * \brief A class for field method 'OBCChrgGunSt'Get used for type definitions.
 */
class OBCChrgGunStGet final {
 public:
  /*!
   * \brief Struct representing the field input data.
   */
  struct Input {};

  /*!
   * \brief Struct representing the field output value.
   */
  struct Output {
    /*!
     * \brief Reference of field value (/DataTypes/ImplementationDataTypes/OBCChrgGunSt_Struct_Idt)
     */
    ::gwm::pt_powersys::powersys_idt::OBCChrgGunSt_Struct_Idt out_val;
  };
};


/*!
 * \brief Data class for service field 'OBCErrSt'.
 * \remark generated
 */
class OBCErrSt final {
 public:
  /*!
   * \brief Return/output parameters of service field 'OBCErrSt'
   */
  using Output = ::gwm::pt_powersys::powersys_idt::OBCErrSt_Struct_Idt;
};


  /*!
 * \brief A class for field method 'OBCErrSt'Get used for type definitions.
 */
class OBCErrStGet final {
 public:
  /*!
   * \brief Struct representing the field input data.
   */
  struct Input {};

  /*!
   * \brief Struct representing the field output value.
   */
  struct Output {
    /*!
     * \brief Reference of field value (/DataTypes/ImplementationDataTypes/OBCErrSt_Struct_Idt)
     */
    ::gwm::pt_powersys::powersys_idt::OBCErrSt_Struct_Idt out_val;
  };
};


/*!
 * \brief Data class for service field 'OBCHVILSt'.
 * \remark generated
 */
class OBCHVILSt final {
 public:
  /*!
   * \brief Return/output parameters of service field 'OBCHVILSt'
   */
  using Output = ::gwm::pt_powersys::powersys_idt::OBCHVILSt_Enum_Idt;
};


  /*!
 * \brief A class for field method 'OBCHVILSt'Get used for type definitions.
 */
class OBCHVILStGet final {
 public:
  /*!
   * \brief Struct representing the field input data.
   */
  struct Input {};

  /*!
   * \brief Struct representing the field output value.
   */
  struct Output {
    /*!
     * \brief Reference of field value (/DataTypes/ImplementationDataTypes/OBCHVILSt_Enum_Idt)
     */
    ::gwm::pt_powersys::powersys_idt::OBCHVILSt_Enum_Idt out_val;
  };
};


/*!
 * \brief Data class for service field 'OBCSt'.
 * \remark generated
 */
class OBCSt final {
 public:
  /*!
   * \brief Return/output parameters of service field 'OBCSt'
   */
  using Output = ::gwm::pt_powersys::powersys_idt::OBCSt_Enum_Idt;
};


  /*!
 * \brief A class for field method 'OBCSt'Get used for type definitions.
 */
class OBCStGet final {
 public:
  /*!
   * \brief Struct representing the field input data.
   */
  struct Input {};

  /*!
   * \brief Struct representing the field output value.
   */
  struct Output {
    /*!
     * \brief Reference of field value (/DataTypes/ImplementationDataTypes/OBCSt_Enum_Idt)
     */
    ::gwm::pt_powersys::powersys_idt::OBCSt_Enum_Idt out_val;
  };
};


/*!
 * \brief Data class for service field 'OBCV2XSt'.
 * \remark generated
 */
class OBCV2XSt final {
 public:
  /*!
   * \brief Return/output parameters of service field 'OBCV2XSt'
   */
  using Output = ::gwm::pt_powersys::powersys_idt::OBCV2XSt_Enum_Idt;
};


  /*!
 * \brief A class for field method 'OBCV2XSt'Get used for type definitions.
 */
class OBCV2XStGet final {
 public:
  /*!
   * \brief Struct representing the field input data.
   */
  struct Input {};

  /*!
   * \brief Struct representing the field output value.
   */
  struct Output {
    /*!
     * \brief Reference of field value (/DataTypes/ImplementationDataTypes/OBCV2XSt_Enum_Idt)
     */
    ::gwm::pt_powersys::powersys_idt::OBCV2XSt_Enum_Idt out_val;
  };
};


}  // namespace fields
}  // namespace internal

}  //  namespace obcstsrv_si
}  //  namespace pt_powersys
}  //  namespace gwm

#endif  // DENPENDENCYSRVEXE_INCLUDE_GWM_PT_POWERSYS_OBCSTSRV_SI_SI_OBCSTSRV_TYPES_H_

