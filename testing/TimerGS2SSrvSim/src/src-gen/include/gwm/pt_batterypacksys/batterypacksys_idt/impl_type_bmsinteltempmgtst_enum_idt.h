/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/include/gwm/pt_batterypacksys/batterypacksys_idt/impl_type_bmsinteltempmgtst_enum_idt.h
 *        \brief
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_modelleddatatypes_api
 *         Commit ID: d9332226824a4a6c4616e2d948d41c862d192343
 *********************************************************************************************************************/

#ifndef DENPENDENCYSRVEXE_INCLUDE_GWM_PT_BATTERYPACKSYS_BATTERYPACKSYS_IDT_IMPL_TYPE_BMSINTELTEMPMGTST_ENUM_IDT_H_
#define DENPENDENCYSRVEXE_INCLUDE_GWM_PT_BATTERYPACKSYS_BATTERYPACKSYS_IDT_IMPL_TYPE_BMSINTELTEMPMGTST_ENUM_IDT_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include <array>
#include <cstdint>
#include <map>
#include <string>
#include <vector>

namespace gwm {
namespace pt_batterypacksys {
namespace batterypacksys_idt {

/*!
 * \brief Type BMSIntelTempMgtSt_Enum_Idt.
 * \remark generated
 * \trace SPEC-5951372
 *
 * IMPLEMENTATION-DATA-TYPE /DataTypes/ImplementationDataTypes/BMSIntelTempMgtSt_Enum_Idt
 */
enum class BMSIntelTempMgtSt_Enum_Idt : std::uint8_t {
  Off = 0,
  On = 1
};

}  // namespace batterypacksys_idt
}  // namespace pt_batterypacksys
}  // namespace gwm

#endif  // DENPENDENCYSRVEXE_INCLUDE_GWM_PT_BATTERYPACKSYS_BATTERYPACKSYS_IDT_IMPL_TYPE_BMSINTELTEMPMGTST_ENUM_IDT_H_
