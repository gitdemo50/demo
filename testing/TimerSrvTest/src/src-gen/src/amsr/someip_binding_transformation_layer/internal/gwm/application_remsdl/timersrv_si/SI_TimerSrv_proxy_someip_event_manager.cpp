/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  TiMgrSrvExe/src/amsr/someip_binding_transformation_layer/internal/gwm/application_remsdl/timersrv_si/SI_TimerSrv_proxy_someip_event_manager.cpp
 *        \brief  SOME/IP proxy event handling for events and field notifications of service 'SI_TimerSrv'
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_someipbinding
 *         Commit ID: 43b33f0283d4d61385b98a3599688499abd86a75
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "amsr/someip_binding_transformation_layer/internal/gwm/application_remsdl/timersrv_si/SI_TimerSrv_proxy_someip_event_manager.h"

namespace amsr {
namespace someip_binding_transformation_layer {
namespace internal {

namespace gwm {
namespace application_remsdl {
namespace timersrv_si {

// ---- Field notifier 'TimerSt' -------------------------------------------

::amsr::someip_protocol::internal::ServiceId constexpr SI_TimerSrvProxySomeIpEventConfigurationTimerSt::kServiceId;
::amsr::someip_protocol::internal::EventId constexpr SI_TimerSrvProxySomeIpEventConfigurationTimerSt::kEventId;
vac::container::CStringView constexpr SI_TimerSrvProxySomeIpEventConfigurationTimerSt::kEventName;

}  // namespace internal
}  // namespace someip_binding_transformation_layer
}  // namespace amsr
}  // namespace timersrv_si
}  // namespace application_remsdl
}  // namespace gwm

