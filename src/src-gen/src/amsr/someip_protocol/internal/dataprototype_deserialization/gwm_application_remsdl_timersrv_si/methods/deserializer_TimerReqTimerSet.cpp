/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  TimerSrvExe/src/amsr/someip_protocol/internal/dataprototype_deserialization/gwm_application_remsdl_timersrv_si/methods/deserializer_TimerReqTimerSet.cpp
 *        \brief  SOME/IP protocol deserializer implementation for data prototype '/ServiceInterfaces/SI_TimerSrv/TimerReq/TimerSet
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_someipprotocol
 *         Commit ID: 0e137b9e6356987a6d2839c8d79c286219b68d60
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "someip-protocol/internal/deserialization/deser_wrapper.h"
#include "amsr/someip_protocol/internal/dataprototype_deserialization/gwm_application_remsdl_timersrv_si/methods/deserializer_TimerReqTimerSet.h"

namespace amsr {
namespace someip_protocol {
namespace internal {
namespace dataprototype_deserializer {
namespace gwm_application_remsdl_timersrv_si {
namespace methods {

deserialization::Result DeserializerTimerReqTimerSet::Deserialize(deserialization::Reader &reader, std::uint32_t &data) {
  // Transformation properties parameter pack for data prototype /ServiceInterfaces/SI_TimerSrv/TimerReq/TimerSet
    using TpPackAlias = deserialization::TpPack<
        BigEndian,
        deserialization::SizeOfArrayLengthField<0>, 
        deserialization::SizeOfVectorLengthField<4>,
        deserialization::SizeOfMapLengthField<4>,
        deserialization::SizeOfStringLengthField<4>,
        deserialization::SizeOfStructLengthField<0>,
        deserialization::SizeOfUnionLengthField<4>,
        deserialization::SizeOfUnionTypeSelectorField<4>,
        deserialization::StringBomActive,
        deserialization::StringNullTerminationActive>;


  // Verify static size
  constexpr std::size_t static_size{deserialization::SomeIpProtocolGetStaticSize<
      TpPackAlias,
        // Byte-order of primitive datatype (/AUTOSAR/StdTypes/uint32_t)
      typename deserialization::Tp<TpPackAlias>::ByteOrder

      >(deserialization::SizeToken<std::uint32_t>{})};

  deserialization::Result result{reader.VerifySize(static_size)};
  if (result) {
    // Deserialize byte stream
    result = deserialization::SomeIpProtocolDeserialize<
      TpPackAlias,
      // Byte-order of primitive datatype (/AUTOSAR/StdTypes/uint32_t)
      typename deserialization::Tp<TpPackAlias>::ByteOrder

      >(reader, data);
  }

  return result;
}

}  // namespace methods
}  // namespace gwm_application_remsdl_timersrv_si
}  // namespace dataprototype_deserializer
}  // namespace internal
}  // namespace someip_protocol
}  // namespace amsr

