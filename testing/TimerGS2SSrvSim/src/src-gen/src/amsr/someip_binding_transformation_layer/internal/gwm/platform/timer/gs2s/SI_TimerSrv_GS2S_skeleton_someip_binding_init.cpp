// VECTOR Same Line AutosarC++17_10-A1.1.1: MD_SOMEIPBINDING_AutosarC++17_10-A1.1.1_external_identifiers
/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/src/amsr/someip_binding_transformation_layer/internal/gwm/platform/timer/gs2s/SI_TimerSrv_GS2S_skeleton_someip_binding_init.cpp
 *        \brief  Skeleton-side ara::com SOME/IP binding initialization for ServiceInterface 'SI_TimerSrv_GS2S'
 *
 *      \details  Full ServiceInterface path: '/S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/SI_TimerSrv_GS2S'
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_someipbinding
 *         Commit ID: 43b33f0283d4d61385b98a3599688499abd86a75
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "amsr/someip_binding_transformation_layer/internal/gwm/platform/timer/gs2s/SI_TimerSrv_GS2S_skeleton_someip_binding_init.h"
#include "amsr/someip_binding_transformation_layer/internal/gwm/platform/timer/gs2s/SI_TimerSrv_GS2S_skeleton_someip_binding.h"
#include "ara/core/optional.h"

namespace amsr {
namespace someip_binding_transformation_layer {
namespace internal {

namespace gwm {
namespace platform {
namespace timer {
namespace gs2s {

void AraComSomeIpBindingInitializeSkeletonSomeIpEventBackendsSI_TimerSrv_GS2S(
    AraComSomeIpBindingSpecializationSkeleton::ServerManager& server_manager) {
  { // ServiceInstance: 0x1

    { // Field notifier: TimerSt

      // SOME/IP Skeleton event backend type for field 'TimerSt'.
      using SI_TimerSrv_GS2SSkeletonSomeIpEventBackendTimerSt = ::amsr::someip_binding_transformation_layer::internal::SomeIpSkeletonEventBackend<SI_TimerSrv_GS2SSkeletonSomeIpEventConfigurationTimerSt>;

      std::unique_ptr<SI_TimerSrv_GS2SSkeletonSomeIpEventBackendTimerSt> event_backend{
        std::make_unique<SI_TimerSrv_GS2SSkeletonSomeIpEventBackendTimerSt>(1U, server_manager)};
      gwm::platform::timer::gs2s::SI_TimerSrv_GS2SSkeletonSomeIpFieldNotifierTimerSt::EmplaceBackend(1U, std::move(event_backend));
    }
  }
}

void AraComSomeIpBindingDeInitializeSkeletonSomeIpEventBackendsSI_TimerSrv_GS2S() {

  // Field notifier: TimerSt
  gwm::platform::timer::gs2s::SI_TimerSrv_GS2SSkeletonSomeIpFieldNotifierTimerSt::ClearBackendList();

}


}  // namespace gs2s
}  // namespace timer
}  // namespace platform
}  // namespace gwm

}  // namespace internal
}  // namespace someip_binding_transformation_layer
}  // namespace amsr

