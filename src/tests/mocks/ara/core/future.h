#pragma once
#include <future>
#include <string_view>

namespace ara {
    namespace core {
        namespace FutureErrorDomain {
            enum Errc { broken_promise };
        }

        int MakeErrorCode(int a,int b, std::string c);

        template<typename T>
        using Future = std::future<T>;

        template<typename T>
        class Promise {
        public:
            void SetError(int a) { set_exception(std::make_exception_ptr(std::exception())); }

            void set_value(T v) { _prom.set_value(v); }
            void set_exception( std::exception_ptr p ) { _prom.set_exception(p); }
            Future<T> get_future() { return _prom.get_future(); }
        private:
            std::promise<T> _prom;
        };

        //using Promise = std::promise<T>;
        
        using StringView = std::string;

        class InstanceSpecifier {

            public:
            InstanceSpecifier(StringView v) : sv(v) {}

            std::string ToString() {
                return sv;
            }

            private:
                StringView sv;
        };

    }
}