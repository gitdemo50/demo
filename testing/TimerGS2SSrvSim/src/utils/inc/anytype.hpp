#ifndef GWM_UTIL_ANY_TYPE_HPP
#define GWM_UTIL_ANY_TYPE_HPP

/*
*   COPY RIGHT NOTICE
*   
*
*
*/

/*
*   Implement C++17 std::any kind of variant type
*   Idea is to have a concrete facade "AnyType" which aggregate a generic container type.
*   
*   It can hold any type now. However only for few types specilized cast and equality operations are overloaded
*   TODO: Improve equality, conversion and assignment operations
*/

#include <memory>
#include <typeinfo>
#include <exception>

namespace gwm {
namespace utils {

//  Anytype thorw this exception for error conditions
//  TODO: Improve to have better error details
struct InvalidCast : public std::exception {
   const char * what () const throw () {
      return "Invalid cast";
   }
};

//  Abstract base containter for types.
//  The implementation of this is generic
class AnyContainer {
public:
    virtual const std::type_info& type() const = 0;
    virtual std::unique_ptr<AnyContainer> duplicate() = 0;
    virtual const std::string toString() = 0;

    virtual ~AnyContainer() = default;  
};

//  Generic container
template<typename T>
class AnyContainerImpl : public AnyContainer {
public:
    AnyContainerImpl(T& val) : data(val) {}

    //  Type info is used to validate operations such as converson and comparisons
    const std::type_info& type() const  override {
        return typeid(T);
    }
    
    //  Return contained value
    const T& value() const {
        return data;
    }

    //  duplicate for copy
    std::unique_ptr<AnyContainer> duplicate() override {
        return std::make_unique<AnyContainerImpl<T>>(data);
    }

    //  Only specialized implementations support this operation
    const std::string toString() override { 
        throw InvalidCast();
    }
private:
    T data;
};

//  Specialization for 'bool' type
template<>
class AnyContainerImpl<bool> : public AnyContainer { 
public:
    AnyContainerImpl(const bool& val) : data(val) {}

    const std::type_info& type() const  override {
        return typeid(bool);
    }
    
    const bool& value() const {
        return data;
    }

    std::unique_ptr<AnyContainer> duplicate() override {
        return std::make_unique<AnyContainerImpl<bool>>(data);
    }

    const std::string toString() override { 
       return data ? "true" : "false";
    }

private:
    bool data;
};

//  Specialization for 'int' type
template<>
class AnyContainerImpl<int> : public AnyContainer { 
public:
    AnyContainerImpl(const int& val) : data(val) {}

    const std::type_info& type() const  override {
        return typeid(int);
    }
    
    const int& value() const {
        return data;
    }

    std::unique_ptr<AnyContainer> duplicate() override {
        return std::make_unique<AnyContainerImpl<int>>(data);
    }

    const std::string toString() override { 
       return std::to_string(data);
    }

private:
    int data;
};

//  Specialization for 'short' type
template<>
class AnyContainerImpl<short> : public AnyContainer { 
public:
    AnyContainerImpl(const short& val) : data(val) {}

    const std::type_info& type() const  override {
        return typeid(short);
    }
    
    const short& value() const {
        return data;
    }

    std::unique_ptr<AnyContainer> duplicate()  override {
        return std::make_unique<AnyContainerImpl<short>>(data);
    }

    const std::string toString() override { 
       return std::to_string(data);
    }

private:
    short data;
};

//  Specialization for 'char' type
template<>
class AnyContainerImpl<char> : public AnyContainer { 
public:
    AnyContainerImpl(const char& val) : data(val) {}

    const std::type_info& type() const  override {
        return typeid(char);
    }
    
    const char& value() const {
        return data;
    }

    std::unique_ptr<AnyContainer> duplicate()  override {
        return std::make_unique<AnyContainerImpl<char>>(data);
    }

    const std::string toString() override { 
       return std::string(1,data);
    }

private:
    char data;
};

//  Specialization for 'float' type
template<>
class AnyContainerImpl<float> : public AnyContainer { 
public:
    AnyContainerImpl(const float& val) : data(val) {}

    const std::type_info& type() const  override {
        return typeid(float);
    }
    
    const float& value() const {
        return data;
    }

    std::unique_ptr<AnyContainer> duplicate()  override {
        return std::make_unique<AnyContainerImpl<float>>(data);
    }

    const std::string toString() override { 
       return std::to_string(data);
    }

private:
    float data;
};

//  Specialization for 'double' type
template<>
class AnyContainerImpl<double> : public AnyContainer { 
public:
    AnyContainerImpl(const double& val) : data(val) {}

    const std::type_info& type() const  override {
        return typeid(double);
    }
    
    const double& value() const {
        return data;
    }

    std::unique_ptr<AnyContainer> duplicate()  override {
        return std::make_unique<AnyContainerImpl<double>>(data);
    }

    const std::string toString() override { 
       return std::to_string(data);
    }

private:
    double data;
};

/*
*   Concrete class to hold genetic type
*   Constructor is generic to store any type
*/

class AnyType {
public:

    AnyType() : container(nullptr) {

    }

    template<typename T>
    AnyType(T v) : container(std::make_unique<AnyContainerImpl<T>>(v)) {

    }

    AnyType(const AnyType& v) : container(v.container.get()->duplicate()) {
        
    }

    AnyType(AnyType&& v) : container(std::move(v.container)) {
        v.container = nullptr;
    }
    
    template<typename T>
    const T& getValue() const {
        if(container && (container->type() == typeid(T))) {
            AnyContainerImpl<T>* c = static_cast<AnyContainerImpl<T>*>(container.get());
            return c->value();
        } else if(!container) {
            throw InvalidCast();
        } else {
            throw InvalidCast();
        }
    }
    
    // explicit cast to operation
    template<typename T>
    explicit operator T () const {
        if(!container)
            throw InvalidCast();
        else if (typeid(T) == container->type()) {
            return getValue<T>();
        } else {
            //TODO: Some more explicit conversion should be able to do here
            printf("Container type %s\n",container->type().name());
            throw InvalidCast();
        }
    }

    // operator int() const {
    //     return 0;
    // }

    // template<typename T>
    // bool operator == (const T& other) const {
    //      if(container && (container->type() == typeid(T))) {
    //          return getValue() == other;
    //      } else {
    //          return false;
    //      }
    // }

    bool operator == (const AnyType& other) const {
         if(container && other.container && (container->type() == other.container->type())) {
             return container->toString() == other.container->toString();
         } else {
             return false;
         }
    }

    AnyType& operator = (const AnyType& other) {
        if(this != &other && !other.isEmpty()) {
            container = other.container->duplicate();
        } else if(other.isEmpty()) {
            container = nullptr;
        }
        return *this;
    }

    //  After a move operation the container can be empty
    bool isEmpty() const {
        if(container)
            return false;
        else
            return true; 
    }
    
    const std::string toString() { 
        if(container)
            return container->toString();
        else 
            throw InvalidCast();
    }
private:
    std::unique_ptr<AnyContainer> container;
};

}
}

#endif