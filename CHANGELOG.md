# 更新日志

## [1.0.0] - 2022-08-17

### 变更

* 使用刘晖（GW00273700）提供的DaVinci项目1.2.8生成的src-gen代码框架和配置json文件
* exec_config.json配置文件中FG名称变更为 BascSrv_TimerFG
* log日志配置变更为kRemote
* 服务名称变更为 TimerSrvExe

### 修复

* 依赖的服务不存在时，返回NOK，避免crash

### 新增

* timerReq的有效时间范围是 1-604800 
* 增加timerCncl
* 根据4.0项目要求，增加服务启动时，日志输出服务名称、版本号、编译日期的特性
* 新增Field字段的Get方法

### 优化

* 

### 已删除

* 
