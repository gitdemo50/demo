/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/src/gwm/platform/timer/gs2s/si_timersrv_gs2s_skeleton.cpp
 *        \brief  Skeleton for service 'SI_TimerSrv_GS2S'.
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_socal
 *         Commit ID: 209b3c0234013e84e3170ee826c9a8c6022de194
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "gwm/platform/timer/gs2s/si_timersrv_gs2s_skeleton.h"
#include "amsr/socal/internal/instance_specifier_lookup_table.h"

/*!
 * \trace SPEC-4980240
 * \trace SPEC-4980241
 */
namespace gwm {
namespace platform {
namespace timer {
namespace gs2s {
namespace skeleton {

/*!
 * \brief Static instance of service discovery.
 */
// VECTOR NC AutosarC++17_10-A3.3.2: MD_SOCAL_AutosarC++17_10-A3.3.2_StaticStorageDurationOfNonPODType
::amsr::generic::Singleton<SI_TimerSrv_GS2SSkeleton::ServiceDiscovery*> SI_TimerSrv_GS2SSkeleton::sd_;

SI_TimerSrv_GS2SSkeleton::ConstructionResult SI_TimerSrv_GS2SSkeleton::Preconstruct(
    ara::com::InstanceIdentifier instance_id, ara::com::MethodCallProcessingMode mode) noexcept {
return Base::Preconstruct(instance_id, mode);
}

SI_TimerSrv_GS2SSkeleton::ConstructionResult SI_TimerSrv_GS2SSkeleton::Preconstruct(
    ara::core::InstanceSpecifier instance, ara::com::MethodCallProcessingMode mode) noexcept {
return Base::Preconstruct(instance, mode);
}

SI_TimerSrv_GS2SSkeleton::ConstructionResult SI_TimerSrv_GS2SSkeleton::Preconstruct(
    ara::com::InstanceIdentifierContainer instance_identifiers, ara::com::MethodCallProcessingMode mode) noexcept {
return Base::Preconstruct(instance_identifiers, mode);
}

SI_TimerSrv_GS2SSkeleton::SI_TimerSrv_GS2SSkeleton(ConstructionToken&& token) noexcept
: Base{std::move(token)}
, TimerSt(this)
 {}

SI_TimerSrv_GS2SSkeleton::SI_TimerSrv_GS2SSkeleton(ara::com::InstanceIdentifier instance,
                                               ara::com::MethodCallProcessingMode mode) noexcept
     : SI_TimerSrv_GS2SSkeleton{Preconstruct(instance, mode).Value()} {}

SI_TimerSrv_GS2SSkeleton::SI_TimerSrv_GS2SSkeleton(ara::core::InstanceSpecifier instance,
                                               ara::com::MethodCallProcessingMode mode) noexcept
     : SI_TimerSrv_GS2SSkeleton{Preconstruct(instance, mode).Value()} {}

SI_TimerSrv_GS2SSkeleton::SI_TimerSrv_GS2SSkeleton(ara::com::InstanceIdentifierContainer instance_identifiers,
                                               ara::com::MethodCallProcessingMode mode) noexcept
     : SI_TimerSrv_GS2SSkeleton{Preconstruct(instance_identifiers, mode).Value()} {}

SI_TimerSrv_GS2SSkeleton::~SI_TimerSrv_GS2SSkeleton() noexcept {
  // Next line might block until all running method requests are done.
  StopOfferService();
}

void SI_TimerSrv_GS2SSkeleton::DoFieldInitializationChecks() noexcept {

    if (!TimerSt.IsUpdated()) {
        logger_.LogError([this](ara::log::LogStream& s) {
                          s << "Failed to offer service 'SI_TimerSrv_GS2S' with instance id(s) '";
                          for (::amsr::socal::internal::InstanceSpecifierLookupTableEntry const& entry : offered_instances_) {
                              ara::core::StringView const instance_id_str{entry.GetInstanceIdentifier().ToString()};
                              s << " " << instance_id_str;
                          }
                          s << "' - No initial value has been set for field 'TimerSt' (TimerSt::Update(...) has never been called)."; },
                          __func__, __LINE__);
        ara::core::Abort("No initial value has been set for field 'TimerSt' (TimerSt::Update(...) has never been called).");
    }
}

void SI_TimerSrv_GS2SSkeleton::SendInitialFieldNotifications() noexcept {
  // Send initial field events for all fields with "hasNotifier = true"
  TimerSt.SendInitialValue();
}

void SI_TimerSrv_GS2SSkeleton::OfferServiceInternal(::ara::com::InstanceIdentifier const& instance_id) noexcept {
  (*(sd_.GetAccess()))->OfferService(instance_id, this);
}

void SI_TimerSrv_GS2SSkeleton::StopOfferServiceInternal(::ara::com::InstanceIdentifier const& instance_id) noexcept {
  (*(sd_.GetAccess()))->StopOfferService(instance_id);
}

::amsr::generic::Singleton<SI_TimerSrv_GS2SSkeleton::ServiceDiscovery*>& SI_TimerSrv_GS2SSkeleton::GetServiceDiscovery() noexcept {
  return sd_;
}

void SI_TimerSrv_GS2SSkeleton::RegisterServiceDiscovery(SI_TimerSrv_GS2SSkeleton::ServiceDiscovery* service_discovery) noexcept {
  sd_.Create(service_discovery);
}

void SI_TimerSrv_GS2SSkeleton::DeRegisterServiceDiscovery() noexcept { sd_.Destroy(); }

}  // namespace skeleton
}  // namespace gs2s
}  // namespace timer
}  // namespace platform
}  // namespace gwm

