#pragma once

#include "shims.hpp"

#include "SI_TimerSrv_types.h"

namespace gwm {
namespace application_remsdl {
namespace timersrv_si {
namespace skeleton {

class SI_TimerSrvSkeleton;

namespace methods {

using TimerCncl = gwm::application_remsdl::timersrv_si::internal::methods::TimerCncl;

using TimerReq = gwm::application_remsdl::timersrv_si::internal::methods::TimerReq;

}  // namespace methods

namespace fields {

  class TimerSt : public gwm::services::FieldBase<gwm::platform_timer::timer_idt::TimerSt_Struct_Idt> {
  };


}  // namespace fields

class SI_TimerSrvSkeleton
    : public gwm::services::SkeletonBase {
 public:

  SI_TimerSrvSkeleton(ConstructionToken token) {}

  virtual ara::core::Future<methods::TimerCncl::Output> TimerCncl() = 0;

  virtual ara::core::Future<methods::TimerReq::Output> TimerReq(::gwm::platform_timer::timer_idt::TimerSet_Integer_Idt const& TimerSet) = 0;

  fields::TimerSt TimerSt;
};  // class SI_TimerSrvSkeleton

}  // namespace skeleton
}  // namespace timersrv_si
}  // namespace platform_timer
}  // namespace gwm


