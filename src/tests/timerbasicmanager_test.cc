#include <memory>
#include <condition_variable>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "TimerBasicManager.hpp"

namespace gwm {
namespace manager {

TEST(TimerBasicManager,TimerBasicManager) {
    std::condition_variable cv;
    std::mutex cv_m;

    int foundServices = 0;
    gwm::services::ProxyBase::setStopHandler([&foundServices,&cv](std::string s) {
        foundServices++;
        printf("Found %d \n",foundServices);
        if(foundServices >= 1)
        {
            cv.notify_all();
        }
    });

    TimerBasicManager::getInstance()->start();

    std::unique_lock<std::mutex> lk(cv_m);
    cv.wait(lk);

    std::this_thread::sleep_for(std::chrono::seconds(2));
    gwm::services::ProxyBase::setStopHandler(nullptr);
}

}}