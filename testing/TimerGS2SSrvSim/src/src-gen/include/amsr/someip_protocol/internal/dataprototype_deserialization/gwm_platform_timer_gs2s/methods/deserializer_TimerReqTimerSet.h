/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/include/amsr/someip_protocol/internal/dataprototype_deserialization/gwm_platform_timer_gs2s/methods/deserializer_TimerReqTimerSet.h
 *        \brief  SOME/IP protocol deserializer implementation for data prototype '/S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/SI_TimerSrv_GS2S/TimerReq/TimerSet
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_someipprotocol
 *         Commit ID: 0e137b9e6356987a6d2839c8d79c286219b68d60
 *********************************************************************************************************************/

#ifndef DENPENDENCYSRVEXE_INCLUDE_AMSR_SOMEIP_PROTOCOL_INTERNAL_DATAPROTOTYPE_DESERIALIZATION_GWM_PLATFORM_TIMER_GS2S_METHODS_deserializer_TimerReqTimerSet_h_
#define DENPENDENCYSRVEXE_INCLUDE_AMSR_SOMEIP_PROTOCOL_INTERNAL_DATAPROTOTYPE_DESERIALIZATION_GWM_PLATFORM_TIMER_GS2S_METHODS_deserializer_TimerReqTimerSet_h_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include <cstdint>
#include "someip-protocol/internal/deserialization/common.h"
#include "someip-protocol/internal/deserialization/reader.h"

namespace amsr {
namespace someip_protocol {
namespace internal {
namespace dataprototype_deserializer {
namespace gwm_platform_timer_gs2s {
namespace methods {

/*!
 * \brief Deserializer for method argument /S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/SI_TimerSrv_GS2S/TimerReq/TimerSet
 *        of service interface /S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/SI_TimerSrv_GS2S.
 * \details Top-Level data type: /AUTOSAR/StdTypes/uint32_t
 *          Effective transformation properties of the DataPrototype:
 *          - ByteOrder:                    MOST-SIGNIFICANT-BYTE-FIRST (big-endian)
 *          - sizeOfArrayLengthField:       0
 *          - sizeOfVectorLengthField:      4
 *          - sizeOfMapLengthField:         4
 *          - sizeOfStringLengthField:      4
 *          - sizeOfStructLengthField:      0
 *          - sizeOfUnionLengthField:       4
 *          - sizeOfUnionTypeSelectorField: 4
 *          - isBomActive:                  true
 *          - isNullTerminationActive:      true
 *          - isDynamicLengthFieldSize:     false
 */
class DeserializerTimerReqTimerSet {
 public:
  /*!
   * \brief Deserialize the data prototype method argument /S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/SI_TimerSrv_GS2S/TimerReq/TimerSet.
   *
   * \param[in,out] reader Reference to the byte stream reader.
   * \param[out]    data Reference to data object of top-level data type
   *                /AUTOSAR/StdTypes/uint32_t
   *                in which the deserialized value will be written.
   *
   * \pre           -
   * \context       Reactor|App
   * \threadsafe    FALSE
   * \reentrant     TRUE for different reader objects.
   * \return        True if the deserialization is successful, false otherwise.
   * \synchronous   TRUE
   */
  static deserialization::Result Deserialize(deserialization::Reader &reader, std::uint32_t &data);
};

}  // namespace methods
}  // namespace gwm_platform_timer_gs2s
}  // namespace dataprototype_deserializer
}  // namespace internal
}  // namespace someip_protocol
}  // namespace amsr

#endif  // DENPENDENCYSRVEXE_INCLUDE_AMSR_SOMEIP_PROTOCOL_INTERNAL_DATAPROTOTYPE_DESERIALIZATION_GWM_PLATFORM_TIMER_GS2S_METHODS_deserializer_TimerReqTimerSet_h_

