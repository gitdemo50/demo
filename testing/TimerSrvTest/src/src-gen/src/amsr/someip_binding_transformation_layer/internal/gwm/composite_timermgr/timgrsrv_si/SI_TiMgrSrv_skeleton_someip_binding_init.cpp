// VECTOR Same Line AutosarC++17_10-A1.1.1: MD_SOMEIPBINDING_AutosarC++17_10-A1.1.1_external_identifiers
/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  TiMgrSrvExe/src/amsr/someip_binding_transformation_layer/internal/gwm/composite_timermgr/timgrsrv_si/SI_TiMgrSrv_skeleton_someip_binding_init.cpp
 *        \brief  Skeleton-side ara::com SOME/IP binding initialization for ServiceInterface 'SI_TiMgrSrv'
 *
 *      \details  Full ServiceInterface path: '/ServiceInterfaces/SI_TiMgrSrv'
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_someipbinding
 *         Commit ID: 43b33f0283d4d61385b98a3599688499abd86a75
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "amsr/someip_binding_transformation_layer/internal/gwm/composite_timermgr/timgrsrv_si/SI_TiMgrSrv_skeleton_someip_binding_init.h"
#include "amsr/someip_binding_transformation_layer/internal/gwm/composite_timermgr/timgrsrv_si/SI_TiMgrSrv_skeleton_someip_binding.h"
#include "ara/core/optional.h"

namespace amsr {
namespace someip_binding_transformation_layer {
namespace internal {

namespace gwm {
namespace composite_timermgr {
namespace timgrsrv_si {

void AraComSomeIpBindingInitializeSkeletonSomeIpEventBackendsSI_TiMgrSrv(
    AraComSomeIpBindingSpecializationSkeleton::ServerManager& server_manager) {
  { // ServiceInstance: 0x1

    { // Field notifier: AlrmClkWkeEve

      // SOME/IP Skeleton event backend type for field 'AlrmClkWkeEve'.
      using SI_TiMgrSrvSkeletonSomeIpEventBackendAlrmClkWkeEve = ::amsr::someip_binding_transformation_layer::internal::SomeIpSkeletonEventBackend<SI_TiMgrSrvSkeletonSomeIpEventConfigurationAlrmClkWkeEve>;

      std::unique_ptr<SI_TiMgrSrvSkeletonSomeIpEventBackendAlrmClkWkeEve> event_backend{
        std::make_unique<SI_TiMgrSrvSkeletonSomeIpEventBackendAlrmClkWkeEve>(1U, server_manager)};
      gwm::composite_timermgr::timgrsrv_si::SI_TiMgrSrvSkeletonSomeIpFieldNotifierAlrmClkWkeEve::EmplaceBackend(1U, std::move(event_backend));
    }
  }
}

void AraComSomeIpBindingDeInitializeSkeletonSomeIpEventBackendsSI_TiMgrSrv() {

  // Field notifier: AlrmClkWkeEve
  gwm::composite_timermgr::timgrsrv_si::SI_TiMgrSrvSkeletonSomeIpFieldNotifierAlrmClkWkeEve::ClearBackendList();

}


}  // namespace timgrsrv_si
}  // namespace composite_timermgr
}  // namespace gwm

}  // namespace internal
}  // namespace someip_binding_transformation_layer
}  // namespace amsr

