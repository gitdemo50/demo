/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/include/amsr/someip_binding_transformation_layer/internal/gwm/pt_powersys/obcstsrv_si/SI_OBCStSrv_skeleton_someip_methods.h
 *        \brief  SOME/IP skeleton method de- /serialization handling for methods and field methods of service 'SI_OBCStSrv'
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_someipbinding
 *         Commit ID: 43b33f0283d4d61385b98a3599688499abd86a75
 *********************************************************************************************************************/

#ifndef DENPENDENCYSRVEXE_INCLUDE_AMSR_SOMEIP_BINDING_TRANSFORMATION_LAYER_INTERNAL_GWM_PT_POWERSYS_OBCSTSRV_SI_SI_OBCSTSRV_SKELETON_SOMEIP_METHODS_H_
#define DENPENDENCYSRVEXE_INCLUDE_AMSR_SOMEIP_BINDING_TRANSFORMATION_LAYER_INTERNAL_GWM_PT_POWERSYS_OBCSTSRV_SI_SI_OBCSTSRV_SKELETON_SOMEIP_METHODS_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include <memory>
#include "amsr/someip_protocol/internal/method_deserialization/gwm_pt_powersys_obcstsrv_si/fields/deserializer_Request_OBCChrgGunStGet.h"
#include "amsr/someip_protocol/internal/method_deserialization/gwm_pt_powersys_obcstsrv_si/fields/deserializer_Request_OBCErrStGet.h"
#include "amsr/someip_protocol/internal/method_deserialization/gwm_pt_powersys_obcstsrv_si/fields/deserializer_Request_OBCHVILStGet.h"
#include "amsr/someip_protocol/internal/method_deserialization/gwm_pt_powersys_obcstsrv_si/fields/deserializer_Request_OBCStGet.h"
#include "amsr/someip_protocol/internal/method_deserialization/gwm_pt_powersys_obcstsrv_si/fields/deserializer_Request_OBCV2XStGet.h"
#include "amsr/someip_protocol/internal/method_serialization/gwm_pt_powersys_obcstsrv_si/fields/serializer_Response_OBCChrgGunStGet.h"
#include "amsr/someip_protocol/internal/method_serialization/gwm_pt_powersys_obcstsrv_si/fields/serializer_Response_OBCErrStGet.h"
#include "amsr/someip_protocol/internal/method_serialization/gwm_pt_powersys_obcstsrv_si/fields/serializer_Response_OBCHVILStGet.h"
#include "amsr/someip_protocol/internal/method_serialization/gwm_pt_powersys_obcstsrv_si/fields/serializer_Response_OBCStGet.h"
#include "amsr/someip_protocol/internal/method_serialization/gwm_pt_powersys_obcstsrv_si/fields/serializer_Response_OBCV2XStGet.h"
#include "ara/core/result.h"
#include "gwm/pt_powersys/obcstsrv_si/SI_OBCStSrv_types.h"
#include "gwm/pt_powersys/obcstsrv_si/si_obcstsrv_skeleton.h"
#include "osabstraction/io/io_buffer.h"
#include "someip-protocol/internal/message.h"
#include "someip_binding_transformation_layer/internal/methods/skeleton_method_xf.h"
#include "someip_binding_transformation_layer/internal/methods/skeleton_response_handler.h"
#include "vac/container/c_string_view.h"

namespace amsr {
namespace someip_binding_transformation_layer {
namespace internal {
namespace gwm {
namespace pt_powersys {
namespace obcstsrv_si {

// Forward-declaration for back-reference
class SI_OBCStSrvSkeletonSomeIpBinding;


namespace fields {

/*!
 * \brief Forward-declaration for back-reference
 */
class SkeletonOBCChrgGunStGetAsyncRequest;

namespace obcchrggunstget {

// VECTOR NC AutosarC++17_10-M7.3.6, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.6_using_directive
// VECTOR NC AutosarC++17_10-M7.3.4, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.4_using_directive
using namespace vac::container::literals;  // NOLINT(build/namespaces)

/*!
 * \brief Struct combining all required types/values for creating the method manager.
 *
 * \remark generated
 */
struct SkeletonConfiguration final {

/*!
 * \brief Method name string.
 */
static constexpr vac::container::CStringView kName{"OBCChrgGunStGet"_sv};

/*!
 * \brief Method Id.
 */
static constexpr ::amsr::someip_protocol::internal::MethodId kMethodId{0x4005U};

/*!
 * \brief Skeleton SomeIp Binding.
 */
using SkeletonSomeIpBinding = SI_OBCStSrvSkeletonSomeIpBinding;

/*!
 * \brief Method Input struct.
 */
using Input = ::gwm::pt_powersys::obcstsrv_si::internal::fields::OBCChrgGunStGet::Input;

/*!
 * \brief Async request type.
 */
using AsyncRequest = SkeletonOBCChrgGunStGetAsyncRequest;

/*!
 * \brief Method Request deserializer.
 */
using MethodRequestDeserializer = ::amsr::someip_protocol::internal::method_deserializer::gwm_pt_powersys_obcstsrv_si::fields::DeserializerRequestOBCChrgGunStGet;

/*!
 * \brief Method response serializer.
 */
using MethodResponseSerializer = ::amsr::someip_protocol::internal::method_serializer::gwm_pt_powersys_obcstsrv_si::fields::SerializerResponseOkOBCChrgGunStGet;
};

/*!
 * \brief Method manager configuration class.
 *
 * \remark generated
 */
using SkeletonOBCChrgGunStGetConfiguration = ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonMethodXfConfiguration<SkeletonConfiguration::kMethodId, SkeletonConfiguration::SkeletonSomeIpBinding, SkeletonConfiguration::Input, SkeletonConfiguration::AsyncRequest, SkeletonConfiguration::MethodRequestDeserializer>;

} // namespace obcchrggunstget

/*!
 * \brief SOME/IP Skeleton method class for method 'OBCChrgGunStGet'.
 * \details Handles SOME/IP de-/serialization.
 * \remark generated
 */
using SkeletonOBCChrgGunStGet = ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonMethodXf<obcchrggunstget::SkeletonOBCChrgGunStGetConfiguration>;

/*!
 * \brief For each method request a task of this type is assembled and is passed to the skeleton's frontend,
 *        where it is being executed asynchronously according to the chosen processing mode.
 */
class SkeletonOBCChrgGunStGetAsyncRequest : public ::amsr::socal::internal::PendingRequest {
 public:
  /*!
   * \brief At initialization store all relevant information, to be able to invoke the method call
   *        and route the response to the binding-related part again.
   * \param[in] skeleton A pointer to the skeleton frontend for calling the concrete method asynchronously in a separate
   *                     worker thread.
   * \param[in] skeleton_method A pointer to the skeleton method class needed to send the response after processing the
   *                            method request.
   * \param[in] header The SOME/IP header which must be stored for a possible response
   *                   (in case this method is not fire & forget).
   * \param[in] packet Serialized Method Request [SOME/IP Header + Payload].
   * \pre -
   * \context     Reactor
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  SkeletonOBCChrgGunStGetAsyncRequest(::gwm::pt_powersys::obcstsrv_si::skeleton::SI_OBCStSrvSkeleton* skeleton,
      SkeletonOBCChrgGunStGet& skeleton_method,
      ::amsr::someip_protocol::internal::SomeIpMessageHeader const& header,
      ::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet)
      : ::amsr::socal::internal::PendingRequest{skeleton},
        skeleton_{skeleton},
        response_handler_{skeleton_method},
        header_{header},
        packet_{std::move(packet)} {}

  /*!
   * \brief   Operator gets called when method invocation is planned in the frontend.
   * \details It shall be called only once for each instance.
   * \pre -
   * \context     Callback
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   *
   */
  void operator()() override {

    ara::core::Result<::gwm::pt_powersys::obcstsrv_si::internal::fields::OBCChrgGunSt::Output> result{skeleton_->OBCChrgGunSt.Get().GetResult()};
    if (result.HasValue()) {
      response_handler_.SerializeAndSendMethodResponse<Serializer>(header_, result.Value());
    } else {
      response_handler_.SerializeAndSendApplicationErrorMethodResponse(header_, result.Error());
    }
  }

 private:
  /*!
   * \brief Serialization class of the method response.
   */
  using Serializer = obcchrggunstget::SkeletonConfiguration::MethodResponseSerializer;


  /*!
   * \brief Pointer to the skeleton to invoke the method request call.
   */
  ::gwm::pt_powersys::obcstsrv_si::skeleton::SI_OBCStSrvSkeleton* skeleton_;

  /*!
   * \brief Binding-related handler to serialize and send a response right after the method call.
   */
  ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonResponseHandler<SkeletonOBCChrgGunStGet> response_handler_;

  /*!
   * \brief Buffer the SOME/IP header from the request for the response with the corresponding client ID and session ID.
   */
  ::amsr::someip_protocol::internal::SomeIpMessageHeader const header_;

  /*!
   * \brief Serialized Method Request [SOME/IP Header + Payload].
   */
  ::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet_;
};


}  // namespace fields


namespace fields {

/*!
 * \brief Forward-declaration for back-reference
 */
class SkeletonOBCErrStGetAsyncRequest;

namespace obcerrstget {

// VECTOR NC AutosarC++17_10-M7.3.6, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.6_using_directive
// VECTOR NC AutosarC++17_10-M7.3.4, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.4_using_directive
using namespace vac::container::literals;  // NOLINT(build/namespaces)

/*!
 * \brief Struct combining all required types/values for creating the method manager.
 *
 * \remark generated
 */
struct SkeletonConfiguration final {

/*!
 * \brief Method name string.
 */
static constexpr vac::container::CStringView kName{"OBCErrStGet"_sv};

/*!
 * \brief Method Id.
 */
static constexpr ::amsr::someip_protocol::internal::MethodId kMethodId{0x4002U};

/*!
 * \brief Skeleton SomeIp Binding.
 */
using SkeletonSomeIpBinding = SI_OBCStSrvSkeletonSomeIpBinding;

/*!
 * \brief Method Input struct.
 */
using Input = ::gwm::pt_powersys::obcstsrv_si::internal::fields::OBCErrStGet::Input;

/*!
 * \brief Async request type.
 */
using AsyncRequest = SkeletonOBCErrStGetAsyncRequest;

/*!
 * \brief Method Request deserializer.
 */
using MethodRequestDeserializer = ::amsr::someip_protocol::internal::method_deserializer::gwm_pt_powersys_obcstsrv_si::fields::DeserializerRequestOBCErrStGet;

/*!
 * \brief Method response serializer.
 */
using MethodResponseSerializer = ::amsr::someip_protocol::internal::method_serializer::gwm_pt_powersys_obcstsrv_si::fields::SerializerResponseOkOBCErrStGet;
};

/*!
 * \brief Method manager configuration class.
 *
 * \remark generated
 */
using SkeletonOBCErrStGetConfiguration = ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonMethodXfConfiguration<SkeletonConfiguration::kMethodId, SkeletonConfiguration::SkeletonSomeIpBinding, SkeletonConfiguration::Input, SkeletonConfiguration::AsyncRequest, SkeletonConfiguration::MethodRequestDeserializer>;

} // namespace obcerrstget

/*!
 * \brief SOME/IP Skeleton method class for method 'OBCErrStGet'.
 * \details Handles SOME/IP de-/serialization.
 * \remark generated
 */
using SkeletonOBCErrStGet = ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonMethodXf<obcerrstget::SkeletonOBCErrStGetConfiguration>;

/*!
 * \brief For each method request a task of this type is assembled and is passed to the skeleton's frontend,
 *        where it is being executed asynchronously according to the chosen processing mode.
 */
class SkeletonOBCErrStGetAsyncRequest : public ::amsr::socal::internal::PendingRequest {
 public:
  /*!
   * \brief At initialization store all relevant information, to be able to invoke the method call
   *        and route the response to the binding-related part again.
   * \param[in] skeleton A pointer to the skeleton frontend for calling the concrete method asynchronously in a separate
   *                     worker thread.
   * \param[in] skeleton_method A pointer to the skeleton method class needed to send the response after processing the
   *                            method request.
   * \param[in] header The SOME/IP header which must be stored for a possible response
   *                   (in case this method is not fire & forget).
   * \param[in] packet Serialized Method Request [SOME/IP Header + Payload].
   * \pre -
   * \context     Reactor
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  SkeletonOBCErrStGetAsyncRequest(::gwm::pt_powersys::obcstsrv_si::skeleton::SI_OBCStSrvSkeleton* skeleton,
      SkeletonOBCErrStGet& skeleton_method,
      ::amsr::someip_protocol::internal::SomeIpMessageHeader const& header,
      ::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet)
      : ::amsr::socal::internal::PendingRequest{skeleton},
        skeleton_{skeleton},
        response_handler_{skeleton_method},
        header_{header},
        packet_{std::move(packet)} {}

  /*!
   * \brief   Operator gets called when method invocation is planned in the frontend.
   * \details It shall be called only once for each instance.
   * \pre -
   * \context     Callback
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   *
   */
  void operator()() override {

    ara::core::Result<::gwm::pt_powersys::obcstsrv_si::internal::fields::OBCErrSt::Output> result{skeleton_->OBCErrSt.Get().GetResult()};
    if (result.HasValue()) {
      response_handler_.SerializeAndSendMethodResponse<Serializer>(header_, result.Value());
    } else {
      response_handler_.SerializeAndSendApplicationErrorMethodResponse(header_, result.Error());
    }
  }

 private:
  /*!
   * \brief Serialization class of the method response.
   */
  using Serializer = obcerrstget::SkeletonConfiguration::MethodResponseSerializer;


  /*!
   * \brief Pointer to the skeleton to invoke the method request call.
   */
  ::gwm::pt_powersys::obcstsrv_si::skeleton::SI_OBCStSrvSkeleton* skeleton_;

  /*!
   * \brief Binding-related handler to serialize and send a response right after the method call.
   */
  ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonResponseHandler<SkeletonOBCErrStGet> response_handler_;

  /*!
   * \brief Buffer the SOME/IP header from the request for the response with the corresponding client ID and session ID.
   */
  ::amsr::someip_protocol::internal::SomeIpMessageHeader const header_;

  /*!
   * \brief Serialized Method Request [SOME/IP Header + Payload].
   */
  ::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet_;
};


}  // namespace fields


namespace fields {

/*!
 * \brief Forward-declaration for back-reference
 */
class SkeletonOBCHVILStGetAsyncRequest;

namespace obchvilstget {

// VECTOR NC AutosarC++17_10-M7.3.6, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.6_using_directive
// VECTOR NC AutosarC++17_10-M7.3.4, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.4_using_directive
using namespace vac::container::literals;  // NOLINT(build/namespaces)

/*!
 * \brief Struct combining all required types/values for creating the method manager.
 *
 * \remark generated
 */
struct SkeletonConfiguration final {

/*!
 * \brief Method name string.
 */
static constexpr vac::container::CStringView kName{"OBCHVILStGet"_sv};

/*!
 * \brief Method Id.
 */
static constexpr ::amsr::someip_protocol::internal::MethodId kMethodId{0x4003U};

/*!
 * \brief Skeleton SomeIp Binding.
 */
using SkeletonSomeIpBinding = SI_OBCStSrvSkeletonSomeIpBinding;

/*!
 * \brief Method Input struct.
 */
using Input = ::gwm::pt_powersys::obcstsrv_si::internal::fields::OBCHVILStGet::Input;

/*!
 * \brief Async request type.
 */
using AsyncRequest = SkeletonOBCHVILStGetAsyncRequest;

/*!
 * \brief Method Request deserializer.
 */
using MethodRequestDeserializer = ::amsr::someip_protocol::internal::method_deserializer::gwm_pt_powersys_obcstsrv_si::fields::DeserializerRequestOBCHVILStGet;

/*!
 * \brief Method response serializer.
 */
using MethodResponseSerializer = ::amsr::someip_protocol::internal::method_serializer::gwm_pt_powersys_obcstsrv_si::fields::SerializerResponseOkOBCHVILStGet;
};

/*!
 * \brief Method manager configuration class.
 *
 * \remark generated
 */
using SkeletonOBCHVILStGetConfiguration = ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonMethodXfConfiguration<SkeletonConfiguration::kMethodId, SkeletonConfiguration::SkeletonSomeIpBinding, SkeletonConfiguration::Input, SkeletonConfiguration::AsyncRequest, SkeletonConfiguration::MethodRequestDeserializer>;

} // namespace obchvilstget

/*!
 * \brief SOME/IP Skeleton method class for method 'OBCHVILStGet'.
 * \details Handles SOME/IP de-/serialization.
 * \remark generated
 */
using SkeletonOBCHVILStGet = ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonMethodXf<obchvilstget::SkeletonOBCHVILStGetConfiguration>;

/*!
 * \brief For each method request a task of this type is assembled and is passed to the skeleton's frontend,
 *        where it is being executed asynchronously according to the chosen processing mode.
 */
class SkeletonOBCHVILStGetAsyncRequest : public ::amsr::socal::internal::PendingRequest {
 public:
  /*!
   * \brief At initialization store all relevant information, to be able to invoke the method call
   *        and route the response to the binding-related part again.
   * \param[in] skeleton A pointer to the skeleton frontend for calling the concrete method asynchronously in a separate
   *                     worker thread.
   * \param[in] skeleton_method A pointer to the skeleton method class needed to send the response after processing the
   *                            method request.
   * \param[in] header The SOME/IP header which must be stored for a possible response
   *                   (in case this method is not fire & forget).
   * \param[in] packet Serialized Method Request [SOME/IP Header + Payload].
   * \pre -
   * \context     Reactor
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  SkeletonOBCHVILStGetAsyncRequest(::gwm::pt_powersys::obcstsrv_si::skeleton::SI_OBCStSrvSkeleton* skeleton,
      SkeletonOBCHVILStGet& skeleton_method,
      ::amsr::someip_protocol::internal::SomeIpMessageHeader const& header,
      ::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet)
      : ::amsr::socal::internal::PendingRequest{skeleton},
        skeleton_{skeleton},
        response_handler_{skeleton_method},
        header_{header},
        packet_{std::move(packet)} {}

  /*!
   * \brief   Operator gets called when method invocation is planned in the frontend.
   * \details It shall be called only once for each instance.
   * \pre -
   * \context     Callback
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   *
   */
  void operator()() override {

    ara::core::Result<::gwm::pt_powersys::obcstsrv_si::internal::fields::OBCHVILSt::Output> result{skeleton_->OBCHVILSt.Get().GetResult()};
    if (result.HasValue()) {
      response_handler_.SerializeAndSendMethodResponse<Serializer>(header_, result.Value());
    } else {
      response_handler_.SerializeAndSendApplicationErrorMethodResponse(header_, result.Error());
    }
  }

 private:
  /*!
   * \brief Serialization class of the method response.
   */
  using Serializer = obchvilstget::SkeletonConfiguration::MethodResponseSerializer;


  /*!
   * \brief Pointer to the skeleton to invoke the method request call.
   */
  ::gwm::pt_powersys::obcstsrv_si::skeleton::SI_OBCStSrvSkeleton* skeleton_;

  /*!
   * \brief Binding-related handler to serialize and send a response right after the method call.
   */
  ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonResponseHandler<SkeletonOBCHVILStGet> response_handler_;

  /*!
   * \brief Buffer the SOME/IP header from the request for the response with the corresponding client ID and session ID.
   */
  ::amsr::someip_protocol::internal::SomeIpMessageHeader const header_;

  /*!
   * \brief Serialized Method Request [SOME/IP Header + Payload].
   */
  ::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet_;
};


}  // namespace fields


namespace fields {

/*!
 * \brief Forward-declaration for back-reference
 */
class SkeletonOBCStGetAsyncRequest;

namespace obcstget {

// VECTOR NC AutosarC++17_10-M7.3.6, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.6_using_directive
// VECTOR NC AutosarC++17_10-M7.3.4, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.4_using_directive
using namespace vac::container::literals;  // NOLINT(build/namespaces)

/*!
 * \brief Struct combining all required types/values for creating the method manager.
 *
 * \remark generated
 */
struct SkeletonConfiguration final {

/*!
 * \brief Method name string.
 */
static constexpr vac::container::CStringView kName{"OBCStGet"_sv};

/*!
 * \brief Method Id.
 */
static constexpr ::amsr::someip_protocol::internal::MethodId kMethodId{0x4001U};

/*!
 * \brief Skeleton SomeIp Binding.
 */
using SkeletonSomeIpBinding = SI_OBCStSrvSkeletonSomeIpBinding;

/*!
 * \brief Method Input struct.
 */
using Input = ::gwm::pt_powersys::obcstsrv_si::internal::fields::OBCStGet::Input;

/*!
 * \brief Async request type.
 */
using AsyncRequest = SkeletonOBCStGetAsyncRequest;

/*!
 * \brief Method Request deserializer.
 */
using MethodRequestDeserializer = ::amsr::someip_protocol::internal::method_deserializer::gwm_pt_powersys_obcstsrv_si::fields::DeserializerRequestOBCStGet;

/*!
 * \brief Method response serializer.
 */
using MethodResponseSerializer = ::amsr::someip_protocol::internal::method_serializer::gwm_pt_powersys_obcstsrv_si::fields::SerializerResponseOkOBCStGet;
};

/*!
 * \brief Method manager configuration class.
 *
 * \remark generated
 */
using SkeletonOBCStGetConfiguration = ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonMethodXfConfiguration<SkeletonConfiguration::kMethodId, SkeletonConfiguration::SkeletonSomeIpBinding, SkeletonConfiguration::Input, SkeletonConfiguration::AsyncRequest, SkeletonConfiguration::MethodRequestDeserializer>;

} // namespace obcstget

/*!
 * \brief SOME/IP Skeleton method class for method 'OBCStGet'.
 * \details Handles SOME/IP de-/serialization.
 * \remark generated
 */
using SkeletonOBCStGet = ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonMethodXf<obcstget::SkeletonOBCStGetConfiguration>;

/*!
 * \brief For each method request a task of this type is assembled and is passed to the skeleton's frontend,
 *        where it is being executed asynchronously according to the chosen processing mode.
 */
class SkeletonOBCStGetAsyncRequest : public ::amsr::socal::internal::PendingRequest {
 public:
  /*!
   * \brief At initialization store all relevant information, to be able to invoke the method call
   *        and route the response to the binding-related part again.
   * \param[in] skeleton A pointer to the skeleton frontend for calling the concrete method asynchronously in a separate
   *                     worker thread.
   * \param[in] skeleton_method A pointer to the skeleton method class needed to send the response after processing the
   *                            method request.
   * \param[in] header The SOME/IP header which must be stored for a possible response
   *                   (in case this method is not fire & forget).
   * \param[in] packet Serialized Method Request [SOME/IP Header + Payload].
   * \pre -
   * \context     Reactor
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  SkeletonOBCStGetAsyncRequest(::gwm::pt_powersys::obcstsrv_si::skeleton::SI_OBCStSrvSkeleton* skeleton,
      SkeletonOBCStGet& skeleton_method,
      ::amsr::someip_protocol::internal::SomeIpMessageHeader const& header,
      ::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet)
      : ::amsr::socal::internal::PendingRequest{skeleton},
        skeleton_{skeleton},
        response_handler_{skeleton_method},
        header_{header},
        packet_{std::move(packet)} {}

  /*!
   * \brief   Operator gets called when method invocation is planned in the frontend.
   * \details It shall be called only once for each instance.
   * \pre -
   * \context     Callback
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   *
   */
  void operator()() override {

    ara::core::Result<::gwm::pt_powersys::obcstsrv_si::internal::fields::OBCSt::Output> result{skeleton_->OBCSt.Get().GetResult()};
    if (result.HasValue()) {
      response_handler_.SerializeAndSendMethodResponse<Serializer>(header_, result.Value());
    } else {
      response_handler_.SerializeAndSendApplicationErrorMethodResponse(header_, result.Error());
    }
  }

 private:
  /*!
   * \brief Serialization class of the method response.
   */
  using Serializer = obcstget::SkeletonConfiguration::MethodResponseSerializer;


  /*!
   * \brief Pointer to the skeleton to invoke the method request call.
   */
  ::gwm::pt_powersys::obcstsrv_si::skeleton::SI_OBCStSrvSkeleton* skeleton_;

  /*!
   * \brief Binding-related handler to serialize and send a response right after the method call.
   */
  ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonResponseHandler<SkeletonOBCStGet> response_handler_;

  /*!
   * \brief Buffer the SOME/IP header from the request for the response with the corresponding client ID and session ID.
   */
  ::amsr::someip_protocol::internal::SomeIpMessageHeader const header_;

  /*!
   * \brief Serialized Method Request [SOME/IP Header + Payload].
   */
  ::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet_;
};


}  // namespace fields


namespace fields {

/*!
 * \brief Forward-declaration for back-reference
 */
class SkeletonOBCV2XStGetAsyncRequest;

namespace obcv2xstget {

// VECTOR NC AutosarC++17_10-M7.3.6, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.6_using_directive
// VECTOR NC AutosarC++17_10-M7.3.4, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.4_using_directive
using namespace vac::container::literals;  // NOLINT(build/namespaces)

/*!
 * \brief Struct combining all required types/values for creating the method manager.
 *
 * \remark generated
 */
struct SkeletonConfiguration final {

/*!
 * \brief Method name string.
 */
static constexpr vac::container::CStringView kName{"OBCV2XStGet"_sv};

/*!
 * \brief Method Id.
 */
static constexpr ::amsr::someip_protocol::internal::MethodId kMethodId{0x4004U};

/*!
 * \brief Skeleton SomeIp Binding.
 */
using SkeletonSomeIpBinding = SI_OBCStSrvSkeletonSomeIpBinding;

/*!
 * \brief Method Input struct.
 */
using Input = ::gwm::pt_powersys::obcstsrv_si::internal::fields::OBCV2XStGet::Input;

/*!
 * \brief Async request type.
 */
using AsyncRequest = SkeletonOBCV2XStGetAsyncRequest;

/*!
 * \brief Method Request deserializer.
 */
using MethodRequestDeserializer = ::amsr::someip_protocol::internal::method_deserializer::gwm_pt_powersys_obcstsrv_si::fields::DeserializerRequestOBCV2XStGet;

/*!
 * \brief Method response serializer.
 */
using MethodResponseSerializer = ::amsr::someip_protocol::internal::method_serializer::gwm_pt_powersys_obcstsrv_si::fields::SerializerResponseOkOBCV2XStGet;
};

/*!
 * \brief Method manager configuration class.
 *
 * \remark generated
 */
using SkeletonOBCV2XStGetConfiguration = ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonMethodXfConfiguration<SkeletonConfiguration::kMethodId, SkeletonConfiguration::SkeletonSomeIpBinding, SkeletonConfiguration::Input, SkeletonConfiguration::AsyncRequest, SkeletonConfiguration::MethodRequestDeserializer>;

} // namespace obcv2xstget

/*!
 * \brief SOME/IP Skeleton method class for method 'OBCV2XStGet'.
 * \details Handles SOME/IP de-/serialization.
 * \remark generated
 */
using SkeletonOBCV2XStGet = ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonMethodXf<obcv2xstget::SkeletonOBCV2XStGetConfiguration>;

/*!
 * \brief For each method request a task of this type is assembled and is passed to the skeleton's frontend,
 *        where it is being executed asynchronously according to the chosen processing mode.
 */
class SkeletonOBCV2XStGetAsyncRequest : public ::amsr::socal::internal::PendingRequest {
 public:
  /*!
   * \brief At initialization store all relevant information, to be able to invoke the method call
   *        and route the response to the binding-related part again.
   * \param[in] skeleton A pointer to the skeleton frontend for calling the concrete method asynchronously in a separate
   *                     worker thread.
   * \param[in] skeleton_method A pointer to the skeleton method class needed to send the response after processing the
   *                            method request.
   * \param[in] header The SOME/IP header which must be stored for a possible response
   *                   (in case this method is not fire & forget).
   * \param[in] packet Serialized Method Request [SOME/IP Header + Payload].
   * \pre -
   * \context     Reactor
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  SkeletonOBCV2XStGetAsyncRequest(::gwm::pt_powersys::obcstsrv_si::skeleton::SI_OBCStSrvSkeleton* skeleton,
      SkeletonOBCV2XStGet& skeleton_method,
      ::amsr::someip_protocol::internal::SomeIpMessageHeader const& header,
      ::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet)
      : ::amsr::socal::internal::PendingRequest{skeleton},
        skeleton_{skeleton},
        response_handler_{skeleton_method},
        header_{header},
        packet_{std::move(packet)} {}

  /*!
   * \brief   Operator gets called when method invocation is planned in the frontend.
   * \details It shall be called only once for each instance.
   * \pre -
   * \context     Callback
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   *
   */
  void operator()() override {

    ara::core::Result<::gwm::pt_powersys::obcstsrv_si::internal::fields::OBCV2XSt::Output> result{skeleton_->OBCV2XSt.Get().GetResult()};
    if (result.HasValue()) {
      response_handler_.SerializeAndSendMethodResponse<Serializer>(header_, result.Value());
    } else {
      response_handler_.SerializeAndSendApplicationErrorMethodResponse(header_, result.Error());
    }
  }

 private:
  /*!
   * \brief Serialization class of the method response.
   */
  using Serializer = obcv2xstget::SkeletonConfiguration::MethodResponseSerializer;


  /*!
   * \brief Pointer to the skeleton to invoke the method request call.
   */
  ::gwm::pt_powersys::obcstsrv_si::skeleton::SI_OBCStSrvSkeleton* skeleton_;

  /*!
   * \brief Binding-related handler to serialize and send a response right after the method call.
   */
  ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonResponseHandler<SkeletonOBCV2XStGet> response_handler_;

  /*!
   * \brief Buffer the SOME/IP header from the request for the response with the corresponding client ID and session ID.
   */
  ::amsr::someip_protocol::internal::SomeIpMessageHeader const header_;

  /*!
   * \brief Serialized Method Request [SOME/IP Header + Payload].
   */
  ::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet_;
};


}  // namespace fields


}  // namespace internal
}  // namespace someip_binding_transformation_layer
}  // namespace amsr
}  // namespace obcstsrv_si
}  // namespace pt_powersys
}  // namespace gwm

#endif  // DENPENDENCYSRVEXE_INCLUDE_AMSR_SOMEIP_BINDING_TRANSFORMATION_LAYER_INTERNAL_GWM_PT_POWERSYS_OBCSTSRV_SI_SI_OBCSTSRV_SKELETON_SOMEIP_METHODS_H_

