/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  TiMgrSrvExe/include/ara/per/internal/config_validation_keys.h
 *        \brief
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_persistency_config
 *         Commit ID: 3c577be6a7ef0697cae45fabe324d59e826d2342
 *********************************************************************************************************************/

#ifndef TIMGRSRVEXE_INCLUDE_ARA_PER_INTERNAL_CONFIG_VALIDATION_KEYS_H_
#define TIMGRSRVEXE_INCLUDE_ARA_PER_INTERNAL_CONFIG_VALIDATION_KEYS_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "vac/container/string_literals.h"

namespace ara {
namespace per {
namespace internal {
/*!
 * \brief Unqualified access to the StringView literal.
 */
// VECTOR NL AutosarC++17_10-M7.3.6: MD_ARAPER_using_in_header
using vac::container::literals::operator""_sv;

/*!
 * \brief The design CRC of the FileStorage config.
 */
// VECTOR NL AutosarC++17_10-M3.4.1: MD_ARAPER_AutosarC++17_10-M3.4.1_can_be_declared_inside_function
constexpr static vac::container::CStringView kFileStorageConfigCrc{"0x21D7E482"_sv};

/*!
 * \brief The design CRC of the KeyValueStorage config.
 */
// VECTOR NL AutosarC++17_10-M3.4.1: MD_ARAPER_AutosarC++17_10-M3.4.1_can_be_declared_inside_function
constexpr static vac::container::CStringView kKeyValueStorageConfigCrc{"0x7862F0CE"_sv};
}  // namespace internal
}  // namespace per
}  // namespace ara

#endif  // TIMGRSRVEXE_INCLUDE_ARA_PER_INTERNAL_CONFIG_VALIDATION_KEYS_H_
