/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/include/amsr/someip_binding_transformation_layer/internal/gwm/pt_batterypacksys/bmsstsrv_si/SI_BmsStSrv_skeleton_someip_methods.h
 *        \brief  SOME/IP skeleton method de- /serialization handling for methods and field methods of service 'SI_BmsStSrv'
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_someipbinding
 *         Commit ID: 43b33f0283d4d61385b98a3599688499abd86a75
 *********************************************************************************************************************/

#ifndef DENPENDENCYSRVEXE_INCLUDE_AMSR_SOMEIP_BINDING_TRANSFORMATION_LAYER_INTERNAL_GWM_PT_BATTERYPACKSYS_BMSSTSRV_SI_SI_BMSSTSRV_SKELETON_SOMEIP_METHODS_H_
#define DENPENDENCYSRVEXE_INCLUDE_AMSR_SOMEIP_BINDING_TRANSFORMATION_LAYER_INTERNAL_GWM_PT_BATTERYPACKSYS_BMSSTSRV_SI_SI_BMSSTSRV_SKELETON_SOMEIP_METHODS_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include <memory>
#include "amsr/someip_protocol/internal/method_deserialization/gwm_pt_batterypacksys_bmsstsrv_si/fields/deserializer_Request_BMSChrgStGet.h"
#include "amsr/someip_protocol/internal/method_deserialization/gwm_pt_batterypacksys_bmsstsrv_si/fields/deserializer_Request_BMSDCChrgCnctStGet.h"
#include "amsr/someip_protocol/internal/method_deserialization/gwm_pt_batterypacksys_bmsstsrv_si/fields/deserializer_Request_BMSDCChrgEndReasGet.h"
#include "amsr/someip_protocol/internal/method_deserialization/gwm_pt_batterypacksys_bmsstsrv_si/fields/deserializer_Request_BMSDchrgPrmsStGet.h"
#include "amsr/someip_protocol/internal/method_deserialization/gwm_pt_batterypacksys_bmsstsrv_si/fields/deserializer_Request_BMSHeatRunawayStGet.h"
#include "amsr/someip_protocol/internal/method_deserialization/gwm_pt_batterypacksys_bmsstsrv_si/fields/deserializer_Request_BMSInnerSOCStGet.h"
#include "amsr/someip_protocol/internal/method_deserialization/gwm_pt_batterypacksys_bmsstsrv_si/fields/deserializer_Request_BMSIntelTempMgtStGet.h"
#include "amsr/someip_protocol/internal/method_deserialization/gwm_pt_batterypacksys_bmsstsrv_si/fields/deserializer_Request_BMSLowTempStGet.h"
#include "amsr/someip_protocol/internal/method_deserialization/gwm_pt_batterypacksys_bmsstsrv_si/fields/deserializer_Request_BMSRmtPreHeatStGet.h"
#include "amsr/someip_protocol/internal/method_deserialization/gwm_pt_batterypacksys_bmsstsrv_si/fields/deserializer_Request_BMSSafCnctStGet.h"
#include "amsr/someip_protocol/internal/method_deserialization/gwm_pt_batterypacksys_bmsstsrv_si/fields/deserializer_Request_BmsErrStGet.h"
#include "amsr/someip_protocol/internal/method_deserialization/gwm_pt_batterypacksys_bmsstsrv_si/fields/deserializer_Request_BmsHVILStGet.h"
#include "amsr/someip_protocol/internal/method_deserialization/gwm_pt_batterypacksys_bmsstsrv_si/fields/deserializer_Request_BmsIsoMeasStGet.h"
#include "amsr/someip_protocol/internal/method_deserialization/gwm_pt_batterypacksys_bmsstsrv_si/fields/deserializer_Request_BmsStGet.h"
#include "amsr/someip_protocol/internal/method_deserialization/gwm_pt_batterypacksys_bmsstsrv_si/fields/deserializer_Request_HVBattSOCLimGet.h"
#include "amsr/someip_protocol/internal/method_deserialization/gwm_pt_batterypacksys_bmsstsrv_si/fields/deserializer_Request_HVMaiSwtStGet.h"
#include "amsr/someip_protocol/internal/method_serialization/gwm_pt_batterypacksys_bmsstsrv_si/fields/serializer_Response_BMSChrgStGet.h"
#include "amsr/someip_protocol/internal/method_serialization/gwm_pt_batterypacksys_bmsstsrv_si/fields/serializer_Response_BMSDCChrgCnctStGet.h"
#include "amsr/someip_protocol/internal/method_serialization/gwm_pt_batterypacksys_bmsstsrv_si/fields/serializer_Response_BMSDCChrgEndReasGet.h"
#include "amsr/someip_protocol/internal/method_serialization/gwm_pt_batterypacksys_bmsstsrv_si/fields/serializer_Response_BMSDchrgPrmsStGet.h"
#include "amsr/someip_protocol/internal/method_serialization/gwm_pt_batterypacksys_bmsstsrv_si/fields/serializer_Response_BMSHeatRunawayStGet.h"
#include "amsr/someip_protocol/internal/method_serialization/gwm_pt_batterypacksys_bmsstsrv_si/fields/serializer_Response_BMSInnerSOCStGet.h"
#include "amsr/someip_protocol/internal/method_serialization/gwm_pt_batterypacksys_bmsstsrv_si/fields/serializer_Response_BMSIntelTempMgtStGet.h"
#include "amsr/someip_protocol/internal/method_serialization/gwm_pt_batterypacksys_bmsstsrv_si/fields/serializer_Response_BMSLowTempStGet.h"
#include "amsr/someip_protocol/internal/method_serialization/gwm_pt_batterypacksys_bmsstsrv_si/fields/serializer_Response_BMSRmtPreHeatStGet.h"
#include "amsr/someip_protocol/internal/method_serialization/gwm_pt_batterypacksys_bmsstsrv_si/fields/serializer_Response_BMSSafCnctStGet.h"
#include "amsr/someip_protocol/internal/method_serialization/gwm_pt_batterypacksys_bmsstsrv_si/fields/serializer_Response_BmsErrStGet.h"
#include "amsr/someip_protocol/internal/method_serialization/gwm_pt_batterypacksys_bmsstsrv_si/fields/serializer_Response_BmsHVILStGet.h"
#include "amsr/someip_protocol/internal/method_serialization/gwm_pt_batterypacksys_bmsstsrv_si/fields/serializer_Response_BmsIsoMeasStGet.h"
#include "amsr/someip_protocol/internal/method_serialization/gwm_pt_batterypacksys_bmsstsrv_si/fields/serializer_Response_BmsStGet.h"
#include "amsr/someip_protocol/internal/method_serialization/gwm_pt_batterypacksys_bmsstsrv_si/fields/serializer_Response_HVBattSOCLimGet.h"
#include "amsr/someip_protocol/internal/method_serialization/gwm_pt_batterypacksys_bmsstsrv_si/fields/serializer_Response_HVMaiSwtStGet.h"
#include "ara/core/result.h"
#include "gwm/pt_batterypacksys/bmsstsrv_si/SI_BmsStSrv_types.h"
#include "gwm/pt_batterypacksys/bmsstsrv_si/si_bmsstsrv_skeleton.h"
#include "osabstraction/io/io_buffer.h"
#include "someip-protocol/internal/message.h"
#include "someip_binding_transformation_layer/internal/methods/skeleton_method_xf.h"
#include "someip_binding_transformation_layer/internal/methods/skeleton_response_handler.h"
#include "vac/container/c_string_view.h"

namespace amsr {
namespace someip_binding_transformation_layer {
namespace internal {
namespace gwm {
namespace pt_batterypacksys {
namespace bmsstsrv_si {

// Forward-declaration for back-reference
class SI_BmsStSrvSkeletonSomeIpBinding;


namespace fields {

/*!
 * \brief Forward-declaration for back-reference
 */
class SkeletonBMSChrgStGetAsyncRequest;

namespace bmschrgstget {

// VECTOR NC AutosarC++17_10-M7.3.6, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.6_using_directive
// VECTOR NC AutosarC++17_10-M7.3.4, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.4_using_directive
using namespace vac::container::literals;  // NOLINT(build/namespaces)

/*!
 * \brief Struct combining all required types/values for creating the method manager.
 *
 * \remark generated
 */
struct SkeletonConfiguration final {

/*!
 * \brief Method name string.
 */
static constexpr vac::container::CStringView kName{"BMSChrgStGet"_sv};

/*!
 * \brief Method Id.
 */
static constexpr ::amsr::someip_protocol::internal::MethodId kMethodId{0x4009U};

/*!
 * \brief Skeleton SomeIp Binding.
 */
using SkeletonSomeIpBinding = SI_BmsStSrvSkeletonSomeIpBinding;

/*!
 * \brief Method Input struct.
 */
using Input = ::gwm::pt_batterypacksys::bmsstsrv_si::internal::fields::BMSChrgStGet::Input;

/*!
 * \brief Async request type.
 */
using AsyncRequest = SkeletonBMSChrgStGetAsyncRequest;

/*!
 * \brief Method Request deserializer.
 */
using MethodRequestDeserializer = ::amsr::someip_protocol::internal::method_deserializer::gwm_pt_batterypacksys_bmsstsrv_si::fields::DeserializerRequestBMSChrgStGet;

/*!
 * \brief Method response serializer.
 */
using MethodResponseSerializer = ::amsr::someip_protocol::internal::method_serializer::gwm_pt_batterypacksys_bmsstsrv_si::fields::SerializerResponseOkBMSChrgStGet;
};

/*!
 * \brief Method manager configuration class.
 *
 * \remark generated
 */
using SkeletonBMSChrgStGetConfiguration = ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonMethodXfConfiguration<SkeletonConfiguration::kMethodId, SkeletonConfiguration::SkeletonSomeIpBinding, SkeletonConfiguration::Input, SkeletonConfiguration::AsyncRequest, SkeletonConfiguration::MethodRequestDeserializer>;

} // namespace bmschrgstget

/*!
 * \brief SOME/IP Skeleton method class for method 'BMSChrgStGet'.
 * \details Handles SOME/IP de-/serialization.
 * \remark generated
 */
using SkeletonBMSChrgStGet = ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonMethodXf<bmschrgstget::SkeletonBMSChrgStGetConfiguration>;

/*!
 * \brief For each method request a task of this type is assembled and is passed to the skeleton's frontend,
 *        where it is being executed asynchronously according to the chosen processing mode.
 */
class SkeletonBMSChrgStGetAsyncRequest : public ::amsr::socal::internal::PendingRequest {
 public:
  /*!
   * \brief At initialization store all relevant information, to be able to invoke the method call
   *        and route the response to the binding-related part again.
   * \param[in] skeleton A pointer to the skeleton frontend for calling the concrete method asynchronously in a separate
   *                     worker thread.
   * \param[in] skeleton_method A pointer to the skeleton method class needed to send the response after processing the
   *                            method request.
   * \param[in] header The SOME/IP header which must be stored for a possible response
   *                   (in case this method is not fire & forget).
   * \param[in] packet Serialized Method Request [SOME/IP Header + Payload].
   * \pre -
   * \context     Reactor
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  SkeletonBMSChrgStGetAsyncRequest(::gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton* skeleton,
      SkeletonBMSChrgStGet& skeleton_method,
      ::amsr::someip_protocol::internal::SomeIpMessageHeader const& header,
      ::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet)
      : ::amsr::socal::internal::PendingRequest{skeleton},
        skeleton_{skeleton},
        response_handler_{skeleton_method},
        header_{header},
        packet_{std::move(packet)} {}

  /*!
   * \brief   Operator gets called when method invocation is planned in the frontend.
   * \details It shall be called only once for each instance.
   * \pre -
   * \context     Callback
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   *
   */
  void operator()() override {

    ara::core::Result<::gwm::pt_batterypacksys::bmsstsrv_si::internal::fields::BMSChrgSt::Output> result{skeleton_->BMSChrgSt.Get().GetResult()};
    if (result.HasValue()) {
      response_handler_.SerializeAndSendMethodResponse<Serializer>(header_, result.Value());
    } else {
      response_handler_.SerializeAndSendApplicationErrorMethodResponse(header_, result.Error());
    }
  }

 private:
  /*!
   * \brief Serialization class of the method response.
   */
  using Serializer = bmschrgstget::SkeletonConfiguration::MethodResponseSerializer;


  /*!
   * \brief Pointer to the skeleton to invoke the method request call.
   */
  ::gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton* skeleton_;

  /*!
   * \brief Binding-related handler to serialize and send a response right after the method call.
   */
  ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonResponseHandler<SkeletonBMSChrgStGet> response_handler_;

  /*!
   * \brief Buffer the SOME/IP header from the request for the response with the corresponding client ID and session ID.
   */
  ::amsr::someip_protocol::internal::SomeIpMessageHeader const header_;

  /*!
   * \brief Serialized Method Request [SOME/IP Header + Payload].
   */
  ::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet_;
};


}  // namespace fields


namespace fields {

/*!
 * \brief Forward-declaration for back-reference
 */
class SkeletonBMSDCChrgCnctStGetAsyncRequest;

namespace bmsdcchrgcnctstget {

// VECTOR NC AutosarC++17_10-M7.3.6, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.6_using_directive
// VECTOR NC AutosarC++17_10-M7.3.4, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.4_using_directive
using namespace vac::container::literals;  // NOLINT(build/namespaces)

/*!
 * \brief Struct combining all required types/values for creating the method manager.
 *
 * \remark generated
 */
struct SkeletonConfiguration final {

/*!
 * \brief Method name string.
 */
static constexpr vac::container::CStringView kName{"BMSDCChrgCnctStGet"_sv};

/*!
 * \brief Method Id.
 */
static constexpr ::amsr::someip_protocol::internal::MethodId kMethodId{0x4008U};

/*!
 * \brief Skeleton SomeIp Binding.
 */
using SkeletonSomeIpBinding = SI_BmsStSrvSkeletonSomeIpBinding;

/*!
 * \brief Method Input struct.
 */
using Input = ::gwm::pt_batterypacksys::bmsstsrv_si::internal::fields::BMSDCChrgCnctStGet::Input;

/*!
 * \brief Async request type.
 */
using AsyncRequest = SkeletonBMSDCChrgCnctStGetAsyncRequest;

/*!
 * \brief Method Request deserializer.
 */
using MethodRequestDeserializer = ::amsr::someip_protocol::internal::method_deserializer::gwm_pt_batterypacksys_bmsstsrv_si::fields::DeserializerRequestBMSDCChrgCnctStGet;

/*!
 * \brief Method response serializer.
 */
using MethodResponseSerializer = ::amsr::someip_protocol::internal::method_serializer::gwm_pt_batterypacksys_bmsstsrv_si::fields::SerializerResponseOkBMSDCChrgCnctStGet;
};

/*!
 * \brief Method manager configuration class.
 *
 * \remark generated
 */
using SkeletonBMSDCChrgCnctStGetConfiguration = ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonMethodXfConfiguration<SkeletonConfiguration::kMethodId, SkeletonConfiguration::SkeletonSomeIpBinding, SkeletonConfiguration::Input, SkeletonConfiguration::AsyncRequest, SkeletonConfiguration::MethodRequestDeserializer>;

} // namespace bmsdcchrgcnctstget

/*!
 * \brief SOME/IP Skeleton method class for method 'BMSDCChrgCnctStGet'.
 * \details Handles SOME/IP de-/serialization.
 * \remark generated
 */
using SkeletonBMSDCChrgCnctStGet = ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonMethodXf<bmsdcchrgcnctstget::SkeletonBMSDCChrgCnctStGetConfiguration>;

/*!
 * \brief For each method request a task of this type is assembled and is passed to the skeleton's frontend,
 *        where it is being executed asynchronously according to the chosen processing mode.
 */
class SkeletonBMSDCChrgCnctStGetAsyncRequest : public ::amsr::socal::internal::PendingRequest {
 public:
  /*!
   * \brief At initialization store all relevant information, to be able to invoke the method call
   *        and route the response to the binding-related part again.
   * \param[in] skeleton A pointer to the skeleton frontend for calling the concrete method asynchronously in a separate
   *                     worker thread.
   * \param[in] skeleton_method A pointer to the skeleton method class needed to send the response after processing the
   *                            method request.
   * \param[in] header The SOME/IP header which must be stored for a possible response
   *                   (in case this method is not fire & forget).
   * \param[in] packet Serialized Method Request [SOME/IP Header + Payload].
   * \pre -
   * \context     Reactor
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  SkeletonBMSDCChrgCnctStGetAsyncRequest(::gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton* skeleton,
      SkeletonBMSDCChrgCnctStGet& skeleton_method,
      ::amsr::someip_protocol::internal::SomeIpMessageHeader const& header,
      ::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet)
      : ::amsr::socal::internal::PendingRequest{skeleton},
        skeleton_{skeleton},
        response_handler_{skeleton_method},
        header_{header},
        packet_{std::move(packet)} {}

  /*!
   * \brief   Operator gets called when method invocation is planned in the frontend.
   * \details It shall be called only once for each instance.
   * \pre -
   * \context     Callback
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   *
   */
  void operator()() override {

    ara::core::Result<::gwm::pt_batterypacksys::bmsstsrv_si::internal::fields::BMSDCChrgCnctSt::Output> result{skeleton_->BMSDCChrgCnctSt.Get().GetResult()};
    if (result.HasValue()) {
      response_handler_.SerializeAndSendMethodResponse<Serializer>(header_, result.Value());
    } else {
      response_handler_.SerializeAndSendApplicationErrorMethodResponse(header_, result.Error());
    }
  }

 private:
  /*!
   * \brief Serialization class of the method response.
   */
  using Serializer = bmsdcchrgcnctstget::SkeletonConfiguration::MethodResponseSerializer;


  /*!
   * \brief Pointer to the skeleton to invoke the method request call.
   */
  ::gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton* skeleton_;

  /*!
   * \brief Binding-related handler to serialize and send a response right after the method call.
   */
  ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonResponseHandler<SkeletonBMSDCChrgCnctStGet> response_handler_;

  /*!
   * \brief Buffer the SOME/IP header from the request for the response with the corresponding client ID and session ID.
   */
  ::amsr::someip_protocol::internal::SomeIpMessageHeader const header_;

  /*!
   * \brief Serialized Method Request [SOME/IP Header + Payload].
   */
  ::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet_;
};


}  // namespace fields


namespace fields {

/*!
 * \brief Forward-declaration for back-reference
 */
class SkeletonBMSDCChrgEndReasGetAsyncRequest;

namespace bmsdcchrgendreasget {

// VECTOR NC AutosarC++17_10-M7.3.6, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.6_using_directive
// VECTOR NC AutosarC++17_10-M7.3.4, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.4_using_directive
using namespace vac::container::literals;  // NOLINT(build/namespaces)

/*!
 * \brief Struct combining all required types/values for creating the method manager.
 *
 * \remark generated
 */
struct SkeletonConfiguration final {

/*!
 * \brief Method name string.
 */
static constexpr vac::container::CStringView kName{"BMSDCChrgEndReasGet"_sv};

/*!
 * \brief Method Id.
 */
static constexpr ::amsr::someip_protocol::internal::MethodId kMethodId{0x400FU};

/*!
 * \brief Skeleton SomeIp Binding.
 */
using SkeletonSomeIpBinding = SI_BmsStSrvSkeletonSomeIpBinding;

/*!
 * \brief Method Input struct.
 */
using Input = ::gwm::pt_batterypacksys::bmsstsrv_si::internal::fields::BMSDCChrgEndReasGet::Input;

/*!
 * \brief Async request type.
 */
using AsyncRequest = SkeletonBMSDCChrgEndReasGetAsyncRequest;

/*!
 * \brief Method Request deserializer.
 */
using MethodRequestDeserializer = ::amsr::someip_protocol::internal::method_deserializer::gwm_pt_batterypacksys_bmsstsrv_si::fields::DeserializerRequestBMSDCChrgEndReasGet;

/*!
 * \brief Method response serializer.
 */
using MethodResponseSerializer = ::amsr::someip_protocol::internal::method_serializer::gwm_pt_batterypacksys_bmsstsrv_si::fields::SerializerResponseOkBMSDCChrgEndReasGet;
};

/*!
 * \brief Method manager configuration class.
 *
 * \remark generated
 */
using SkeletonBMSDCChrgEndReasGetConfiguration = ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonMethodXfConfiguration<SkeletonConfiguration::kMethodId, SkeletonConfiguration::SkeletonSomeIpBinding, SkeletonConfiguration::Input, SkeletonConfiguration::AsyncRequest, SkeletonConfiguration::MethodRequestDeserializer>;

} // namespace bmsdcchrgendreasget

/*!
 * \brief SOME/IP Skeleton method class for method 'BMSDCChrgEndReasGet'.
 * \details Handles SOME/IP de-/serialization.
 * \remark generated
 */
using SkeletonBMSDCChrgEndReasGet = ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonMethodXf<bmsdcchrgendreasget::SkeletonBMSDCChrgEndReasGetConfiguration>;

/*!
 * \brief For each method request a task of this type is assembled and is passed to the skeleton's frontend,
 *        where it is being executed asynchronously according to the chosen processing mode.
 */
class SkeletonBMSDCChrgEndReasGetAsyncRequest : public ::amsr::socal::internal::PendingRequest {
 public:
  /*!
   * \brief At initialization store all relevant information, to be able to invoke the method call
   *        and route the response to the binding-related part again.
   * \param[in] skeleton A pointer to the skeleton frontend for calling the concrete method asynchronously in a separate
   *                     worker thread.
   * \param[in] skeleton_method A pointer to the skeleton method class needed to send the response after processing the
   *                            method request.
   * \param[in] header The SOME/IP header which must be stored for a possible response
   *                   (in case this method is not fire & forget).
   * \param[in] packet Serialized Method Request [SOME/IP Header + Payload].
   * \pre -
   * \context     Reactor
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  SkeletonBMSDCChrgEndReasGetAsyncRequest(::gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton* skeleton,
      SkeletonBMSDCChrgEndReasGet& skeleton_method,
      ::amsr::someip_protocol::internal::SomeIpMessageHeader const& header,
      ::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet)
      : ::amsr::socal::internal::PendingRequest{skeleton},
        skeleton_{skeleton},
        response_handler_{skeleton_method},
        header_{header},
        packet_{std::move(packet)} {}

  /*!
   * \brief   Operator gets called when method invocation is planned in the frontend.
   * \details It shall be called only once for each instance.
   * \pre -
   * \context     Callback
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   *
   */
  void operator()() override {

    ara::core::Result<::gwm::pt_batterypacksys::bmsstsrv_si::internal::fields::BMSDCChrgEndReas::Output> result{skeleton_->BMSDCChrgEndReas.Get().GetResult()};
    if (result.HasValue()) {
      response_handler_.SerializeAndSendMethodResponse<Serializer>(header_, result.Value());
    } else {
      response_handler_.SerializeAndSendApplicationErrorMethodResponse(header_, result.Error());
    }
  }

 private:
  /*!
   * \brief Serialization class of the method response.
   */
  using Serializer = bmsdcchrgendreasget::SkeletonConfiguration::MethodResponseSerializer;


  /*!
   * \brief Pointer to the skeleton to invoke the method request call.
   */
  ::gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton* skeleton_;

  /*!
   * \brief Binding-related handler to serialize and send a response right after the method call.
   */
  ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonResponseHandler<SkeletonBMSDCChrgEndReasGet> response_handler_;

  /*!
   * \brief Buffer the SOME/IP header from the request for the response with the corresponding client ID and session ID.
   */
  ::amsr::someip_protocol::internal::SomeIpMessageHeader const header_;

  /*!
   * \brief Serialized Method Request [SOME/IP Header + Payload].
   */
  ::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet_;
};


}  // namespace fields


namespace fields {

/*!
 * \brief Forward-declaration for back-reference
 */
class SkeletonBMSDchrgPrmsStGetAsyncRequest;

namespace bmsdchrgprmsstget {

// VECTOR NC AutosarC++17_10-M7.3.6, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.6_using_directive
// VECTOR NC AutosarC++17_10-M7.3.4, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.4_using_directive
using namespace vac::container::literals;  // NOLINT(build/namespaces)

/*!
 * \brief Struct combining all required types/values for creating the method manager.
 *
 * \remark generated
 */
struct SkeletonConfiguration final {

/*!
 * \brief Method name string.
 */
static constexpr vac::container::CStringView kName{"BMSDchrgPrmsStGet"_sv};

/*!
 * \brief Method Id.
 */
static constexpr ::amsr::someip_protocol::internal::MethodId kMethodId{0x400DU};

/*!
 * \brief Skeleton SomeIp Binding.
 */
using SkeletonSomeIpBinding = SI_BmsStSrvSkeletonSomeIpBinding;

/*!
 * \brief Method Input struct.
 */
using Input = ::gwm::pt_batterypacksys::bmsstsrv_si::internal::fields::BMSDchrgPrmsStGet::Input;

/*!
 * \brief Async request type.
 */
using AsyncRequest = SkeletonBMSDchrgPrmsStGetAsyncRequest;

/*!
 * \brief Method Request deserializer.
 */
using MethodRequestDeserializer = ::amsr::someip_protocol::internal::method_deserializer::gwm_pt_batterypacksys_bmsstsrv_si::fields::DeserializerRequestBMSDchrgPrmsStGet;

/*!
 * \brief Method response serializer.
 */
using MethodResponseSerializer = ::amsr::someip_protocol::internal::method_serializer::gwm_pt_batterypacksys_bmsstsrv_si::fields::SerializerResponseOkBMSDchrgPrmsStGet;
};

/*!
 * \brief Method manager configuration class.
 *
 * \remark generated
 */
using SkeletonBMSDchrgPrmsStGetConfiguration = ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonMethodXfConfiguration<SkeletonConfiguration::kMethodId, SkeletonConfiguration::SkeletonSomeIpBinding, SkeletonConfiguration::Input, SkeletonConfiguration::AsyncRequest, SkeletonConfiguration::MethodRequestDeserializer>;

} // namespace bmsdchrgprmsstget

/*!
 * \brief SOME/IP Skeleton method class for method 'BMSDchrgPrmsStGet'.
 * \details Handles SOME/IP de-/serialization.
 * \remark generated
 */
using SkeletonBMSDchrgPrmsStGet = ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonMethodXf<bmsdchrgprmsstget::SkeletonBMSDchrgPrmsStGetConfiguration>;

/*!
 * \brief For each method request a task of this type is assembled and is passed to the skeleton's frontend,
 *        where it is being executed asynchronously according to the chosen processing mode.
 */
class SkeletonBMSDchrgPrmsStGetAsyncRequest : public ::amsr::socal::internal::PendingRequest {
 public:
  /*!
   * \brief At initialization store all relevant information, to be able to invoke the method call
   *        and route the response to the binding-related part again.
   * \param[in] skeleton A pointer to the skeleton frontend for calling the concrete method asynchronously in a separate
   *                     worker thread.
   * \param[in] skeleton_method A pointer to the skeleton method class needed to send the response after processing the
   *                            method request.
   * \param[in] header The SOME/IP header which must be stored for a possible response
   *                   (in case this method is not fire & forget).
   * \param[in] packet Serialized Method Request [SOME/IP Header + Payload].
   * \pre -
   * \context     Reactor
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  SkeletonBMSDchrgPrmsStGetAsyncRequest(::gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton* skeleton,
      SkeletonBMSDchrgPrmsStGet& skeleton_method,
      ::amsr::someip_protocol::internal::SomeIpMessageHeader const& header,
      ::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet)
      : ::amsr::socal::internal::PendingRequest{skeleton},
        skeleton_{skeleton},
        response_handler_{skeleton_method},
        header_{header},
        packet_{std::move(packet)} {}

  /*!
   * \brief   Operator gets called when method invocation is planned in the frontend.
   * \details It shall be called only once for each instance.
   * \pre -
   * \context     Callback
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   *
   */
  void operator()() override {

    ara::core::Result<::gwm::pt_batterypacksys::bmsstsrv_si::internal::fields::BMSDchrgPrmsSt::Output> result{skeleton_->BMSDchrgPrmsSt.Get().GetResult()};
    if (result.HasValue()) {
      response_handler_.SerializeAndSendMethodResponse<Serializer>(header_, result.Value());
    } else {
      response_handler_.SerializeAndSendApplicationErrorMethodResponse(header_, result.Error());
    }
  }

 private:
  /*!
   * \brief Serialization class of the method response.
   */
  using Serializer = bmsdchrgprmsstget::SkeletonConfiguration::MethodResponseSerializer;


  /*!
   * \brief Pointer to the skeleton to invoke the method request call.
   */
  ::gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton* skeleton_;

  /*!
   * \brief Binding-related handler to serialize and send a response right after the method call.
   */
  ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonResponseHandler<SkeletonBMSDchrgPrmsStGet> response_handler_;

  /*!
   * \brief Buffer the SOME/IP header from the request for the response with the corresponding client ID and session ID.
   */
  ::amsr::someip_protocol::internal::SomeIpMessageHeader const header_;

  /*!
   * \brief Serialized Method Request [SOME/IP Header + Payload].
   */
  ::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet_;
};


}  // namespace fields


namespace fields {

/*!
 * \brief Forward-declaration for back-reference
 */
class SkeletonBMSHeatRunawayStGetAsyncRequest;

namespace bmsheatrunawaystget {

// VECTOR NC AutosarC++17_10-M7.3.6, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.6_using_directive
// VECTOR NC AutosarC++17_10-M7.3.4, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.4_using_directive
using namespace vac::container::literals;  // NOLINT(build/namespaces)

/*!
 * \brief Struct combining all required types/values for creating the method manager.
 *
 * \remark generated
 */
struct SkeletonConfiguration final {

/*!
 * \brief Method name string.
 */
static constexpr vac::container::CStringView kName{"BMSHeatRunawayStGet"_sv};

/*!
 * \brief Method Id.
 */
static constexpr ::amsr::someip_protocol::internal::MethodId kMethodId{0x400EU};

/*!
 * \brief Skeleton SomeIp Binding.
 */
using SkeletonSomeIpBinding = SI_BmsStSrvSkeletonSomeIpBinding;

/*!
 * \brief Method Input struct.
 */
using Input = ::gwm::pt_batterypacksys::bmsstsrv_si::internal::fields::BMSHeatRunawayStGet::Input;

/*!
 * \brief Async request type.
 */
using AsyncRequest = SkeletonBMSHeatRunawayStGetAsyncRequest;

/*!
 * \brief Method Request deserializer.
 */
using MethodRequestDeserializer = ::amsr::someip_protocol::internal::method_deserializer::gwm_pt_batterypacksys_bmsstsrv_si::fields::DeserializerRequestBMSHeatRunawayStGet;

/*!
 * \brief Method response serializer.
 */
using MethodResponseSerializer = ::amsr::someip_protocol::internal::method_serializer::gwm_pt_batterypacksys_bmsstsrv_si::fields::SerializerResponseOkBMSHeatRunawayStGet;
};

/*!
 * \brief Method manager configuration class.
 *
 * \remark generated
 */
using SkeletonBMSHeatRunawayStGetConfiguration = ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonMethodXfConfiguration<SkeletonConfiguration::kMethodId, SkeletonConfiguration::SkeletonSomeIpBinding, SkeletonConfiguration::Input, SkeletonConfiguration::AsyncRequest, SkeletonConfiguration::MethodRequestDeserializer>;

} // namespace bmsheatrunawaystget

/*!
 * \brief SOME/IP Skeleton method class for method 'BMSHeatRunawayStGet'.
 * \details Handles SOME/IP de-/serialization.
 * \remark generated
 */
using SkeletonBMSHeatRunawayStGet = ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonMethodXf<bmsheatrunawaystget::SkeletonBMSHeatRunawayStGetConfiguration>;

/*!
 * \brief For each method request a task of this type is assembled and is passed to the skeleton's frontend,
 *        where it is being executed asynchronously according to the chosen processing mode.
 */
class SkeletonBMSHeatRunawayStGetAsyncRequest : public ::amsr::socal::internal::PendingRequest {
 public:
  /*!
   * \brief At initialization store all relevant information, to be able to invoke the method call
   *        and route the response to the binding-related part again.
   * \param[in] skeleton A pointer to the skeleton frontend for calling the concrete method asynchronously in a separate
   *                     worker thread.
   * \param[in] skeleton_method A pointer to the skeleton method class needed to send the response after processing the
   *                            method request.
   * \param[in] header The SOME/IP header which must be stored for a possible response
   *                   (in case this method is not fire & forget).
   * \param[in] packet Serialized Method Request [SOME/IP Header + Payload].
   * \pre -
   * \context     Reactor
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  SkeletonBMSHeatRunawayStGetAsyncRequest(::gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton* skeleton,
      SkeletonBMSHeatRunawayStGet& skeleton_method,
      ::amsr::someip_protocol::internal::SomeIpMessageHeader const& header,
      ::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet)
      : ::amsr::socal::internal::PendingRequest{skeleton},
        skeleton_{skeleton},
        response_handler_{skeleton_method},
        header_{header},
        packet_{std::move(packet)} {}

  /*!
   * \brief   Operator gets called when method invocation is planned in the frontend.
   * \details It shall be called only once for each instance.
   * \pre -
   * \context     Callback
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   *
   */
  void operator()() override {

    ara::core::Result<::gwm::pt_batterypacksys::bmsstsrv_si::internal::fields::BMSHeatRunawaySt::Output> result{skeleton_->BMSHeatRunawaySt.Get().GetResult()};
    if (result.HasValue()) {
      response_handler_.SerializeAndSendMethodResponse<Serializer>(header_, result.Value());
    } else {
      response_handler_.SerializeAndSendApplicationErrorMethodResponse(header_, result.Error());
    }
  }

 private:
  /*!
   * \brief Serialization class of the method response.
   */
  using Serializer = bmsheatrunawaystget::SkeletonConfiguration::MethodResponseSerializer;


  /*!
   * \brief Pointer to the skeleton to invoke the method request call.
   */
  ::gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton* skeleton_;

  /*!
   * \brief Binding-related handler to serialize and send a response right after the method call.
   */
  ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonResponseHandler<SkeletonBMSHeatRunawayStGet> response_handler_;

  /*!
   * \brief Buffer the SOME/IP header from the request for the response with the corresponding client ID and session ID.
   */
  ::amsr::someip_protocol::internal::SomeIpMessageHeader const header_;

  /*!
   * \brief Serialized Method Request [SOME/IP Header + Payload].
   */
  ::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet_;
};


}  // namespace fields


namespace fields {

/*!
 * \brief Forward-declaration for back-reference
 */
class SkeletonBMSInnerSOCStGetAsyncRequest;

namespace bmsinnersocstget {

// VECTOR NC AutosarC++17_10-M7.3.6, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.6_using_directive
// VECTOR NC AutosarC++17_10-M7.3.4, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.4_using_directive
using namespace vac::container::literals;  // NOLINT(build/namespaces)

/*!
 * \brief Struct combining all required types/values for creating the method manager.
 *
 * \remark generated
 */
struct SkeletonConfiguration final {

/*!
 * \brief Method name string.
 */
static constexpr vac::container::CStringView kName{"BMSInnerSOCStGet"_sv};

/*!
 * \brief Method Id.
 */
static constexpr ::amsr::someip_protocol::internal::MethodId kMethodId{0x400CU};

/*!
 * \brief Skeleton SomeIp Binding.
 */
using SkeletonSomeIpBinding = SI_BmsStSrvSkeletonSomeIpBinding;

/*!
 * \brief Method Input struct.
 */
using Input = ::gwm::pt_batterypacksys::bmsstsrv_si::internal::fields::BMSInnerSOCStGet::Input;

/*!
 * \brief Async request type.
 */
using AsyncRequest = SkeletonBMSInnerSOCStGetAsyncRequest;

/*!
 * \brief Method Request deserializer.
 */
using MethodRequestDeserializer = ::amsr::someip_protocol::internal::method_deserializer::gwm_pt_batterypacksys_bmsstsrv_si::fields::DeserializerRequestBMSInnerSOCStGet;

/*!
 * \brief Method response serializer.
 */
using MethodResponseSerializer = ::amsr::someip_protocol::internal::method_serializer::gwm_pt_batterypacksys_bmsstsrv_si::fields::SerializerResponseOkBMSInnerSOCStGet;
};

/*!
 * \brief Method manager configuration class.
 *
 * \remark generated
 */
using SkeletonBMSInnerSOCStGetConfiguration = ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonMethodXfConfiguration<SkeletonConfiguration::kMethodId, SkeletonConfiguration::SkeletonSomeIpBinding, SkeletonConfiguration::Input, SkeletonConfiguration::AsyncRequest, SkeletonConfiguration::MethodRequestDeserializer>;

} // namespace bmsinnersocstget

/*!
 * \brief SOME/IP Skeleton method class for method 'BMSInnerSOCStGet'.
 * \details Handles SOME/IP de-/serialization.
 * \remark generated
 */
using SkeletonBMSInnerSOCStGet = ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonMethodXf<bmsinnersocstget::SkeletonBMSInnerSOCStGetConfiguration>;

/*!
 * \brief For each method request a task of this type is assembled and is passed to the skeleton's frontend,
 *        where it is being executed asynchronously according to the chosen processing mode.
 */
class SkeletonBMSInnerSOCStGetAsyncRequest : public ::amsr::socal::internal::PendingRequest {
 public:
  /*!
   * \brief At initialization store all relevant information, to be able to invoke the method call
   *        and route the response to the binding-related part again.
   * \param[in] skeleton A pointer to the skeleton frontend for calling the concrete method asynchronously in a separate
   *                     worker thread.
   * \param[in] skeleton_method A pointer to the skeleton method class needed to send the response after processing the
   *                            method request.
   * \param[in] header The SOME/IP header which must be stored for a possible response
   *                   (in case this method is not fire & forget).
   * \param[in] packet Serialized Method Request [SOME/IP Header + Payload].
   * \pre -
   * \context     Reactor
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  SkeletonBMSInnerSOCStGetAsyncRequest(::gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton* skeleton,
      SkeletonBMSInnerSOCStGet& skeleton_method,
      ::amsr::someip_protocol::internal::SomeIpMessageHeader const& header,
      ::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet)
      : ::amsr::socal::internal::PendingRequest{skeleton},
        skeleton_{skeleton},
        response_handler_{skeleton_method},
        header_{header},
        packet_{std::move(packet)} {}

  /*!
   * \brief   Operator gets called when method invocation is planned in the frontend.
   * \details It shall be called only once for each instance.
   * \pre -
   * \context     Callback
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   *
   */
  void operator()() override {

    ara::core::Result<::gwm::pt_batterypacksys::bmsstsrv_si::internal::fields::BMSInnerSOCSt::Output> result{skeleton_->BMSInnerSOCSt.Get().GetResult()};
    if (result.HasValue()) {
      response_handler_.SerializeAndSendMethodResponse<Serializer>(header_, result.Value());
    } else {
      response_handler_.SerializeAndSendApplicationErrorMethodResponse(header_, result.Error());
    }
  }

 private:
  /*!
   * \brief Serialization class of the method response.
   */
  using Serializer = bmsinnersocstget::SkeletonConfiguration::MethodResponseSerializer;


  /*!
   * \brief Pointer to the skeleton to invoke the method request call.
   */
  ::gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton* skeleton_;

  /*!
   * \brief Binding-related handler to serialize and send a response right after the method call.
   */
  ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonResponseHandler<SkeletonBMSInnerSOCStGet> response_handler_;

  /*!
   * \brief Buffer the SOME/IP header from the request for the response with the corresponding client ID and session ID.
   */
  ::amsr::someip_protocol::internal::SomeIpMessageHeader const header_;

  /*!
   * \brief Serialized Method Request [SOME/IP Header + Payload].
   */
  ::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet_;
};


}  // namespace fields


namespace fields {

/*!
 * \brief Forward-declaration for back-reference
 */
class SkeletonBMSIntelTempMgtStGetAsyncRequest;

namespace bmsinteltempmgtstget {

// VECTOR NC AutosarC++17_10-M7.3.6, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.6_using_directive
// VECTOR NC AutosarC++17_10-M7.3.4, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.4_using_directive
using namespace vac::container::literals;  // NOLINT(build/namespaces)

/*!
 * \brief Struct combining all required types/values for creating the method manager.
 *
 * \remark generated
 */
struct SkeletonConfiguration final {

/*!
 * \brief Method name string.
 */
static constexpr vac::container::CStringView kName{"BMSIntelTempMgtStGet"_sv};

/*!
 * \brief Method Id.
 */
static constexpr ::amsr::someip_protocol::internal::MethodId kMethodId{0x400BU};

/*!
 * \brief Skeleton SomeIp Binding.
 */
using SkeletonSomeIpBinding = SI_BmsStSrvSkeletonSomeIpBinding;

/*!
 * \brief Method Input struct.
 */
using Input = ::gwm::pt_batterypacksys::bmsstsrv_si::internal::fields::BMSIntelTempMgtStGet::Input;

/*!
 * \brief Async request type.
 */
using AsyncRequest = SkeletonBMSIntelTempMgtStGetAsyncRequest;

/*!
 * \brief Method Request deserializer.
 */
using MethodRequestDeserializer = ::amsr::someip_protocol::internal::method_deserializer::gwm_pt_batterypacksys_bmsstsrv_si::fields::DeserializerRequestBMSIntelTempMgtStGet;

/*!
 * \brief Method response serializer.
 */
using MethodResponseSerializer = ::amsr::someip_protocol::internal::method_serializer::gwm_pt_batterypacksys_bmsstsrv_si::fields::SerializerResponseOkBMSIntelTempMgtStGet;
};

/*!
 * \brief Method manager configuration class.
 *
 * \remark generated
 */
using SkeletonBMSIntelTempMgtStGetConfiguration = ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonMethodXfConfiguration<SkeletonConfiguration::kMethodId, SkeletonConfiguration::SkeletonSomeIpBinding, SkeletonConfiguration::Input, SkeletonConfiguration::AsyncRequest, SkeletonConfiguration::MethodRequestDeserializer>;

} // namespace bmsinteltempmgtstget

/*!
 * \brief SOME/IP Skeleton method class for method 'BMSIntelTempMgtStGet'.
 * \details Handles SOME/IP de-/serialization.
 * \remark generated
 */
using SkeletonBMSIntelTempMgtStGet = ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonMethodXf<bmsinteltempmgtstget::SkeletonBMSIntelTempMgtStGetConfiguration>;

/*!
 * \brief For each method request a task of this type is assembled and is passed to the skeleton's frontend,
 *        where it is being executed asynchronously according to the chosen processing mode.
 */
class SkeletonBMSIntelTempMgtStGetAsyncRequest : public ::amsr::socal::internal::PendingRequest {
 public:
  /*!
   * \brief At initialization store all relevant information, to be able to invoke the method call
   *        and route the response to the binding-related part again.
   * \param[in] skeleton A pointer to the skeleton frontend for calling the concrete method asynchronously in a separate
   *                     worker thread.
   * \param[in] skeleton_method A pointer to the skeleton method class needed to send the response after processing the
   *                            method request.
   * \param[in] header The SOME/IP header which must be stored for a possible response
   *                   (in case this method is not fire & forget).
   * \param[in] packet Serialized Method Request [SOME/IP Header + Payload].
   * \pre -
   * \context     Reactor
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  SkeletonBMSIntelTempMgtStGetAsyncRequest(::gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton* skeleton,
      SkeletonBMSIntelTempMgtStGet& skeleton_method,
      ::amsr::someip_protocol::internal::SomeIpMessageHeader const& header,
      ::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet)
      : ::amsr::socal::internal::PendingRequest{skeleton},
        skeleton_{skeleton},
        response_handler_{skeleton_method},
        header_{header},
        packet_{std::move(packet)} {}

  /*!
   * \brief   Operator gets called when method invocation is planned in the frontend.
   * \details It shall be called only once for each instance.
   * \pre -
   * \context     Callback
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   *
   */
  void operator()() override {

    ara::core::Result<::gwm::pt_batterypacksys::bmsstsrv_si::internal::fields::BMSIntelTempMgtSt::Output> result{skeleton_->BMSIntelTempMgtSt.Get().GetResult()};
    if (result.HasValue()) {
      response_handler_.SerializeAndSendMethodResponse<Serializer>(header_, result.Value());
    } else {
      response_handler_.SerializeAndSendApplicationErrorMethodResponse(header_, result.Error());
    }
  }

 private:
  /*!
   * \brief Serialization class of the method response.
   */
  using Serializer = bmsinteltempmgtstget::SkeletonConfiguration::MethodResponseSerializer;


  /*!
   * \brief Pointer to the skeleton to invoke the method request call.
   */
  ::gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton* skeleton_;

  /*!
   * \brief Binding-related handler to serialize and send a response right after the method call.
   */
  ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonResponseHandler<SkeletonBMSIntelTempMgtStGet> response_handler_;

  /*!
   * \brief Buffer the SOME/IP header from the request for the response with the corresponding client ID and session ID.
   */
  ::amsr::someip_protocol::internal::SomeIpMessageHeader const header_;

  /*!
   * \brief Serialized Method Request [SOME/IP Header + Payload].
   */
  ::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet_;
};


}  // namespace fields


namespace fields {

/*!
 * \brief Forward-declaration for back-reference
 */
class SkeletonBMSLowTempStGetAsyncRequest;

namespace bmslowtempstget {

// VECTOR NC AutosarC++17_10-M7.3.6, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.6_using_directive
// VECTOR NC AutosarC++17_10-M7.3.4, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.4_using_directive
using namespace vac::container::literals;  // NOLINT(build/namespaces)

/*!
 * \brief Struct combining all required types/values for creating the method manager.
 *
 * \remark generated
 */
struct SkeletonConfiguration final {

/*!
 * \brief Method name string.
 */
static constexpr vac::container::CStringView kName{"BMSLowTempStGet"_sv};

/*!
 * \brief Method Id.
 */
static constexpr ::amsr::someip_protocol::internal::MethodId kMethodId{0x400AU};

/*!
 * \brief Skeleton SomeIp Binding.
 */
using SkeletonSomeIpBinding = SI_BmsStSrvSkeletonSomeIpBinding;

/*!
 * \brief Method Input struct.
 */
using Input = ::gwm::pt_batterypacksys::bmsstsrv_si::internal::fields::BMSLowTempStGet::Input;

/*!
 * \brief Async request type.
 */
using AsyncRequest = SkeletonBMSLowTempStGetAsyncRequest;

/*!
 * \brief Method Request deserializer.
 */
using MethodRequestDeserializer = ::amsr::someip_protocol::internal::method_deserializer::gwm_pt_batterypacksys_bmsstsrv_si::fields::DeserializerRequestBMSLowTempStGet;

/*!
 * \brief Method response serializer.
 */
using MethodResponseSerializer = ::amsr::someip_protocol::internal::method_serializer::gwm_pt_batterypacksys_bmsstsrv_si::fields::SerializerResponseOkBMSLowTempStGet;
};

/*!
 * \brief Method manager configuration class.
 *
 * \remark generated
 */
using SkeletonBMSLowTempStGetConfiguration = ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonMethodXfConfiguration<SkeletonConfiguration::kMethodId, SkeletonConfiguration::SkeletonSomeIpBinding, SkeletonConfiguration::Input, SkeletonConfiguration::AsyncRequest, SkeletonConfiguration::MethodRequestDeserializer>;

} // namespace bmslowtempstget

/*!
 * \brief SOME/IP Skeleton method class for method 'BMSLowTempStGet'.
 * \details Handles SOME/IP de-/serialization.
 * \remark generated
 */
using SkeletonBMSLowTempStGet = ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonMethodXf<bmslowtempstget::SkeletonBMSLowTempStGetConfiguration>;

/*!
 * \brief For each method request a task of this type is assembled and is passed to the skeleton's frontend,
 *        where it is being executed asynchronously according to the chosen processing mode.
 */
class SkeletonBMSLowTempStGetAsyncRequest : public ::amsr::socal::internal::PendingRequest {
 public:
  /*!
   * \brief At initialization store all relevant information, to be able to invoke the method call
   *        and route the response to the binding-related part again.
   * \param[in] skeleton A pointer to the skeleton frontend for calling the concrete method asynchronously in a separate
   *                     worker thread.
   * \param[in] skeleton_method A pointer to the skeleton method class needed to send the response after processing the
   *                            method request.
   * \param[in] header The SOME/IP header which must be stored for a possible response
   *                   (in case this method is not fire & forget).
   * \param[in] packet Serialized Method Request [SOME/IP Header + Payload].
   * \pre -
   * \context     Reactor
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  SkeletonBMSLowTempStGetAsyncRequest(::gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton* skeleton,
      SkeletonBMSLowTempStGet& skeleton_method,
      ::amsr::someip_protocol::internal::SomeIpMessageHeader const& header,
      ::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet)
      : ::amsr::socal::internal::PendingRequest{skeleton},
        skeleton_{skeleton},
        response_handler_{skeleton_method},
        header_{header},
        packet_{std::move(packet)} {}

  /*!
   * \brief   Operator gets called when method invocation is planned in the frontend.
   * \details It shall be called only once for each instance.
   * \pre -
   * \context     Callback
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   *
   */
  void operator()() override {

    ara::core::Result<::gwm::pt_batterypacksys::bmsstsrv_si::internal::fields::BMSLowTempSt::Output> result{skeleton_->BMSLowTempSt.Get().GetResult()};
    if (result.HasValue()) {
      response_handler_.SerializeAndSendMethodResponse<Serializer>(header_, result.Value());
    } else {
      response_handler_.SerializeAndSendApplicationErrorMethodResponse(header_, result.Error());
    }
  }

 private:
  /*!
   * \brief Serialization class of the method response.
   */
  using Serializer = bmslowtempstget::SkeletonConfiguration::MethodResponseSerializer;


  /*!
   * \brief Pointer to the skeleton to invoke the method request call.
   */
  ::gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton* skeleton_;

  /*!
   * \brief Binding-related handler to serialize and send a response right after the method call.
   */
  ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonResponseHandler<SkeletonBMSLowTempStGet> response_handler_;

  /*!
   * \brief Buffer the SOME/IP header from the request for the response with the corresponding client ID and session ID.
   */
  ::amsr::someip_protocol::internal::SomeIpMessageHeader const header_;

  /*!
   * \brief Serialized Method Request [SOME/IP Header + Payload].
   */
  ::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet_;
};


}  // namespace fields


namespace fields {

/*!
 * \brief Forward-declaration for back-reference
 */
class SkeletonBMSRmtPreHeatStGetAsyncRequest;

namespace bmsrmtpreheatstget {

// VECTOR NC AutosarC++17_10-M7.3.6, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.6_using_directive
// VECTOR NC AutosarC++17_10-M7.3.4, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.4_using_directive
using namespace vac::container::literals;  // NOLINT(build/namespaces)

/*!
 * \brief Struct combining all required types/values for creating the method manager.
 *
 * \remark generated
 */
struct SkeletonConfiguration final {

/*!
 * \brief Method name string.
 */
static constexpr vac::container::CStringView kName{"BMSRmtPreHeatStGet"_sv};

/*!
 * \brief Method Id.
 */
static constexpr ::amsr::someip_protocol::internal::MethodId kMethodId{0x4006U};

/*!
 * \brief Skeleton SomeIp Binding.
 */
using SkeletonSomeIpBinding = SI_BmsStSrvSkeletonSomeIpBinding;

/*!
 * \brief Method Input struct.
 */
using Input = ::gwm::pt_batterypacksys::bmsstsrv_si::internal::fields::BMSRmtPreHeatStGet::Input;

/*!
 * \brief Async request type.
 */
using AsyncRequest = SkeletonBMSRmtPreHeatStGetAsyncRequest;

/*!
 * \brief Method Request deserializer.
 */
using MethodRequestDeserializer = ::amsr::someip_protocol::internal::method_deserializer::gwm_pt_batterypacksys_bmsstsrv_si::fields::DeserializerRequestBMSRmtPreHeatStGet;

/*!
 * \brief Method response serializer.
 */
using MethodResponseSerializer = ::amsr::someip_protocol::internal::method_serializer::gwm_pt_batterypacksys_bmsstsrv_si::fields::SerializerResponseOkBMSRmtPreHeatStGet;
};

/*!
 * \brief Method manager configuration class.
 *
 * \remark generated
 */
using SkeletonBMSRmtPreHeatStGetConfiguration = ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonMethodXfConfiguration<SkeletonConfiguration::kMethodId, SkeletonConfiguration::SkeletonSomeIpBinding, SkeletonConfiguration::Input, SkeletonConfiguration::AsyncRequest, SkeletonConfiguration::MethodRequestDeserializer>;

} // namespace bmsrmtpreheatstget

/*!
 * \brief SOME/IP Skeleton method class for method 'BMSRmtPreHeatStGet'.
 * \details Handles SOME/IP de-/serialization.
 * \remark generated
 */
using SkeletonBMSRmtPreHeatStGet = ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonMethodXf<bmsrmtpreheatstget::SkeletonBMSRmtPreHeatStGetConfiguration>;

/*!
 * \brief For each method request a task of this type is assembled and is passed to the skeleton's frontend,
 *        where it is being executed asynchronously according to the chosen processing mode.
 */
class SkeletonBMSRmtPreHeatStGetAsyncRequest : public ::amsr::socal::internal::PendingRequest {
 public:
  /*!
   * \brief At initialization store all relevant information, to be able to invoke the method call
   *        and route the response to the binding-related part again.
   * \param[in] skeleton A pointer to the skeleton frontend for calling the concrete method asynchronously in a separate
   *                     worker thread.
   * \param[in] skeleton_method A pointer to the skeleton method class needed to send the response after processing the
   *                            method request.
   * \param[in] header The SOME/IP header which must be stored for a possible response
   *                   (in case this method is not fire & forget).
   * \param[in] packet Serialized Method Request [SOME/IP Header + Payload].
   * \pre -
   * \context     Reactor
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  SkeletonBMSRmtPreHeatStGetAsyncRequest(::gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton* skeleton,
      SkeletonBMSRmtPreHeatStGet& skeleton_method,
      ::amsr::someip_protocol::internal::SomeIpMessageHeader const& header,
      ::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet)
      : ::amsr::socal::internal::PendingRequest{skeleton},
        skeleton_{skeleton},
        response_handler_{skeleton_method},
        header_{header},
        packet_{std::move(packet)} {}

  /*!
   * \brief   Operator gets called when method invocation is planned in the frontend.
   * \details It shall be called only once for each instance.
   * \pre -
   * \context     Callback
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   *
   */
  void operator()() override {

    ara::core::Result<::gwm::pt_batterypacksys::bmsstsrv_si::internal::fields::BMSRmtPreHeatSt::Output> result{skeleton_->BMSRmtPreHeatSt.Get().GetResult()};
    if (result.HasValue()) {
      response_handler_.SerializeAndSendMethodResponse<Serializer>(header_, result.Value());
    } else {
      response_handler_.SerializeAndSendApplicationErrorMethodResponse(header_, result.Error());
    }
  }

 private:
  /*!
   * \brief Serialization class of the method response.
   */
  using Serializer = bmsrmtpreheatstget::SkeletonConfiguration::MethodResponseSerializer;


  /*!
   * \brief Pointer to the skeleton to invoke the method request call.
   */
  ::gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton* skeleton_;

  /*!
   * \brief Binding-related handler to serialize and send a response right after the method call.
   */
  ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonResponseHandler<SkeletonBMSRmtPreHeatStGet> response_handler_;

  /*!
   * \brief Buffer the SOME/IP header from the request for the response with the corresponding client ID and session ID.
   */
  ::amsr::someip_protocol::internal::SomeIpMessageHeader const header_;

  /*!
   * \brief Serialized Method Request [SOME/IP Header + Payload].
   */
  ::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet_;
};


}  // namespace fields


namespace fields {

/*!
 * \brief Forward-declaration for back-reference
 */
class SkeletonBMSSafCnctStGetAsyncRequest;

namespace bmssafcnctstget {

// VECTOR NC AutosarC++17_10-M7.3.6, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.6_using_directive
// VECTOR NC AutosarC++17_10-M7.3.4, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.4_using_directive
using namespace vac::container::literals;  // NOLINT(build/namespaces)

/*!
 * \brief Struct combining all required types/values for creating the method manager.
 *
 * \remark generated
 */
struct SkeletonConfiguration final {

/*!
 * \brief Method name string.
 */
static constexpr vac::container::CStringView kName{"BMSSafCnctStGet"_sv};

/*!
 * \brief Method Id.
 */
static constexpr ::amsr::someip_protocol::internal::MethodId kMethodId{0x4007U};

/*!
 * \brief Skeleton SomeIp Binding.
 */
using SkeletonSomeIpBinding = SI_BmsStSrvSkeletonSomeIpBinding;

/*!
 * \brief Method Input struct.
 */
using Input = ::gwm::pt_batterypacksys::bmsstsrv_si::internal::fields::BMSSafCnctStGet::Input;

/*!
 * \brief Async request type.
 */
using AsyncRequest = SkeletonBMSSafCnctStGetAsyncRequest;

/*!
 * \brief Method Request deserializer.
 */
using MethodRequestDeserializer = ::amsr::someip_protocol::internal::method_deserializer::gwm_pt_batterypacksys_bmsstsrv_si::fields::DeserializerRequestBMSSafCnctStGet;

/*!
 * \brief Method response serializer.
 */
using MethodResponseSerializer = ::amsr::someip_protocol::internal::method_serializer::gwm_pt_batterypacksys_bmsstsrv_si::fields::SerializerResponseOkBMSSafCnctStGet;
};

/*!
 * \brief Method manager configuration class.
 *
 * \remark generated
 */
using SkeletonBMSSafCnctStGetConfiguration = ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonMethodXfConfiguration<SkeletonConfiguration::kMethodId, SkeletonConfiguration::SkeletonSomeIpBinding, SkeletonConfiguration::Input, SkeletonConfiguration::AsyncRequest, SkeletonConfiguration::MethodRequestDeserializer>;

} // namespace bmssafcnctstget

/*!
 * \brief SOME/IP Skeleton method class for method 'BMSSafCnctStGet'.
 * \details Handles SOME/IP de-/serialization.
 * \remark generated
 */
using SkeletonBMSSafCnctStGet = ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonMethodXf<bmssafcnctstget::SkeletonBMSSafCnctStGetConfiguration>;

/*!
 * \brief For each method request a task of this type is assembled and is passed to the skeleton's frontend,
 *        where it is being executed asynchronously according to the chosen processing mode.
 */
class SkeletonBMSSafCnctStGetAsyncRequest : public ::amsr::socal::internal::PendingRequest {
 public:
  /*!
   * \brief At initialization store all relevant information, to be able to invoke the method call
   *        and route the response to the binding-related part again.
   * \param[in] skeleton A pointer to the skeleton frontend for calling the concrete method asynchronously in a separate
   *                     worker thread.
   * \param[in] skeleton_method A pointer to the skeleton method class needed to send the response after processing the
   *                            method request.
   * \param[in] header The SOME/IP header which must be stored for a possible response
   *                   (in case this method is not fire & forget).
   * \param[in] packet Serialized Method Request [SOME/IP Header + Payload].
   * \pre -
   * \context     Reactor
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  SkeletonBMSSafCnctStGetAsyncRequest(::gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton* skeleton,
      SkeletonBMSSafCnctStGet& skeleton_method,
      ::amsr::someip_protocol::internal::SomeIpMessageHeader const& header,
      ::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet)
      : ::amsr::socal::internal::PendingRequest{skeleton},
        skeleton_{skeleton},
        response_handler_{skeleton_method},
        header_{header},
        packet_{std::move(packet)} {}

  /*!
   * \brief   Operator gets called when method invocation is planned in the frontend.
   * \details It shall be called only once for each instance.
   * \pre -
   * \context     Callback
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   *
   */
  void operator()() override {

    ara::core::Result<::gwm::pt_batterypacksys::bmsstsrv_si::internal::fields::BMSSafCnctSt::Output> result{skeleton_->BMSSafCnctSt.Get().GetResult()};
    if (result.HasValue()) {
      response_handler_.SerializeAndSendMethodResponse<Serializer>(header_, result.Value());
    } else {
      response_handler_.SerializeAndSendApplicationErrorMethodResponse(header_, result.Error());
    }
  }

 private:
  /*!
   * \brief Serialization class of the method response.
   */
  using Serializer = bmssafcnctstget::SkeletonConfiguration::MethodResponseSerializer;


  /*!
   * \brief Pointer to the skeleton to invoke the method request call.
   */
  ::gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton* skeleton_;

  /*!
   * \brief Binding-related handler to serialize and send a response right after the method call.
   */
  ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonResponseHandler<SkeletonBMSSafCnctStGet> response_handler_;

  /*!
   * \brief Buffer the SOME/IP header from the request for the response with the corresponding client ID and session ID.
   */
  ::amsr::someip_protocol::internal::SomeIpMessageHeader const header_;

  /*!
   * \brief Serialized Method Request [SOME/IP Header + Payload].
   */
  ::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet_;
};


}  // namespace fields


namespace fields {

/*!
 * \brief Forward-declaration for back-reference
 */
class SkeletonBmsErrStGetAsyncRequest;

namespace bmserrstget {

// VECTOR NC AutosarC++17_10-M7.3.6, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.6_using_directive
// VECTOR NC AutosarC++17_10-M7.3.4, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.4_using_directive
using namespace vac::container::literals;  // NOLINT(build/namespaces)

/*!
 * \brief Struct combining all required types/values for creating the method manager.
 *
 * \remark generated
 */
struct SkeletonConfiguration final {

/*!
 * \brief Method name string.
 */
static constexpr vac::container::CStringView kName{"BmsErrStGet"_sv};

/*!
 * \brief Method Id.
 */
static constexpr ::amsr::someip_protocol::internal::MethodId kMethodId{0x4003U};

/*!
 * \brief Skeleton SomeIp Binding.
 */
using SkeletonSomeIpBinding = SI_BmsStSrvSkeletonSomeIpBinding;

/*!
 * \brief Method Input struct.
 */
using Input = ::gwm::pt_batterypacksys::bmsstsrv_si::internal::fields::BmsErrStGet::Input;

/*!
 * \brief Async request type.
 */
using AsyncRequest = SkeletonBmsErrStGetAsyncRequest;

/*!
 * \brief Method Request deserializer.
 */
using MethodRequestDeserializer = ::amsr::someip_protocol::internal::method_deserializer::gwm_pt_batterypacksys_bmsstsrv_si::fields::DeserializerRequestBmsErrStGet;

/*!
 * \brief Method response serializer.
 */
using MethodResponseSerializer = ::amsr::someip_protocol::internal::method_serializer::gwm_pt_batterypacksys_bmsstsrv_si::fields::SerializerResponseOkBmsErrStGet;
};

/*!
 * \brief Method manager configuration class.
 *
 * \remark generated
 */
using SkeletonBmsErrStGetConfiguration = ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonMethodXfConfiguration<SkeletonConfiguration::kMethodId, SkeletonConfiguration::SkeletonSomeIpBinding, SkeletonConfiguration::Input, SkeletonConfiguration::AsyncRequest, SkeletonConfiguration::MethodRequestDeserializer>;

} // namespace bmserrstget

/*!
 * \brief SOME/IP Skeleton method class for method 'BmsErrStGet'.
 * \details Handles SOME/IP de-/serialization.
 * \remark generated
 */
using SkeletonBmsErrStGet = ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonMethodXf<bmserrstget::SkeletonBmsErrStGetConfiguration>;

/*!
 * \brief For each method request a task of this type is assembled and is passed to the skeleton's frontend,
 *        where it is being executed asynchronously according to the chosen processing mode.
 */
class SkeletonBmsErrStGetAsyncRequest : public ::amsr::socal::internal::PendingRequest {
 public:
  /*!
   * \brief At initialization store all relevant information, to be able to invoke the method call
   *        and route the response to the binding-related part again.
   * \param[in] skeleton A pointer to the skeleton frontend for calling the concrete method asynchronously in a separate
   *                     worker thread.
   * \param[in] skeleton_method A pointer to the skeleton method class needed to send the response after processing the
   *                            method request.
   * \param[in] header The SOME/IP header which must be stored for a possible response
   *                   (in case this method is not fire & forget).
   * \param[in] packet Serialized Method Request [SOME/IP Header + Payload].
   * \pre -
   * \context     Reactor
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  SkeletonBmsErrStGetAsyncRequest(::gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton* skeleton,
      SkeletonBmsErrStGet& skeleton_method,
      ::amsr::someip_protocol::internal::SomeIpMessageHeader const& header,
      ::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet)
      : ::amsr::socal::internal::PendingRequest{skeleton},
        skeleton_{skeleton},
        response_handler_{skeleton_method},
        header_{header},
        packet_{std::move(packet)} {}

  /*!
   * \brief   Operator gets called when method invocation is planned in the frontend.
   * \details It shall be called only once for each instance.
   * \pre -
   * \context     Callback
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   *
   */
  void operator()() override {

    ara::core::Result<::gwm::pt_batterypacksys::bmsstsrv_si::internal::fields::BmsErrSt::Output> result{skeleton_->BmsErrSt.Get().GetResult()};
    if (result.HasValue()) {
      response_handler_.SerializeAndSendMethodResponse<Serializer>(header_, result.Value());
    } else {
      response_handler_.SerializeAndSendApplicationErrorMethodResponse(header_, result.Error());
    }
  }

 private:
  /*!
   * \brief Serialization class of the method response.
   */
  using Serializer = bmserrstget::SkeletonConfiguration::MethodResponseSerializer;


  /*!
   * \brief Pointer to the skeleton to invoke the method request call.
   */
  ::gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton* skeleton_;

  /*!
   * \brief Binding-related handler to serialize and send a response right after the method call.
   */
  ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonResponseHandler<SkeletonBmsErrStGet> response_handler_;

  /*!
   * \brief Buffer the SOME/IP header from the request for the response with the corresponding client ID and session ID.
   */
  ::amsr::someip_protocol::internal::SomeIpMessageHeader const header_;

  /*!
   * \brief Serialized Method Request [SOME/IP Header + Payload].
   */
  ::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet_;
};


}  // namespace fields


namespace fields {

/*!
 * \brief Forward-declaration for back-reference
 */
class SkeletonBmsHVILStGetAsyncRequest;

namespace bmshvilstget {

// VECTOR NC AutosarC++17_10-M7.3.6, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.6_using_directive
// VECTOR NC AutosarC++17_10-M7.3.4, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.4_using_directive
using namespace vac::container::literals;  // NOLINT(build/namespaces)

/*!
 * \brief Struct combining all required types/values for creating the method manager.
 *
 * \remark generated
 */
struct SkeletonConfiguration final {

/*!
 * \brief Method name string.
 */
static constexpr vac::container::CStringView kName{"BmsHVILStGet"_sv};

/*!
 * \brief Method Id.
 */
static constexpr ::amsr::someip_protocol::internal::MethodId kMethodId{0x4005U};

/*!
 * \brief Skeleton SomeIp Binding.
 */
using SkeletonSomeIpBinding = SI_BmsStSrvSkeletonSomeIpBinding;

/*!
 * \brief Method Input struct.
 */
using Input = ::gwm::pt_batterypacksys::bmsstsrv_si::internal::fields::BmsHVILStGet::Input;

/*!
 * \brief Async request type.
 */
using AsyncRequest = SkeletonBmsHVILStGetAsyncRequest;

/*!
 * \brief Method Request deserializer.
 */
using MethodRequestDeserializer = ::amsr::someip_protocol::internal::method_deserializer::gwm_pt_batterypacksys_bmsstsrv_si::fields::DeserializerRequestBmsHVILStGet;

/*!
 * \brief Method response serializer.
 */
using MethodResponseSerializer = ::amsr::someip_protocol::internal::method_serializer::gwm_pt_batterypacksys_bmsstsrv_si::fields::SerializerResponseOkBmsHVILStGet;
};

/*!
 * \brief Method manager configuration class.
 *
 * \remark generated
 */
using SkeletonBmsHVILStGetConfiguration = ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonMethodXfConfiguration<SkeletonConfiguration::kMethodId, SkeletonConfiguration::SkeletonSomeIpBinding, SkeletonConfiguration::Input, SkeletonConfiguration::AsyncRequest, SkeletonConfiguration::MethodRequestDeserializer>;

} // namespace bmshvilstget

/*!
 * \brief SOME/IP Skeleton method class for method 'BmsHVILStGet'.
 * \details Handles SOME/IP de-/serialization.
 * \remark generated
 */
using SkeletonBmsHVILStGet = ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonMethodXf<bmshvilstget::SkeletonBmsHVILStGetConfiguration>;

/*!
 * \brief For each method request a task of this type is assembled and is passed to the skeleton's frontend,
 *        where it is being executed asynchronously according to the chosen processing mode.
 */
class SkeletonBmsHVILStGetAsyncRequest : public ::amsr::socal::internal::PendingRequest {
 public:
  /*!
   * \brief At initialization store all relevant information, to be able to invoke the method call
   *        and route the response to the binding-related part again.
   * \param[in] skeleton A pointer to the skeleton frontend for calling the concrete method asynchronously in a separate
   *                     worker thread.
   * \param[in] skeleton_method A pointer to the skeleton method class needed to send the response after processing the
   *                            method request.
   * \param[in] header The SOME/IP header which must be stored for a possible response
   *                   (in case this method is not fire & forget).
   * \param[in] packet Serialized Method Request [SOME/IP Header + Payload].
   * \pre -
   * \context     Reactor
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  SkeletonBmsHVILStGetAsyncRequest(::gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton* skeleton,
      SkeletonBmsHVILStGet& skeleton_method,
      ::amsr::someip_protocol::internal::SomeIpMessageHeader const& header,
      ::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet)
      : ::amsr::socal::internal::PendingRequest{skeleton},
        skeleton_{skeleton},
        response_handler_{skeleton_method},
        header_{header},
        packet_{std::move(packet)} {}

  /*!
   * \brief   Operator gets called when method invocation is planned in the frontend.
   * \details It shall be called only once for each instance.
   * \pre -
   * \context     Callback
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   *
   */
  void operator()() override {

    ara::core::Result<::gwm::pt_batterypacksys::bmsstsrv_si::internal::fields::BmsHVILSt::Output> result{skeleton_->BmsHVILSt.Get().GetResult()};
    if (result.HasValue()) {
      response_handler_.SerializeAndSendMethodResponse<Serializer>(header_, result.Value());
    } else {
      response_handler_.SerializeAndSendApplicationErrorMethodResponse(header_, result.Error());
    }
  }

 private:
  /*!
   * \brief Serialization class of the method response.
   */
  using Serializer = bmshvilstget::SkeletonConfiguration::MethodResponseSerializer;


  /*!
   * \brief Pointer to the skeleton to invoke the method request call.
   */
  ::gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton* skeleton_;

  /*!
   * \brief Binding-related handler to serialize and send a response right after the method call.
   */
  ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonResponseHandler<SkeletonBmsHVILStGet> response_handler_;

  /*!
   * \brief Buffer the SOME/IP header from the request for the response with the corresponding client ID and session ID.
   */
  ::amsr::someip_protocol::internal::SomeIpMessageHeader const header_;

  /*!
   * \brief Serialized Method Request [SOME/IP Header + Payload].
   */
  ::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet_;
};


}  // namespace fields


namespace fields {

/*!
 * \brief Forward-declaration for back-reference
 */
class SkeletonBmsIsoMeasStGetAsyncRequest;

namespace bmsisomeasstget {

// VECTOR NC AutosarC++17_10-M7.3.6, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.6_using_directive
// VECTOR NC AutosarC++17_10-M7.3.4, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.4_using_directive
using namespace vac::container::literals;  // NOLINT(build/namespaces)

/*!
 * \brief Struct combining all required types/values for creating the method manager.
 *
 * \remark generated
 */
struct SkeletonConfiguration final {

/*!
 * \brief Method name string.
 */
static constexpr vac::container::CStringView kName{"BmsIsoMeasStGet"_sv};

/*!
 * \brief Method Id.
 */
static constexpr ::amsr::someip_protocol::internal::MethodId kMethodId{0x4004U};

/*!
 * \brief Skeleton SomeIp Binding.
 */
using SkeletonSomeIpBinding = SI_BmsStSrvSkeletonSomeIpBinding;

/*!
 * \brief Method Input struct.
 */
using Input = ::gwm::pt_batterypacksys::bmsstsrv_si::internal::fields::BmsIsoMeasStGet::Input;

/*!
 * \brief Async request type.
 */
using AsyncRequest = SkeletonBmsIsoMeasStGetAsyncRequest;

/*!
 * \brief Method Request deserializer.
 */
using MethodRequestDeserializer = ::amsr::someip_protocol::internal::method_deserializer::gwm_pt_batterypacksys_bmsstsrv_si::fields::DeserializerRequestBmsIsoMeasStGet;

/*!
 * \brief Method response serializer.
 */
using MethodResponseSerializer = ::amsr::someip_protocol::internal::method_serializer::gwm_pt_batterypacksys_bmsstsrv_si::fields::SerializerResponseOkBmsIsoMeasStGet;
};

/*!
 * \brief Method manager configuration class.
 *
 * \remark generated
 */
using SkeletonBmsIsoMeasStGetConfiguration = ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonMethodXfConfiguration<SkeletonConfiguration::kMethodId, SkeletonConfiguration::SkeletonSomeIpBinding, SkeletonConfiguration::Input, SkeletonConfiguration::AsyncRequest, SkeletonConfiguration::MethodRequestDeserializer>;

} // namespace bmsisomeasstget

/*!
 * \brief SOME/IP Skeleton method class for method 'BmsIsoMeasStGet'.
 * \details Handles SOME/IP de-/serialization.
 * \remark generated
 */
using SkeletonBmsIsoMeasStGet = ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonMethodXf<bmsisomeasstget::SkeletonBmsIsoMeasStGetConfiguration>;

/*!
 * \brief For each method request a task of this type is assembled and is passed to the skeleton's frontend,
 *        where it is being executed asynchronously according to the chosen processing mode.
 */
class SkeletonBmsIsoMeasStGetAsyncRequest : public ::amsr::socal::internal::PendingRequest {
 public:
  /*!
   * \brief At initialization store all relevant information, to be able to invoke the method call
   *        and route the response to the binding-related part again.
   * \param[in] skeleton A pointer to the skeleton frontend for calling the concrete method asynchronously in a separate
   *                     worker thread.
   * \param[in] skeleton_method A pointer to the skeleton method class needed to send the response after processing the
   *                            method request.
   * \param[in] header The SOME/IP header which must be stored for a possible response
   *                   (in case this method is not fire & forget).
   * \param[in] packet Serialized Method Request [SOME/IP Header + Payload].
   * \pre -
   * \context     Reactor
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  SkeletonBmsIsoMeasStGetAsyncRequest(::gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton* skeleton,
      SkeletonBmsIsoMeasStGet& skeleton_method,
      ::amsr::someip_protocol::internal::SomeIpMessageHeader const& header,
      ::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet)
      : ::amsr::socal::internal::PendingRequest{skeleton},
        skeleton_{skeleton},
        response_handler_{skeleton_method},
        header_{header},
        packet_{std::move(packet)} {}

  /*!
   * \brief   Operator gets called when method invocation is planned in the frontend.
   * \details It shall be called only once for each instance.
   * \pre -
   * \context     Callback
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   *
   */
  void operator()() override {

    ara::core::Result<::gwm::pt_batterypacksys::bmsstsrv_si::internal::fields::BmsIsoMeasSt::Output> result{skeleton_->BmsIsoMeasSt.Get().GetResult()};
    if (result.HasValue()) {
      response_handler_.SerializeAndSendMethodResponse<Serializer>(header_, result.Value());
    } else {
      response_handler_.SerializeAndSendApplicationErrorMethodResponse(header_, result.Error());
    }
  }

 private:
  /*!
   * \brief Serialization class of the method response.
   */
  using Serializer = bmsisomeasstget::SkeletonConfiguration::MethodResponseSerializer;


  /*!
   * \brief Pointer to the skeleton to invoke the method request call.
   */
  ::gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton* skeleton_;

  /*!
   * \brief Binding-related handler to serialize and send a response right after the method call.
   */
  ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonResponseHandler<SkeletonBmsIsoMeasStGet> response_handler_;

  /*!
   * \brief Buffer the SOME/IP header from the request for the response with the corresponding client ID and session ID.
   */
  ::amsr::someip_protocol::internal::SomeIpMessageHeader const header_;

  /*!
   * \brief Serialized Method Request [SOME/IP Header + Payload].
   */
  ::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet_;
};


}  // namespace fields


namespace fields {

/*!
 * \brief Forward-declaration for back-reference
 */
class SkeletonBmsStGetAsyncRequest;

namespace bmsstget {

// VECTOR NC AutosarC++17_10-M7.3.6, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.6_using_directive
// VECTOR NC AutosarC++17_10-M7.3.4, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.4_using_directive
using namespace vac::container::literals;  // NOLINT(build/namespaces)

/*!
 * \brief Struct combining all required types/values for creating the method manager.
 *
 * \remark generated
 */
struct SkeletonConfiguration final {

/*!
 * \brief Method name string.
 */
static constexpr vac::container::CStringView kName{"BmsStGet"_sv};

/*!
 * \brief Method Id.
 */
static constexpr ::amsr::someip_protocol::internal::MethodId kMethodId{0x4002U};

/*!
 * \brief Skeleton SomeIp Binding.
 */
using SkeletonSomeIpBinding = SI_BmsStSrvSkeletonSomeIpBinding;

/*!
 * \brief Method Input struct.
 */
using Input = ::gwm::pt_batterypacksys::bmsstsrv_si::internal::fields::BmsStGet::Input;

/*!
 * \brief Async request type.
 */
using AsyncRequest = SkeletonBmsStGetAsyncRequest;

/*!
 * \brief Method Request deserializer.
 */
using MethodRequestDeserializer = ::amsr::someip_protocol::internal::method_deserializer::gwm_pt_batterypacksys_bmsstsrv_si::fields::DeserializerRequestBmsStGet;

/*!
 * \brief Method response serializer.
 */
using MethodResponseSerializer = ::amsr::someip_protocol::internal::method_serializer::gwm_pt_batterypacksys_bmsstsrv_si::fields::SerializerResponseOkBmsStGet;
};

/*!
 * \brief Method manager configuration class.
 *
 * \remark generated
 */
using SkeletonBmsStGetConfiguration = ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonMethodXfConfiguration<SkeletonConfiguration::kMethodId, SkeletonConfiguration::SkeletonSomeIpBinding, SkeletonConfiguration::Input, SkeletonConfiguration::AsyncRequest, SkeletonConfiguration::MethodRequestDeserializer>;

} // namespace bmsstget

/*!
 * \brief SOME/IP Skeleton method class for method 'BmsStGet'.
 * \details Handles SOME/IP de-/serialization.
 * \remark generated
 */
using SkeletonBmsStGet = ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonMethodXf<bmsstget::SkeletonBmsStGetConfiguration>;

/*!
 * \brief For each method request a task of this type is assembled and is passed to the skeleton's frontend,
 *        where it is being executed asynchronously according to the chosen processing mode.
 */
class SkeletonBmsStGetAsyncRequest : public ::amsr::socal::internal::PendingRequest {
 public:
  /*!
   * \brief At initialization store all relevant information, to be able to invoke the method call
   *        and route the response to the binding-related part again.
   * \param[in] skeleton A pointer to the skeleton frontend for calling the concrete method asynchronously in a separate
   *                     worker thread.
   * \param[in] skeleton_method A pointer to the skeleton method class needed to send the response after processing the
   *                            method request.
   * \param[in] header The SOME/IP header which must be stored for a possible response
   *                   (in case this method is not fire & forget).
   * \param[in] packet Serialized Method Request [SOME/IP Header + Payload].
   * \pre -
   * \context     Reactor
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  SkeletonBmsStGetAsyncRequest(::gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton* skeleton,
      SkeletonBmsStGet& skeleton_method,
      ::amsr::someip_protocol::internal::SomeIpMessageHeader const& header,
      ::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet)
      : ::amsr::socal::internal::PendingRequest{skeleton},
        skeleton_{skeleton},
        response_handler_{skeleton_method},
        header_{header},
        packet_{std::move(packet)} {}

  /*!
   * \brief   Operator gets called when method invocation is planned in the frontend.
   * \details It shall be called only once for each instance.
   * \pre -
   * \context     Callback
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   *
   */
  void operator()() override {

    ara::core::Result<::gwm::pt_batterypacksys::bmsstsrv_si::internal::fields::BmsSt::Output> result{skeleton_->BmsSt.Get().GetResult()};
    if (result.HasValue()) {
      response_handler_.SerializeAndSendMethodResponse<Serializer>(header_, result.Value());
    } else {
      response_handler_.SerializeAndSendApplicationErrorMethodResponse(header_, result.Error());
    }
  }

 private:
  /*!
   * \brief Serialization class of the method response.
   */
  using Serializer = bmsstget::SkeletonConfiguration::MethodResponseSerializer;


  /*!
   * \brief Pointer to the skeleton to invoke the method request call.
   */
  ::gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton* skeleton_;

  /*!
   * \brief Binding-related handler to serialize and send a response right after the method call.
   */
  ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonResponseHandler<SkeletonBmsStGet> response_handler_;

  /*!
   * \brief Buffer the SOME/IP header from the request for the response with the corresponding client ID and session ID.
   */
  ::amsr::someip_protocol::internal::SomeIpMessageHeader const header_;

  /*!
   * \brief Serialized Method Request [SOME/IP Header + Payload].
   */
  ::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet_;
};


}  // namespace fields


namespace fields {

/*!
 * \brief Forward-declaration for back-reference
 */
class SkeletonHVBattSOCLimGetAsyncRequest;

namespace hvbattsoclimget {

// VECTOR NC AutosarC++17_10-M7.3.6, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.6_using_directive
// VECTOR NC AutosarC++17_10-M7.3.4, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.4_using_directive
using namespace vac::container::literals;  // NOLINT(build/namespaces)

/*!
 * \brief Struct combining all required types/values for creating the method manager.
 *
 * \remark generated
 */
struct SkeletonConfiguration final {

/*!
 * \brief Method name string.
 */
static constexpr vac::container::CStringView kName{"HVBattSOCLimGet"_sv};

/*!
 * \brief Method Id.
 */
static constexpr ::amsr::someip_protocol::internal::MethodId kMethodId{0x4010U};

/*!
 * \brief Skeleton SomeIp Binding.
 */
using SkeletonSomeIpBinding = SI_BmsStSrvSkeletonSomeIpBinding;

/*!
 * \brief Method Input struct.
 */
using Input = ::gwm::pt_batterypacksys::bmsstsrv_si::internal::fields::HVBattSOCLimGet::Input;

/*!
 * \brief Async request type.
 */
using AsyncRequest = SkeletonHVBattSOCLimGetAsyncRequest;

/*!
 * \brief Method Request deserializer.
 */
using MethodRequestDeserializer = ::amsr::someip_protocol::internal::method_deserializer::gwm_pt_batterypacksys_bmsstsrv_si::fields::DeserializerRequestHVBattSOCLimGet;

/*!
 * \brief Method response serializer.
 */
using MethodResponseSerializer = ::amsr::someip_protocol::internal::method_serializer::gwm_pt_batterypacksys_bmsstsrv_si::fields::SerializerResponseOkHVBattSOCLimGet;
};

/*!
 * \brief Method manager configuration class.
 *
 * \remark generated
 */
using SkeletonHVBattSOCLimGetConfiguration = ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonMethodXfConfiguration<SkeletonConfiguration::kMethodId, SkeletonConfiguration::SkeletonSomeIpBinding, SkeletonConfiguration::Input, SkeletonConfiguration::AsyncRequest, SkeletonConfiguration::MethodRequestDeserializer>;

} // namespace hvbattsoclimget

/*!
 * \brief SOME/IP Skeleton method class for method 'HVBattSOCLimGet'.
 * \details Handles SOME/IP de-/serialization.
 * \remark generated
 */
using SkeletonHVBattSOCLimGet = ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonMethodXf<hvbattsoclimget::SkeletonHVBattSOCLimGetConfiguration>;

/*!
 * \brief For each method request a task of this type is assembled and is passed to the skeleton's frontend,
 *        where it is being executed asynchronously according to the chosen processing mode.
 */
class SkeletonHVBattSOCLimGetAsyncRequest : public ::amsr::socal::internal::PendingRequest {
 public:
  /*!
   * \brief At initialization store all relevant information, to be able to invoke the method call
   *        and route the response to the binding-related part again.
   * \param[in] skeleton A pointer to the skeleton frontend for calling the concrete method asynchronously in a separate
   *                     worker thread.
   * \param[in] skeleton_method A pointer to the skeleton method class needed to send the response after processing the
   *                            method request.
   * \param[in] header The SOME/IP header which must be stored for a possible response
   *                   (in case this method is not fire & forget).
   * \param[in] packet Serialized Method Request [SOME/IP Header + Payload].
   * \pre -
   * \context     Reactor
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  SkeletonHVBattSOCLimGetAsyncRequest(::gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton* skeleton,
      SkeletonHVBattSOCLimGet& skeleton_method,
      ::amsr::someip_protocol::internal::SomeIpMessageHeader const& header,
      ::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet)
      : ::amsr::socal::internal::PendingRequest{skeleton},
        skeleton_{skeleton},
        response_handler_{skeleton_method},
        header_{header},
        packet_{std::move(packet)} {}

  /*!
   * \brief   Operator gets called when method invocation is planned in the frontend.
   * \details It shall be called only once for each instance.
   * \pre -
   * \context     Callback
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   *
   */
  void operator()() override {

    ara::core::Result<::gwm::pt_batterypacksys::bmsstsrv_si::internal::fields::HVBattSOCLim::Output> result{skeleton_->HVBattSOCLim.Get().GetResult()};
    if (result.HasValue()) {
      response_handler_.SerializeAndSendMethodResponse<Serializer>(header_, result.Value());
    } else {
      response_handler_.SerializeAndSendApplicationErrorMethodResponse(header_, result.Error());
    }
  }

 private:
  /*!
   * \brief Serialization class of the method response.
   */
  using Serializer = hvbattsoclimget::SkeletonConfiguration::MethodResponseSerializer;


  /*!
   * \brief Pointer to the skeleton to invoke the method request call.
   */
  ::gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton* skeleton_;

  /*!
   * \brief Binding-related handler to serialize and send a response right after the method call.
   */
  ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonResponseHandler<SkeletonHVBattSOCLimGet> response_handler_;

  /*!
   * \brief Buffer the SOME/IP header from the request for the response with the corresponding client ID and session ID.
   */
  ::amsr::someip_protocol::internal::SomeIpMessageHeader const header_;

  /*!
   * \brief Serialized Method Request [SOME/IP Header + Payload].
   */
  ::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet_;
};


}  // namespace fields


namespace fields {

/*!
 * \brief Forward-declaration for back-reference
 */
class SkeletonHVMaiSwtStGetAsyncRequest;

namespace hvmaiswtstget {

// VECTOR NC AutosarC++17_10-M7.3.6, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.6_using_directive
// VECTOR NC AutosarC++17_10-M7.3.4, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.4_using_directive
using namespace vac::container::literals;  // NOLINT(build/namespaces)

/*!
 * \brief Struct combining all required types/values for creating the method manager.
 *
 * \remark generated
 */
struct SkeletonConfiguration final {

/*!
 * \brief Method name string.
 */
static constexpr vac::container::CStringView kName{"HVMaiSwtStGet"_sv};

/*!
 * \brief Method Id.
 */
static constexpr ::amsr::someip_protocol::internal::MethodId kMethodId{0x4001U};

/*!
 * \brief Skeleton SomeIp Binding.
 */
using SkeletonSomeIpBinding = SI_BmsStSrvSkeletonSomeIpBinding;

/*!
 * \brief Method Input struct.
 */
using Input = ::gwm::pt_batterypacksys::bmsstsrv_si::internal::fields::HVMaiSwtStGet::Input;

/*!
 * \brief Async request type.
 */
using AsyncRequest = SkeletonHVMaiSwtStGetAsyncRequest;

/*!
 * \brief Method Request deserializer.
 */
using MethodRequestDeserializer = ::amsr::someip_protocol::internal::method_deserializer::gwm_pt_batterypacksys_bmsstsrv_si::fields::DeserializerRequestHVMaiSwtStGet;

/*!
 * \brief Method response serializer.
 */
using MethodResponseSerializer = ::amsr::someip_protocol::internal::method_serializer::gwm_pt_batterypacksys_bmsstsrv_si::fields::SerializerResponseOkHVMaiSwtStGet;
};

/*!
 * \brief Method manager configuration class.
 *
 * \remark generated
 */
using SkeletonHVMaiSwtStGetConfiguration = ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonMethodXfConfiguration<SkeletonConfiguration::kMethodId, SkeletonConfiguration::SkeletonSomeIpBinding, SkeletonConfiguration::Input, SkeletonConfiguration::AsyncRequest, SkeletonConfiguration::MethodRequestDeserializer>;

} // namespace hvmaiswtstget

/*!
 * \brief SOME/IP Skeleton method class for method 'HVMaiSwtStGet'.
 * \details Handles SOME/IP de-/serialization.
 * \remark generated
 */
using SkeletonHVMaiSwtStGet = ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonMethodXf<hvmaiswtstget::SkeletonHVMaiSwtStGetConfiguration>;

/*!
 * \brief For each method request a task of this type is assembled and is passed to the skeleton's frontend,
 *        where it is being executed asynchronously according to the chosen processing mode.
 */
class SkeletonHVMaiSwtStGetAsyncRequest : public ::amsr::socal::internal::PendingRequest {
 public:
  /*!
   * \brief At initialization store all relevant information, to be able to invoke the method call
   *        and route the response to the binding-related part again.
   * \param[in] skeleton A pointer to the skeleton frontend for calling the concrete method asynchronously in a separate
   *                     worker thread.
   * \param[in] skeleton_method A pointer to the skeleton method class needed to send the response after processing the
   *                            method request.
   * \param[in] header The SOME/IP header which must be stored for a possible response
   *                   (in case this method is not fire & forget).
   * \param[in] packet Serialized Method Request [SOME/IP Header + Payload].
   * \pre -
   * \context     Reactor
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  SkeletonHVMaiSwtStGetAsyncRequest(::gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton* skeleton,
      SkeletonHVMaiSwtStGet& skeleton_method,
      ::amsr::someip_protocol::internal::SomeIpMessageHeader const& header,
      ::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet)
      : ::amsr::socal::internal::PendingRequest{skeleton},
        skeleton_{skeleton},
        response_handler_{skeleton_method},
        header_{header},
        packet_{std::move(packet)} {}

  /*!
   * \brief   Operator gets called when method invocation is planned in the frontend.
   * \details It shall be called only once for each instance.
   * \pre -
   * \context     Callback
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   *
   */
  void operator()() override {

    ara::core::Result<::gwm::pt_batterypacksys::bmsstsrv_si::internal::fields::HVMaiSwtSt::Output> result{skeleton_->HVMaiSwtSt.Get().GetResult()};
    if (result.HasValue()) {
      response_handler_.SerializeAndSendMethodResponse<Serializer>(header_, result.Value());
    } else {
      response_handler_.SerializeAndSendApplicationErrorMethodResponse(header_, result.Error());
    }
  }

 private:
  /*!
   * \brief Serialization class of the method response.
   */
  using Serializer = hvmaiswtstget::SkeletonConfiguration::MethodResponseSerializer;


  /*!
   * \brief Pointer to the skeleton to invoke the method request call.
   */
  ::gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton* skeleton_;

  /*!
   * \brief Binding-related handler to serialize and send a response right after the method call.
   */
  ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonResponseHandler<SkeletonHVMaiSwtStGet> response_handler_;

  /*!
   * \brief Buffer the SOME/IP header from the request for the response with the corresponding client ID and session ID.
   */
  ::amsr::someip_protocol::internal::SomeIpMessageHeader const header_;

  /*!
   * \brief Serialized Method Request [SOME/IP Header + Payload].
   */
  ::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet_;
};


}  // namespace fields


}  // namespace internal
}  // namespace someip_binding_transformation_layer
}  // namespace amsr
}  // namespace bmsstsrv_si
}  // namespace pt_batterypacksys
}  // namespace gwm

#endif  // DENPENDENCYSRVEXE_INCLUDE_AMSR_SOMEIP_BINDING_TRANSFORMATION_LAYER_INTERNAL_GWM_PT_BATTERYPACKSYS_BMSSTSRV_SI_SI_BMSSTSRV_SKELETON_SOMEIP_METHODS_H_

