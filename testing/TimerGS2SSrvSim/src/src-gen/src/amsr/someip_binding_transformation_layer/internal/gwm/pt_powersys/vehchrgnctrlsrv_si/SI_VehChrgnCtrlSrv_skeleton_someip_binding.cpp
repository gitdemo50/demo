// VECTOR Same Line AutosarC++17_10-A1.1.1: MD_SOMEIPBINDING_AutosarC++17_10-A1.1.1_external_identifiers
/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/src/amsr/someip_binding_transformation_layer/internal/gwm/pt_powersys/vehchrgnctrlsrv_si/SI_VehChrgnCtrlSrv_skeleton_someip_binding.cpp
 *        \brief  SOME/IP skeleton binding of service 'SI_VehChrgnCtrlSrv'
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_someipbinding
 *         Commit ID: 43b33f0283d4d61385b98a3599688499abd86a75
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "amsr/someip_binding_transformation_layer/internal/gwm/pt_powersys/vehchrgnctrlsrv_si/SI_VehChrgnCtrlSrv_skeleton_someip_binding.h"
#include <utility>

namespace amsr {
namespace someip_binding_transformation_layer {
namespace internal {

namespace gwm {
namespace pt_powersys {
namespace vehchrgnctrlsrv_si {

/*!
 * \brief Generated SOME/IP related service ID.
 */
::amsr::someip_protocol::internal::ServiceId constexpr SI_VehChrgnCtrlSrvSkeletonSomeIpBinding::kServiceId;

/*!
 * \brief SOME/IP major version of this service interface deployment.
 */
constexpr ::amsr::someip_protocol::internal::MajorVersion SI_VehChrgnCtrlSrvSkeletonSomeIpBinding::kMajorVersion;

SI_VehChrgnCtrlSrvSkeletonSomeIpBinding::SI_VehChrgnCtrlSrvSkeletonSomeIpBinding(
    ::amsr::someip_protocol::internal::InstanceId const instance_id,
    ::amsr::someip_binding::internal::ServerManagerInterface& someip_binding_server_manager,
    ::gwm::pt_powersys::vehchrgnctrlsrv_si::skeleton::SI_VehChrgnCtrlSrvSkeleton& skeleton)
    : instance_id_(instance_id),
      someip_binding_server_manager_(someip_binding_server_manager),
      skeleton_(skeleton),
      methods_ChargingTypeSet_(*this, "ChargingTypeSet" ),
      methods_VehChrgnCtrl_(*this, "VehChrgnCtrl" ) {
}

void SI_VehChrgnCtrlSrvSkeletonSomeIpBinding::HandleMethodRequest(
  ::amsr::someip_protocol::internal::SomeIpMessageHeader const& header,
  ::amsr::someip_protocol::internal::SomeIpMessage  packet) {
  // Based on the method id -> static dispatching to the method request/response manager
  switch (header.method_id_) {
    case methods::SkeletonChargingTypeSet::kMethodId: {
      methods_ChargingTypeSet_.HandleMethodRequest(header, std::move(packet));
      break;
    }
    case methods::SkeletonVehChrgnCtrl::kMethodId: {
      methods_VehChrgnCtrl_.HandleMethodRequest(header, std::move(packet));
      break;
    }
    default: {
      // Method implementation is missing.
      SendErrorResponse<static_cast<::amsr::someip_protocol::internal::ReturnCode>(::amsr::someip_protocol::internal::SomeIpReturnCode::kUnknownMethod)>(header);
      break;
    }
  }
}

void SI_VehChrgnCtrlSrvSkeletonSomeIpBinding::SendMethodResponse(::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet) {
  someip_binding_server_manager_.SendMethodResponse(instance_id_, std::move(packet));
}

// ---- Events -----------------------------------------------------------------------------------------------------

// ---- Fields -----------------------------------------------------------------------------------------------------

}  // namespace internal
}  // namespace someip_binding_transformation_layer
}  // namespace amsr
}  // namespace vehchrgnctrlsrv_si
}  // namespace pt_powersys
}  // namespace gwm

