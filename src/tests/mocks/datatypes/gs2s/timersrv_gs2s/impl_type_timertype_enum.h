/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  TimerSrvExe/include/datatypes/gs2s/timersrv_gs2s/impl_type_timertype_enum.h
 *        \brief
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_modelleddatatypes_api
 *         Commit ID: d9332226824a4a6c4616e2d948d41c862d192343
 *********************************************************************************************************************/

#ifndef TIMERSRVEXE_INCLUDE_DATATYPES_GS2S_TIMERSRV_GS2S_IMPL_TYPE_TIMERTYPE_ENUM_H_
#define TIMERSRVEXE_INCLUDE_DATATYPES_GS2S_TIMERSRV_GS2S_IMPL_TYPE_TIMERTYPE_ENUM_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include <array>
#include <cstdint>
#include <map>
#include <string>
#include <vector>

namespace datatypes {
namespace gs2s {
namespace timersrv_gs2s {

/*!
 * \brief Type TimerType_Enum.
 * \remark generated
 * \trace SPEC-5951372
 *
 * IMPLEMENTATION-DATA-TYPE /S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/TimerType_Enum
 */
enum class TimerType_Enum : std::uint8_t {
  DurationTime = 0,
  CalendarTime = 1
};

}  // namespace timersrv_gs2s
}  // namespace gs2s
}  // namespace datatypes

#endif  // TIMERSRVEXE_INCLUDE_DATATYPES_GS2S_TIMERSRV_GS2S_IMPL_TYPE_TIMERTYPE_ENUM_H_
