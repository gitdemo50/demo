/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/include/amsr/someip_binding_transformation_layer/internal/gwm/pt_powersys/obcstsrv_si/SI_OBCStSrv_skeleton_someip_event_manager.h
 *        \brief  SOME/IP skeleton event handling for events and field notifications of service 'SI_OBCStSrv'
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_someipbinding
 *         Commit ID: 43b33f0283d4d61385b98a3599688499abd86a75
 *********************************************************************************************************************/

#ifndef DENPENDENCYSRVEXE_INCLUDE_AMSR_SOMEIP_BINDING_TRANSFORMATION_LAYER_INTERNAL_GWM_PT_POWERSYS_OBCSTSRV_SI_SI_OBCSTSRV_SKELETON_SOMEIP_EVENT_MANAGER_H_
#define DENPENDENCYSRVEXE_INCLUDE_AMSR_SOMEIP_BINDING_TRANSFORMATION_LAYER_INTERNAL_GWM_PT_POWERSYS_OBCSTSRV_SI_SI_OBCSTSRV_SKELETON_SOMEIP_EVENT_MANAGER_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "amsr/someip_protocol/internal/dataprototype_serialization/gwm_pt_powersys_obcstsrv_si/events/serializer_OBCActT.h"
#include "amsr/someip_protocol/internal/dataprototype_serialization/gwm_pt_powersys_obcstsrv_si/events/serializer_OBCChrgLim.h"
#include "amsr/someip_protocol/internal/dataprototype_serialization/gwm_pt_powersys_obcstsrv_si/events/serializer_OBCDchrOutpI.h"
#include "amsr/someip_protocol/internal/dataprototype_serialization/gwm_pt_powersys_obcstsrv_si/events/serializer_OBCDchrOutpU.h"
#include "amsr/someip_protocol/internal/dataprototype_serialization/gwm_pt_powersys_obcstsrv_si/events/serializer_OBCInptAcI.h"
#include "amsr/someip_protocol/internal/dataprototype_serialization/gwm_pt_powersys_obcstsrv_si/events/serializer_OBCInptAcU.h"
#include "amsr/someip_protocol/internal/dataprototype_serialization/gwm_pt_powersys_obcstsrv_si/events/serializer_OBCOuptDcI.h"
#include "amsr/someip_protocol/internal/dataprototype_serialization/gwm_pt_powersys_obcstsrv_si/events/serializer_OBCOuptDcU.h"
#include "amsr/someip_protocol/internal/dataprototype_serialization/gwm_pt_powersys_obcstsrv_si/fields/serializer_OBCChrgGunSt.h"
#include "amsr/someip_protocol/internal/dataprototype_serialization/gwm_pt_powersys_obcstsrv_si/fields/serializer_OBCErrSt.h"
#include "amsr/someip_protocol/internal/dataprototype_serialization/gwm_pt_powersys_obcstsrv_si/fields/serializer_OBCHVILSt.h"
#include "amsr/someip_protocol/internal/dataprototype_serialization/gwm_pt_powersys_obcstsrv_si/fields/serializer_OBCSt.h"
#include "amsr/someip_protocol/internal/dataprototype_serialization/gwm_pt_powersys_obcstsrv_si/fields/serializer_OBCV2XSt.h"
#include "gwm/pt_powersys/obcstsrv_si/SI_OBCStSrv_types.h"
#include "someip_binding/internal/e2e/e2e_wrapper.h"
#include "someip_binding/internal/inactive_session_handler.h"
#include "someip_binding/internal/session_handler.h"
#include "someip_binding_transformation_layer/internal/skeleton_event_xf.h"

namespace amsr {
namespace someip_binding_transformation_layer {
namespace internal {

// VECTOR NC AutosarC++17_10-M7.3.6, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.6_using_directive
// VECTOR NC AutosarC++17_10-M7.3.4, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.4_using_directive
using namespace vac::container::literals;  // NOLINT(build/namespaces)

namespace gwm {
namespace pt_powersys {
namespace obcstsrv_si {


// ---- Event 'OBCActT' -------------------------------------------

/*!
 * \brief Skeleton-side configuration struct for event 'OBCActT'.
 */
struct SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCActT {
  /*!
   * \brief SOME/IP service ID.
   */
  static constexpr ::amsr::someip_protocol::internal::ServiceId kServiceId{0x4004};

  /*!
   * \brief SOME/IP major version.
   */
  static constexpr ::amsr::someip_protocol::internal::InterfaceVersion kMajorVersion{1};

  /*!
   * \brief SOME/IP event ID.
   */
  static constexpr ::amsr::someip_protocol::internal::EventId kEventId{0x8001};
  
  /*!
   * \brief Deserializer type for this event
   */
  static constexpr  ::amsr::someipd_app_protocol::internal::MessageType kMessageType{::amsr::someipd_app_protocol::internal::MessageType::kSomeIp};

  /*!
   * \brief Datatype of the event sample.
   * \details AsrPath: /DataTypes/ImplementationDataTypes/OBCActT_Float_Idt
   */
  using SampleType = ::gwm::pt_powersys::powersys_idt::OBCActT_Float_Idt;

  /*!
   * \brief Serializer for event payload.
   */
  using PayloadSerializer = ::amsr::someip_protocol::internal::dataprototype_serializer::gwm_pt_powersys_obcstsrv_si::events::SerializerOBCActT;

  /*!
   * \brief Used session handler.
   * \details Session handling is enabled for the event.
   * \trace CREQ-SomeIpBinding-SessionHandling
   */
  using SessionHandler = ::amsr::someip_binding::internal::SessionHandler;

  /*!
   * \brief E2E profile configuration.
   * \details No E2E protection configured for the event.
   */
  using E2eProfileConfig = void;
};

/*!
 * \brief SOME/IP Skeleton event manager type for event 'OBCActT'.
 */
using SI_OBCStSrvSkeletonSomeIpEventManagerOBCActT =
    ::amsr::someip_binding_transformation_layer::internal::SkeletonEventXf<SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCActT>;


// ---- Event 'OBCChrgLim' -------------------------------------------

/*!
 * \brief Skeleton-side configuration struct for event 'OBCChrgLim'.
 */
struct SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCChrgLim {
  /*!
   * \brief SOME/IP service ID.
   */
  static constexpr ::amsr::someip_protocol::internal::ServiceId kServiceId{0x4004};

  /*!
   * \brief SOME/IP major version.
   */
  static constexpr ::amsr::someip_protocol::internal::InterfaceVersion kMajorVersion{1};

  /*!
   * \brief SOME/IP event ID.
   */
  static constexpr ::amsr::someip_protocol::internal::EventId kEventId{0x8008};
  
  /*!
   * \brief Deserializer type for this event
   */
  static constexpr  ::amsr::someipd_app_protocol::internal::MessageType kMessageType{::amsr::someipd_app_protocol::internal::MessageType::kSomeIp};

  /*!
   * \brief Datatype of the event sample.
   * \details AsrPath: /DataTypes/ImplementationDataTypes/OBCChrgLim_Struct_Idt
   */
  using SampleType = ::gwm::pt_powersys::powersys_idt::OBCChrgLim_Struct_Idt;

  /*!
   * \brief Serializer for event payload.
   */
  using PayloadSerializer = ::amsr::someip_protocol::internal::dataprototype_serializer::gwm_pt_powersys_obcstsrv_si::events::SerializerOBCChrgLim;

  /*!
   * \brief Used session handler.
   * \details Session handling is enabled for the event.
   * \trace CREQ-SomeIpBinding-SessionHandling
   */
  using SessionHandler = ::amsr::someip_binding::internal::SessionHandler;

  /*!
   * \brief E2E profile configuration.
   * \details No E2E protection configured for the event.
   */
  using E2eProfileConfig = void;
};

/*!
 * \brief SOME/IP Skeleton event manager type for event 'OBCChrgLim'.
 */
using SI_OBCStSrvSkeletonSomeIpEventManagerOBCChrgLim =
    ::amsr::someip_binding_transformation_layer::internal::SkeletonEventXf<SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCChrgLim>;


// ---- Event 'OBCDchrOutpI' -------------------------------------------

/*!
 * \brief Skeleton-side configuration struct for event 'OBCDchrOutpI'.
 */
struct SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCDchrOutpI {
  /*!
   * \brief SOME/IP service ID.
   */
  static constexpr ::amsr::someip_protocol::internal::ServiceId kServiceId{0x4004};

  /*!
   * \brief SOME/IP major version.
   */
  static constexpr ::amsr::someip_protocol::internal::InterfaceVersion kMajorVersion{1};

  /*!
   * \brief SOME/IP event ID.
   */
  static constexpr ::amsr::someip_protocol::internal::EventId kEventId{0x8007};
  
  /*!
   * \brief Deserializer type for this event
   */
  static constexpr  ::amsr::someipd_app_protocol::internal::MessageType kMessageType{::amsr::someipd_app_protocol::internal::MessageType::kSomeIp};

  /*!
   * \brief Datatype of the event sample.
   * \details AsrPath: /DataTypes/ImplementationDataTypes/OBCDchrOutpI_Float_Idt
   */
  using SampleType = ::gwm::pt_powersys::powersys_idt::OBCDchrOutpI_Float_Idt;

  /*!
   * \brief Serializer for event payload.
   */
  using PayloadSerializer = ::amsr::someip_protocol::internal::dataprototype_serializer::gwm_pt_powersys_obcstsrv_si::events::SerializerOBCDchrOutpI;

  /*!
   * \brief Used session handler.
   * \details Session handling is enabled for the event.
   * \trace CREQ-SomeIpBinding-SessionHandling
   */
  using SessionHandler = ::amsr::someip_binding::internal::SessionHandler;

  /*!
   * \brief E2E profile configuration.
   * \details No E2E protection configured for the event.
   */
  using E2eProfileConfig = void;
};

/*!
 * \brief SOME/IP Skeleton event manager type for event 'OBCDchrOutpI'.
 */
using SI_OBCStSrvSkeletonSomeIpEventManagerOBCDchrOutpI =
    ::amsr::someip_binding_transformation_layer::internal::SkeletonEventXf<SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCDchrOutpI>;


// ---- Event 'OBCDchrOutpU' -------------------------------------------

/*!
 * \brief Skeleton-side configuration struct for event 'OBCDchrOutpU'.
 */
struct SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCDchrOutpU {
  /*!
   * \brief SOME/IP service ID.
   */
  static constexpr ::amsr::someip_protocol::internal::ServiceId kServiceId{0x4004};

  /*!
   * \brief SOME/IP major version.
   */
  static constexpr ::amsr::someip_protocol::internal::InterfaceVersion kMajorVersion{1};

  /*!
   * \brief SOME/IP event ID.
   */
  static constexpr ::amsr::someip_protocol::internal::EventId kEventId{0x8006};
  
  /*!
   * \brief Deserializer type for this event
   */
  static constexpr  ::amsr::someipd_app_protocol::internal::MessageType kMessageType{::amsr::someipd_app_protocol::internal::MessageType::kSomeIp};

  /*!
   * \brief Datatype of the event sample.
   * \details AsrPath: /DataTypes/ImplementationDataTypes/OBCDchrOutpU_Float_Idt
   */
  using SampleType = ::gwm::pt_powersys::powersys_idt::OBCDchrOutpU_Float_Idt;

  /*!
   * \brief Serializer for event payload.
   */
  using PayloadSerializer = ::amsr::someip_protocol::internal::dataprototype_serializer::gwm_pt_powersys_obcstsrv_si::events::SerializerOBCDchrOutpU;

  /*!
   * \brief Used session handler.
   * \details Session handling is enabled for the event.
   * \trace CREQ-SomeIpBinding-SessionHandling
   */
  using SessionHandler = ::amsr::someip_binding::internal::SessionHandler;

  /*!
   * \brief E2E profile configuration.
   * \details No E2E protection configured for the event.
   */
  using E2eProfileConfig = void;
};

/*!
 * \brief SOME/IP Skeleton event manager type for event 'OBCDchrOutpU'.
 */
using SI_OBCStSrvSkeletonSomeIpEventManagerOBCDchrOutpU =
    ::amsr::someip_binding_transformation_layer::internal::SkeletonEventXf<SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCDchrOutpU>;


// ---- Event 'OBCInptAcI' -------------------------------------------

/*!
 * \brief Skeleton-side configuration struct for event 'OBCInptAcI'.
 */
struct SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCInptAcI {
  /*!
   * \brief SOME/IP service ID.
   */
  static constexpr ::amsr::someip_protocol::internal::ServiceId kServiceId{0x4004};

  /*!
   * \brief SOME/IP major version.
   */
  static constexpr ::amsr::someip_protocol::internal::InterfaceVersion kMajorVersion{1};

  /*!
   * \brief SOME/IP event ID.
   */
  static constexpr ::amsr::someip_protocol::internal::EventId kEventId{0x8002};
  
  /*!
   * \brief Deserializer type for this event
   */
  static constexpr  ::amsr::someipd_app_protocol::internal::MessageType kMessageType{::amsr::someipd_app_protocol::internal::MessageType::kSomeIp};

  /*!
   * \brief Datatype of the event sample.
   * \details AsrPath: /DataTypes/ImplementationDataTypes/OBCInptAcI_Float_Idt
   */
  using SampleType = ::gwm::pt_powersys::powersys_idt::OBCInptAcI_Float_Idt;

  /*!
   * \brief Serializer for event payload.
   */
  using PayloadSerializer = ::amsr::someip_protocol::internal::dataprototype_serializer::gwm_pt_powersys_obcstsrv_si::events::SerializerOBCInptAcI;

  /*!
   * \brief Used session handler.
   * \details Session handling is enabled for the event.
   * \trace CREQ-SomeIpBinding-SessionHandling
   */
  using SessionHandler = ::amsr::someip_binding::internal::SessionHandler;

  /*!
   * \brief E2E profile configuration.
   * \details No E2E protection configured for the event.
   */
  using E2eProfileConfig = void;
};

/*!
 * \brief SOME/IP Skeleton event manager type for event 'OBCInptAcI'.
 */
using SI_OBCStSrvSkeletonSomeIpEventManagerOBCInptAcI =
    ::amsr::someip_binding_transformation_layer::internal::SkeletonEventXf<SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCInptAcI>;


// ---- Event 'OBCInptAcU' -------------------------------------------

/*!
 * \brief Skeleton-side configuration struct for event 'OBCInptAcU'.
 */
struct SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCInptAcU {
  /*!
   * \brief SOME/IP service ID.
   */
  static constexpr ::amsr::someip_protocol::internal::ServiceId kServiceId{0x4004};

  /*!
   * \brief SOME/IP major version.
   */
  static constexpr ::amsr::someip_protocol::internal::InterfaceVersion kMajorVersion{1};

  /*!
   * \brief SOME/IP event ID.
   */
  static constexpr ::amsr::someip_protocol::internal::EventId kEventId{0x8003};
  
  /*!
   * \brief Deserializer type for this event
   */
  static constexpr  ::amsr::someipd_app_protocol::internal::MessageType kMessageType{::amsr::someipd_app_protocol::internal::MessageType::kSomeIp};

  /*!
   * \brief Datatype of the event sample.
   * \details AsrPath: /DataTypes/ImplementationDataTypes/OBCInptAcU_Float_Idt
   */
  using SampleType = ::gwm::pt_powersys::powersys_idt::OBCInptAcU_Float_Idt;

  /*!
   * \brief Serializer for event payload.
   */
  using PayloadSerializer = ::amsr::someip_protocol::internal::dataprototype_serializer::gwm_pt_powersys_obcstsrv_si::events::SerializerOBCInptAcU;

  /*!
   * \brief Used session handler.
   * \details Session handling is enabled for the event.
   * \trace CREQ-SomeIpBinding-SessionHandling
   */
  using SessionHandler = ::amsr::someip_binding::internal::SessionHandler;

  /*!
   * \brief E2E profile configuration.
   * \details No E2E protection configured for the event.
   */
  using E2eProfileConfig = void;
};

/*!
 * \brief SOME/IP Skeleton event manager type for event 'OBCInptAcU'.
 */
using SI_OBCStSrvSkeletonSomeIpEventManagerOBCInptAcU =
    ::amsr::someip_binding_transformation_layer::internal::SkeletonEventXf<SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCInptAcU>;


// ---- Event 'OBCOuptDcI' -------------------------------------------

/*!
 * \brief Skeleton-side configuration struct for event 'OBCOuptDcI'.
 */
struct SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCOuptDcI {
  /*!
   * \brief SOME/IP service ID.
   */
  static constexpr ::amsr::someip_protocol::internal::ServiceId kServiceId{0x4004};

  /*!
   * \brief SOME/IP major version.
   */
  static constexpr ::amsr::someip_protocol::internal::InterfaceVersion kMajorVersion{1};

  /*!
   * \brief SOME/IP event ID.
   */
  static constexpr ::amsr::someip_protocol::internal::EventId kEventId{0x8004};
  
  /*!
   * \brief Deserializer type for this event
   */
  static constexpr  ::amsr::someipd_app_protocol::internal::MessageType kMessageType{::amsr::someipd_app_protocol::internal::MessageType::kSomeIp};

  /*!
   * \brief Datatype of the event sample.
   * \details AsrPath: /DataTypes/ImplementationDataTypes/OBCOuptDcI_Float_Idt
   */
  using SampleType = ::gwm::pt_powersys::powersys_idt::OBCOuptDcI_Float_Idt;

  /*!
   * \brief Serializer for event payload.
   */
  using PayloadSerializer = ::amsr::someip_protocol::internal::dataprototype_serializer::gwm_pt_powersys_obcstsrv_si::events::SerializerOBCOuptDcI;

  /*!
   * \brief Used session handler.
   * \details Session handling is enabled for the event.
   * \trace CREQ-SomeIpBinding-SessionHandling
   */
  using SessionHandler = ::amsr::someip_binding::internal::SessionHandler;

  /*!
   * \brief E2E profile configuration.
   * \details No E2E protection configured for the event.
   */
  using E2eProfileConfig = void;
};

/*!
 * \brief SOME/IP Skeleton event manager type for event 'OBCOuptDcI'.
 */
using SI_OBCStSrvSkeletonSomeIpEventManagerOBCOuptDcI =
    ::amsr::someip_binding_transformation_layer::internal::SkeletonEventXf<SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCOuptDcI>;


// ---- Event 'OBCOuptDcU' -------------------------------------------

/*!
 * \brief Skeleton-side configuration struct for event 'OBCOuptDcU'.
 */
struct SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCOuptDcU {
  /*!
   * \brief SOME/IP service ID.
   */
  static constexpr ::amsr::someip_protocol::internal::ServiceId kServiceId{0x4004};

  /*!
   * \brief SOME/IP major version.
   */
  static constexpr ::amsr::someip_protocol::internal::InterfaceVersion kMajorVersion{1};

  /*!
   * \brief SOME/IP event ID.
   */
  static constexpr ::amsr::someip_protocol::internal::EventId kEventId{0x8005};
  
  /*!
   * \brief Deserializer type for this event
   */
  static constexpr  ::amsr::someipd_app_protocol::internal::MessageType kMessageType{::amsr::someipd_app_protocol::internal::MessageType::kSomeIp};

  /*!
   * \brief Datatype of the event sample.
   * \details AsrPath: /DataTypes/ImplementationDataTypes/OBCOuptDcU_Float_Idt
   */
  using SampleType = ::gwm::pt_powersys::powersys_idt::OBCOuptDcU_Float_Idt;

  /*!
   * \brief Serializer for event payload.
   */
  using PayloadSerializer = ::amsr::someip_protocol::internal::dataprototype_serializer::gwm_pt_powersys_obcstsrv_si::events::SerializerOBCOuptDcU;

  /*!
   * \brief Used session handler.
   * \details Session handling is enabled for the event.
   * \trace CREQ-SomeIpBinding-SessionHandling
   */
  using SessionHandler = ::amsr::someip_binding::internal::SessionHandler;

  /*!
   * \brief E2E profile configuration.
   * \details No E2E protection configured for the event.
   */
  using E2eProfileConfig = void;
};

/*!
 * \brief SOME/IP Skeleton event manager type for event 'OBCOuptDcU'.
 */
using SI_OBCStSrvSkeletonSomeIpEventManagerOBCOuptDcU =
    ::amsr::someip_binding_transformation_layer::internal::SkeletonEventXf<SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCOuptDcU>;


// ---- Field notifier 'OBCChrgGunSt' -------------------------------------------

/*!
 * \brief Skeleton-side configuration struct for event 'OBCChrgGunSt'.
 */
struct SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCChrgGunSt {
  /*!
   * \brief SOME/IP service ID.
   */
  static constexpr ::amsr::someip_protocol::internal::ServiceId kServiceId{0x4004};

  /*!
   * \brief SOME/IP major version.
   */
  static constexpr ::amsr::someip_protocol::internal::InterfaceVersion kMajorVersion{1};

  /*!
   * \brief SOME/IP event ID.
   */
  static constexpr ::amsr::someip_protocol::internal::EventId kEventId{0x9005};
  
  /*!
   * \brief Deserializer type for this event
   */
  static constexpr  ::amsr::someipd_app_protocol::internal::MessageType kMessageType{::amsr::someipd_app_protocol::internal::MessageType::kSomeIp};

  /*!
   * \brief Datatype of the event sample.
   * \details AsrPath: /DataTypes/ImplementationDataTypes/OBCChrgGunSt_Struct_Idt
   */
  using SampleType = ::gwm::pt_powersys::powersys_idt::OBCChrgGunSt_Struct_Idt;

  /*!
   * \brief Serializer for event payload.
   */
  using PayloadSerializer = ::amsr::someip_protocol::internal::dataprototype_serializer::gwm_pt_powersys_obcstsrv_si::fields::SerializerOBCChrgGunSt;

  /*!
   * \brief Used session handler.
   * \details Session handling is enabled for the event.
   * \trace CREQ-SomeIpBinding-SessionHandling
   */
  using SessionHandler = ::amsr::someip_binding::internal::SessionHandler;

  /*!
   * \brief E2E profile configuration.
   * \details No E2E protection configured for the event.
   */
  using E2eProfileConfig = void;
};

/*!
 * \brief SOME/IP Skeleton event manager type for event 'OBCChrgGunSt'.
 */
using SI_OBCStSrvSkeletonSomeIpFieldNotifierOBCChrgGunSt =
    ::amsr::someip_binding_transformation_layer::internal::SkeletonEventXf<SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCChrgGunSt>;


// ---- Field notifier 'OBCErrSt' -------------------------------------------

/*!
 * \brief Skeleton-side configuration struct for event 'OBCErrSt'.
 */
struct SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCErrSt {
  /*!
   * \brief SOME/IP service ID.
   */
  static constexpr ::amsr::someip_protocol::internal::ServiceId kServiceId{0x4004};

  /*!
   * \brief SOME/IP major version.
   */
  static constexpr ::amsr::someip_protocol::internal::InterfaceVersion kMajorVersion{1};

  /*!
   * \brief SOME/IP event ID.
   */
  static constexpr ::amsr::someip_protocol::internal::EventId kEventId{0x9002};
  
  /*!
   * \brief Deserializer type for this event
   */
  static constexpr  ::amsr::someipd_app_protocol::internal::MessageType kMessageType{::amsr::someipd_app_protocol::internal::MessageType::kSomeIp};

  /*!
   * \brief Datatype of the event sample.
   * \details AsrPath: /DataTypes/ImplementationDataTypes/OBCErrSt_Struct_Idt
   */
  using SampleType = ::gwm::pt_powersys::powersys_idt::OBCErrSt_Struct_Idt;

  /*!
   * \brief Serializer for event payload.
   */
  using PayloadSerializer = ::amsr::someip_protocol::internal::dataprototype_serializer::gwm_pt_powersys_obcstsrv_si::fields::SerializerOBCErrSt;

  /*!
   * \brief Used session handler.
   * \details Session handling is enabled for the event.
   * \trace CREQ-SomeIpBinding-SessionHandling
   */
  using SessionHandler = ::amsr::someip_binding::internal::SessionHandler;

  /*!
   * \brief E2E profile configuration.
   * \details No E2E protection configured for the event.
   */
  using E2eProfileConfig = void;
};

/*!
 * \brief SOME/IP Skeleton event manager type for event 'OBCErrSt'.
 */
using SI_OBCStSrvSkeletonSomeIpFieldNotifierOBCErrSt =
    ::amsr::someip_binding_transformation_layer::internal::SkeletonEventXf<SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCErrSt>;


// ---- Field notifier 'OBCHVILSt' -------------------------------------------

/*!
 * \brief Skeleton-side configuration struct for event 'OBCHVILSt'.
 */
struct SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCHVILSt {
  /*!
   * \brief SOME/IP service ID.
   */
  static constexpr ::amsr::someip_protocol::internal::ServiceId kServiceId{0x4004};

  /*!
   * \brief SOME/IP major version.
   */
  static constexpr ::amsr::someip_protocol::internal::InterfaceVersion kMajorVersion{1};

  /*!
   * \brief SOME/IP event ID.
   */
  static constexpr ::amsr::someip_protocol::internal::EventId kEventId{0x9003};
  
  /*!
   * \brief Deserializer type for this event
   */
  static constexpr  ::amsr::someipd_app_protocol::internal::MessageType kMessageType{::amsr::someipd_app_protocol::internal::MessageType::kSomeIp};

  /*!
   * \brief Datatype of the event sample.
   * \details AsrPath: /DataTypes/ImplementationDataTypes/OBCHVILSt_Enum_Idt
   */
  using SampleType = ::gwm::pt_powersys::powersys_idt::OBCHVILSt_Enum_Idt;

  /*!
   * \brief Serializer for event payload.
   */
  using PayloadSerializer = ::amsr::someip_protocol::internal::dataprototype_serializer::gwm_pt_powersys_obcstsrv_si::fields::SerializerOBCHVILSt;

  /*!
   * \brief Used session handler.
   * \details Session handling is enabled for the event.
   * \trace CREQ-SomeIpBinding-SessionHandling
   */
  using SessionHandler = ::amsr::someip_binding::internal::SessionHandler;

  /*!
   * \brief E2E profile configuration.
   * \details No E2E protection configured for the event.
   */
  using E2eProfileConfig = void;
};

/*!
 * \brief SOME/IP Skeleton event manager type for event 'OBCHVILSt'.
 */
using SI_OBCStSrvSkeletonSomeIpFieldNotifierOBCHVILSt =
    ::amsr::someip_binding_transformation_layer::internal::SkeletonEventXf<SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCHVILSt>;


// ---- Field notifier 'OBCSt' -------------------------------------------

/*!
 * \brief Skeleton-side configuration struct for event 'OBCSt'.
 */
struct SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCSt {
  /*!
   * \brief SOME/IP service ID.
   */
  static constexpr ::amsr::someip_protocol::internal::ServiceId kServiceId{0x4004};

  /*!
   * \brief SOME/IP major version.
   */
  static constexpr ::amsr::someip_protocol::internal::InterfaceVersion kMajorVersion{1};

  /*!
   * \brief SOME/IP event ID.
   */
  static constexpr ::amsr::someip_protocol::internal::EventId kEventId{0x9001};
  
  /*!
   * \brief Deserializer type for this event
   */
  static constexpr  ::amsr::someipd_app_protocol::internal::MessageType kMessageType{::amsr::someipd_app_protocol::internal::MessageType::kSomeIp};

  /*!
   * \brief Datatype of the event sample.
   * \details AsrPath: /DataTypes/ImplementationDataTypes/OBCSt_Enum_Idt
   */
  using SampleType = ::gwm::pt_powersys::powersys_idt::OBCSt_Enum_Idt;

  /*!
   * \brief Serializer for event payload.
   */
  using PayloadSerializer = ::amsr::someip_protocol::internal::dataprototype_serializer::gwm_pt_powersys_obcstsrv_si::fields::SerializerOBCSt;

  /*!
   * \brief Used session handler.
   * \details Session handling is enabled for the event.
   * \trace CREQ-SomeIpBinding-SessionHandling
   */
  using SessionHandler = ::amsr::someip_binding::internal::SessionHandler;

  /*!
   * \brief E2E profile configuration.
   * \details No E2E protection configured for the event.
   */
  using E2eProfileConfig = void;
};

/*!
 * \brief SOME/IP Skeleton event manager type for event 'OBCSt'.
 */
using SI_OBCStSrvSkeletonSomeIpFieldNotifierOBCSt =
    ::amsr::someip_binding_transformation_layer::internal::SkeletonEventXf<SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCSt>;


// ---- Field notifier 'OBCV2XSt' -------------------------------------------

/*!
 * \brief Skeleton-side configuration struct for event 'OBCV2XSt'.
 */
struct SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCV2XSt {
  /*!
   * \brief SOME/IP service ID.
   */
  static constexpr ::amsr::someip_protocol::internal::ServiceId kServiceId{0x4004};

  /*!
   * \brief SOME/IP major version.
   */
  static constexpr ::amsr::someip_protocol::internal::InterfaceVersion kMajorVersion{1};

  /*!
   * \brief SOME/IP event ID.
   */
  static constexpr ::amsr::someip_protocol::internal::EventId kEventId{0x9004};
  
  /*!
   * \brief Deserializer type for this event
   */
  static constexpr  ::amsr::someipd_app_protocol::internal::MessageType kMessageType{::amsr::someipd_app_protocol::internal::MessageType::kSomeIp};

  /*!
   * \brief Datatype of the event sample.
   * \details AsrPath: /DataTypes/ImplementationDataTypes/OBCV2XSt_Enum_Idt
   */
  using SampleType = ::gwm::pt_powersys::powersys_idt::OBCV2XSt_Enum_Idt;

  /*!
   * \brief Serializer for event payload.
   */
  using PayloadSerializer = ::amsr::someip_protocol::internal::dataprototype_serializer::gwm_pt_powersys_obcstsrv_si::fields::SerializerOBCV2XSt;

  /*!
   * \brief Used session handler.
   * \details Session handling is enabled for the event.
   * \trace CREQ-SomeIpBinding-SessionHandling
   */
  using SessionHandler = ::amsr::someip_binding::internal::SessionHandler;

  /*!
   * \brief E2E profile configuration.
   * \details No E2E protection configured for the event.
   */
  using E2eProfileConfig = void;
};

/*!
 * \brief SOME/IP Skeleton event manager type for event 'OBCV2XSt'.
 */
using SI_OBCStSrvSkeletonSomeIpFieldNotifierOBCV2XSt =
    ::amsr::someip_binding_transformation_layer::internal::SkeletonEventXf<SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCV2XSt>;


}  // namespace internal
}  // namespace someip_binding_transformation_layer
}  // namespace amsr
}  // namespace obcstsrv_si
}  // namespace pt_powersys
}  // namespace gwm

#endif  // DENPENDENCYSRVEXE_INCLUDE_AMSR_SOMEIP_BINDING_TRANSFORMATION_LAYER_INTERNAL_GWM_PT_POWERSYS_OBCSTSRV_SI_SI_OBCSTSRV_SKELETON_SOMEIP_EVENT_MANAGER_H_

