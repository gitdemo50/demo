/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/include/gwm/pt_batterypacksys/batterypacksys_idt/impl_type_bmsdcchrgendreas_enum_idt.h
 *        \brief
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_modelleddatatypes_api
 *         Commit ID: d9332226824a4a6c4616e2d948d41c862d192343
 *********************************************************************************************************************/

#ifndef DENPENDENCYSRVEXE_INCLUDE_GWM_PT_BATTERYPACKSYS_BATTERYPACKSYS_IDT_IMPL_TYPE_BMSDCCHRGENDREAS_ENUM_IDT_H_
#define DENPENDENCYSRVEXE_INCLUDE_GWM_PT_BATTERYPACKSYS_BATTERYPACKSYS_IDT_IMPL_TYPE_BMSDCCHRGENDREAS_ENUM_IDT_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include <array>
#include <cstdint>
#include <map>
#include <string>
#include <vector>

namespace gwm {
namespace pt_batterypacksys {
namespace batterypacksys_idt {

/*!
 * \brief Type BMSDCChrgEndReas_Enum_Idt.
 * \remark generated
 * \trace SPEC-5951372
 *
 * IMPLEMENTATION-DATA-TYPE /DataTypes/ImplementationDataTypes/BMSDCChrgEndReas_Enum_Idt
 */
enum class BMSDCChrgEndReas_Enum_Idt : std::uint8_t {
  No_Reach_The_Required_SOC_Target_Value_Is = 0,
  Reach_The_Required_SOC_Target_Value = 1,
  Untrusted_Status = 2,
  Reach_The_Total_Voltage_Set_Value = 3,
  Reach_The_Single_Voltage_Set_Value = 4,
  Reserved1 = 5,
  Reserved2 = 6,
  Reserved3 = 7
};

}  // namespace batterypacksys_idt
}  // namespace pt_batterypacksys
}  // namespace gwm

#endif  // DENPENDENCYSRVEXE_INCLUDE_GWM_PT_BATTERYPACKSYS_BATTERYPACKSYS_IDT_IMPL_TYPE_BMSDCCHRGENDREAS_ENUM_IDT_H_
