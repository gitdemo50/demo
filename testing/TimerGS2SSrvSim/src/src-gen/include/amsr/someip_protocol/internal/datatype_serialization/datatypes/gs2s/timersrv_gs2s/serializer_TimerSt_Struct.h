/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/include/amsr/someip_protocol/internal/datatype_serialization/datatypes/gs2s/timersrv_gs2s/serializer_TimerSt_Struct.h
 *        \brief  SOME/IP protocol serializer implementation for datatype 'TimerSt_Struct'
 *
 *      \details  /S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/TimerSt_Struct
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_someipprotocol
 *         Commit ID: 0e137b9e6356987a6d2839c8d79c286219b68d60
 *********************************************************************************************************************/

#ifndef DENPENDENCYSRVEXE_INCLUDE_AMSR_SOMEIP_PROTOCOL_INTERNAL_DATATYPE_SERIALIZATION_DATATYPES_GS2S_TIMERSRV_GS2S_SERIALIZER_TIMERST_STRUCT_H_
#define DENPENDENCYSRVEXE_INCLUDE_AMSR_SOMEIP_PROTOCOL_INTERNAL_DATATYPE_SERIALIZATION_DATATYPES_GS2S_TIMERSRV_GS2S_SERIALIZER_TIMERST_STRUCT_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "datatypes/gs2s/timersrv_gs2s/impl_type_timerst_struct.h"
#include "someip-protocol/internal/serialization/ser_common.h"
#include "someip-protocol/internal/serialization/ser_forward.h"
#include "someip-protocol/internal/serialization/ser_sizing.h"
#include "someip-protocol/internal/serialization/writer.h"

namespace datatypes {
namespace gs2s {
namespace timersrv_gs2s {

/*!
 * \brief Serializer for datatype /S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/TimerSt_Struct.
 * \details Data type class: DataTypeStruct (CppImplDataTypeStruct).
 *          is TLV struct: false
 *
 * \tparam TpPack Transformation properties to be used for serialization.
 *
 * \param[in, out] writer Reference to byte stream writer.
 * \param[in] data Reference to the data object of type /S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/TimerSt_Struct to be serialized.
 * \pre The writer holds a memory buffer which is enough to store the serialized data.
 * \context Reactor|App
 * \threadsafe FALSE
 * \reentrant TRUE
 * \vprivate Vector component internal API.
 * \synchronous TRUE
 */
template <typename TpPack>
void SomeIpProtocolSerialize(amsr::someip_protocol::internal::serialization::Writer& writer, ::datatypes::gs2s::timersrv_gs2s::TimerSt_Struct const &data) noexcept {
  // Namespace alias for static serialization code
  namespace serialization = amsr::someip_protocol::internal::serialization;
  // Serialize non-TLV struct
    // Serialize struct member 'WakeSrcexpired' of type /S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/TimerSt_WakeSrcexpired_Enum
    serialization::SomeIpProtocolSerialize<
                        TpPack,
                        // Byte-order of primitive datatype (/S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/TimerSt_WakeSrcexpired_Enum)
                        typename serialization::Tp<TpPack>::ByteOrder

                        >(writer, data.WakeSrcexpired);
    // Serialize struct member 'WakeSource' of type /S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/TimerSt_WakeSource_Enum
    serialization::SomeIpProtocolSerialize<
                        TpPack,
                        // Byte-order of primitive datatype (/S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/TimerSt_WakeSource_Enum)
                        typename serialization::Tp<TpPack>::ByteOrder

                        >(writer, data.WakeSource);
}

/*!
 * \brief Calculates the required buffer size for datatype /S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/TimerSt_Struct.
 * \details Data type class: DataTypeStruct (CppImplDataTypeStruct).
 *          is TLV struct: false
 *
 * \tparam TpPack Transformation properties to be used for serialization.
 * \param[in] data Reference to the data object of type /S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/TimerSt_Struct.
 * \return Returns the required buffer size for the struct in bytes.
 * \pre -
 * \context Reactor|App
 * \threadsafe FALSE
 * \reentrant TRUE
 * \vprivate Vector component internal API.
 * \synchronous TRUE
 */
template <typename TpPack>
constexpr std::size_t GetRequiredBufferSize(::datatypes::gs2s::timersrv_gs2s::TimerSt_Struct const &data) noexcept {
  // Namespace alias for static serialization code
  namespace serialization = amsr::someip_protocol::internal::serialization;

  // Sum of required size
  std::size_t required_buffer_size{0};

  // Accumulate the static size of struct member 'WakeSrcexpired' of type /S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/TimerSt_WakeSrcexpired_Enum
  required_buffer_size += serialization::GetRequiredBufferSize<
                        TpPack,
                        // Byte-order of primitive datatype (/S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/TimerSt_WakeSrcexpired_Enum)
                        typename serialization::Tp<TpPack>::ByteOrder

                        >(data.WakeSrcexpired);
  // Accumulate the static size of struct member 'WakeSource' of type /S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/TimerSt_WakeSource_Enum
  required_buffer_size += serialization::GetRequiredBufferSize<
                        TpPack,
                        // Byte-order of primitive datatype (/S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/TimerSt_WakeSource_Enum)
                        typename serialization::Tp<TpPack>::ByteOrder

                        >(data.WakeSource);

  return required_buffer_size;
}

/*!
 * \brief Checks if datatype /S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/TimerSt_Struct is of static size.
 * \details Data type class: DataTypeStruct (CppImplDataTypeStruct).
 *          is TLV struct: false
 *
 * \tparam TpPack Transformation properties to be used for serialization.
 * \return True if datatype has static size, false otherwise.
 * \pre -
 * \context Reactor|App
 * \threadsafe FALSE
 * \reentrant TRUE
 * \vprivate Vector component internal API.
 * \synchronous TRUE
 */
template <typename TpPack>
constexpr bool IsStaticSize(amsr::someip_protocol::internal::serialization::SizeToken<::datatypes::gs2s::timersrv_gs2s::TimerSt_Struct>) noexcept {
  // Namespace alias for static serialization code
  namespace serialization = amsr::someip_protocol::internal::serialization;

  constexpr bool is_static_size{
  // Check static size status of struct member 'WakeSrcexpired' of type /S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/TimerSt_WakeSrcexpired_Enum
  serialization::IsStaticSize<
                      TpPack,
                      // Byte-order of primitive datatype (/S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/TimerSt_WakeSrcexpired_Enum)
                      typename serialization::Tp<TpPack>::ByteOrder

                      >(serialization::SizeToken<::datatypes::gs2s::timersrv_gs2s::TimerSt_WakeSrcexpired_Enum>{})  && 
  
  // Check static size status of struct member 'WakeSource' of type /S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/TimerSt_WakeSource_Enum
  serialization::IsStaticSize<
                      TpPack,
                      // Byte-order of primitive datatype (/S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/TimerSt_WakeSource_Enum)
                      typename serialization::Tp<TpPack>::ByteOrder

                      >(serialization::SizeToken<::datatypes::gs2s::timersrv_gs2s::TimerSt_WakeSource_Enum>{}) 
  };
    return is_static_size;
}

}  // namespace timersrv_gs2s
}  // namespace gs2s
}  // namespace datatypes

#endif  // DENPENDENCYSRVEXE_INCLUDE_AMSR_SOMEIP_PROTOCOL_INTERNAL_DATATYPE_SERIALIZATION_DATATYPES_GS2S_TIMERSRV_GS2S_SERIALIZER_TIMERST_STRUCT_H_

