#ifndef TESTS_PROXYIMPL_MOCK_HPP
#define TESTS_PROXYIMPL_MOCK_HPP

#include <gmock/gmock.h>

#include <functional>

#include "shims.hpp"

namespace gwm{
namespace services {
namespace timer {
namespace proxy {

struct DIImpl {
    int data;
};

class MockEvent : public gwm::services::EventBase<int> {
public:
    using ReceiveHandler = std::function<void()>;
    using SampleType = int;
    using CacheType = ara::com::SampleContainer<ara::com::SamplePtrInternal<const SampleType>>;

    MOCK_METHOD(void,SetReceiveHandler,(ReceiveHandler),());
    MOCK_METHOD(void,Subscribe,(ara::com::EventCacheUpdatePolicy,int),());
    MOCK_METHOD(bool,Update,(),());
    MOCK_METHOD((const CacheType&),GetCachedSamples,(),(const));
};

class ProxyImpl {
public:
    struct HandleType
    {
        int i;
    };

    
    using HandleContainer = ara::com::ServiceHandleContainer<HandleType>;
    using Handle = HandleType;
    using Handler = ara::com::FindServiceHandler<Handle>;


    ProxyImpl() {

    }

    ProxyImpl(HandleType) {

    }

    static ara::com::FindServiceHandle StartFindService(Handler handler, ara::core::InstanceSpecifier inst) {
        static int FindServiceHandleCount=0;
        HandleContainer c;
        c.push_back(HandleType{1});
        FindServiceHandleCount++;
        if(handler)
            handler(c);
        return {inst.ToString()};
    }

    static void StopFindService(ara::com::FindServiceHandle h) {

    }
    
    MockEvent mockEvent;
    //MOCK_METHOD(ara::com::FindServiceHandle, StartFindService, (ara::com::HandleType));
};

}}}}

#endif