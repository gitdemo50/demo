/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  TimerSrvExe/include/amsr/someip_protocol/internal/method_deserialization/gwm_application_remsdl_timersrv_si/methods/deserializer_Request_TimerCncl.h
 *        \brief  SOME/IP packet deserializer of service 'SI_TimerSrv'.
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_someipprotocol
 *         Commit ID: 0e137b9e6356987a6d2839c8d79c286219b68d60
 *********************************************************************************************************************/

#ifndef TIMERSRVEXE_INCLUDE_AMSR_SOMEIP_PROTOCOL_INTERNAL_METHOD_DESERIALIZATION_GWM_APPLICATION_REMSDL_TIMERSRV_SI_METHODS_deserializer_Request_TimerCncl_h_
#define TIMERSRVEXE_INCLUDE_AMSR_SOMEIP_PROTOCOL_INTERNAL_METHOD_DESERIALIZATION_GWM_APPLICATION_REMSDL_TIMERSRV_SI_METHODS_deserializer_Request_TimerCncl_h_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "someip-protocol/internal/deserialization/common.h"
#include "someip-protocol/internal/deserialization/reader.h"

namespace amsr {
namespace someip_protocol {
namespace internal {
namespace method_deserializer {
namespace gwm_application_remsdl_timersrv_si {
namespace methods {

/*!
 * \brief Deserializer for a SOME/IP packet of method '/ServiceInterfaces/SI_TimerSrv/TimerCncl'
 *        of service interface '/ServiceInterfaces/SI_TimerSrv'.
 */
class DeserializerRequestTimerCncl final {
 public:
  /*!
   * \brief Deserialize the SOME/IP packet payload of method 'TimerCncl'.
   *
   * \tparam Output Type of data structure storing all deserialized in-/output arguments.
   *                Type must support member initializer lists. Typically a struct or a std::tuple is used.
   * \param[in,out] reader Reference to the byte stream reader.
   * \param[out]    output Parameter to store deserialized output.
   * \return        True if the deserialization is successful, false otherwise.
   * \pre           -
   * \context       Reactor|App
   * \threadsafe    FALSE
   * \reentrant     TRUE for different reader objects.
   * \synchronous   TRUE
   */
  template <typename Output>
  static bool Deserialize(deserialization::Reader const& reader, Output  const& output) noexcept {    
    // No arguments to deserialize
    static_cast<void>(reader);
    static_cast<void>(output);

    // Return always true as no deserialization is necessary.
    return true;
  }
};

}  // namespace methods
}  // namespace gwm_application_remsdl_timersrv_si
}  // namespace method_deserializer
}  // namespace internal
}  // namespace someip_protocol
}  // namespace amsr

#endif  // TIMERSRVEXE_INCLUDE_AMSR_SOMEIP_PROTOCOL_INTERNAL_METHOD_DESERIALIZATION_GWM_APPLICATION_REMSDL_TIMERSRV_SI_METHODS_deserializer_Request_TimerCncl_h_

