/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  TimerSrvExe/include/gwm/platform/timer/gs2s/SI_TimerSrv_GS2S_proxy_update_manager.h
 *        \brief  Proxy update manager of service 'SI_TimerSrv_GS2S'.
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_socal
 *         Commit ID: 209b3c0234013e84e3170ee826c9a8c6022de194
 *********************************************************************************************************************/

#ifndef TIMERSRVEXE_INCLUDE_GWM_PLATFORM_TIMER_GS2S_SI_TIMERSRV_GS2S_PROXY_UPDATE_MANAGER_H_
#define TIMERSRVEXE_INCLUDE_GWM_PLATFORM_TIMER_GS2S_SI_TIMERSRV_GS2S_PROXY_UPDATE_MANAGER_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "amsr/socal/internal/events/proxy_event_backend_interface.h"
#include "amsr/socal/internal/proxy_update_manager.h"
#include "ara/core/future.h"
#include "gwm/platform/timer/gs2s/SI_TimerSrv_GS2S_proxy_backend_interface.h"
#include "gwm/platform/timer/gs2s/SI_TimerSrv_GS2S_types.h"

namespace gwm {
namespace platform {
namespace timer {
namespace gs2s {
namespace internal {

/*!
 * \brief Proxy update manager for the Service 'SI_TimerSrv_GS2S'
 * \remark generated
 */
class SI_TimerSrv_GS2SProxyUpdateManager : public SI_TimerSrv_GS2SProxyBackendInterface,
                                                            public ::amsr::socal::internal::ProxyUpdateManager<SI_TimerSrv_GS2SProxyBackendInterface>{
 public:
  using BackendInterfaceType = SI_TimerSrv_GS2SProxyBackendInterface;
  using Base = ::amsr::socal::internal::ProxyUpdateManager<SI_TimerSrv_GS2SProxyBackendInterface>;

  SI_TimerSrv_GS2SProxyUpdateManager() = delete;

  SI_TimerSrv_GS2SProxyUpdateManager(SI_TimerSrv_GS2SProxyUpdateManager const &) = delete;

  SI_TimerSrv_GS2SProxyUpdateManager(SI_TimerSrv_GS2SProxyUpdateManager &&) = delete;

  SI_TimerSrv_GS2SProxyUpdateManager &operator=(SI_TimerSrv_GS2SProxyUpdateManager const &) & = delete;

  SI_TimerSrv_GS2SProxyUpdateManager &operator=(SI_TimerSrv_GS2SProxyUpdateManager &&) & = delete;

  /*!
   * \brief Destroys the proxy update manager.
   * \pre -
   * \context   App
   */
   ~SI_TimerSrv_GS2SProxyUpdateManager() noexcept override = default;

  /*!
   * \brief Initializes the ProxyUpdateManager and starts listening to the service discovery.
   * \param[in] id The instance identifier.
   * \param[in] service_discovery The proxy service discovery singleton instance.
   * \param[in] proxy_backend The proxy backend to communicate with the bindings.
   * \pre -
   * \context   App
   */
  SI_TimerSrv_GS2SProxyUpdateManager(ara::com::InstanceIdentifier const& id,
    ::amsr::generic::Singleton<ServiceDiscovery*>& service_discovery,
    BackendInterfaceType& proxy_backend) noexcept
    : ProxyUpdateManager{id, service_discovery, proxy_backend} {
      StartListening();
    }

  // ---- Methods --------------------------------------------------------------------------------------------------

  /*!
   * \brief Invokes request response method TimerReq.
    * \param[in] TimerSet IN parameter of type ::datatypes::gs2s::timersrv_gs2s::TimerSet_Integer
    * \param[in] StartPoint IN parameter of type ::datatypes::gs2s::timersrv_gs2s::StartPoint_Enum
    * \param[in] TimerType IN parameter of type ::datatypes::gs2s::timersrv_gs2s::TimerType_Enum
    * \param[in] SleepType IN parameter of type ::datatypes::gs2s::timersrv_gs2s::SleepType_Enum
   * \return ara::core::Future with method output data element.
   * \pre -
   * \context       App
   * \synchronous   FALSE
   */
  ara::core::Future<methods::TimerReq::Output> HandleMethodTimerReq(::datatypes::gs2s::timersrv_gs2s::TimerSet_Integer const& TimerSet, ::datatypes::gs2s::timersrv_gs2s::StartPoint_Enum const& StartPoint, ::datatypes::gs2s::timersrv_gs2s::TimerType_Enum const& TimerType, ::datatypes::gs2s::timersrv_gs2s::SleepType_Enum const& SleepType) noexcept override {    
    MethodHandler<::datatypes::gs2s::timersrv_gs2s::TimerSet_Integer const&, ::datatypes::gs2s::timersrv_gs2s::StartPoint_Enum const&, ::datatypes::gs2s::timersrv_gs2s::TimerType_Enum const&, ::datatypes::gs2s::timersrv_gs2s::SleepType_Enum const&> const methodHandler{GetBackend()};
    return methodHandler.RequestResponseMethod<methods::TimerReq::Output, 
            &BackendInterfaceType::HandleMethodTimerReq>(TimerSet, StartPoint, TimerType, SleepType);
  }

  // ---- Events ---------------------------------------------------------------------------------------------------

  // ---- Fields ---------------------------------------------------------------------------------------------------

  // ---- Field 'TimerSt' ----
  /*!
   * \brief Get the field notifier object for the service field 'TimerSt'
   * \return A proxy event object supporting event sample and subscription.
   * \pre -
   * \context App
   */
    ::amsr::socal::internal::events::ProxyEventBackendInterface<::datatypes::gs2s::timersrv_gs2s::TimerSt_Struct>& GetFieldNotifierBackendTimerSt() noexcept override {
      return GetBackend().GetFieldNotifierBackendTimerSt();
    }

  /*!
   * \brief Getter for the field 'TimerSt'
   * \return ara::core::Future with Field data element.
   * \pre -
   * \context App
   */
    ara::core::Future<fields::TimerSt::Output> HandleFieldTimerStMethodGet() noexcept override {
      return this->FieldMethodGet<fields::TimerSt::Output, &BackendInterfaceType::HandleFieldTimerStMethodGet>();
    }

};

} //namespace internal
}  // namespace gs2s
}  // namespace timer
}  // namespace platform
}  // namespace gwm

#endif  // TIMERSRVEXE_INCLUDE_GWM_PLATFORM_TIMER_GS2S_SI_TIMERSRV_GS2S_PROXY_UPDATE_MANAGER_H_

