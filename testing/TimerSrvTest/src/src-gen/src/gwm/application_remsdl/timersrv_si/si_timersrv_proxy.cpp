/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  TiMgrSrvExe/src/gwm/application_remsdl/timersrv_si/si_timersrv_proxy.cpp
 *        \brief  Proxy for service 'SI_TimerSrv'.
 *
 *      \details  This Service interface provides timer
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_socal
 *         Commit ID: 209b3c0234013e84e3170ee826c9a8c6022de194
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "gwm/application_remsdl/timersrv_si/si_timersrv_proxy.h"

/*!
 * \trace SPEC-4980240
 * \trace SPEC-4980242
 */
namespace gwm {
namespace application_remsdl {
namespace timersrv_si {
namespace proxy {

 /*!
  * \trace SPEC-8053550
  */
// ============ Proxy preconstructor (returns a token or an error) ============
SI_TimerSrvProxy::ConstructionResult SI_TimerSrvProxy::Preconstruct(
    SI_TimerSrvProxy::HandleType const& handle) noexcept {
    return Base::Preconstruct(handle.GetInstanceId());
}

/*!
 * \trace SPEC-8053550
 */
// ====================== Proxy constructor (token based) =====================
SI_TimerSrvProxy::SI_TimerSrvProxy(ConstructionToken&& token) noexcept
  : Base{std::move(token)},
    logger_(amsr::socal::internal::logging::kAraComLoggerContextId, amsr::socal::internal::logging::kAraComLoggerContextDescription,
            "SI_TimerSrvProxy"),
    proxy_update_manager_{Base::GetInstanceId(), Base::GetServiceDiscovery(), Base::GetProxyBackend()},
    TimerCncl{proxy_update_manager_},
    TimerReq{proxy_update_manager_},
// VECTOR Disable VectorC++-V5.0.1: MD_SOCAL_VectorC++-V5.0.1_UnsequencedFunctionCalls
    TimerSt{&(proxy_update_manager_.GetFieldNotifierBackendTimerSt()), proxy_update_manager_, Base::GetRuntime(), "SI_TimerSrv", "TimerSt", Base::GetInstanceId()}
    
// VECTOR Enable VectorC++-V5.0.1
    {}

// ====================== Proxy constructor ======================
SI_TimerSrvProxy::SI_TimerSrvProxy(
  SI_TimerSrvProxy::HandleType const& handle) noexcept
  : SI_TimerSrvProxy{Preconstruct(handle).Value()} {}

// ============================= Proxy destructor =============================
SI_TimerSrvProxy::~SI_TimerSrvProxy() noexcept {
  // Start cleanup of proxy by unsubscribing all event and field notifications
  // Events

  // Fields
  
  TimerSt.UnsetReceiveHandler();
  TimerSt.UnsetSubscriptionStateHandler();
  TimerSt.Unsubscribe();
}

::amsr::socal::ServiceState SI_TimerSrvProxy::ReadServiceState() noexcept {
  return proxy_update_manager_.ReadServiceState();
}

}  // namespace proxy
}  // namespace timersrv_si
}  // namespace application_remsdl
}  // namespace gwm

