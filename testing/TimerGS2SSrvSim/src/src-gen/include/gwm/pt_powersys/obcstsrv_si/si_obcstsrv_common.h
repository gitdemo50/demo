/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/include/gwm/pt_powersys/obcstsrv_si/si_obcstsrv_common.h
 *        \brief  Header for service 'SI_OBCStSrv'.
 *
 *      \details  This service interface provides OBC working status information
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_socal
 *         Commit ID: 209b3c0234013e84e3170ee826c9a8c6022de194
 *********************************************************************************************************************/

#ifndef DENPENDENCYSRVEXE_INCLUDE_GWM_PT_POWERSYS_OBCSTSRV_SI_SI_OBCSTSRV_COMMON_H_
#define DENPENDENCYSRVEXE_INCLUDE_GWM_PT_POWERSYS_OBCSTSRV_SI_SI_OBCSTSRV_COMMON_H_

/*!
 * \trace SPEC-4980247, SPEC-4980248, SPEC-5951130, SPEC-4980251
 */
/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "ara/com/types.h"
#include "gwm/pt_powersys/obcstsrv_si/SI_OBCStSrv.h"
#include "gwm/pt_powersys/powersys_idt/impl_type_obcactt_float_idt.h"
#include "gwm/pt_powersys/powersys_idt/impl_type_obcchrggunst_struct_idt.h"
#include "gwm/pt_powersys/powersys_idt/impl_type_obcchrglim_struct_idt.h"
#include "gwm/pt_powersys/powersys_idt/impl_type_obcdchroutpi_float_idt.h"
#include "gwm/pt_powersys/powersys_idt/impl_type_obcdchroutpu_float_idt.h"
#include "gwm/pt_powersys/powersys_idt/impl_type_obcerrst_struct_idt.h"
#include "gwm/pt_powersys/powersys_idt/impl_type_obchvilst_enum_idt.h"
#include "gwm/pt_powersys/powersys_idt/impl_type_obcinptaci_float_idt.h"
#include "gwm/pt_powersys/powersys_idt/impl_type_obcinptacu_float_idt.h"
#include "gwm/pt_powersys/powersys_idt/impl_type_obcouptdci_float_idt.h"
#include "gwm/pt_powersys/powersys_idt/impl_type_obcouptdcu_float_idt.h"
#include "gwm/pt_powersys/powersys_idt/impl_type_obcst_enum_idt.h"
#include "gwm/pt_powersys/powersys_idt/impl_type_obcv2xst_enum_idt.h"

#endif  // DENPENDENCYSRVEXE_INCLUDE_GWM_PT_POWERSYS_OBCSTSRV_SI_SI_OBCSTSRV_COMMON_H_
