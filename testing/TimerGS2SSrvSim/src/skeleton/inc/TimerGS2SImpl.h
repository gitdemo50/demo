#ifndef TIMERGS2SIMPL
#define TIMERGS2SIMPL

#include <memory>

#include "gwm/platform/timer/gs2s/si_timersrv_gs2s_skeleton.h"

#include "workerthread.hpp"
#include "skeletonmixin.hpp"

namespace gwm {
namespace services {
namespace timersrv_gs2s {

namespace skeleton {

using TimerSrvGS2SSkeleton = gwm::platform::timer::gs2s::skeleton::SI_TimerSrv_GS2SSkeleton;
namespace methods = gwm::platform::timer::gs2s::skeleton::methods;

using TimerStFieldType = ::datatypes::gs2s::timersrv_gs2s::TimerSt_Struct;
using TimerSetFieldType = ::datatypes::gs2s::timersrv_gs2s::TimerSet_Integer;
using StartPointFieldType = ::datatypes::gs2s::timersrv_gs2s::StartPoint_Enum;
using TimerTypeFieldType = ::datatypes::gs2s::timersrv_gs2s::TimerType_Enum;
using SleepTypeFieldType = ::datatypes::gs2s::timersrv_gs2s::SleepType_Enum;

class TimerSrvGS2SImpl : public gwm::skeleton::SkeletonMixin<TimerSrvGS2SSkeleton> {
public:

    TimerSrvGS2SImpl();
    ~TimerSrvGS2SImpl();

    void SetTimerSt(TimerStFieldType timerSt);
    ara::core::Future<TimerStFieldType> getTimerSt();

    ara::core::Future<methods::TimerReq::Output> TimerReq(const ::datatypes::gs2s::timersrv_gs2s::TimerSet_Integer& TimerSet, const ::datatypes::gs2s::timersrv_gs2s::StartPoint_Enum& StartPoint, const ::datatypes::gs2s::timersrv_gs2s::TimerType_Enum& TimerType, const ::datatypes::gs2s::timersrv_gs2s::SleepType_Enum& SleepType) override;

private:
    TimerStFieldType m_sctTimerSt;

    TimerSetFieldType m_uiTimerSet;
    StartPointFieldType m_enStartPoint;
    TimerTypeFieldType m_enTimerType;
    SleepTypeFieldType m_enSleepType;

    uint32_t m_uiTimer;
    bool m_bTiming;

    std::unique_ptr<gwm::utils::WorkerThread> worker;

    inline void ontimer(bool timing);
};

}}}}

#endif