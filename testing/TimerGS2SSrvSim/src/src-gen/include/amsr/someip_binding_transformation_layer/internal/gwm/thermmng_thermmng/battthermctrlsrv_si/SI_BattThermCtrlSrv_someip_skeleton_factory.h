/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/include/amsr/someip_binding_transformation_layer/internal/gwm/thermmng_thermmng/battthermctrlsrv_si/SI_BattThermCtrlSrv_someip_skeleton_factory.h
 *        \brief  SOME/IP skeleton factory for service 'SI_BattThermCtrlSrv'
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_someipbinding
 *         Commit ID: 43b33f0283d4d61385b98a3599688499abd86a75
 *********************************************************************************************************************/

#ifndef DENPENDENCYSRVEXE_INCLUDE_AMSR_SOMEIP_BINDING_TRANSFORMATION_LAYER_INTERNAL_GWM_THERMMNG_THERMMNG_BATTTHERMCTRLSRV_SI_SI_BATTTHERMCTRLSRV_SOMEIP_SKELETON_FACTORY_H_
#define DENPENDENCYSRVEXE_INCLUDE_AMSR_SOMEIP_BINDING_TRANSFORMATION_LAYER_INTERNAL_GWM_THERMMNG_THERMMNG_BATTTHERMCTRLSRV_SI_SI_BATTTHERMCTRLSRV_SOMEIP_SKELETON_FACTORY_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "amsr/someip_binding_transformation_layer/internal/gwm/thermmng_thermmng/battthermctrlsrv_si/SI_BattThermCtrlSrv_skeleton_someip_binding.h"
#include "gwm/thermmng_thermmng/battthermctrlsrv_si/si_battthermctrlsrv_skeleton.h"

namespace amsr {
namespace someip_binding_transformation_layer {
namespace internal {

namespace gwm {
namespace thermmng_thermmng {
namespace battthermctrlsrv_si {

/*!
 * \brief SOME/IP skeleton factory for the service interface 'SI_BattThermCtrlSrv'
 * \remark generated
 */
// VECTOR NL AutosarC++17_10-M3.4.1: MD_SOMEIPBINDING_AutosarC++17_10-M3.4.1_CanBeDeclaredLocallyInPrimaryFile
using SI_BattThermCtrlSrvSomeIpSkeletonFactory = ::amsr::someip_binding_transformation_layer::internal::AraComSomeIpSkeletonFactory<::gwm::thermmng_thermmng::battthermctrlsrv_si::skeleton::SI_BattThermCtrlSrvSkeleton, SI_BattThermCtrlSrvSkeletonSomeIpBinding>;

}  // namespace internal
}  // namespace someip_binding_transformation_layer
}  // namespace amsr
}  // namespace battthermctrlsrv_si
}  // namespace thermmng_thermmng
}  // namespace gwm

#endif  // DENPENDENCYSRVEXE_INCLUDE_AMSR_SOMEIP_BINDING_TRANSFORMATION_LAYER_INTERNAL_GWM_THERMMNG_THERMMNG_BATTTHERMCTRLSRV_SI_SI_BATTTHERMCTRLSRV_SOMEIP_SKELETON_FACTORY_H_

