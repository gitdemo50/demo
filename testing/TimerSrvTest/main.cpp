#include <iostream>
#include <chrono>
#include <thread>
#include <map>

#include "ara/core/future.h"
#include "ara/core/initialization.h"
#include "ara/log/logging.h"

#include "TimerManager.hpp"
#include "gwm/composite_timermgr/timgrsrv_si/si_timgrsrv_skeleton.h"

using namespace vac::container::literals;
using namespace vac::language::literals;

void menu(){

    std::cout<<"************************************ "<<std::endl;
    std::cout<<"  1 TimerReq " <<std::endl;
    std::cout<<"  2 TimerCncl " <<std::endl;
    std::cout<<"************************************ "<<std::endl;
}


int main(int, char**) {
    
    ara::core::Result<void> init_result{ara::core::Initialize()};

    if (!init_result.HasValue()) {
        ara::core::Abort("ara::core::Initialize() failed Aborting");
    }
    
    auto TimerManager = gwm::manager::TimerManager::getInstance();
    TimerManager->getInstance()->start();

    while (true)
    {
        menu();

        int b;
        std::cin >> b ;

        if(b==1){

            std::cout<<"Set Timer, and valid time range is 1-604800. "<<std::endl;
            TimerManager->test(b);
        }else if(b==2){

            std::cout<<"Cancel timer "<<std::endl;
            TimerManager->test(b);
        }else{

            std::cout<<"Only two choices for this: 0 or 1 "<<std::endl;
        }
    }
}
