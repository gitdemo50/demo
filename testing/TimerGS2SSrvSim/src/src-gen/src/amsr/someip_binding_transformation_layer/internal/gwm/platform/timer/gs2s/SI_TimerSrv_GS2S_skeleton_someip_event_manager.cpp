/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/src/amsr/someip_binding_transformation_layer/internal/gwm/platform/timer/gs2s/SI_TimerSrv_GS2S_skeleton_someip_event_manager.cpp
 *        \brief  SOME/IP skeleton event handling for events and field notifications of service 'SI_TimerSrv_GS2S'
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_someipbinding
 *         Commit ID: 43b33f0283d4d61385b98a3599688499abd86a75
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "amsr/someip_binding_transformation_layer/internal/gwm/platform/timer/gs2s/SI_TimerSrv_GS2S_skeleton_someip_event_manager.h"

namespace amsr {
namespace someip_binding_transformation_layer {
namespace internal {
namespace gwm {
namespace platform {
namespace timer {
namespace gs2s {


// ---- Field notifier 'TimerSt' -------------------------------------------

/*!
 * \brief Definition of the service ID of the skeleton event configuration for event 'TimerSt'.
 */
::amsr::someip_protocol::internal::ServiceId constexpr SI_TimerSrv_GS2SSkeletonSomeIpEventConfigurationTimerSt::kServiceId;

/*!
 * \brief Definition of the event ID of the skeleton event configuration for event 'TimerSt'.
 */
::amsr::someip_protocol::internal::EventId constexpr SI_TimerSrv_GS2SSkeletonSomeIpEventConfigurationTimerSt::kEventId;

}  // namespace internal
}  // namespace someip_binding_transformation_layer
}  // namespace amsr
}  // namespace gs2s
}  // namespace timer
}  // namespace platform
}  // namespace gwm

