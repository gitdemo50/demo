#pragma once

#include <string>

namespace vac {
namespace container {
inline namespace literals {

std::string operator"" _sv(char const* s, std::size_t size) noexcept;

}
}
}