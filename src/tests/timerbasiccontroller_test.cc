#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "TimerBasicController.hpp"

namespace gwm {
namespace controller {

using ::testing::MockFunction;
using ::testing::Throw;

class MockTimerS2SService : public TimerS2SService {
public:
    void raiseTimer(datatypes::gs2s::timersrv_gs2s::TimerSt_Struct val) {
        proxy->TimerSt.raiseEvent(val);
    }

    void setTimerReqExpectOK(datatypes::gs2s::timersrv_gs2s::TimerSet_Integer a1,
          datatypes::gs2s::timersrv_gs2s::StartPoint_Enum a2, datatypes::gs2s::timersrv_gs2s::TimerType_Enum a3,
          datatypes::gs2s::timersrv_gs2s::SleepType_Enum a4) {
            
        EXPECT_CALL(proxy->TimerReq,doTimerReq(a1,a2,a3,a4))
            .WillRepeatedly(Return(datatypes::gs2s::timersrv_gs2s::TimerSet_Response_Enum::OK));
    }

    void setTimerReqExpectNOK(datatypes::gs2s::timersrv_gs2s::TimerSet_Integer a1,
          datatypes::gs2s::timersrv_gs2s::StartPoint_Enum a2, datatypes::gs2s::timersrv_gs2s::TimerType_Enum a3,
          datatypes::gs2s::timersrv_gs2s::SleepType_Enum a4) {
            
        EXPECT_CALL(proxy->TimerReq,doTimerReq(a1,a2,a3,a4))
            .WillRepeatedly(Return(datatypes::gs2s::timersrv_gs2s::TimerSet_Response_Enum::NOK));
    }

    void setTimerReqExpectException(datatypes::gs2s::timersrv_gs2s::TimerSet_Integer a1,
          datatypes::gs2s::timersrv_gs2s::StartPoint_Enum a2, datatypes::gs2s::timersrv_gs2s::TimerType_Enum a3,
          datatypes::gs2s::timersrv_gs2s::SleepType_Enum a4) {
            
        EXPECT_CALL(proxy->TimerReq,doTimerReq(a1,a2,a3,a4))
            .WillRepeatedly(Throw(std::exception()));
    }

    void setUp() {      
        TimerS2SService::doSubscription();
    }
    MOCK_METHOD(void, doSubscription, (), (override));
};

TEST(TimerBasicController, constr) {
    std::condition_variable cv;
    std::mutex cv_m;

    std::shared_ptr<MockTimerS2SService> timerS2SSvc = std::make_shared<MockTimerS2SService>();
    TimerBasicController tbc(timerS2SSvc);

    EXPECT_CALL((*timerS2SSvc),doSubscription())
        .WillOnce([&cv,&timerS2SSvc ] {
            timerS2SSvc->setUp();
            cv.notify_all(); 
        });
    
    timerS2SSvc->start();

    datatypes::gs2s::timersrv_gs2s::TimerSt_Struct data;

    std::unique_lock<std::mutex> lk(cv_m);
    cv.wait_for(lk,std::chrono::seconds(1));

    timerS2SSvc->raiseTimer(data);

    MockFunction<void(TimerBasicController::TimerState)> mock_notifier;

    EXPECT_CALL(mock_notifier,Call)
        .WillOnce([] (TimerBasicController::TimerState v) {
            ASSERT_EQ(v,TimerBasicController::TimerState::TimerDefault);
        })
         .WillOnce([] (TimerBasicController::TimerState v) {
             ASSERT_EQ(v,TimerBasicController::TimerState::TimerTrigger);
        })
        .WillOnce([] (TimerBasicController::TimerState v) {
             ASSERT_EQ(v,TimerBasicController::TimerState::Timing);
        });
    
    tbc.setTimerNotifier(mock_notifier.AsStdFunction());


    timerS2SSvc->raiseTimer(data);
    data.WakeSrcexpired = datatypes::gs2s::timersrv_gs2s::TimerSt_WakeSrcexpired_Enum::WakeupSourceExpired;
    timerS2SSvc->raiseTimer(data);
    data.WakeSrcexpired = datatypes::gs2s::timersrv_gs2s::TimerSt_WakeSrcexpired_Enum::NoWakeupSourceExpired;
    timerS2SSvc->raiseTimer(data);
}

TEST(TimerBasicController, requestTimer) {
    std::condition_variable cv;
    std::mutex cv_m;

    std::shared_ptr<MockTimerS2SService> timerS2SSvc = std::make_shared<MockTimerS2SService>();
    TimerBasicController tbc(timerS2SSvc);

    EXPECT_CALL((*timerS2SSvc),doSubscription())
        .WillOnce([&cv,&timerS2SSvc] {
            timerS2SSvc->setUp();
            cv.notify_all(); 
        });
    
    timerS2SSvc->start();

    std::unique_lock<std::mutex> lk(cv_m);
    cv.wait_for(lk,std::chrono::seconds(1));

    int time = 60;

    timerS2SSvc->setTimerReqExpectOK(time,datatypes::gs2s::timersrv_gs2s::StartPoint_Enum::CountFromTriggered,
        datatypes::gs2s::timersrv_gs2s::TimerType_Enum::DurationTime,
        datatypes::gs2s::timersrv_gs2s::SleepType_Enum::NormalSleep);

    auto ret = tbc.requestTimer(time);
    ASSERT_EQ(ret, 0);
}

TEST(TimerBasicController, requestTimerNOK) {
    std::condition_variable cv;
    std::mutex cv_m;

    std::shared_ptr<MockTimerS2SService> timerS2SSvc = std::make_shared<MockTimerS2SService>();
    TimerBasicController tbc(timerS2SSvc);

    EXPECT_CALL((*timerS2SSvc),doSubscription())
        .WillOnce([&cv,&timerS2SSvc] {
            timerS2SSvc->setUp();
            cv.notify_all(); 
        });
    
    timerS2SSvc->start();

    std::unique_lock<std::mutex> lk(cv_m);
    cv.wait_for(lk,std::chrono::seconds(1));

    int time = 61;

    timerS2SSvc->setTimerReqExpectNOK(time,datatypes::gs2s::timersrv_gs2s::StartPoint_Enum::CountFromTriggered,
        datatypes::gs2s::timersrv_gs2s::TimerType_Enum::DurationTime,
        datatypes::gs2s::timersrv_gs2s::SleepType_Enum::NormalSleep);

    auto ret = tbc.requestTimer(time);
    ASSERT_EQ(ret, 1);

}

TEST(TimerBasicController, requestTimerException) {
    std::condition_variable cv;
    std::mutex cv_m;

    std::shared_ptr<MockTimerS2SService> timerS2SSvc = std::make_shared<MockTimerS2SService>();
    TimerBasicController tbc(timerS2SSvc);

    EXPECT_CALL((*timerS2SSvc),doSubscription())
        .WillOnce([&cv,&timerS2SSvc] {
            timerS2SSvc->setUp();
            cv.notify_all(); 
        });
    
    timerS2SSvc->start();

    std::unique_lock<std::mutex> lk(cv_m);
    cv.wait_for(lk,std::chrono::seconds(1));

    int time = 62;

    timerS2SSvc->setTimerReqExpectException(time,datatypes::gs2s::timersrv_gs2s::StartPoint_Enum::CountFromTriggered,
        datatypes::gs2s::timersrv_gs2s::TimerType_Enum::DurationTime,
        datatypes::gs2s::timersrv_gs2s::SleepType_Enum::NormalSleep);
    auto ret = tbc.requestTimer(time);
    ASSERT_EQ(ret, 1);
}

TEST(TimerBasicController, cancelActiveTimer) {
    std::condition_variable cv;
    std::mutex cv_m;

    std::shared_ptr<MockTimerS2SService> timerS2SSvc = std::make_shared<MockTimerS2SService>();
    TimerBasicController tbc(timerS2SSvc);

    EXPECT_CALL((*timerS2SSvc),doSubscription())
        .WillOnce([&cv,&timerS2SSvc] {
            timerS2SSvc->setUp();
            cv.notify_all(); 
        });
    
    timerS2SSvc->start();

    std::unique_lock<std::mutex> lk(cv_m);
    cv.wait_for(lk,std::chrono::seconds(1));

    int time = 0xFFFFFFFF;

    timerS2SSvc->setTimerReqExpectOK(time,datatypes::gs2s::timersrv_gs2s::StartPoint_Enum::CountFromTriggered,
        datatypes::gs2s::timersrv_gs2s::TimerType_Enum::DurationTime,
        datatypes::gs2s::timersrv_gs2s::SleepType_Enum::NormalSleep);

    auto ret = tbc.cancelActiveTimer();
    ASSERT_EQ(ret, 0);
}


}
}