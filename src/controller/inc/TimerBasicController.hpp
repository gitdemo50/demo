#ifndef GWM_TIMER_BASIC_CONTROLLER_HPP
#define GWM_TIMER_BASIC_CONTROLLER_HPP

#include <memory>

#include "logger/logger.hpp"

#include "TimerS2SProxy.hpp"

namespace gwm {
namespace controller {

using TimerS2SService = gwm::services::timer::proxy::TimerS2SService;

class TimerBasicController {
public:
    enum class TimerState { TimerDefault = 0, TimerTrigger = 1, Timing = 2 };

    using NotifierFn = std::function<void(TimerState)>;

    TimerBasicController(std::shared_ptr<TimerS2SService> service);

    MOCKABLE int requestTimer(int time);
    MOCKABLE int cancelActiveTimer();

    MOCKABLE void setTimerNotifier(NotifierFn notifier);
private:

    int transformResult(datatypes::gs2s::timersrv_gs2s::TimerSet_Response_Enum res);
    std::shared_ptr<TimerS2SService> _timerS2S;
    NotifierFn _notifier;
    gwm::platform::Logger _logger;
};

}}

#endif