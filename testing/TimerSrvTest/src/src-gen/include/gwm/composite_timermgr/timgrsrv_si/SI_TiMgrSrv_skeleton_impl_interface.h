/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  TiMgrSrvExe/include/gwm/composite_timermgr/timgrsrv_si/SI_TiMgrSrv_skeleton_impl_interface.h
 *        \brief  Skeleton implementation interface of service 'SI_TiMgrSrv'.
 *
 *      \details  This Service interface provides the timer manager request
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_socal
 *         Commit ID: 209b3c0234013e84e3170ee826c9a8c6022de194
 *********************************************************************************************************************/

#ifndef TIMGRSRVEXE_INCLUDE_GWM_COMPOSITE_TIMERMGR_TIMGRSRV_SI_SI_TIMGRSRV_SKELETON_IMPL_INTERFACE_H_
#define TIMGRSRVEXE_INCLUDE_GWM_COMPOSITE_TIMERMGR_TIMGRSRV_SI_SI_TIMGRSRV_SKELETON_IMPL_INTERFACE_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "amsr/socal/internal/events/skeleton_event_manager_interface.h"
#include "gwm/composite_timermgr/timgrsrv_si/si_timgrsrv_common.h"

namespace gwm {
namespace composite_timermgr {
namespace timgrsrv_si {
namespace internal {

/*!
 * \brief Skeleton implementation interface of service 'SI_TiMgrSrv'
 */
class SI_TiMgrSrvSkeletonImplInterface {
 public:
 /*!
   * \brief Define default constructor.
   * \pre -
   * \context App
   */
  SI_TiMgrSrvSkeletonImplInterface() noexcept = default;

  /*!
   * \brief Use default destructor
   */
  virtual ~SI_TiMgrSrvSkeletonImplInterface() noexcept = default;

 protected:

  /*!
   * \brief Use default move constructor
   * \pre -
   * \context App
   */
  SI_TiMgrSrvSkeletonImplInterface(SI_TiMgrSrvSkeletonImplInterface &&) noexcept = default;

  /*!
   * \brief Use default move assignment
   * \pre -
   * \context App
   */
  SI_TiMgrSrvSkeletonImplInterface &operator=(SI_TiMgrSrvSkeletonImplInterface &&) & noexcept = default;

  SI_TiMgrSrvSkeletonImplInterface(SI_TiMgrSrvSkeletonImplInterface const &) = delete;

  SI_TiMgrSrvSkeletonImplInterface &operator=(SI_TiMgrSrvSkeletonImplInterface const &) & = delete;

 public:

  // ---- Events ---------------------------------------------------------------------------------------------------

  // ---- Fields ---------------------------------------------------------------------------------------------------

  /*!
   * \brief Get the event manager object for the field notifier of field 'AlrmClkWkeEve'.
   * \details Field data type: ::gwm::composite_timermgr::timermgr_idt::AlrmClkWkeEve_Enum_Idt.
   * \return A binding-specific event management object/interface supporting field notifier updates.
   * \pre -
   * \context App
   */
   virtual ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::composite_timermgr::timermgr_idt::AlrmClkWkeEve_Enum_Idt>* GetFieldNotifierAlrmClkWkeEve() noexcept = 0;
};

} // namespace internal
}  // namespace timgrsrv_si
}  // namespace composite_timermgr
}  // namespace gwm

#endif  // TIMGRSRVEXE_INCLUDE_GWM_COMPOSITE_TIMERMGR_TIMGRSRV_SI_SI_TIMGRSRV_SKELETON_IMPL_INTERFACE_H_

