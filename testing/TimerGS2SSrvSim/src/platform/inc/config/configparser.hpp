#ifndef GWM_PLATFORM_CONFIG_PARSER_HPP
#define GWM_PLATFORM_CONFIG_PARSER_HPP

/*
*   COPY RIGHT NOTICE
*   
*
*
*/

/*
*
*   Config Parser Interface
*
*/

#include <string>
#include <map>
#include <fstream>
#include <sstream>


namespace gwm {
namespace platform {
namespace parser {

class ConfigINIParser {
 public:
    explicit ConfigINIParser(std::string path)
    : filePath_(path) {
    }

    bool parse() {
        bool retValue { false };
        std::ifstream ifs(filePath_, std::ifstream::in);

        if (true == ifs.is_open()) {
            std::string sectionName;

            while (false == ifs.eof()) {
                std::string lineBuffer;
                std::getline(ifs, lineBuffer);

                if (std::string::npos != lineBuffer.find(';')) {
                    lineBuffer.erase(lineBuffer.find(';'));
                }

                // Section name
                size_t startLoc = lineBuffer.find('[');
                if (std::string::npos != startLoc) {
                    sectionName = lineBuffer.substr(startLoc + 1U, lineBuffer.rfind(']') - startLoc - 1U);
                    continue;
                }

                if (lineBuffer.length() > 0) {
                    // trim lines
                    std::string trimChars = "\t\n\v\f\r ";
                    lineBuffer.erase(0, lineBuffer.find_first_not_of(trimChars));
                    lineBuffer.erase(lineBuffer.find_last_not_of(trimChars) + 1);

                    // Properties
                    std::stringstream str(lineBuffer);
                    std::string keyName, temp;
                    int value;
                    str >> keyName >> temp >> value;

                    data_[sectionName][keyName] = value;
                }
            }

            if(true == ifs.eof()) {
                retValue = true;
            }

            ifs.close();
        }

        return retValue;
    }

    bool getValue(const std::string& section, const std::string& key, uint32_t& value) {
        bool retValue { true };

        try {
            value = data_.at(section).at(key);
        }  catch (const std::exception& ex) {
            retValue = false;
        } catch (...) {
            retValue = false;
        }

        return retValue;
    }

 private:
    std::string filePath_;
    std::map<std::string, std::map<std::string, int32_t> > data_;
    static const uint32_t lineMaxLength_ { 256 };
};

}  /* namespace parser */
}  /* namespace platform */
}  /* namespace gwm */

#endif  /* GWM_PLATFORM_CONFIG_PARSER_HPP */