/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/include/gwm/pt_powersys/vehchrgnctrlsrv_si/si_vehchrgnctrlsrv_common.h
 *        \brief  Header for service 'SI_VehChrgnCtrlSrv'.
 *
 *      \details  The service interface provides vehicle charging control
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_socal
 *         Commit ID: 209b3c0234013e84e3170ee826c9a8c6022de194
 *********************************************************************************************************************/

#ifndef DENPENDENCYSRVEXE_INCLUDE_GWM_PT_POWERSYS_VEHCHRGNCTRLSRV_SI_SI_VEHCHRGNCTRLSRV_COMMON_H_
#define DENPENDENCYSRVEXE_INCLUDE_GWM_PT_POWERSYS_VEHCHRGNCTRLSRV_SI_SI_VEHCHRGNCTRLSRV_COMMON_H_

/*!
 * \trace SPEC-4980247, SPEC-4980248, SPEC-5951130, SPEC-4980251
 */
/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "ara/com/types.h"
#include "gwm/pt_powersys/powersys_idt/impl_type_chargingtypeset_chrgmod_enum_idt.h"
#include "gwm/pt_powersys/powersys_idt/impl_type_chargingtypeset_response_enum_idt.h"
#include "gwm/pt_powersys/powersys_idt/impl_type_vehchrgnctrl_clntid_enum_idt.h"
#include "gwm/pt_powersys/powersys_idt/impl_type_vehchrgnctrl_command_enum_idt.h"
#include "gwm/pt_powersys/powersys_idt/impl_type_vehchrgnctrl_response_enum_idt.h"
#include "gwm/pt_powersys/vehchrgnctrlsrv_si/SI_VehChrgnCtrlSrv.h"

#endif  // DENPENDENCYSRVEXE_INCLUDE_GWM_PT_POWERSYS_VEHCHRGNCTRLSRV_SI_SI_VEHCHRGNCTRLSRV_COMMON_H_
