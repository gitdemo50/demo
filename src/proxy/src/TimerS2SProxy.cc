#include "TimerS2SProxy.hpp"
namespace gwm {
namespace services {
namespace timer {

namespace proxy {

TimerS2SService::TimerS2SService() : serviceSpec("TimerSrvExe/RootSwComponentPrototype/AdaptiveRequiredPortType_GS2STimerSrv"_sv),
    SensorProxy((gwm::platform::Logger("TimerS2SService"))) {
}

TimerS2SService::~TimerS2SService() {

}

//同时设置所有依赖的接口的 field的回调函数
void TimerS2SService::doSubscription() {
    logger.LogVerbose() << "TimerS2SService doSubscription " << proxy.get();
    ev_timerSt.subscribe();
}

//同时初始化同一个依赖的 服务接口内的所有对象
void TimerS2SService::initProxyItems() {
    logger.LogVerbose() << "TimerS2SService initProxyItems ";
    
    timerReq.set(&proxy->TimerReq);
    ev_timerSt.set(&proxy->TimerSt);
}


}}}}