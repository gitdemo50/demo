/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  TimerSrvExe/include/gwm/platform_timer/timer_idt/impl_type_timerst_struct_idt.h
 *        \brief
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_modelleddatatypes_api
 *         Commit ID: d9332226824a4a6c4616e2d948d41c862d192343
 *********************************************************************************************************************/

#ifndef TIMERSRVEXE_INCLUDE_GWM_PLATFORM_TIMER_TIMER_IDT_IMPL_TYPE_TIMERST_STRUCT_IDT_H_
#define TIMERSRVEXE_INCLUDE_GWM_PLATFORM_TIMER_TIMER_IDT_IMPL_TYPE_TIMERST_STRUCT_IDT_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include <array>
#include <cstdint>
#include <map>
#include <string>
#include <vector>
#include "gwm/platform_timer/timer_idt/impl_type_timerst_wakesource_enum_idt.h"
#include "gwm/platform_timer/timer_idt/impl_type_timerst_wakesrcexpired_enum_idt.h"

namespace gwm {
namespace platform_timer {
namespace timer_idt {

// VECTOR Disable AutosarC++17_10-A12.6.1: MD_MDTG_A12.6.1_GeneratedStructUninitializedMembers
// VECTOR Disable AutosarC++17_10-M8.5.1: MD_MDTG_M8.5.1_GeneratedStructUninitializedMembers
/*!
 * \brief Type TimerSt_Struct_Idt.
 * \remark generated
 * \trace SPEC-5951372
 *
 * IMPLEMENTATION-DATA-TYPE /DataTypes/ImplementationDataTypes/TimerSt_Struct_Idt
 */
struct TimerSt_Struct_Idt {
  using WakeSrcexpired_Idt_generated_type = gwm::platform_timer::timer_idt::TimerSt_WakeSrcexpired_Enum_Idt;
  using WakeSource_Idt_generated_type = gwm::platform_timer::timer_idt::TimerSt_WakeSource_Enum_Idt;

  WakeSrcexpired_Idt_generated_type WakeSrcexpired_Idt;
  WakeSource_Idt_generated_type WakeSource_Idt;
};
// VECTOR Enable AutosarC++17_10-A12.6.1
// VECTOR Enable AutosarC++17_10-M8.5.1

/*!
 * \brief Compare for equality with another TimerSt_Struct_Idt instance.
 */
inline bool operator==(TimerSt_Struct_Idt const& l,
                       TimerSt_Struct_Idt const& r) noexcept {
  return (&l == &r) || ((l.WakeSrcexpired_Idt == r.WakeSrcexpired_Idt)
                         && (l.WakeSource_Idt == r.WakeSource_Idt)
  );
}

/*!
 * \brief Compare for inequality with another TimerSt_Struct_Idt instance.
 */
inline bool operator!=(TimerSt_Struct_Idt const& l,
                       TimerSt_Struct_Idt const& r) noexcept {
  return !(l == r);
}

}  // namespace timer_idt
}  // namespace platform_timer
}  // namespace gwm

#endif  // TIMERSRVEXE_INCLUDE_GWM_PLATFORM_TIMER_TIMER_IDT_IMPL_TYPE_TIMERST_STRUCT_IDT_H_
