/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/include/amsr/someip_protocol/internal/method_serialization/gwm_pt_batterypacksys_bmsstsrv_si/fields/serializer_Response_BMSIntelTempMgtStGet.h
 *        \brief  SOME/IP packet serializer of service 'SI_BmsStSrv'.
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_someipprotocol
 *         Commit ID: 0e137b9e6356987a6d2839c8d79c286219b68d60
 *********************************************************************************************************************/

#ifndef DENPENDENCYSRVEXE_INCLUDE_AMSR_SOMEIP_PROTOCOL_INTERNAL_METHOD_SERIALIZATION_GWM_PT_BATTERYPACKSYS_BMSSTSRV_SI_FIELDS_serializer_Response_BMSIntelTempMgtStGet_h_
#define DENPENDENCYSRVEXE_INCLUDE_AMSR_SOMEIP_PROTOCOL_INTERNAL_METHOD_SERIALIZATION_GWM_PT_BATTERYPACKSYS_BMSSTSRV_SI_FIELDS_serializer_Response_BMSIntelTempMgtStGet_h_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "amsr/someip_protocol/internal/dataprototype_serialization/gwm_pt_batterypacksys_bmsstsrv_si/fields/serializer_BMSIntelTempMgtSt.h"
#include "ara/log/logging.h"
#include "osabstraction/io/io_buffer.h"
#include "someip-protocol/internal/serialization/ser_common.h"
#include "someip-protocol/internal/serialization/writer.h"
#include "vac/memory/memory_buffer.h"

namespace amsr {
namespace someip_protocol {
namespace internal {
namespace method_serializer {
namespace gwm_pt_batterypacksys_bmsstsrv_si {
namespace fields {

/*!
 * \brief Serializer for a SOME/IP packet of method '/ServiceInterfaces/SI_BmsStSrv/BMSIntelTempMgtSt'
 *        of service interface '/ServiceInterfaces/SI_BmsStSrv'.
 */
class SerializerResponseOkBMSIntelTempMgtStGet final {
 public:

  /*!
   * \brief Type alias for the concrete memory buffer type.
   */
   using BufferPtrType = ::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer>;

  /*!
   * \brief Returns the required buffer size to serialize the SOME/IP packet payload of method 'BMSIntelTempMgtStGet'.
   *
   * \tparam      Input Type of data structure storing all in-/output arguments to be serialized.
   *              Type must support member initializer lists. Typically a struct or a std::tuple is used.
   * \param[in]   input Parameter to serialize.
   * \return      Calculated buffer size for serialization.
   * \pre         -
   * \context     Reactor|App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  template <typename Input>
  constexpr static std::size_t GetRequiredBufferSize(Input const& input) noexcept {
    std::size_t required_buffer_size{0};


    // Get required buffer size for the argument 'BMSIntelTempMgtSt' of type '/DataTypes/ImplementationDataTypes/BMSIntelTempMgtSt_Enum_Idt'
    required_buffer_size += dataprototype_serializer::gwm_pt_batterypacksys_bmsstsrv_si::fields::SerializerBMSIntelTempMgtSt::GetRequiredBufferSize(
        input);

    return required_buffer_size;
  }

  /*!
   * \brief Serialize the SOME/IP packet payload of method 'BMSIntelTempMgtStGet'.
   *
   * \tparam Input Type of data structure storing all in-/output arguments to be serialized.
   *         Type must support member initializer lists. Typically a struct or a std::tuple is used.
   * \param[out] logger Reference to the logger used for logging during serialization.
   * \param[in, out] writer Reference to the byte stream writer.
   * \param[in] input Parameter to serialize.
   */
  template <typename Input>
  static void Serialize(ara::log::Logger& logger, serialization::Writer& writer, Input const& input) noexcept {

    // Serialize the argument 'BMSIntelTempMgtSt' of type '/DataTypes/ImplementationDataTypes/BMSIntelTempMgtSt_Enum_Idt'
    dataprototype_serializer::gwm_pt_batterypacksys_bmsstsrv_si::fields::SerializerBMSIntelTempMgtSt::Serialize(
        writer, input);

    logger.LogDebug([](ara::log::LogStream& s) { s << "Serializer done."; }, __func__, __LINE__);
  }
};

}  // namespace fields
}  // namespace gwm_pt_batterypacksys_bmsstsrv_si
}  // namespace method_serializer
}  // namespace internal
}  // namespace someip_protocol
}  // namespace amsr

#endif  // DENPENDENCYSRVEXE_INCLUDE_AMSR_SOMEIP_PROTOCOL_INTERNAL_METHOD_SERIALIZATION_GWM_PT_BATTERYPACKSYS_BMSSTSRV_SI_FIELDS_serializer_Response_BMSIntelTempMgtStGet_h_

