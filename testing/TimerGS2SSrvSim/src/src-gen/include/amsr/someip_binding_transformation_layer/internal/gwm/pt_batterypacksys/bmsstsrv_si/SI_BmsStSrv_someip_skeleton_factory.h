/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/include/amsr/someip_binding_transformation_layer/internal/gwm/pt_batterypacksys/bmsstsrv_si/SI_BmsStSrv_someip_skeleton_factory.h
 *        \brief  SOME/IP skeleton factory for service 'SI_BmsStSrv'
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_someipbinding
 *         Commit ID: 43b33f0283d4d61385b98a3599688499abd86a75
 *********************************************************************************************************************/

#ifndef DENPENDENCYSRVEXE_INCLUDE_AMSR_SOMEIP_BINDING_TRANSFORMATION_LAYER_INTERNAL_GWM_PT_BATTERYPACKSYS_BMSSTSRV_SI_SI_BMSSTSRV_SOMEIP_SKELETON_FACTORY_H_
#define DENPENDENCYSRVEXE_INCLUDE_AMSR_SOMEIP_BINDING_TRANSFORMATION_LAYER_INTERNAL_GWM_PT_BATTERYPACKSYS_BMSSTSRV_SI_SI_BMSSTSRV_SOMEIP_SKELETON_FACTORY_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "amsr/someip_binding_transformation_layer/internal/gwm/pt_batterypacksys/bmsstsrv_si/SI_BmsStSrv_skeleton_someip_binding.h"
#include "gwm/pt_batterypacksys/bmsstsrv_si/si_bmsstsrv_skeleton.h"

namespace amsr {
namespace someip_binding_transformation_layer {
namespace internal {

namespace gwm {
namespace pt_batterypacksys {
namespace bmsstsrv_si {

/*!
 * \brief SOME/IP skeleton factory for the service interface 'SI_BmsStSrv'
 * \remark generated
 */
// VECTOR NL AutosarC++17_10-M3.4.1: MD_SOMEIPBINDING_AutosarC++17_10-M3.4.1_CanBeDeclaredLocallyInPrimaryFile
using SI_BmsStSrvSomeIpSkeletonFactory = ::amsr::someip_binding_transformation_layer::internal::AraComSomeIpSkeletonFactory<::gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton, SI_BmsStSrvSkeletonSomeIpBinding>;

}  // namespace internal
}  // namespace someip_binding_transformation_layer
}  // namespace amsr
}  // namespace bmsstsrv_si
}  // namespace pt_batterypacksys
}  // namespace gwm

#endif  // DENPENDENCYSRVEXE_INCLUDE_AMSR_SOMEIP_BINDING_TRANSFORMATION_LAYER_INTERNAL_GWM_PT_BATTERYPACKSYS_BMSSTSRV_SI_SI_BMSSTSRV_SOMEIP_SKELETON_FACTORY_H_

