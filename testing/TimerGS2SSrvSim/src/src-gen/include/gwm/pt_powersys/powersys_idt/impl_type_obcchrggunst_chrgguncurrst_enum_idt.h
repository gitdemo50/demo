/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/include/gwm/pt_powersys/powersys_idt/impl_type_obcchrggunst_chrgguncurrst_enum_idt.h
 *        \brief
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_modelleddatatypes_api
 *         Commit ID: d9332226824a4a6c4616e2d948d41c862d192343
 *********************************************************************************************************************/

#ifndef DENPENDENCYSRVEXE_INCLUDE_GWM_PT_POWERSYS_POWERSYS_IDT_IMPL_TYPE_OBCCHRGGUNST_CHRGGUNCURRST_ENUM_IDT_H_
#define DENPENDENCYSRVEXE_INCLUDE_GWM_PT_POWERSYS_POWERSYS_IDT_IMPL_TYPE_OBCCHRGGUNST_CHRGGUNCURRST_ENUM_IDT_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include <array>
#include <cstdint>
#include <map>
#include <string>
#include <vector>

namespace gwm {
namespace pt_powersys {
namespace powersys_idt {

/*!
 * \brief Type OBCChrgGunSt_ChrgGunCurrSt_Enum_Idt.
 * \remark generated
 * \trace SPEC-5951372
 *
 * IMPLEMENTATION-DATA-TYPE /DataTypes/ImplementationDataTypes/OBCChrgGunSt_ChrgGunCurrSt_Enum_Idt
 */
enum class OBCChrgGunSt_ChrgGunCurrSt_Enum_Idt : std::uint8_t {
  Default = 0,
  RC1500ChrgrInputCurr10A = 1,
  RC680ChrgrInputCurr16A = 2,
  RC220ChrgrInputCurr32A = 3,
  RC100ChrgrInputCurr63A = 4,
  RC2700ChrgrInputCurr10A = 5,
  RC2000ChrgrInputCurr16A = 6,
  RC1000ChrgrInputCurr32A = 7,
  RC470ChrgrInputCurr63A = 8,
  Reserved1 = 9,
  Reserved2 = 10,
  Reserved3 = 11,
  Reserved4 = 12,
  Reserved5 = 13,
  Reserved6 = 14,
  Reserved7 = 15
};

}  // namespace powersys_idt
}  // namespace pt_powersys
}  // namespace gwm

#endif  // DENPENDENCYSRVEXE_INCLUDE_GWM_PT_POWERSYS_POWERSYS_IDT_IMPL_TYPE_OBCCHRGGUNST_CHRGGUNCURRST_ENUM_IDT_H_
