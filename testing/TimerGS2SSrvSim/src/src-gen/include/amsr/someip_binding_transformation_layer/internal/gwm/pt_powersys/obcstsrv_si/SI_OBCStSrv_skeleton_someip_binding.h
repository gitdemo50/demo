/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/include/amsr/someip_binding_transformation_layer/internal/gwm/pt_powersys/obcstsrv_si/SI_OBCStSrv_skeleton_someip_binding.h
 *        \brief  SOME/IP binding of service skeleton for service 'SI_OBCStSrv'
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_someipbinding
 *         Commit ID: 43b33f0283d4d61385b98a3599688499abd86a75
 *********************************************************************************************************************/

#ifndef DENPENDENCYSRVEXE_INCLUDE_AMSR_SOMEIP_BINDING_TRANSFORMATION_LAYER_INTERNAL_GWM_PT_POWERSYS_OBCSTSRV_SI_SI_OBCSTSRV_SKELETON_SOMEIP_BINDING_H_
#define DENPENDENCYSRVEXE_INCLUDE_AMSR_SOMEIP_BINDING_TRANSFORMATION_LAYER_INTERNAL_GWM_PT_POWERSYS_OBCSTSRV_SI_SI_OBCSTSRV_SKELETON_SOMEIP_BINDING_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "amsr/someip_binding_transformation_layer/internal/gwm/pt_powersys/obcstsrv_si/SI_OBCStSrv_skeleton_someip_event_manager.h"
#include "amsr/someip_binding_transformation_layer/internal/gwm/pt_powersys/obcstsrv_si/SI_OBCStSrv_skeleton_someip_methods.h"
#include "gwm/pt_powersys/obcstsrv_si/SI_OBCStSrv_skeleton_impl_interface.h"
#include "gwm/pt_powersys/obcstsrv_si/si_obcstsrv_skeleton.h"
#include "someip_binding/internal/server_interface.h"
#include "someip_binding/internal/server_manager_interface.h"

namespace amsr {
namespace someip_binding_transformation_layer {
namespace internal {

namespace gwm {
namespace pt_powersys {
namespace obcstsrv_si {

/*!
 * \brief SOME/IP skeleton binding of Service 'SI_OBCStSrv'.
 * \details Handles serialization and deserialization of all method calls, events, etc.
 * \remark generated
 */
class SI_OBCStSrvSkeletonSomeIpBinding final :
    public ::gwm::pt_powersys::obcstsrv_si::internal::SI_OBCStSrvSkeletonImplInterface,
    public ::amsr::someip_binding::internal::ServerInterface {
 public:
  /*!
   * \brief Type alias for used skeleton frontend.
   */
  using SkeletonFrontend = ::gwm::pt_powersys::obcstsrv_si::skeleton::SI_OBCStSrvSkeleton;
  /*!
   * \brief Generated SOME/IP related service ID.
   */
  static constexpr ::amsr::someip_protocol::internal::ServiceId kServiceId{0x4004U};

  /*!
   * \brief SOME/IP major version of this service interface deployment.
   */
  static constexpr ::amsr::someip_protocol::internal::MajorVersion kMajorVersion{1U};

  /*!
   * \brief Constructor of SI_OBCStSrvSkeletonSomeIpBinding.
   * \details Constructed when the service is offered.
   *
   * \param[in] instance_id                    SOME/IP service instance ID offered by this skeleton.
   * \param[in] someip_binding_server_manager  Reference to the related SOME/IP transport binding manager.
   * \param[in] skeleton                       Reference to the related frontend skeleton.
   * \pre         -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  SI_OBCStSrvSkeletonSomeIpBinding(::amsr::someip_protocol::internal::InstanceId const instance_id,
                                                         ::amsr::someip_binding::internal::ServerManagerInterface& someip_binding_server_manager,
                                                         ::gwm::pt_powersys::obcstsrv_si::skeleton::SI_OBCStSrvSkeleton& skeleton);

  /*!
   * \brief Default destructor must de-register the implementation from the SomeIpBinding.
   * \details Destroyed when the service is stopped.
   * \pre         -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR NC AutosarC++17_10-A15.5.1: MD_SOMEIPBINDING_AutosarC++17_10-A15.4.2_A15.5.1_A15.5.2_A15.5.3_GoogleTest
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  ~SI_OBCStSrvSkeletonSomeIpBinding() noexcept override  = default;

  /*!
   * \brief Getter for accessing the related frontend skeleton.
   * \pre         -
   * \context     Callback
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR Next Line AutosarC++17_10-M9.3.3: MD_SOMEIPBINDING_AutosarC++17_10-M9.3.3_returns_nonconst
  SkeletonFrontend& GetSkeleton() noexcept { return skeleton_; }

  /*!
   * \brief Receive handler for method requests.
   *
   * \param[in] header Deserialized SOME/IP message header.
   * \param[in] packet Serialized SOME/IP Method Request [SOME/IP header + Payload]
   * \pre         -
   * \context     Reactor
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  void HandleMethodRequest(::amsr::someip_protocol::internal::SomeIpMessageHeader const& header,
      ::amsr::someip_protocol::internal::SomeIpMessage packet) override;

  /*!
   * \brief Forward a prepared packet to the SomeIpBinding.
   * \param[in] packet the serialized response packet.
   * \pre         -
   * \context     Callback
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  void SendMethodResponse(::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet);

  /*!
   * \brief Send a SOME/IP error response packet. Used in case of:
   *  - Infrastructural checks failed (protocol version, return code, message type).
   *  - Deserialization failed.
   *  - Serialization of method responses failed.
   *  - Service / Method is not implemented.
   *
   * \tparam ReturnCode The error return code as template parameter to make a compile-time check.
   * \param[in] request_header The unmodified SOME/IP header.
   * \pre         -
   * \context     Reactor
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  template <typename ::amsr::someip_protocol::internal::ReturnCode ReturnCode>
  void SendErrorResponse(::amsr::someip_protocol::internal::SomeIpMessageHeader const& request_header) noexcept {
    static_assert((static_cast<::amsr::someip_protocol::internal::SomeIpReturnCode>(ReturnCode) >= ::amsr::someip_protocol::internal::SomeIpReturnCode::kNotOk) &&
                      (static_cast<::amsr::someip_protocol::internal::SomeIpReturnCode>(ReturnCode) <= ::amsr::someip_protocol::internal::SomeIpReturnCode::kRangeServiceErrorsEnd),
                  "Return code for errors must be in the range of 0x01 .. 0x5E.");
    someip_binding_server_manager_.SendErrorResponse(ReturnCode, instance_id_, request_header);
  }

  /*!
   * \brief Send an error response if the SOME/IP request is malformed.
   * \details This method is invoked when the return code of the error response is only known during runtime.
   * \param[in] return_code    The return code to send.
   * \param[in] request_header The header taken from the request.
   * \pre         -
   * \context     Reactor
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  void SendErrorResponse(::amsr::someip_protocol::internal::ReturnCode const return_code,
                         ::amsr::someip_protocol::internal::SomeIpMessageHeader const& request_header) {
    someip_binding_server_manager_.SendErrorResponse(return_code, instance_id_, request_header);
  }

  // ---- Events --------------------------------------------------------------------------------------------------- */

  /*!
   * \brief Get the event manager object for the SOME/IP binding service event 'OBCActT'.
   * \details Event sample type: ::gwm::pt_powersys::powersys_idt::OBCActT_Float_Idt.
   * \return The SOME/IP binding specific event management object/interface supporting event transmission.
   *
   * \pre         -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  SI_OBCStSrvSkeletonSomeIpEventManagerOBCActT* GetEventManagerOBCActT() noexcept override;

  /*!
   * \brief Get the event manager object for the SOME/IP binding service event 'OBCChrgLim'.
   * \details Event sample type: ::gwm::pt_powersys::powersys_idt::OBCChrgLim_Struct_Idt.
   * \return The SOME/IP binding specific event management object/interface supporting event transmission.
   *
   * \pre         -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  SI_OBCStSrvSkeletonSomeIpEventManagerOBCChrgLim* GetEventManagerOBCChrgLim() noexcept override;

  /*!
   * \brief Get the event manager object for the SOME/IP binding service event 'OBCDchrOutpI'.
   * \details Event sample type: ::gwm::pt_powersys::powersys_idt::OBCDchrOutpI_Float_Idt.
   * \return The SOME/IP binding specific event management object/interface supporting event transmission.
   *
   * \pre         -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  SI_OBCStSrvSkeletonSomeIpEventManagerOBCDchrOutpI* GetEventManagerOBCDchrOutpI() noexcept override;

  /*!
   * \brief Get the event manager object for the SOME/IP binding service event 'OBCDchrOutpU'.
   * \details Event sample type: ::gwm::pt_powersys::powersys_idt::OBCDchrOutpU_Float_Idt.
   * \return The SOME/IP binding specific event management object/interface supporting event transmission.
   *
   * \pre         -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  SI_OBCStSrvSkeletonSomeIpEventManagerOBCDchrOutpU* GetEventManagerOBCDchrOutpU() noexcept override;

  /*!
   * \brief Get the event manager object for the SOME/IP binding service event 'OBCInptAcI'.
   * \details Event sample type: ::gwm::pt_powersys::powersys_idt::OBCInptAcI_Float_Idt.
   * \return The SOME/IP binding specific event management object/interface supporting event transmission.
   *
   * \pre         -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  SI_OBCStSrvSkeletonSomeIpEventManagerOBCInptAcI* GetEventManagerOBCInptAcI() noexcept override;

  /*!
   * \brief Get the event manager object for the SOME/IP binding service event 'OBCInptAcU'.
   * \details Event sample type: ::gwm::pt_powersys::powersys_idt::OBCInptAcU_Float_Idt.
   * \return The SOME/IP binding specific event management object/interface supporting event transmission.
   *
   * \pre         -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  SI_OBCStSrvSkeletonSomeIpEventManagerOBCInptAcU* GetEventManagerOBCInptAcU() noexcept override;

  /*!
   * \brief Get the event manager object for the SOME/IP binding service event 'OBCOuptDcI'.
   * \details Event sample type: ::gwm::pt_powersys::powersys_idt::OBCOuptDcI_Float_Idt.
   * \return The SOME/IP binding specific event management object/interface supporting event transmission.
   *
   * \pre         -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  SI_OBCStSrvSkeletonSomeIpEventManagerOBCOuptDcI* GetEventManagerOBCOuptDcI() noexcept override;

  /*!
   * \brief Get the event manager object for the SOME/IP binding service event 'OBCOuptDcU'.
   * \details Event sample type: ::gwm::pt_powersys::powersys_idt::OBCOuptDcU_Float_Idt.
   * \return The SOME/IP binding specific event management object/interface supporting event transmission.
   *
   * \pre         -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  SI_OBCStSrvSkeletonSomeIpEventManagerOBCOuptDcU* GetEventManagerOBCOuptDcU() noexcept override;

  // ---- Fields ---------------------------------------------------------------------------------------------------

  /*!
   * \brief Get the event manager object for the SOME/IP binding field notifier of field 'OBCChrgGunSt'.
   * \details Field data type: ::gwm::pt_powersys::powersys_idt::OBCChrgGunSt_Struct_Idt.
   * \return The SOME/IP binding specific event management object/interface supporting field notifier updates.
   *
   * \pre         -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  SI_OBCStSrvSkeletonSomeIpFieldNotifierOBCChrgGunSt* GetFieldNotifierOBCChrgGunSt() noexcept override;

  /*!
   * \brief Get the event manager object for the SOME/IP binding field notifier of field 'OBCErrSt'.
   * \details Field data type: ::gwm::pt_powersys::powersys_idt::OBCErrSt_Struct_Idt.
   * \return The SOME/IP binding specific event management object/interface supporting field notifier updates.
   *
   * \pre         -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  SI_OBCStSrvSkeletonSomeIpFieldNotifierOBCErrSt* GetFieldNotifierOBCErrSt() noexcept override;

  /*!
   * \brief Get the event manager object for the SOME/IP binding field notifier of field 'OBCHVILSt'.
   * \details Field data type: ::gwm::pt_powersys::powersys_idt::OBCHVILSt_Enum_Idt.
   * \return The SOME/IP binding specific event management object/interface supporting field notifier updates.
   *
   * \pre         -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  SI_OBCStSrvSkeletonSomeIpFieldNotifierOBCHVILSt* GetFieldNotifierOBCHVILSt() noexcept override;

  /*!
   * \brief Get the event manager object for the SOME/IP binding field notifier of field 'OBCSt'.
   * \details Field data type: ::gwm::pt_powersys::powersys_idt::OBCSt_Enum_Idt.
   * \return The SOME/IP binding specific event management object/interface supporting field notifier updates.
   *
   * \pre         -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  SI_OBCStSrvSkeletonSomeIpFieldNotifierOBCSt* GetFieldNotifierOBCSt() noexcept override;

  /*!
   * \brief Get the event manager object for the SOME/IP binding field notifier of field 'OBCV2XSt'.
   * \details Field data type: ::gwm::pt_powersys::powersys_idt::OBCV2XSt_Enum_Idt.
   * \return The SOME/IP binding specific event management object/interface supporting field notifier updates.
   *
   * \pre         -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  SI_OBCStSrvSkeletonSomeIpFieldNotifierOBCV2XSt* GetFieldNotifierOBCV2XSt() noexcept override;

 private:
  // SOME/IP instance ID offered by this skeleton
  ::amsr::someip_protocol::internal::InstanceId instance_id_;

  // Related SOME/IP binding
  ::amsr::someip_binding::internal::ServerManagerInterface& someip_binding_server_manager_;

  // Reference to the related frontend skeleton
  ::gwm::pt_powersys::obcstsrv_si::skeleton::SI_OBCStSrvSkeleton& skeleton_;

  // ---- Methods ------------------------------------------------------------------------------------------

  // ---- Event manager --------------------------------------------------------------------------------------------

  // Event manager for skeleton event 'OBCActT'
  SI_OBCStSrvSkeletonSomeIpEventManagerOBCActT event_manager_OBCActT_;

  // Event manager for skeleton event 'OBCChrgLim'
  SI_OBCStSrvSkeletonSomeIpEventManagerOBCChrgLim event_manager_OBCChrgLim_;

  // Event manager for skeleton event 'OBCDchrOutpI'
  SI_OBCStSrvSkeletonSomeIpEventManagerOBCDchrOutpI event_manager_OBCDchrOutpI_;

  // Event manager for skeleton event 'OBCDchrOutpU'
  SI_OBCStSrvSkeletonSomeIpEventManagerOBCDchrOutpU event_manager_OBCDchrOutpU_;

  // Event manager for skeleton event 'OBCInptAcI'
  SI_OBCStSrvSkeletonSomeIpEventManagerOBCInptAcI event_manager_OBCInptAcI_;

  // Event manager for skeleton event 'OBCInptAcU'
  SI_OBCStSrvSkeletonSomeIpEventManagerOBCInptAcU event_manager_OBCInptAcU_;

  // Event manager for skeleton event 'OBCOuptDcI'
  SI_OBCStSrvSkeletonSomeIpEventManagerOBCOuptDcI event_manager_OBCOuptDcI_;

  // Event manager for skeleton event 'OBCOuptDcU'
  SI_OBCStSrvSkeletonSomeIpEventManagerOBCOuptDcU event_manager_OBCOuptDcU_;

  // ---- Field manager --------------------------------------------------------------------------------------------

  // ---- Field 'OBCChrgGunSt' ----

  // Field notifier for the the skeleton field 'OBCChrgGunSt'
  SI_OBCStSrvSkeletonSomeIpFieldNotifierOBCChrgGunSt field_notifier_OBCChrgGunSt_;

  // Field method manager for method Get of skeleton field 'OBCChrgGunSt'
  fields::SkeletonOBCChrgGunStGet field_manager_OBCChrgGunSt_get_;

  // ---- Field 'OBCErrSt' ----

  // Field notifier for the the skeleton field 'OBCErrSt'
  SI_OBCStSrvSkeletonSomeIpFieldNotifierOBCErrSt field_notifier_OBCErrSt_;

  // Field method manager for method Get of skeleton field 'OBCErrSt'
  fields::SkeletonOBCErrStGet field_manager_OBCErrSt_get_;

  // ---- Field 'OBCHVILSt' ----

  // Field notifier for the the skeleton field 'OBCHVILSt'
  SI_OBCStSrvSkeletonSomeIpFieldNotifierOBCHVILSt field_notifier_OBCHVILSt_;

  // Field method manager for method Get of skeleton field 'OBCHVILSt'
  fields::SkeletonOBCHVILStGet field_manager_OBCHVILSt_get_;

  // ---- Field 'OBCSt' ----

  // Field notifier for the the skeleton field 'OBCSt'
  SI_OBCStSrvSkeletonSomeIpFieldNotifierOBCSt field_notifier_OBCSt_;

  // Field method manager for method Get of skeleton field 'OBCSt'
  fields::SkeletonOBCStGet field_manager_OBCSt_get_;

  // ---- Field 'OBCV2XSt' ----

  // Field notifier for the the skeleton field 'OBCV2XSt'
  SI_OBCStSrvSkeletonSomeIpFieldNotifierOBCV2XSt field_notifier_OBCV2XSt_;

  // Field method manager for method Get of skeleton field 'OBCV2XSt'
  fields::SkeletonOBCV2XStGet field_manager_OBCV2XSt_get_;
};

}  // namespace internal
}  // namespace someip_binding_transformation_layer
}  // namespace amsr
}  // namespace obcstsrv_si
}  // namespace pt_powersys
}  // namespace gwm

#endif  // DENPENDENCYSRVEXE_INCLUDE_AMSR_SOMEIP_BINDING_TRANSFORMATION_LAYER_INTERNAL_GWM_PT_POWERSYS_OBCSTSRV_SI_SI_OBCSTSRV_SKELETON_SOMEIP_BINDING_H_

