/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/include/amsr/someip_binding_transformation_layer/internal/gwm/pt_batterypacksys/bmsstsrv_si/SI_BmsStSrv_skeleton_someip_event_manager.h
 *        \brief  SOME/IP skeleton event handling for events and field notifications of service 'SI_BmsStSrv'
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_someipbinding
 *         Commit ID: 43b33f0283d4d61385b98a3599688499abd86a75
 *********************************************************************************************************************/

#ifndef DENPENDENCYSRVEXE_INCLUDE_AMSR_SOMEIP_BINDING_TRANSFORMATION_LAYER_INTERNAL_GWM_PT_BATTERYPACKSYS_BMSSTSRV_SI_SI_BMSSTSRV_SKELETON_SOMEIP_EVENT_MANAGER_H_
#define DENPENDENCYSRVEXE_INCLUDE_AMSR_SOMEIP_BINDING_TRANSFORMATION_LAYER_INTERNAL_GWM_PT_BATTERYPACKSYS_BMSSTSRV_SI_SI_BMSSTSRV_SKELETON_SOMEIP_EVENT_MANAGER_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "amsr/someip_protocol/internal/dataprototype_serialization/gwm_pt_batterypacksys_bmsstsrv_si/events/serializer_BMSChrgTi.h"
#include "amsr/someip_protocol/internal/dataprototype_serialization/gwm_pt_batterypacksys_bmsstsrv_si/events/serializer_BMSConDchrMaxPwr.h"
#include "amsr/someip_protocol/internal/dataprototype_serialization/gwm_pt_batterypacksys_bmsstsrv_si/events/serializer_BMSConFBMaxPwr.h"
#include "amsr/someip_protocol/internal/dataprototype_serialization/gwm_pt_batterypacksys_bmsstsrv_si/events/serializer_BMSPlsDchrMaxPwr.h"
#include "amsr/someip_protocol/internal/dataprototype_serialization/gwm_pt_batterypacksys_bmsstsrv_si/events/serializer_BMSPlsFBMaxPwr.h"
#include "amsr/someip_protocol/internal/dataprototype_serialization/gwm_pt_batterypacksys_bmsstsrv_si/events/serializer_BmsInletCooltT.h"
#include "amsr/someip_protocol/internal/dataprototype_serialization/gwm_pt_batterypacksys_bmsstsrv_si/events/serializer_BmsIsoR.h"
#include "amsr/someip_protocol/internal/dataprototype_serialization/gwm_pt_batterypacksys_bmsstsrv_si/events/serializer_BmsOutletCooltT.h"
#include "amsr/someip_protocol/internal/dataprototype_serialization/gwm_pt_batterypacksys_bmsstsrv_si/events/serializer_CellMaxT.h"
#include "amsr/someip_protocol/internal/dataprototype_serialization/gwm_pt_batterypacksys_bmsstsrv_si/events/serializer_CellMaxU.h"
#include "amsr/someip_protocol/internal/dataprototype_serialization/gwm_pt_batterypacksys_bmsstsrv_si/events/serializer_CellMinT.h"
#include "amsr/someip_protocol/internal/dataprototype_serialization/gwm_pt_batterypacksys_bmsstsrv_si/events/serializer_CellMinU.h"
#include "amsr/someip_protocol/internal/dataprototype_serialization/gwm_pt_batterypacksys_bmsstsrv_si/events/serializer_HVBattDispySoc.h"
#include "amsr/someip_protocol/internal/dataprototype_serialization/gwm_pt_batterypacksys_bmsstsrv_si/events/serializer_HVBattI.h"
#include "amsr/someip_protocol/internal/dataprototype_serialization/gwm_pt_batterypacksys_bmsstsrv_si/events/serializer_HVBattInnerSoc.h"
#include "amsr/someip_protocol/internal/dataprototype_serialization/gwm_pt_batterypacksys_bmsstsrv_si/events/serializer_HVBattSoh.h"
#include "amsr/someip_protocol/internal/dataprototype_serialization/gwm_pt_batterypacksys_bmsstsrv_si/events/serializer_HVBattTotEgy.h"
#include "amsr/someip_protocol/internal/dataprototype_serialization/gwm_pt_batterypacksys_bmsstsrv_si/events/serializer_HVBattU.h"
#include "amsr/someip_protocol/internal/dataprototype_serialization/gwm_pt_batterypacksys_bmsstsrv_si/events/serializer_ModulAvrgT.h"
#include "amsr/someip_protocol/internal/dataprototype_serialization/gwm_pt_batterypacksys_bmsstsrv_si/fields/serializer_BMSChrgSt.h"
#include "amsr/someip_protocol/internal/dataprototype_serialization/gwm_pt_batterypacksys_bmsstsrv_si/fields/serializer_BMSDCChrgCnctSt.h"
#include "amsr/someip_protocol/internal/dataprototype_serialization/gwm_pt_batterypacksys_bmsstsrv_si/fields/serializer_BMSDCChrgEndReas.h"
#include "amsr/someip_protocol/internal/dataprototype_serialization/gwm_pt_batterypacksys_bmsstsrv_si/fields/serializer_BMSDchrgPrmsSt.h"
#include "amsr/someip_protocol/internal/dataprototype_serialization/gwm_pt_batterypacksys_bmsstsrv_si/fields/serializer_BMSHeatRunawaySt.h"
#include "amsr/someip_protocol/internal/dataprototype_serialization/gwm_pt_batterypacksys_bmsstsrv_si/fields/serializer_BMSInnerSOCSt.h"
#include "amsr/someip_protocol/internal/dataprototype_serialization/gwm_pt_batterypacksys_bmsstsrv_si/fields/serializer_BMSIntelTempMgtSt.h"
#include "amsr/someip_protocol/internal/dataprototype_serialization/gwm_pt_batterypacksys_bmsstsrv_si/fields/serializer_BMSLowTempSt.h"
#include "amsr/someip_protocol/internal/dataprototype_serialization/gwm_pt_batterypacksys_bmsstsrv_si/fields/serializer_BMSRmtPreHeatSt.h"
#include "amsr/someip_protocol/internal/dataprototype_serialization/gwm_pt_batterypacksys_bmsstsrv_si/fields/serializer_BMSSafCnctSt.h"
#include "amsr/someip_protocol/internal/dataprototype_serialization/gwm_pt_batterypacksys_bmsstsrv_si/fields/serializer_BmsErrSt.h"
#include "amsr/someip_protocol/internal/dataprototype_serialization/gwm_pt_batterypacksys_bmsstsrv_si/fields/serializer_BmsHVILSt.h"
#include "amsr/someip_protocol/internal/dataprototype_serialization/gwm_pt_batterypacksys_bmsstsrv_si/fields/serializer_BmsIsoMeasSt.h"
#include "amsr/someip_protocol/internal/dataprototype_serialization/gwm_pt_batterypacksys_bmsstsrv_si/fields/serializer_BmsSt.h"
#include "amsr/someip_protocol/internal/dataprototype_serialization/gwm_pt_batterypacksys_bmsstsrv_si/fields/serializer_HVBattSOCLim.h"
#include "amsr/someip_protocol/internal/dataprototype_serialization/gwm_pt_batterypacksys_bmsstsrv_si/fields/serializer_HVMaiSwtSt.h"
#include "gwm/pt_batterypacksys/bmsstsrv_si/SI_BmsStSrv_types.h"
#include "someip_binding/internal/e2e/e2e_wrapper.h"
#include "someip_binding/internal/inactive_session_handler.h"
#include "someip_binding/internal/session_handler.h"
#include "someip_binding_transformation_layer/internal/skeleton_event_xf.h"

namespace amsr {
namespace someip_binding_transformation_layer {
namespace internal {

// VECTOR NC AutosarC++17_10-M7.3.6, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.6_using_directive
// VECTOR NC AutosarC++17_10-M7.3.4, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.4_using_directive
using namespace vac::container::literals;  // NOLINT(build/namespaces)

namespace gwm {
namespace pt_batterypacksys {
namespace bmsstsrv_si {


// ---- Event 'BMSChrgTi' -------------------------------------------

/*!
 * \brief Skeleton-side configuration struct for event 'BMSChrgTi'.
 */
struct SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSChrgTi {
  /*!
   * \brief SOME/IP service ID.
   */
  static constexpr ::amsr::someip_protocol::internal::ServiceId kServiceId{0x4008};

  /*!
   * \brief SOME/IP major version.
   */
  static constexpr ::amsr::someip_protocol::internal::InterfaceVersion kMajorVersion{1};

  /*!
   * \brief SOME/IP event ID.
   */
  static constexpr ::amsr::someip_protocol::internal::EventId kEventId{0x8011};
  
  /*!
   * \brief Deserializer type for this event
   */
  static constexpr  ::amsr::someipd_app_protocol::internal::MessageType kMessageType{::amsr::someipd_app_protocol::internal::MessageType::kSomeIp};

  /*!
   * \brief Datatype of the event sample.
   * \details AsrPath: /DataTypes/ImplementationDataTypes/BMSChrgTi_Integer_Idt
   */
  using SampleType = ::gwm::pt_batterypacksys::batterypacksys_idt::BMSChrgTi_Integer_Idt;

  /*!
   * \brief Serializer for event payload.
   */
  using PayloadSerializer = ::amsr::someip_protocol::internal::dataprototype_serializer::gwm_pt_batterypacksys_bmsstsrv_si::events::SerializerBMSChrgTi;

  /*!
   * \brief Used session handler.
   * \details Session handling is enabled for the event.
   * \trace CREQ-SomeIpBinding-SessionHandling
   */
  using SessionHandler = ::amsr::someip_binding::internal::SessionHandler;

  /*!
   * \brief E2E profile configuration.
   * \details No E2E protection configured for the event.
   */
  using E2eProfileConfig = void;
};

/*!
 * \brief SOME/IP Skeleton event manager type for event 'BMSChrgTi'.
 */
using SI_BmsStSrvSkeletonSomeIpEventManagerBMSChrgTi =
    ::amsr::someip_binding_transformation_layer::internal::SkeletonEventXf<SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSChrgTi>;


// ---- Event 'BMSConDchrMaxPwr' -------------------------------------------

/*!
 * \brief Skeleton-side configuration struct for event 'BMSConDchrMaxPwr'.
 */
struct SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSConDchrMaxPwr {
  /*!
   * \brief SOME/IP service ID.
   */
  static constexpr ::amsr::someip_protocol::internal::ServiceId kServiceId{0x4008};

  /*!
   * \brief SOME/IP major version.
   */
  static constexpr ::amsr::someip_protocol::internal::InterfaceVersion kMajorVersion{1};

  /*!
   * \brief SOME/IP event ID.
   */
  static constexpr ::amsr::someip_protocol::internal::EventId kEventId{0x800C};
  
  /*!
   * \brief Deserializer type for this event
   */
  static constexpr  ::amsr::someipd_app_protocol::internal::MessageType kMessageType{::amsr::someipd_app_protocol::internal::MessageType::kSomeIp};

  /*!
   * \brief Datatype of the event sample.
   * \details AsrPath: /DataTypes/ImplementationDataTypes/BMSConDchrMaxPwr_Float_Idt
   */
  using SampleType = ::gwm::pt_batterypacksys::batterypacksys_idt::BMSConDchrMaxPwr_Float_Idt;

  /*!
   * \brief Serializer for event payload.
   */
  using PayloadSerializer = ::amsr::someip_protocol::internal::dataprototype_serializer::gwm_pt_batterypacksys_bmsstsrv_si::events::SerializerBMSConDchrMaxPwr;

  /*!
   * \brief Used session handler.
   * \details Session handling is enabled for the event.
   * \trace CREQ-SomeIpBinding-SessionHandling
   */
  using SessionHandler = ::amsr::someip_binding::internal::SessionHandler;

  /*!
   * \brief E2E profile configuration.
   * \details No E2E protection configured for the event.
   */
  using E2eProfileConfig = void;
};

/*!
 * \brief SOME/IP Skeleton event manager type for event 'BMSConDchrMaxPwr'.
 */
using SI_BmsStSrvSkeletonSomeIpEventManagerBMSConDchrMaxPwr =
    ::amsr::someip_binding_transformation_layer::internal::SkeletonEventXf<SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSConDchrMaxPwr>;


// ---- Event 'BMSConFBMaxPwr' -------------------------------------------

/*!
 * \brief Skeleton-side configuration struct for event 'BMSConFBMaxPwr'.
 */
struct SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSConFBMaxPwr {
  /*!
   * \brief SOME/IP service ID.
   */
  static constexpr ::amsr::someip_protocol::internal::ServiceId kServiceId{0x4008};

  /*!
   * \brief SOME/IP major version.
   */
  static constexpr ::amsr::someip_protocol::internal::InterfaceVersion kMajorVersion{1};

  /*!
   * \brief SOME/IP event ID.
   */
  static constexpr ::amsr::someip_protocol::internal::EventId kEventId{0x800D};
  
  /*!
   * \brief Deserializer type for this event
   */
  static constexpr  ::amsr::someipd_app_protocol::internal::MessageType kMessageType{::amsr::someipd_app_protocol::internal::MessageType::kSomeIp};

  /*!
   * \brief Datatype of the event sample.
   * \details AsrPath: /DataTypes/ImplementationDataTypes/BMSConFBMaxPwr_Float_Idt
   */
  using SampleType = ::gwm::pt_batterypacksys::batterypacksys_idt::BMSConFBMaxPwr_Float_Idt;

  /*!
   * \brief Serializer for event payload.
   */
  using PayloadSerializer = ::amsr::someip_protocol::internal::dataprototype_serializer::gwm_pt_batterypacksys_bmsstsrv_si::events::SerializerBMSConFBMaxPwr;

  /*!
   * \brief Used session handler.
   * \details Session handling is enabled for the event.
   * \trace CREQ-SomeIpBinding-SessionHandling
   */
  using SessionHandler = ::amsr::someip_binding::internal::SessionHandler;

  /*!
   * \brief E2E profile configuration.
   * \details No E2E protection configured for the event.
   */
  using E2eProfileConfig = void;
};

/*!
 * \brief SOME/IP Skeleton event manager type for event 'BMSConFBMaxPwr'.
 */
using SI_BmsStSrvSkeletonSomeIpEventManagerBMSConFBMaxPwr =
    ::amsr::someip_binding_transformation_layer::internal::SkeletonEventXf<SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSConFBMaxPwr>;


// ---- Event 'BMSPlsDchrMaxPwr' -------------------------------------------

/*!
 * \brief Skeleton-side configuration struct for event 'BMSPlsDchrMaxPwr'.
 */
struct SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSPlsDchrMaxPwr {
  /*!
   * \brief SOME/IP service ID.
   */
  static constexpr ::amsr::someip_protocol::internal::ServiceId kServiceId{0x4008};

  /*!
   * \brief SOME/IP major version.
   */
  static constexpr ::amsr::someip_protocol::internal::InterfaceVersion kMajorVersion{1};

  /*!
   * \brief SOME/IP event ID.
   */
  static constexpr ::amsr::someip_protocol::internal::EventId kEventId{0x800A};
  
  /*!
   * \brief Deserializer type for this event
   */
  static constexpr  ::amsr::someipd_app_protocol::internal::MessageType kMessageType{::amsr::someipd_app_protocol::internal::MessageType::kSomeIp};

  /*!
   * \brief Datatype of the event sample.
   * \details AsrPath: /DataTypes/ImplementationDataTypes/BMSPlsDchrMaxPwr_Float_Idt
   */
  using SampleType = ::gwm::pt_batterypacksys::batterypacksys_idt::BMSPlsDchrMaxPwr_Float_Idt;

  /*!
   * \brief Serializer for event payload.
   */
  using PayloadSerializer = ::amsr::someip_protocol::internal::dataprototype_serializer::gwm_pt_batterypacksys_bmsstsrv_si::events::SerializerBMSPlsDchrMaxPwr;

  /*!
   * \brief Used session handler.
   * \details Session handling is enabled for the event.
   * \trace CREQ-SomeIpBinding-SessionHandling
   */
  using SessionHandler = ::amsr::someip_binding::internal::SessionHandler;

  /*!
   * \brief E2E profile configuration.
   * \details No E2E protection configured for the event.
   */
  using E2eProfileConfig = void;
};

/*!
 * \brief SOME/IP Skeleton event manager type for event 'BMSPlsDchrMaxPwr'.
 */
using SI_BmsStSrvSkeletonSomeIpEventManagerBMSPlsDchrMaxPwr =
    ::amsr::someip_binding_transformation_layer::internal::SkeletonEventXf<SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSPlsDchrMaxPwr>;


// ---- Event 'BMSPlsFBMaxPwr' -------------------------------------------

/*!
 * \brief Skeleton-side configuration struct for event 'BMSPlsFBMaxPwr'.
 */
struct SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSPlsFBMaxPwr {
  /*!
   * \brief SOME/IP service ID.
   */
  static constexpr ::amsr::someip_protocol::internal::ServiceId kServiceId{0x4008};

  /*!
   * \brief SOME/IP major version.
   */
  static constexpr ::amsr::someip_protocol::internal::InterfaceVersion kMajorVersion{1};

  /*!
   * \brief SOME/IP event ID.
   */
  static constexpr ::amsr::someip_protocol::internal::EventId kEventId{0x800B};
  
  /*!
   * \brief Deserializer type for this event
   */
  static constexpr  ::amsr::someipd_app_protocol::internal::MessageType kMessageType{::amsr::someipd_app_protocol::internal::MessageType::kSomeIp};

  /*!
   * \brief Datatype of the event sample.
   * \details AsrPath: /DataTypes/ImplementationDataTypes/BMSPlsFBMaxPwr_Float_Idt
   */
  using SampleType = ::gwm::pt_batterypacksys::batterypacksys_idt::BMSPlsFBMaxPwr_Float_Idt;

  /*!
   * \brief Serializer for event payload.
   */
  using PayloadSerializer = ::amsr::someip_protocol::internal::dataprototype_serializer::gwm_pt_batterypacksys_bmsstsrv_si::events::SerializerBMSPlsFBMaxPwr;

  /*!
   * \brief Used session handler.
   * \details Session handling is enabled for the event.
   * \trace CREQ-SomeIpBinding-SessionHandling
   */
  using SessionHandler = ::amsr::someip_binding::internal::SessionHandler;

  /*!
   * \brief E2E profile configuration.
   * \details No E2E protection configured for the event.
   */
  using E2eProfileConfig = void;
};

/*!
 * \brief SOME/IP Skeleton event manager type for event 'BMSPlsFBMaxPwr'.
 */
using SI_BmsStSrvSkeletonSomeIpEventManagerBMSPlsFBMaxPwr =
    ::amsr::someip_binding_transformation_layer::internal::SkeletonEventXf<SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSPlsFBMaxPwr>;


// ---- Event 'BmsInletCooltT' -------------------------------------------

/*!
 * \brief Skeleton-side configuration struct for event 'BmsInletCooltT'.
 */
struct SI_BmsStSrvSkeletonSomeIpEventConfigurationBmsInletCooltT {
  /*!
   * \brief SOME/IP service ID.
   */
  static constexpr ::amsr::someip_protocol::internal::ServiceId kServiceId{0x4008};

  /*!
   * \brief SOME/IP major version.
   */
  static constexpr ::amsr::someip_protocol::internal::InterfaceVersion kMajorVersion{1};

  /*!
   * \brief SOME/IP event ID.
   */
  static constexpr ::amsr::someip_protocol::internal::EventId kEventId{0x8009};
  
  /*!
   * \brief Deserializer type for this event
   */
  static constexpr  ::amsr::someipd_app_protocol::internal::MessageType kMessageType{::amsr::someipd_app_protocol::internal::MessageType::kSomeIp};

  /*!
   * \brief Datatype of the event sample.
   * \details AsrPath: /DataTypes/ImplementationDataTypes/BmsInletCooltT_Integer_Idt
   */
  using SampleType = ::gwm::pt_batterypacksys::batterypacksys_idt::BmsInletCooltT_Integer_Idt;

  /*!
   * \brief Serializer for event payload.
   */
  using PayloadSerializer = ::amsr::someip_protocol::internal::dataprototype_serializer::gwm_pt_batterypacksys_bmsstsrv_si::events::SerializerBmsInletCooltT;

  /*!
   * \brief Used session handler.
   * \details Session handling is enabled for the event.
   * \trace CREQ-SomeIpBinding-SessionHandling
   */
  using SessionHandler = ::amsr::someip_binding::internal::SessionHandler;

  /*!
   * \brief E2E profile configuration.
   * \details No E2E protection configured for the event.
   */
  using E2eProfileConfig = void;
};

/*!
 * \brief SOME/IP Skeleton event manager type for event 'BmsInletCooltT'.
 */
using SI_BmsStSrvSkeletonSomeIpEventManagerBmsInletCooltT =
    ::amsr::someip_binding_transformation_layer::internal::SkeletonEventXf<SI_BmsStSrvSkeletonSomeIpEventConfigurationBmsInletCooltT>;


// ---- Event 'BmsIsoR' -------------------------------------------

/*!
 * \brief Skeleton-side configuration struct for event 'BmsIsoR'.
 */
struct SI_BmsStSrvSkeletonSomeIpEventConfigurationBmsIsoR {
  /*!
   * \brief SOME/IP service ID.
   */
  static constexpr ::amsr::someip_protocol::internal::ServiceId kServiceId{0x4008};

  /*!
   * \brief SOME/IP major version.
   */
  static constexpr ::amsr::someip_protocol::internal::InterfaceVersion kMajorVersion{1};

  /*!
   * \brief SOME/IP event ID.
   */
  static constexpr ::amsr::someip_protocol::internal::EventId kEventId{0x8008};
  
  /*!
   * \brief Deserializer type for this event
   */
  static constexpr  ::amsr::someipd_app_protocol::internal::MessageType kMessageType{::amsr::someipd_app_protocol::internal::MessageType::kSomeIp};

  /*!
   * \brief Datatype of the event sample.
   * \details AsrPath: /DataTypes/ImplementationDataTypes/BmsIsoR_Integer_Idt
   */
  using SampleType = ::gwm::pt_batterypacksys::batterypacksys_idt::BmsIsoR_Integer_Idt;

  /*!
   * \brief Serializer for event payload.
   */
  using PayloadSerializer = ::amsr::someip_protocol::internal::dataprototype_serializer::gwm_pt_batterypacksys_bmsstsrv_si::events::SerializerBmsIsoR;

  /*!
   * \brief Used session handler.
   * \details Session handling is enabled for the event.
   * \trace CREQ-SomeIpBinding-SessionHandling
   */
  using SessionHandler = ::amsr::someip_binding::internal::SessionHandler;

  /*!
   * \brief E2E profile configuration.
   * \details No E2E protection configured for the event.
   */
  using E2eProfileConfig = void;
};

/*!
 * \brief SOME/IP Skeleton event manager type for event 'BmsIsoR'.
 */
using SI_BmsStSrvSkeletonSomeIpEventManagerBmsIsoR =
    ::amsr::someip_binding_transformation_layer::internal::SkeletonEventXf<SI_BmsStSrvSkeletonSomeIpEventConfigurationBmsIsoR>;


// ---- Event 'BmsOutletCooltT' -------------------------------------------

/*!
 * \brief Skeleton-side configuration struct for event 'BmsOutletCooltT'.
 */
struct SI_BmsStSrvSkeletonSomeIpEventConfigurationBmsOutletCooltT {
  /*!
   * \brief SOME/IP service ID.
   */
  static constexpr ::amsr::someip_protocol::internal::ServiceId kServiceId{0x4008};

  /*!
   * \brief SOME/IP major version.
   */
  static constexpr ::amsr::someip_protocol::internal::InterfaceVersion kMajorVersion{1};

  /*!
   * \brief SOME/IP event ID.
   */
  static constexpr ::amsr::someip_protocol::internal::EventId kEventId{0x8013};
  
  /*!
   * \brief Deserializer type for this event
   */
  static constexpr  ::amsr::someipd_app_protocol::internal::MessageType kMessageType{::amsr::someipd_app_protocol::internal::MessageType::kSomeIp};

  /*!
   * \brief Datatype of the event sample.
   * \details AsrPath: /DataTypes/ImplementationDataTypes/BmsOutletCooltT_Integer_Idt
   */
  using SampleType = ::gwm::pt_batterypacksys::batterypacksys_idt::BmsOutletCooltT_Integer_Idt;

  /*!
   * \brief Serializer for event payload.
   */
  using PayloadSerializer = ::amsr::someip_protocol::internal::dataprototype_serializer::gwm_pt_batterypacksys_bmsstsrv_si::events::SerializerBmsOutletCooltT;

  /*!
   * \brief Used session handler.
   * \details Session handling is enabled for the event.
   * \trace CREQ-SomeIpBinding-SessionHandling
   */
  using SessionHandler = ::amsr::someip_binding::internal::SessionHandler;

  /*!
   * \brief E2E profile configuration.
   * \details No E2E protection configured for the event.
   */
  using E2eProfileConfig = void;
};

/*!
 * \brief SOME/IP Skeleton event manager type for event 'BmsOutletCooltT'.
 */
using SI_BmsStSrvSkeletonSomeIpEventManagerBmsOutletCooltT =
    ::amsr::someip_binding_transformation_layer::internal::SkeletonEventXf<SI_BmsStSrvSkeletonSomeIpEventConfigurationBmsOutletCooltT>;


// ---- Event 'CellMaxT' -------------------------------------------

/*!
 * \brief Skeleton-side configuration struct for event 'CellMaxT'.
 */
struct SI_BmsStSrvSkeletonSomeIpEventConfigurationCellMaxT {
  /*!
   * \brief SOME/IP service ID.
   */
  static constexpr ::amsr::someip_protocol::internal::ServiceId kServiceId{0x4008};

  /*!
   * \brief SOME/IP major version.
   */
  static constexpr ::amsr::someip_protocol::internal::InterfaceVersion kMajorVersion{1};

  /*!
   * \brief SOME/IP event ID.
   */
  static constexpr ::amsr::someip_protocol::internal::EventId kEventId{0x800E};
  
  /*!
   * \brief Deserializer type for this event
   */
  static constexpr  ::amsr::someipd_app_protocol::internal::MessageType kMessageType{::amsr::someipd_app_protocol::internal::MessageType::kSomeIp};

  /*!
   * \brief Datatype of the event sample.
   * \details AsrPath: /DataTypes/ImplementationDataTypes/CellMaxT_Struct_Idt
   */
  using SampleType = ::gwm::pt_batterypacksys::batterypacksys_idt::CellMaxT_Struct_Idt;

  /*!
   * \brief Serializer for event payload.
   */
  using PayloadSerializer = ::amsr::someip_protocol::internal::dataprototype_serializer::gwm_pt_batterypacksys_bmsstsrv_si::events::SerializerCellMaxT;

  /*!
   * \brief Used session handler.
   * \details Session handling is enabled for the event.
   * \trace CREQ-SomeIpBinding-SessionHandling
   */
  using SessionHandler = ::amsr::someip_binding::internal::SessionHandler;

  /*!
   * \brief E2E profile configuration.
   * \details No E2E protection configured for the event.
   */
  using E2eProfileConfig = void;
};

/*!
 * \brief SOME/IP Skeleton event manager type for event 'CellMaxT'.
 */
using SI_BmsStSrvSkeletonSomeIpEventManagerCellMaxT =
    ::amsr::someip_binding_transformation_layer::internal::SkeletonEventXf<SI_BmsStSrvSkeletonSomeIpEventConfigurationCellMaxT>;


// ---- Event 'CellMaxU' -------------------------------------------

/*!
 * \brief Skeleton-side configuration struct for event 'CellMaxU'.
 */
struct SI_BmsStSrvSkeletonSomeIpEventConfigurationCellMaxU {
  /*!
   * \brief SOME/IP service ID.
   */
  static constexpr ::amsr::someip_protocol::internal::ServiceId kServiceId{0x4008};

  /*!
   * \brief SOME/IP major version.
   */
  static constexpr ::amsr::someip_protocol::internal::InterfaceVersion kMajorVersion{1};

  /*!
   * \brief SOME/IP event ID.
   */
  static constexpr ::amsr::someip_protocol::internal::EventId kEventId{0x8003};
  
  /*!
   * \brief Deserializer type for this event
   */
  static constexpr  ::amsr::someipd_app_protocol::internal::MessageType kMessageType{::amsr::someipd_app_protocol::internal::MessageType::kSomeIp};

  /*!
   * \brief Datatype of the event sample.
   * \details AsrPath: /DataTypes/ImplementationDataTypes/CellMaxU_Struct_Idt
   */
  using SampleType = ::gwm::pt_batterypacksys::batterypacksys_idt::CellMaxU_Struct_Idt;

  /*!
   * \brief Serializer for event payload.
   */
  using PayloadSerializer = ::amsr::someip_protocol::internal::dataprototype_serializer::gwm_pt_batterypacksys_bmsstsrv_si::events::SerializerCellMaxU;

  /*!
   * \brief Used session handler.
   * \details Session handling is enabled for the event.
   * \trace CREQ-SomeIpBinding-SessionHandling
   */
  using SessionHandler = ::amsr::someip_binding::internal::SessionHandler;

  /*!
   * \brief E2E profile configuration.
   * \details No E2E protection configured for the event.
   */
  using E2eProfileConfig = void;
};

/*!
 * \brief SOME/IP Skeleton event manager type for event 'CellMaxU'.
 */
using SI_BmsStSrvSkeletonSomeIpEventManagerCellMaxU =
    ::amsr::someip_binding_transformation_layer::internal::SkeletonEventXf<SI_BmsStSrvSkeletonSomeIpEventConfigurationCellMaxU>;


// ---- Event 'CellMinT' -------------------------------------------

/*!
 * \brief Skeleton-side configuration struct for event 'CellMinT'.
 */
struct SI_BmsStSrvSkeletonSomeIpEventConfigurationCellMinT {
  /*!
   * \brief SOME/IP service ID.
   */
  static constexpr ::amsr::someip_protocol::internal::ServiceId kServiceId{0x4008};

  /*!
   * \brief SOME/IP major version.
   */
  static constexpr ::amsr::someip_protocol::internal::InterfaceVersion kMajorVersion{1};

  /*!
   * \brief SOME/IP event ID.
   */
  static constexpr ::amsr::someip_protocol::internal::EventId kEventId{0x800F};
  
  /*!
   * \brief Deserializer type for this event
   */
  static constexpr  ::amsr::someipd_app_protocol::internal::MessageType kMessageType{::amsr::someipd_app_protocol::internal::MessageType::kSomeIp};

  /*!
   * \brief Datatype of the event sample.
   * \details AsrPath: /DataTypes/ImplementationDataTypes/CellMinT_Struct_Idt
   */
  using SampleType = ::gwm::pt_batterypacksys::batterypacksys_idt::CellMinT_Struct_Idt;

  /*!
   * \brief Serializer for event payload.
   */
  using PayloadSerializer = ::amsr::someip_protocol::internal::dataprototype_serializer::gwm_pt_batterypacksys_bmsstsrv_si::events::SerializerCellMinT;

  /*!
   * \brief Used session handler.
   * \details Session handling is enabled for the event.
   * \trace CREQ-SomeIpBinding-SessionHandling
   */
  using SessionHandler = ::amsr::someip_binding::internal::SessionHandler;

  /*!
   * \brief E2E profile configuration.
   * \details No E2E protection configured for the event.
   */
  using E2eProfileConfig = void;
};

/*!
 * \brief SOME/IP Skeleton event manager type for event 'CellMinT'.
 */
using SI_BmsStSrvSkeletonSomeIpEventManagerCellMinT =
    ::amsr::someip_binding_transformation_layer::internal::SkeletonEventXf<SI_BmsStSrvSkeletonSomeIpEventConfigurationCellMinT>;


// ---- Event 'CellMinU' -------------------------------------------

/*!
 * \brief Skeleton-side configuration struct for event 'CellMinU'.
 */
struct SI_BmsStSrvSkeletonSomeIpEventConfigurationCellMinU {
  /*!
   * \brief SOME/IP service ID.
   */
  static constexpr ::amsr::someip_protocol::internal::ServiceId kServiceId{0x4008};

  /*!
   * \brief SOME/IP major version.
   */
  static constexpr ::amsr::someip_protocol::internal::InterfaceVersion kMajorVersion{1};

  /*!
   * \brief SOME/IP event ID.
   */
  static constexpr ::amsr::someip_protocol::internal::EventId kEventId{0x8004};
  
  /*!
   * \brief Deserializer type for this event
   */
  static constexpr  ::amsr::someipd_app_protocol::internal::MessageType kMessageType{::amsr::someipd_app_protocol::internal::MessageType::kSomeIp};

  /*!
   * \brief Datatype of the event sample.
   * \details AsrPath: /DataTypes/ImplementationDataTypes/CellMinU_Struct_Idt
   */
  using SampleType = ::gwm::pt_batterypacksys::batterypacksys_idt::CellMinU_Struct_Idt;

  /*!
   * \brief Serializer for event payload.
   */
  using PayloadSerializer = ::amsr::someip_protocol::internal::dataprototype_serializer::gwm_pt_batterypacksys_bmsstsrv_si::events::SerializerCellMinU;

  /*!
   * \brief Used session handler.
   * \details Session handling is enabled for the event.
   * \trace CREQ-SomeIpBinding-SessionHandling
   */
  using SessionHandler = ::amsr::someip_binding::internal::SessionHandler;

  /*!
   * \brief E2E profile configuration.
   * \details No E2E protection configured for the event.
   */
  using E2eProfileConfig = void;
};

/*!
 * \brief SOME/IP Skeleton event manager type for event 'CellMinU'.
 */
using SI_BmsStSrvSkeletonSomeIpEventManagerCellMinU =
    ::amsr::someip_binding_transformation_layer::internal::SkeletonEventXf<SI_BmsStSrvSkeletonSomeIpEventConfigurationCellMinU>;


// ---- Event 'HVBattDispySoc' -------------------------------------------

/*!
 * \brief Skeleton-side configuration struct for event 'HVBattDispySoc'.
 */
struct SI_BmsStSrvSkeletonSomeIpEventConfigurationHVBattDispySoc {
  /*!
   * \brief SOME/IP service ID.
   */
  static constexpr ::amsr::someip_protocol::internal::ServiceId kServiceId{0x4008};

  /*!
   * \brief SOME/IP major version.
   */
  static constexpr ::amsr::someip_protocol::internal::InterfaceVersion kMajorVersion{1};

  /*!
   * \brief SOME/IP event ID.
   */
  static constexpr ::amsr::someip_protocol::internal::EventId kEventId{0x8005};
  
  /*!
   * \brief Deserializer type for this event
   */
  static constexpr  ::amsr::someipd_app_protocol::internal::MessageType kMessageType{::amsr::someipd_app_protocol::internal::MessageType::kSomeIp};

  /*!
   * \brief Datatype of the event sample.
   * \details AsrPath: /DataTypes/ImplementationDataTypes/HVBattDispySoc_Float_Idt
   */
  using SampleType = ::gwm::pt_batterypacksys::batterypacksys_idt::HVBattDispySoc_Float_Idt;

  /*!
   * \brief Serializer for event payload.
   */
  using PayloadSerializer = ::amsr::someip_protocol::internal::dataprototype_serializer::gwm_pt_batterypacksys_bmsstsrv_si::events::SerializerHVBattDispySoc;

  /*!
   * \brief Used session handler.
   * \details Session handling is enabled for the event.
   * \trace CREQ-SomeIpBinding-SessionHandling
   */
  using SessionHandler = ::amsr::someip_binding::internal::SessionHandler;

  /*!
   * \brief E2E profile configuration.
   * \details No E2E protection configured for the event.
   */
  using E2eProfileConfig = void;
};

/*!
 * \brief SOME/IP Skeleton event manager type for event 'HVBattDispySoc'.
 */
using SI_BmsStSrvSkeletonSomeIpEventManagerHVBattDispySoc =
    ::amsr::someip_binding_transformation_layer::internal::SkeletonEventXf<SI_BmsStSrvSkeletonSomeIpEventConfigurationHVBattDispySoc>;


// ---- Event 'HVBattI' -------------------------------------------

/*!
 * \brief Skeleton-side configuration struct for event 'HVBattI'.
 */
struct SI_BmsStSrvSkeletonSomeIpEventConfigurationHVBattI {
  /*!
   * \brief SOME/IP service ID.
   */
  static constexpr ::amsr::someip_protocol::internal::ServiceId kServiceId{0x4008};

  /*!
   * \brief SOME/IP major version.
   */
  static constexpr ::amsr::someip_protocol::internal::InterfaceVersion kMajorVersion{1};

  /*!
   * \brief SOME/IP event ID.
   */
  static constexpr ::amsr::someip_protocol::internal::EventId kEventId{0x8001};
  
  /*!
   * \brief Deserializer type for this event
   */
  static constexpr  ::amsr::someipd_app_protocol::internal::MessageType kMessageType{::amsr::someipd_app_protocol::internal::MessageType::kSomeIp};

  /*!
   * \brief Datatype of the event sample.
   * \details AsrPath: /DataTypes/ImplementationDataTypes/HVBattI_Float_Idt
   */
  using SampleType = ::gwm::pt_batterypacksys::batterypacksys_idt::HVBattI_Float_Idt;

  /*!
   * \brief Serializer for event payload.
   */
  using PayloadSerializer = ::amsr::someip_protocol::internal::dataprototype_serializer::gwm_pt_batterypacksys_bmsstsrv_si::events::SerializerHVBattI;

  /*!
   * \brief Used session handler.
   * \details Session handling is enabled for the event.
   * \trace CREQ-SomeIpBinding-SessionHandling
   */
  using SessionHandler = ::amsr::someip_binding::internal::SessionHandler;

  /*!
   * \brief E2E profile configuration.
   * \details No E2E protection configured for the event.
   */
  using E2eProfileConfig = void;
};

/*!
 * \brief SOME/IP Skeleton event manager type for event 'HVBattI'.
 */
using SI_BmsStSrvSkeletonSomeIpEventManagerHVBattI =
    ::amsr::someip_binding_transformation_layer::internal::SkeletonEventXf<SI_BmsStSrvSkeletonSomeIpEventConfigurationHVBattI>;


// ---- Event 'HVBattInnerSoc' -------------------------------------------

/*!
 * \brief Skeleton-side configuration struct for event 'HVBattInnerSoc'.
 */
struct SI_BmsStSrvSkeletonSomeIpEventConfigurationHVBattInnerSoc {
  /*!
   * \brief SOME/IP service ID.
   */
  static constexpr ::amsr::someip_protocol::internal::ServiceId kServiceId{0x4008};

  /*!
   * \brief SOME/IP major version.
   */
  static constexpr ::amsr::someip_protocol::internal::InterfaceVersion kMajorVersion{1};

  /*!
   * \brief SOME/IP event ID.
   */
  static constexpr ::amsr::someip_protocol::internal::EventId kEventId{0x8012};
  
  /*!
   * \brief Deserializer type for this event
   */
  static constexpr  ::amsr::someipd_app_protocol::internal::MessageType kMessageType{::amsr::someipd_app_protocol::internal::MessageType::kSomeIp};

  /*!
   * \brief Datatype of the event sample.
   * \details AsrPath: /DataTypes/ImplementationDataTypes/HVBattInnerSoc_Float_Idt
   */
  using SampleType = ::gwm::pt_batterypacksys::batterypacksys_idt::HVBattInnerSoc_Float_Idt;

  /*!
   * \brief Serializer for event payload.
   */
  using PayloadSerializer = ::amsr::someip_protocol::internal::dataprototype_serializer::gwm_pt_batterypacksys_bmsstsrv_si::events::SerializerHVBattInnerSoc;

  /*!
   * \brief Used session handler.
   * \details Session handling is enabled for the event.
   * \trace CREQ-SomeIpBinding-SessionHandling
   */
  using SessionHandler = ::amsr::someip_binding::internal::SessionHandler;

  /*!
   * \brief E2E profile configuration.
   * \details No E2E protection configured for the event.
   */
  using E2eProfileConfig = void;
};

/*!
 * \brief SOME/IP Skeleton event manager type for event 'HVBattInnerSoc'.
 */
using SI_BmsStSrvSkeletonSomeIpEventManagerHVBattInnerSoc =
    ::amsr::someip_binding_transformation_layer::internal::SkeletonEventXf<SI_BmsStSrvSkeletonSomeIpEventConfigurationHVBattInnerSoc>;


// ---- Event 'HVBattSoh' -------------------------------------------

/*!
 * \brief Skeleton-side configuration struct for event 'HVBattSoh'.
 */
struct SI_BmsStSrvSkeletonSomeIpEventConfigurationHVBattSoh {
  /*!
   * \brief SOME/IP service ID.
   */
  static constexpr ::amsr::someip_protocol::internal::ServiceId kServiceId{0x4008};

  /*!
   * \brief SOME/IP major version.
   */
  static constexpr ::amsr::someip_protocol::internal::InterfaceVersion kMajorVersion{1};

  /*!
   * \brief SOME/IP event ID.
   */
  static constexpr ::amsr::someip_protocol::internal::EventId kEventId{0x8006};
  
  /*!
   * \brief Deserializer type for this event
   */
  static constexpr  ::amsr::someipd_app_protocol::internal::MessageType kMessageType{::amsr::someipd_app_protocol::internal::MessageType::kSomeIp};

  /*!
   * \brief Datatype of the event sample.
   * \details AsrPath: /DataTypes/ImplementationDataTypes/HVBattSoh_Float_Idt
   */
  using SampleType = ::gwm::pt_batterypacksys::batterypacksys_idt::HVBattSoh_Float_Idt;

  /*!
   * \brief Serializer for event payload.
   */
  using PayloadSerializer = ::amsr::someip_protocol::internal::dataprototype_serializer::gwm_pt_batterypacksys_bmsstsrv_si::events::SerializerHVBattSoh;

  /*!
   * \brief Used session handler.
   * \details Session handling is enabled for the event.
   * \trace CREQ-SomeIpBinding-SessionHandling
   */
  using SessionHandler = ::amsr::someip_binding::internal::SessionHandler;

  /*!
   * \brief E2E profile configuration.
   * \details No E2E protection configured for the event.
   */
  using E2eProfileConfig = void;
};

/*!
 * \brief SOME/IP Skeleton event manager type for event 'HVBattSoh'.
 */
using SI_BmsStSrvSkeletonSomeIpEventManagerHVBattSoh =
    ::amsr::someip_binding_transformation_layer::internal::SkeletonEventXf<SI_BmsStSrvSkeletonSomeIpEventConfigurationHVBattSoh>;


// ---- Event 'HVBattTotEgy' -------------------------------------------

/*!
 * \brief Skeleton-side configuration struct for event 'HVBattTotEgy'.
 */
struct SI_BmsStSrvSkeletonSomeIpEventConfigurationHVBattTotEgy {
  /*!
   * \brief SOME/IP service ID.
   */
  static constexpr ::amsr::someip_protocol::internal::ServiceId kServiceId{0x4008};

  /*!
   * \brief SOME/IP major version.
   */
  static constexpr ::amsr::someip_protocol::internal::InterfaceVersion kMajorVersion{1};

  /*!
   * \brief SOME/IP event ID.
   */
  static constexpr ::amsr::someip_protocol::internal::EventId kEventId{0x8007};
  
  /*!
   * \brief Deserializer type for this event
   */
  static constexpr  ::amsr::someipd_app_protocol::internal::MessageType kMessageType{::amsr::someipd_app_protocol::internal::MessageType::kSomeIp};

  /*!
   * \brief Datatype of the event sample.
   * \details AsrPath: /DataTypes/ImplementationDataTypes/HVBattTotEgy_Float_Idt
   */
  using SampleType = ::gwm::pt_batterypacksys::batterypacksys_idt::HVBattTotEgy_Float_Idt;

  /*!
   * \brief Serializer for event payload.
   */
  using PayloadSerializer = ::amsr::someip_protocol::internal::dataprototype_serializer::gwm_pt_batterypacksys_bmsstsrv_si::events::SerializerHVBattTotEgy;

  /*!
   * \brief Used session handler.
   * \details Session handling is enabled for the event.
   * \trace CREQ-SomeIpBinding-SessionHandling
   */
  using SessionHandler = ::amsr::someip_binding::internal::SessionHandler;

  /*!
   * \brief E2E profile configuration.
   * \details No E2E protection configured for the event.
   */
  using E2eProfileConfig = void;
};

/*!
 * \brief SOME/IP Skeleton event manager type for event 'HVBattTotEgy'.
 */
using SI_BmsStSrvSkeletonSomeIpEventManagerHVBattTotEgy =
    ::amsr::someip_binding_transformation_layer::internal::SkeletonEventXf<SI_BmsStSrvSkeletonSomeIpEventConfigurationHVBattTotEgy>;


// ---- Event 'HVBattU' -------------------------------------------

/*!
 * \brief Skeleton-side configuration struct for event 'HVBattU'.
 */
struct SI_BmsStSrvSkeletonSomeIpEventConfigurationHVBattU {
  /*!
   * \brief SOME/IP service ID.
   */
  static constexpr ::amsr::someip_protocol::internal::ServiceId kServiceId{0x4008};

  /*!
   * \brief SOME/IP major version.
   */
  static constexpr ::amsr::someip_protocol::internal::InterfaceVersion kMajorVersion{1};

  /*!
   * \brief SOME/IP event ID.
   */
  static constexpr ::amsr::someip_protocol::internal::EventId kEventId{0x8002};
  
  /*!
   * \brief Deserializer type for this event
   */
  static constexpr  ::amsr::someipd_app_protocol::internal::MessageType kMessageType{::amsr::someipd_app_protocol::internal::MessageType::kSomeIp};

  /*!
   * \brief Datatype of the event sample.
   * \details AsrPath: /DataTypes/ImplementationDataTypes/HVBattU_Float_Idt
   */
  using SampleType = ::gwm::pt_batterypacksys::batterypacksys_idt::HVBattU_Float_Idt;

  /*!
   * \brief Serializer for event payload.
   */
  using PayloadSerializer = ::amsr::someip_protocol::internal::dataprototype_serializer::gwm_pt_batterypacksys_bmsstsrv_si::events::SerializerHVBattU;

  /*!
   * \brief Used session handler.
   * \details Session handling is enabled for the event.
   * \trace CREQ-SomeIpBinding-SessionHandling
   */
  using SessionHandler = ::amsr::someip_binding::internal::SessionHandler;

  /*!
   * \brief E2E profile configuration.
   * \details No E2E protection configured for the event.
   */
  using E2eProfileConfig = void;
};

/*!
 * \brief SOME/IP Skeleton event manager type for event 'HVBattU'.
 */
using SI_BmsStSrvSkeletonSomeIpEventManagerHVBattU =
    ::amsr::someip_binding_transformation_layer::internal::SkeletonEventXf<SI_BmsStSrvSkeletonSomeIpEventConfigurationHVBattU>;


// ---- Event 'ModulAvrgT' -------------------------------------------

/*!
 * \brief Skeleton-side configuration struct for event 'ModulAvrgT'.
 */
struct SI_BmsStSrvSkeletonSomeIpEventConfigurationModulAvrgT {
  /*!
   * \brief SOME/IP service ID.
   */
  static constexpr ::amsr::someip_protocol::internal::ServiceId kServiceId{0x4008};

  /*!
   * \brief SOME/IP major version.
   */
  static constexpr ::amsr::someip_protocol::internal::InterfaceVersion kMajorVersion{1};

  /*!
   * \brief SOME/IP event ID.
   */
  static constexpr ::amsr::someip_protocol::internal::EventId kEventId{0x8010};
  
  /*!
   * \brief Deserializer type for this event
   */
  static constexpr  ::amsr::someipd_app_protocol::internal::MessageType kMessageType{::amsr::someipd_app_protocol::internal::MessageType::kSomeIp};

  /*!
   * \brief Datatype of the event sample.
   * \details AsrPath: /DataTypes/ImplementationDataTypes/ModulAvrgT_Integer_Idt
   */
  using SampleType = ::gwm::pt_batterypacksys::batterypacksys_idt::ModulAvrgT_Integer_Idt;

  /*!
   * \brief Serializer for event payload.
   */
  using PayloadSerializer = ::amsr::someip_protocol::internal::dataprototype_serializer::gwm_pt_batterypacksys_bmsstsrv_si::events::SerializerModulAvrgT;

  /*!
   * \brief Used session handler.
   * \details Session handling is enabled for the event.
   * \trace CREQ-SomeIpBinding-SessionHandling
   */
  using SessionHandler = ::amsr::someip_binding::internal::SessionHandler;

  /*!
   * \brief E2E profile configuration.
   * \details No E2E protection configured for the event.
   */
  using E2eProfileConfig = void;
};

/*!
 * \brief SOME/IP Skeleton event manager type for event 'ModulAvrgT'.
 */
using SI_BmsStSrvSkeletonSomeIpEventManagerModulAvrgT =
    ::amsr::someip_binding_transformation_layer::internal::SkeletonEventXf<SI_BmsStSrvSkeletonSomeIpEventConfigurationModulAvrgT>;


// ---- Field notifier 'BMSChrgSt' -------------------------------------------

/*!
 * \brief Skeleton-side configuration struct for event 'BMSChrgSt'.
 */
struct SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSChrgSt {
  /*!
   * \brief SOME/IP service ID.
   */
  static constexpr ::amsr::someip_protocol::internal::ServiceId kServiceId{0x4008};

  /*!
   * \brief SOME/IP major version.
   */
  static constexpr ::amsr::someip_protocol::internal::InterfaceVersion kMajorVersion{1};

  /*!
   * \brief SOME/IP event ID.
   */
  static constexpr ::amsr::someip_protocol::internal::EventId kEventId{0x9009};
  
  /*!
   * \brief Deserializer type for this event
   */
  static constexpr  ::amsr::someipd_app_protocol::internal::MessageType kMessageType{::amsr::someipd_app_protocol::internal::MessageType::kSomeIp};

  /*!
   * \brief Datatype of the event sample.
   * \details AsrPath: /DataTypes/ImplementationDataTypes/BMSChrgSt_Enum_Idt
   */
  using SampleType = ::gwm::pt_batterypacksys::batterypacksys_idt::BMSChrgSt_Enum_Idt;

  /*!
   * \brief Serializer for event payload.
   */
  using PayloadSerializer = ::amsr::someip_protocol::internal::dataprototype_serializer::gwm_pt_batterypacksys_bmsstsrv_si::fields::SerializerBMSChrgSt;

  /*!
   * \brief Used session handler.
   * \details Session handling is enabled for the event.
   * \trace CREQ-SomeIpBinding-SessionHandling
   */
  using SessionHandler = ::amsr::someip_binding::internal::SessionHandler;

  /*!
   * \brief E2E profile configuration.
   * \details No E2E protection configured for the event.
   */
  using E2eProfileConfig = void;
};

/*!
 * \brief SOME/IP Skeleton event manager type for event 'BMSChrgSt'.
 */
using SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSChrgSt =
    ::amsr::someip_binding_transformation_layer::internal::SkeletonEventXf<SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSChrgSt>;


// ---- Field notifier 'BMSDCChrgCnctSt' -------------------------------------------

/*!
 * \brief Skeleton-side configuration struct for event 'BMSDCChrgCnctSt'.
 */
struct SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSDCChrgCnctSt {
  /*!
   * \brief SOME/IP service ID.
   */
  static constexpr ::amsr::someip_protocol::internal::ServiceId kServiceId{0x4008};

  /*!
   * \brief SOME/IP major version.
   */
  static constexpr ::amsr::someip_protocol::internal::InterfaceVersion kMajorVersion{1};

  /*!
   * \brief SOME/IP event ID.
   */
  static constexpr ::amsr::someip_protocol::internal::EventId kEventId{0x9008};
  
  /*!
   * \brief Deserializer type for this event
   */
  static constexpr  ::amsr::someipd_app_protocol::internal::MessageType kMessageType{::amsr::someipd_app_protocol::internal::MessageType::kSomeIp};

  /*!
   * \brief Datatype of the event sample.
   * \details AsrPath: /DataTypes/ImplementationDataTypes/BMSDCChrgCnctSt_Enum_Idt
   */
  using SampleType = ::gwm::pt_batterypacksys::batterypacksys_idt::BMSDCChrgCnctSt_Enum_Idt;

  /*!
   * \brief Serializer for event payload.
   */
  using PayloadSerializer = ::amsr::someip_protocol::internal::dataprototype_serializer::gwm_pt_batterypacksys_bmsstsrv_si::fields::SerializerBMSDCChrgCnctSt;

  /*!
   * \brief Used session handler.
   * \details Session handling is enabled for the event.
   * \trace CREQ-SomeIpBinding-SessionHandling
   */
  using SessionHandler = ::amsr::someip_binding::internal::SessionHandler;

  /*!
   * \brief E2E profile configuration.
   * \details No E2E protection configured for the event.
   */
  using E2eProfileConfig = void;
};

/*!
 * \brief SOME/IP Skeleton event manager type for event 'BMSDCChrgCnctSt'.
 */
using SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSDCChrgCnctSt =
    ::amsr::someip_binding_transformation_layer::internal::SkeletonEventXf<SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSDCChrgCnctSt>;


// ---- Field notifier 'BMSDCChrgEndReas' -------------------------------------------

/*!
 * \brief Skeleton-side configuration struct for event 'BMSDCChrgEndReas'.
 */
struct SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSDCChrgEndReas {
  /*!
   * \brief SOME/IP service ID.
   */
  static constexpr ::amsr::someip_protocol::internal::ServiceId kServiceId{0x4008};

  /*!
   * \brief SOME/IP major version.
   */
  static constexpr ::amsr::someip_protocol::internal::InterfaceVersion kMajorVersion{1};

  /*!
   * \brief SOME/IP event ID.
   */
  static constexpr ::amsr::someip_protocol::internal::EventId kEventId{0x900F};
  
  /*!
   * \brief Deserializer type for this event
   */
  static constexpr  ::amsr::someipd_app_protocol::internal::MessageType kMessageType{::amsr::someipd_app_protocol::internal::MessageType::kSomeIp};

  /*!
   * \brief Datatype of the event sample.
   * \details AsrPath: /DataTypes/ImplementationDataTypes/BMSDCChrgEndReas_Enum_Idt
   */
  using SampleType = ::gwm::pt_batterypacksys::batterypacksys_idt::BMSDCChrgEndReas_Enum_Idt;

  /*!
   * \brief Serializer for event payload.
   */
  using PayloadSerializer = ::amsr::someip_protocol::internal::dataprototype_serializer::gwm_pt_batterypacksys_bmsstsrv_si::fields::SerializerBMSDCChrgEndReas;

  /*!
   * \brief Used session handler.
   * \details Session handling is enabled for the event.
   * \trace CREQ-SomeIpBinding-SessionHandling
   */
  using SessionHandler = ::amsr::someip_binding::internal::SessionHandler;

  /*!
   * \brief E2E profile configuration.
   * \details No E2E protection configured for the event.
   */
  using E2eProfileConfig = void;
};

/*!
 * \brief SOME/IP Skeleton event manager type for event 'BMSDCChrgEndReas'.
 */
using SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSDCChrgEndReas =
    ::amsr::someip_binding_transformation_layer::internal::SkeletonEventXf<SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSDCChrgEndReas>;


// ---- Field notifier 'BMSDchrgPrmsSt' -------------------------------------------

/*!
 * \brief Skeleton-side configuration struct for event 'BMSDchrgPrmsSt'.
 */
struct SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSDchrgPrmsSt {
  /*!
   * \brief SOME/IP service ID.
   */
  static constexpr ::amsr::someip_protocol::internal::ServiceId kServiceId{0x4008};

  /*!
   * \brief SOME/IP major version.
   */
  static constexpr ::amsr::someip_protocol::internal::InterfaceVersion kMajorVersion{1};

  /*!
   * \brief SOME/IP event ID.
   */
  static constexpr ::amsr::someip_protocol::internal::EventId kEventId{0x900D};
  
  /*!
   * \brief Deserializer type for this event
   */
  static constexpr  ::amsr::someipd_app_protocol::internal::MessageType kMessageType{::amsr::someipd_app_protocol::internal::MessageType::kSomeIp};

  /*!
   * \brief Datatype of the event sample.
   * \details AsrPath: /DataTypes/ImplementationDataTypes/BMSDchrgPrmsSt_Enum_Idt
   */
  using SampleType = ::gwm::pt_batterypacksys::batterypacksys_idt::BMSDchrgPrmsSt_Enum_Idt;

  /*!
   * \brief Serializer for event payload.
   */
  using PayloadSerializer = ::amsr::someip_protocol::internal::dataprototype_serializer::gwm_pt_batterypacksys_bmsstsrv_si::fields::SerializerBMSDchrgPrmsSt;

  /*!
   * \brief Used session handler.
   * \details Session handling is enabled for the event.
   * \trace CREQ-SomeIpBinding-SessionHandling
   */
  using SessionHandler = ::amsr::someip_binding::internal::SessionHandler;

  /*!
   * \brief E2E profile configuration.
   * \details No E2E protection configured for the event.
   */
  using E2eProfileConfig = void;
};

/*!
 * \brief SOME/IP Skeleton event manager type for event 'BMSDchrgPrmsSt'.
 */
using SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSDchrgPrmsSt =
    ::amsr::someip_binding_transformation_layer::internal::SkeletonEventXf<SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSDchrgPrmsSt>;


// ---- Field notifier 'BMSHeatRunawaySt' -------------------------------------------

/*!
 * \brief Skeleton-side configuration struct for event 'BMSHeatRunawaySt'.
 */
struct SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSHeatRunawaySt {
  /*!
   * \brief SOME/IP service ID.
   */
  static constexpr ::amsr::someip_protocol::internal::ServiceId kServiceId{0x4008};

  /*!
   * \brief SOME/IP major version.
   */
  static constexpr ::amsr::someip_protocol::internal::InterfaceVersion kMajorVersion{1};

  /*!
   * \brief SOME/IP event ID.
   */
  static constexpr ::amsr::someip_protocol::internal::EventId kEventId{0x900E};
  
  /*!
   * \brief Deserializer type for this event
   */
  static constexpr  ::amsr::someipd_app_protocol::internal::MessageType kMessageType{::amsr::someipd_app_protocol::internal::MessageType::kSomeIp};

  /*!
   * \brief Datatype of the event sample.
   * \details AsrPath: /DataTypes/ImplementationDataTypes/BMSHeatRunawaySt_Enum_Idt
   */
  using SampleType = ::gwm::pt_batterypacksys::batterypacksys_idt::BMSHeatRunawaySt_Enum_Idt;

  /*!
   * \brief Serializer for event payload.
   */
  using PayloadSerializer = ::amsr::someip_protocol::internal::dataprototype_serializer::gwm_pt_batterypacksys_bmsstsrv_si::fields::SerializerBMSHeatRunawaySt;

  /*!
   * \brief Used session handler.
   * \details Session handling is enabled for the event.
   * \trace CREQ-SomeIpBinding-SessionHandling
   */
  using SessionHandler = ::amsr::someip_binding::internal::SessionHandler;

  /*!
   * \brief E2E profile configuration.
   * \details No E2E protection configured for the event.
   */
  using E2eProfileConfig = void;
};

/*!
 * \brief SOME/IP Skeleton event manager type for event 'BMSHeatRunawaySt'.
 */
using SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSHeatRunawaySt =
    ::amsr::someip_binding_transformation_layer::internal::SkeletonEventXf<SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSHeatRunawaySt>;


// ---- Field notifier 'BMSInnerSOCSt' -------------------------------------------

/*!
 * \brief Skeleton-side configuration struct for event 'BMSInnerSOCSt'.
 */
struct SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSInnerSOCSt {
  /*!
   * \brief SOME/IP service ID.
   */
  static constexpr ::amsr::someip_protocol::internal::ServiceId kServiceId{0x4008};

  /*!
   * \brief SOME/IP major version.
   */
  static constexpr ::amsr::someip_protocol::internal::InterfaceVersion kMajorVersion{1};

  /*!
   * \brief SOME/IP event ID.
   */
  static constexpr ::amsr::someip_protocol::internal::EventId kEventId{0x900C};
  
  /*!
   * \brief Deserializer type for this event
   */
  static constexpr  ::amsr::someipd_app_protocol::internal::MessageType kMessageType{::amsr::someipd_app_protocol::internal::MessageType::kSomeIp};

  /*!
   * \brief Datatype of the event sample.
   * \details AsrPath: /DataTypes/ImplementationDataTypes/BMSInnerSOCSt_Struct_Idt
   */
  using SampleType = ::gwm::pt_batterypacksys::batterypacksys_idt::BMSInnerSOCSt_Struct_Idt;

  /*!
   * \brief Serializer for event payload.
   */
  using PayloadSerializer = ::amsr::someip_protocol::internal::dataprototype_serializer::gwm_pt_batterypacksys_bmsstsrv_si::fields::SerializerBMSInnerSOCSt;

  /*!
   * \brief Used session handler.
   * \details Session handling is enabled for the event.
   * \trace CREQ-SomeIpBinding-SessionHandling
   */
  using SessionHandler = ::amsr::someip_binding::internal::SessionHandler;

  /*!
   * \brief E2E profile configuration.
   * \details No E2E protection configured for the event.
   */
  using E2eProfileConfig = void;
};

/*!
 * \brief SOME/IP Skeleton event manager type for event 'BMSInnerSOCSt'.
 */
using SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSInnerSOCSt =
    ::amsr::someip_binding_transformation_layer::internal::SkeletonEventXf<SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSInnerSOCSt>;


// ---- Field notifier 'BMSIntelTempMgtSt' -------------------------------------------

/*!
 * \brief Skeleton-side configuration struct for event 'BMSIntelTempMgtSt'.
 */
struct SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSIntelTempMgtSt {
  /*!
   * \brief SOME/IP service ID.
   */
  static constexpr ::amsr::someip_protocol::internal::ServiceId kServiceId{0x4008};

  /*!
   * \brief SOME/IP major version.
   */
  static constexpr ::amsr::someip_protocol::internal::InterfaceVersion kMajorVersion{1};

  /*!
   * \brief SOME/IP event ID.
   */
  static constexpr ::amsr::someip_protocol::internal::EventId kEventId{0x900B};
  
  /*!
   * \brief Deserializer type for this event
   */
  static constexpr  ::amsr::someipd_app_protocol::internal::MessageType kMessageType{::amsr::someipd_app_protocol::internal::MessageType::kSomeIp};

  /*!
   * \brief Datatype of the event sample.
   * \details AsrPath: /DataTypes/ImplementationDataTypes/BMSIntelTempMgtSt_Enum_Idt
   */
  using SampleType = ::gwm::pt_batterypacksys::batterypacksys_idt::BMSIntelTempMgtSt_Enum_Idt;

  /*!
   * \brief Serializer for event payload.
   */
  using PayloadSerializer = ::amsr::someip_protocol::internal::dataprototype_serializer::gwm_pt_batterypacksys_bmsstsrv_si::fields::SerializerBMSIntelTempMgtSt;

  /*!
   * \brief Used session handler.
   * \details Session handling is enabled for the event.
   * \trace CREQ-SomeIpBinding-SessionHandling
   */
  using SessionHandler = ::amsr::someip_binding::internal::SessionHandler;

  /*!
   * \brief E2E profile configuration.
   * \details No E2E protection configured for the event.
   */
  using E2eProfileConfig = void;
};

/*!
 * \brief SOME/IP Skeleton event manager type for event 'BMSIntelTempMgtSt'.
 */
using SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSIntelTempMgtSt =
    ::amsr::someip_binding_transformation_layer::internal::SkeletonEventXf<SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSIntelTempMgtSt>;


// ---- Field notifier 'BMSLowTempSt' -------------------------------------------

/*!
 * \brief Skeleton-side configuration struct for event 'BMSLowTempSt'.
 */
struct SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSLowTempSt {
  /*!
   * \brief SOME/IP service ID.
   */
  static constexpr ::amsr::someip_protocol::internal::ServiceId kServiceId{0x4008};

  /*!
   * \brief SOME/IP major version.
   */
  static constexpr ::amsr::someip_protocol::internal::InterfaceVersion kMajorVersion{1};

  /*!
   * \brief SOME/IP event ID.
   */
  static constexpr ::amsr::someip_protocol::internal::EventId kEventId{0x900A};
  
  /*!
   * \brief Deserializer type for this event
   */
  static constexpr  ::amsr::someipd_app_protocol::internal::MessageType kMessageType{::amsr::someipd_app_protocol::internal::MessageType::kSomeIp};

  /*!
   * \brief Datatype of the event sample.
   * \details AsrPath: /DataTypes/ImplementationDataTypes/BMSLowTempSt_Struct_Idt
   */
  using SampleType = ::gwm::pt_batterypacksys::batterypacksys_idt::BMSLowTempSt_Struct_Idt;

  /*!
   * \brief Serializer for event payload.
   */
  using PayloadSerializer = ::amsr::someip_protocol::internal::dataprototype_serializer::gwm_pt_batterypacksys_bmsstsrv_si::fields::SerializerBMSLowTempSt;

  /*!
   * \brief Used session handler.
   * \details Session handling is enabled for the event.
   * \trace CREQ-SomeIpBinding-SessionHandling
   */
  using SessionHandler = ::amsr::someip_binding::internal::SessionHandler;

  /*!
   * \brief E2E profile configuration.
   * \details No E2E protection configured for the event.
   */
  using E2eProfileConfig = void;
};

/*!
 * \brief SOME/IP Skeleton event manager type for event 'BMSLowTempSt'.
 */
using SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSLowTempSt =
    ::amsr::someip_binding_transformation_layer::internal::SkeletonEventXf<SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSLowTempSt>;


// ---- Field notifier 'BMSRmtPreHeatSt' -------------------------------------------

/*!
 * \brief Skeleton-side configuration struct for event 'BMSRmtPreHeatSt'.
 */
struct SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSRmtPreHeatSt {
  /*!
   * \brief SOME/IP service ID.
   */
  static constexpr ::amsr::someip_protocol::internal::ServiceId kServiceId{0x4008};

  /*!
   * \brief SOME/IP major version.
   */
  static constexpr ::amsr::someip_protocol::internal::InterfaceVersion kMajorVersion{1};

  /*!
   * \brief SOME/IP event ID.
   */
  static constexpr ::amsr::someip_protocol::internal::EventId kEventId{0x9006};
  
  /*!
   * \brief Deserializer type for this event
   */
  static constexpr  ::amsr::someipd_app_protocol::internal::MessageType kMessageType{::amsr::someipd_app_protocol::internal::MessageType::kSomeIp};

  /*!
   * \brief Datatype of the event sample.
   * \details AsrPath: /DataTypes/ImplementationDataTypes/BMSRmtPreHeatSt_Enum_Idt
   */
  using SampleType = ::gwm::pt_batterypacksys::batterypacksys_idt::BMSRmtPreHeatSt_Enum_Idt;

  /*!
   * \brief Serializer for event payload.
   */
  using PayloadSerializer = ::amsr::someip_protocol::internal::dataprototype_serializer::gwm_pt_batterypacksys_bmsstsrv_si::fields::SerializerBMSRmtPreHeatSt;

  /*!
   * \brief Used session handler.
   * \details Session handling is enabled for the event.
   * \trace CREQ-SomeIpBinding-SessionHandling
   */
  using SessionHandler = ::amsr::someip_binding::internal::SessionHandler;

  /*!
   * \brief E2E profile configuration.
   * \details No E2E protection configured for the event.
   */
  using E2eProfileConfig = void;
};

/*!
 * \brief SOME/IP Skeleton event manager type for event 'BMSRmtPreHeatSt'.
 */
using SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSRmtPreHeatSt =
    ::amsr::someip_binding_transformation_layer::internal::SkeletonEventXf<SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSRmtPreHeatSt>;


// ---- Field notifier 'BMSSafCnctSt' -------------------------------------------

/*!
 * \brief Skeleton-side configuration struct for event 'BMSSafCnctSt'.
 */
struct SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSSafCnctSt {
  /*!
   * \brief SOME/IP service ID.
   */
  static constexpr ::amsr::someip_protocol::internal::ServiceId kServiceId{0x4008};

  /*!
   * \brief SOME/IP major version.
   */
  static constexpr ::amsr::someip_protocol::internal::InterfaceVersion kMajorVersion{1};

  /*!
   * \brief SOME/IP event ID.
   */
  static constexpr ::amsr::someip_protocol::internal::EventId kEventId{0x9007};
  
  /*!
   * \brief Deserializer type for this event
   */
  static constexpr  ::amsr::someipd_app_protocol::internal::MessageType kMessageType{::amsr::someipd_app_protocol::internal::MessageType::kSomeIp};

  /*!
   * \brief Datatype of the event sample.
   * \details AsrPath: /DataTypes/ImplementationDataTypes/BMSSafCnctSt_Enum_Idt
   */
  using SampleType = ::gwm::pt_batterypacksys::batterypacksys_idt::BMSSafCnctSt_Enum_Idt;

  /*!
   * \brief Serializer for event payload.
   */
  using PayloadSerializer = ::amsr::someip_protocol::internal::dataprototype_serializer::gwm_pt_batterypacksys_bmsstsrv_si::fields::SerializerBMSSafCnctSt;

  /*!
   * \brief Used session handler.
   * \details Session handling is enabled for the event.
   * \trace CREQ-SomeIpBinding-SessionHandling
   */
  using SessionHandler = ::amsr::someip_binding::internal::SessionHandler;

  /*!
   * \brief E2E profile configuration.
   * \details No E2E protection configured for the event.
   */
  using E2eProfileConfig = void;
};

/*!
 * \brief SOME/IP Skeleton event manager type for event 'BMSSafCnctSt'.
 */
using SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSSafCnctSt =
    ::amsr::someip_binding_transformation_layer::internal::SkeletonEventXf<SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSSafCnctSt>;


// ---- Field notifier 'BmsErrSt' -------------------------------------------

/*!
 * \brief Skeleton-side configuration struct for event 'BmsErrSt'.
 */
struct SI_BmsStSrvSkeletonSomeIpEventConfigurationBmsErrSt {
  /*!
   * \brief SOME/IP service ID.
   */
  static constexpr ::amsr::someip_protocol::internal::ServiceId kServiceId{0x4008};

  /*!
   * \brief SOME/IP major version.
   */
  static constexpr ::amsr::someip_protocol::internal::InterfaceVersion kMajorVersion{1};

  /*!
   * \brief SOME/IP event ID.
   */
  static constexpr ::amsr::someip_protocol::internal::EventId kEventId{0x9003};
  
  /*!
   * \brief Deserializer type for this event
   */
  static constexpr  ::amsr::someipd_app_protocol::internal::MessageType kMessageType{::amsr::someipd_app_protocol::internal::MessageType::kSomeIp};

  /*!
   * \brief Datatype of the event sample.
   * \details AsrPath: /DataTypes/ImplementationDataTypes/BmsErrSt_Struct_Idt
   */
  using SampleType = ::gwm::pt_batterypacksys::batterypacksys_idt::BmsErrSt_Struct_Idt;

  /*!
   * \brief Serializer for event payload.
   */
  using PayloadSerializer = ::amsr::someip_protocol::internal::dataprototype_serializer::gwm_pt_batterypacksys_bmsstsrv_si::fields::SerializerBmsErrSt;

  /*!
   * \brief Used session handler.
   * \details Session handling is enabled for the event.
   * \trace CREQ-SomeIpBinding-SessionHandling
   */
  using SessionHandler = ::amsr::someip_binding::internal::SessionHandler;

  /*!
   * \brief E2E profile configuration.
   * \details No E2E protection configured for the event.
   */
  using E2eProfileConfig = void;
};

/*!
 * \brief SOME/IP Skeleton event manager type for event 'BmsErrSt'.
 */
using SI_BmsStSrvSkeletonSomeIpFieldNotifierBmsErrSt =
    ::amsr::someip_binding_transformation_layer::internal::SkeletonEventXf<SI_BmsStSrvSkeletonSomeIpEventConfigurationBmsErrSt>;


// ---- Field notifier 'BmsHVILSt' -------------------------------------------

/*!
 * \brief Skeleton-side configuration struct for event 'BmsHVILSt'.
 */
struct SI_BmsStSrvSkeletonSomeIpEventConfigurationBmsHVILSt {
  /*!
   * \brief SOME/IP service ID.
   */
  static constexpr ::amsr::someip_protocol::internal::ServiceId kServiceId{0x4008};

  /*!
   * \brief SOME/IP major version.
   */
  static constexpr ::amsr::someip_protocol::internal::InterfaceVersion kMajorVersion{1};

  /*!
   * \brief SOME/IP event ID.
   */
  static constexpr ::amsr::someip_protocol::internal::EventId kEventId{0x9005};
  
  /*!
   * \brief Deserializer type for this event
   */
  static constexpr  ::amsr::someipd_app_protocol::internal::MessageType kMessageType{::amsr::someipd_app_protocol::internal::MessageType::kSomeIp};

  /*!
   * \brief Datatype of the event sample.
   * \details AsrPath: /DataTypes/ImplementationDataTypes/BmsHVILSt_Enum_Idt
   */
  using SampleType = ::gwm::pt_batterypacksys::batterypacksys_idt::BmsHVILSt_Enum_Idt;

  /*!
   * \brief Serializer for event payload.
   */
  using PayloadSerializer = ::amsr::someip_protocol::internal::dataprototype_serializer::gwm_pt_batterypacksys_bmsstsrv_si::fields::SerializerBmsHVILSt;

  /*!
   * \brief Used session handler.
   * \details Session handling is enabled for the event.
   * \trace CREQ-SomeIpBinding-SessionHandling
   */
  using SessionHandler = ::amsr::someip_binding::internal::SessionHandler;

  /*!
   * \brief E2E profile configuration.
   * \details No E2E protection configured for the event.
   */
  using E2eProfileConfig = void;
};

/*!
 * \brief SOME/IP Skeleton event manager type for event 'BmsHVILSt'.
 */
using SI_BmsStSrvSkeletonSomeIpFieldNotifierBmsHVILSt =
    ::amsr::someip_binding_transformation_layer::internal::SkeletonEventXf<SI_BmsStSrvSkeletonSomeIpEventConfigurationBmsHVILSt>;


// ---- Field notifier 'BmsIsoMeasSt' -------------------------------------------

/*!
 * \brief Skeleton-side configuration struct for event 'BmsIsoMeasSt'.
 */
struct SI_BmsStSrvSkeletonSomeIpEventConfigurationBmsIsoMeasSt {
  /*!
   * \brief SOME/IP service ID.
   */
  static constexpr ::amsr::someip_protocol::internal::ServiceId kServiceId{0x4008};

  /*!
   * \brief SOME/IP major version.
   */
  static constexpr ::amsr::someip_protocol::internal::InterfaceVersion kMajorVersion{1};

  /*!
   * \brief SOME/IP event ID.
   */
  static constexpr ::amsr::someip_protocol::internal::EventId kEventId{0x9004};
  
  /*!
   * \brief Deserializer type for this event
   */
  static constexpr  ::amsr::someipd_app_protocol::internal::MessageType kMessageType{::amsr::someipd_app_protocol::internal::MessageType::kSomeIp};

  /*!
   * \brief Datatype of the event sample.
   * \details AsrPath: /DataTypes/ImplementationDataTypes/BmsIsoMeasSt_Enum_Idt
   */
  using SampleType = ::gwm::pt_batterypacksys::batterypacksys_idt::BmsIsoMeasSt_Enum_Idt;

  /*!
   * \brief Serializer for event payload.
   */
  using PayloadSerializer = ::amsr::someip_protocol::internal::dataprototype_serializer::gwm_pt_batterypacksys_bmsstsrv_si::fields::SerializerBmsIsoMeasSt;

  /*!
   * \brief Used session handler.
   * \details Session handling is enabled for the event.
   * \trace CREQ-SomeIpBinding-SessionHandling
   */
  using SessionHandler = ::amsr::someip_binding::internal::SessionHandler;

  /*!
   * \brief E2E profile configuration.
   * \details No E2E protection configured for the event.
   */
  using E2eProfileConfig = void;
};

/*!
 * \brief SOME/IP Skeleton event manager type for event 'BmsIsoMeasSt'.
 */
using SI_BmsStSrvSkeletonSomeIpFieldNotifierBmsIsoMeasSt =
    ::amsr::someip_binding_transformation_layer::internal::SkeletonEventXf<SI_BmsStSrvSkeletonSomeIpEventConfigurationBmsIsoMeasSt>;


// ---- Field notifier 'BmsSt' -------------------------------------------

/*!
 * \brief Skeleton-side configuration struct for event 'BmsSt'.
 */
struct SI_BmsStSrvSkeletonSomeIpEventConfigurationBmsSt {
  /*!
   * \brief SOME/IP service ID.
   */
  static constexpr ::amsr::someip_protocol::internal::ServiceId kServiceId{0x4008};

  /*!
   * \brief SOME/IP major version.
   */
  static constexpr ::amsr::someip_protocol::internal::InterfaceVersion kMajorVersion{1};

  /*!
   * \brief SOME/IP event ID.
   */
  static constexpr ::amsr::someip_protocol::internal::EventId kEventId{0x9002};
  
  /*!
   * \brief Deserializer type for this event
   */
  static constexpr  ::amsr::someipd_app_protocol::internal::MessageType kMessageType{::amsr::someipd_app_protocol::internal::MessageType::kSomeIp};

  /*!
   * \brief Datatype of the event sample.
   * \details AsrPath: /DataTypes/ImplementationDataTypes/BmsSt_Enum_Idt
   */
  using SampleType = ::gwm::pt_batterypacksys::batterypacksys_idt::BmsSt_Enum_Idt;

  /*!
   * \brief Serializer for event payload.
   */
  using PayloadSerializer = ::amsr::someip_protocol::internal::dataprototype_serializer::gwm_pt_batterypacksys_bmsstsrv_si::fields::SerializerBmsSt;

  /*!
   * \brief Used session handler.
   * \details Session handling is enabled for the event.
   * \trace CREQ-SomeIpBinding-SessionHandling
   */
  using SessionHandler = ::amsr::someip_binding::internal::SessionHandler;

  /*!
   * \brief E2E profile configuration.
   * \details No E2E protection configured for the event.
   */
  using E2eProfileConfig = void;
};

/*!
 * \brief SOME/IP Skeleton event manager type for event 'BmsSt'.
 */
using SI_BmsStSrvSkeletonSomeIpFieldNotifierBmsSt =
    ::amsr::someip_binding_transformation_layer::internal::SkeletonEventXf<SI_BmsStSrvSkeletonSomeIpEventConfigurationBmsSt>;


// ---- Field notifier 'HVBattSOCLim' -------------------------------------------

/*!
 * \brief Skeleton-side configuration struct for event 'HVBattSOCLim'.
 */
struct SI_BmsStSrvSkeletonSomeIpEventConfigurationHVBattSOCLim {
  /*!
   * \brief SOME/IP service ID.
   */
  static constexpr ::amsr::someip_protocol::internal::ServiceId kServiceId{0x4008};

  /*!
   * \brief SOME/IP major version.
   */
  static constexpr ::amsr::someip_protocol::internal::InterfaceVersion kMajorVersion{1};

  /*!
   * \brief SOME/IP event ID.
   */
  static constexpr ::amsr::someip_protocol::internal::EventId kEventId{0x9010};
  
  /*!
   * \brief Deserializer type for this event
   */
  static constexpr  ::amsr::someipd_app_protocol::internal::MessageType kMessageType{::amsr::someipd_app_protocol::internal::MessageType::kSomeIp};

  /*!
   * \brief Datatype of the event sample.
   * \details AsrPath: /DataTypes/ImplementationDataTypes/HVBattSOCLim_Enum_Idt
   */
  using SampleType = ::gwm::pt_batterypacksys::batterypacksys_idt::HVBattSOCLim_Enum_Idt;

  /*!
   * \brief Serializer for event payload.
   */
  using PayloadSerializer = ::amsr::someip_protocol::internal::dataprototype_serializer::gwm_pt_batterypacksys_bmsstsrv_si::fields::SerializerHVBattSOCLim;

  /*!
   * \brief Used session handler.
   * \details Session handling is enabled for the event.
   * \trace CREQ-SomeIpBinding-SessionHandling
   */
  using SessionHandler = ::amsr::someip_binding::internal::SessionHandler;

  /*!
   * \brief E2E profile configuration.
   * \details No E2E protection configured for the event.
   */
  using E2eProfileConfig = void;
};

/*!
 * \brief SOME/IP Skeleton event manager type for event 'HVBattSOCLim'.
 */
using SI_BmsStSrvSkeletonSomeIpFieldNotifierHVBattSOCLim =
    ::amsr::someip_binding_transformation_layer::internal::SkeletonEventXf<SI_BmsStSrvSkeletonSomeIpEventConfigurationHVBattSOCLim>;


// ---- Field notifier 'HVMaiSwtSt' -------------------------------------------

/*!
 * \brief Skeleton-side configuration struct for event 'HVMaiSwtSt'.
 */
struct SI_BmsStSrvSkeletonSomeIpEventConfigurationHVMaiSwtSt {
  /*!
   * \brief SOME/IP service ID.
   */
  static constexpr ::amsr::someip_protocol::internal::ServiceId kServiceId{0x4008};

  /*!
   * \brief SOME/IP major version.
   */
  static constexpr ::amsr::someip_protocol::internal::InterfaceVersion kMajorVersion{1};

  /*!
   * \brief SOME/IP event ID.
   */
  static constexpr ::amsr::someip_protocol::internal::EventId kEventId{0x9001};
  
  /*!
   * \brief Deserializer type for this event
   */
  static constexpr  ::amsr::someipd_app_protocol::internal::MessageType kMessageType{::amsr::someipd_app_protocol::internal::MessageType::kSomeIp};

  /*!
   * \brief Datatype of the event sample.
   * \details AsrPath: /DataTypes/ImplementationDataTypes/HVMaiSwtSt_Struct_Idt
   */
  using SampleType = ::gwm::pt_batterypacksys::batterypacksys_idt::HVMaiSwtSt_Struct_Idt;

  /*!
   * \brief Serializer for event payload.
   */
  using PayloadSerializer = ::amsr::someip_protocol::internal::dataprototype_serializer::gwm_pt_batterypacksys_bmsstsrv_si::fields::SerializerHVMaiSwtSt;

  /*!
   * \brief Used session handler.
   * \details Session handling is enabled for the event.
   * \trace CREQ-SomeIpBinding-SessionHandling
   */
  using SessionHandler = ::amsr::someip_binding::internal::SessionHandler;

  /*!
   * \brief E2E profile configuration.
   * \details No E2E protection configured for the event.
   */
  using E2eProfileConfig = void;
};

/*!
 * \brief SOME/IP Skeleton event manager type for event 'HVMaiSwtSt'.
 */
using SI_BmsStSrvSkeletonSomeIpFieldNotifierHVMaiSwtSt =
    ::amsr::someip_binding_transformation_layer::internal::SkeletonEventXf<SI_BmsStSrvSkeletonSomeIpEventConfigurationHVMaiSwtSt>;


}  // namespace internal
}  // namespace someip_binding_transformation_layer
}  // namespace amsr
}  // namespace bmsstsrv_si
}  // namespace pt_batterypacksys
}  // namespace gwm

#endif  // DENPENDENCYSRVEXE_INCLUDE_AMSR_SOMEIP_BINDING_TRANSFORMATION_LAYER_INTERNAL_GWM_PT_BATTERYPACKSYS_BMSSTSRV_SI_SI_BMSSTSRV_SKELETON_SOMEIP_EVENT_MANAGER_H_

