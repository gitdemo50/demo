// VECTOR Same Line AutosarC++17_10-A1.1.1: MD_SOMEIPBINDING_AutosarC++17_10-A1.1.1_external_identifiers
/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/src/amsr/someip_binding_transformation_layer/internal/gwm/pt_powersys/obcstsrv_si/SI_OBCStSrv_skeleton_someip_binding.cpp
 *        \brief  SOME/IP skeleton binding of service 'SI_OBCStSrv'
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_someipbinding
 *         Commit ID: 43b33f0283d4d61385b98a3599688499abd86a75
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "amsr/someip_binding_transformation_layer/internal/gwm/pt_powersys/obcstsrv_si/SI_OBCStSrv_skeleton_someip_binding.h"
#include <utility>

namespace amsr {
namespace someip_binding_transformation_layer {
namespace internal {

namespace gwm {
namespace pt_powersys {
namespace obcstsrv_si {

/*!
 * \brief Generated SOME/IP related service ID.
 */
::amsr::someip_protocol::internal::ServiceId constexpr SI_OBCStSrvSkeletonSomeIpBinding::kServiceId;

/*!
 * \brief SOME/IP major version of this service interface deployment.
 */
constexpr ::amsr::someip_protocol::internal::MajorVersion SI_OBCStSrvSkeletonSomeIpBinding::kMajorVersion;

SI_OBCStSrvSkeletonSomeIpBinding::SI_OBCStSrvSkeletonSomeIpBinding(
    ::amsr::someip_protocol::internal::InstanceId const instance_id,
    ::amsr::someip_binding::internal::ServerManagerInterface& someip_binding_server_manager,
    ::gwm::pt_powersys::obcstsrv_si::skeleton::SI_OBCStSrvSkeleton& skeleton)
    : instance_id_(instance_id),
      someip_binding_server_manager_(someip_binding_server_manager),
      skeleton_(skeleton) ,
      event_manager_OBCActT_(instance_id),
      event_manager_OBCChrgLim_(instance_id),
      event_manager_OBCDchrOutpI_(instance_id),
      event_manager_OBCDchrOutpU_(instance_id),
      event_manager_OBCInptAcI_(instance_id),
      event_manager_OBCInptAcU_(instance_id),
      event_manager_OBCOuptDcI_(instance_id),
      event_manager_OBCOuptDcU_(instance_id),
      field_notifier_OBCChrgGunSt_(instance_id),
      field_manager_OBCChrgGunSt_get_(*this, "OBCChrgGunSt" ),
      field_notifier_OBCErrSt_(instance_id),
      field_manager_OBCErrSt_get_(*this, "OBCErrSt" ),
      field_notifier_OBCHVILSt_(instance_id),
      field_manager_OBCHVILSt_get_(*this, "OBCHVILSt" ),
      field_notifier_OBCSt_(instance_id),
      field_manager_OBCSt_get_(*this, "OBCSt" ),
      field_notifier_OBCV2XSt_(instance_id),
      field_manager_OBCV2XSt_get_(*this, "OBCV2XSt" ){
}

void SI_OBCStSrvSkeletonSomeIpBinding::HandleMethodRequest(
  ::amsr::someip_protocol::internal::SomeIpMessageHeader const& header,
  ::amsr::someip_protocol::internal::SomeIpMessage  packet) {
  // Based on the method id -> static dispatching to the method request/response manager
  switch (header.method_id_) {
    case fields::SkeletonOBCChrgGunStGet::kMethodId: {
      field_manager_OBCChrgGunSt_get_.HandleMethodRequest(header, std::move(packet));
      break;
    }
    case fields::SkeletonOBCErrStGet::kMethodId: {
      field_manager_OBCErrSt_get_.HandleMethodRequest(header, std::move(packet));
      break;
    }
    case fields::SkeletonOBCHVILStGet::kMethodId: {
      field_manager_OBCHVILSt_get_.HandleMethodRequest(header, std::move(packet));
      break;
    }
    case fields::SkeletonOBCStGet::kMethodId: {
      field_manager_OBCSt_get_.HandleMethodRequest(header, std::move(packet));
      break;
    }
    case fields::SkeletonOBCV2XStGet::kMethodId: {
      field_manager_OBCV2XSt_get_.HandleMethodRequest(header, std::move(packet));
      break;
    }
    default: {
      // Method implementation is missing.
      SendErrorResponse<static_cast<::amsr::someip_protocol::internal::ReturnCode>(::amsr::someip_protocol::internal::SomeIpReturnCode::kUnknownMethod)>(header);
      break;
    }
  }
}

void SI_OBCStSrvSkeletonSomeIpBinding::SendMethodResponse(::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet) {
  someip_binding_server_manager_.SendMethodResponse(instance_id_, std::move(packet));
}

// ---- Events -----------------------------------------------------------------------------------------------------

// Event 'OBCActT'
SI_OBCStSrvSkeletonSomeIpEventManagerOBCActT* SI_OBCStSrvSkeletonSomeIpBinding::GetEventManagerOBCActT() noexcept {
  return &event_manager_OBCActT_;
}

// Event 'OBCChrgLim'
SI_OBCStSrvSkeletonSomeIpEventManagerOBCChrgLim* SI_OBCStSrvSkeletonSomeIpBinding::GetEventManagerOBCChrgLim() noexcept {
  return &event_manager_OBCChrgLim_;
}

// Event 'OBCDchrOutpI'
SI_OBCStSrvSkeletonSomeIpEventManagerOBCDchrOutpI* SI_OBCStSrvSkeletonSomeIpBinding::GetEventManagerOBCDchrOutpI() noexcept {
  return &event_manager_OBCDchrOutpI_;
}

// Event 'OBCDchrOutpU'
SI_OBCStSrvSkeletonSomeIpEventManagerOBCDchrOutpU* SI_OBCStSrvSkeletonSomeIpBinding::GetEventManagerOBCDchrOutpU() noexcept {
  return &event_manager_OBCDchrOutpU_;
}

// Event 'OBCInptAcI'
SI_OBCStSrvSkeletonSomeIpEventManagerOBCInptAcI* SI_OBCStSrvSkeletonSomeIpBinding::GetEventManagerOBCInptAcI() noexcept {
  return &event_manager_OBCInptAcI_;
}

// Event 'OBCInptAcU'
SI_OBCStSrvSkeletonSomeIpEventManagerOBCInptAcU* SI_OBCStSrvSkeletonSomeIpBinding::GetEventManagerOBCInptAcU() noexcept {
  return &event_manager_OBCInptAcU_;
}

// Event 'OBCOuptDcI'
SI_OBCStSrvSkeletonSomeIpEventManagerOBCOuptDcI* SI_OBCStSrvSkeletonSomeIpBinding::GetEventManagerOBCOuptDcI() noexcept {
  return &event_manager_OBCOuptDcI_;
}

// Event 'OBCOuptDcU'
SI_OBCStSrvSkeletonSomeIpEventManagerOBCOuptDcU* SI_OBCStSrvSkeletonSomeIpBinding::GetEventManagerOBCOuptDcU() noexcept {
  return &event_manager_OBCOuptDcU_;
}

// ---- Fields -----------------------------------------------------------------------------------------------------

// Field 'OBCChrgGunSt'
SI_OBCStSrvSkeletonSomeIpFieldNotifierOBCChrgGunSt* SI_OBCStSrvSkeletonSomeIpBinding::GetFieldNotifierOBCChrgGunSt() noexcept {
  return &field_notifier_OBCChrgGunSt_;
}

// Field 'OBCErrSt'
SI_OBCStSrvSkeletonSomeIpFieldNotifierOBCErrSt* SI_OBCStSrvSkeletonSomeIpBinding::GetFieldNotifierOBCErrSt() noexcept {
  return &field_notifier_OBCErrSt_;
}

// Field 'OBCHVILSt'
SI_OBCStSrvSkeletonSomeIpFieldNotifierOBCHVILSt* SI_OBCStSrvSkeletonSomeIpBinding::GetFieldNotifierOBCHVILSt() noexcept {
  return &field_notifier_OBCHVILSt_;
}

// Field 'OBCSt'
SI_OBCStSrvSkeletonSomeIpFieldNotifierOBCSt* SI_OBCStSrvSkeletonSomeIpBinding::GetFieldNotifierOBCSt() noexcept {
  return &field_notifier_OBCSt_;
}

// Field 'OBCV2XSt'
SI_OBCStSrvSkeletonSomeIpFieldNotifierOBCV2XSt* SI_OBCStSrvSkeletonSomeIpBinding::GetFieldNotifierOBCV2XSt() noexcept {
  return &field_notifier_OBCV2XSt_;
}

}  // namespace internal
}  // namespace someip_binding_transformation_layer
}  // namespace amsr
}  // namespace obcstsrv_si
}  // namespace pt_powersys
}  // namespace gwm

