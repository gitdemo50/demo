/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/src/gwm/pt_powersys/vehchrgnctrlsrv_si/si_vehchrgnctrlsrv.cpp
 *        \brief  Header for service 'SI_VehChrgnCtrlSrv'.
 *
 *      \details  The service interface provides vehicle charging control
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_socal
 *         Commit ID: 209b3c0234013e84e3170ee826c9a8c6022de194
 *********************************************************************************************************************/

#ifndef DENPENDENCYSRVEXE_SRC_GWM_PT_POWERSYS_VEHCHRGNCTRLSRV_SI_SI_VEHCHRGNCTRLSRV_CPP_
#define DENPENDENCYSRVEXE_SRC_GWM_PT_POWERSYS_VEHCHRGNCTRLSRV_SI_SI_VEHCHRGNCTRLSRV_CPP_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "gwm/pt_powersys/vehchrgnctrlsrv_si/SI_VehChrgnCtrlSrv.h"

namespace gwm {
namespace pt_powersys {
namespace vehchrgnctrlsrv_si {

/*!
 * \brief Service Identifier instance.
 */
constexpr ara::com::ServiceIdentifierType SI_VehChrgnCtrlSrv::ServiceIdentifier;

/*!
 * \brief Service shortname path instance.
 */
constexpr vac::container::CStringView SI_VehChrgnCtrlSrv::kServiceShortNamePath;

}  // namespace vehchrgnctrlsrv_si
}  // namespace pt_powersys
}  // namespace gwm

#endif  // DENPENDENCYSRVEXE_SRC_GWM_PT_POWERSYS_VEHCHRGNCTRLSRV_SI_SI_VEHCHRGNCTRLSRV_CPP_
