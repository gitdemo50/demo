// VECTOR SL AutosarC++17_10-A1.1.1: MD_ARAPER_number_of_external_identifiers
/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  TiMgrSrvExe/include/ara/per/key_value_storage_data_visitor.h
 *        \brief
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_persistency_config
 *         Commit ID: 3c577be6a7ef0697cae45fabe324d59e826d2342
 *********************************************************************************************************************/

#ifndef TIMGRSRVEXE_INCLUDE_ARA_PER_KEY_VALUE_STORAGE_DATA_VISITOR_H_
#define TIMGRSRVEXE_INCLUDE_ARA_PER_KEY_VALUE_STORAGE_DATA_VISITOR_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include <algorithm>
#include <cstdint>
#include <limits>
#include <map>
#include <string>
#include <utility>
#include <vector>

#include "vac/container/c_string_view.h"
#include "vac/container/string_literals.h"
#include "vac/language/byte.h"
#include "vac/language/overload.h"

#include "amsr/json/writer.h"
#include "amsr/json/writer/serializers/stl/primitives.h"
#include "amsr/json/writer/serializers/stl/sequence_containers.h"
#include "amsr/json/writer/serializers/vac/primitives.h"
#include "amsr/json/writer/serializers/vac/sequence_containers.h"

#include "ara/per/internal/config/key_value_storage/key_value_storage_decl.h"
#include "ara/per/detail/dynamic_key_value_storage_decl.h"
#include "ara/per/key_value_storage_data_types.h"
#include "ara/per/key_value_storage_variant.h"

// Static serializers
#include "amsr/per/serializer/serialize_array.h"
#include "amsr/per/serializer/serialize_bool.h"
#include "amsr/per/serializer/serialize_map.h"
#include "amsr/per/serializer/serialize_optional.h"
#include "amsr/per/serializer/serialize_string.h"
#include "amsr/per/serializer/serialize_trivially_copyable.h"
#include "amsr/per/serializer/serialize_vector.h"

namespace ara {
namespace per {
namespace detail {

// VECTOR Disable AutosarC++17_10-A5.1.8: MD_ARAPER_AutosarC++17_10-A5.1.8_nested_lambda

// VECTOR NC AutosarC++17_10-M7.3.6: MD_ARAPER_using_in_header
// VECTOR NC AutosarC++17_10-M7.3.4: MD_ARAPER_AutosarC++17_10-A7.3.4_using_shall_not_be_used
using namespace amsr::json;  // NOLINT(build/namespaces)

// VECTOR NC AutosarC++17_10-M7.3.6: MD_ARAPER_using_in_header
// VECTOR NC AutosarC++17_10-M7.3.4: MD_ARAPER_AutosarC++17_10-A7.3.4_using_shall_not_be_used
using namespace amsr::per::serializer;  // NOLINT(build/namespaces)

/*!
 * \brief JkeyType for the value type
 */
constexpr static JKeyType kJsonKeyType{internal::config::key_value_storage::detail::value_type};

/*!
 * \brief JKeyType for the data type
 */
constexpr static JKeyType kJsonKeyData{internal::config::key_value_storage::detail::data_type};

// VECTOR NC Metric-OO.WMC.One: MD_ARAPER_Metric.OO.WMC.One
/*!
 * \brief Visitor to serialize the value of a variant
 */
class KeyValueStorageDataVisitor final {
 public:
  /*!
   * \brief Return type of the JSON variant serializer
   */
  using ReturnType = ObjectStart;

  /*!
   * \brief Constructor of the Visitor to serialize a element of the KVS variant.
   * \param os The ObjectStart element to write the element.
   * \param buffer_size The size of the serializer buffer in bytes.
   */
  explicit KeyValueStorageDataVisitor(ObjectStart&& os, std::size_t buffer_size = 10000)
      : os_{std::move(os)},
        buffer_{buffer_size} {}

  /*!
   * \brief () operator for NotSet value. A NotSet value is not serialized.
   */
  ReturnType operator()(NotSet const&) { return std::move(os_); }

  /*!
   * \brief () operator for string value
   * \param value the string value
   * \return The ObjectStart element
   */
  ReturnType operator()(std::string const& value) {
    JBinStringType const value_string{JBinString(value)};
    return std::move(os_) << kJsonKeyType << JBinString(internal::config::key_value_storage::detail::str_type) << kJsonKeyData << value_string;
  }

  /*!
   * \brief () operator for bool value
   * \param value the bool value
   * \return The ObjectStart element
   */
  ReturnType operator()(bool const& value) {
    return std::move(os_) << kJsonKeyType << JBinString(internal::config::key_value_storage::detail::bool_type) << kJsonKeyData << value;
  }

  /*!
   * \brief () operator for a std::vector<vac::language::byte> data type.
   * \param value a vector of vac::language::byte
   * \return The ObjectStart element
   */
  ReturnType operator()(std::vector<vac::language::byte> const& value) {
    // VECTOR NL AutosarC++17_10-A5.2.4: MD_ARAPER_AutosarC++17_10-A5.2.4_char_cast
    ara::core::Span<char const> const char_buffer{reinterpret_cast<char const*>(value.data()), value.size()};
    return std::move(os_) << kJsonKeyType << JBinString(internal::config::key_value_storage::detail::bin_type) << kJsonKeyData << JBin(char_buffer);
  }

  /*!
   * \brief () operator for std::uint8_t data type.
   * \param value A basic data type value
   * \return The ObjectStart element
   */
  ReturnType operator()(std::uint8_t const value) {
    return std::move(os_) << kJsonKeyType << JBinString(internal::config::key_value_storage::detail::ui8_type) << kJsonKeyData << value;
  }

  /*!
   * \brief () operator for std::uint16_t data type.
   * \param value A basic data type value
   * \return The ObjectStart element
   */
  ReturnType operator()(std::uint16_t const value) {
    return std::move(os_) << kJsonKeyType << JBinString(internal::config::key_value_storage::detail::ui16_type) << kJsonKeyData << value;
  }

  /*!
   * \brief () operator for std::uint32_t data type.
   * \param value A basic data type value
   * \return The ObjectStart element
   */
  ReturnType operator()(std::uint32_t const value) {
    return std::move(os_) << kJsonKeyType << JBinString(internal::config::key_value_storage::detail::ui32_type) << kJsonKeyData << value;
  }

  /*!
   * \brief () operator for std::uint64_t data type.
   * \param value A basic data type value
   * \return The ObjectStart element
   */
  ReturnType operator()(std::uint64_t const value) {
    return std::move(os_) << kJsonKeyType << JBinString(internal::config::key_value_storage::detail::ui64_type) << kJsonKeyData << value;
  }

  /*!
   * \brief () operator for std::int8_t data type.
   * \param value A basic data type value
   * \return The ObjectStart element
   */
  ReturnType operator()(std::int8_t const value) {
    return std::move(os_) << kJsonKeyType << JBinString(internal::config::key_value_storage::detail::i8_type) << kJsonKeyData << value;
  }

  /*!
   * \brief () operator for std::int16_t data type.
   * \param value A basic data type value
   * \return The ObjectStart element
   */
  ReturnType operator()(std::int16_t const value) {
    return std::move(os_) << kJsonKeyType << JBinString(internal::config::key_value_storage::detail::i16_type) << kJsonKeyData << value;
  }

  /*!
   * \brief () operator for std::int32_t data type.
   * \param value A basic data type value
   * \return The ObjectStart element
   */
  ReturnType operator()(std::int32_t const value) {
    return std::move(os_) << kJsonKeyType << JBinString(internal::config::key_value_storage::detail::i32_type) << kJsonKeyData << value;
  }

  /*!
   * \brief () operator for std::int64_t data type.
   * \param value A basic data type value
   * \return The ObjectStart element
   */
  ReturnType operator()(std::int64_t const value) {
    return std::move(os_) << kJsonKeyType << JBinString(internal::config::key_value_storage::detail::i64_type) << kJsonKeyData << value;
  }

  /*!
   * \brief () operator for float data type.
   * \param value A basic data type value
   * \return The ObjectStart element
   */
  ReturnType operator()(float const value) {
    WriteBuffer const buffer{this->buffer_};
    SerResult const res{amsr::per::serializer::serialize_trivially_copyable(buffer, value)};
    WriteBuffer const filled_buffer{buffer.subspan(0, 4)};
    // VECTOR NL AutosarC++17_10-A5.2.4: MD_ARAPER_AutosarC++17_10-A5.2.4_char_cast
    ara::core::Span<char const> const char_buffer{reinterpret_cast<char const*>(filled_buffer.data()), filled_buffer.size()};
    return std::move(os_) << kJsonKeyType << JBinString(internal::config::key_value_storage::detail::fl_type) << kJsonKeyData << JBin(char_buffer);
  }

  /*!
   * \brief () operator for double data type.
   * \param value A basic data type value
   * \return The ObjectStart element
   */
  ReturnType operator()(double const value) {
    WriteBuffer const buffer{this->buffer_};
    SerResult const res{amsr::per::serializer::serialize_trivially_copyable(buffer, value)};
    WriteBuffer const filled_buffer{buffer.subspan(0, 8)};
    // VECTOR NL AutosarC++17_10-A5.2.4: MD_ARAPER_AutosarC++17_10-A5.2.4_char_cast
    ara::core::Span<char const> const char_buffer{reinterpret_cast<char const*>(filled_buffer.data()), filled_buffer.size()};
    return std::move(os_) << kJsonKeyType << JBinString(internal::config::key_value_storage::detail::do_type) << kJsonKeyData << JBin(char_buffer);
  }

// VECTOR Disable AutosarC++17_10-A16.0.1: MD_ARAPER_AutosarC++17_10-A16.0.1_conditional_compilation

#ifdef __GNUC__
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wshadow"
#endif


#ifdef __GNUC__
#pragma GCC diagnostic pop
#endif

// VECTOR Enable AutosarC++17_10-A16.0.1

 private:
  /*!
   * \brief The string stream for the serialized data.
   */
  ObjectStart os_;

  /*!
   * \brief The buffer on which the serializers operate on.
   * todo: find out where to put the buffer
   */
  std::vector<vac::language::byte> buffer_;
};

// VECTOR Enable AutosarC++17_10-A5.1.8
}  // namespace detail
}  // namespace per
}  // namespace ara

#endif  // TIMGRSRVEXE_INCLUDE_ARA_PER_KEY_VALUE_STORAGE_DATA_VISITOR_H_
