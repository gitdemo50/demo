#ifndef GWM_SERVICES_TIMER_SRV_PROXY_HPP
#define GWM_SERVICES_TIMER_SRV_PROXY_HPP

#include "ara/com/runtime.h"
#include "ara/com/types.h"
#include "ara/core/future.h"
#include "ara/log/logging.h"
#include "vac/container/string_literals.h"
#include "gwm/application_remsdl/timersrv_si/si_timersrv_proxy.h"

#include "SensorProxy.hpp"

namespace gwm {
namespace services {
namespace timer {

namespace proxy {

using TimerSrvProxy = gwm::application_remsdl::timersrv_si::proxy::SI_TimerSrvProxy;

using vac::container::literals::operator""_sv;

class TimerSrvService : public SensorProxy<TimerSrvProxy> {
public:
    using AvailabilityNotifier = std::function<void()>;

    TimerSrvService();

    virtual ~TimerSrvService();

    void start() {
        SensorProxy::start(serviceSpec);
    }

    void setAvailabilityNotifier(AvailabilityNotifier notifier) {
        if(isServiceAvailable()) {
            notifier();
            return;
        }
        _availabilityNotifier = notifier;
    } 

    MethodWrapper<gwm::application_remsdl::timersrv_si::proxy::methods::TimerReq> timerReq {&proxy->TimerReq};
    MethodWrapper<gwm::application_remsdl::timersrv_si::proxy::methods::TimerCncl> timerCncl {&proxy->TimerCncl};
    EventWrapper<gwm::application_remsdl::timersrv_si::proxy::fields::FieldNotifierTimerSt> ev_timerSt {*this, &proxy->TimerSt};

protected:
    void doSubscription() override;
    void initProxyItems() override;

    ara::core::StringView serviceSpec;

    AvailabilityNotifier _availabilityNotifier;
};

}
}
}
}

#endif