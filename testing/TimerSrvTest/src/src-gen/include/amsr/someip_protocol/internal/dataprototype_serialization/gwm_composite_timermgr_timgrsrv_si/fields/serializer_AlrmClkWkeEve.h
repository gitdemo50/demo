/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  TiMgrSrvExe/include/amsr/someip_protocol/internal/dataprototype_serialization/gwm_composite_timermgr_timgrsrv_si/fields/serializer_AlrmClkWkeEve.h
 *        \brief  SOME/IP protocol serializer implementation for data prototype '/ServiceInterfaces/SI_TiMgrSrv/AlrmClkWkeEve
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_someipprotocol
 *         Commit ID: 0e137b9e6356987a6d2839c8d79c286219b68d60
 *********************************************************************************************************************/

#ifndef TIMGRSRVEXE_INCLUDE_AMSR_SOMEIP_PROTOCOL_INTERNAL_DATAPROTOTYPE_SERIALIZATION_GWM_COMPOSITE_TIMERMGR_TIMGRSRV_SI_FIELDS_serializer_AlrmClkWkeEve_h_
#define TIMGRSRVEXE_INCLUDE_AMSR_SOMEIP_PROTOCOL_INTERNAL_DATAPROTOTYPE_SERIALIZATION_GWM_COMPOSITE_TIMERMGR_TIMGRSRV_SI_FIELDS_serializer_AlrmClkWkeEve_h_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "gwm/composite_timermgr/timermgr_idt/impl_type_alrmclkwkeeve_enum_idt.h"
#include "someip-protocol/internal/serialization/ser_common.h"
#include "someip-protocol/internal/serialization/ser_forward.h"
#include "someip-protocol/internal/serialization/ser_sizing.h"
#include "someip-protocol/internal/serialization/writer.h"

namespace amsr {
namespace someip_protocol {
namespace internal {
namespace dataprototype_serializer {
namespace gwm_composite_timermgr_timgrsrv_si {
namespace fields {

/*!
 * \brief Serializer for service field /ServiceInterfaces/SI_TiMgrSrv/AlrmClkWkeEve
 *        of service interface /ServiceInterfaces/SI_TiMgrSrv.
 * \details Top-Level data type: /DataTypes/ImplementationDataTypes/AlrmClkWkeEve_Enum_Idt
 *          Effective transformation properties of the DataPrototype:
 *          - ByteOrder:                    MOST-SIGNIFICANT-BYTE-FIRST (big-endian)
 *          - sizeOfArrayLengthField:       4
 *          - sizeOfVectorLengthField:      4
 *          - sizeOfMapLengthField:         4
 *          - sizeOfStringLengthField:      4
 *          - sizeOfStructLengthField:      0
 *          - sizeOfUnionLengthField:       4
 *          - sizeOfUnionTypeSelectorField: 4
 *          - isBomActive:                  true
 *          - isNullTerminationActive:      true
 *          - isDynamicLengthFieldSize:     false
 */
class SerializerAlrmClkWkeEve {
 public:
  /*!
   * \brief Returns the required buffer size for the data prototype service field /ServiceInterfaces/SI_TiMgrSrv/AlrmClkWkeEve.
   *
   * \param[in]   data Reference to data object of top-level data type /DataTypes/ImplementationDataTypes/AlrmClkWkeEve_Enum_Idt.
   *
   * \return      Calculated buffer size for serialization.
   * \pre         -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  constexpr static std::size_t GetRequiredBufferSize(::gwm::composite_timermgr::timermgr_idt::AlrmClkWkeEve_Enum_Idt const &data) noexcept {
    return serialization::GetRequiredBufferSize<
      TpPackDataPrototype,
      // Byte-order of primitive datatype (/DataTypes/ImplementationDataTypes/AlrmClkWkeEve_Enum_Idt)
      typename serialization::Tp<TpPackDataPrototype>::ByteOrder

      >(data);
  }

  /*!
   * \brief Serialize the data prototype service field /ServiceInterfaces/SI_TiMgrSrv/AlrmClkWkeEve.
   *
   * \param[in,out] writer Reference to the byte stream writer.
   * \param[in]     data Reference to data object of top-level data type /DataTypes/ImplementationDataTypes/AlrmClkWkeEve_Enum_Idt
   *                , whose value will be serialized into the writer.
   *
   * \pre         -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  static void Serialize(serialization::Writer &writer, ::gwm::composite_timermgr::timermgr_idt::AlrmClkWkeEve_Enum_Idt const &data);

 private:
  /*!
   * \brief Transformation properties of the data prototype.
   */
  using TpPackDataPrototype = serialization::TpPack<
      BigEndian,
      serialization::SizeOfArrayLengthField<4>,
      serialization::SizeOfVectorLengthField<4>,
      serialization::SizeOfMapLengthField<4>,
      serialization::SizeOfStringLengthField<4>,
      serialization::SizeOfStructLengthField<0>,
      serialization::SizeOfUnionLengthField<4>,
      serialization::SizeOfUnionTypeSelectorField<4>,
      serialization::StringBomActive,
      serialization::StringNullTerminationActive,
      serialization::DynamicLengthFieldSizeInactive>;

};

}  // namespace fields
}  // namespace gwm_composite_timermgr_timgrsrv_si
}  // namespace dataprototype_serializer
}  // namespace internal
}  // namespace someip_protocol
}  // namespace amsr

#endif  // TIMGRSRVEXE_INCLUDE_AMSR_SOMEIP_PROTOCOL_INTERNAL_DATAPROTOTYPE_SERIALIZATION_GWM_COMPOSITE_TIMERMGR_TIMGRSRV_SI_FIELDS_serializer_AlrmClkWkeEve_h_

