/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/src/amsr/someip_binding_transformation_layer/internal/gwm/pt_powersys/obcstsrv_si/SI_OBCStSrv_skeleton_someip_event_manager.cpp
 *        \brief  SOME/IP skeleton event handling for events and field notifications of service 'SI_OBCStSrv'
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_someipbinding
 *         Commit ID: 43b33f0283d4d61385b98a3599688499abd86a75
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "amsr/someip_binding_transformation_layer/internal/gwm/pt_powersys/obcstsrv_si/SI_OBCStSrv_skeleton_someip_event_manager.h"

namespace amsr {
namespace someip_binding_transformation_layer {
namespace internal {
namespace gwm {
namespace pt_powersys {
namespace obcstsrv_si {


// ---- Event 'OBCActT' -------------------------------------------

/*!
 * \brief Definition of the service ID of the skeleton event configuration for event 'OBCActT'.
 */
::amsr::someip_protocol::internal::ServiceId constexpr SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCActT::kServiceId;

/*!
 * \brief Definition of the event ID of the skeleton event configuration for event 'OBCActT'.
 */
::amsr::someip_protocol::internal::EventId constexpr SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCActT::kEventId;

// ---- Event 'OBCChrgLim' -------------------------------------------

/*!
 * \brief Definition of the service ID of the skeleton event configuration for event 'OBCChrgLim'.
 */
::amsr::someip_protocol::internal::ServiceId constexpr SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCChrgLim::kServiceId;

/*!
 * \brief Definition of the event ID of the skeleton event configuration for event 'OBCChrgLim'.
 */
::amsr::someip_protocol::internal::EventId constexpr SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCChrgLim::kEventId;

// ---- Event 'OBCDchrOutpI' -------------------------------------------

/*!
 * \brief Definition of the service ID of the skeleton event configuration for event 'OBCDchrOutpI'.
 */
::amsr::someip_protocol::internal::ServiceId constexpr SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCDchrOutpI::kServiceId;

/*!
 * \brief Definition of the event ID of the skeleton event configuration for event 'OBCDchrOutpI'.
 */
::amsr::someip_protocol::internal::EventId constexpr SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCDchrOutpI::kEventId;

// ---- Event 'OBCDchrOutpU' -------------------------------------------

/*!
 * \brief Definition of the service ID of the skeleton event configuration for event 'OBCDchrOutpU'.
 */
::amsr::someip_protocol::internal::ServiceId constexpr SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCDchrOutpU::kServiceId;

/*!
 * \brief Definition of the event ID of the skeleton event configuration for event 'OBCDchrOutpU'.
 */
::amsr::someip_protocol::internal::EventId constexpr SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCDchrOutpU::kEventId;

// ---- Event 'OBCInptAcI' -------------------------------------------

/*!
 * \brief Definition of the service ID of the skeleton event configuration for event 'OBCInptAcI'.
 */
::amsr::someip_protocol::internal::ServiceId constexpr SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCInptAcI::kServiceId;

/*!
 * \brief Definition of the event ID of the skeleton event configuration for event 'OBCInptAcI'.
 */
::amsr::someip_protocol::internal::EventId constexpr SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCInptAcI::kEventId;

// ---- Event 'OBCInptAcU' -------------------------------------------

/*!
 * \brief Definition of the service ID of the skeleton event configuration for event 'OBCInptAcU'.
 */
::amsr::someip_protocol::internal::ServiceId constexpr SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCInptAcU::kServiceId;

/*!
 * \brief Definition of the event ID of the skeleton event configuration for event 'OBCInptAcU'.
 */
::amsr::someip_protocol::internal::EventId constexpr SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCInptAcU::kEventId;

// ---- Event 'OBCOuptDcI' -------------------------------------------

/*!
 * \brief Definition of the service ID of the skeleton event configuration for event 'OBCOuptDcI'.
 */
::amsr::someip_protocol::internal::ServiceId constexpr SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCOuptDcI::kServiceId;

/*!
 * \brief Definition of the event ID of the skeleton event configuration for event 'OBCOuptDcI'.
 */
::amsr::someip_protocol::internal::EventId constexpr SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCOuptDcI::kEventId;

// ---- Event 'OBCOuptDcU' -------------------------------------------

/*!
 * \brief Definition of the service ID of the skeleton event configuration for event 'OBCOuptDcU'.
 */
::amsr::someip_protocol::internal::ServiceId constexpr SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCOuptDcU::kServiceId;

/*!
 * \brief Definition of the event ID of the skeleton event configuration for event 'OBCOuptDcU'.
 */
::amsr::someip_protocol::internal::EventId constexpr SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCOuptDcU::kEventId;

// ---- Field notifier 'OBCChrgGunSt' -------------------------------------------

/*!
 * \brief Definition of the service ID of the skeleton event configuration for event 'OBCChrgGunSt'.
 */
::amsr::someip_protocol::internal::ServiceId constexpr SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCChrgGunSt::kServiceId;

/*!
 * \brief Definition of the event ID of the skeleton event configuration for event 'OBCChrgGunSt'.
 */
::amsr::someip_protocol::internal::EventId constexpr SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCChrgGunSt::kEventId;

// ---- Field notifier 'OBCErrSt' -------------------------------------------

/*!
 * \brief Definition of the service ID of the skeleton event configuration for event 'OBCErrSt'.
 */
::amsr::someip_protocol::internal::ServiceId constexpr SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCErrSt::kServiceId;

/*!
 * \brief Definition of the event ID of the skeleton event configuration for event 'OBCErrSt'.
 */
::amsr::someip_protocol::internal::EventId constexpr SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCErrSt::kEventId;

// ---- Field notifier 'OBCHVILSt' -------------------------------------------

/*!
 * \brief Definition of the service ID of the skeleton event configuration for event 'OBCHVILSt'.
 */
::amsr::someip_protocol::internal::ServiceId constexpr SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCHVILSt::kServiceId;

/*!
 * \brief Definition of the event ID of the skeleton event configuration for event 'OBCHVILSt'.
 */
::amsr::someip_protocol::internal::EventId constexpr SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCHVILSt::kEventId;

// ---- Field notifier 'OBCSt' -------------------------------------------

/*!
 * \brief Definition of the service ID of the skeleton event configuration for event 'OBCSt'.
 */
::amsr::someip_protocol::internal::ServiceId constexpr SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCSt::kServiceId;

/*!
 * \brief Definition of the event ID of the skeleton event configuration for event 'OBCSt'.
 */
::amsr::someip_protocol::internal::EventId constexpr SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCSt::kEventId;

// ---- Field notifier 'OBCV2XSt' -------------------------------------------

/*!
 * \brief Definition of the service ID of the skeleton event configuration for event 'OBCV2XSt'.
 */
::amsr::someip_protocol::internal::ServiceId constexpr SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCV2XSt::kServiceId;

/*!
 * \brief Definition of the event ID of the skeleton event configuration for event 'OBCV2XSt'.
 */
::amsr::someip_protocol::internal::EventId constexpr SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCV2XSt::kEventId;

}  // namespace internal
}  // namespace someip_binding_transformation_layer
}  // namespace amsr
}  // namespace obcstsrv_si
}  // namespace pt_powersys
}  // namespace gwm

