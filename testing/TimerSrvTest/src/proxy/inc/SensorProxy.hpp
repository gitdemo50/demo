#ifndef GWM_SERVICES_WINDOWCONTROL_SENSORPROXY_HPP
#define GWM_SERVICES_WINDOWCONTROL_SENSORPROXY_HPP

/*
*   COPY RIGHT NOTICE
*   
*
*
*/

/*
*
*   This class completely abstracts the ARA specif part of the communication from the app code.
*   It is specifically intended for proxy interfacse.
*   Internally wraps event subscription, event and method handling for the consumers
*
*/

#include <memory>
#include <vector>
#include <algorithm>

#include "logger/logger.hpp"
#include "workerthread.hpp"

//#include "shims.hpp"

namespace gwm{
namespace services {
namespace timer {
namespace proxy {

template <typename ProxyType>
class SensorProxy {
public:

    template<typename T>
    using Future = ara::core::Future<T>;

    template<typename T>
    using Promise = ara::core::Promise<T>;

    // Wraps sepcifics of the ARA COM event handling part for application code
    template<typename FieldType>
    struct EventWrapper {
        using EventHandler =  std::function<void(typename FieldType::SampleType &)>;
        using EventType = typename FieldType::SampleType;
        
        EventWrapper(SensorProxy& sensorProxy, FieldType* fld) : field(fld), sensorProxy(sensorProxy),
            maxSample(5) {

        }

        void set(FieldType* fld) {
            field = fld;
        }

        void subscribe() {
            sensorProxy.logger.LogVerbose() << "Setting receive handler and subscribing";
            field->SetReceiveHandler([this] () { processData(); });
            field->Subscribe(ara::com::EventCacheUpdatePolicy::kNewestN, maxSample);
        }

        void setEventHandler(EventHandler eventHandler) {
            sensorProxy.logger.LogVerbose() << "Event handler set";
            handlerList.push_back(eventHandler);
        }

    private:
        void processData() {
            sensorProxy.logger.LogInfo() << "Data available";
            //Update and get cached samples and call set eventhandler
            if(field->Update() == false) {
                sensorProxy.logger.LogWarn() << "Data available but update failed";
                return;
            }

            // Process the samples
            auto samples = field->GetCachedSamples();
            sensorProxy.logger.LogVerbose() << "No of samples available " << samples.size(); 
            if(samples.size() > 0) {
                auto sample = *(samples[0]);
                if(!handlerList.empty()) {
                    (void) std::for_each(handlerList.begin(), handlerList.end(), [&sample] (const EventHandler eventHandler) {
                        eventHandler(sample);
                    });
                } else {
                    sensorProxy.logger.LogWarn() << "No handler to process data";
                }
            }
        }

        std::vector<EventHandler> handlerList;
        FieldType* field;
        SensorProxy& sensorProxy;
        std::size_t maxSample;
    };

    // Wraps sepcifics of the ARA COM method handling part for application code
    template<typename MethodType>
    struct MethodWrapper {

        using OutputType = typename MethodType::Output;

        MethodWrapper(MethodType* mthd) : method(mthd) {
           
        }

        void set(MethodType* mthd) {
            method = mthd;
        }

        template<typename ... Args>
        auto operator () (Args ... args) {
            if(method == nullptr) {
                //logger.LogError() << "Method target not set, service not found";    
            }

            future = (*method)(std::forward<Args>(args) ...);
            
            //TODO: Handle time out and then part
            // future.then([this] (Future<OutputType> f) {
            //     this->promise->set_value(f.get());
            // });

            promise = std::make_unique<Promise<OutputType>>();
            try {
                promise->set_value(future.get());
            } catch(const std::exception& ex) {
                promise->SetError(ara::core::MakeErrorCode(ara::core::FutureErrorDomain::Errc::broken_promise,0,""));
            }
            return promise->get_future();
        }

    private:
        MethodType* method;
        std::unique_ptr<Promise<OutputType>> promise;
        Future<OutputType> future;
    };
    
    SensorProxy() : proxy(nullptr), logger("Proxy") {
        
    }

    SensorProxy(gwm::platform::Logger&& logger) : proxy(nullptr), logger(std::move(logger)) {
        
    }

    virtual ~SensorProxy() {
#ifdef UT_BUILD
        logger.LogVerbose() << "Destructing proxy";
#endif
    }

    MOCKABLE void start(ara::core::StringView instanceName) {
        logger.LogVerbose() << "Proxy start";

        worker = std::make_unique<gwm::utils::WorkerThread>();

        worker->schedule([this,instanceName] {
            this->startFindService(instanceName);
        });
    }

    MOCKABLE bool isServiceAvailable() {
        return (proxy != nullptr);
    }

protected:
    virtual void doSubscription() = 0;
    //TODO: think about better method dispatch
    virtual void initProxyItems() = 0;
    
    std::unique_ptr<ProxyType> proxy;
    gwm::platform::Logger logger;
private:

    inline void startFindService(const ara::core::StringView instanceSpec) {
        logger.LogVerbose() << "startFindService";
        ara::core::InstanceSpecifier instance{instanceSpec};
        logger.LogVerbose() << " Looking for " << instance.ToString();
        ara::com::FindServiceHandle handle = ProxyType::StartFindService([this] (ara::com::ServiceHandleContainer<typename ProxyType::HandleType> handles) {
                    logger.LogVerbose() << "Service found, delegating";
                    this->handleServiceAvailability(std::move(handles));
                },instance);
        logger.LogVerbose() << " Triggered lookup";
        serviceHandle = std::make_unique<ara::com::FindServiceHandle>(handle);
    }

    inline void handleServiceAvailability(ara::com::ServiceHandleContainer<typename ProxyType::HandleType> handles) {
        logger.LogInfo() << "handleServiceAvailability found " << handles.size() << " services";

        if(handles.size() == 1) {
            proxy = std::make_unique<ProxyType>(handles[0]);
            initProxyItems();
            worker->schedule([this] {
                logger.LogVerbose() << "Stopping service finding and start subscribing";
                if(serviceHandle)
                    ProxyType::StopFindService(*serviceHandle);
                this->doSubscription();
            });
        } else {
            //ToDo: Handle
        }
    }
   
    std::unique_ptr<ara::com::FindServiceHandle> serviceHandle;

    std::unique_ptr<gwm::utils::WorkerThread> worker;
};

}
}
}
}

#endif