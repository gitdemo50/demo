#ifndef GWM_PLATFORM_LOGGER_IMPL_GWM_LOGGER_TARGET_HPP
#define GWM_PLATFORM_LOGGER_IMPL_GWM_LOGGER_TARGET_HPP

#include <sstream>
#include <iostream>

namespace gwm {
namespace platform {
namespace impl {

class GWMLoggerTarget {
public:
    virtual ~GWMLoggerTarget() = default;
    virtual void log(std::stringstream& stream) = 0;
    virtual void log(const std::string& prefix, std::stringstream& stream) = 0;
};

class ConsoleTarget : public GWMLoggerTarget {
public:
    ConsoleTarget() {
        std::cout.sync_with_stdio(false);
        std::cin.tie(nullptr);
    }

    void log(std::stringstream& stream) override {
        std::cerr << stream.rdbuf() << std::endl;
    }

    void log(const std::string& prefix, std::stringstream& stream) override {
        std::cerr << prefix  << stream.rdbuf() << std::endl;
    }
};

}
}
}

#endif
