
/*
*   COPY RIGHT NOTICE
*   
*
*
*/

#include <memory>
#include <iostream>
#include "TimerManager.hpp"
#include <threads.h>

namespace gwm {
namespace manager {

TimerManager::TimerManager() : 
        srv_TimerSrvService(std::make_shared<TimerSrvService>()),
        logger(gwm::platform::Logger("TimerManager")) {
    logger.LogInfo() << "TimerManager ready";
}

void TimerManager::start() {
    logger.LogInfo() << "Starting TimerManager";
    startControllers();
    initControllers();
}

void TimerManager::startControllers() {
    srv_TimerSrvService->start();
}

void TimerManager::initControllers() {

}

void TimerManager::test(int a){

    if (!srv_TimerSrvService->isServiceAvailable()) {

        std::cout << "The dependent service is not found." << std::endl;

        return;
    }

    switch (a)
    {
    case 1:
        uint32_t timerAfter;

        std::cin >> timerAfter; 
        try {
            if(srv_TimerSrvService->timerReq(timerAfter).get().Response !=gwm::platform_timer::timer_idt::TimerSet_Response_Enum_Idt::OK) {
                std::cout << "Error while TimerReq " << std::endl;
            }   
        } catch(const std::exception& ex) {
            std::cout << "No response while timerReq " << std::endl;
        }
        
        break;

    case 2:

        try {
            if(srv_TimerSrvService->timerCncl().get().Response !=gwm::platform_timer::timer_idt::TimerSet_Response_Enum_Idt::OK) {
                std::cout << "Error while TimerCncl " << std::endl;
            }  
        } catch(const std::exception& ex) {
            std::cout << "No response while timerCncl " << std::endl;
        }   
    
        break;
    }
}

}}