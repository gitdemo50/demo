/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  TimerSrvExe/include/amsr/someip_protocol/internal/method_serialization/gwm_application_remsdl_timersrv_si/fields/serializer_Response_TimerStGet.h
 *        \brief  SOME/IP packet serializer of service 'SI_TimerSrv'.
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_someipprotocol
 *         Commit ID: 0e137b9e6356987a6d2839c8d79c286219b68d60
 *********************************************************************************************************************/

#ifndef TIMERSRVEXE_INCLUDE_AMSR_SOMEIP_PROTOCOL_INTERNAL_METHOD_SERIALIZATION_GWM_APPLICATION_REMSDL_TIMERSRV_SI_FIELDS_serializer_Response_TimerStGet_h_
#define TIMERSRVEXE_INCLUDE_AMSR_SOMEIP_PROTOCOL_INTERNAL_METHOD_SERIALIZATION_GWM_APPLICATION_REMSDL_TIMERSRV_SI_FIELDS_serializer_Response_TimerStGet_h_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "amsr/someip_protocol/internal/dataprototype_serialization/gwm_application_remsdl_timersrv_si/fields/serializer_TimerSt.h"
#include "ara/log/logging.h"
#include "osabstraction/io/io_buffer.h"
#include "someip-protocol/internal/serialization/ser_common.h"
#include "someip-protocol/internal/serialization/writer.h"
#include "vac/memory/memory_buffer.h"

namespace amsr {
namespace someip_protocol {
namespace internal {
namespace method_serializer {
namespace gwm_application_remsdl_timersrv_si {
namespace fields {

/*!
 * \brief Serializer for a SOME/IP packet of method '/ServiceInterfaces/SI_TimerSrv/TimerSt'
 *        of service interface '/ServiceInterfaces/SI_TimerSrv'.
 */
class SerializerResponseOkTimerStGet final {
 public:

  /*!
   * \brief Type alias for the concrete memory buffer type.
   */
   using BufferPtrType = ::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer>;

  /*!
   * \brief Returns the required buffer size to serialize the SOME/IP packet payload of method 'TimerStGet'.
   *
   * \tparam      Input Type of data structure storing all in-/output arguments to be serialized.
   *              Type must support member initializer lists. Typically a struct or a std::tuple is used.
   * \param[in]   input Parameter to serialize.
   * \return      Calculated buffer size for serialization.
   * \pre         -
   * \context     Reactor|App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  template <typename Input>
  constexpr static std::size_t GetRequiredBufferSize(Input const& input) noexcept {
    std::size_t required_buffer_size{0};


    // Get required buffer size for the argument 'TimerSt' of type '/DataTypes/ImplementationDataTypes/TimerSt_Struct_Idt'
    required_buffer_size += dataprototype_serializer::gwm_application_remsdl_timersrv_si::fields::SerializerTimerSt::GetRequiredBufferSize(
        input);

    return required_buffer_size;
  }

  /*!
   * \brief Serialize the SOME/IP packet payload of method 'TimerStGet'.
   *
   * \tparam Input Type of data structure storing all in-/output arguments to be serialized.
   *         Type must support member initializer lists. Typically a struct or a std::tuple is used.
   * \param[out] logger Reference to the logger used for logging during serialization.
   * \param[in, out] writer Reference to the byte stream writer.
   * \param[in] input Parameter to serialize.
   */
  template <typename Input>
  static void Serialize(ara::log::Logger& logger, serialization::Writer& writer, Input const& input) noexcept {

    // Serialize the argument 'TimerSt' of type '/DataTypes/ImplementationDataTypes/TimerSt_Struct_Idt'
    dataprototype_serializer::gwm_application_remsdl_timersrv_si::fields::SerializerTimerSt::Serialize(
        writer, input);

    logger.LogDebug([](ara::log::LogStream& s) { s << "Serializer done."; }, __func__, __LINE__);
  }
};

}  // namespace fields
}  // namespace gwm_application_remsdl_timersrv_si
}  // namespace method_serializer
}  // namespace internal
}  // namespace someip_protocol
}  // namespace amsr

#endif  // TIMERSRVEXE_INCLUDE_AMSR_SOMEIP_PROTOCOL_INTERNAL_METHOD_SERIALIZATION_GWM_APPLICATION_REMSDL_TIMERSRV_SI_FIELDS_serializer_Response_TimerStGet_h_

