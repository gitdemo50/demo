/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  TimerSrvExe/include/gwm/application_remsdl/timersrv_si/SI_TimerSrv_skeleton_impl_interface.h
 *        \brief  Skeleton implementation interface of service 'SI_TimerSrv'.
 *
 *      \details  This Service interface provides timer
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_socal
 *         Commit ID: 209b3c0234013e84e3170ee826c9a8c6022de194
 *********************************************************************************************************************/

#ifndef TIMERSRVEXE_INCLUDE_GWM_APPLICATION_REMSDL_TIMERSRV_SI_SI_TIMERSRV_SKELETON_IMPL_INTERFACE_H_
#define TIMERSRVEXE_INCLUDE_GWM_APPLICATION_REMSDL_TIMERSRV_SI_SI_TIMERSRV_SKELETON_IMPL_INTERFACE_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "amsr/socal/internal/events/skeleton_event_manager_interface.h"
#include "gwm/application_remsdl/timersrv_si/si_timersrv_common.h"

namespace gwm {
namespace application_remsdl {
namespace timersrv_si {
namespace internal {

/*!
 * \brief Skeleton implementation interface of service 'SI_TimerSrv'
 */
class SI_TimerSrvSkeletonImplInterface {
 public:
 /*!
   * \brief Define default constructor.
   * \pre -
   * \context App
   */
  SI_TimerSrvSkeletonImplInterface() noexcept = default;

  /*!
   * \brief Use default destructor
   */
  virtual ~SI_TimerSrvSkeletonImplInterface() noexcept = default;

 protected:

  /*!
   * \brief Use default move constructor
   * \pre -
   * \context App
   */
  SI_TimerSrvSkeletonImplInterface(SI_TimerSrvSkeletonImplInterface &&) noexcept = default;

  /*!
   * \brief Use default move assignment
   * \pre -
   * \context App
   */
  SI_TimerSrvSkeletonImplInterface &operator=(SI_TimerSrvSkeletonImplInterface &&) & noexcept = default;

  SI_TimerSrvSkeletonImplInterface(SI_TimerSrvSkeletonImplInterface const &) = delete;

  SI_TimerSrvSkeletonImplInterface &operator=(SI_TimerSrvSkeletonImplInterface const &) & = delete;

 public:

  // ---- Events ---------------------------------------------------------------------------------------------------

  // ---- Fields ---------------------------------------------------------------------------------------------------

  /*!
   * \brief Get the event manager object for the field notifier of field 'TimerSt'.
   * \details Field data type: ::gwm::platform_timer::timer_idt::TimerSt_Struct_Idt.
   * \return A binding-specific event management object/interface supporting field notifier updates.
   * \pre -
   * \context App
   */
   virtual ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::platform_timer::timer_idt::TimerSt_Struct_Idt>* GetFieldNotifierTimerSt() noexcept = 0;
};

} // namespace internal
}  // namespace timersrv_si
}  // namespace application_remsdl
}  // namespace gwm

#endif  // TIMERSRVEXE_INCLUDE_GWM_APPLICATION_REMSDL_TIMERSRV_SI_SI_TIMERSRV_SKELETON_IMPL_INTERFACE_H_

