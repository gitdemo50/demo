/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/src/gwm/pt_batterypacksys/bmsstsrv_si/si_bmsstsrv.cpp
 *        \brief  Header for service 'SI_BmsStSrv'.
 *
 *      \details  The service interface provides HV battery real time current information
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_socal
 *         Commit ID: 209b3c0234013e84e3170ee826c9a8c6022de194
 *********************************************************************************************************************/

#ifndef DENPENDENCYSRVEXE_SRC_GWM_PT_BATTERYPACKSYS_BMSSTSRV_SI_SI_BMSSTSRV_CPP_
#define DENPENDENCYSRVEXE_SRC_GWM_PT_BATTERYPACKSYS_BMSSTSRV_SI_SI_BMSSTSRV_CPP_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "gwm/pt_batterypacksys/bmsstsrv_si/SI_BmsStSrv.h"

namespace gwm {
namespace pt_batterypacksys {
namespace bmsstsrv_si {

/*!
 * \brief Service Identifier instance.
 */
constexpr ara::com::ServiceIdentifierType SI_BmsStSrv::ServiceIdentifier;

/*!
 * \brief Service shortname path instance.
 */
constexpr vac::container::CStringView SI_BmsStSrv::kServiceShortNamePath;

}  // namespace bmsstsrv_si
}  // namespace pt_batterypacksys
}  // namespace gwm

#endif  // DENPENDENCYSRVEXE_SRC_GWM_PT_BATTERYPACKSYS_BMSSTSRV_SI_SI_BMSSTSRV_CPP_
