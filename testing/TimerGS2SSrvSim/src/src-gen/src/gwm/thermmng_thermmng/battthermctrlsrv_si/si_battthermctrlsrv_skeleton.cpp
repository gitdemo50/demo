/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/src/gwm/thermmng_thermmng/battthermctrlsrv_si/si_battthermctrlsrv_skeleton.cpp
 *        \brief  Skeleton for service 'SI_BattThermCtrlSrv'.
 *
 *      \details  The service interface provides vehicle thermal control strategy level control
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_socal
 *         Commit ID: 209b3c0234013e84e3170ee826c9a8c6022de194
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "gwm/thermmng_thermmng/battthermctrlsrv_si/si_battthermctrlsrv_skeleton.h"
#include "amsr/socal/internal/instance_specifier_lookup_table.h"

/*!
 * \trace SPEC-4980240
 * \trace SPEC-4980241
 */
namespace gwm {
namespace thermmng_thermmng {
namespace battthermctrlsrv_si {
namespace skeleton {

/*!
 * \brief Static instance of service discovery.
 */
// VECTOR NC AutosarC++17_10-A3.3.2: MD_SOCAL_AutosarC++17_10-A3.3.2_StaticStorageDurationOfNonPODType
::amsr::generic::Singleton<SI_BattThermCtrlSrvSkeleton::ServiceDiscovery*> SI_BattThermCtrlSrvSkeleton::sd_;

SI_BattThermCtrlSrvSkeleton::ConstructionResult SI_BattThermCtrlSrvSkeleton::Preconstruct(
    ara::com::InstanceIdentifier instance_id, ara::com::MethodCallProcessingMode mode) noexcept {
return Base::Preconstruct(instance_id, mode);
}

SI_BattThermCtrlSrvSkeleton::ConstructionResult SI_BattThermCtrlSrvSkeleton::Preconstruct(
    ara::core::InstanceSpecifier instance, ara::com::MethodCallProcessingMode mode) noexcept {
return Base::Preconstruct(instance, mode);
}

SI_BattThermCtrlSrvSkeleton::ConstructionResult SI_BattThermCtrlSrvSkeleton::Preconstruct(
    ara::com::InstanceIdentifierContainer instance_identifiers, ara::com::MethodCallProcessingMode mode) noexcept {
return Base::Preconstruct(instance_identifiers, mode);
}

SI_BattThermCtrlSrvSkeleton::SI_BattThermCtrlSrvSkeleton(ConstructionToken&& token) noexcept
: Base{std::move(token)}
 {}

SI_BattThermCtrlSrvSkeleton::SI_BattThermCtrlSrvSkeleton(ara::com::InstanceIdentifier instance,
                                               ara::com::MethodCallProcessingMode mode) noexcept
     : SI_BattThermCtrlSrvSkeleton{Preconstruct(instance, mode).Value()} {}

SI_BattThermCtrlSrvSkeleton::SI_BattThermCtrlSrvSkeleton(ara::core::InstanceSpecifier instance,
                                               ara::com::MethodCallProcessingMode mode) noexcept
     : SI_BattThermCtrlSrvSkeleton{Preconstruct(instance, mode).Value()} {}

SI_BattThermCtrlSrvSkeleton::SI_BattThermCtrlSrvSkeleton(ara::com::InstanceIdentifierContainer instance_identifiers,
                                               ara::com::MethodCallProcessingMode mode) noexcept
     : SI_BattThermCtrlSrvSkeleton{Preconstruct(instance_identifiers, mode).Value()} {}

SI_BattThermCtrlSrvSkeleton::~SI_BattThermCtrlSrvSkeleton() noexcept {
  // Next line might block until all running method requests are done.
  StopOfferService();
}

void SI_BattThermCtrlSrvSkeleton::DoFieldInitializationChecks() noexcept {
}

void SI_BattThermCtrlSrvSkeleton::SendInitialFieldNotifications() noexcept {
  // Send initial field events for all fields with "hasNotifier = true"
}

void SI_BattThermCtrlSrvSkeleton::OfferServiceInternal(::ara::com::InstanceIdentifier const& instance_id) noexcept {
  (*(sd_.GetAccess()))->OfferService(instance_id, this);
}

void SI_BattThermCtrlSrvSkeleton::StopOfferServiceInternal(::ara::com::InstanceIdentifier const& instance_id) noexcept {
  (*(sd_.GetAccess()))->StopOfferService(instance_id);
}

::amsr::generic::Singleton<SI_BattThermCtrlSrvSkeleton::ServiceDiscovery*>& SI_BattThermCtrlSrvSkeleton::GetServiceDiscovery() noexcept {
  return sd_;
}

void SI_BattThermCtrlSrvSkeleton::RegisterServiceDiscovery(SI_BattThermCtrlSrvSkeleton::ServiceDiscovery* service_discovery) noexcept {
  sd_.Create(service_discovery);
}

void SI_BattThermCtrlSrvSkeleton::DeRegisterServiceDiscovery() noexcept { sd_.Destroy(); }

}  // namespace skeleton
}  // namespace battthermctrlsrv_si
}  // namespace thermmng_thermmng
}  // namespace gwm

