/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  TimerSrvExe/include/amsr/someip_protocol/internal/datatype_serialization/gwm/platform_timer/timer_idt/serializer_TimerSt_Struct_Idt.h
 *        \brief  SOME/IP protocol serializer implementation for datatype 'TimerSt_Struct_Idt'
 *
 *      \details  /DataTypes/ImplementationDataTypes/TimerSt_Struct_Idt
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_someipprotocol
 *         Commit ID: 0e137b9e6356987a6d2839c8d79c286219b68d60
 *********************************************************************************************************************/

#ifndef TIMERSRVEXE_INCLUDE_AMSR_SOMEIP_PROTOCOL_INTERNAL_DATATYPE_SERIALIZATION_GWM_PLATFORM_TIMER_TIMER_IDT_SERIALIZER_TIMERST_STRUCT_IDT_H_
#define TIMERSRVEXE_INCLUDE_AMSR_SOMEIP_PROTOCOL_INTERNAL_DATATYPE_SERIALIZATION_GWM_PLATFORM_TIMER_TIMER_IDT_SERIALIZER_TIMERST_STRUCT_IDT_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "gwm/platform_timer/timer_idt/impl_type_timerst_struct_idt.h"
#include "someip-protocol/internal/serialization/ser_common.h"
#include "someip-protocol/internal/serialization/ser_forward.h"
#include "someip-protocol/internal/serialization/ser_sizing.h"
#include "someip-protocol/internal/serialization/writer.h"

namespace gwm {
namespace platform_timer {
namespace timer_idt {

/*!
 * \brief Serializer for datatype /DataTypes/ImplementationDataTypes/TimerSt_Struct_Idt.
 * \details Data type class: DataTypeStruct (CppImplDataTypeStruct).
 *          is TLV struct: false
 *
 * \tparam TpPack Transformation properties to be used for serialization.
 *
 * \param[in, out] writer Reference to byte stream writer.
 * \param[in] data Reference to the data object of type /DataTypes/ImplementationDataTypes/TimerSt_Struct_Idt to be serialized.
 * \pre The writer holds a memory buffer which is enough to store the serialized data.
 * \context Reactor|App
 * \threadsafe FALSE
 * \reentrant TRUE
 * \vprivate Vector component internal API.
 * \synchronous TRUE
 */
template <typename TpPack>
void SomeIpProtocolSerialize(amsr::someip_protocol::internal::serialization::Writer& writer, ::gwm::platform_timer::timer_idt::TimerSt_Struct_Idt const &data) noexcept {
  // Namespace alias for static serialization code
  namespace serialization = amsr::someip_protocol::internal::serialization;
  // Serialize non-TLV struct
    // Serialize struct member 'WakeSrcexpired_Idt' of type /DataTypes/ImplementationDataTypes/TimerSt_WakeSrcexpired_Enum_Idt
    serialization::SomeIpProtocolSerialize<
                        TpPack,
                        // Byte-order of primitive datatype (/DataTypes/ImplementationDataTypes/TimerSt_WakeSrcexpired_Enum_Idt)
                        typename serialization::Tp<TpPack>::ByteOrder

                        >(writer, data.WakeSrcexpired_Idt);
    // Serialize struct member 'WakeSource_Idt' of type /DataTypes/ImplementationDataTypes/TimerSt_WakeSource_Enum_Idt
    serialization::SomeIpProtocolSerialize<
                        TpPack,
                        // Byte-order of primitive datatype (/DataTypes/ImplementationDataTypes/TimerSt_WakeSource_Enum_Idt)
                        typename serialization::Tp<TpPack>::ByteOrder

                        >(writer, data.WakeSource_Idt);
}

/*!
 * \brief Calculates the required buffer size for datatype /DataTypes/ImplementationDataTypes/TimerSt_Struct_Idt.
 * \details Data type class: DataTypeStruct (CppImplDataTypeStruct).
 *          is TLV struct: false
 *
 * \tparam TpPack Transformation properties to be used for serialization.
 * \param[in] data Reference to the data object of type /DataTypes/ImplementationDataTypes/TimerSt_Struct_Idt.
 * \return Returns the required buffer size for the struct in bytes.
 * \pre -
 * \context Reactor|App
 * \threadsafe FALSE
 * \reentrant TRUE
 * \vprivate Vector component internal API.
 * \synchronous TRUE
 */
template <typename TpPack>
constexpr std::size_t GetRequiredBufferSize(::gwm::platform_timer::timer_idt::TimerSt_Struct_Idt const &data) noexcept {
  // Namespace alias for static serialization code
  namespace serialization = amsr::someip_protocol::internal::serialization;

  // Sum of required size
  std::size_t required_buffer_size{0};

  // Accumulate the static size of struct member 'WakeSrcexpired_Idt' of type /DataTypes/ImplementationDataTypes/TimerSt_WakeSrcexpired_Enum_Idt
  required_buffer_size += serialization::GetRequiredBufferSize<
                        TpPack,
                        // Byte-order of primitive datatype (/DataTypes/ImplementationDataTypes/TimerSt_WakeSrcexpired_Enum_Idt)
                        typename serialization::Tp<TpPack>::ByteOrder

                        >(data.WakeSrcexpired_Idt);
  // Accumulate the static size of struct member 'WakeSource_Idt' of type /DataTypes/ImplementationDataTypes/TimerSt_WakeSource_Enum_Idt
  required_buffer_size += serialization::GetRequiredBufferSize<
                        TpPack,
                        // Byte-order of primitive datatype (/DataTypes/ImplementationDataTypes/TimerSt_WakeSource_Enum_Idt)
                        typename serialization::Tp<TpPack>::ByteOrder

                        >(data.WakeSource_Idt);

  return required_buffer_size;
}

/*!
 * \brief Checks if datatype /DataTypes/ImplementationDataTypes/TimerSt_Struct_Idt is of static size.
 * \details Data type class: DataTypeStruct (CppImplDataTypeStruct).
 *          is TLV struct: false
 *
 * \tparam TpPack Transformation properties to be used for serialization.
 * \return True if datatype has static size, false otherwise.
 * \pre -
 * \context Reactor|App
 * \threadsafe FALSE
 * \reentrant TRUE
 * \vprivate Vector component internal API.
 * \synchronous TRUE
 */
template <typename TpPack>
constexpr bool IsStaticSize(amsr::someip_protocol::internal::serialization::SizeToken<::gwm::platform_timer::timer_idt::TimerSt_Struct_Idt>) noexcept {
  // Namespace alias for static serialization code
  namespace serialization = amsr::someip_protocol::internal::serialization;

  constexpr bool is_static_size{
  // Check static size status of struct member 'WakeSrcexpired_Idt' of type /DataTypes/ImplementationDataTypes/TimerSt_WakeSrcexpired_Enum_Idt
  serialization::IsStaticSize<
                      TpPack,
                      // Byte-order of primitive datatype (/DataTypes/ImplementationDataTypes/TimerSt_WakeSrcexpired_Enum_Idt)
                      typename serialization::Tp<TpPack>::ByteOrder

                      >(serialization::SizeToken<::gwm::platform_timer::timer_idt::TimerSt_WakeSrcexpired_Enum_Idt>{})  && 
  
  // Check static size status of struct member 'WakeSource_Idt' of type /DataTypes/ImplementationDataTypes/TimerSt_WakeSource_Enum_Idt
  serialization::IsStaticSize<
                      TpPack,
                      // Byte-order of primitive datatype (/DataTypes/ImplementationDataTypes/TimerSt_WakeSource_Enum_Idt)
                      typename serialization::Tp<TpPack>::ByteOrder

                      >(serialization::SizeToken<::gwm::platform_timer::timer_idt::TimerSt_WakeSource_Enum_Idt>{}) 
  };
    return is_static_size;
}

}  // namespace timer_idt
}  // namespace platform_timer
}  // namespace gwm

#endif  // TIMERSRVEXE_INCLUDE_AMSR_SOMEIP_PROTOCOL_INTERNAL_DATATYPE_SERIALIZATION_GWM_PLATFORM_TIMER_TIMER_IDT_SERIALIZER_TIMERST_STRUCT_IDT_H_

