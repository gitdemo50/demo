/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  TimerSrvExe/src/gwm/application_remsdl/timersrv_si/si_timersrv_skeleton.cpp
 *        \brief  Skeleton for service 'SI_TimerSrv'.
 *
 *      \details  This Service interface provides timer
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_socal
 *         Commit ID: 209b3c0234013e84e3170ee826c9a8c6022de194
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "gwm/application_remsdl/timersrv_si/si_timersrv_skeleton.h"
#include "amsr/socal/internal/instance_specifier_lookup_table.h"

/*!
 * \trace SPEC-4980240
 * \trace SPEC-4980241
 */
namespace gwm {
namespace application_remsdl {
namespace timersrv_si {
namespace skeleton {

/*!
 * \brief Static instance of service discovery.
 */
// VECTOR NC AutosarC++17_10-A3.3.2: MD_SOCAL_AutosarC++17_10-A3.3.2_StaticStorageDurationOfNonPODType
::amsr::generic::Singleton<SI_TimerSrvSkeleton::ServiceDiscovery*> SI_TimerSrvSkeleton::sd_;

SI_TimerSrvSkeleton::ConstructionResult SI_TimerSrvSkeleton::Preconstruct(
    ara::com::InstanceIdentifier instance_id, ara::com::MethodCallProcessingMode mode) noexcept {
return Base::Preconstruct(instance_id, mode);
}

SI_TimerSrvSkeleton::ConstructionResult SI_TimerSrvSkeleton::Preconstruct(
    ara::core::InstanceSpecifier instance, ara::com::MethodCallProcessingMode mode) noexcept {
return Base::Preconstruct(instance, mode);
}

SI_TimerSrvSkeleton::ConstructionResult SI_TimerSrvSkeleton::Preconstruct(
    ara::com::InstanceIdentifierContainer instance_identifiers, ara::com::MethodCallProcessingMode mode) noexcept {
return Base::Preconstruct(instance_identifiers, mode);
}

SI_TimerSrvSkeleton::SI_TimerSrvSkeleton(ConstructionToken&& token) noexcept
: Base{std::move(token)}
, TimerSt(this)
 {}

SI_TimerSrvSkeleton::SI_TimerSrvSkeleton(ara::com::InstanceIdentifier instance,
                                               ara::com::MethodCallProcessingMode mode) noexcept
     : SI_TimerSrvSkeleton{Preconstruct(instance, mode).Value()} {}

SI_TimerSrvSkeleton::SI_TimerSrvSkeleton(ara::core::InstanceSpecifier instance,
                                               ara::com::MethodCallProcessingMode mode) noexcept
     : SI_TimerSrvSkeleton{Preconstruct(instance, mode).Value()} {}

SI_TimerSrvSkeleton::SI_TimerSrvSkeleton(ara::com::InstanceIdentifierContainer instance_identifiers,
                                               ara::com::MethodCallProcessingMode mode) noexcept
     : SI_TimerSrvSkeleton{Preconstruct(instance_identifiers, mode).Value()} {}

SI_TimerSrvSkeleton::~SI_TimerSrvSkeleton() noexcept {
  // Next line might block until all running method requests are done.
  StopOfferService();
}

void SI_TimerSrvSkeleton::DoFieldInitializationChecks() noexcept {

    if (!TimerSt.IsUpdated()) {
        logger_.LogError([this](ara::log::LogStream& s) {
                          s << "Failed to offer service 'SI_TimerSrv' with instance id(s) '";
                          for (::amsr::socal::internal::InstanceSpecifierLookupTableEntry const& entry : offered_instances_) {
                              ara::core::StringView const instance_id_str{entry.GetInstanceIdentifier().ToString()};
                              s << " " << instance_id_str;
                          }
                          s << "' - No initial value has been set for field 'TimerSt' (TimerSt::Update(...) has never been called)."; },
                          __func__, __LINE__);
        ara::core::Abort("No initial value has been set for field 'TimerSt' (TimerSt::Update(...) has never been called).");
    }
}

void SI_TimerSrvSkeleton::SendInitialFieldNotifications() noexcept {
  // Send initial field events for all fields with "hasNotifier = true"
  TimerSt.SendInitialValue();
}

void SI_TimerSrvSkeleton::OfferServiceInternal(::ara::com::InstanceIdentifier const& instance_id) noexcept {
  (*(sd_.GetAccess()))->OfferService(instance_id, this);
}

void SI_TimerSrvSkeleton::StopOfferServiceInternal(::ara::com::InstanceIdentifier const& instance_id) noexcept {
  (*(sd_.GetAccess()))->StopOfferService(instance_id);
}

::amsr::generic::Singleton<SI_TimerSrvSkeleton::ServiceDiscovery*>& SI_TimerSrvSkeleton::GetServiceDiscovery() noexcept {
  return sd_;
}

void SI_TimerSrvSkeleton::RegisterServiceDiscovery(SI_TimerSrvSkeleton::ServiceDiscovery* service_discovery) noexcept {
  sd_.Create(service_discovery);
}

void SI_TimerSrvSkeleton::DeRegisterServiceDiscovery() noexcept { sd_.Destroy(); }

}  // namespace skeleton
}  // namespace timersrv_si
}  // namespace application_remsdl
}  // namespace gwm

