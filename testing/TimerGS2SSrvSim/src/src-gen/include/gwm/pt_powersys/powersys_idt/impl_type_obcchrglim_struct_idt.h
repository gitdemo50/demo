/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/include/gwm/pt_powersys/powersys_idt/impl_type_obcchrglim_struct_idt.h
 *        \brief
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_modelleddatatypes_api
 *         Commit ID: d9332226824a4a6c4616e2d948d41c862d192343
 *********************************************************************************************************************/

#ifndef DENPENDENCYSRVEXE_INCLUDE_GWM_PT_POWERSYS_POWERSYS_IDT_IMPL_TYPE_OBCCHRGLIM_STRUCT_IDT_H_
#define DENPENDENCYSRVEXE_INCLUDE_GWM_PT_POWERSYS_POWERSYS_IDT_IMPL_TYPE_OBCCHRGLIM_STRUCT_IDT_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include <array>
#include <cstdint>
#include <map>
#include <string>
#include <vector>
#include "gwm/pt_powersys/powersys_idt/impl_type_obcchrglim_chrgcurrmax_float_idt.h"
#include "gwm/pt_powersys/powersys_idt/impl_type_obcchrglim_chrgvoltmax_float_idt.h"

namespace gwm {
namespace pt_powersys {
namespace powersys_idt {

// VECTOR Disable AutosarC++17_10-A12.6.1: MD_MDTG_A12.6.1_GeneratedStructUninitializedMembers
// VECTOR Disable AutosarC++17_10-M8.5.1: MD_MDTG_M8.5.1_GeneratedStructUninitializedMembers
/*!
 * \brief Type OBCChrgLim_Struct_Idt.
 * \remark generated
 * \trace SPEC-5951372
 *
 * IMPLEMENTATION-DATA-TYPE /DataTypes/ImplementationDataTypes/OBCChrgLim_Struct_Idt
 */
struct OBCChrgLim_Struct_Idt {
  using ChrgCurrMax_Idt_generated_type = gwm::pt_powersys::powersys_idt::OBCChrgLim_ChrgCurrMax_float_Idt;
  using ChrgVoltMax_Idt_generated_type = gwm::pt_powersys::powersys_idt::OBCChrgLim_ChrgVoltMax_float_Idt;

  ChrgCurrMax_Idt_generated_type ChrgCurrMax_Idt;
  ChrgVoltMax_Idt_generated_type ChrgVoltMax_Idt;
};
// VECTOR Enable AutosarC++17_10-A12.6.1
// VECTOR Enable AutosarC++17_10-M8.5.1

/*!
 * \brief Compare for equality with another OBCChrgLim_Struct_Idt instance.
 */
inline bool operator==(OBCChrgLim_Struct_Idt const& l,
                       OBCChrgLim_Struct_Idt const& r) noexcept {
  return (&l == &r) || ((l.ChrgCurrMax_Idt == r.ChrgCurrMax_Idt)
                         && (l.ChrgVoltMax_Idt == r.ChrgVoltMax_Idt)
  );
}

/*!
 * \brief Compare for inequality with another OBCChrgLim_Struct_Idt instance.
 */
inline bool operator!=(OBCChrgLim_Struct_Idt const& l,
                       OBCChrgLim_Struct_Idt const& r) noexcept {
  return !(l == r);
}

}  // namespace powersys_idt
}  // namespace pt_powersys
}  // namespace gwm

#endif  // DENPENDENCYSRVEXE_INCLUDE_GWM_PT_POWERSYS_POWERSYS_IDT_IMPL_TYPE_OBCCHRGLIM_STRUCT_IDT_H_
