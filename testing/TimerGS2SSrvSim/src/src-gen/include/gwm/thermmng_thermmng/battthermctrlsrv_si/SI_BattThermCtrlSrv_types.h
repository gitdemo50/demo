
/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/include/gwm/thermmng_thermmng/battthermctrlsrv_si/SI_BattThermCtrlSrv_types.h
 *        \brief  Input and output structures for methods, fields and application errors of service 'SI_BattThermCtrlSrv'
 *
 *      \details  Definition of common input-/output structs used for simplified argument / marshalling handling. For all elements like methods, events fields structs with the related in-/output arguments are generated.
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_socal
 *         Commit ID: 209b3c0234013e84e3170ee826c9a8c6022de194
 *********************************************************************************************************************/

#ifndef DENPENDENCYSRVEXE_INCLUDE_GWM_THERMMNG_THERMMNG_BATTTHERMCTRLSRV_SI_SI_BATTTHERMCTRLSRV_TYPES_H_
#define DENPENDENCYSRVEXE_INCLUDE_GWM_THERMMNG_THERMMNG_BATTTHERMCTRLSRV_SI_SI_BATTTHERMCTRLSRV_TYPES_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "amsr/socal/internal/handle_type.h"
#include "gwm/thermmng_thermmng/battthermctrlsrv_si/si_battthermctrlsrv_common.h"

namespace gwm {
namespace thermmng_thermmng {
namespace battthermctrlsrv_si {

namespace internal {

/*!
 * \brief Proxy HandleType for the Service 'SI_BattThermCtrlSrv'.
 * \trace SPEC-4980259
 */
class SI_BattThermCtrlSrvHandleType final : public ::amsr::socal::internal::HandleType {
  public:
  /*!
   * \brief Inherit constructor.
   */
  using HandleType::HandleType;
};

namespace methods {

/*!
 * \brief A class for service method 'BattPackHeatCtrl' used for type definitions.
 */
class BattPackHeatCtrl final {
 public:
  /*!
   * \brief Struct representing all output arguments of the service method.
   */
  struct Output {
    /*!
     * \brief Reference of output argument 'Response' (/DataTypes/ImplementationDataTypes/BattPackHeatCtrl_Response_Enum_Idt)
     */
    ::gwm::thermmng_thermmng::thermmng_idt::BattPackHeatCtrl_Response_Enum_Idt Response;
  };

  /*!
   * \brief Struct representing all input arguments of the service method.
   */
  struct Input {
    /*!
     * \brief Reference of input argument 'ClntId' (/DataTypes/ImplementationDataTypes/BattPackHeatCtrl_ClntId_Enum_Idt)
     */
    ::gwm::thermmng_thermmng::thermmng_idt::BattPackHeatCtrl_ClntId_Enum_Idt ClntId;
    /*!
     * \brief Reference of input argument 'Command' (/DataTypes/ImplementationDataTypes/BattPackHeatCtrl_Command_Enum_Idt)
     */
    ::gwm::thermmng_thermmng::thermmng_idt::BattPackHeatCtrl_Command_Enum_Idt Command;
  };
};

/*!
 * \brief A class for service method 'BattThermCtrl' used for type definitions.
 */
class BattThermCtrl final {
 public:
  /*!
   * \brief Struct representing all output arguments of the service method.
   */
  struct Output {
    /*!
     * \brief Reference of output argument 'Response' (/DataTypes/ImplementationDataTypes/BattThermCtrl_Response_Enum_Idt)
     */
    ::gwm::thermmng_thermmng::thermmng_idt::BattThermCtrl_Response_Enum_Idt Response;
  };

  /*!
   * \brief Struct representing all input arguments of the service method.
   */
  struct Input {
    /*!
     * \brief Reference of input argument 'ClntId' (/DataTypes/ImplementationDataTypes/BattThermCtrl_ClntId_Enum_Idt)
     */
    ::gwm::thermmng_thermmng::thermmng_idt::BattThermCtrl_ClntId_Enum_Idt ClntId;
    /*!
     * \brief Reference of input argument 'Command' (/DataTypes/ImplementationDataTypes/BattThermCtrl_Command_Integer_Idt)
     */
    ::gwm::thermmng_thermmng::thermmng_idt::BattThermCtrl_Command_Integer_Idt Command;
  };
};

}  // namespace methods

namespace fields {

}  // namespace fields
}  // namespace internal

}  //  namespace battthermctrlsrv_si
}  //  namespace thermmng_thermmng
}  //  namespace gwm

#endif  // DENPENDENCYSRVEXE_INCLUDE_GWM_THERMMNG_THERMMNG_BATTTHERMCTRLSRV_SI_SI_BATTTHERMCTRLSRV_TYPES_H_

