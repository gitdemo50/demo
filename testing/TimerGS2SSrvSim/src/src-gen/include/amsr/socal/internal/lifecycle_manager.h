/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/include/amsr/socal/internal/lifecycle_manager.h
 *        \brief  Manages the lifecycle of internal objects with static storage duration.
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_socal
 *         Commit ID: 209b3c0234013e84e3170ee826c9a8c6022de194
 *********************************************************************************************************************/

#ifndef DENPENDENCYSRVEXE_INCLUDE_AMSR_SOCAL_INTERNAL_LIFECYCLE_MANAGER_H_
#define DENPENDENCYSRVEXE_INCLUDE_AMSR_SOCAL_INTERNAL_LIFECYCLE_MANAGER_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "amsr/generic/singleton_wrapper.h"
#include "amsr/socal/internal/service_discovery/service_discovery.h"
#include "gwm/platform/timer/gs2s/si_timersrv_gs2s_skeleton.h"
#include "gwm/pt_batterypacksys/bmsstsrv_si/si_bmsstsrv_skeleton.h"
#include "gwm/pt_powersys/obcstsrv_si/si_obcstsrv_skeleton.h"
#include "gwm/pt_powersys/vehchrgnctrlsrv_si/si_vehchrgnctrlsrv_skeleton.h"
#include "gwm/thermmng_thermmng/battthermctrlsrv_si/si_battthermctrlsrv_skeleton.h"

namespace amsr {
namespace socal {
namespace internal {

/*!
 * \brief Initiates the creation and destruction of objects with static storage duration and
 *        (de-)registers the static service discovery instances to the Proxy and Skeleton classes.
 */
class LifecycleManager final {
 public:
  /*!
   * \brief       Initializes the lifecycle manager and triggers the creation of singletons.
   * \pre         This function must only be called once.
   * \context     Init
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   *
   * \internal
   * - Create singleton instance of the lifecycle manager.
   * - Register all the service discovery objects to their respective proxy and skeleton classes.
   * - Create singleton instances of all the proxy classes.
   * - Create singleton instances of all the skeleton classes.
   * \endinternal
   */
  static void Initialize() noexcept {
    // Consider the dependencies when changing the initialization order below.
    GetInstance().Create();
    GetInstance().GetAccess()->InitializeServiceDiscovery();


    gwm::platform::timer::gs2s::skeleton::SI_TimerSrv_GS2SSkeleton::CreateSingletons();
    gwm::thermmng_thermmng::battthermctrlsrv_si::skeleton::SI_BattThermCtrlSrvSkeleton::CreateSingletons();
    gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton::CreateSingletons();
    gwm::pt_powersys::obcstsrv_si::skeleton::SI_OBCStSrvSkeleton::CreateSingletons();
    gwm::pt_powersys::vehchrgnctrlsrv_si::skeleton::SI_VehChrgnCtrlSrvSkeleton::CreateSingletons();
  }

  /*!
   * \brief       Deinitializes the lifecycle manager and triggers the destruction of singletons.
   * \pre         Initialize has been called.
   * \context     Shutdown
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   *
   * \internal
   * - Destroy singleton instances of all the skeleton classes.
   * - Destroy singleton instances of all the proxy classes.
   * - Deregister all the service discovery objects to their respective proxy and skeleton classes.
   * - Destroy singleton instance of the lifecycle manager.
   * \endinternal
   */
  static void Deinitialize() noexcept {
    // Consider the dependencies when changing the deinitialization order below.
    gwm::platform::timer::gs2s::skeleton::SI_TimerSrv_GS2SSkeleton::DestroySingletons();
    gwm::thermmng_thermmng::battthermctrlsrv_si::skeleton::SI_BattThermCtrlSrvSkeleton::DestroySingletons();
    gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton::DestroySingletons();
    gwm::pt_powersys::obcstsrv_si::skeleton::SI_OBCStSrvSkeleton::DestroySingletons();
    gwm::pt_powersys::vehchrgnctrlsrv_si::skeleton::SI_VehChrgnCtrlSrvSkeleton::DestroySingletons();


    GetInstance().GetAccess()->DeInitializeServiceDiscovery();
    GetInstance().Destroy();
  }

  /*!
   * \brief Constructor.
   * \details Shall not directly be used.
   * \context     Init
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  LifecycleManager() = default;

 private:
  /*!
   * \brief Returns an instance of lifecycle manager class
   * \return LifecycleManager (singleton) instance
   * \pre         -
   * \context     Init | Shutdown
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  static amsr::generic::Singleton<LifecycleManager>& GetInstance() noexcept {
    // VECTOR NC AutosarC++17_10-A3.3.2: MD_SOCAL_AutosarC++17_10-A3.3.2_StaticStorageDurationOfNonPODType
    static amsr::generic::Singleton<LifecycleManager> lifecycle_manager;
    return lifecycle_manager;
  }

  /*!
   * \brief          Registers all service discovery objects to the respective proxy and skeleton classes.
   * \pre            -
   * \context        Init
   */
  void InitializeServiceDiscovery() noexcept {
    
    
    gwm::platform::timer::gs2s::skeleton::SI_TimerSrv_GS2SSkeleton::RegisterServiceDiscovery(&si_timersrv_gs2s_skeleton_sd_);
    gwm::thermmng_thermmng::battthermctrlsrv_si::skeleton::SI_BattThermCtrlSrvSkeleton::RegisterServiceDiscovery(&si_battthermctrlsrv_skeleton_sd_);
    gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton::RegisterServiceDiscovery(&si_bmsstsrv_skeleton_sd_);
    gwm::pt_powersys::obcstsrv_si::skeleton::SI_OBCStSrvSkeleton::RegisterServiceDiscovery(&si_obcstsrv_skeleton_sd_);
    gwm::pt_powersys::vehchrgnctrlsrv_si::skeleton::SI_VehChrgnCtrlSrvSkeleton::RegisterServiceDiscovery(&si_vehchrgnctrlsrv_skeleton_sd_);
  }

  // VECTOR NC AutosarC++17_10-M9.3.3: MD_SOCAL_AutosarC++17_10-M9.3.3_Method_can_be_declared_static
  /*!
   * \brief          Deregisters all service discovery objects from the proxy and skeleton classes.
   * \pre            -
   * \context        Shutdown
   */
  void DeInitializeServiceDiscovery() noexcept {
    
    gwm::platform::timer::gs2s::skeleton::SI_TimerSrv_GS2SSkeleton::DeRegisterServiceDiscovery();
    gwm::thermmng_thermmng::battthermctrlsrv_si::skeleton::SI_BattThermCtrlSrvSkeleton::DeRegisterServiceDiscovery();
    gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton::DeRegisterServiceDiscovery();
    gwm::pt_powersys::obcstsrv_si::skeleton::SI_OBCStSrvSkeleton::DeRegisterServiceDiscovery();
    gwm::pt_powersys::vehchrgnctrlsrv_si::skeleton::SI_VehChrgnCtrlSrvSkeleton::DeRegisterServiceDiscovery();
  }




 
  /*!
   * \brief Type alias for the service discovery type on skeleton side
   *        used for the service interface SI_TimerSrv_GS2S.
   */
  using SI_TimerSrv_GS2SSkeletonSdType = gwm::platform::timer::gs2s::skeleton::SI_TimerSrv_GS2SSkeleton::ServiceDiscovery;

  /*!
   * \brief Service discovery instance used on skeleton side for the service interface SI_TimerSrv_GS2S.
   */
  SI_TimerSrv_GS2SSkeletonSdType si_timersrv_gs2s_skeleton_sd_;
 
  /*!
   * \brief Type alias for the service discovery type on skeleton side
   *        used for the service interface SI_BattThermCtrlSrv.
   */
  using SI_BattThermCtrlSrvSkeletonSdType = gwm::thermmng_thermmng::battthermctrlsrv_si::skeleton::SI_BattThermCtrlSrvSkeleton::ServiceDiscovery;

  /*!
   * \brief Service discovery instance used on skeleton side for the service interface SI_BattThermCtrlSrv.
   */
  SI_BattThermCtrlSrvSkeletonSdType si_battthermctrlsrv_skeleton_sd_;
 
  /*!
   * \brief Type alias for the service discovery type on skeleton side
   *        used for the service interface SI_BmsStSrv.
   */
  using SI_BmsStSrvSkeletonSdType = gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton::ServiceDiscovery;

  /*!
   * \brief Service discovery instance used on skeleton side for the service interface SI_BmsStSrv.
   */
  SI_BmsStSrvSkeletonSdType si_bmsstsrv_skeleton_sd_;
 
  /*!
   * \brief Type alias for the service discovery type on skeleton side
   *        used for the service interface SI_OBCStSrv.
   */
  using SI_OBCStSrvSkeletonSdType = gwm::pt_powersys::obcstsrv_si::skeleton::SI_OBCStSrvSkeleton::ServiceDiscovery;

  /*!
   * \brief Service discovery instance used on skeleton side for the service interface SI_OBCStSrv.
   */
  SI_OBCStSrvSkeletonSdType si_obcstsrv_skeleton_sd_;
 
  /*!
   * \brief Type alias for the service discovery type on skeleton side
   *        used for the service interface SI_VehChrgnCtrlSrv.
   */
  using SI_VehChrgnCtrlSrvSkeletonSdType = gwm::pt_powersys::vehchrgnctrlsrv_si::skeleton::SI_VehChrgnCtrlSrvSkeleton::ServiceDiscovery;

  /*!
   * \brief Service discovery instance used on skeleton side for the service interface SI_VehChrgnCtrlSrv.
   */
  SI_VehChrgnCtrlSrvSkeletonSdType si_vehchrgnctrlsrv_skeleton_sd_;
};
}  // namespace internal
}  // namespace socal
}  // namespace amsr

#endif  // DENPENDENCYSRVEXE_INCLUDE_AMSR_SOCAL_INTERNAL_LIFECYCLE_MANAGER_H_

