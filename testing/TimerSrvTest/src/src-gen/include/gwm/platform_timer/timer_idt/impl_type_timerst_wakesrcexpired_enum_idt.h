/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  TiMgrSrvExe/include/gwm/platform_timer/timer_idt/impl_type_timerst_wakesrcexpired_enum_idt.h
 *        \brief
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_modelleddatatypes_api
 *         Commit ID: d9332226824a4a6c4616e2d948d41c862d192343
 *********************************************************************************************************************/

#ifndef TIMGRSRVEXE_INCLUDE_GWM_PLATFORM_TIMER_TIMER_IDT_IMPL_TYPE_TIMERST_WAKESRCEXPIRED_ENUM_IDT_H_
#define TIMGRSRVEXE_INCLUDE_GWM_PLATFORM_TIMER_TIMER_IDT_IMPL_TYPE_TIMERST_WAKESRCEXPIRED_ENUM_IDT_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include <array>
#include <cstdint>
#include <map>
#include <string>
#include <vector>

namespace gwm {
namespace platform_timer {
namespace timer_idt {

/*!
 * \brief Type TimerSt_WakeSrcexpired_Enum_Idt.
 * \remark generated
 * \trace SPEC-5951372
 *
 * IMPLEMENTATION-DATA-TYPE /DataTypes/ImplementationDataTypes/TimerSt_WakeSrcexpired_Enum_Idt
 */
enum class TimerSt_WakeSrcexpired_Enum_Idt : std::uint8_t {
  NoTimer = 0,
  Timing = 1,
  TimeOut = 2
};

}  // namespace timer_idt
}  // namespace platform_timer
}  // namespace gwm

#endif  // TIMGRSRVEXE_INCLUDE_GWM_PLATFORM_TIMER_TIMER_IDT_IMPL_TYPE_TIMERST_WAKESRCEXPIRED_ENUM_IDT_H_
