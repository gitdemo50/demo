
#ifndef TESTS_LOOPER_MOCK_HPP
#define TESTS_LOOPER_MOCK_HPP

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "eventloop.hpp"
#include "events.hpp"

namespace gwm {
namespace testmock {

using EventParamType = gwm::manager::EventLoop::EventParamType;
using EventType = gwm::manager::EventType;
using EventSource = gwm::manager::EventSource;
using EventResult =  gwm::manager::EventResult;

class MockLooper : public gwm::manager::EventLoop {
public:
    MOCK_METHOD(EventResult, postEvent, (EventType et, EventSource es, EventParamType ep), (override));
};


}
}

#endif
