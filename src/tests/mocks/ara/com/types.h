#pragma once
#include <vector>
#include <functional>
#include <memory>

#include "vac/container/string_literals.h"
#include "ara/core/result.h"

namespace ara {
    namespace com {

        struct FindServiceHandle {
            FindServiceHandle(std::string s) {
                instanceSpec = s;
            }
            std::string instanceSpec;
        };
        
        template <typename T>
        using ServiceHandleContainer = std::vector<T>;

        template <typename T>
        using FindServiceHandler = std::function<void(ServiceHandleContainer<T>)>;

        using HandleType = __uint16_t;

        using EventReceiveHandler = std::function<void()>;

        template <typename T>
        using SampleContainer = std::vector<T>;

        template<typename T>
        using SamplePtrInternal = std::shared_ptr<T>;

        enum EventCacheUpdatePolicy {
            kNewestN
        };
    }
}
