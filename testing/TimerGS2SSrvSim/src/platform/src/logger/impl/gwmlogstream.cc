
#include "logger/impl/gwmlogger.hpp"
#include "logger/impl/gwmlogstream.hpp"

namespace gwm {
namespace platform {
namespace impl {

GWMLogStream::~GWMLogStream() {
    Flush();
}

void GWMLogStream::Flush() {
    logger.log(stream, logLevel);
}

}}}
