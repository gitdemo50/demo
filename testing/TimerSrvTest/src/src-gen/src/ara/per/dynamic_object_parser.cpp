// VECTOR SL AutosarC++17_10-A1.1.1: MD_ARAPER_number_of_external_identifiers
/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  TiMgrSrvExe/src/ara/per/dynamic_object_parser.cpp
 *        \brief
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_persistency_config
 *         Commit ID: 3c577be6a7ef0697cae45fabe324d59e826d2342
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include <cstdint>
#include <map>
#include <string>
#include <utility>
#include <vector>


// Static deserializers
#include "amsr/per/deserializer/deserialize_array.h"
#include "amsr/per/deserializer/deserialize_bool.h"
#include "amsr/per/deserializer/deserialize_map.h"
#include "amsr/per/deserializer/deserialize_optional.h"
#include "amsr/per/deserializer/deserialize_string.h"
#include "amsr/per/deserializer/deserialize_trivially_copyable.h"
#include "amsr/per/deserializer/deserialize_vector.h"

#include "amsr/per/deserializer/types.h"
#include "ara/per/detail/dynamic_key_value_storage_decl.h"
#include "ara/per/object_parser.h"
#include "ara/per/internal/byte_order.h"
#include "ara/per/internal/config/key_value_storage/init_value_object_parser.h"
#include "vac/language/switch.h"

namespace ara {
namespace per {

// VECTOR Disable AutosarC++17_10-A5.1.8: MD_ARAPER_AutosarC++17_10-A5.1.8_nested_lambda
// VECTOR Disable AutosarC++17_10-A16.0.1: MD_ARAPER_AutosarC++17_10-A16.0.1_conditional_compilation

#ifdef __GNUC__
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wshadow"
#endif

/*!
   * \brief Converts a vector of bytes to a complex type.
   * \tparam A Type of the function to add the type to the storage.
   * \param[in] add Function used to add the type to the storage.
   * \param[in] buffer Parsed binary data.
   * \param[in] type Name of the parsed complex type.
   * \param[in] net_order The input data is in network byte order (big endian).
   * \pre           -
   * \threadsafe    FALSE
   * \reentrant     FALSE
   * \context       ANY
   * \return Running if no errors occurred.
   *
   * \internal
   * - #10 Deserialize the type.
   *   - #20 Call the passed add function with the deserialized type.
   * \endinternal
   */
template <typename A>
auto ConvertComplexType(A&& add, amsr::per::deserializer::ReadBuffer buffer, vac::container::CStringView type, bool net_order) noexcept -> amsr::json::ParserResult {
  using namespace amsr::per::deserializer;  // NOLINT(build/namespaces)
  using vac::container::literals::operator""_sv;
  amsr::json::ParserResult parser_result{amsr::json::ParserState::kRunning};

  parser_result = vac::language::Switch<amsr::json::ParserResult>(type)
      .Case(ara::per::internal::config::key_value_storage::detail::do_type,
            [&add, &buffer, net_order]() {
              return deserialize_trivially_copyable<double>(buffer, net_order).Map(
                  [&add](DeResult<double> &&des) {
                    add(std::move(des)->second);
                    return amsr::json::ParserState::kRunning;
                  });
            })
      .Case(ara::per::internal::config::key_value_storage::detail::fl_type,
            [&add, &buffer, net_order]() {
              return deserialize_trivially_copyable<float>(buffer, net_order).Map(
                  [&add](DeResult<float> &&des) {
                    add(std::move(des)->second);
                    return amsr::json::ParserState::kRunning;
                  });
            })
      .Default(amsr::json::ParserResult::FromError(PerErrc::kIntegrityError, "ObjectParser: Unknown data type."));

  return parser_result;
}

auto ObjectParser::ParseComplexType() -> amsr::json::ParserResult {
  amsr::json::ParserResult result{amsr::json::JsonErrc::kUserValidationFailed};
  if (VersionIsBinaryBlob(this->version_)) {
    result = Binary([this](ara::core::Span<char const> chars){
      // VECTOR NL AutosarC++17_10-A5.2.4: MD_ARAPER_AutosarC++17_10-A5.2.4_reinterpret_cast
      amsr::per::deserializer::ReadBuffer const bytes{reinterpret_cast<vac::language::byte const*>(chars.data()), chars.size()};
      return ConvertComplexType([this](auto&& data) -> void { this->AddData(data); },
                                bytes,
                                vac::container::CStringView::FromString(this->type_),
                                (VersionIsNetOrder(this->version_) || this->net_order_)).Drop();
    });
  } else {
    result = NumberArray<vac::language::byte>(
        [this](std::size_t, vac::language::byte i) { binary_list_.push_back(i); })
        .AndThen([this](amsr::json::ParserState) {
          return ConvertComplexType(
              [this](auto&& data) -> void { this->AddData(data); },
              amsr::per::deserializer::ReadBuffer{this->binary_list_},
              vac::container::CStringView::FromString(this->type_),
              (VersionIsNetOrder(this->version_) || this->net_order_));
        });
  }
  return result;
}

auto internal::config::key_value_storage::InitValueObjectParser::ParseComplexType() -> amsr::json::ParserResult {
  return Binary([this](ara::core::Span<char const> chars) {
    // VECTOR NL AutosarC++17_10-A5.2.4: MD_ARAPER_AutosarC++17_10-A5.2.4_reinterpret_cast
    amsr::per::deserializer::ReadBuffer const bytes{reinterpret_cast<vac::language::byte const*>(chars.data()), chars.size()};
    return ConvertComplexType([this](auto&& data) -> void { this->init_value_config_.SetValue(data); }, bytes,
                              vac::container::CStringView::FromString(this->type_), true).Drop();
  });
}

#ifdef __GNUC__
#pragma GCC diagnostic pop
#endif

// VECTOR Enable AutosarC++17_10-A16.0.1
// VECTOR Enable AutosarC++17_10-A5.1.8
}  // namespace per
}  // namespace ara

