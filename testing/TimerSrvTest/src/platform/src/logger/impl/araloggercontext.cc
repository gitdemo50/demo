#include "logger/logger.hpp"

namespace gwm {
namespace platform {

using vac::container::literals::operator""_sv;

template<>
ara::log::Logger& createLogger<ara::log::Logger>(std::string name) {
    return ara::log::CreateLogger(name.substr(0,4), "Platform logger ARA"_sv);
}

}
}