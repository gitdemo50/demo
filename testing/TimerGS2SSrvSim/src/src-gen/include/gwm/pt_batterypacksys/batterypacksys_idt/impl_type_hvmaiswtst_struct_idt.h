/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/include/gwm/pt_batterypacksys/batterypacksys_idt/impl_type_hvmaiswtst_struct_idt.h
 *        \brief
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_modelleddatatypes_api
 *         Commit ID: d9332226824a4a6c4616e2d948d41c862d192343
 *********************************************************************************************************************/

#ifndef DENPENDENCYSRVEXE_INCLUDE_GWM_PT_BATTERYPACKSYS_BATTERYPACKSYS_IDT_IMPL_TYPE_HVMAISWTST_STRUCT_IDT_H_
#define DENPENDENCYSRVEXE_INCLUDE_GWM_PT_BATTERYPACKSYS_BATTERYPACKSYS_IDT_IMPL_TYPE_HVMAISWTST_STRUCT_IDT_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include <array>
#include <cstdint>
#include <map>
#include <string>
#include <vector>
#include "gwm/pt_batterypacksys/batterypacksys_idt/impl_type_hvmaiswtst_dcchrgnegply_enum_idt.h"
#include "gwm/pt_batterypacksys/batterypacksys_idt/impl_type_hvmaiswtst_dcchrgposply_enum_idt.h"
#include "gwm/pt_batterypacksys/batterypacksys_idt/impl_type_hvmaiswtst_negrlyst_enum_idt.h"
#include "gwm/pt_batterypacksys/batterypacksys_idt/impl_type_hvmaiswtst_posrlyst_enum_idt.h"
#include "gwm/pt_batterypacksys/batterypacksys_idt/impl_type_hvmaiswtst_prerly_enum_idt.h"
#include "gwm/pt_batterypacksys/batterypacksys_idt/impl_type_hvmaiswtst_s1rly_enum_idt.h"
#include "gwm/pt_batterypacksys/batterypacksys_idt/impl_type_hvmaiswtst_s2rly_enum_idt.h"
#include "gwm/pt_batterypacksys/batterypacksys_idt/impl_type_hvmaiswtst_s3rly_enum_idt.h"

namespace gwm {
namespace pt_batterypacksys {
namespace batterypacksys_idt {

// VECTOR Disable AutosarC++17_10-A12.6.1: MD_MDTG_A12.6.1_GeneratedStructUninitializedMembers
// VECTOR Disable AutosarC++17_10-M8.5.1: MD_MDTG_M8.5.1_GeneratedStructUninitializedMembers
/*!
 * \brief Type HVMaiSwtSt_Struct_Idt.
 * \remark generated
 * \trace SPEC-5951372
 *
 * IMPLEMENTATION-DATA-TYPE /DataTypes/ImplementationDataTypes/HVMaiSwtSt_Struct_Idt
 */
struct HVMaiSwtSt_Struct_Idt {
  using PosRlySt_Idt_generated_type = gwm::pt_batterypacksys::batterypacksys_idt::HVMaiSwtSt_PosRlySt_Enum_Idt;
  using NegRlyst_Idt_generated_type = gwm::pt_batterypacksys::batterypacksys_idt::HVMaiSwtSt_NegRlyst_Enum_Idt;
  using PreRly_Idt_generated_type = gwm::pt_batterypacksys::batterypacksys_idt::HVMaiSwtSt_PreRly_Enum_Idt;
  using DCChrgPosPly_Idt_generated_type = gwm::pt_batterypacksys::batterypacksys_idt::HVMaiSwtSt_DCChrgPosPly_Enum_Idt;
  using DCChrgNegPly_Idt_generated_type = gwm::pt_batterypacksys::batterypacksys_idt::HVMaiSwtSt_DCChrgNegPly_Enum_Idt;
  using S1Rly_Idt_generated_type = gwm::pt_batterypacksys::batterypacksys_idt::HVMaiSwtSt_S1Rly_Enum_Idt;
  using S2Rly_Idt_generated_type = gwm::pt_batterypacksys::batterypacksys_idt::HVMaiSwtSt_S2Rly_Enum_Idt;
  using S3Rly_Idt_generated_type = gwm::pt_batterypacksys::batterypacksys_idt::HVMaiSwtSt_S3Rly_Enum_Idt;

  PosRlySt_Idt_generated_type PosRlySt_Idt;
  NegRlyst_Idt_generated_type NegRlyst_Idt;
  PreRly_Idt_generated_type PreRly_Idt;
  DCChrgPosPly_Idt_generated_type DCChrgPosPly_Idt;
  DCChrgNegPly_Idt_generated_type DCChrgNegPly_Idt;
  S1Rly_Idt_generated_type S1Rly_Idt;
  S2Rly_Idt_generated_type S2Rly_Idt;
  S3Rly_Idt_generated_type S3Rly_Idt;
};
// VECTOR Enable AutosarC++17_10-A12.6.1
// VECTOR Enable AutosarC++17_10-M8.5.1

/*!
 * \brief Compare for equality with another HVMaiSwtSt_Struct_Idt instance.
 */
inline bool operator==(HVMaiSwtSt_Struct_Idt const& l,
                       HVMaiSwtSt_Struct_Idt const& r) noexcept {
  return (&l == &r) || ((l.PosRlySt_Idt == r.PosRlySt_Idt)
                         && (l.NegRlyst_Idt == r.NegRlyst_Idt)
                         && (l.PreRly_Idt == r.PreRly_Idt)
                         && (l.DCChrgPosPly_Idt == r.DCChrgPosPly_Idt)
                         && (l.DCChrgNegPly_Idt == r.DCChrgNegPly_Idt)
                         && (l.S1Rly_Idt == r.S1Rly_Idt)
                         && (l.S2Rly_Idt == r.S2Rly_Idt)
                         && (l.S3Rly_Idt == r.S3Rly_Idt)
  );
}

/*!
 * \brief Compare for inequality with another HVMaiSwtSt_Struct_Idt instance.
 */
inline bool operator!=(HVMaiSwtSt_Struct_Idt const& l,
                       HVMaiSwtSt_Struct_Idt const& r) noexcept {
  return !(l == r);
}

}  // namespace batterypacksys_idt
}  // namespace pt_batterypacksys
}  // namespace gwm

#endif  // DENPENDENCYSRVEXE_INCLUDE_GWM_PT_BATTERYPACKSYS_BATTERYPACKSYS_IDT_IMPL_TYPE_HVMAISWTST_STRUCT_IDT_H_
