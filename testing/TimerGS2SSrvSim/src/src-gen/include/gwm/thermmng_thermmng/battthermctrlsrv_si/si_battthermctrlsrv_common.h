/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/include/gwm/thermmng_thermmng/battthermctrlsrv_si/si_battthermctrlsrv_common.h
 *        \brief  Header for service 'SI_BattThermCtrlSrv'.
 *
 *      \details  The service interface provides vehicle thermal control strategy level control
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_socal
 *         Commit ID: 209b3c0234013e84e3170ee826c9a8c6022de194
 *********************************************************************************************************************/

#ifndef DENPENDENCYSRVEXE_INCLUDE_GWM_THERMMNG_THERMMNG_BATTTHERMCTRLSRV_SI_SI_BATTTHERMCTRLSRV_COMMON_H_
#define DENPENDENCYSRVEXE_INCLUDE_GWM_THERMMNG_THERMMNG_BATTTHERMCTRLSRV_SI_SI_BATTTHERMCTRLSRV_COMMON_H_

/*!
 * \trace SPEC-4980247, SPEC-4980248, SPEC-5951130, SPEC-4980251
 */
/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "ara/com/types.h"
#include "gwm/thermmng_thermmng/battthermctrlsrv_si/SI_BattThermCtrlSrv.h"
#include "gwm/thermmng_thermmng/thermmng_idt/impl_type_battpackheatctrl_clntid_enum_idt.h"
#include "gwm/thermmng_thermmng/thermmng_idt/impl_type_battpackheatctrl_command_enum_idt.h"
#include "gwm/thermmng_thermmng/thermmng_idt/impl_type_battpackheatctrl_response_enum_idt.h"
#include "gwm/thermmng_thermmng/thermmng_idt/impl_type_battthermctrl_clntid_enum_idt.h"
#include "gwm/thermmng_thermmng/thermmng_idt/impl_type_battthermctrl_command_integer_idt.h"
#include "gwm/thermmng_thermmng/thermmng_idt/impl_type_battthermctrl_response_enum_idt.h"

#endif  // DENPENDENCYSRVEXE_INCLUDE_GWM_THERMMNG_THERMMNG_BATTTHERMCTRLSRV_SI_SI_BATTTHERMCTRLSRV_COMMON_H_
