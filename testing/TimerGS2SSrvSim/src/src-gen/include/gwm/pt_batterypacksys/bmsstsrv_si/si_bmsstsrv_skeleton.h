/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/include/gwm/pt_batterypacksys/bmsstsrv_si/si_bmsstsrv_skeleton.h
 *        \brief  Skeleton for service 'SI_BmsStSrv'.
 *
 *      \details  The service interface provides HV battery real time current information
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_socal
 *         Commit ID: 209b3c0234013e84e3170ee826c9a8c6022de194
 *********************************************************************************************************************/

#ifndef DENPENDENCYSRVEXE_INCLUDE_GWM_PT_BATTERYPACKSYS_BMSSTSRV_SI_SI_BMSSTSRV_SKELETON_H_
#define DENPENDENCYSRVEXE_INCLUDE_GWM_PT_BATTERYPACKSYS_BMSSTSRV_SI_SI_BMSSTSRV_SKELETON_H_

/*!
 * \trace SPEC-4980239
 */
/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "amsr/generic/singleton_wrapper.h"
#include "amsr/socal/events/skeleton_event.h"
#include "amsr/socal/fields/skeleton_field.h"
#include "amsr/socal/internal/events/skeleton_event_manager_interface.h"
#include "amsr/socal/internal/service_discovery/service_discovery.h"
#include "amsr/socal/skeleton.h"
#include "ara/core/future.h"
#include "ara/core/instance_specifier.h"
#include "gwm/pt_batterypacksys/bmsstsrv_si/SI_BmsStSrv_skeleton_impl_interface.h"
#include "gwm/pt_batterypacksys/bmsstsrv_si/SI_BmsStSrv_types.h"

/*!
 * \trace SPEC-4980240
 */
namespace gwm {
namespace pt_batterypacksys {
namespace bmsstsrv_si {
/*!
 * \trace SPEC-4980241
 */
namespace skeleton {
/*!
 * \brief Forward declaration for inserting as a type into the template class SkeletonEvent
 */
class SI_BmsStSrvSkeleton;

/*!
 * \trace SPEC-4980244
 */
namespace methods {

}  // namespace methods

/*!
 * \trace SPEC-4980243
 */
namespace events {

/*!
 * \brief Type alias for service event 'BMSChrgTi', that is part of the skeleton.
 *
 * \trace SPEC-4980342
 */
using BMSChrgTi = ::amsr::socal::events::SkeletonEvent<
    gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton,
    ::gwm::pt_batterypacksys::batterypacksys_idt::BMSChrgTi_Integer_Idt,
    gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface,
    ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::BMSChrgTi_Integer_Idt>,
    &gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface::GetEventManagerBMSChrgTi>;

/*!
 * \brief Type alias for service event 'BMSConDchrMaxPwr', that is part of the skeleton.
 *
 * \trace SPEC-4980342
 */
using BMSConDchrMaxPwr = ::amsr::socal::events::SkeletonEvent<
    gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton,
    ::gwm::pt_batterypacksys::batterypacksys_idt::BMSConDchrMaxPwr_Float_Idt,
    gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface,
    ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::BMSConDchrMaxPwr_Float_Idt>,
    &gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface::GetEventManagerBMSConDchrMaxPwr>;

/*!
 * \brief Type alias for service event 'BMSConFBMaxPwr', that is part of the skeleton.
 *
 * \trace SPEC-4980342
 */
using BMSConFBMaxPwr = ::amsr::socal::events::SkeletonEvent<
    gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton,
    ::gwm::pt_batterypacksys::batterypacksys_idt::BMSConFBMaxPwr_Float_Idt,
    gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface,
    ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::BMSConFBMaxPwr_Float_Idt>,
    &gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface::GetEventManagerBMSConFBMaxPwr>;

/*!
 * \brief Type alias for service event 'BMSPlsDchrMaxPwr', that is part of the skeleton.
 *
 * \trace SPEC-4980342
 */
using BMSPlsDchrMaxPwr = ::amsr::socal::events::SkeletonEvent<
    gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton,
    ::gwm::pt_batterypacksys::batterypacksys_idt::BMSPlsDchrMaxPwr_Float_Idt,
    gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface,
    ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::BMSPlsDchrMaxPwr_Float_Idt>,
    &gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface::GetEventManagerBMSPlsDchrMaxPwr>;

/*!
 * \brief Type alias for service event 'BMSPlsFBMaxPwr', that is part of the skeleton.
 *
 * \trace SPEC-4980342
 */
using BMSPlsFBMaxPwr = ::amsr::socal::events::SkeletonEvent<
    gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton,
    ::gwm::pt_batterypacksys::batterypacksys_idt::BMSPlsFBMaxPwr_Float_Idt,
    gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface,
    ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::BMSPlsFBMaxPwr_Float_Idt>,
    &gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface::GetEventManagerBMSPlsFBMaxPwr>;

/*!
 * \brief Type alias for service event 'BmsInletCooltT', that is part of the skeleton.
 *
 * \trace SPEC-4980342
 */
using BmsInletCooltT = ::amsr::socal::events::SkeletonEvent<
    gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton,
    ::gwm::pt_batterypacksys::batterypacksys_idt::BmsInletCooltT_Integer_Idt,
    gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface,
    ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::BmsInletCooltT_Integer_Idt>,
    &gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface::GetEventManagerBmsInletCooltT>;

/*!
 * \brief Type alias for service event 'BmsIsoR', that is part of the skeleton.
 *
 * \trace SPEC-4980342
 */
using BmsIsoR = ::amsr::socal::events::SkeletonEvent<
    gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton,
    ::gwm::pt_batterypacksys::batterypacksys_idt::BmsIsoR_Integer_Idt,
    gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface,
    ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::BmsIsoR_Integer_Idt>,
    &gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface::GetEventManagerBmsIsoR>;

/*!
 * \brief Type alias for service event 'BmsOutletCooltT', that is part of the skeleton.
 *
 * \trace SPEC-4980342
 */
using BmsOutletCooltT = ::amsr::socal::events::SkeletonEvent<
    gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton,
    ::gwm::pt_batterypacksys::batterypacksys_idt::BmsOutletCooltT_Integer_Idt,
    gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface,
    ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::BmsOutletCooltT_Integer_Idt>,
    &gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface::GetEventManagerBmsOutletCooltT>;

/*!
 * \brief Type alias for service event 'CellMaxT', that is part of the skeleton.
 *
 * \trace SPEC-4980342
 */
using CellMaxT = ::amsr::socal::events::SkeletonEvent<
    gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton,
    ::gwm::pt_batterypacksys::batterypacksys_idt::CellMaxT_Struct_Idt,
    gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface,
    ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::CellMaxT_Struct_Idt>,
    &gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface::GetEventManagerCellMaxT>;

/*!
 * \brief Type alias for service event 'CellMaxU', that is part of the skeleton.
 *
 * \trace SPEC-4980342
 */
using CellMaxU = ::amsr::socal::events::SkeletonEvent<
    gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton,
    ::gwm::pt_batterypacksys::batterypacksys_idt::CellMaxU_Struct_Idt,
    gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface,
    ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::CellMaxU_Struct_Idt>,
    &gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface::GetEventManagerCellMaxU>;

/*!
 * \brief Type alias for service event 'CellMinT', that is part of the skeleton.
 *
 * \trace SPEC-4980342
 */
using CellMinT = ::amsr::socal::events::SkeletonEvent<
    gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton,
    ::gwm::pt_batterypacksys::batterypacksys_idt::CellMinT_Struct_Idt,
    gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface,
    ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::CellMinT_Struct_Idt>,
    &gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface::GetEventManagerCellMinT>;

/*!
 * \brief Type alias for service event 'CellMinU', that is part of the skeleton.
 *
 * \trace SPEC-4980342
 */
using CellMinU = ::amsr::socal::events::SkeletonEvent<
    gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton,
    ::gwm::pt_batterypacksys::batterypacksys_idt::CellMinU_Struct_Idt,
    gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface,
    ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::CellMinU_Struct_Idt>,
    &gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface::GetEventManagerCellMinU>;

/*!
 * \brief Type alias for service event 'HVBattDispySoc', that is part of the skeleton.
 *
 * \trace SPEC-4980342
 */
using HVBattDispySoc = ::amsr::socal::events::SkeletonEvent<
    gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton,
    ::gwm::pt_batterypacksys::batterypacksys_idt::HVBattDispySoc_Float_Idt,
    gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface,
    ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::HVBattDispySoc_Float_Idt>,
    &gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface::GetEventManagerHVBattDispySoc>;

/*!
 * \brief Type alias for service event 'HVBattI', that is part of the skeleton.
 *
 * \trace SPEC-4980342
 */
using HVBattI = ::amsr::socal::events::SkeletonEvent<
    gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton,
    ::gwm::pt_batterypacksys::batterypacksys_idt::HVBattI_Float_Idt,
    gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface,
    ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::HVBattI_Float_Idt>,
    &gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface::GetEventManagerHVBattI>;

/*!
 * \brief Type alias for service event 'HVBattInnerSoc', that is part of the skeleton.
 *
 * \trace SPEC-4980342
 */
using HVBattInnerSoc = ::amsr::socal::events::SkeletonEvent<
    gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton,
    ::gwm::pt_batterypacksys::batterypacksys_idt::HVBattInnerSoc_Float_Idt,
    gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface,
    ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::HVBattInnerSoc_Float_Idt>,
    &gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface::GetEventManagerHVBattInnerSoc>;

/*!
 * \brief Type alias for service event 'HVBattSoh', that is part of the skeleton.
 *
 * \trace SPEC-4980342
 */
using HVBattSoh = ::amsr::socal::events::SkeletonEvent<
    gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton,
    ::gwm::pt_batterypacksys::batterypacksys_idt::HVBattSoh_Float_Idt,
    gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface,
    ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::HVBattSoh_Float_Idt>,
    &gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface::GetEventManagerHVBattSoh>;

/*!
 * \brief Type alias for service event 'HVBattTotEgy', that is part of the skeleton.
 *
 * \trace SPEC-4980342
 */
using HVBattTotEgy = ::amsr::socal::events::SkeletonEvent<
    gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton,
    ::gwm::pt_batterypacksys::batterypacksys_idt::HVBattTotEgy_Float_Idt,
    gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface,
    ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::HVBattTotEgy_Float_Idt>,
    &gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface::GetEventManagerHVBattTotEgy>;

/*!
 * \brief Type alias for service event 'HVBattU', that is part of the skeleton.
 *
 * \trace SPEC-4980342
 */
using HVBattU = ::amsr::socal::events::SkeletonEvent<
    gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton,
    ::gwm::pt_batterypacksys::batterypacksys_idt::HVBattU_Float_Idt,
    gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface,
    ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::HVBattU_Float_Idt>,
    &gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface::GetEventManagerHVBattU>;

/*!
 * \brief Type alias for service event 'ModulAvrgT', that is part of the skeleton.
 *
 * \trace SPEC-4980342
 */
using ModulAvrgT = ::amsr::socal::events::SkeletonEvent<
    gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton,
    ::gwm::pt_batterypacksys::batterypacksys_idt::ModulAvrgT_Integer_Idt,
    gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface,
    ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::ModulAvrgT_Integer_Idt>,
    &gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface::GetEventManagerModulAvrgT>;

}  // namespace events

/*!
 * \trace SPEC-4980245
 */
namespace fields {
/*!
 * \brief Type alias for the notification of field 'BMSChrgSt'.
 */
using FieldNotifierBMSChrgSt = ::amsr::socal::events::SkeletonEvent<
  gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton,
  ::gwm::pt_batterypacksys::batterypacksys_idt::BMSChrgSt_Enum_Idt,
  gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface,
  ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::BMSChrgSt_Enum_Idt>,
  &gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface::GetFieldNotifierBMSChrgSt>;

/*!
 * \brief Type alias for the notifier configuration of field 'BMSChrgSt'.
 */
using FieldNotifierConfigBMSChrgSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasNotifier<true, FieldNotifierBMSChrgSt>;

/*!
 * \brief Type alias for the getter configuration of field 'BMSChrgSt'.
 */
using FieldGetterConfigBMSChrgSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasGetter<true>;

/*!
 * \brief Type alias for the setter configuration of field 'BMSChrgSt'.
 */
using FieldSetterConfigBMSChrgSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasSetter<false>;

/*!
 * \brief Field name.
 */
struct BMSChrgStName {
  /*!
   * \brief Field name string.
   */
  static constexpr vac::container::CStringView Name{"BMSChrgSt"_sv};
};

/*!
 * \brief Type alias for the parameterization of field 'BMSChrgSt'.
 */
using FieldConfigBMSChrgSt = ::amsr::socal::internal::fields::SkeletonFieldConfig<gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton, FieldNotifierConfigBMSChrgSt, FieldGetterConfigBMSChrgSt, FieldSetterConfigBMSChrgSt, BMSChrgStName>;

/*!
 * \brief Type alias for service field 'BMSChrgSt', that is part of the skeleton.
 *
 * \trace SPEC-4980343
 */
using BMSChrgSt = ::amsr::socal::fields::SkeletonField<::gwm::pt_batterypacksys::batterypacksys_idt::BMSChrgSt_Enum_Idt, FieldConfigBMSChrgSt>;

/*!
 * \brief Type alias for the notification of field 'BMSDCChrgCnctSt'.
 */
using FieldNotifierBMSDCChrgCnctSt = ::amsr::socal::events::SkeletonEvent<
  gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton,
  ::gwm::pt_batterypacksys::batterypacksys_idt::BMSDCChrgCnctSt_Enum_Idt,
  gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface,
  ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::BMSDCChrgCnctSt_Enum_Idt>,
  &gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface::GetFieldNotifierBMSDCChrgCnctSt>;

/*!
 * \brief Type alias for the notifier configuration of field 'BMSDCChrgCnctSt'.
 */
using FieldNotifierConfigBMSDCChrgCnctSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasNotifier<true, FieldNotifierBMSDCChrgCnctSt>;

/*!
 * \brief Type alias for the getter configuration of field 'BMSDCChrgCnctSt'.
 */
using FieldGetterConfigBMSDCChrgCnctSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasGetter<true>;

/*!
 * \brief Type alias for the setter configuration of field 'BMSDCChrgCnctSt'.
 */
using FieldSetterConfigBMSDCChrgCnctSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasSetter<false>;

/*!
 * \brief Field name.
 */
struct BMSDCChrgCnctStName {
  /*!
   * \brief Field name string.
   */
  static constexpr vac::container::CStringView Name{"BMSDCChrgCnctSt"_sv};
};

/*!
 * \brief Type alias for the parameterization of field 'BMSDCChrgCnctSt'.
 */
using FieldConfigBMSDCChrgCnctSt = ::amsr::socal::internal::fields::SkeletonFieldConfig<gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton, FieldNotifierConfigBMSDCChrgCnctSt, FieldGetterConfigBMSDCChrgCnctSt, FieldSetterConfigBMSDCChrgCnctSt, BMSDCChrgCnctStName>;

/*!
 * \brief Type alias for service field 'BMSDCChrgCnctSt', that is part of the skeleton.
 *
 * \trace SPEC-4980343
 */
using BMSDCChrgCnctSt = ::amsr::socal::fields::SkeletonField<::gwm::pt_batterypacksys::batterypacksys_idt::BMSDCChrgCnctSt_Enum_Idt, FieldConfigBMSDCChrgCnctSt>;

/*!
 * \brief Type alias for the notification of field 'BMSDCChrgEndReas'.
 */
using FieldNotifierBMSDCChrgEndReas = ::amsr::socal::events::SkeletonEvent<
  gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton,
  ::gwm::pt_batterypacksys::batterypacksys_idt::BMSDCChrgEndReas_Enum_Idt,
  gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface,
  ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::BMSDCChrgEndReas_Enum_Idt>,
  &gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface::GetFieldNotifierBMSDCChrgEndReas>;

/*!
 * \brief Type alias for the notifier configuration of field 'BMSDCChrgEndReas'.
 */
using FieldNotifierConfigBMSDCChrgEndReas = ::amsr::socal::internal::fields::SkeletonFieldParams::HasNotifier<true, FieldNotifierBMSDCChrgEndReas>;

/*!
 * \brief Type alias for the getter configuration of field 'BMSDCChrgEndReas'.
 */
using FieldGetterConfigBMSDCChrgEndReas = ::amsr::socal::internal::fields::SkeletonFieldParams::HasGetter<true>;

/*!
 * \brief Type alias for the setter configuration of field 'BMSDCChrgEndReas'.
 */
using FieldSetterConfigBMSDCChrgEndReas = ::amsr::socal::internal::fields::SkeletonFieldParams::HasSetter<false>;

/*!
 * \brief Field name.
 */
struct BMSDCChrgEndReasName {
  /*!
   * \brief Field name string.
   */
  static constexpr vac::container::CStringView Name{"BMSDCChrgEndReas"_sv};
};

/*!
 * \brief Type alias for the parameterization of field 'BMSDCChrgEndReas'.
 */
using FieldConfigBMSDCChrgEndReas = ::amsr::socal::internal::fields::SkeletonFieldConfig<gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton, FieldNotifierConfigBMSDCChrgEndReas, FieldGetterConfigBMSDCChrgEndReas, FieldSetterConfigBMSDCChrgEndReas, BMSDCChrgEndReasName>;

/*!
 * \brief Type alias for service field 'BMSDCChrgEndReas', that is part of the skeleton.
 *
 * \trace SPEC-4980343
 */
using BMSDCChrgEndReas = ::amsr::socal::fields::SkeletonField<::gwm::pt_batterypacksys::batterypacksys_idt::BMSDCChrgEndReas_Enum_Idt, FieldConfigBMSDCChrgEndReas>;

/*!
 * \brief Type alias for the notification of field 'BMSDchrgPrmsSt'.
 */
using FieldNotifierBMSDchrgPrmsSt = ::amsr::socal::events::SkeletonEvent<
  gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton,
  ::gwm::pt_batterypacksys::batterypacksys_idt::BMSDchrgPrmsSt_Enum_Idt,
  gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface,
  ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::BMSDchrgPrmsSt_Enum_Idt>,
  &gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface::GetFieldNotifierBMSDchrgPrmsSt>;

/*!
 * \brief Type alias for the notifier configuration of field 'BMSDchrgPrmsSt'.
 */
using FieldNotifierConfigBMSDchrgPrmsSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasNotifier<true, FieldNotifierBMSDchrgPrmsSt>;

/*!
 * \brief Type alias for the getter configuration of field 'BMSDchrgPrmsSt'.
 */
using FieldGetterConfigBMSDchrgPrmsSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasGetter<true>;

/*!
 * \brief Type alias for the setter configuration of field 'BMSDchrgPrmsSt'.
 */
using FieldSetterConfigBMSDchrgPrmsSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasSetter<false>;

/*!
 * \brief Field name.
 */
struct BMSDchrgPrmsStName {
  /*!
   * \brief Field name string.
   */
  static constexpr vac::container::CStringView Name{"BMSDchrgPrmsSt"_sv};
};

/*!
 * \brief Type alias for the parameterization of field 'BMSDchrgPrmsSt'.
 */
using FieldConfigBMSDchrgPrmsSt = ::amsr::socal::internal::fields::SkeletonFieldConfig<gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton, FieldNotifierConfigBMSDchrgPrmsSt, FieldGetterConfigBMSDchrgPrmsSt, FieldSetterConfigBMSDchrgPrmsSt, BMSDchrgPrmsStName>;

/*!
 * \brief Type alias for service field 'BMSDchrgPrmsSt', that is part of the skeleton.
 *
 * \trace SPEC-4980343
 */
using BMSDchrgPrmsSt = ::amsr::socal::fields::SkeletonField<::gwm::pt_batterypacksys::batterypacksys_idt::BMSDchrgPrmsSt_Enum_Idt, FieldConfigBMSDchrgPrmsSt>;

/*!
 * \brief Type alias for the notification of field 'BMSHeatRunawaySt'.
 */
using FieldNotifierBMSHeatRunawaySt = ::amsr::socal::events::SkeletonEvent<
  gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton,
  ::gwm::pt_batterypacksys::batterypacksys_idt::BMSHeatRunawaySt_Enum_Idt,
  gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface,
  ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::BMSHeatRunawaySt_Enum_Idt>,
  &gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface::GetFieldNotifierBMSHeatRunawaySt>;

/*!
 * \brief Type alias for the notifier configuration of field 'BMSHeatRunawaySt'.
 */
using FieldNotifierConfigBMSHeatRunawaySt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasNotifier<true, FieldNotifierBMSHeatRunawaySt>;

/*!
 * \brief Type alias for the getter configuration of field 'BMSHeatRunawaySt'.
 */
using FieldGetterConfigBMSHeatRunawaySt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasGetter<true>;

/*!
 * \brief Type alias for the setter configuration of field 'BMSHeatRunawaySt'.
 */
using FieldSetterConfigBMSHeatRunawaySt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasSetter<false>;

/*!
 * \brief Field name.
 */
struct BMSHeatRunawayStName {
  /*!
   * \brief Field name string.
   */
  static constexpr vac::container::CStringView Name{"BMSHeatRunawaySt"_sv};
};

/*!
 * \brief Type alias for the parameterization of field 'BMSHeatRunawaySt'.
 */
using FieldConfigBMSHeatRunawaySt = ::amsr::socal::internal::fields::SkeletonFieldConfig<gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton, FieldNotifierConfigBMSHeatRunawaySt, FieldGetterConfigBMSHeatRunawaySt, FieldSetterConfigBMSHeatRunawaySt, BMSHeatRunawayStName>;

/*!
 * \brief Type alias for service field 'BMSHeatRunawaySt', that is part of the skeleton.
 *
 * \trace SPEC-4980343
 */
using BMSHeatRunawaySt = ::amsr::socal::fields::SkeletonField<::gwm::pt_batterypacksys::batterypacksys_idt::BMSHeatRunawaySt_Enum_Idt, FieldConfigBMSHeatRunawaySt>;

/*!
 * \brief Type alias for the notification of field 'BMSInnerSOCSt'.
 */
using FieldNotifierBMSInnerSOCSt = ::amsr::socal::events::SkeletonEvent<
  gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton,
  ::gwm::pt_batterypacksys::batterypacksys_idt::BMSInnerSOCSt_Struct_Idt,
  gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface,
  ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::BMSInnerSOCSt_Struct_Idt>,
  &gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface::GetFieldNotifierBMSInnerSOCSt>;

/*!
 * \brief Type alias for the notifier configuration of field 'BMSInnerSOCSt'.
 */
using FieldNotifierConfigBMSInnerSOCSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasNotifier<true, FieldNotifierBMSInnerSOCSt>;

/*!
 * \brief Type alias for the getter configuration of field 'BMSInnerSOCSt'.
 */
using FieldGetterConfigBMSInnerSOCSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasGetter<true>;

/*!
 * \brief Type alias for the setter configuration of field 'BMSInnerSOCSt'.
 */
using FieldSetterConfigBMSInnerSOCSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasSetter<false>;

/*!
 * \brief Field name.
 */
struct BMSInnerSOCStName {
  /*!
   * \brief Field name string.
   */
  static constexpr vac::container::CStringView Name{"BMSInnerSOCSt"_sv};
};

/*!
 * \brief Type alias for the parameterization of field 'BMSInnerSOCSt'.
 */
using FieldConfigBMSInnerSOCSt = ::amsr::socal::internal::fields::SkeletonFieldConfig<gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton, FieldNotifierConfigBMSInnerSOCSt, FieldGetterConfigBMSInnerSOCSt, FieldSetterConfigBMSInnerSOCSt, BMSInnerSOCStName>;

/*!
 * \brief Type alias for service field 'BMSInnerSOCSt', that is part of the skeleton.
 *
 * \trace SPEC-4980343
 */
using BMSInnerSOCSt = ::amsr::socal::fields::SkeletonField<::gwm::pt_batterypacksys::batterypacksys_idt::BMSInnerSOCSt_Struct_Idt, FieldConfigBMSInnerSOCSt>;

/*!
 * \brief Type alias for the notification of field 'BMSIntelTempMgtSt'.
 */
using FieldNotifierBMSIntelTempMgtSt = ::amsr::socal::events::SkeletonEvent<
  gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton,
  ::gwm::pt_batterypacksys::batterypacksys_idt::BMSIntelTempMgtSt_Enum_Idt,
  gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface,
  ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::BMSIntelTempMgtSt_Enum_Idt>,
  &gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface::GetFieldNotifierBMSIntelTempMgtSt>;

/*!
 * \brief Type alias for the notifier configuration of field 'BMSIntelTempMgtSt'.
 */
using FieldNotifierConfigBMSIntelTempMgtSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasNotifier<true, FieldNotifierBMSIntelTempMgtSt>;

/*!
 * \brief Type alias for the getter configuration of field 'BMSIntelTempMgtSt'.
 */
using FieldGetterConfigBMSIntelTempMgtSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasGetter<true>;

/*!
 * \brief Type alias for the setter configuration of field 'BMSIntelTempMgtSt'.
 */
using FieldSetterConfigBMSIntelTempMgtSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasSetter<false>;

/*!
 * \brief Field name.
 */
struct BMSIntelTempMgtStName {
  /*!
   * \brief Field name string.
   */
  static constexpr vac::container::CStringView Name{"BMSIntelTempMgtSt"_sv};
};

/*!
 * \brief Type alias for the parameterization of field 'BMSIntelTempMgtSt'.
 */
using FieldConfigBMSIntelTempMgtSt = ::amsr::socal::internal::fields::SkeletonFieldConfig<gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton, FieldNotifierConfigBMSIntelTempMgtSt, FieldGetterConfigBMSIntelTempMgtSt, FieldSetterConfigBMSIntelTempMgtSt, BMSIntelTempMgtStName>;

/*!
 * \brief Type alias for service field 'BMSIntelTempMgtSt', that is part of the skeleton.
 *
 * \trace SPEC-4980343
 */
using BMSIntelTempMgtSt = ::amsr::socal::fields::SkeletonField<::gwm::pt_batterypacksys::batterypacksys_idt::BMSIntelTempMgtSt_Enum_Idt, FieldConfigBMSIntelTempMgtSt>;

/*!
 * \brief Type alias for the notification of field 'BMSLowTempSt'.
 */
using FieldNotifierBMSLowTempSt = ::amsr::socal::events::SkeletonEvent<
  gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton,
  ::gwm::pt_batterypacksys::batterypacksys_idt::BMSLowTempSt_Struct_Idt,
  gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface,
  ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::BMSLowTempSt_Struct_Idt>,
  &gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface::GetFieldNotifierBMSLowTempSt>;

/*!
 * \brief Type alias for the notifier configuration of field 'BMSLowTempSt'.
 */
using FieldNotifierConfigBMSLowTempSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasNotifier<true, FieldNotifierBMSLowTempSt>;

/*!
 * \brief Type alias for the getter configuration of field 'BMSLowTempSt'.
 */
using FieldGetterConfigBMSLowTempSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasGetter<true>;

/*!
 * \brief Type alias for the setter configuration of field 'BMSLowTempSt'.
 */
using FieldSetterConfigBMSLowTempSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasSetter<false>;

/*!
 * \brief Field name.
 */
struct BMSLowTempStName {
  /*!
   * \brief Field name string.
   */
  static constexpr vac::container::CStringView Name{"BMSLowTempSt"_sv};
};

/*!
 * \brief Type alias for the parameterization of field 'BMSLowTempSt'.
 */
using FieldConfigBMSLowTempSt = ::amsr::socal::internal::fields::SkeletonFieldConfig<gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton, FieldNotifierConfigBMSLowTempSt, FieldGetterConfigBMSLowTempSt, FieldSetterConfigBMSLowTempSt, BMSLowTempStName>;

/*!
 * \brief Type alias for service field 'BMSLowTempSt', that is part of the skeleton.
 *
 * \trace SPEC-4980343
 */
using BMSLowTempSt = ::amsr::socal::fields::SkeletonField<::gwm::pt_batterypacksys::batterypacksys_idt::BMSLowTempSt_Struct_Idt, FieldConfigBMSLowTempSt>;

/*!
 * \brief Type alias for the notification of field 'BMSRmtPreHeatSt'.
 */
using FieldNotifierBMSRmtPreHeatSt = ::amsr::socal::events::SkeletonEvent<
  gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton,
  ::gwm::pt_batterypacksys::batterypacksys_idt::BMSRmtPreHeatSt_Enum_Idt,
  gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface,
  ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::BMSRmtPreHeatSt_Enum_Idt>,
  &gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface::GetFieldNotifierBMSRmtPreHeatSt>;

/*!
 * \brief Type alias for the notifier configuration of field 'BMSRmtPreHeatSt'.
 */
using FieldNotifierConfigBMSRmtPreHeatSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasNotifier<true, FieldNotifierBMSRmtPreHeatSt>;

/*!
 * \brief Type alias for the getter configuration of field 'BMSRmtPreHeatSt'.
 */
using FieldGetterConfigBMSRmtPreHeatSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasGetter<true>;

/*!
 * \brief Type alias for the setter configuration of field 'BMSRmtPreHeatSt'.
 */
using FieldSetterConfigBMSRmtPreHeatSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasSetter<false>;

/*!
 * \brief Field name.
 */
struct BMSRmtPreHeatStName {
  /*!
   * \brief Field name string.
   */
  static constexpr vac::container::CStringView Name{"BMSRmtPreHeatSt"_sv};
};

/*!
 * \brief Type alias for the parameterization of field 'BMSRmtPreHeatSt'.
 */
using FieldConfigBMSRmtPreHeatSt = ::amsr::socal::internal::fields::SkeletonFieldConfig<gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton, FieldNotifierConfigBMSRmtPreHeatSt, FieldGetterConfigBMSRmtPreHeatSt, FieldSetterConfigBMSRmtPreHeatSt, BMSRmtPreHeatStName>;

/*!
 * \brief Type alias for service field 'BMSRmtPreHeatSt', that is part of the skeleton.
 *
 * \trace SPEC-4980343
 */
using BMSRmtPreHeatSt = ::amsr::socal::fields::SkeletonField<::gwm::pt_batterypacksys::batterypacksys_idt::BMSRmtPreHeatSt_Enum_Idt, FieldConfigBMSRmtPreHeatSt>;

/*!
 * \brief Type alias for the notification of field 'BMSSafCnctSt'.
 */
using FieldNotifierBMSSafCnctSt = ::amsr::socal::events::SkeletonEvent<
  gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton,
  ::gwm::pt_batterypacksys::batterypacksys_idt::BMSSafCnctSt_Enum_Idt,
  gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface,
  ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::BMSSafCnctSt_Enum_Idt>,
  &gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface::GetFieldNotifierBMSSafCnctSt>;

/*!
 * \brief Type alias for the notifier configuration of field 'BMSSafCnctSt'.
 */
using FieldNotifierConfigBMSSafCnctSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasNotifier<true, FieldNotifierBMSSafCnctSt>;

/*!
 * \brief Type alias for the getter configuration of field 'BMSSafCnctSt'.
 */
using FieldGetterConfigBMSSafCnctSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasGetter<true>;

/*!
 * \brief Type alias for the setter configuration of field 'BMSSafCnctSt'.
 */
using FieldSetterConfigBMSSafCnctSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasSetter<false>;

/*!
 * \brief Field name.
 */
struct BMSSafCnctStName {
  /*!
   * \brief Field name string.
   */
  static constexpr vac::container::CStringView Name{"BMSSafCnctSt"_sv};
};

/*!
 * \brief Type alias for the parameterization of field 'BMSSafCnctSt'.
 */
using FieldConfigBMSSafCnctSt = ::amsr::socal::internal::fields::SkeletonFieldConfig<gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton, FieldNotifierConfigBMSSafCnctSt, FieldGetterConfigBMSSafCnctSt, FieldSetterConfigBMSSafCnctSt, BMSSafCnctStName>;

/*!
 * \brief Type alias for service field 'BMSSafCnctSt', that is part of the skeleton.
 *
 * \trace SPEC-4980343
 */
using BMSSafCnctSt = ::amsr::socal::fields::SkeletonField<::gwm::pt_batterypacksys::batterypacksys_idt::BMSSafCnctSt_Enum_Idt, FieldConfigBMSSafCnctSt>;

/*!
 * \brief Type alias for the notification of field 'BmsErrSt'.
 */
using FieldNotifierBmsErrSt = ::amsr::socal::events::SkeletonEvent<
  gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton,
  ::gwm::pt_batterypacksys::batterypacksys_idt::BmsErrSt_Struct_Idt,
  gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface,
  ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::BmsErrSt_Struct_Idt>,
  &gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface::GetFieldNotifierBmsErrSt>;

/*!
 * \brief Type alias for the notifier configuration of field 'BmsErrSt'.
 */
using FieldNotifierConfigBmsErrSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasNotifier<true, FieldNotifierBmsErrSt>;

/*!
 * \brief Type alias for the getter configuration of field 'BmsErrSt'.
 */
using FieldGetterConfigBmsErrSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasGetter<true>;

/*!
 * \brief Type alias for the setter configuration of field 'BmsErrSt'.
 */
using FieldSetterConfigBmsErrSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasSetter<false>;

/*!
 * \brief Field name.
 */
struct BmsErrStName {
  /*!
   * \brief Field name string.
   */
  static constexpr vac::container::CStringView Name{"BmsErrSt"_sv};
};

/*!
 * \brief Type alias for the parameterization of field 'BmsErrSt'.
 */
using FieldConfigBmsErrSt = ::amsr::socal::internal::fields::SkeletonFieldConfig<gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton, FieldNotifierConfigBmsErrSt, FieldGetterConfigBmsErrSt, FieldSetterConfigBmsErrSt, BmsErrStName>;

/*!
 * \brief Type alias for service field 'BmsErrSt', that is part of the skeleton.
 *
 * \trace SPEC-4980343
 */
using BmsErrSt = ::amsr::socal::fields::SkeletonField<::gwm::pt_batterypacksys::batterypacksys_idt::BmsErrSt_Struct_Idt, FieldConfigBmsErrSt>;

/*!
 * \brief Type alias for the notification of field 'BmsHVILSt'.
 */
using FieldNotifierBmsHVILSt = ::amsr::socal::events::SkeletonEvent<
  gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton,
  ::gwm::pt_batterypacksys::batterypacksys_idt::BmsHVILSt_Enum_Idt,
  gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface,
  ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::BmsHVILSt_Enum_Idt>,
  &gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface::GetFieldNotifierBmsHVILSt>;

/*!
 * \brief Type alias for the notifier configuration of field 'BmsHVILSt'.
 */
using FieldNotifierConfigBmsHVILSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasNotifier<true, FieldNotifierBmsHVILSt>;

/*!
 * \brief Type alias for the getter configuration of field 'BmsHVILSt'.
 */
using FieldGetterConfigBmsHVILSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasGetter<true>;

/*!
 * \brief Type alias for the setter configuration of field 'BmsHVILSt'.
 */
using FieldSetterConfigBmsHVILSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasSetter<false>;

/*!
 * \brief Field name.
 */
struct BmsHVILStName {
  /*!
   * \brief Field name string.
   */
  static constexpr vac::container::CStringView Name{"BmsHVILSt"_sv};
};

/*!
 * \brief Type alias for the parameterization of field 'BmsHVILSt'.
 */
using FieldConfigBmsHVILSt = ::amsr::socal::internal::fields::SkeletonFieldConfig<gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton, FieldNotifierConfigBmsHVILSt, FieldGetterConfigBmsHVILSt, FieldSetterConfigBmsHVILSt, BmsHVILStName>;

/*!
 * \brief Type alias for service field 'BmsHVILSt', that is part of the skeleton.
 *
 * \trace SPEC-4980343
 */
using BmsHVILSt = ::amsr::socal::fields::SkeletonField<::gwm::pt_batterypacksys::batterypacksys_idt::BmsHVILSt_Enum_Idt, FieldConfigBmsHVILSt>;

/*!
 * \brief Type alias for the notification of field 'BmsIsoMeasSt'.
 */
using FieldNotifierBmsIsoMeasSt = ::amsr::socal::events::SkeletonEvent<
  gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton,
  ::gwm::pt_batterypacksys::batterypacksys_idt::BmsIsoMeasSt_Enum_Idt,
  gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface,
  ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::BmsIsoMeasSt_Enum_Idt>,
  &gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface::GetFieldNotifierBmsIsoMeasSt>;

/*!
 * \brief Type alias for the notifier configuration of field 'BmsIsoMeasSt'.
 */
using FieldNotifierConfigBmsIsoMeasSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasNotifier<true, FieldNotifierBmsIsoMeasSt>;

/*!
 * \brief Type alias for the getter configuration of field 'BmsIsoMeasSt'.
 */
using FieldGetterConfigBmsIsoMeasSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasGetter<true>;

/*!
 * \brief Type alias for the setter configuration of field 'BmsIsoMeasSt'.
 */
using FieldSetterConfigBmsIsoMeasSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasSetter<false>;

/*!
 * \brief Field name.
 */
struct BmsIsoMeasStName {
  /*!
   * \brief Field name string.
   */
  static constexpr vac::container::CStringView Name{"BmsIsoMeasSt"_sv};
};

/*!
 * \brief Type alias for the parameterization of field 'BmsIsoMeasSt'.
 */
using FieldConfigBmsIsoMeasSt = ::amsr::socal::internal::fields::SkeletonFieldConfig<gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton, FieldNotifierConfigBmsIsoMeasSt, FieldGetterConfigBmsIsoMeasSt, FieldSetterConfigBmsIsoMeasSt, BmsIsoMeasStName>;

/*!
 * \brief Type alias for service field 'BmsIsoMeasSt', that is part of the skeleton.
 *
 * \trace SPEC-4980343
 */
using BmsIsoMeasSt = ::amsr::socal::fields::SkeletonField<::gwm::pt_batterypacksys::batterypacksys_idt::BmsIsoMeasSt_Enum_Idt, FieldConfigBmsIsoMeasSt>;

/*!
 * \brief Type alias for the notification of field 'BmsSt'.
 */
using FieldNotifierBmsSt = ::amsr::socal::events::SkeletonEvent<
  gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton,
  ::gwm::pt_batterypacksys::batterypacksys_idt::BmsSt_Enum_Idt,
  gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface,
  ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::BmsSt_Enum_Idt>,
  &gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface::GetFieldNotifierBmsSt>;

/*!
 * \brief Type alias for the notifier configuration of field 'BmsSt'.
 */
using FieldNotifierConfigBmsSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasNotifier<true, FieldNotifierBmsSt>;

/*!
 * \brief Type alias for the getter configuration of field 'BmsSt'.
 */
using FieldGetterConfigBmsSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasGetter<true>;

/*!
 * \brief Type alias for the setter configuration of field 'BmsSt'.
 */
using FieldSetterConfigBmsSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasSetter<false>;

/*!
 * \brief Field name.
 */
struct BmsStName {
  /*!
   * \brief Field name string.
   */
  static constexpr vac::container::CStringView Name{"BmsSt"_sv};
};

/*!
 * \brief Type alias for the parameterization of field 'BmsSt'.
 */
using FieldConfigBmsSt = ::amsr::socal::internal::fields::SkeletonFieldConfig<gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton, FieldNotifierConfigBmsSt, FieldGetterConfigBmsSt, FieldSetterConfigBmsSt, BmsStName>;

/*!
 * \brief Type alias for service field 'BmsSt', that is part of the skeleton.
 *
 * \trace SPEC-4980343
 */
using BmsSt = ::amsr::socal::fields::SkeletonField<::gwm::pt_batterypacksys::batterypacksys_idt::BmsSt_Enum_Idt, FieldConfigBmsSt>;

/*!
 * \brief Type alias for the notification of field 'HVBattSOCLim'.
 */
using FieldNotifierHVBattSOCLim = ::amsr::socal::events::SkeletonEvent<
  gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton,
  ::gwm::pt_batterypacksys::batterypacksys_idt::HVBattSOCLim_Enum_Idt,
  gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface,
  ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::HVBattSOCLim_Enum_Idt>,
  &gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface::GetFieldNotifierHVBattSOCLim>;

/*!
 * \brief Type alias for the notifier configuration of field 'HVBattSOCLim'.
 */
using FieldNotifierConfigHVBattSOCLim = ::amsr::socal::internal::fields::SkeletonFieldParams::HasNotifier<true, FieldNotifierHVBattSOCLim>;

/*!
 * \brief Type alias for the getter configuration of field 'HVBattSOCLim'.
 */
using FieldGetterConfigHVBattSOCLim = ::amsr::socal::internal::fields::SkeletonFieldParams::HasGetter<true>;

/*!
 * \brief Type alias for the setter configuration of field 'HVBattSOCLim'.
 */
using FieldSetterConfigHVBattSOCLim = ::amsr::socal::internal::fields::SkeletonFieldParams::HasSetter<false>;

/*!
 * \brief Field name.
 */
struct HVBattSOCLimName {
  /*!
   * \brief Field name string.
   */
  static constexpr vac::container::CStringView Name{"HVBattSOCLim"_sv};
};

/*!
 * \brief Type alias for the parameterization of field 'HVBattSOCLim'.
 */
using FieldConfigHVBattSOCLim = ::amsr::socal::internal::fields::SkeletonFieldConfig<gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton, FieldNotifierConfigHVBattSOCLim, FieldGetterConfigHVBattSOCLim, FieldSetterConfigHVBattSOCLim, HVBattSOCLimName>;

/*!
 * \brief Type alias for service field 'HVBattSOCLim', that is part of the skeleton.
 *
 * \trace SPEC-4980343
 */
using HVBattSOCLim = ::amsr::socal::fields::SkeletonField<::gwm::pt_batterypacksys::batterypacksys_idt::HVBattSOCLim_Enum_Idt, FieldConfigHVBattSOCLim>;

/*!
 * \brief Type alias for the notification of field 'HVMaiSwtSt'.
 */
using FieldNotifierHVMaiSwtSt = ::amsr::socal::events::SkeletonEvent<
  gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton,
  ::gwm::pt_batterypacksys::batterypacksys_idt::HVMaiSwtSt_Struct_Idt,
  gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface,
  ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::HVMaiSwtSt_Struct_Idt>,
  &gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface::GetFieldNotifierHVMaiSwtSt>;

/*!
 * \brief Type alias for the notifier configuration of field 'HVMaiSwtSt'.
 */
using FieldNotifierConfigHVMaiSwtSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasNotifier<true, FieldNotifierHVMaiSwtSt>;

/*!
 * \brief Type alias for the getter configuration of field 'HVMaiSwtSt'.
 */
using FieldGetterConfigHVMaiSwtSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasGetter<true>;

/*!
 * \brief Type alias for the setter configuration of field 'HVMaiSwtSt'.
 */
using FieldSetterConfigHVMaiSwtSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasSetter<false>;

/*!
 * \brief Field name.
 */
struct HVMaiSwtStName {
  /*!
   * \brief Field name string.
   */
  static constexpr vac::container::CStringView Name{"HVMaiSwtSt"_sv};
};

/*!
 * \brief Type alias for the parameterization of field 'HVMaiSwtSt'.
 */
using FieldConfigHVMaiSwtSt = ::amsr::socal::internal::fields::SkeletonFieldConfig<gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton, FieldNotifierConfigHVMaiSwtSt, FieldGetterConfigHVMaiSwtSt, FieldSetterConfigHVMaiSwtSt, HVMaiSwtStName>;

/*!
 * \brief Type alias for service field 'HVMaiSwtSt', that is part of the skeleton.
 *
 * \trace SPEC-4980343
 */
using HVMaiSwtSt = ::amsr::socal::fields::SkeletonField<::gwm::pt_batterypacksys::batterypacksys_idt::HVMaiSwtSt_Struct_Idt, FieldConfigHVMaiSwtSt>;


}  // namespace fields

/*!
 * \brief Skeleton interface class for the service 'SI_BmsStSrv'.
 * \details The service interface provides HV battery real time current information
 *
 * \vpublic
 * \trace SPEC-4980341
 */
class SI_BmsStSrvSkeleton
    : public ::amsr::socal::Skeleton<gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrv,
                                          gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface> {
 public:
// ---- Constructors / Destructors -----------------------------------------------------------------------------------
/*!
 * \brief Exception-less pre-construction of SI_BmsStSrv.
 *
 * \param[in] instance The InstanceIdentifier of the service instance to be created.
 *                     Expected format: "<Binding type/prefix>:<binding specific instance ID>".
 *                     The InstanceIdentifier must fulfill the following preconditions:
 *                     - Must be configured in the ARXML model.
 *                     - Must belong to the service interface.
 * \param[in] mode The mode of the service implementation for processing service method invocations.
 *                 Default: Event-driven processing.
 *                 Preconditions to be fulfilled:
 *                 - If the mode 'kEventSingleThread' is used, a possible user-defined ThreadPool must have exactly one
 *                   worker thread configured.
 * \return Result<ConstructionToken> Result containing construction token from which a skeleton object can be
 *         constructed.
 *
 * \pre No other service skeleton for the same instance must exist concurrently. After destruction of an already
 *      created skeleton object, the instantiation for the same service instance will be possible.
 * \context App
 * \threadsafe FALSE
 * \reentrant FALSE
 * \vpublic
 * \synchronous TRUE
 * \trace SPEC-8053551
 * \trace SPEC-8053550
 */
static ConstructionResult Preconstruct(
    ara::com::InstanceIdentifier instance_id,
    ara::com::MethodCallProcessingMode const mode = ara::com::MethodCallProcessingMode::kEvent) noexcept;

/*!
 * \brief Exception-less pre-construction of SI_BmsStSrvSkeleton.
 *
 * \param[in] instance InstanceSpecifier of this service.
 *                     The provided InstanceSpecifier must fulfill the following preconditions:
 *                     - Must be configured in the ARXML model.
 *                     - Must belong to the service interface.
 * \param[in] mode The mode of the service implementation for processing service method invocations.
 *                 Default: Event-driven processing.
 *                 Preconditions to be fulfilled:
 *                 - If the mode 'kEventSingleThread' is used, a possible user-defined ThreadPool must have exactly one
 *                   worker thread configured.
 * \return Result<ConstructionToken> Result containing construction token from which a skeleton object can be
 *         constructed.
 *
 * \pre No other service skeleton for the same instance must exist concurrently. After destruction of an already
 *      created skeleton object, the instantation for the same service instance will be possible.
 * \context App
 * \threadsafe FALSE
 * \reentrant FALSE
 * \vpublic
 * \synchronous TRUE
 * \trace SPEC-8053553
 * \trace SPEC-8053550
 */
static ConstructionResult Preconstruct(
    ara::core::InstanceSpecifier instance,
    ara::com::MethodCallProcessingMode const mode = ara::com::MethodCallProcessingMode::kEvent) noexcept;

/*!
 * \brief Exception-less pre-construction of SI_BmsStSrvSkeleton.
 *
 * \param[in] instance_identifiers The container of instances of a service, each instance element needed to distinguish
 *                                 different instances of exactly the same service in the system.
 *                                 The provided InstanceIdentifierContainer must fulfill the following preconditions:
 *                                 - Every InstanceIdentifier of the container must be configured in the ARXML model.
 *                                 - Every InstanceIdentifier of the container must belong to the service interface to be
 *                                   instantiated.
 *                                 - The container must not be empty.
 *                                 - All elements of the container must be unique (no duplicates).
 * \param[in] mode The mode of the service implementation for processing service method invocations.
 *                 Default: Event-driven processing.
 *                 Preconditions to be fulfilled:
 *                 - If the mode 'kEventSingleThread' is used, a possible user-defined ThreadPool must have exactly one
 *                   worker thread configured.
 * \return Result<ConstructionToken> Result containing construction token from which a skeleton object can be
 *         constructed.
 *
 * \pre No other service skeleton for the same instance must exist concurrently. After destruction of an already
 *      created skeleton object, the instantation for the same service instance will be possible.
 * \context App
 * \threadsafe FALSE
 * \reentrant FALSE
 * \vpublic
 * \synchronous TRUE
 * \trace SPEC-8053555
 * \trace SPEC-8053550
 */
static ConstructionResult Preconstruct(
    ara::com::InstanceIdentifierContainer instance_identifiers,
    ara::com::MethodCallProcessingMode const mode = ara::com::MethodCallProcessingMode::kEvent) noexcept;

/*!
 * \brief Exception-less constructor of SI_BmsStSrvSkeleton.
 * \details Because of internal resource management strategy, all created skeletons shall be released before the
 *          Runtime is destroyed; i.e. they cannot not be stored in variables with longer life period than the
 *          application's main(). If not followed, it's not guaranteed that the communication middleware is shut down
 *          properly and may lead to segmentation fault.
 *
 * \param[in] token ConstructionToken created with Preconstruct() API.
 * \context App
 * \threadsafe FALSE
 * \reentrant FALSE
 * \vpublic
 * \synchronous TRUE
 * \trace SPEC-8053551
 * \trace SPEC-8053553
 * \trace SPEC-8053555
 */
explicit SI_BmsStSrvSkeleton(ConstructionToken&& token) noexcept;

  /*!
   * \brief Delete default constructor.
   */
  SI_BmsStSrvSkeleton() = delete;
  /*!
   * \brief Delete copy constructor.
   */
  SI_BmsStSrvSkeleton(SI_BmsStSrvSkeleton const &) = delete;
  /*!
   * \brief Delete move constructor.
   */
  SI_BmsStSrvSkeleton(SI_BmsStSrvSkeleton &&) = delete;
  /*!
   * \brief Delete copy assignment.
   */
  SI_BmsStSrvSkeleton &operator=(SI_BmsStSrvSkeleton const &) & = delete;
  /*!
   * \brief Delete move assignment.
   */
  SI_BmsStSrvSkeleton &operator=(SI_BmsStSrvSkeleton &&) & = delete;

  /*!
   * \brief Constructor of SI_BmsStSrvSkeleton.
   *
   * \remark Because of internal resource management strategy, all created skeletons shall be released before the Runtime
   * is destroyed; i.e. they cannot not be stored in variables with longer life period than the application's main().
   * If not followed, it's not guaranteed that the communication middleware is shut down properly and may lead to
   * segmentation fault.
   *
   * \param[in] instance The identifier of a specific instance of a service, needed to distinguish different instances of
   *                     exactly the same service in the system.
   *
   * \param[in] mode The mode of the service implpementation for processing service method invocations.
   *                 Default: Event-driven processing
   *
   * \pre No other service skeleton for the same instance must exist concurrently. After destruction of an already
   *      created skeleton object, the instantiation for the same service instance will be possible.
   *
   * \context App
   * \threadsafe FALSE
   * \reentrant FALSE
   * \vprivate Vector component internal API.
   * \synchronous TRUE
   * \trace SPEC-4980351
   * \trace SPEC-4980356
   */
   explicit SI_BmsStSrvSkeleton(
      ara::com::InstanceIdentifier instance,
      ara::com::MethodCallProcessingMode mode = ara::com::MethodCallProcessingMode::kEvent) noexcept;

  /*!
   * \brief Constructor of SI_BmsStSrvSkeleton.
   *
   * \remark Because of internal resource management strategy, all created skeletons shall be released before the Runtime
   * is destroyed; i.e. they cannot not be stored in variables with longer life period than the application's main().
   * If not followed, it's not guaranteed that the communication middleware is shut down properly and may lead to
   * segmentation fault.
   *
   * \param[in] instance The InstanceSpecifier of a specific service instance, needed to distinguish different instances
   *                     of exactly the same service in the system.
   *
   * \param[in] mode The mode of the service implementation for processing service method invocations.
   *                 Default: Event-driven processing
   *
   * \pre No other service skeleton for the same instance must exist concurrently. After destruction of an already
   *      created skeleton object, the instantiation for the same service instance will be possible.
   *
   * \context App
   * \threadsafe FALSE
   * \reentrant FALSE
   * \vprivate Vector component internal API.
   * \synchronous TRUE
   * \trace SPEC-8053552
   * \trace SPEC-4980356
   */
   explicit SI_BmsStSrvSkeleton(
      ara::core::InstanceSpecifier instance,
      ara::com::MethodCallProcessingMode mode = ara::com::MethodCallProcessingMode::kEvent) noexcept;

  /*!
   * \brief Constructor of SI_BmsStSrvSkeleton.
   *
   * \remark Because of internal resource management strategy, all created skeletons shall be released before the Runtime
   * is destroyed; i.e. they cannot not be stored in variables with longer life period than the application's main().
   * If not followed, it's not guaranteed that the communication middleware is shut down properly and may lead to
   * segmentation fault.
   *
   * \param[in] instance_identifiers The container of instances of a service, each instance element needed to
   *                                 distinguish different instances of exactly the same service in the system.
   *
   * \param[in] mode The mode of the service implementation for processing service method invocations.
   *                 Default: Event-driven processing
   *
   * \pre No other service skeleton for the same instance must exist concurrently. After destruction of an already
   *      created skeleton object, the instantiation for the same service instance will be possible.
   *
   * \context App
   * \threadsafe FALSE
   * \reentrant FALSE
   * \vprivate Vector component internal API.
   * \synchronous TRUE
   * \trace SPEC-8053554
   * \trace SPEC-4980356
   */
   explicit SI_BmsStSrvSkeleton(
      ara::com::InstanceIdentifierContainer instance_identifiers,
      ara::com::MethodCallProcessingMode mode = ara::com::MethodCallProcessingMode::kEvent) noexcept;


  /*!
   * \brief Stops the service if it is currently offered.
   * \details This call will be blocked until all current method requests are finished/canceled.
   * \pre -
   * \context App
   * \vpublic
   * \synchronous TRUE
   * \trace SPEC-4980351
   */
  ~SI_BmsStSrvSkeleton() noexcept override;

  /*!
   * \brief Type alias for ServiceDiscovery.
   */
  using ServiceDiscovery = ::amsr::socal::internal::service_discovery::ServiceDiscovery<SI_BmsStSrvSkeleton>;

  /*!
   * \brief       Returns the service discovery singleton.
   * \return      Reference to service discovery singleton.
   * \pre         Service discovery has been registered via RegisterServiceDiscovery.
   * \context     ANY
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \vprivate
   * \synchronous TRUE
   */
  static ::amsr::generic::Singleton<ServiceDiscovery*>& GetServiceDiscovery() noexcept;

  /*!
   * \brief       Registers the service discovery.
   * \param[in]   service_discovery Pointer to service discovery.
   * \pre         -
   * \context     Init
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \vprivate
   * \synchronous TRUE
   */
  static void RegisterServiceDiscovery(ServiceDiscovery* service_discovery) noexcept;

  /*!
   * \brief       Deregisters the service discovery.
   * \pre         RegisterServiceDiscovery has been called.
   * \context     Shutdown
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \vprivate
   * \synchronous TRUE
   */
  static void DeRegisterServiceDiscovery() noexcept;

  // ---- Methods --------------------------------------------------------------------------------------------------

  // ---- Events ---------------------------------------------------------------------------------------------------

  // VECTOR NC VectorC++-V11.0.2: MD_SOCAL_V11-0-2_public_member_in_generated_code
  /*!
   * \brief The skeleton event 'BMSChrgTi' which can be used by application developer.
   * \details 
   * Data of type ::gwm::pt_batterypacksys::batterypacksys_idt::BMSChrgTi_Integer_Idt
   * \vpublic
   */
  events::BMSChrgTi BMSChrgTi;

  // VECTOR NC VectorC++-V11.0.2: MD_SOCAL_V11-0-2_public_member_in_generated_code
  /*!
   * \brief The skeleton event 'BMSConDchrMaxPwr' which can be used by application developer.
   * \details 
   * Data of type ::gwm::pt_batterypacksys::batterypacksys_idt::BMSConDchrMaxPwr_Float_Idt
   * \vpublic
   */
  events::BMSConDchrMaxPwr BMSConDchrMaxPwr;

  // VECTOR NC VectorC++-V11.0.2: MD_SOCAL_V11-0-2_public_member_in_generated_code
  /*!
   * \brief The skeleton event 'BMSConFBMaxPwr' which can be used by application developer.
   * \details 
   * Data of type ::gwm::pt_batterypacksys::batterypacksys_idt::BMSConFBMaxPwr_Float_Idt
   * \vpublic
   */
  events::BMSConFBMaxPwr BMSConFBMaxPwr;

  // VECTOR NC VectorC++-V11.0.2: MD_SOCAL_V11-0-2_public_member_in_generated_code
  /*!
   * \brief The skeleton event 'BMSPlsDchrMaxPwr' which can be used by application developer.
   * \details 
   * Data of type ::gwm::pt_batterypacksys::batterypacksys_idt::BMSPlsDchrMaxPwr_Float_Idt
   * \vpublic
   */
  events::BMSPlsDchrMaxPwr BMSPlsDchrMaxPwr;

  // VECTOR NC VectorC++-V11.0.2: MD_SOCAL_V11-0-2_public_member_in_generated_code
  /*!
   * \brief The skeleton event 'BMSPlsFBMaxPwr' which can be used by application developer.
   * \details 
   * Data of type ::gwm::pt_batterypacksys::batterypacksys_idt::BMSPlsFBMaxPwr_Float_Idt
   * \vpublic
   */
  events::BMSPlsFBMaxPwr BMSPlsFBMaxPwr;

  // VECTOR NC VectorC++-V11.0.2: MD_SOCAL_V11-0-2_public_member_in_generated_code
  /*!
   * \brief The skeleton event 'BmsInletCooltT' which can be used by application developer.
   * \details 
   * Data of type ::gwm::pt_batterypacksys::batterypacksys_idt::BmsInletCooltT_Integer_Idt
   * \vpublic
   */
  events::BmsInletCooltT BmsInletCooltT;

  // VECTOR NC VectorC++-V11.0.2: MD_SOCAL_V11-0-2_public_member_in_generated_code
  /*!
   * \brief The skeleton event 'BmsIsoR' which can be used by application developer.
   * \details 
   * Data of type ::gwm::pt_batterypacksys::batterypacksys_idt::BmsIsoR_Integer_Idt
   * \vpublic
   */
  events::BmsIsoR BmsIsoR;

  // VECTOR NC VectorC++-V11.0.2: MD_SOCAL_V11-0-2_public_member_in_generated_code
  /*!
   * \brief The skeleton event 'BmsOutletCooltT' which can be used by application developer.
   * \details 
   * Data of type ::gwm::pt_batterypacksys::batterypacksys_idt::BmsOutletCooltT_Integer_Idt
   * \vpublic
   */
  events::BmsOutletCooltT BmsOutletCooltT;

  // VECTOR NC VectorC++-V11.0.2: MD_SOCAL_V11-0-2_public_member_in_generated_code
  /*!
   * \brief The skeleton event 'CellMaxT' which can be used by application developer.
   * \details 
   * Data of type ::gwm::pt_batterypacksys::batterypacksys_idt::CellMaxT_Struct_Idt
   * \vpublic
   */
  events::CellMaxT CellMaxT;

  // VECTOR NC VectorC++-V11.0.2: MD_SOCAL_V11-0-2_public_member_in_generated_code
  /*!
   * \brief The skeleton event 'CellMaxU' which can be used by application developer.
   * \details 
   * Data of type ::gwm::pt_batterypacksys::batterypacksys_idt::CellMaxU_Struct_Idt
   * \vpublic
   */
  events::CellMaxU CellMaxU;

  // VECTOR NC VectorC++-V11.0.2: MD_SOCAL_V11-0-2_public_member_in_generated_code
  /*!
   * \brief The skeleton event 'CellMinT' which can be used by application developer.
   * \details 
   * Data of type ::gwm::pt_batterypacksys::batterypacksys_idt::CellMinT_Struct_Idt
   * \vpublic
   */
  events::CellMinT CellMinT;

  // VECTOR NC VectorC++-V11.0.2: MD_SOCAL_V11-0-2_public_member_in_generated_code
  /*!
   * \brief The skeleton event 'CellMinU' which can be used by application developer.
   * \details 
   * Data of type ::gwm::pt_batterypacksys::batterypacksys_idt::CellMinU_Struct_Idt
   * \vpublic
   */
  events::CellMinU CellMinU;

  // VECTOR NC VectorC++-V11.0.2: MD_SOCAL_V11-0-2_public_member_in_generated_code
  /*!
   * \brief The skeleton event 'HVBattDispySoc' which can be used by application developer.
   * \details 
   * Data of type ::gwm::pt_batterypacksys::batterypacksys_idt::HVBattDispySoc_Float_Idt
   * \vpublic
   */
  events::HVBattDispySoc HVBattDispySoc;

  // VECTOR NC VectorC++-V11.0.2: MD_SOCAL_V11-0-2_public_member_in_generated_code
  /*!
   * \brief The skeleton event 'HVBattI' which can be used by application developer.
   * \details 
   * Data of type ::gwm::pt_batterypacksys::batterypacksys_idt::HVBattI_Float_Idt
   * \vpublic
   */
  events::HVBattI HVBattI;

  // VECTOR NC VectorC++-V11.0.2: MD_SOCAL_V11-0-2_public_member_in_generated_code
  /*!
   * \brief The skeleton event 'HVBattInnerSoc' which can be used by application developer.
   * \details 
   * Data of type ::gwm::pt_batterypacksys::batterypacksys_idt::HVBattInnerSoc_Float_Idt
   * \vpublic
   */
  events::HVBattInnerSoc HVBattInnerSoc;

  // VECTOR NC VectorC++-V11.0.2: MD_SOCAL_V11-0-2_public_member_in_generated_code
  /*!
   * \brief The skeleton event 'HVBattSoh' which can be used by application developer.
   * \details 
   * Data of type ::gwm::pt_batterypacksys::batterypacksys_idt::HVBattSoh_Float_Idt
   * \vpublic
   */
  events::HVBattSoh HVBattSoh;

  // VECTOR NC VectorC++-V11.0.2: MD_SOCAL_V11-0-2_public_member_in_generated_code
  /*!
   * \brief The skeleton event 'HVBattTotEgy' which can be used by application developer.
   * \details 
   * Data of type ::gwm::pt_batterypacksys::batterypacksys_idt::HVBattTotEgy_Float_Idt
   * \vpublic
   */
  events::HVBattTotEgy HVBattTotEgy;

  // VECTOR NC VectorC++-V11.0.2: MD_SOCAL_V11-0-2_public_member_in_generated_code
  /*!
   * \brief The skeleton event 'HVBattU' which can be used by application developer.
   * \details 
   * Data of type ::gwm::pt_batterypacksys::batterypacksys_idt::HVBattU_Float_Idt
   * \vpublic
   */
  events::HVBattU HVBattU;

  // VECTOR NC VectorC++-V11.0.2: MD_SOCAL_V11-0-2_public_member_in_generated_code
  /*!
   * \brief The skeleton event 'ModulAvrgT' which can be used by application developer.
   * \details 
   * Data of type ::gwm::pt_batterypacksys::batterypacksys_idt::ModulAvrgT_Integer_Idt
   * \vpublic
   */
  events::ModulAvrgT ModulAvrgT;

  // ---- Fields ---------------------------------------------------------------------------------------------------

  // VECTOR NC VectorC++-V11.0.2: MD_SOCAL_V11-0-2_public_member_in_generated_code
  /*!
   * \brief The skeleton field 'BMSChrgSt' which can be used by application developer.
   * \details 
   * Data of type ::gwm::pt_batterypacksys::batterypacksys_idt::BMSChrgSt_Enum_Idt
   * \vpublic
   */
  fields::BMSChrgSt BMSChrgSt;

  // VECTOR NC VectorC++-V11.0.2: MD_SOCAL_V11-0-2_public_member_in_generated_code
  /*!
   * \brief The skeleton field 'BMSDCChrgCnctSt' which can be used by application developer.
   * \details 
   * Data of type ::gwm::pt_batterypacksys::batterypacksys_idt::BMSDCChrgCnctSt_Enum_Idt
   * \vpublic
   */
  fields::BMSDCChrgCnctSt BMSDCChrgCnctSt;

  // VECTOR NC VectorC++-V11.0.2: MD_SOCAL_V11-0-2_public_member_in_generated_code
  /*!
   * \brief The skeleton field 'BMSDCChrgEndReas' which can be used by application developer.
   * \details 
   * Data of type ::gwm::pt_batterypacksys::batterypacksys_idt::BMSDCChrgEndReas_Enum_Idt
   * \vpublic
   */
  fields::BMSDCChrgEndReas BMSDCChrgEndReas;

  // VECTOR NC VectorC++-V11.0.2: MD_SOCAL_V11-0-2_public_member_in_generated_code
  /*!
   * \brief The skeleton field 'BMSDchrgPrmsSt' which can be used by application developer.
   * \details 
   * Data of type ::gwm::pt_batterypacksys::batterypacksys_idt::BMSDchrgPrmsSt_Enum_Idt
   * \vpublic
   */
  fields::BMSDchrgPrmsSt BMSDchrgPrmsSt;

  // VECTOR NC VectorC++-V11.0.2: MD_SOCAL_V11-0-2_public_member_in_generated_code
  /*!
   * \brief The skeleton field 'BMSHeatRunawaySt' which can be used by application developer.
   * \details 
   * Data of type ::gwm::pt_batterypacksys::batterypacksys_idt::BMSHeatRunawaySt_Enum_Idt
   * \vpublic
   */
  fields::BMSHeatRunawaySt BMSHeatRunawaySt;

  // VECTOR NC VectorC++-V11.0.2: MD_SOCAL_V11-0-2_public_member_in_generated_code
  /*!
   * \brief The skeleton field 'BMSInnerSOCSt' which can be used by application developer.
   * \details 
   * Data of type ::gwm::pt_batterypacksys::batterypacksys_idt::BMSInnerSOCSt_Struct_Idt
   * \vpublic
   */
  fields::BMSInnerSOCSt BMSInnerSOCSt;

  // VECTOR NC VectorC++-V11.0.2: MD_SOCAL_V11-0-2_public_member_in_generated_code
  /*!
   * \brief The skeleton field 'BMSIntelTempMgtSt' which can be used by application developer.
   * \details 
   * Data of type ::gwm::pt_batterypacksys::batterypacksys_idt::BMSIntelTempMgtSt_Enum_Idt
   * \vpublic
   */
  fields::BMSIntelTempMgtSt BMSIntelTempMgtSt;

  // VECTOR NC VectorC++-V11.0.2: MD_SOCAL_V11-0-2_public_member_in_generated_code
  /*!
   * \brief The skeleton field 'BMSLowTempSt' which can be used by application developer.
   * \details 
   * Data of type ::gwm::pt_batterypacksys::batterypacksys_idt::BMSLowTempSt_Struct_Idt
   * \vpublic
   */
  fields::BMSLowTempSt BMSLowTempSt;

  // VECTOR NC VectorC++-V11.0.2: MD_SOCAL_V11-0-2_public_member_in_generated_code
  /*!
   * \brief The skeleton field 'BMSRmtPreHeatSt' which can be used by application developer.
   * \details 
   * Data of type ::gwm::pt_batterypacksys::batterypacksys_idt::BMSRmtPreHeatSt_Enum_Idt
   * \vpublic
   */
  fields::BMSRmtPreHeatSt BMSRmtPreHeatSt;

  // VECTOR NC VectorC++-V11.0.2: MD_SOCAL_V11-0-2_public_member_in_generated_code
  /*!
   * \brief The skeleton field 'BMSSafCnctSt' which can be used by application developer.
   * \details 
   * Data of type ::gwm::pt_batterypacksys::batterypacksys_idt::BMSSafCnctSt_Enum_Idt
   * \vpublic
   */
  fields::BMSSafCnctSt BMSSafCnctSt;

  // VECTOR NC VectorC++-V11.0.2: MD_SOCAL_V11-0-2_public_member_in_generated_code
  /*!
   * \brief The skeleton field 'BmsErrSt' which can be used by application developer.
   * \details 
   * Data of type ::gwm::pt_batterypacksys::batterypacksys_idt::BmsErrSt_Struct_Idt
   * \vpublic
   */
  fields::BmsErrSt BmsErrSt;

  // VECTOR NC VectorC++-V11.0.2: MD_SOCAL_V11-0-2_public_member_in_generated_code
  /*!
   * \brief The skeleton field 'BmsHVILSt' which can be used by application developer.
   * \details 
   * Data of type ::gwm::pt_batterypacksys::batterypacksys_idt::BmsHVILSt_Enum_Idt
   * \vpublic
   */
  fields::BmsHVILSt BmsHVILSt;

  // VECTOR NC VectorC++-V11.0.2: MD_SOCAL_V11-0-2_public_member_in_generated_code
  /*!
   * \brief The skeleton field 'BmsIsoMeasSt' which can be used by application developer.
   * \details 
   * Data of type ::gwm::pt_batterypacksys::batterypacksys_idt::BmsIsoMeasSt_Enum_Idt
   * \vpublic
   */
  fields::BmsIsoMeasSt BmsIsoMeasSt;

  // VECTOR NC VectorC++-V11.0.2: MD_SOCAL_V11-0-2_public_member_in_generated_code
  /*!
   * \brief The skeleton field 'BmsSt' which can be used by application developer.
   * \details 
   * Data of type ::gwm::pt_batterypacksys::batterypacksys_idt::BmsSt_Enum_Idt
   * \vpublic
   */
  fields::BmsSt BmsSt;

  // VECTOR NC VectorC++-V11.0.2: MD_SOCAL_V11-0-2_public_member_in_generated_code
  /*!
   * \brief The skeleton field 'HVBattSOCLim' which can be used by application developer.
   * \details 
   * Data of type ::gwm::pt_batterypacksys::batterypacksys_idt::HVBattSOCLim_Enum_Idt
   * \vpublic
   */
  fields::HVBattSOCLim HVBattSOCLim;

  // VECTOR NC VectorC++-V11.0.2: MD_SOCAL_V11-0-2_public_member_in_generated_code
  /*!
   * \brief The skeleton field 'HVMaiSwtSt' which can be used by application developer.
   * \details 
   * Data of type ::gwm::pt_batterypacksys::batterypacksys_idt::HVMaiSwtSt_Struct_Idt
   * \vpublic
   */
  fields::HVMaiSwtSt HVMaiSwtSt;

 private:
  /*!
   * \brief Type alias for the base class.
   */
  using Base = ::amsr::socal::Skeleton<gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrv, gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface>;

  /*!
   * \copydoc amsr::socal::Skeleton<gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrv,gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface>::DoFieldInitializationChecks()
   */
  void DoFieldInitializationChecks() noexcept override;

  /*!
   * \copydoc amsr::socal::Skeleton<gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrv,gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface>::SendInitialFieldNotifications()
   */
  void SendInitialFieldNotifications() noexcept override;

  /*!
   * \copydoc amsr::socal::Skeleton<gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrv,gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface>::OfferServiceInternal()
   */
  void OfferServiceInternal(::ara::com::InstanceIdentifier const& instance_id) noexcept override;

  /*!
   * \copydoc amsr::socal::Skeleton<gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrv,gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface>::StopOfferServiceInternal()
   */
  void StopOfferServiceInternal(::ara::com::InstanceIdentifier const& instance_id) noexcept override;

  /*!
   * \brief The service discovery instance.
   */
  // VECTOR NC AutosarC++17_10-A3.3.2: MD_SOCAL_AutosarC++17_10-A3.3.2_StaticStorageDurationOfNonPODType
  static ::amsr::generic::Singleton<ServiceDiscovery*> sd_;
};  // class SI_BmsStSrvSkeleton

}  // namespace skeleton
}  // namespace bmsstsrv_si
}  // namespace pt_batterypacksys
}  // namespace gwm

#endif  // DENPENDENCYSRVEXE_INCLUDE_GWM_PT_BATTERYPACKSYS_BMSSTSRV_SI_SI_BMSSTSRV_SKELETON_H_

