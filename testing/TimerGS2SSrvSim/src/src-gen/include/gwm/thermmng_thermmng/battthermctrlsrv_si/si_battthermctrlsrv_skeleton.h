/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/include/gwm/thermmng_thermmng/battthermctrlsrv_si/si_battthermctrlsrv_skeleton.h
 *        \brief  Skeleton for service 'SI_BattThermCtrlSrv'.
 *
 *      \details  The service interface provides vehicle thermal control strategy level control
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_socal
 *         Commit ID: 209b3c0234013e84e3170ee826c9a8c6022de194
 *********************************************************************************************************************/

#ifndef DENPENDENCYSRVEXE_INCLUDE_GWM_THERMMNG_THERMMNG_BATTTHERMCTRLSRV_SI_SI_BATTTHERMCTRLSRV_SKELETON_H_
#define DENPENDENCYSRVEXE_INCLUDE_GWM_THERMMNG_THERMMNG_BATTTHERMCTRLSRV_SI_SI_BATTTHERMCTRLSRV_SKELETON_H_

/*!
 * \trace SPEC-4980239
 */
/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "amsr/generic/singleton_wrapper.h"
#include "amsr/socal/events/skeleton_event.h"
#include "amsr/socal/fields/skeleton_field.h"
#include "amsr/socal/internal/events/skeleton_event_manager_interface.h"
#include "amsr/socal/internal/service_discovery/service_discovery.h"
#include "amsr/socal/skeleton.h"
#include "ara/core/future.h"
#include "ara/core/instance_specifier.h"
#include "gwm/thermmng_thermmng/battthermctrlsrv_si/SI_BattThermCtrlSrv_skeleton_impl_interface.h"
#include "gwm/thermmng_thermmng/battthermctrlsrv_si/SI_BattThermCtrlSrv_types.h"

/*!
 * \trace SPEC-4980240
 */
namespace gwm {
namespace thermmng_thermmng {
namespace battthermctrlsrv_si {
/*!
 * \trace SPEC-4980241
 */
namespace skeleton {
/*!
 * \brief Forward declaration for inserting as a type into the template class SkeletonEvent
 */
class SI_BattThermCtrlSrvSkeleton;

/*!
 * \trace SPEC-4980244
 */
namespace methods {

/*!
 * \brief Data class for service method 'BattPackHeatCtrl'.
 */
using BattPackHeatCtrl = gwm::thermmng_thermmng::battthermctrlsrv_si::internal::methods::BattPackHeatCtrl;

/*!
 * \brief Data class for service method 'BattThermCtrl'.
 */
using BattThermCtrl = gwm::thermmng_thermmng::battthermctrlsrv_si::internal::methods::BattThermCtrl;

}  // namespace methods

/*!
 * \trace SPEC-4980243
 */
namespace events {

}  // namespace events

/*!
 * \trace SPEC-4980245
 */
namespace fields {

}  // namespace fields

/*!
 * \brief Skeleton interface class for the service 'SI_BattThermCtrlSrv'.
 * \details The service interface provides vehicle thermal control strategy level control
 *
 * \vpublic
 * \trace SPEC-4980341
 */
class SI_BattThermCtrlSrvSkeleton
    : public ::amsr::socal::Skeleton<gwm::thermmng_thermmng::battthermctrlsrv_si::SI_BattThermCtrlSrv,
                                          gwm::thermmng_thermmng::battthermctrlsrv_si::internal::SI_BattThermCtrlSrvSkeletonImplInterface> {
 public:
// ---- Constructors / Destructors -----------------------------------------------------------------------------------
/*!
 * \brief Exception-less pre-construction of SI_BattThermCtrlSrv.
 *
 * \param[in] instance The InstanceIdentifier of the service instance to be created.
 *                     Expected format: "<Binding type/prefix>:<binding specific instance ID>".
 *                     The InstanceIdentifier must fulfill the following preconditions:
 *                     - Must be configured in the ARXML model.
 *                     - Must belong to the service interface.
 * \param[in] mode The mode of the service implementation for processing service method invocations.
 *                 Default: Event-driven processing.
 *                 Preconditions to be fulfilled:
 *                 - If the mode 'kEventSingleThread' is used, a possible user-defined ThreadPool must have exactly one
 *                   worker thread configured.
 * \return Result<ConstructionToken> Result containing construction token from which a skeleton object can be
 *         constructed.
 *
 * \pre No other service skeleton for the same instance must exist concurrently. After destruction of an already
 *      created skeleton object, the instantiation for the same service instance will be possible.
 * \context App
 * \threadsafe FALSE
 * \reentrant FALSE
 * \vpublic
 * \synchronous TRUE
 * \trace SPEC-8053551
 * \trace SPEC-8053550
 */
static ConstructionResult Preconstruct(
    ara::com::InstanceIdentifier instance_id,
    ara::com::MethodCallProcessingMode const mode = ara::com::MethodCallProcessingMode::kEvent) noexcept;

/*!
 * \brief Exception-less pre-construction of SI_BattThermCtrlSrvSkeleton.
 *
 * \param[in] instance InstanceSpecifier of this service.
 *                     The provided InstanceSpecifier must fulfill the following preconditions:
 *                     - Must be configured in the ARXML model.
 *                     - Must belong to the service interface.
 * \param[in] mode The mode of the service implementation for processing service method invocations.
 *                 Default: Event-driven processing.
 *                 Preconditions to be fulfilled:
 *                 - If the mode 'kEventSingleThread' is used, a possible user-defined ThreadPool must have exactly one
 *                   worker thread configured.
 * \return Result<ConstructionToken> Result containing construction token from which a skeleton object can be
 *         constructed.
 *
 * \pre No other service skeleton for the same instance must exist concurrently. After destruction of an already
 *      created skeleton object, the instantation for the same service instance will be possible.
 * \context App
 * \threadsafe FALSE
 * \reentrant FALSE
 * \vpublic
 * \synchronous TRUE
 * \trace SPEC-8053553
 * \trace SPEC-8053550
 */
static ConstructionResult Preconstruct(
    ara::core::InstanceSpecifier instance,
    ara::com::MethodCallProcessingMode const mode = ara::com::MethodCallProcessingMode::kEvent) noexcept;

/*!
 * \brief Exception-less pre-construction of SI_BattThermCtrlSrvSkeleton.
 *
 * \param[in] instance_identifiers The container of instances of a service, each instance element needed to distinguish
 *                                 different instances of exactly the same service in the system.
 *                                 The provided InstanceIdentifierContainer must fulfill the following preconditions:
 *                                 - Every InstanceIdentifier of the container must be configured in the ARXML model.
 *                                 - Every InstanceIdentifier of the container must belong to the service interface to be
 *                                   instantiated.
 *                                 - The container must not be empty.
 *                                 - All elements of the container must be unique (no duplicates).
 * \param[in] mode The mode of the service implementation for processing service method invocations.
 *                 Default: Event-driven processing.
 *                 Preconditions to be fulfilled:
 *                 - If the mode 'kEventSingleThread' is used, a possible user-defined ThreadPool must have exactly one
 *                   worker thread configured.
 * \return Result<ConstructionToken> Result containing construction token from which a skeleton object can be
 *         constructed.
 *
 * \pre No other service skeleton for the same instance must exist concurrently. After destruction of an already
 *      created skeleton object, the instantation for the same service instance will be possible.
 * \context App
 * \threadsafe FALSE
 * \reentrant FALSE
 * \vpublic
 * \synchronous TRUE
 * \trace SPEC-8053555
 * \trace SPEC-8053550
 */
static ConstructionResult Preconstruct(
    ara::com::InstanceIdentifierContainer instance_identifiers,
    ara::com::MethodCallProcessingMode const mode = ara::com::MethodCallProcessingMode::kEvent) noexcept;

/*!
 * \brief Exception-less constructor of SI_BattThermCtrlSrvSkeleton.
 * \details Because of internal resource management strategy, all created skeletons shall be released before the
 *          Runtime is destroyed; i.e. they cannot not be stored in variables with longer life period than the
 *          application's main(). If not followed, it's not guaranteed that the communication middleware is shut down
 *          properly and may lead to segmentation fault.
 *
 * \param[in] token ConstructionToken created with Preconstruct() API.
 * \context App
 * \threadsafe FALSE
 * \reentrant FALSE
 * \vpublic
 * \synchronous TRUE
 * \trace SPEC-8053551
 * \trace SPEC-8053553
 * \trace SPEC-8053555
 */
explicit SI_BattThermCtrlSrvSkeleton(ConstructionToken&& token) noexcept;

  /*!
   * \brief Delete default constructor.
   */
  SI_BattThermCtrlSrvSkeleton() = delete;
  /*!
   * \brief Delete copy constructor.
   */
  SI_BattThermCtrlSrvSkeleton(SI_BattThermCtrlSrvSkeleton const &) = delete;
  /*!
   * \brief Delete move constructor.
   */
  SI_BattThermCtrlSrvSkeleton(SI_BattThermCtrlSrvSkeleton &&) = delete;
  /*!
   * \brief Delete copy assignment.
   */
  SI_BattThermCtrlSrvSkeleton &operator=(SI_BattThermCtrlSrvSkeleton const &) & = delete;
  /*!
   * \brief Delete move assignment.
   */
  SI_BattThermCtrlSrvSkeleton &operator=(SI_BattThermCtrlSrvSkeleton &&) & = delete;

  /*!
   * \brief Constructor of SI_BattThermCtrlSrvSkeleton.
   *
   * \remark Because of internal resource management strategy, all created skeletons shall be released before the Runtime
   * is destroyed; i.e. they cannot not be stored in variables with longer life period than the application's main().
   * If not followed, it's not guaranteed that the communication middleware is shut down properly and may lead to
   * segmentation fault.
   *
   * \param[in] instance The identifier of a specific instance of a service, needed to distinguish different instances of
   *                     exactly the same service in the system.
   *
   * \param[in] mode The mode of the service implpementation for processing service method invocations.
   *                 Default: Event-driven processing
   *
   * \pre No other service skeleton for the same instance must exist concurrently. After destruction of an already
   *      created skeleton object, the instantiation for the same service instance will be possible.
   *
   * \context App
   * \threadsafe FALSE
   * \reentrant FALSE
   * \vprivate Vector component internal API.
   * \synchronous TRUE
   * \trace SPEC-4980351
   * \trace SPEC-4980356
   */
   explicit SI_BattThermCtrlSrvSkeleton(
      ara::com::InstanceIdentifier instance,
      ara::com::MethodCallProcessingMode mode = ara::com::MethodCallProcessingMode::kEvent) noexcept;

  /*!
   * \brief Constructor of SI_BattThermCtrlSrvSkeleton.
   *
   * \remark Because of internal resource management strategy, all created skeletons shall be released before the Runtime
   * is destroyed; i.e. they cannot not be stored in variables with longer life period than the application's main().
   * If not followed, it's not guaranteed that the communication middleware is shut down properly and may lead to
   * segmentation fault.
   *
   * \param[in] instance The InstanceSpecifier of a specific service instance, needed to distinguish different instances
   *                     of exactly the same service in the system.
   *
   * \param[in] mode The mode of the service implementation for processing service method invocations.
   *                 Default: Event-driven processing
   *
   * \pre No other service skeleton for the same instance must exist concurrently. After destruction of an already
   *      created skeleton object, the instantiation for the same service instance will be possible.
   *
   * \context App
   * \threadsafe FALSE
   * \reentrant FALSE
   * \vprivate Vector component internal API.
   * \synchronous TRUE
   * \trace SPEC-8053552
   * \trace SPEC-4980356
   */
   explicit SI_BattThermCtrlSrvSkeleton(
      ara::core::InstanceSpecifier instance,
      ara::com::MethodCallProcessingMode mode = ara::com::MethodCallProcessingMode::kEvent) noexcept;

  /*!
   * \brief Constructor of SI_BattThermCtrlSrvSkeleton.
   *
   * \remark Because of internal resource management strategy, all created skeletons shall be released before the Runtime
   * is destroyed; i.e. they cannot not be stored in variables with longer life period than the application's main().
   * If not followed, it's not guaranteed that the communication middleware is shut down properly and may lead to
   * segmentation fault.
   *
   * \param[in] instance_identifiers The container of instances of a service, each instance element needed to
   *                                 distinguish different instances of exactly the same service in the system.
   *
   * \param[in] mode The mode of the service implementation for processing service method invocations.
   *                 Default: Event-driven processing
   *
   * \pre No other service skeleton for the same instance must exist concurrently. After destruction of an already
   *      created skeleton object, the instantiation for the same service instance will be possible.
   *
   * \context App
   * \threadsafe FALSE
   * \reentrant FALSE
   * \vprivate Vector component internal API.
   * \synchronous TRUE
   * \trace SPEC-8053554
   * \trace SPEC-4980356
   */
   explicit SI_BattThermCtrlSrvSkeleton(
      ara::com::InstanceIdentifierContainer instance_identifiers,
      ara::com::MethodCallProcessingMode mode = ara::com::MethodCallProcessingMode::kEvent) noexcept;


  /*!
   * \brief Stops the service if it is currently offered.
   * \details This call will be blocked until all current method requests are finished/canceled.
   * \pre -
   * \context App
   * \vpublic
   * \synchronous TRUE
   * \trace SPEC-4980351
   */
  ~SI_BattThermCtrlSrvSkeleton() noexcept override;

  /*!
   * \brief Type alias for ServiceDiscovery.
   */
  using ServiceDiscovery = ::amsr::socal::internal::service_discovery::ServiceDiscovery<SI_BattThermCtrlSrvSkeleton>;

  /*!
   * \brief       Returns the service discovery singleton.
   * \return      Reference to service discovery singleton.
   * \pre         Service discovery has been registered via RegisterServiceDiscovery.
   * \context     ANY
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \vprivate
   * \synchronous TRUE
   */
  static ::amsr::generic::Singleton<ServiceDiscovery*>& GetServiceDiscovery() noexcept;

  /*!
   * \brief       Registers the service discovery.
   * \param[in]   service_discovery Pointer to service discovery.
   * \pre         -
   * \context     Init
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \vprivate
   * \synchronous TRUE
   */
  static void RegisterServiceDiscovery(ServiceDiscovery* service_discovery) noexcept;

  /*!
   * \brief       Deregisters the service discovery.
   * \pre         RegisterServiceDiscovery has been called.
   * \context     Shutdown
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \vprivate
   * \synchronous TRUE
   */
  static void DeRegisterServiceDiscovery() noexcept;

  // ---- Methods --------------------------------------------------------------------------------------------------

  /*!
   * \brief Provided implementation for service interface method 'BattPackHeatCtrl'.
   *
   * \param[in] ClntId IN parameter of type ::gwm::thermmng_thermmng::thermmng_idt::BattPackHeatCtrl_ClntId_Enum_Idt
   * \param[in] Command IN parameter of type ::gwm::thermmng_thermmng::thermmng_idt::BattPackHeatCtrl_Command_Enum_Idt
   * \return ara::core::Future which gets resolved with the method output arguments or an ApApplicationError error code
   *         after processing has finished.
   *         Output arguments:
   *         - Response OUT parameter of type ::gwm::thermmng_thermmng::thermmng_idt::BattPackHeatCtrl_Response_Enum_Idt
   *
   * \pre        -
   * \context    Callback
   * \threadsafe TRUE if MethodCallProcessing mode is kEvent, FALSE otherwise.
   * \vpublic
   * \reentrant  TRUE if MethodCallProcessing mode is kEvent, FALSE otherwise.
   * \synchronous FALSE
   * \trace SPEC-4980355
   */
  virtual ara::core::Future<methods::BattPackHeatCtrl::Output> BattPackHeatCtrl(::gwm::thermmng_thermmng::thermmng_idt::BattPackHeatCtrl_ClntId_Enum_Idt const& ClntId, ::gwm::thermmng_thermmng::thermmng_idt::BattPackHeatCtrl_Command_Enum_Idt const& Command) = 0;

  /*!
   * \brief Provided implementation for service interface method 'BattThermCtrl'.
   *
   * \param[in] ClntId IN parameter of type ::gwm::thermmng_thermmng::thermmng_idt::BattThermCtrl_ClntId_Enum_Idt
   * \param[in] Command Thermal control  strategy temperature
   * \return ara::core::Future which gets resolved with the method output arguments or an ApApplicationError error code
   *         after processing has finished.
   *         Output arguments:
   *         - Response OUT parameter of type ::gwm::thermmng_thermmng::thermmng_idt::BattThermCtrl_Response_Enum_Idt
   *
   * \pre        -
   * \context    Callback
   * \threadsafe TRUE if MethodCallProcessing mode is kEvent, FALSE otherwise.
   * \vpublic
   * \reentrant  TRUE if MethodCallProcessing mode is kEvent, FALSE otherwise.
   * \synchronous FALSE
   * \trace SPEC-4980355
   */
  virtual ara::core::Future<methods::BattThermCtrl::Output> BattThermCtrl(::gwm::thermmng_thermmng::thermmng_idt::BattThermCtrl_ClntId_Enum_Idt const& ClntId, ::gwm::thermmng_thermmng::thermmng_idt::BattThermCtrl_Command_Integer_Idt const& Command) = 0;

  // ---- Events ---------------------------------------------------------------------------------------------------

  // ---- Fields ---------------------------------------------------------------------------------------------------

 private:
  /*!
   * \brief Type alias for the base class.
   */
  using Base = ::amsr::socal::Skeleton<gwm::thermmng_thermmng::battthermctrlsrv_si::SI_BattThermCtrlSrv, gwm::thermmng_thermmng::battthermctrlsrv_si::internal::SI_BattThermCtrlSrvSkeletonImplInterface>;

  /*!
   * \copydoc amsr::socal::Skeleton<gwm::thermmng_thermmng::battthermctrlsrv_si::SI_BattThermCtrlSrv,gwm::thermmng_thermmng::battthermctrlsrv_si::internal::SI_BattThermCtrlSrvSkeletonImplInterface>::DoFieldInitializationChecks()
   */
  void DoFieldInitializationChecks() noexcept override;

  /*!
   * \copydoc amsr::socal::Skeleton<gwm::thermmng_thermmng::battthermctrlsrv_si::SI_BattThermCtrlSrv,gwm::thermmng_thermmng::battthermctrlsrv_si::internal::SI_BattThermCtrlSrvSkeletonImplInterface>::SendInitialFieldNotifications()
   */
  void SendInitialFieldNotifications() noexcept override;

  /*!
   * \copydoc amsr::socal::Skeleton<gwm::thermmng_thermmng::battthermctrlsrv_si::SI_BattThermCtrlSrv,gwm::thermmng_thermmng::battthermctrlsrv_si::internal::SI_BattThermCtrlSrvSkeletonImplInterface>::OfferServiceInternal()
   */
  void OfferServiceInternal(::ara::com::InstanceIdentifier const& instance_id) noexcept override;

  /*!
   * \copydoc amsr::socal::Skeleton<gwm::thermmng_thermmng::battthermctrlsrv_si::SI_BattThermCtrlSrv,gwm::thermmng_thermmng::battthermctrlsrv_si::internal::SI_BattThermCtrlSrvSkeletonImplInterface>::StopOfferServiceInternal()
   */
  void StopOfferServiceInternal(::ara::com::InstanceIdentifier const& instance_id) noexcept override;

  /*!
   * \brief The service discovery instance.
   */
  // VECTOR NC AutosarC++17_10-A3.3.2: MD_SOCAL_AutosarC++17_10-A3.3.2_StaticStorageDurationOfNonPODType
  static ::amsr::generic::Singleton<ServiceDiscovery*> sd_;
};  // class SI_BattThermCtrlSrvSkeleton

}  // namespace skeleton
}  // namespace battthermctrlsrv_si
}  // namespace thermmng_thermmng
}  // namespace gwm

#endif  // DENPENDENCYSRVEXE_INCLUDE_GWM_THERMMNG_THERMMNG_BATTTHERMCTRLSRV_SI_SI_BATTTHERMCTRLSRV_SKELETON_H_

