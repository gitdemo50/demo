#include <iostream>
#include <chrono>
#include <thread>
#include <map>

//#include "actionmenu.h"

#include "ara/core/future.h"
#include "ara/core/initialization.h"
#include "ara/log/logging.h"


#include "ara/exec/application_client.h"


#include "TimerBasicManager.hpp"
#include "version.h"

using vac::container::literals::operator""_sv;

int main(int, char**) {

    ara::core::Result<void> init_result{ara::core::Initialize()};

    if (!init_result.HasValue()) {
        ara::core::Abort("ara::core::Initialize() failed Aborting");
    }

    std::string srv_version{SERVICE_VERSION};

    ara::log::Logger& logger_ctx{ara::log::CreateLogger("main"_sv, "Context for main function!"_sv)};
    logger_ctx.LogInfo() << "Reporting application version: " << srv_version;


    ara::exec::ApplicationClient appClient;

    gwm::manager::TimerBasicManager::getInstance()->start();


    appClient.ReportApplicationState(ara::exec::ApplicationState::kRunning);


    while (true) {
        std::this_thread::sleep_for(std::chrono::seconds(2));
    }


    appClient.ReportApplicationState(ara::exec::ApplicationState::kTerminating);

}
