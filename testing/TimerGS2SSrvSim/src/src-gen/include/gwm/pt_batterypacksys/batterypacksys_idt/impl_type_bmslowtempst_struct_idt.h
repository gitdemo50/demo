/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/include/gwm/pt_batterypacksys/batterypacksys_idt/impl_type_bmslowtempst_struct_idt.h
 *        \brief
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_modelleddatatypes_api
 *         Commit ID: d9332226824a4a6c4616e2d948d41c862d192343
 *********************************************************************************************************************/

#ifndef DENPENDENCYSRVEXE_INCLUDE_GWM_PT_BATTERYPACKSYS_BATTERYPACKSYS_IDT_IMPL_TYPE_BMSLOWTEMPST_STRUCT_IDT_H_
#define DENPENDENCYSRVEXE_INCLUDE_GWM_PT_BATTERYPACKSYS_BATTERYPACKSYS_IDT_IMPL_TYPE_BMSLOWTEMPST_STRUCT_IDT_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include <array>
#include <cstdint>
#include <map>
#include <string>
#include <vector>
#include "gwm/pt_batterypacksys/batterypacksys_idt/impl_type_bmslowtempst_heatst_enum_idt.h"
#include "gwm/pt_batterypacksys/batterypacksys_idt/impl_type_bmslowtempst_warnst_enum_idt.h"

namespace gwm {
namespace pt_batterypacksys {
namespace batterypacksys_idt {

// VECTOR Disable AutosarC++17_10-A12.6.1: MD_MDTG_A12.6.1_GeneratedStructUninitializedMembers
// VECTOR Disable AutosarC++17_10-M8.5.1: MD_MDTG_M8.5.1_GeneratedStructUninitializedMembers
/*!
 * \brief Type BMSLowTempSt_Struct_Idt.
 * \remark generated
 * \trace SPEC-5951372
 *
 * IMPLEMENTATION-DATA-TYPE /DataTypes/ImplementationDataTypes/BMSLowTempSt_Struct_Idt
 */
struct BMSLowTempSt_Struct_Idt {
  using HeatSt_Idt_generated_type = gwm::pt_batterypacksys::batterypacksys_idt::BMSLowTempSt_HeatSt_Enum_Idt;
  using WarnSt_Idt_generated_type = gwm::pt_batterypacksys::batterypacksys_idt::BMSLowTempSt_WarnSt_Enum_Idt;

  HeatSt_Idt_generated_type HeatSt_Idt;
  WarnSt_Idt_generated_type WarnSt_Idt;
};
// VECTOR Enable AutosarC++17_10-A12.6.1
// VECTOR Enable AutosarC++17_10-M8.5.1

/*!
 * \brief Compare for equality with another BMSLowTempSt_Struct_Idt instance.
 */
inline bool operator==(BMSLowTempSt_Struct_Idt const& l,
                       BMSLowTempSt_Struct_Idt const& r) noexcept {
  return (&l == &r) || ((l.HeatSt_Idt == r.HeatSt_Idt)
                         && (l.WarnSt_Idt == r.WarnSt_Idt)
  );
}

/*!
 * \brief Compare for inequality with another BMSLowTempSt_Struct_Idt instance.
 */
inline bool operator!=(BMSLowTempSt_Struct_Idt const& l,
                       BMSLowTempSt_Struct_Idt const& r) noexcept {
  return !(l == r);
}

}  // namespace batterypacksys_idt
}  // namespace pt_batterypacksys
}  // namespace gwm

#endif  // DENPENDENCYSRVEXE_INCLUDE_GWM_PT_BATTERYPACKSYS_BATTERYPACKSYS_IDT_IMPL_TYPE_BMSLOWTEMPST_STRUCT_IDT_H_
