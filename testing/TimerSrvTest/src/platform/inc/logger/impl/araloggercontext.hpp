#ifndef GWM_PLATFORM_LOGGER_IMPL_ARA_LOGGER_HPP
#define GWM_PLATFORM_LOGGER_IMPL_ARA_LOGGER_HPP

#include "ara/log/logging.h"

namespace gwm {
namespace platform {

using Logger = LoggerWrapper<ara::log::Logger>;

template<>
ara::log::Logger& createLogger<ara::log::Logger>(std::string name);

}}


#endif