/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/src/amsr/someip_protocol/internal/dataprototype_serialization/gwm_pt_batterypacksys_bmsstsrv_si/events/serializer_CellMinU.cpp
 *        \brief  SOME/IP protocol serializer implementation for data prototype '/ServiceInterfaces/SI_BmsStSrv/CellMinU
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_someipprotocol
 *         Commit ID: 0e137b9e6356987a6d2839c8d79c286219b68d60
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "someip-protocol/internal/serialization/ser_wrapper.h"

#include "amsr/someip_protocol/internal/dataprototype_serialization/gwm_pt_batterypacksys_bmsstsrv_si/events/serializer_CellMinU.h"
#include "amsr/someip_protocol/internal/datatype_serialization/gwm/pt_batterypacksys/batterypacksys_idt/serializer_CellMinU_Struct_Idt.h"
#include "ara/log/logging.h"

namespace amsr {
namespace someip_protocol {
namespace internal {
namespace dataprototype_serializer {
namespace gwm_pt_batterypacksys_bmsstsrv_si {
namespace events {

void SerializerCellMinU::Serialize(serialization::Writer
&writer, ::gwm::pt_batterypacksys::batterypacksys_idt::CellMinU_Struct_Idt const &data) {
  // Serialize byte stream
  serialization::SomeIpProtocolSerialize<
      TpPackDataPrototype,
      // Config of struct length field (/DataTypes/ImplementationDataTypes/CellMinU_Struct_Idt)
      serialization::LengthSize<serialization::Tp<TpPackDataPrototype>::kSizeOfStructLengthField, typename serialization::Tp<TpPackDataPrototype>::ByteOrder>

      >(writer, data);
}

}  // namespace events
}  // namespace gwm_pt_batterypacksys_bmsstsrv_si
}  // namespace dataprototype_serializer
}  // namespace internal
}  // namespace someip_protocol
}  // namespace amsr

