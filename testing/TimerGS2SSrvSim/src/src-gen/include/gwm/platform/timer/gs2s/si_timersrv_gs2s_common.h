/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/include/gwm/platform/timer/gs2s/si_timersrv_gs2s_common.h
 *        \brief  Header for service 'SI_TimerSrv_GS2S'.
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_socal
 *         Commit ID: 209b3c0234013e84e3170ee826c9a8c6022de194
 *********************************************************************************************************************/

#ifndef DENPENDENCYSRVEXE_INCLUDE_GWM_PLATFORM_TIMER_GS2S_SI_TIMERSRV_GS2S_COMMON_H_
#define DENPENDENCYSRVEXE_INCLUDE_GWM_PLATFORM_TIMER_GS2S_SI_TIMERSRV_GS2S_COMMON_H_

/*!
 * \trace SPEC-4980247, SPEC-4980248, SPEC-5951130, SPEC-4980251
 */
/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "ara/com/types.h"
#include "datatypes/gs2s/timersrv_gs2s/impl_type_sleeptype_enum.h"
#include "datatypes/gs2s/timersrv_gs2s/impl_type_startpoint_enum.h"
#include "datatypes/gs2s/timersrv_gs2s/impl_type_timerset_integer.h"
#include "datatypes/gs2s/timersrv_gs2s/impl_type_timerset_response_enum.h"
#include "datatypes/gs2s/timersrv_gs2s/impl_type_timerst_struct.h"
#include "datatypes/gs2s/timersrv_gs2s/impl_type_timertype_enum.h"
#include "gwm/platform/timer/gs2s/SI_TimerSrv_GS2S.h"

#endif  // DENPENDENCYSRVEXE_INCLUDE_GWM_PLATFORM_TIMER_GS2S_SI_TIMERSRV_GS2S_COMMON_H_
