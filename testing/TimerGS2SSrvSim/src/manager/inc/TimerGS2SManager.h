#ifndef GWM_MANAGER_WINDOW_MANAGER_HPP
#define GWM_MANAGER_WINDOW_MANAGER_HPP



#include<future>
#include<memory>
#include<functional>

#include "ara/log/logging.h"
#include "TimerGS2SImpl.h"
#include "logger/logger.hpp"

namespace gwm {
namespace manager {

using TimerSrvService = gwm::services::timersrv_gs2s::skeleton::TimerSrvGS2SImpl;

class TimerGS2SManager {
public:
    static TimerGS2SManager* getInstance() {
        static TimerGS2SManager* intsance = new TimerGS2SManager();
        return intsance;
    }

    void start();

    std::shared_ptr<TimerSrvService> getTimerSrvService() { return srv_TimerSrvService; }

protected:
    TimerGS2SManager();
    TimerGS2SManager(const TimerGS2SManager& ) = delete;

    void initWindowController();
    void startControllers();
    void startHealthMonitors();

    std::shared_ptr<TimerSrvService> srv_TimerSrvService;

    gwm::platform::Logger logger;
};

}
}

#endif