// VECTOR Same Line AutosarC++17_10-A1.1.1: MD_SOMEIPBINDING_AutosarC++17_10-A1.1.1_external_identifiers
/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/src/amsr/someip_binding_transformation_layer/internal/gwm/pt_batterypacksys/bmsstsrv_si/SI_BmsStSrv_skeleton_someip_binding_init.cpp
 *        \brief  Skeleton-side ara::com SOME/IP binding initialization for ServiceInterface 'SI_BmsStSrv'
 *
 *      \details  Full ServiceInterface path: '/ServiceInterfaces/SI_BmsStSrv'
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_someipbinding
 *         Commit ID: 43b33f0283d4d61385b98a3599688499abd86a75
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "amsr/someip_binding_transformation_layer/internal/gwm/pt_batterypacksys/bmsstsrv_si/SI_BmsStSrv_skeleton_someip_binding_init.h"
#include "amsr/someip_binding_transformation_layer/internal/gwm/pt_batterypacksys/bmsstsrv_si/SI_BmsStSrv_skeleton_someip_binding.h"
#include "ara/core/optional.h"

namespace amsr {
namespace someip_binding_transformation_layer {
namespace internal {

namespace gwm {
namespace pt_batterypacksys {
namespace bmsstsrv_si {

void AraComSomeIpBindingInitializeSkeletonSomeIpEventBackendsSI_BmsStSrv(
    AraComSomeIpBindingSpecializationSkeleton::ServerManager& server_manager) {
  { // ServiceInstance: 0x1
    { // Event: BMSChrgTi

      // SOME/IP Skeleton event backend type for event 'BMSChrgTi'.
      using SI_BmsStSrvSkeletonSomeIpEventBackendBMSChrgTi = ::amsr::someip_binding_transformation_layer::internal::SomeIpSkeletonEventBackend<SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSChrgTi>;

      std::unique_ptr<SI_BmsStSrvSkeletonSomeIpEventBackendBMSChrgTi> event_backend{
        std::make_unique<SI_BmsStSrvSkeletonSomeIpEventBackendBMSChrgTi>(1U, server_manager)};
      gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpEventManagerBMSChrgTi::EmplaceBackend(1U, std::move(event_backend));
    }
    { // Event: BMSConDchrMaxPwr

      // SOME/IP Skeleton event backend type for event 'BMSConDchrMaxPwr'.
      using SI_BmsStSrvSkeletonSomeIpEventBackendBMSConDchrMaxPwr = ::amsr::someip_binding_transformation_layer::internal::SomeIpSkeletonEventBackend<SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSConDchrMaxPwr>;

      std::unique_ptr<SI_BmsStSrvSkeletonSomeIpEventBackendBMSConDchrMaxPwr> event_backend{
        std::make_unique<SI_BmsStSrvSkeletonSomeIpEventBackendBMSConDchrMaxPwr>(1U, server_manager)};
      gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpEventManagerBMSConDchrMaxPwr::EmplaceBackend(1U, std::move(event_backend));
    }
    { // Event: BMSConFBMaxPwr

      // SOME/IP Skeleton event backend type for event 'BMSConFBMaxPwr'.
      using SI_BmsStSrvSkeletonSomeIpEventBackendBMSConFBMaxPwr = ::amsr::someip_binding_transformation_layer::internal::SomeIpSkeletonEventBackend<SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSConFBMaxPwr>;

      std::unique_ptr<SI_BmsStSrvSkeletonSomeIpEventBackendBMSConFBMaxPwr> event_backend{
        std::make_unique<SI_BmsStSrvSkeletonSomeIpEventBackendBMSConFBMaxPwr>(1U, server_manager)};
      gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpEventManagerBMSConFBMaxPwr::EmplaceBackend(1U, std::move(event_backend));
    }
    { // Event: BMSPlsDchrMaxPwr

      // SOME/IP Skeleton event backend type for event 'BMSPlsDchrMaxPwr'.
      using SI_BmsStSrvSkeletonSomeIpEventBackendBMSPlsDchrMaxPwr = ::amsr::someip_binding_transformation_layer::internal::SomeIpSkeletonEventBackend<SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSPlsDchrMaxPwr>;

      std::unique_ptr<SI_BmsStSrvSkeletonSomeIpEventBackendBMSPlsDchrMaxPwr> event_backend{
        std::make_unique<SI_BmsStSrvSkeletonSomeIpEventBackendBMSPlsDchrMaxPwr>(1U, server_manager)};
      gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpEventManagerBMSPlsDchrMaxPwr::EmplaceBackend(1U, std::move(event_backend));
    }
    { // Event: BMSPlsFBMaxPwr

      // SOME/IP Skeleton event backend type for event 'BMSPlsFBMaxPwr'.
      using SI_BmsStSrvSkeletonSomeIpEventBackendBMSPlsFBMaxPwr = ::amsr::someip_binding_transformation_layer::internal::SomeIpSkeletonEventBackend<SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSPlsFBMaxPwr>;

      std::unique_ptr<SI_BmsStSrvSkeletonSomeIpEventBackendBMSPlsFBMaxPwr> event_backend{
        std::make_unique<SI_BmsStSrvSkeletonSomeIpEventBackendBMSPlsFBMaxPwr>(1U, server_manager)};
      gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpEventManagerBMSPlsFBMaxPwr::EmplaceBackend(1U, std::move(event_backend));
    }
    { // Event: BmsInletCooltT

      // SOME/IP Skeleton event backend type for event 'BmsInletCooltT'.
      using SI_BmsStSrvSkeletonSomeIpEventBackendBmsInletCooltT = ::amsr::someip_binding_transformation_layer::internal::SomeIpSkeletonEventBackend<SI_BmsStSrvSkeletonSomeIpEventConfigurationBmsInletCooltT>;

      std::unique_ptr<SI_BmsStSrvSkeletonSomeIpEventBackendBmsInletCooltT> event_backend{
        std::make_unique<SI_BmsStSrvSkeletonSomeIpEventBackendBmsInletCooltT>(1U, server_manager)};
      gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpEventManagerBmsInletCooltT::EmplaceBackend(1U, std::move(event_backend));
    }
    { // Event: BmsIsoR

      // SOME/IP Skeleton event backend type for event 'BmsIsoR'.
      using SI_BmsStSrvSkeletonSomeIpEventBackendBmsIsoR = ::amsr::someip_binding_transformation_layer::internal::SomeIpSkeletonEventBackend<SI_BmsStSrvSkeletonSomeIpEventConfigurationBmsIsoR>;

      std::unique_ptr<SI_BmsStSrvSkeletonSomeIpEventBackendBmsIsoR> event_backend{
        std::make_unique<SI_BmsStSrvSkeletonSomeIpEventBackendBmsIsoR>(1U, server_manager)};
      gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpEventManagerBmsIsoR::EmplaceBackend(1U, std::move(event_backend));
    }
    { // Event: BmsOutletCooltT

      // SOME/IP Skeleton event backend type for event 'BmsOutletCooltT'.
      using SI_BmsStSrvSkeletonSomeIpEventBackendBmsOutletCooltT = ::amsr::someip_binding_transformation_layer::internal::SomeIpSkeletonEventBackend<SI_BmsStSrvSkeletonSomeIpEventConfigurationBmsOutletCooltT>;

      std::unique_ptr<SI_BmsStSrvSkeletonSomeIpEventBackendBmsOutletCooltT> event_backend{
        std::make_unique<SI_BmsStSrvSkeletonSomeIpEventBackendBmsOutletCooltT>(1U, server_manager)};
      gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpEventManagerBmsOutletCooltT::EmplaceBackend(1U, std::move(event_backend));
    }
    { // Event: CellMaxT

      // SOME/IP Skeleton event backend type for event 'CellMaxT'.
      using SI_BmsStSrvSkeletonSomeIpEventBackendCellMaxT = ::amsr::someip_binding_transformation_layer::internal::SomeIpSkeletonEventBackend<SI_BmsStSrvSkeletonSomeIpEventConfigurationCellMaxT>;

      std::unique_ptr<SI_BmsStSrvSkeletonSomeIpEventBackendCellMaxT> event_backend{
        std::make_unique<SI_BmsStSrvSkeletonSomeIpEventBackendCellMaxT>(1U, server_manager)};
      gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpEventManagerCellMaxT::EmplaceBackend(1U, std::move(event_backend));
    }
    { // Event: CellMaxU

      // SOME/IP Skeleton event backend type for event 'CellMaxU'.
      using SI_BmsStSrvSkeletonSomeIpEventBackendCellMaxU = ::amsr::someip_binding_transformation_layer::internal::SomeIpSkeletonEventBackend<SI_BmsStSrvSkeletonSomeIpEventConfigurationCellMaxU>;

      std::unique_ptr<SI_BmsStSrvSkeletonSomeIpEventBackendCellMaxU> event_backend{
        std::make_unique<SI_BmsStSrvSkeletonSomeIpEventBackendCellMaxU>(1U, server_manager)};
      gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpEventManagerCellMaxU::EmplaceBackend(1U, std::move(event_backend));
    }
    { // Event: CellMinT

      // SOME/IP Skeleton event backend type for event 'CellMinT'.
      using SI_BmsStSrvSkeletonSomeIpEventBackendCellMinT = ::amsr::someip_binding_transformation_layer::internal::SomeIpSkeletonEventBackend<SI_BmsStSrvSkeletonSomeIpEventConfigurationCellMinT>;

      std::unique_ptr<SI_BmsStSrvSkeletonSomeIpEventBackendCellMinT> event_backend{
        std::make_unique<SI_BmsStSrvSkeletonSomeIpEventBackendCellMinT>(1U, server_manager)};
      gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpEventManagerCellMinT::EmplaceBackend(1U, std::move(event_backend));
    }
    { // Event: CellMinU

      // SOME/IP Skeleton event backend type for event 'CellMinU'.
      using SI_BmsStSrvSkeletonSomeIpEventBackendCellMinU = ::amsr::someip_binding_transformation_layer::internal::SomeIpSkeletonEventBackend<SI_BmsStSrvSkeletonSomeIpEventConfigurationCellMinU>;

      std::unique_ptr<SI_BmsStSrvSkeletonSomeIpEventBackendCellMinU> event_backend{
        std::make_unique<SI_BmsStSrvSkeletonSomeIpEventBackendCellMinU>(1U, server_manager)};
      gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpEventManagerCellMinU::EmplaceBackend(1U, std::move(event_backend));
    }
    { // Event: HVBattDispySoc

      // SOME/IP Skeleton event backend type for event 'HVBattDispySoc'.
      using SI_BmsStSrvSkeletonSomeIpEventBackendHVBattDispySoc = ::amsr::someip_binding_transformation_layer::internal::SomeIpSkeletonEventBackend<SI_BmsStSrvSkeletonSomeIpEventConfigurationHVBattDispySoc>;

      std::unique_ptr<SI_BmsStSrvSkeletonSomeIpEventBackendHVBattDispySoc> event_backend{
        std::make_unique<SI_BmsStSrvSkeletonSomeIpEventBackendHVBattDispySoc>(1U, server_manager)};
      gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpEventManagerHVBattDispySoc::EmplaceBackend(1U, std::move(event_backend));
    }
    { // Event: HVBattI

      // SOME/IP Skeleton event backend type for event 'HVBattI'.
      using SI_BmsStSrvSkeletonSomeIpEventBackendHVBattI = ::amsr::someip_binding_transformation_layer::internal::SomeIpSkeletonEventBackend<SI_BmsStSrvSkeletonSomeIpEventConfigurationHVBattI>;

      std::unique_ptr<SI_BmsStSrvSkeletonSomeIpEventBackendHVBattI> event_backend{
        std::make_unique<SI_BmsStSrvSkeletonSomeIpEventBackendHVBattI>(1U, server_manager)};
      gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpEventManagerHVBattI::EmplaceBackend(1U, std::move(event_backend));
    }
    { // Event: HVBattInnerSoc

      // SOME/IP Skeleton event backend type for event 'HVBattInnerSoc'.
      using SI_BmsStSrvSkeletonSomeIpEventBackendHVBattInnerSoc = ::amsr::someip_binding_transformation_layer::internal::SomeIpSkeletonEventBackend<SI_BmsStSrvSkeletonSomeIpEventConfigurationHVBattInnerSoc>;

      std::unique_ptr<SI_BmsStSrvSkeletonSomeIpEventBackendHVBattInnerSoc> event_backend{
        std::make_unique<SI_BmsStSrvSkeletonSomeIpEventBackendHVBattInnerSoc>(1U, server_manager)};
      gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpEventManagerHVBattInnerSoc::EmplaceBackend(1U, std::move(event_backend));
    }
    { // Event: HVBattSoh

      // SOME/IP Skeleton event backend type for event 'HVBattSoh'.
      using SI_BmsStSrvSkeletonSomeIpEventBackendHVBattSoh = ::amsr::someip_binding_transformation_layer::internal::SomeIpSkeletonEventBackend<SI_BmsStSrvSkeletonSomeIpEventConfigurationHVBattSoh>;

      std::unique_ptr<SI_BmsStSrvSkeletonSomeIpEventBackendHVBattSoh> event_backend{
        std::make_unique<SI_BmsStSrvSkeletonSomeIpEventBackendHVBattSoh>(1U, server_manager)};
      gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpEventManagerHVBattSoh::EmplaceBackend(1U, std::move(event_backend));
    }
    { // Event: HVBattTotEgy

      // SOME/IP Skeleton event backend type for event 'HVBattTotEgy'.
      using SI_BmsStSrvSkeletonSomeIpEventBackendHVBattTotEgy = ::amsr::someip_binding_transformation_layer::internal::SomeIpSkeletonEventBackend<SI_BmsStSrvSkeletonSomeIpEventConfigurationHVBattTotEgy>;

      std::unique_ptr<SI_BmsStSrvSkeletonSomeIpEventBackendHVBattTotEgy> event_backend{
        std::make_unique<SI_BmsStSrvSkeletonSomeIpEventBackendHVBattTotEgy>(1U, server_manager)};
      gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpEventManagerHVBattTotEgy::EmplaceBackend(1U, std::move(event_backend));
    }
    { // Event: HVBattU

      // SOME/IP Skeleton event backend type for event 'HVBattU'.
      using SI_BmsStSrvSkeletonSomeIpEventBackendHVBattU = ::amsr::someip_binding_transformation_layer::internal::SomeIpSkeletonEventBackend<SI_BmsStSrvSkeletonSomeIpEventConfigurationHVBattU>;

      std::unique_ptr<SI_BmsStSrvSkeletonSomeIpEventBackendHVBattU> event_backend{
        std::make_unique<SI_BmsStSrvSkeletonSomeIpEventBackendHVBattU>(1U, server_manager)};
      gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpEventManagerHVBattU::EmplaceBackend(1U, std::move(event_backend));
    }
    { // Event: ModulAvrgT

      // SOME/IP Skeleton event backend type for event 'ModulAvrgT'.
      using SI_BmsStSrvSkeletonSomeIpEventBackendModulAvrgT = ::amsr::someip_binding_transformation_layer::internal::SomeIpSkeletonEventBackend<SI_BmsStSrvSkeletonSomeIpEventConfigurationModulAvrgT>;

      std::unique_ptr<SI_BmsStSrvSkeletonSomeIpEventBackendModulAvrgT> event_backend{
        std::make_unique<SI_BmsStSrvSkeletonSomeIpEventBackendModulAvrgT>(1U, server_manager)};
      gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpEventManagerModulAvrgT::EmplaceBackend(1U, std::move(event_backend));
    }

    { // Field notifier: BMSChrgSt

      // SOME/IP Skeleton event backend type for field 'BMSChrgSt'.
      using SI_BmsStSrvSkeletonSomeIpEventBackendBMSChrgSt = ::amsr::someip_binding_transformation_layer::internal::SomeIpSkeletonEventBackend<SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSChrgSt>;

      std::unique_ptr<SI_BmsStSrvSkeletonSomeIpEventBackendBMSChrgSt> event_backend{
        std::make_unique<SI_BmsStSrvSkeletonSomeIpEventBackendBMSChrgSt>(1U, server_manager)};
      gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSChrgSt::EmplaceBackend(1U, std::move(event_backend));
    }
    { // Field notifier: BMSDCChrgCnctSt

      // SOME/IP Skeleton event backend type for field 'BMSDCChrgCnctSt'.
      using SI_BmsStSrvSkeletonSomeIpEventBackendBMSDCChrgCnctSt = ::amsr::someip_binding_transformation_layer::internal::SomeIpSkeletonEventBackend<SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSDCChrgCnctSt>;

      std::unique_ptr<SI_BmsStSrvSkeletonSomeIpEventBackendBMSDCChrgCnctSt> event_backend{
        std::make_unique<SI_BmsStSrvSkeletonSomeIpEventBackendBMSDCChrgCnctSt>(1U, server_manager)};
      gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSDCChrgCnctSt::EmplaceBackend(1U, std::move(event_backend));
    }
    { // Field notifier: BMSDCChrgEndReas

      // SOME/IP Skeleton event backend type for field 'BMSDCChrgEndReas'.
      using SI_BmsStSrvSkeletonSomeIpEventBackendBMSDCChrgEndReas = ::amsr::someip_binding_transformation_layer::internal::SomeIpSkeletonEventBackend<SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSDCChrgEndReas>;

      std::unique_ptr<SI_BmsStSrvSkeletonSomeIpEventBackendBMSDCChrgEndReas> event_backend{
        std::make_unique<SI_BmsStSrvSkeletonSomeIpEventBackendBMSDCChrgEndReas>(1U, server_manager)};
      gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSDCChrgEndReas::EmplaceBackend(1U, std::move(event_backend));
    }
    { // Field notifier: BMSDchrgPrmsSt

      // SOME/IP Skeleton event backend type for field 'BMSDchrgPrmsSt'.
      using SI_BmsStSrvSkeletonSomeIpEventBackendBMSDchrgPrmsSt = ::amsr::someip_binding_transformation_layer::internal::SomeIpSkeletonEventBackend<SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSDchrgPrmsSt>;

      std::unique_ptr<SI_BmsStSrvSkeletonSomeIpEventBackendBMSDchrgPrmsSt> event_backend{
        std::make_unique<SI_BmsStSrvSkeletonSomeIpEventBackendBMSDchrgPrmsSt>(1U, server_manager)};
      gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSDchrgPrmsSt::EmplaceBackend(1U, std::move(event_backend));
    }
    { // Field notifier: BMSHeatRunawaySt

      // SOME/IP Skeleton event backend type for field 'BMSHeatRunawaySt'.
      using SI_BmsStSrvSkeletonSomeIpEventBackendBMSHeatRunawaySt = ::amsr::someip_binding_transformation_layer::internal::SomeIpSkeletonEventBackend<SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSHeatRunawaySt>;

      std::unique_ptr<SI_BmsStSrvSkeletonSomeIpEventBackendBMSHeatRunawaySt> event_backend{
        std::make_unique<SI_BmsStSrvSkeletonSomeIpEventBackendBMSHeatRunawaySt>(1U, server_manager)};
      gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSHeatRunawaySt::EmplaceBackend(1U, std::move(event_backend));
    }
    { // Field notifier: BMSInnerSOCSt

      // SOME/IP Skeleton event backend type for field 'BMSInnerSOCSt'.
      using SI_BmsStSrvSkeletonSomeIpEventBackendBMSInnerSOCSt = ::amsr::someip_binding_transformation_layer::internal::SomeIpSkeletonEventBackend<SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSInnerSOCSt>;

      std::unique_ptr<SI_BmsStSrvSkeletonSomeIpEventBackendBMSInnerSOCSt> event_backend{
        std::make_unique<SI_BmsStSrvSkeletonSomeIpEventBackendBMSInnerSOCSt>(1U, server_manager)};
      gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSInnerSOCSt::EmplaceBackend(1U, std::move(event_backend));
    }
    { // Field notifier: BMSIntelTempMgtSt

      // SOME/IP Skeleton event backend type for field 'BMSIntelTempMgtSt'.
      using SI_BmsStSrvSkeletonSomeIpEventBackendBMSIntelTempMgtSt = ::amsr::someip_binding_transformation_layer::internal::SomeIpSkeletonEventBackend<SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSIntelTempMgtSt>;

      std::unique_ptr<SI_BmsStSrvSkeletonSomeIpEventBackendBMSIntelTempMgtSt> event_backend{
        std::make_unique<SI_BmsStSrvSkeletonSomeIpEventBackendBMSIntelTempMgtSt>(1U, server_manager)};
      gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSIntelTempMgtSt::EmplaceBackend(1U, std::move(event_backend));
    }
    { // Field notifier: BMSLowTempSt

      // SOME/IP Skeleton event backend type for field 'BMSLowTempSt'.
      using SI_BmsStSrvSkeletonSomeIpEventBackendBMSLowTempSt = ::amsr::someip_binding_transformation_layer::internal::SomeIpSkeletonEventBackend<SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSLowTempSt>;

      std::unique_ptr<SI_BmsStSrvSkeletonSomeIpEventBackendBMSLowTempSt> event_backend{
        std::make_unique<SI_BmsStSrvSkeletonSomeIpEventBackendBMSLowTempSt>(1U, server_manager)};
      gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSLowTempSt::EmplaceBackend(1U, std::move(event_backend));
    }
    { // Field notifier: BMSRmtPreHeatSt

      // SOME/IP Skeleton event backend type for field 'BMSRmtPreHeatSt'.
      using SI_BmsStSrvSkeletonSomeIpEventBackendBMSRmtPreHeatSt = ::amsr::someip_binding_transformation_layer::internal::SomeIpSkeletonEventBackend<SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSRmtPreHeatSt>;

      std::unique_ptr<SI_BmsStSrvSkeletonSomeIpEventBackendBMSRmtPreHeatSt> event_backend{
        std::make_unique<SI_BmsStSrvSkeletonSomeIpEventBackendBMSRmtPreHeatSt>(1U, server_manager)};
      gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSRmtPreHeatSt::EmplaceBackend(1U, std::move(event_backend));
    }
    { // Field notifier: BMSSafCnctSt

      // SOME/IP Skeleton event backend type for field 'BMSSafCnctSt'.
      using SI_BmsStSrvSkeletonSomeIpEventBackendBMSSafCnctSt = ::amsr::someip_binding_transformation_layer::internal::SomeIpSkeletonEventBackend<SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSSafCnctSt>;

      std::unique_ptr<SI_BmsStSrvSkeletonSomeIpEventBackendBMSSafCnctSt> event_backend{
        std::make_unique<SI_BmsStSrvSkeletonSomeIpEventBackendBMSSafCnctSt>(1U, server_manager)};
      gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSSafCnctSt::EmplaceBackend(1U, std::move(event_backend));
    }
    { // Field notifier: BmsErrSt

      // SOME/IP Skeleton event backend type for field 'BmsErrSt'.
      using SI_BmsStSrvSkeletonSomeIpEventBackendBmsErrSt = ::amsr::someip_binding_transformation_layer::internal::SomeIpSkeletonEventBackend<SI_BmsStSrvSkeletonSomeIpEventConfigurationBmsErrSt>;

      std::unique_ptr<SI_BmsStSrvSkeletonSomeIpEventBackendBmsErrSt> event_backend{
        std::make_unique<SI_BmsStSrvSkeletonSomeIpEventBackendBmsErrSt>(1U, server_manager)};
      gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpFieldNotifierBmsErrSt::EmplaceBackend(1U, std::move(event_backend));
    }
    { // Field notifier: BmsHVILSt

      // SOME/IP Skeleton event backend type for field 'BmsHVILSt'.
      using SI_BmsStSrvSkeletonSomeIpEventBackendBmsHVILSt = ::amsr::someip_binding_transformation_layer::internal::SomeIpSkeletonEventBackend<SI_BmsStSrvSkeletonSomeIpEventConfigurationBmsHVILSt>;

      std::unique_ptr<SI_BmsStSrvSkeletonSomeIpEventBackendBmsHVILSt> event_backend{
        std::make_unique<SI_BmsStSrvSkeletonSomeIpEventBackendBmsHVILSt>(1U, server_manager)};
      gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpFieldNotifierBmsHVILSt::EmplaceBackend(1U, std::move(event_backend));
    }
    { // Field notifier: BmsIsoMeasSt

      // SOME/IP Skeleton event backend type for field 'BmsIsoMeasSt'.
      using SI_BmsStSrvSkeletonSomeIpEventBackendBmsIsoMeasSt = ::amsr::someip_binding_transformation_layer::internal::SomeIpSkeletonEventBackend<SI_BmsStSrvSkeletonSomeIpEventConfigurationBmsIsoMeasSt>;

      std::unique_ptr<SI_BmsStSrvSkeletonSomeIpEventBackendBmsIsoMeasSt> event_backend{
        std::make_unique<SI_BmsStSrvSkeletonSomeIpEventBackendBmsIsoMeasSt>(1U, server_manager)};
      gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpFieldNotifierBmsIsoMeasSt::EmplaceBackend(1U, std::move(event_backend));
    }
    { // Field notifier: BmsSt

      // SOME/IP Skeleton event backend type for field 'BmsSt'.
      using SI_BmsStSrvSkeletonSomeIpEventBackendBmsSt = ::amsr::someip_binding_transformation_layer::internal::SomeIpSkeletonEventBackend<SI_BmsStSrvSkeletonSomeIpEventConfigurationBmsSt>;

      std::unique_ptr<SI_BmsStSrvSkeletonSomeIpEventBackendBmsSt> event_backend{
        std::make_unique<SI_BmsStSrvSkeletonSomeIpEventBackendBmsSt>(1U, server_manager)};
      gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpFieldNotifierBmsSt::EmplaceBackend(1U, std::move(event_backend));
    }
    { // Field notifier: HVBattSOCLim

      // SOME/IP Skeleton event backend type for field 'HVBattSOCLim'.
      using SI_BmsStSrvSkeletonSomeIpEventBackendHVBattSOCLim = ::amsr::someip_binding_transformation_layer::internal::SomeIpSkeletonEventBackend<SI_BmsStSrvSkeletonSomeIpEventConfigurationHVBattSOCLim>;

      std::unique_ptr<SI_BmsStSrvSkeletonSomeIpEventBackendHVBattSOCLim> event_backend{
        std::make_unique<SI_BmsStSrvSkeletonSomeIpEventBackendHVBattSOCLim>(1U, server_manager)};
      gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpFieldNotifierHVBattSOCLim::EmplaceBackend(1U, std::move(event_backend));
    }
    { // Field notifier: HVMaiSwtSt

      // SOME/IP Skeleton event backend type for field 'HVMaiSwtSt'.
      using SI_BmsStSrvSkeletonSomeIpEventBackendHVMaiSwtSt = ::amsr::someip_binding_transformation_layer::internal::SomeIpSkeletonEventBackend<SI_BmsStSrvSkeletonSomeIpEventConfigurationHVMaiSwtSt>;

      std::unique_ptr<SI_BmsStSrvSkeletonSomeIpEventBackendHVMaiSwtSt> event_backend{
        std::make_unique<SI_BmsStSrvSkeletonSomeIpEventBackendHVMaiSwtSt>(1U, server_manager)};
      gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpFieldNotifierHVMaiSwtSt::EmplaceBackend(1U, std::move(event_backend));
    }
  }
}

void AraComSomeIpBindingDeInitializeSkeletonSomeIpEventBackendsSI_BmsStSrv() {
  // Event: BMSChrgTi
  gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpEventManagerBMSChrgTi::ClearBackendList();
  // Event: BMSConDchrMaxPwr
  gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpEventManagerBMSConDchrMaxPwr::ClearBackendList();
  // Event: BMSConFBMaxPwr
  gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpEventManagerBMSConFBMaxPwr::ClearBackendList();
  // Event: BMSPlsDchrMaxPwr
  gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpEventManagerBMSPlsDchrMaxPwr::ClearBackendList();
  // Event: BMSPlsFBMaxPwr
  gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpEventManagerBMSPlsFBMaxPwr::ClearBackendList();
  // Event: BmsInletCooltT
  gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpEventManagerBmsInletCooltT::ClearBackendList();
  // Event: BmsIsoR
  gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpEventManagerBmsIsoR::ClearBackendList();
  // Event: BmsOutletCooltT
  gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpEventManagerBmsOutletCooltT::ClearBackendList();
  // Event: CellMaxT
  gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpEventManagerCellMaxT::ClearBackendList();
  // Event: CellMaxU
  gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpEventManagerCellMaxU::ClearBackendList();
  // Event: CellMinT
  gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpEventManagerCellMinT::ClearBackendList();
  // Event: CellMinU
  gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpEventManagerCellMinU::ClearBackendList();
  // Event: HVBattDispySoc
  gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpEventManagerHVBattDispySoc::ClearBackendList();
  // Event: HVBattI
  gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpEventManagerHVBattI::ClearBackendList();
  // Event: HVBattInnerSoc
  gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpEventManagerHVBattInnerSoc::ClearBackendList();
  // Event: HVBattSoh
  gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpEventManagerHVBattSoh::ClearBackendList();
  // Event: HVBattTotEgy
  gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpEventManagerHVBattTotEgy::ClearBackendList();
  // Event: HVBattU
  gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpEventManagerHVBattU::ClearBackendList();
  // Event: ModulAvrgT
  gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpEventManagerModulAvrgT::ClearBackendList();

  // Field notifier: BMSChrgSt
  gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSChrgSt::ClearBackendList();

  // Field notifier: BMSDCChrgCnctSt
  gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSDCChrgCnctSt::ClearBackendList();

  // Field notifier: BMSDCChrgEndReas
  gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSDCChrgEndReas::ClearBackendList();

  // Field notifier: BMSDchrgPrmsSt
  gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSDchrgPrmsSt::ClearBackendList();

  // Field notifier: BMSHeatRunawaySt
  gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSHeatRunawaySt::ClearBackendList();

  // Field notifier: BMSInnerSOCSt
  gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSInnerSOCSt::ClearBackendList();

  // Field notifier: BMSIntelTempMgtSt
  gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSIntelTempMgtSt::ClearBackendList();

  // Field notifier: BMSLowTempSt
  gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSLowTempSt::ClearBackendList();

  // Field notifier: BMSRmtPreHeatSt
  gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSRmtPreHeatSt::ClearBackendList();

  // Field notifier: BMSSafCnctSt
  gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSSafCnctSt::ClearBackendList();

  // Field notifier: BmsErrSt
  gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpFieldNotifierBmsErrSt::ClearBackendList();

  // Field notifier: BmsHVILSt
  gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpFieldNotifierBmsHVILSt::ClearBackendList();

  // Field notifier: BmsIsoMeasSt
  gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpFieldNotifierBmsIsoMeasSt::ClearBackendList();

  // Field notifier: BmsSt
  gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpFieldNotifierBmsSt::ClearBackendList();

  // Field notifier: HVBattSOCLim
  gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpFieldNotifierHVBattSOCLim::ClearBackendList();

  // Field notifier: HVMaiSwtSt
  gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpFieldNotifierHVMaiSwtSt::ClearBackendList();

}


}  // namespace bmsstsrv_si
}  // namespace pt_batterypacksys
}  // namespace gwm

}  // namespace internal
}  // namespace someip_binding_transformation_layer
}  // namespace amsr

