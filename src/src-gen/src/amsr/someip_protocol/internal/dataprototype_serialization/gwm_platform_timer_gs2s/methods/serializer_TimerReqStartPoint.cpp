/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  TimerSrvExe/src/amsr/someip_protocol/internal/dataprototype_serialization/gwm_platform_timer_gs2s/methods/serializer_TimerReqStartPoint.cpp
 *        \brief  SOME/IP protocol serializer implementation for data prototype '/S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/SI_TimerSrv_GS2S/TimerReq/StartPoint
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_someipprotocol
 *         Commit ID: 0e137b9e6356987a6d2839c8d79c286219b68d60
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "someip-protocol/internal/serialization/ser_wrapper.h"

#include "amsr/someip_protocol/internal/dataprototype_serialization/gwm_platform_timer_gs2s/methods/serializer_TimerReqStartPoint.h"
#include "ara/log/logging.h"

namespace amsr {
namespace someip_protocol {
namespace internal {
namespace dataprototype_serializer {
namespace gwm_platform_timer_gs2s {
namespace methods {

void SerializerTimerReqStartPoint::Serialize(serialization::Writer
&writer, ::datatypes::gs2s::timersrv_gs2s::StartPoint_Enum const &data) {
  // Serialize byte stream
  serialization::SomeIpProtocolSerialize<
      TpPackDataPrototype,
      // Byte-order of primitive datatype (/S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/StartPoint_Enum)
      typename serialization::Tp<TpPackDataPrototype>::ByteOrder

      >(writer, data);
}

}  // namespace methods
}  // namespace gwm_platform_timer_gs2s
}  // namespace dataprototype_serializer
}  // namespace internal
}  // namespace someip_protocol
}  // namespace amsr

