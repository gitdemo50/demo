#include <iostream>
#include <chrono>
#include <thread>
#include <map>

#include "ara/core/future.h"
#include "ara/core/initialization.h"

#include "TimerGS2SManager.h"

int main(int, char**) {
    
    ara::core::Result<void> init_result{ara::core::Initialize()};

    if (!init_result.HasValue()) {
        ara::core::Abort("ara::core::Initialize() failed Aborting");
    }

    auto manager = gwm::manager::TimerGS2SManager::getInstance();

    manager->start();

    while (true)
    {
        std::this_thread::sleep_for(std::chrono::seconds(2));
    }
    
    return 0;
}
