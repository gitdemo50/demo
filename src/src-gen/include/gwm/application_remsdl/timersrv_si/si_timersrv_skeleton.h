/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  TimerSrvExe/include/gwm/application_remsdl/timersrv_si/si_timersrv_skeleton.h
 *        \brief  Skeleton for service 'SI_TimerSrv'.
 *
 *      \details  This Service interface provides timer
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_socal
 *         Commit ID: 209b3c0234013e84e3170ee826c9a8c6022de194
 *********************************************************************************************************************/

#ifndef TIMERSRVEXE_INCLUDE_GWM_APPLICATION_REMSDL_TIMERSRV_SI_SI_TIMERSRV_SKELETON_H_
#define TIMERSRVEXE_INCLUDE_GWM_APPLICATION_REMSDL_TIMERSRV_SI_SI_TIMERSRV_SKELETON_H_

/*!
 * \trace SPEC-4980239
 */
/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "amsr/generic/singleton_wrapper.h"
#include "amsr/socal/events/skeleton_event.h"
#include "amsr/socal/fields/skeleton_field.h"
#include "amsr/socal/internal/events/skeleton_event_manager_interface.h"
#include "amsr/socal/internal/service_discovery/service_discovery.h"
#include "amsr/socal/skeleton.h"
#include "ara/core/future.h"
#include "ara/core/instance_specifier.h"
#include "gwm/application_remsdl/timersrv_si/SI_TimerSrv_skeleton_impl_interface.h"
#include "gwm/application_remsdl/timersrv_si/SI_TimerSrv_types.h"

/*!
 * \trace SPEC-4980240
 */
namespace gwm {
namespace application_remsdl {
namespace timersrv_si {
/*!
 * \trace SPEC-4980241
 */
namespace skeleton {
/*!
 * \brief Forward declaration for inserting as a type into the template class SkeletonEvent
 */
class SI_TimerSrvSkeleton;

/*!
 * \trace SPEC-4980244
 */
namespace methods {

/*!
 * \brief Data class for service method 'TimerCncl'.
 */
using TimerCncl = gwm::application_remsdl::timersrv_si::internal::methods::TimerCncl;

/*!
 * \brief Data class for service method 'TimerReq'.
 */
using TimerReq = gwm::application_remsdl::timersrv_si::internal::methods::TimerReq;

}  // namespace methods

/*!
 * \trace SPEC-4980243
 */
namespace events {

}  // namespace events

/*!
 * \trace SPEC-4980245
 */
namespace fields {
/*!
 * \brief Type alias for the notification of field 'TimerSt'.
 */
using FieldNotifierTimerSt = ::amsr::socal::events::SkeletonEvent<
  gwm::application_remsdl::timersrv_si::skeleton::SI_TimerSrvSkeleton,
  ::gwm::platform_timer::timer_idt::TimerSt_Struct_Idt,
  gwm::application_remsdl::timersrv_si::internal::SI_TimerSrvSkeletonImplInterface,
  ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::platform_timer::timer_idt::TimerSt_Struct_Idt>,
  &gwm::application_remsdl::timersrv_si::internal::SI_TimerSrvSkeletonImplInterface::GetFieldNotifierTimerSt>;

/*!
 * \brief Type alias for the notifier configuration of field 'TimerSt'.
 */
using FieldNotifierConfigTimerSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasNotifier<true, FieldNotifierTimerSt>;

/*!
 * \brief Type alias for the getter configuration of field 'TimerSt'.
 */
using FieldGetterConfigTimerSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasGetter<true>;

/*!
 * \brief Type alias for the setter configuration of field 'TimerSt'.
 */
using FieldSetterConfigTimerSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasSetter<false>;

/*!
 * \brief Field name.
 */
struct TimerStName {
  /*!
   * \brief Field name string.
   */
  static constexpr vac::container::CStringView Name{"TimerSt"_sv};
};

/*!
 * \brief Type alias for the parameterization of field 'TimerSt'.
 */
using FieldConfigTimerSt = ::amsr::socal::internal::fields::SkeletonFieldConfig<gwm::application_remsdl::timersrv_si::skeleton::SI_TimerSrvSkeleton, FieldNotifierConfigTimerSt, FieldGetterConfigTimerSt, FieldSetterConfigTimerSt, TimerStName>;

/*!
 * \brief Type alias for service field 'TimerSt', that is part of the skeleton.
 *
 * \trace SPEC-4980343
 */
using TimerSt = ::amsr::socal::fields::SkeletonField<::gwm::platform_timer::timer_idt::TimerSt_Struct_Idt, FieldConfigTimerSt>;


}  // namespace fields

/*!
 * \brief Skeleton interface class for the service 'SI_TimerSrv'.
 * \details This Service interface provides timer
 *
 * \vpublic
 * \trace SPEC-4980341
 */
class SI_TimerSrvSkeleton
    : public ::amsr::socal::Skeleton<gwm::application_remsdl::timersrv_si::SI_TimerSrv,
                                          gwm::application_remsdl::timersrv_si::internal::SI_TimerSrvSkeletonImplInterface> {
 public:
// ---- Constructors / Destructors -----------------------------------------------------------------------------------
/*!
 * \brief Exception-less pre-construction of SI_TimerSrv.
 *
 * \param[in] instance The InstanceIdentifier of the service instance to be created.
 *                     Expected format: "<Binding type/prefix>:<binding specific instance ID>".
 *                     The InstanceIdentifier must fulfill the following preconditions:
 *                     - Must be configured in the ARXML model.
 *                     - Must belong to the service interface.
 * \param[in] mode The mode of the service implementation for processing service method invocations.
 *                 Default: Event-driven processing.
 *                 Preconditions to be fulfilled:
 *                 - If the mode 'kEventSingleThread' is used, a possible user-defined ThreadPool must have exactly one
 *                   worker thread configured.
 * \return Result<ConstructionToken> Result containing construction token from which a skeleton object can be
 *         constructed.
 *
 * \pre No other service skeleton for the same instance must exist concurrently. After destruction of an already
 *      created skeleton object, the instantiation for the same service instance will be possible.
 * \context App
 * \threadsafe FALSE
 * \reentrant FALSE
 * \vpublic
 * \synchronous TRUE
 * \trace SPEC-8053551
 * \trace SPEC-8053550
 */
static ConstructionResult Preconstruct(
    ara::com::InstanceIdentifier instance_id,
    ara::com::MethodCallProcessingMode const mode = ara::com::MethodCallProcessingMode::kEvent) noexcept;

/*!
 * \brief Exception-less pre-construction of SI_TimerSrvSkeleton.
 *
 * \param[in] instance InstanceSpecifier of this service.
 *                     The provided InstanceSpecifier must fulfill the following preconditions:
 *                     - Must be configured in the ARXML model.
 *                     - Must belong to the service interface.
 * \param[in] mode The mode of the service implementation for processing service method invocations.
 *                 Default: Event-driven processing.
 *                 Preconditions to be fulfilled:
 *                 - If the mode 'kEventSingleThread' is used, a possible user-defined ThreadPool must have exactly one
 *                   worker thread configured.
 * \return Result<ConstructionToken> Result containing construction token from which a skeleton object can be
 *         constructed.
 *
 * \pre No other service skeleton for the same instance must exist concurrently. After destruction of an already
 *      created skeleton object, the instantation for the same service instance will be possible.
 * \context App
 * \threadsafe FALSE
 * \reentrant FALSE
 * \vpublic
 * \synchronous TRUE
 * \trace SPEC-8053553
 * \trace SPEC-8053550
 */
static ConstructionResult Preconstruct(
    ara::core::InstanceSpecifier instance,
    ara::com::MethodCallProcessingMode const mode = ara::com::MethodCallProcessingMode::kEvent) noexcept;

/*!
 * \brief Exception-less pre-construction of SI_TimerSrvSkeleton.
 *
 * \param[in] instance_identifiers The container of instances of a service, each instance element needed to distinguish
 *                                 different instances of exactly the same service in the system.
 *                                 The provided InstanceIdentifierContainer must fulfill the following preconditions:
 *                                 - Every InstanceIdentifier of the container must be configured in the ARXML model.
 *                                 - Every InstanceIdentifier of the container must belong to the service interface to be
 *                                   instantiated.
 *                                 - The container must not be empty.
 *                                 - All elements of the container must be unique (no duplicates).
 * \param[in] mode The mode of the service implementation for processing service method invocations.
 *                 Default: Event-driven processing.
 *                 Preconditions to be fulfilled:
 *                 - If the mode 'kEventSingleThread' is used, a possible user-defined ThreadPool must have exactly one
 *                   worker thread configured.
 * \return Result<ConstructionToken> Result containing construction token from which a skeleton object can be
 *         constructed.
 *
 * \pre No other service skeleton for the same instance must exist concurrently. After destruction of an already
 *      created skeleton object, the instantation for the same service instance will be possible.
 * \context App
 * \threadsafe FALSE
 * \reentrant FALSE
 * \vpublic
 * \synchronous TRUE
 * \trace SPEC-8053555
 * \trace SPEC-8053550
 */
static ConstructionResult Preconstruct(
    ara::com::InstanceIdentifierContainer instance_identifiers,
    ara::com::MethodCallProcessingMode const mode = ara::com::MethodCallProcessingMode::kEvent) noexcept;

/*!
 * \brief Exception-less constructor of SI_TimerSrvSkeleton.
 * \details Because of internal resource management strategy, all created skeletons shall be released before the
 *          Runtime is destroyed; i.e. they cannot not be stored in variables with longer life period than the
 *          application's main(). If not followed, it's not guaranteed that the communication middleware is shut down
 *          properly and may lead to segmentation fault.
 *
 * \param[in] token ConstructionToken created with Preconstruct() API.
 * \context App
 * \threadsafe FALSE
 * \reentrant FALSE
 * \vpublic
 * \synchronous TRUE
 * \trace SPEC-8053551
 * \trace SPEC-8053553
 * \trace SPEC-8053555
 */
explicit SI_TimerSrvSkeleton(ConstructionToken&& token) noexcept;

  /*!
   * \brief Delete default constructor.
   */
  SI_TimerSrvSkeleton() = delete;
  /*!
   * \brief Delete copy constructor.
   */
  SI_TimerSrvSkeleton(SI_TimerSrvSkeleton const &) = delete;
  /*!
   * \brief Delete move constructor.
   */
  SI_TimerSrvSkeleton(SI_TimerSrvSkeleton &&) = delete;
  /*!
   * \brief Delete copy assignment.
   */
  SI_TimerSrvSkeleton &operator=(SI_TimerSrvSkeleton const &) & = delete;
  /*!
   * \brief Delete move assignment.
   */
  SI_TimerSrvSkeleton &operator=(SI_TimerSrvSkeleton &&) & = delete;

  /*!
   * \brief Constructor of SI_TimerSrvSkeleton.
   *
   * \remark Because of internal resource management strategy, all created skeletons shall be released before the Runtime
   * is destroyed; i.e. they cannot not be stored in variables with longer life period than the application's main().
   * If not followed, it's not guaranteed that the communication middleware is shut down properly and may lead to
   * segmentation fault.
   *
   * \param[in] instance The identifier of a specific instance of a service, needed to distinguish different instances of
   *                     exactly the same service in the system.
   *
   * \param[in] mode The mode of the service implpementation for processing service method invocations.
   *                 Default: Event-driven processing
   *
   * \pre No other service skeleton for the same instance must exist concurrently. After destruction of an already
   *      created skeleton object, the instantiation for the same service instance will be possible.
   *
   * \context App
   * \threadsafe FALSE
   * \reentrant FALSE
   * \vprivate Vector component internal API.
   * \synchronous TRUE
   * \trace SPEC-4980351
   * \trace SPEC-4980356
   */
   explicit SI_TimerSrvSkeleton(
      ara::com::InstanceIdentifier instance,
      ara::com::MethodCallProcessingMode mode = ara::com::MethodCallProcessingMode::kEvent) noexcept;

  /*!
   * \brief Constructor of SI_TimerSrvSkeleton.
   *
   * \remark Because of internal resource management strategy, all created skeletons shall be released before the Runtime
   * is destroyed; i.e. they cannot not be stored in variables with longer life period than the application's main().
   * If not followed, it's not guaranteed that the communication middleware is shut down properly and may lead to
   * segmentation fault.
   *
   * \param[in] instance The InstanceSpecifier of a specific service instance, needed to distinguish different instances
   *                     of exactly the same service in the system.
   *
   * \param[in] mode The mode of the service implementation for processing service method invocations.
   *                 Default: Event-driven processing
   *
   * \pre No other service skeleton for the same instance must exist concurrently. After destruction of an already
   *      created skeleton object, the instantiation for the same service instance will be possible.
   *
   * \context App
   * \threadsafe FALSE
   * \reentrant FALSE
   * \vprivate Vector component internal API.
   * \synchronous TRUE
   * \trace SPEC-8053552
   * \trace SPEC-4980356
   */
   explicit SI_TimerSrvSkeleton(
      ara::core::InstanceSpecifier instance,
      ara::com::MethodCallProcessingMode mode = ara::com::MethodCallProcessingMode::kEvent) noexcept;

  /*!
   * \brief Constructor of SI_TimerSrvSkeleton.
   *
   * \remark Because of internal resource management strategy, all created skeletons shall be released before the Runtime
   * is destroyed; i.e. they cannot not be stored in variables with longer life period than the application's main().
   * If not followed, it's not guaranteed that the communication middleware is shut down properly and may lead to
   * segmentation fault.
   *
   * \param[in] instance_identifiers The container of instances of a service, each instance element needed to
   *                                 distinguish different instances of exactly the same service in the system.
   *
   * \param[in] mode The mode of the service implementation for processing service method invocations.
   *                 Default: Event-driven processing
   *
   * \pre No other service skeleton for the same instance must exist concurrently. After destruction of an already
   *      created skeleton object, the instantiation for the same service instance will be possible.
   *
   * \context App
   * \threadsafe FALSE
   * \reentrant FALSE
   * \vprivate Vector component internal API.
   * \synchronous TRUE
   * \trace SPEC-8053554
   * \trace SPEC-4980356
   */
   explicit SI_TimerSrvSkeleton(
      ara::com::InstanceIdentifierContainer instance_identifiers,
      ara::com::MethodCallProcessingMode mode = ara::com::MethodCallProcessingMode::kEvent) noexcept;


  /*!
   * \brief Stops the service if it is currently offered.
   * \details This call will be blocked until all current method requests are finished/canceled.
   * \pre -
   * \context App
   * \vpublic
   * \synchronous TRUE
   * \trace SPEC-4980351
   */
  ~SI_TimerSrvSkeleton() noexcept override;

  /*!
   * \brief Type alias for ServiceDiscovery.
   */
  using ServiceDiscovery = ::amsr::socal::internal::service_discovery::ServiceDiscovery<SI_TimerSrvSkeleton>;

  /*!
   * \brief       Returns the service discovery singleton.
   * \return      Reference to service discovery singleton.
   * \pre         Service discovery has been registered via RegisterServiceDiscovery.
   * \context     ANY
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \vprivate
   * \synchronous TRUE
   */
  static ::amsr::generic::Singleton<ServiceDiscovery*>& GetServiceDiscovery() noexcept;

  /*!
   * \brief       Registers the service discovery.
   * \param[in]   service_discovery Pointer to service discovery.
   * \pre         -
   * \context     Init
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \vprivate
   * \synchronous TRUE
   */
  static void RegisterServiceDiscovery(ServiceDiscovery* service_discovery) noexcept;

  /*!
   * \brief       Deregisters the service discovery.
   * \pre         RegisterServiceDiscovery has been called.
   * \context     Shutdown
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \vprivate
   * \synchronous TRUE
   */
  static void DeRegisterServiceDiscovery() noexcept;

  // ---- Methods --------------------------------------------------------------------------------------------------

  /*!
   * \brief Provided implementation for service interface method 'TimerCncl'.
   *
   * \return ara::core::Future which gets resolved with the method output arguments or an ApApplicationError error code
   *         after processing has finished.
   *         Output arguments:
   *         - Response OUT parameter of type ::gwm::platform_timer::timer_idt::TimerSet_Response_Enum_Idt
   *
   * \pre        -
   * \context    Callback
   * \threadsafe TRUE if MethodCallProcessing mode is kEvent, FALSE otherwise.
   * \vpublic
   * \reentrant  TRUE if MethodCallProcessing mode is kEvent, FALSE otherwise.
   * \synchronous FALSE
   * \trace SPEC-4980355
   */
  virtual ara::core::Future<methods::TimerCncl::Output> TimerCncl() = 0;

  /*!
   * \brief Provided implementation for service interface method 'TimerReq'.
   *
   * \param[in] TimerSet IN parameter of type ::gwm::platform_timer::timer_idt::TimerSet_Integer_Idt
   * \return ara::core::Future which gets resolved with the method output arguments or an ApApplicationError error code
   *         after processing has finished.
   *         Output arguments:
   *         - Response OUT parameter of type ::gwm::platform_timer::timer_idt::TimerSet_Response_Enum_Idt
   *
   * \pre        -
   * \context    Callback
   * \threadsafe TRUE if MethodCallProcessing mode is kEvent, FALSE otherwise.
   * \vpublic
   * \reentrant  TRUE if MethodCallProcessing mode is kEvent, FALSE otherwise.
   * \synchronous FALSE
   * \trace SPEC-4980355
   */
  virtual ara::core::Future<methods::TimerReq::Output> TimerReq(::gwm::platform_timer::timer_idt::TimerSet_Integer_Idt const& TimerSet) = 0;

  // ---- Events ---------------------------------------------------------------------------------------------------

  // ---- Fields ---------------------------------------------------------------------------------------------------

  // VECTOR NC VectorC++-V11.0.2: MD_SOCAL_V11-0-2_public_member_in_generated_code
  /*!
   * \brief The skeleton field 'TimerSt' which can be used by application developer.
   * \details 
   * Data of type ::gwm::platform_timer::timer_idt::TimerSt_Struct_Idt
   * \vpublic
   */
  fields::TimerSt TimerSt;

 private:
  /*!
   * \brief Type alias for the base class.
   */
  using Base = ::amsr::socal::Skeleton<gwm::application_remsdl::timersrv_si::SI_TimerSrv, gwm::application_remsdl::timersrv_si::internal::SI_TimerSrvSkeletonImplInterface>;

  /*!
   * \copydoc amsr::socal::Skeleton<gwm::application_remsdl::timersrv_si::SI_TimerSrv,gwm::application_remsdl::timersrv_si::internal::SI_TimerSrvSkeletonImplInterface>::DoFieldInitializationChecks()
   */
  void DoFieldInitializationChecks() noexcept override;

  /*!
   * \copydoc amsr::socal::Skeleton<gwm::application_remsdl::timersrv_si::SI_TimerSrv,gwm::application_remsdl::timersrv_si::internal::SI_TimerSrvSkeletonImplInterface>::SendInitialFieldNotifications()
   */
  void SendInitialFieldNotifications() noexcept override;

  /*!
   * \copydoc amsr::socal::Skeleton<gwm::application_remsdl::timersrv_si::SI_TimerSrv,gwm::application_remsdl::timersrv_si::internal::SI_TimerSrvSkeletonImplInterface>::OfferServiceInternal()
   */
  void OfferServiceInternal(::ara::com::InstanceIdentifier const& instance_id) noexcept override;

  /*!
   * \copydoc amsr::socal::Skeleton<gwm::application_remsdl::timersrv_si::SI_TimerSrv,gwm::application_remsdl::timersrv_si::internal::SI_TimerSrvSkeletonImplInterface>::StopOfferServiceInternal()
   */
  void StopOfferServiceInternal(::ara::com::InstanceIdentifier const& instance_id) noexcept override;

  /*!
   * \brief The service discovery instance.
   */
  // VECTOR NC AutosarC++17_10-A3.3.2: MD_SOCAL_AutosarC++17_10-A3.3.2_StaticStorageDurationOfNonPODType
  static ::amsr::generic::Singleton<ServiceDiscovery*> sd_;
};  // class SI_TimerSrvSkeleton

}  // namespace skeleton
}  // namespace timersrv_si
}  // namespace application_remsdl
}  // namespace gwm

#endif  // TIMERSRVEXE_INCLUDE_GWM_APPLICATION_REMSDL_TIMERSRV_SI_SI_TIMERSRV_SKELETON_H_

