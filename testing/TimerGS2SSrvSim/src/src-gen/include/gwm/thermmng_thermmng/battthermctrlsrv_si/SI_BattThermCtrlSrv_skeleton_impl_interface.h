/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/include/gwm/thermmng_thermmng/battthermctrlsrv_si/SI_BattThermCtrlSrv_skeleton_impl_interface.h
 *        \brief  Skeleton implementation interface of service 'SI_BattThermCtrlSrv'.
 *
 *      \details  The service interface provides vehicle thermal control strategy level control
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_socal
 *         Commit ID: 209b3c0234013e84e3170ee826c9a8c6022de194
 *********************************************************************************************************************/

#ifndef DENPENDENCYSRVEXE_INCLUDE_GWM_THERMMNG_THERMMNG_BATTTHERMCTRLSRV_SI_SI_BATTTHERMCTRLSRV_SKELETON_IMPL_INTERFACE_H_
#define DENPENDENCYSRVEXE_INCLUDE_GWM_THERMMNG_THERMMNG_BATTTHERMCTRLSRV_SI_SI_BATTTHERMCTRLSRV_SKELETON_IMPL_INTERFACE_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "amsr/socal/internal/events/skeleton_event_manager_interface.h"
#include "gwm/thermmng_thermmng/battthermctrlsrv_si/si_battthermctrlsrv_common.h"

namespace gwm {
namespace thermmng_thermmng {
namespace battthermctrlsrv_si {
namespace internal {

/*!
 * \brief Skeleton implementation interface of service 'SI_BattThermCtrlSrv'
 */
class SI_BattThermCtrlSrvSkeletonImplInterface {
 public:
 /*!
   * \brief Define default constructor.
   * \pre -
   * \context App
   */
  SI_BattThermCtrlSrvSkeletonImplInterface() noexcept = default;

  /*!
   * \brief Use default destructor
   */
  virtual ~SI_BattThermCtrlSrvSkeletonImplInterface() noexcept = default;

 protected:

  /*!
   * \brief Use default move constructor
   * \pre -
   * \context App
   */
  SI_BattThermCtrlSrvSkeletonImplInterface(SI_BattThermCtrlSrvSkeletonImplInterface &&) noexcept = default;

  /*!
   * \brief Use default move assignment
   * \pre -
   * \context App
   */
  SI_BattThermCtrlSrvSkeletonImplInterface &operator=(SI_BattThermCtrlSrvSkeletonImplInterface &&) & noexcept = default;

  SI_BattThermCtrlSrvSkeletonImplInterface(SI_BattThermCtrlSrvSkeletonImplInterface const &) = delete;

  SI_BattThermCtrlSrvSkeletonImplInterface &operator=(SI_BattThermCtrlSrvSkeletonImplInterface const &) & = delete;

 public:

  // ---- Events ---------------------------------------------------------------------------------------------------

  // ---- Fields ---------------------------------------------------------------------------------------------------
};

} // namespace internal
}  // namespace battthermctrlsrv_si
}  // namespace thermmng_thermmng
}  // namespace gwm

#endif  // DENPENDENCYSRVEXE_INCLUDE_GWM_THERMMNG_THERMMNG_BATTTHERMCTRLSRV_SI_SI_BATTTHERMCTRLSRV_SKELETON_IMPL_INTERFACE_H_

