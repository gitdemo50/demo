/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/include/gwm/pt_powersys/vehchrgnctrlsrv_si/SI_VehChrgnCtrlSrv_skeleton_impl_interface.h
 *        \brief  Skeleton implementation interface of service 'SI_VehChrgnCtrlSrv'.
 *
 *      \details  The service interface provides vehicle charging control
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_socal
 *         Commit ID: 209b3c0234013e84e3170ee826c9a8c6022de194
 *********************************************************************************************************************/

#ifndef DENPENDENCYSRVEXE_INCLUDE_GWM_PT_POWERSYS_VEHCHRGNCTRLSRV_SI_SI_VEHCHRGNCTRLSRV_SKELETON_IMPL_INTERFACE_H_
#define DENPENDENCYSRVEXE_INCLUDE_GWM_PT_POWERSYS_VEHCHRGNCTRLSRV_SI_SI_VEHCHRGNCTRLSRV_SKELETON_IMPL_INTERFACE_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "amsr/socal/internal/events/skeleton_event_manager_interface.h"
#include "gwm/pt_powersys/vehchrgnctrlsrv_si/si_vehchrgnctrlsrv_common.h"

namespace gwm {
namespace pt_powersys {
namespace vehchrgnctrlsrv_si {
namespace internal {

/*!
 * \brief Skeleton implementation interface of service 'SI_VehChrgnCtrlSrv'
 */
class SI_VehChrgnCtrlSrvSkeletonImplInterface {
 public:
 /*!
   * \brief Define default constructor.
   * \pre -
   * \context App
   */
  SI_VehChrgnCtrlSrvSkeletonImplInterface() noexcept = default;

  /*!
   * \brief Use default destructor
   */
  virtual ~SI_VehChrgnCtrlSrvSkeletonImplInterface() noexcept = default;

 protected:

  /*!
   * \brief Use default move constructor
   * \pre -
   * \context App
   */
  SI_VehChrgnCtrlSrvSkeletonImplInterface(SI_VehChrgnCtrlSrvSkeletonImplInterface &&) noexcept = default;

  /*!
   * \brief Use default move assignment
   * \pre -
   * \context App
   */
  SI_VehChrgnCtrlSrvSkeletonImplInterface &operator=(SI_VehChrgnCtrlSrvSkeletonImplInterface &&) & noexcept = default;

  SI_VehChrgnCtrlSrvSkeletonImplInterface(SI_VehChrgnCtrlSrvSkeletonImplInterface const &) = delete;

  SI_VehChrgnCtrlSrvSkeletonImplInterface &operator=(SI_VehChrgnCtrlSrvSkeletonImplInterface const &) & = delete;

 public:

  // ---- Events ---------------------------------------------------------------------------------------------------

  // ---- Fields ---------------------------------------------------------------------------------------------------
};

} // namespace internal
}  // namespace vehchrgnctrlsrv_si
}  // namespace pt_powersys
}  // namespace gwm

#endif  // DENPENDENCYSRVEXE_INCLUDE_GWM_PT_POWERSYS_VEHCHRGNCTRLSRV_SI_SI_VEHCHRGNCTRLSRV_SKELETON_IMPL_INTERFACE_H_

