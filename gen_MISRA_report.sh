
#Set these variables
STATIC_BUILD_FOLDER=statictest
PYTHON_CMD=python3
PCLINT_LINUX_ROOT=~/work/pclint/linux/
PROJECT_ROOT=/home/sharaf/work/neb/WindowBasicService
ARA_STACK_INSTALL_PATH=/home/sharaf/work/stack/CBD2100373_D01/InstallR
GCC_PATH=$(which gcc)

if [ -d $STATIC_BUILD_FOLDER ]; then
   echo "Static test build folder exists. nothing doing"
else
   echo "creating build folder folder"
   mkdir $STATIC_BUILD_FOLDER
fi

cd $STATIC_BUILD_FOLDER

cat << EOM > patch.in
-wlib(4)
-wlib(1)
-elib(4846)
-elib(1511)
-e537

+libdir($ARA_STACK_INSTALL_PATH/include/)
+libdir($ARA_STACK_INSTALL_PATH/lib/cmake/Socal/.*)
+libdir($ARA_STACK_INSTALL_PATH/lib/.*)
+libdir($PROJECT_ROOT/src/src-gen/include/)
+libh(anytype.hpp)
+libh(shims.hpp)
+libh(logger.hpp)
+libh(gwmlogstream.hpp)
+libh(gwmloggertarget.hpp)
+libh(impl/defaultloggercontext.hpp)
+libh(workerthread.hpp)
-w1

EOM

echo "Generating compiler configuration"
$PYTHON_CMD $PCLINT_LINUX_ROOT/config/pclp_config.py --compiler=gcc --compiler-bin=$GCC_PATH --config-output-lnt-file=co-gcc.lnt --config-output-header-file=co-gcc.h --generate-compiler-config --compiler-cpp-options="-std=c++14"

echo "Generating command db using CMAKE"
cmake -DCMAKE_EXPORT_COMPILE_COMMANDS=ON -DMISRA_CONFIG_GEN=ON ..

echo "Generating project configuration"
$PYTHON_CMD $PCLINT_LINUX_ROOT/config/pclp_config.py --compiler=gcc --source-pattern=".*\.(c|cc|cpp)$" --compilation-db=compile_commands.json --config-output-lnt-file=project.in --generate-project-config

echo "Update project configuration"
cat patch.in project.in > project.lnt
rm project.in
rm patch.in

echo "Copy required files from pclint"
cp $PCLINT_LINUX_ROOT/lnt/env-html.js .

echo "Generating MISRA Error reports"
$PCLINT_LINUX_ROOT/pclp64_linux co-gcc.lnt  $PCLINT_LINUX_ROOT/lnt/env-html.lnt $PCLINT_LINUX_ROOT/lnt/au-misra-cpp.lnt project.lnt > MISRA_Error.html
