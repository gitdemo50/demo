#ifndef GWM_PLATFORM_LOGGER_IMPL_GWM_LOG_STREAN_HPP
#define GWM_PLATFORM_LOGGER_IMPL_GWM_LOG_STREAN_HPP

#include <sstream>

namespace gwm {
namespace platform {
namespace impl {

class GWMLogger;
enum GWMLogLevel : uint32_t;

class GWMLogStream {
public:
    GWMLogStream(GWMLogger& logger, GWMLogLevel logLevel) : logger(logger), logLevel(logLevel),
        stream() {

    }

    GWMLogStream(const GWMLogStream&) = delete;

    GWMLogStream(GWMLogStream&&) = default;
    ~GWMLogStream();

    template<typename T>
    GWMLogStream& operator << (const T& val) {
        stream << val;
        return *this;
    }

    void Flush();

private:
    GWMLogger& logger;
    GWMLogLevel logLevel;

    std::stringstream stream;
};

}
}
}

#endif
