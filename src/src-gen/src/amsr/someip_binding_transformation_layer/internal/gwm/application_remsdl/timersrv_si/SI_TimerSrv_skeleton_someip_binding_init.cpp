// VECTOR Same Line AutosarC++17_10-A1.1.1: MD_SOMEIPBINDING_AutosarC++17_10-A1.1.1_external_identifiers
/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  TimerSrvExe/src/amsr/someip_binding_transformation_layer/internal/gwm/application_remsdl/timersrv_si/SI_TimerSrv_skeleton_someip_binding_init.cpp
 *        \brief  Skeleton-side ara::com SOME/IP binding initialization for ServiceInterface 'SI_TimerSrv'
 *
 *      \details  Full ServiceInterface path: '/ServiceInterfaces/SI_TimerSrv'
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_someipbinding
 *         Commit ID: 43b33f0283d4d61385b98a3599688499abd86a75
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "amsr/someip_binding_transformation_layer/internal/gwm/application_remsdl/timersrv_si/SI_TimerSrv_skeleton_someip_binding_init.h"
#include "amsr/someip_binding_transformation_layer/internal/gwm/application_remsdl/timersrv_si/SI_TimerSrv_skeleton_someip_binding.h"
#include "ara/core/optional.h"

namespace amsr {
namespace someip_binding_transformation_layer {
namespace internal {

namespace gwm {
namespace application_remsdl {
namespace timersrv_si {

void AraComSomeIpBindingInitializeSkeletonSomeIpEventBackendsSI_TimerSrv(
    AraComSomeIpBindingSpecializationSkeleton::ServerManager& server_manager) {
  { // ServiceInstance: 0x1

    { // Field notifier: TimerSt

      // SOME/IP Skeleton event backend type for field 'TimerSt'.
      using SI_TimerSrvSkeletonSomeIpEventBackendTimerSt = ::amsr::someip_binding_transformation_layer::internal::SomeIpSkeletonEventBackend<SI_TimerSrvSkeletonSomeIpEventConfigurationTimerSt>;

      std::unique_ptr<SI_TimerSrvSkeletonSomeIpEventBackendTimerSt> event_backend{
        std::make_unique<SI_TimerSrvSkeletonSomeIpEventBackendTimerSt>(1U, server_manager)};
      gwm::application_remsdl::timersrv_si::SI_TimerSrvSkeletonSomeIpFieldNotifierTimerSt::EmplaceBackend(1U, std::move(event_backend));
    }
  }
}

void AraComSomeIpBindingDeInitializeSkeletonSomeIpEventBackendsSI_TimerSrv() {

  // Field notifier: TimerSt
  gwm::application_remsdl::timersrv_si::SI_TimerSrvSkeletonSomeIpFieldNotifierTimerSt::ClearBackendList();

}


}  // namespace timersrv_si
}  // namespace application_remsdl
}  // namespace gwm

}  // namespace internal
}  // namespace someip_binding_transformation_layer
}  // namespace amsr

