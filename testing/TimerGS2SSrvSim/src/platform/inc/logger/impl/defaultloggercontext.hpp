#ifndef GWM_PLATFORM_LOGGER_IMPL_DEFAULT_HPP
#define GWM_PLATFORM_LOGGER_IMPL_DEFAULT_HPP

#include <string>
#include <map>

#include "gwmlogger.hpp"

namespace gwm {
namespace platform {

using Logger = LoggerWrapper<impl::GWMLogger>;

class LoggerContext {
public:
    static LoggerContext& getInstance() {
        static LoggerContext loggerContext;

        return loggerContext;
    }

    void initialize() {

    }

    auto& createLogger(std::string name) {
        auto it = loggerTable.find(name);
    
        if(it == loggerTable.end()) {
            auto pair = loggerTable.emplace(std::piecewise_construct,
            std::forward_as_tuple(name),
            std::forward_as_tuple(name,impl::GWMLogLevel::LogVerbose));

            return pair.first->second;        
        } else {
            return it->second;
        }
    }

    LoggerContext(const LoggerContext&) = delete;
private:
    LoggerContext() {}

    std::map<std::string,impl::GWMLogger> loggerTable;
};

template<>
impl::GWMLogger& createLogger<impl::GWMLogger>(std::string name);

}
}

#endif
