/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/include/gwm/pt_powersys/obcstsrv_si/SI_OBCStSrv_skeleton_impl_interface.h
 *        \brief  Skeleton implementation interface of service 'SI_OBCStSrv'.
 *
 *      \details  This service interface provides OBC working status information
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_socal
 *         Commit ID: 209b3c0234013e84e3170ee826c9a8c6022de194
 *********************************************************************************************************************/

#ifndef DENPENDENCYSRVEXE_INCLUDE_GWM_PT_POWERSYS_OBCSTSRV_SI_SI_OBCSTSRV_SKELETON_IMPL_INTERFACE_H_
#define DENPENDENCYSRVEXE_INCLUDE_GWM_PT_POWERSYS_OBCSTSRV_SI_SI_OBCSTSRV_SKELETON_IMPL_INTERFACE_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "amsr/socal/internal/events/skeleton_event_manager_interface.h"
#include "gwm/pt_powersys/obcstsrv_si/si_obcstsrv_common.h"

namespace gwm {
namespace pt_powersys {
namespace obcstsrv_si {
namespace internal {

/*!
 * \brief Skeleton implementation interface of service 'SI_OBCStSrv'
 */
class SI_OBCStSrvSkeletonImplInterface {
 public:
 /*!
   * \brief Define default constructor.
   * \pre -
   * \context App
   */
  SI_OBCStSrvSkeletonImplInterface() noexcept = default;

  /*!
   * \brief Use default destructor
   */
  virtual ~SI_OBCStSrvSkeletonImplInterface() noexcept = default;

 protected:

  /*!
   * \brief Use default move constructor
   * \pre -
   * \context App
   */
  SI_OBCStSrvSkeletonImplInterface(SI_OBCStSrvSkeletonImplInterface &&) noexcept = default;

  /*!
   * \brief Use default move assignment
   * \pre -
   * \context App
   */
  SI_OBCStSrvSkeletonImplInterface &operator=(SI_OBCStSrvSkeletonImplInterface &&) & noexcept = default;

  SI_OBCStSrvSkeletonImplInterface(SI_OBCStSrvSkeletonImplInterface const &) = delete;

  SI_OBCStSrvSkeletonImplInterface &operator=(SI_OBCStSrvSkeletonImplInterface const &) & = delete;

 public:

  // ---- Events ---------------------------------------------------------------------------------------------------

  /*!
   * \brief Get the event manager object for the service event 'OBCActT'.
   * \details Event sample type: ::gwm::pt_powersys::powersys_idt::OBCActT_Float_Idt.
   * \return A binding-specific event management object/interface supporting event transmission.
   * \pre -
   * \context App
   */
  virtual ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_powersys::powersys_idt::OBCActT_Float_Idt>* GetEventManagerOBCActT() noexcept = 0;

  /*!
   * \brief Get the event manager object for the service event 'OBCChrgLim'.
   * \details Event sample type: ::gwm::pt_powersys::powersys_idt::OBCChrgLim_Struct_Idt.
   * \return A binding-specific event management object/interface supporting event transmission.
   * \pre -
   * \context App
   */
  virtual ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_powersys::powersys_idt::OBCChrgLim_Struct_Idt>* GetEventManagerOBCChrgLim() noexcept = 0;

  /*!
   * \brief Get the event manager object for the service event 'OBCDchrOutpI'.
   * \details Event sample type: ::gwm::pt_powersys::powersys_idt::OBCDchrOutpI_Float_Idt.
   * \return A binding-specific event management object/interface supporting event transmission.
   * \pre -
   * \context App
   */
  virtual ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_powersys::powersys_idt::OBCDchrOutpI_Float_Idt>* GetEventManagerOBCDchrOutpI() noexcept = 0;

  /*!
   * \brief Get the event manager object for the service event 'OBCDchrOutpU'.
   * \details Event sample type: ::gwm::pt_powersys::powersys_idt::OBCDchrOutpU_Float_Idt.
   * \return A binding-specific event management object/interface supporting event transmission.
   * \pre -
   * \context App
   */
  virtual ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_powersys::powersys_idt::OBCDchrOutpU_Float_Idt>* GetEventManagerOBCDchrOutpU() noexcept = 0;

  /*!
   * \brief Get the event manager object for the service event 'OBCInptAcI'.
   * \details Event sample type: ::gwm::pt_powersys::powersys_idt::OBCInptAcI_Float_Idt.
   * \return A binding-specific event management object/interface supporting event transmission.
   * \pre -
   * \context App
   */
  virtual ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_powersys::powersys_idt::OBCInptAcI_Float_Idt>* GetEventManagerOBCInptAcI() noexcept = 0;

  /*!
   * \brief Get the event manager object for the service event 'OBCInptAcU'.
   * \details Event sample type: ::gwm::pt_powersys::powersys_idt::OBCInptAcU_Float_Idt.
   * \return A binding-specific event management object/interface supporting event transmission.
   * \pre -
   * \context App
   */
  virtual ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_powersys::powersys_idt::OBCInptAcU_Float_Idt>* GetEventManagerOBCInptAcU() noexcept = 0;

  /*!
   * \brief Get the event manager object for the service event 'OBCOuptDcI'.
   * \details Event sample type: ::gwm::pt_powersys::powersys_idt::OBCOuptDcI_Float_Idt.
   * \return A binding-specific event management object/interface supporting event transmission.
   * \pre -
   * \context App
   */
  virtual ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_powersys::powersys_idt::OBCOuptDcI_Float_Idt>* GetEventManagerOBCOuptDcI() noexcept = 0;

  /*!
   * \brief Get the event manager object for the service event 'OBCOuptDcU'.
   * \details Event sample type: ::gwm::pt_powersys::powersys_idt::OBCOuptDcU_Float_Idt.
   * \return A binding-specific event management object/interface supporting event transmission.
   * \pre -
   * \context App
   */
  virtual ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_powersys::powersys_idt::OBCOuptDcU_Float_Idt>* GetEventManagerOBCOuptDcU() noexcept = 0;

  // ---- Fields ---------------------------------------------------------------------------------------------------

  /*!
   * \brief Get the event manager object for the field notifier of field 'OBCChrgGunSt'.
   * \details Field data type: ::gwm::pt_powersys::powersys_idt::OBCChrgGunSt_Struct_Idt.
   * \return A binding-specific event management object/interface supporting field notifier updates.
   * \pre -
   * \context App
   */
   virtual ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_powersys::powersys_idt::OBCChrgGunSt_Struct_Idt>* GetFieldNotifierOBCChrgGunSt() noexcept = 0;

  /*!
   * \brief Get the event manager object for the field notifier of field 'OBCErrSt'.
   * \details Field data type: ::gwm::pt_powersys::powersys_idt::OBCErrSt_Struct_Idt.
   * \return A binding-specific event management object/interface supporting field notifier updates.
   * \pre -
   * \context App
   */
   virtual ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_powersys::powersys_idt::OBCErrSt_Struct_Idt>* GetFieldNotifierOBCErrSt() noexcept = 0;

  /*!
   * \brief Get the event manager object for the field notifier of field 'OBCHVILSt'.
   * \details Field data type: ::gwm::pt_powersys::powersys_idt::OBCHVILSt_Enum_Idt.
   * \return A binding-specific event management object/interface supporting field notifier updates.
   * \pre -
   * \context App
   */
   virtual ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_powersys::powersys_idt::OBCHVILSt_Enum_Idt>* GetFieldNotifierOBCHVILSt() noexcept = 0;

  /*!
   * \brief Get the event manager object for the field notifier of field 'OBCSt'.
   * \details Field data type: ::gwm::pt_powersys::powersys_idt::OBCSt_Enum_Idt.
   * \return A binding-specific event management object/interface supporting field notifier updates.
   * \pre -
   * \context App
   */
   virtual ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_powersys::powersys_idt::OBCSt_Enum_Idt>* GetFieldNotifierOBCSt() noexcept = 0;

  /*!
   * \brief Get the event manager object for the field notifier of field 'OBCV2XSt'.
   * \details Field data type: ::gwm::pt_powersys::powersys_idt::OBCV2XSt_Enum_Idt.
   * \return A binding-specific event management object/interface supporting field notifier updates.
   * \pre -
   * \context App
   */
   virtual ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_powersys::powersys_idt::OBCV2XSt_Enum_Idt>* GetFieldNotifierOBCV2XSt() noexcept = 0;
};

} // namespace internal
}  // namespace obcstsrv_si
}  // namespace pt_powersys
}  // namespace gwm

#endif  // DENPENDENCYSRVEXE_INCLUDE_GWM_PT_POWERSYS_OBCSTSRV_SI_SI_OBCSTSRV_SKELETON_IMPL_INTERFACE_H_

