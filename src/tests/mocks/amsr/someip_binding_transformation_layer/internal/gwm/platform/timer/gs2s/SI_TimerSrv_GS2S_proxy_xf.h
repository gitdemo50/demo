/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  TimerSrvExe/include/amsr/someip_binding_transformation_layer/internal/gwm/platform/timer/gs2s/SI_TimerSrv_GS2S_proxy_xf.h
 *        \brief  SOME/IP binding of service proxy for service 'SI_TimerSrv_GS2S'
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_someipbinding
 *         Commit ID: 43b33f0283d4d61385b98a3599688499abd86a75
 *********************************************************************************************************************/

#ifndef TIMERSRVEXE_INCLUDE_AMSR_SOMEIP_BINDING_TRANSFORMATION_LAYER_INTERNAL_GWM_PLATFORM_TIMER_GS2S_SI_TIMERSRV_GS2S_PROXY_XF_H_
#define TIMERSRVEXE_INCLUDE_AMSR_SOMEIP_BINDING_TRANSFORMATION_LAYER_INTERNAL_GWM_PLATFORM_TIMER_GS2S_SI_TIMERSRV_GS2S_PROXY_XF_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include <memory>
#include "amsr/someip_binding/internal/thread_pool_interface.h"
#include "amsr/someip_binding_transformation_layer/internal/gwm/platform/timer/gs2s/SI_TimerSrv_GS2S_proxy_methods.h"
#include "amsr/someip_binding_transformation_layer/internal/gwm/platform/timer/gs2s/SI_TimerSrv_GS2S_proxy_someip_event_manager.h"
#include "gwm/platform/timer/gs2s/SI_TimerSrv_GS2S_proxy_backend_interface.h"
#include "gwm/platform/timer/gs2s/si_timersrv_gs2s_proxy.h"
#include "someip-protocol/internal/someip_message.h"
#include "someip_binding/internal/binding_client.h"
#include "someip_binding/internal/client_interface.h"
#include "someip_binding/internal/client_manager_interface.h"
#include "someip_binding/internal/logging/ara_com_logger.h"

namespace amsr {
namespace someip_binding_transformation_layer {
namespace internal {

namespace gwm {
namespace platform {
namespace timer {
namespace gs2s {

/*!
 * \brief SOME/IP proxy binding of Service 'SI_TimerSrv_GS2S'.
 * \details Handles serialization and deserialization of all method calls, events, etc.
 * \remark generated
 */
class SI_TimerSrv_GS2SProxyXf final :
  public ::gwm::platform::timer::gs2s::internal::SI_TimerSrv_GS2SProxyBackendInterface,
  public ::amsr::someip_binding::internal::ClientInterface,
  public std::enable_shared_from_this<SI_TimerSrv_GS2SProxyXf> {
 public:
  /*!
   * \brief Map used to contain events E2E protection Props.
   */
  using E2EPropsMap =
      std::map<::amsr::someip_protocol::internal::EventId, ::amsr::e2e::profiles::End2EndEventProtectionProps>;

  /*!
   * \brief Constructor.
   * \param[in] service_id       SOME/IP service identifier to be used.
   * \param[in] major_version    SOME/IP major version to be used.
   * \param[in] instance_id      SOME/IP instance identifier to be used.
   * \param[in] client_id        SOME/IP client identifier to be used.
   * \param[in] client_manager   Reference to the related SOME/IP transport binding manager.
   * \param[in] e2e_props_map    E2E Event properties for all E2E Events (can be empty if no E2E events are configured).
   * \pre -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  SI_TimerSrv_GS2SProxyXf(::amsr::someip_protocol::internal::ServiceId const service_id,
                                           ::amsr::someip_protocol::internal::MajorVersion const major_version,
                                           ::amsr::someip_protocol::internal::InstanceId const instance_id,
                                           ::amsr::someip_protocol::internal::ClientId const client_id,
                                           ::amsr::someip_binding::internal::ClientManagerInterface& client_manager,
                                           ::amsr::someip_binding::internal::ThreadPoolInterface& thread_pool_interface,
                                           E2EPropsMap const& e2e_props_map);

  /*!
   * \brief Destructor.
   * \pre -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR NC AutosarC++17_10-A15.4.2: MD_SOMEIPBINDING_AutosarC++17_10-A15.4.2_A15.5.1_A15.5.2_A15.5.3_GoogleTest
  // VECTOR NC AutosarC++17_10-A15.5.1: MD_SOMEIPBINDING_AutosarC++17_10-A15.4.2_A15.5.1_A15.5.2_A15.5.3_GoogleTest
  // VECTOR NC AutosarC++17_10-A15.5.2: MD_SOMEIPBINDING_AutosarC++17_10-A15.4.2_A15.5.1_A15.5.2_A15.5.3_GoogleTest
  // VECTOR NC AutosarC++17_10-A15.5.3: MD_SOMEIPBINDING_AutosarC++17_10-A15.4.2_A15.5.1_A15.5.2_A15.5.3_GoogleTest
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  ~SI_TimerSrv_GS2SProxyXf() noexcept override;

  /*!
   * \brief Receive handler for method responses.
   * \param[in] header The deserialized SOME/IP header for direct access.
   * \param[in] packet Serialized Method response [SOME/IP header + Payload].
   *
   * \details At the point of this method call from SomeIpBinding, we only have to dispatch method id and session
   *          id and look in the correct pending request container, to serve the promise.
   * \error ::ara::com::ComErrc::service_not_available is set in the promise and then thrown when future.get() is called
   *        if SOME/IP return code was kUnknownService.
   * \pre         -
   * \context     Reactor
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  void HandleMethodResponse(::amsr::someip_protocol::internal::SomeIpMessageHeader const& header,
                            ::amsr::someip_protocol::internal::SomeIpMessage packet) override;

  /*!
   * \brief Callback triggered when the service is down.
   *
   * \pre         -
   * \context     Reactor
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  void OnServiceDown() override;

  // ---- Methods --------------------------------------------------------------------------------------------------

  /*!
   * \brief Invokes request response method TimerReq.
    * \param[in] TimerSet IN parameter of type ::datatypes::gs2s::timersrv_gs2s::TimerSet_Integer
    * \param[in] StartPoint IN parameter of type ::datatypes::gs2s::timersrv_gs2s::StartPoint_Enum
    * \param[in] TimerType IN parameter of type ::datatypes::gs2s::timersrv_gs2s::TimerType_Enum
    * \param[in] SleepType IN parameter of type ::datatypes::gs2s::timersrv_gs2s::SleepType_Enum
   * \return ara::core::Future with method output data element.
   * \pre -
   * \context       App
   * \synchronous   FALSE
   */
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  // VECTOR NC AutosarC++17_10-A15.4.2: MD_SOMEIPBINDING_AutosarC++17_10-A15.4.2_A15.5.1_A15.5.2_A15.5.3_GoogleTest
  // VECTOR NC AutosarC++17_10-A15.5.3: MD_SOMEIPBINDING_AutosarC++17_10-A15.4.2_A15.5.1_A15.5.2_A15.5.3_GoogleTest
  ara::core::Future<::gwm::platform::timer::gs2s::proxy::methods::TimerReq::Output> HandleMethodTimerReq(::datatypes::gs2s::timersrv_gs2s::TimerSet_Integer const& TimerSet, ::datatypes::gs2s::timersrv_gs2s::StartPoint_Enum const& StartPoint, ::datatypes::gs2s::timersrv_gs2s::TimerType_Enum const& TimerType, ::datatypes::gs2s::timersrv_gs2s::SleepType_Enum const& SleepType) noexcept override {
    // Build struct with all input arguments
    ::gwm::platform::timer::gs2s::internal::methods::TimerReq::Input const input_struct{TimerSet, StartPoint, TimerType, SleepType};
    return method_TimerReq_.HandleMethodRequest(input_struct);
  }


  // ---- Events ---------------------------------------------------------------------------------------------------

  // ---- Fields ---------------------------------------------------------------------------------------------------

  // ---- Field 'TimerSt' ----
  /*!
   * \brief Get the field notifier object for the service field 'TimerSt'
   * \return A proxy event object supporting event sample and subscription.
   * \pre -
   * \context App | Reactor | Callback
   * \synchronus TRUE
   */
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  ::amsr::socal::internal::events::ProxyEventBackendInterface<::datatypes::gs2s::timersrv_gs2s::TimerSt_Struct>& GetFieldNotifierBackendTimerSt() noexcept override {
    return field_notifier_xf_TimerSt_;
  }

  /*!
   * \brief Getter for the field 'TimerSt'
   * \return ara::core::Future with Field data element.
   * \pre -
   * \context App
   * \synchronous FALSE
   */
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  // VECTOR NC AutosarC++17_10-A15.4.2: MD_SOMEIPBINDING_AutosarC++17_10-A15.4.2_A15.5.1_A15.5.2_A15.5.3_GoogleTest
  // VECTOR NC AutosarC++17_10-A15.5.3: MD_SOMEIPBINDING_AutosarC++17_10-A15.4.2_A15.5.1_A15.5.2_A15.5.3_GoogleTest
  ara::core::Future<::gwm::platform::timer::gs2s::proxy::fields::TimerSt::FieldType> HandleFieldTimerStMethodGet() noexcept override {
    ::gwm::platform::timer::gs2s::internal::fields::TimerStGet::Input const input_struct{};
    return field_manager_TimerSt_get_.HandleMethodRequest(input_struct);
  }


 private:

  // SOME/IP service ID used by this binding.
  ::amsr::someip_protocol::internal::ServiceId  service_id_;

  // SOME/IP major version used by this binding.
  ::amsr::someip_protocol::internal::MajorVersion major_version_;

  // SOME/IP instance ID used by this binding.
  ::amsr::someip_protocol::internal::InstanceId instance_id_;

  // SOME/IP related client ID used by this concrete proxy instance.
  ::amsr::someip_protocol::internal::ClientId client_id_;

  // Related SOME/IP binding
  ::amsr::someip_binding::internal::ClientManagerInterface& client_manager_;

  // Reference to the thread pool interface.
  ::amsr::someip_binding::internal::ThreadPoolInterface& thread_pool_interface_;

  // Logger for tracing and debugging
  ::amsr::someip_binding::internal::logging::AraComLogger logger_{
    ::amsr::someip_binding::internal::logging::kSomeIpLoggerContextId,
    ::amsr::someip_binding::internal::logging::kSomeIpLoggerContextDescription, "SI_TimerSrv_GS2SProxyXf"};

  // ---- Methods ------------------------------------------------------------------------------------------

  // Method request/response class for proxy method 'TimerReq'
  methods::ProxyTimerReq method_TimerReq_{client_manager_, service_id_, instance_id_, major_version_, client_id_, methods::timerreq::ProxyConfiguration::kMethodId, methods::timerreq::ProxyConfiguration::kName, thread_pool_interface_};

  // ---- Event manager --------------------------------------------------------------------------------------------
  /*!
   * \brief Map containing E2E Props for all E2E Events.
   *        If this Proxy does not required any E2E Events, the map will be empty.
   */
  E2EPropsMap const& e2e_props_map_;

  // ---- Field notifiers ------------------------------------------------------------------------------------------

  // Event Xf for proxy field notifier 'TimerSt'
  SI_TimerSrv_GS2SProxySomeIpEventBackendTimerSt field_notifier_xf_TimerSt_{instance_id_, client_manager_};

  // ---- Field setters / getters  ---------------------------------------------------------------------------------
  // Field method manager for method Get of proxy field 'TimerSt' */
  fields::ProxyTimerStGet field_manager_TimerSt_get_{client_manager_, service_id_, instance_id_, major_version_, client_id_, fields::timerstget::ProxyConfiguration::kMethodId, fields::timerstget::ProxyConfiguration::kName, thread_pool_interface_};


  /*!
   * \brief Client object used for the real SOME/IP communication (currently only used for reception).
   */
  std::shared_ptr<::amsr::someip_binding::internal::BindingClient> client_{std::make_shared<::amsr::someip_binding::internal::BindingClient>(*this)};
};

}  // namespace internal
}  // namespace someip_binding_transformation_layer
}  // namespace amsr
}  // namespace gs2s
}  // namespace timer
}  // namespace platform
}  // namespace gwm

#endif  // TIMERSRVEXE_INCLUDE_AMSR_SOMEIP_BINDING_TRANSFORMATION_LAYER_INTERNAL_GWM_PLATFORM_TIMER_GS2S_SI_TIMERSRV_GS2S_PROXY_XF_H_

