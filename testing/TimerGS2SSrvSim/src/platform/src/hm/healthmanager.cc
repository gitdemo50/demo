
#include "hm/healthmanager.hpp"

namespace gwm {
namespace platform {
namespace hm {

HealthManager::HealthManager() : cyclicCheckPointEnabled(false), cyclicCheckPointMs(10000),
    initializedFlag(false), tm{nullptr}, fd(-1), wd(-1), aliveCheckPoint{}, validAliveCP(false), aliveSE{phm},
    logger(gwm::platform::Logger("HealthManager")) {
    
}

void HealthManager::start() {
    initialize();
}

}}}