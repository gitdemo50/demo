/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/include/amsr/someip_protocol/internal/datatype_serialization/gwm/pt_batterypacksys/batterypacksys_idt/serializer_CellMaxU_Struct_Idt.h
 *        \brief  SOME/IP protocol serializer implementation for datatype 'CellMaxU_Struct_Idt'
 *
 *      \details  /DataTypes/ImplementationDataTypes/CellMaxU_Struct_Idt
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_someipprotocol
 *         Commit ID: 0e137b9e6356987a6d2839c8d79c286219b68d60
 *********************************************************************************************************************/

#ifndef DENPENDENCYSRVEXE_INCLUDE_AMSR_SOMEIP_PROTOCOL_INTERNAL_DATATYPE_SERIALIZATION_GWM_PT_BATTERYPACKSYS_BATTERYPACKSYS_IDT_SERIALIZER_CELLMAXU_STRUCT_IDT_H_
#define DENPENDENCYSRVEXE_INCLUDE_AMSR_SOMEIP_PROTOCOL_INTERNAL_DATATYPE_SERIALIZATION_GWM_PT_BATTERYPACKSYS_BATTERYPACKSYS_IDT_SERIALIZER_CELLMAXU_STRUCT_IDT_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "gwm/pt_batterypacksys/batterypacksys_idt/impl_type_cellmaxu_struct_idt.h"
#include "someip-protocol/internal/serialization/ser_common.h"
#include "someip-protocol/internal/serialization/ser_forward.h"
#include "someip-protocol/internal/serialization/ser_sizing.h"
#include "someip-protocol/internal/serialization/writer.h"

namespace gwm {
namespace pt_batterypacksys {
namespace batterypacksys_idt {

/*!
 * \brief Serializer for datatype /DataTypes/ImplementationDataTypes/CellMaxU_Struct_Idt.
 * \details Data type class: DataTypeStruct (CppImplDataTypeStruct).
 *          is TLV struct: false
 *
 * \tparam TpPack Transformation properties to be used for serialization.
 *
 * \param[in, out] writer Reference to byte stream writer.
 * \param[in] data Reference to the data object of type /DataTypes/ImplementationDataTypes/CellMaxU_Struct_Idt to be serialized.
 * \pre The writer holds a memory buffer which is enough to store the serialized data.
 * \context Reactor|App
 * \threadsafe FALSE
 * \reentrant TRUE
 * \vprivate Vector component internal API.
 * \synchronous TRUE
 */
template <typename TpPack>
void SomeIpProtocolSerialize(amsr::someip_protocol::internal::serialization::Writer& writer, ::gwm::pt_batterypacksys::batterypacksys_idt::CellMaxU_Struct_Idt const &data) noexcept {
  // Namespace alias for static serialization code
  namespace serialization = amsr::someip_protocol::internal::serialization;
  // Serialize non-TLV struct
    // Serialize struct member 'Voltage_Idt' of type /AUTOSAR/StdTypes/float
    serialization::SomeIpProtocolSerialize<
                        TpPack,
                        // Byte-order of primitive datatype (/AUTOSAR/StdTypes/float)
                        typename serialization::Tp<TpPack>::ByteOrder

                        >(writer, data.Voltage_Idt);
    // Serialize struct member 'PosnNum_Idt' of type /AUTOSAR/StdTypes/uint8_t
    serialization::SomeIpProtocolSerialize<
                        TpPack,
                        // Byte-order of primitive datatype (/AUTOSAR/StdTypes/uint8_t)
                        typename serialization::Tp<TpPack>::ByteOrder

                        >(writer, data.PosnNum_Idt);
}

/*!
 * \brief Calculates the required buffer size for datatype /DataTypes/ImplementationDataTypes/CellMaxU_Struct_Idt.
 * \details Data type class: DataTypeStruct (CppImplDataTypeStruct).
 *          is TLV struct: false
 *
 * \tparam TpPack Transformation properties to be used for serialization.
 * \param[in] data Reference to the data object of type /DataTypes/ImplementationDataTypes/CellMaxU_Struct_Idt.
 * \return Returns the required buffer size for the struct in bytes.
 * \pre -
 * \context Reactor|App
 * \threadsafe FALSE
 * \reentrant TRUE
 * \vprivate Vector component internal API.
 * \synchronous TRUE
 */
template <typename TpPack>
constexpr std::size_t GetRequiredBufferSize(::gwm::pt_batterypacksys::batterypacksys_idt::CellMaxU_Struct_Idt const &data) noexcept {
  // Namespace alias for static serialization code
  namespace serialization = amsr::someip_protocol::internal::serialization;

  // Sum of required size
  std::size_t required_buffer_size{0};

  // Accumulate the static size of struct member 'Voltage_Idt' of type /AUTOSAR/StdTypes/float
  required_buffer_size += serialization::GetRequiredBufferSize<
                        TpPack,
                        // Byte-order of primitive datatype (/AUTOSAR/StdTypes/float)
                        typename serialization::Tp<TpPack>::ByteOrder

                        >(data.Voltage_Idt);
  // Accumulate the static size of struct member 'PosnNum_Idt' of type /AUTOSAR/StdTypes/uint8_t
  required_buffer_size += serialization::GetRequiredBufferSize<
                        TpPack,
                        // Byte-order of primitive datatype (/AUTOSAR/StdTypes/uint8_t)
                        typename serialization::Tp<TpPack>::ByteOrder

                        >(data.PosnNum_Idt);

  return required_buffer_size;
}

/*!
 * \brief Checks if datatype /DataTypes/ImplementationDataTypes/CellMaxU_Struct_Idt is of static size.
 * \details Data type class: DataTypeStruct (CppImplDataTypeStruct).
 *          is TLV struct: false
 *
 * \tparam TpPack Transformation properties to be used for serialization.
 * \return True if datatype has static size, false otherwise.
 * \pre -
 * \context Reactor|App
 * \threadsafe FALSE
 * \reentrant TRUE
 * \vprivate Vector component internal API.
 * \synchronous TRUE
 */
template <typename TpPack>
constexpr bool IsStaticSize(amsr::someip_protocol::internal::serialization::SizeToken<::gwm::pt_batterypacksys::batterypacksys_idt::CellMaxU_Struct_Idt>) noexcept {
  // Namespace alias for static serialization code
  namespace serialization = amsr::someip_protocol::internal::serialization;

  constexpr bool is_static_size{
  // Check static size status of struct member 'Voltage_Idt' of type /AUTOSAR/StdTypes/float
  serialization::IsStaticSize<
                      TpPack,
                      // Byte-order of primitive datatype (/AUTOSAR/StdTypes/float)
                      typename serialization::Tp<TpPack>::ByteOrder

                      >(serialization::SizeToken<::gwm::pt_batterypacksys::batterypacksys_idt::CellMinU_Voltage_Float_Idt>{})  && 
  
  // Check static size status of struct member 'PosnNum_Idt' of type /AUTOSAR/StdTypes/uint8_t
  serialization::IsStaticSize<
                      TpPack,
                      // Byte-order of primitive datatype (/AUTOSAR/StdTypes/uint8_t)
                      typename serialization::Tp<TpPack>::ByteOrder

                      >(serialization::SizeToken<::gwm::pt_batterypacksys::batterypacksys_idt::CellMinT_PosnNum_Integer_Idt>{}) 
  };
    return is_static_size;
}

}  // namespace batterypacksys_idt
}  // namespace pt_batterypacksys
}  // namespace gwm

#endif  // DENPENDENCYSRVEXE_INCLUDE_AMSR_SOMEIP_PROTOCOL_INTERNAL_DATATYPE_SERIALIZATION_GWM_PT_BATTERYPACKSYS_BATTERYPACKSYS_IDT_SERIALIZER_CELLMAXU_STRUCT_IDT_H_

