/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/src/amsr/someip_binding_transformation_layer/internal/gwm/pt_batterypacksys/bmsstsrv_si/SI_BmsStSrv_skeleton_someip_event_manager.cpp
 *        \brief  SOME/IP skeleton event handling for events and field notifications of service 'SI_BmsStSrv'
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_someipbinding
 *         Commit ID: 43b33f0283d4d61385b98a3599688499abd86a75
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "amsr/someip_binding_transformation_layer/internal/gwm/pt_batterypacksys/bmsstsrv_si/SI_BmsStSrv_skeleton_someip_event_manager.h"

namespace amsr {
namespace someip_binding_transformation_layer {
namespace internal {
namespace gwm {
namespace pt_batterypacksys {
namespace bmsstsrv_si {


// ---- Event 'BMSChrgTi' -------------------------------------------

/*!
 * \brief Definition of the service ID of the skeleton event configuration for event 'BMSChrgTi'.
 */
::amsr::someip_protocol::internal::ServiceId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSChrgTi::kServiceId;

/*!
 * \brief Definition of the event ID of the skeleton event configuration for event 'BMSChrgTi'.
 */
::amsr::someip_protocol::internal::EventId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSChrgTi::kEventId;

// ---- Event 'BMSConDchrMaxPwr' -------------------------------------------

/*!
 * \brief Definition of the service ID of the skeleton event configuration for event 'BMSConDchrMaxPwr'.
 */
::amsr::someip_protocol::internal::ServiceId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSConDchrMaxPwr::kServiceId;

/*!
 * \brief Definition of the event ID of the skeleton event configuration for event 'BMSConDchrMaxPwr'.
 */
::amsr::someip_protocol::internal::EventId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSConDchrMaxPwr::kEventId;

// ---- Event 'BMSConFBMaxPwr' -------------------------------------------

/*!
 * \brief Definition of the service ID of the skeleton event configuration for event 'BMSConFBMaxPwr'.
 */
::amsr::someip_protocol::internal::ServiceId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSConFBMaxPwr::kServiceId;

/*!
 * \brief Definition of the event ID of the skeleton event configuration for event 'BMSConFBMaxPwr'.
 */
::amsr::someip_protocol::internal::EventId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSConFBMaxPwr::kEventId;

// ---- Event 'BMSPlsDchrMaxPwr' -------------------------------------------

/*!
 * \brief Definition of the service ID of the skeleton event configuration for event 'BMSPlsDchrMaxPwr'.
 */
::amsr::someip_protocol::internal::ServiceId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSPlsDchrMaxPwr::kServiceId;

/*!
 * \brief Definition of the event ID of the skeleton event configuration for event 'BMSPlsDchrMaxPwr'.
 */
::amsr::someip_protocol::internal::EventId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSPlsDchrMaxPwr::kEventId;

// ---- Event 'BMSPlsFBMaxPwr' -------------------------------------------

/*!
 * \brief Definition of the service ID of the skeleton event configuration for event 'BMSPlsFBMaxPwr'.
 */
::amsr::someip_protocol::internal::ServiceId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSPlsFBMaxPwr::kServiceId;

/*!
 * \brief Definition of the event ID of the skeleton event configuration for event 'BMSPlsFBMaxPwr'.
 */
::amsr::someip_protocol::internal::EventId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSPlsFBMaxPwr::kEventId;

// ---- Event 'BmsInletCooltT' -------------------------------------------

/*!
 * \brief Definition of the service ID of the skeleton event configuration for event 'BmsInletCooltT'.
 */
::amsr::someip_protocol::internal::ServiceId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationBmsInletCooltT::kServiceId;

/*!
 * \brief Definition of the event ID of the skeleton event configuration for event 'BmsInletCooltT'.
 */
::amsr::someip_protocol::internal::EventId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationBmsInletCooltT::kEventId;

// ---- Event 'BmsIsoR' -------------------------------------------

/*!
 * \brief Definition of the service ID of the skeleton event configuration for event 'BmsIsoR'.
 */
::amsr::someip_protocol::internal::ServiceId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationBmsIsoR::kServiceId;

/*!
 * \brief Definition of the event ID of the skeleton event configuration for event 'BmsIsoR'.
 */
::amsr::someip_protocol::internal::EventId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationBmsIsoR::kEventId;

// ---- Event 'BmsOutletCooltT' -------------------------------------------

/*!
 * \brief Definition of the service ID of the skeleton event configuration for event 'BmsOutletCooltT'.
 */
::amsr::someip_protocol::internal::ServiceId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationBmsOutletCooltT::kServiceId;

/*!
 * \brief Definition of the event ID of the skeleton event configuration for event 'BmsOutletCooltT'.
 */
::amsr::someip_protocol::internal::EventId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationBmsOutletCooltT::kEventId;

// ---- Event 'CellMaxT' -------------------------------------------

/*!
 * \brief Definition of the service ID of the skeleton event configuration for event 'CellMaxT'.
 */
::amsr::someip_protocol::internal::ServiceId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationCellMaxT::kServiceId;

/*!
 * \brief Definition of the event ID of the skeleton event configuration for event 'CellMaxT'.
 */
::amsr::someip_protocol::internal::EventId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationCellMaxT::kEventId;

// ---- Event 'CellMaxU' -------------------------------------------

/*!
 * \brief Definition of the service ID of the skeleton event configuration for event 'CellMaxU'.
 */
::amsr::someip_protocol::internal::ServiceId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationCellMaxU::kServiceId;

/*!
 * \brief Definition of the event ID of the skeleton event configuration for event 'CellMaxU'.
 */
::amsr::someip_protocol::internal::EventId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationCellMaxU::kEventId;

// ---- Event 'CellMinT' -------------------------------------------

/*!
 * \brief Definition of the service ID of the skeleton event configuration for event 'CellMinT'.
 */
::amsr::someip_protocol::internal::ServiceId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationCellMinT::kServiceId;

/*!
 * \brief Definition of the event ID of the skeleton event configuration for event 'CellMinT'.
 */
::amsr::someip_protocol::internal::EventId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationCellMinT::kEventId;

// ---- Event 'CellMinU' -------------------------------------------

/*!
 * \brief Definition of the service ID of the skeleton event configuration for event 'CellMinU'.
 */
::amsr::someip_protocol::internal::ServiceId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationCellMinU::kServiceId;

/*!
 * \brief Definition of the event ID of the skeleton event configuration for event 'CellMinU'.
 */
::amsr::someip_protocol::internal::EventId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationCellMinU::kEventId;

// ---- Event 'HVBattDispySoc' -------------------------------------------

/*!
 * \brief Definition of the service ID of the skeleton event configuration for event 'HVBattDispySoc'.
 */
::amsr::someip_protocol::internal::ServiceId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationHVBattDispySoc::kServiceId;

/*!
 * \brief Definition of the event ID of the skeleton event configuration for event 'HVBattDispySoc'.
 */
::amsr::someip_protocol::internal::EventId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationHVBattDispySoc::kEventId;

// ---- Event 'HVBattI' -------------------------------------------

/*!
 * \brief Definition of the service ID of the skeleton event configuration for event 'HVBattI'.
 */
::amsr::someip_protocol::internal::ServiceId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationHVBattI::kServiceId;

/*!
 * \brief Definition of the event ID of the skeleton event configuration for event 'HVBattI'.
 */
::amsr::someip_protocol::internal::EventId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationHVBattI::kEventId;

// ---- Event 'HVBattInnerSoc' -------------------------------------------

/*!
 * \brief Definition of the service ID of the skeleton event configuration for event 'HVBattInnerSoc'.
 */
::amsr::someip_protocol::internal::ServiceId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationHVBattInnerSoc::kServiceId;

/*!
 * \brief Definition of the event ID of the skeleton event configuration for event 'HVBattInnerSoc'.
 */
::amsr::someip_protocol::internal::EventId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationHVBattInnerSoc::kEventId;

// ---- Event 'HVBattSoh' -------------------------------------------

/*!
 * \brief Definition of the service ID of the skeleton event configuration for event 'HVBattSoh'.
 */
::amsr::someip_protocol::internal::ServiceId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationHVBattSoh::kServiceId;

/*!
 * \brief Definition of the event ID of the skeleton event configuration for event 'HVBattSoh'.
 */
::amsr::someip_protocol::internal::EventId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationHVBattSoh::kEventId;

// ---- Event 'HVBattTotEgy' -------------------------------------------

/*!
 * \brief Definition of the service ID of the skeleton event configuration for event 'HVBattTotEgy'.
 */
::amsr::someip_protocol::internal::ServiceId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationHVBattTotEgy::kServiceId;

/*!
 * \brief Definition of the event ID of the skeleton event configuration for event 'HVBattTotEgy'.
 */
::amsr::someip_protocol::internal::EventId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationHVBattTotEgy::kEventId;

// ---- Event 'HVBattU' -------------------------------------------

/*!
 * \brief Definition of the service ID of the skeleton event configuration for event 'HVBattU'.
 */
::amsr::someip_protocol::internal::ServiceId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationHVBattU::kServiceId;

/*!
 * \brief Definition of the event ID of the skeleton event configuration for event 'HVBattU'.
 */
::amsr::someip_protocol::internal::EventId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationHVBattU::kEventId;

// ---- Event 'ModulAvrgT' -------------------------------------------

/*!
 * \brief Definition of the service ID of the skeleton event configuration for event 'ModulAvrgT'.
 */
::amsr::someip_protocol::internal::ServiceId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationModulAvrgT::kServiceId;

/*!
 * \brief Definition of the event ID of the skeleton event configuration for event 'ModulAvrgT'.
 */
::amsr::someip_protocol::internal::EventId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationModulAvrgT::kEventId;

// ---- Field notifier 'BMSChrgSt' -------------------------------------------

/*!
 * \brief Definition of the service ID of the skeleton event configuration for event 'BMSChrgSt'.
 */
::amsr::someip_protocol::internal::ServiceId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSChrgSt::kServiceId;

/*!
 * \brief Definition of the event ID of the skeleton event configuration for event 'BMSChrgSt'.
 */
::amsr::someip_protocol::internal::EventId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSChrgSt::kEventId;

// ---- Field notifier 'BMSDCChrgCnctSt' -------------------------------------------

/*!
 * \brief Definition of the service ID of the skeleton event configuration for event 'BMSDCChrgCnctSt'.
 */
::amsr::someip_protocol::internal::ServiceId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSDCChrgCnctSt::kServiceId;

/*!
 * \brief Definition of the event ID of the skeleton event configuration for event 'BMSDCChrgCnctSt'.
 */
::amsr::someip_protocol::internal::EventId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSDCChrgCnctSt::kEventId;

// ---- Field notifier 'BMSDCChrgEndReas' -------------------------------------------

/*!
 * \brief Definition of the service ID of the skeleton event configuration for event 'BMSDCChrgEndReas'.
 */
::amsr::someip_protocol::internal::ServiceId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSDCChrgEndReas::kServiceId;

/*!
 * \brief Definition of the event ID of the skeleton event configuration for event 'BMSDCChrgEndReas'.
 */
::amsr::someip_protocol::internal::EventId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSDCChrgEndReas::kEventId;

// ---- Field notifier 'BMSDchrgPrmsSt' -------------------------------------------

/*!
 * \brief Definition of the service ID of the skeleton event configuration for event 'BMSDchrgPrmsSt'.
 */
::amsr::someip_protocol::internal::ServiceId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSDchrgPrmsSt::kServiceId;

/*!
 * \brief Definition of the event ID of the skeleton event configuration for event 'BMSDchrgPrmsSt'.
 */
::amsr::someip_protocol::internal::EventId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSDchrgPrmsSt::kEventId;

// ---- Field notifier 'BMSHeatRunawaySt' -------------------------------------------

/*!
 * \brief Definition of the service ID of the skeleton event configuration for event 'BMSHeatRunawaySt'.
 */
::amsr::someip_protocol::internal::ServiceId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSHeatRunawaySt::kServiceId;

/*!
 * \brief Definition of the event ID of the skeleton event configuration for event 'BMSHeatRunawaySt'.
 */
::amsr::someip_protocol::internal::EventId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSHeatRunawaySt::kEventId;

// ---- Field notifier 'BMSInnerSOCSt' -------------------------------------------

/*!
 * \brief Definition of the service ID of the skeleton event configuration for event 'BMSInnerSOCSt'.
 */
::amsr::someip_protocol::internal::ServiceId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSInnerSOCSt::kServiceId;

/*!
 * \brief Definition of the event ID of the skeleton event configuration for event 'BMSInnerSOCSt'.
 */
::amsr::someip_protocol::internal::EventId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSInnerSOCSt::kEventId;

// ---- Field notifier 'BMSIntelTempMgtSt' -------------------------------------------

/*!
 * \brief Definition of the service ID of the skeleton event configuration for event 'BMSIntelTempMgtSt'.
 */
::amsr::someip_protocol::internal::ServiceId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSIntelTempMgtSt::kServiceId;

/*!
 * \brief Definition of the event ID of the skeleton event configuration for event 'BMSIntelTempMgtSt'.
 */
::amsr::someip_protocol::internal::EventId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSIntelTempMgtSt::kEventId;

// ---- Field notifier 'BMSLowTempSt' -------------------------------------------

/*!
 * \brief Definition of the service ID of the skeleton event configuration for event 'BMSLowTempSt'.
 */
::amsr::someip_protocol::internal::ServiceId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSLowTempSt::kServiceId;

/*!
 * \brief Definition of the event ID of the skeleton event configuration for event 'BMSLowTempSt'.
 */
::amsr::someip_protocol::internal::EventId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSLowTempSt::kEventId;

// ---- Field notifier 'BMSRmtPreHeatSt' -------------------------------------------

/*!
 * \brief Definition of the service ID of the skeleton event configuration for event 'BMSRmtPreHeatSt'.
 */
::amsr::someip_protocol::internal::ServiceId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSRmtPreHeatSt::kServiceId;

/*!
 * \brief Definition of the event ID of the skeleton event configuration for event 'BMSRmtPreHeatSt'.
 */
::amsr::someip_protocol::internal::EventId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSRmtPreHeatSt::kEventId;

// ---- Field notifier 'BMSSafCnctSt' -------------------------------------------

/*!
 * \brief Definition of the service ID of the skeleton event configuration for event 'BMSSafCnctSt'.
 */
::amsr::someip_protocol::internal::ServiceId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSSafCnctSt::kServiceId;

/*!
 * \brief Definition of the event ID of the skeleton event configuration for event 'BMSSafCnctSt'.
 */
::amsr::someip_protocol::internal::EventId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationBMSSafCnctSt::kEventId;

// ---- Field notifier 'BmsErrSt' -------------------------------------------

/*!
 * \brief Definition of the service ID of the skeleton event configuration for event 'BmsErrSt'.
 */
::amsr::someip_protocol::internal::ServiceId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationBmsErrSt::kServiceId;

/*!
 * \brief Definition of the event ID of the skeleton event configuration for event 'BmsErrSt'.
 */
::amsr::someip_protocol::internal::EventId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationBmsErrSt::kEventId;

// ---- Field notifier 'BmsHVILSt' -------------------------------------------

/*!
 * \brief Definition of the service ID of the skeleton event configuration for event 'BmsHVILSt'.
 */
::amsr::someip_protocol::internal::ServiceId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationBmsHVILSt::kServiceId;

/*!
 * \brief Definition of the event ID of the skeleton event configuration for event 'BmsHVILSt'.
 */
::amsr::someip_protocol::internal::EventId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationBmsHVILSt::kEventId;

// ---- Field notifier 'BmsIsoMeasSt' -------------------------------------------

/*!
 * \brief Definition of the service ID of the skeleton event configuration for event 'BmsIsoMeasSt'.
 */
::amsr::someip_protocol::internal::ServiceId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationBmsIsoMeasSt::kServiceId;

/*!
 * \brief Definition of the event ID of the skeleton event configuration for event 'BmsIsoMeasSt'.
 */
::amsr::someip_protocol::internal::EventId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationBmsIsoMeasSt::kEventId;

// ---- Field notifier 'BmsSt' -------------------------------------------

/*!
 * \brief Definition of the service ID of the skeleton event configuration for event 'BmsSt'.
 */
::amsr::someip_protocol::internal::ServiceId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationBmsSt::kServiceId;

/*!
 * \brief Definition of the event ID of the skeleton event configuration for event 'BmsSt'.
 */
::amsr::someip_protocol::internal::EventId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationBmsSt::kEventId;

// ---- Field notifier 'HVBattSOCLim' -------------------------------------------

/*!
 * \brief Definition of the service ID of the skeleton event configuration for event 'HVBattSOCLim'.
 */
::amsr::someip_protocol::internal::ServiceId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationHVBattSOCLim::kServiceId;

/*!
 * \brief Definition of the event ID of the skeleton event configuration for event 'HVBattSOCLim'.
 */
::amsr::someip_protocol::internal::EventId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationHVBattSOCLim::kEventId;

// ---- Field notifier 'HVMaiSwtSt' -------------------------------------------

/*!
 * \brief Definition of the service ID of the skeleton event configuration for event 'HVMaiSwtSt'.
 */
::amsr::someip_protocol::internal::ServiceId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationHVMaiSwtSt::kServiceId;

/*!
 * \brief Definition of the event ID of the skeleton event configuration for event 'HVMaiSwtSt'.
 */
::amsr::someip_protocol::internal::EventId constexpr SI_BmsStSrvSkeletonSomeIpEventConfigurationHVMaiSwtSt::kEventId;

}  // namespace internal
}  // namespace someip_binding_transformation_layer
}  // namespace amsr
}  // namespace bmsstsrv_si
}  // namespace pt_batterypacksys
}  // namespace gwm

