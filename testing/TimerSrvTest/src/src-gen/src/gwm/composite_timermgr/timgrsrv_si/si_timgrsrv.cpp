/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  TiMgrSrvExe/src/gwm/composite_timermgr/timgrsrv_si/si_timgrsrv.cpp
 *        \brief  Header for service 'SI_TiMgrSrv'.
 *
 *      \details  This Service interface provides the timer manager request
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_socal
 *         Commit ID: 209b3c0234013e84e3170ee826c9a8c6022de194
 *********************************************************************************************************************/

#ifndef TIMGRSRVEXE_SRC_GWM_COMPOSITE_TIMERMGR_TIMGRSRV_SI_SI_TIMGRSRV_CPP_
#define TIMGRSRVEXE_SRC_GWM_COMPOSITE_TIMERMGR_TIMGRSRV_SI_SI_TIMGRSRV_CPP_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "gwm/composite_timermgr/timgrsrv_si/SI_TiMgrSrv.h"

namespace gwm {
namespace composite_timermgr {
namespace timgrsrv_si {

/*!
 * \brief Service Identifier instance.
 */
constexpr ara::com::ServiceIdentifierType SI_TiMgrSrv::ServiceIdentifier;

/*!
 * \brief Service shortname path instance.
 */
constexpr vac::container::CStringView SI_TiMgrSrv::kServiceShortNamePath;

}  // namespace timgrsrv_si
}  // namespace composite_timermgr
}  // namespace gwm

#endif  // TIMGRSRVEXE_SRC_GWM_COMPOSITE_TIMERMGR_TIMGRSRV_SI_SI_TIMGRSRV_CPP_
