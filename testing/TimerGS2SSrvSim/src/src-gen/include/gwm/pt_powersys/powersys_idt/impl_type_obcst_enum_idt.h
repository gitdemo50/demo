/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/include/gwm/pt_powersys/powersys_idt/impl_type_obcst_enum_idt.h
 *        \brief
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_modelleddatatypes_api
 *         Commit ID: d9332226824a4a6c4616e2d948d41c862d192343
 *********************************************************************************************************************/

#ifndef DENPENDENCYSRVEXE_INCLUDE_GWM_PT_POWERSYS_POWERSYS_IDT_IMPL_TYPE_OBCST_ENUM_IDT_H_
#define DENPENDENCYSRVEXE_INCLUDE_GWM_PT_POWERSYS_POWERSYS_IDT_IMPL_TYPE_OBCST_ENUM_IDT_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include <array>
#include <cstdint>
#include <map>
#include <string>
#include <vector>

namespace gwm {
namespace pt_powersys {
namespace powersys_idt {

/*!
 * \brief Type OBCSt_Enum_Idt.
 * \remark generated
 * \trace SPEC-5951372
 *
 * IMPLEMENTATION-DATA-TYPE /DataTypes/ImplementationDataTypes/OBCSt_Enum_Idt
 */
enum class OBCSt_Enum_Idt : std::uint8_t {
  Discharging = 0,
  Init = 1,
  Standby = 2,
  Heating = 3,
  ChargingNormal = 4,
  Reserved1 = 5,
  Reserved2 = 6,
  Reserved3 = 7,
  Fault = 8,
  ReducedPower = 9,
  Reserved4 = 10,
  SleepShutDown = 11,
  OFF = 12,
  Reserved5 = 13,
  Reserved6 = 14,
  Reserved7 = 15
};

}  // namespace powersys_idt
}  // namespace pt_powersys
}  // namespace gwm

#endif  // DENPENDENCYSRVEXE_INCLUDE_GWM_PT_POWERSYS_POWERSYS_IDT_IMPL_TYPE_OBCST_ENUM_IDT_H_
