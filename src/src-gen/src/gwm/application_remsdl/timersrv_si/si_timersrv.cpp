/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  TimerSrvExe/src/gwm/application_remsdl/timersrv_si/si_timersrv.cpp
 *        \brief  Header for service 'SI_TimerSrv'.
 *
 *      \details  This Service interface provides timer
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_socal
 *         Commit ID: 209b3c0234013e84e3170ee826c9a8c6022de194
 *********************************************************************************************************************/

#ifndef TIMERSRVEXE_SRC_GWM_APPLICATION_REMSDL_TIMERSRV_SI_SI_TIMERSRV_CPP_
#define TIMERSRVEXE_SRC_GWM_APPLICATION_REMSDL_TIMERSRV_SI_SI_TIMERSRV_CPP_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "gwm/application_remsdl/timersrv_si/SI_TimerSrv.h"

namespace gwm {
namespace application_remsdl {
namespace timersrv_si {

/*!
 * \brief Service Identifier instance.
 */
constexpr ara::com::ServiceIdentifierType SI_TimerSrv::ServiceIdentifier;

/*!
 * \brief Service shortname path instance.
 */
constexpr vac::container::CStringView SI_TimerSrv::kServiceShortNamePath;

}  // namespace timersrv_si
}  // namespace application_remsdl
}  // namespace gwm

#endif  // TIMERSRVEXE_SRC_GWM_APPLICATION_REMSDL_TIMERSRV_SI_SI_TIMERSRV_CPP_
