
#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "anytype.hpp"

namespace gwm {
namespace utils {

using ::testing::StartsWith;

struct TestObj
{
    int a;
    char b;
};

TEST(AnyType, AnyType) {
    
    AnyType intVal(599);
    AnyType charVal('Y');
    AnyType shortVal((short)199);
    AnyType floatVal(3.145f);
    AnyType doubleVal(3.145);
    AnyType boolVal(true);

    TestObj testObj{1000, 'B'};
    TestObj* testObjPtr = &testObj;

    AnyType objType(testObj);
    AnyType ptrType(testObjPtr);

    EXPECT_EQ(intVal.getValue<int>(), 599);
    EXPECT_EQ(charVal.getValue<char>(), 'Y');
    EXPECT_EQ(shortVal.getValue<short>(), (short)199);
    EXPECT_EQ(floatVal.getValue<float>(), 3.145f);
    EXPECT_EQ(doubleVal.getValue<double>(), 3.145);
    EXPECT_EQ(boolVal.getValue<bool>(), true);

    EXPECT_EQ(objType.getValue<TestObj>().a, 1000);
    EXPECT_EQ(objType.getValue<TestObj>().b, 'B');

    EXPECT_EQ(ptrType.getValue<TestObj*>()->a, 1000);
    EXPECT_EQ(ptrType.getValue<TestObj*>()->b, 'B');

    EXPECT_THROW(objType.getValue<int>(), InvalidCast );
}

TEST(AnyType, Copy) {
    
    AnyType intVal(599);
    AnyType charVal('Y');
    AnyType shortVal((short)199);
    AnyType floatVal(3.145f);
    AnyType doubleVal(3.145);
    AnyType boolVal(true);

    TestObj testObj{1000, 'B'};
    TestObj* testObjPtr = &testObj;

    AnyType objType(testObj);
    AnyType ptrType(testObjPtr);

    AnyType anotherIntVal = intVal;
    AnyType anotherCharVal = charVal;
    AnyType anotherShortVal = shortVal;
    AnyType anotherFloatVal = floatVal;
    AnyType anotherDoubleVal = doubleVal;
    AnyType anotherBoolVal = boolVal;
 
    AnyType anotherObjType = objType;
    AnyType anotherPtrType = testObjPtr;

    EXPECT_EQ(anotherIntVal.getValue<int>(), 599);
    EXPECT_EQ(anotherCharVal.getValue<char>(), 'Y');
    EXPECT_EQ(anotherShortVal.getValue<short>(), (short)199);
    EXPECT_EQ(anotherFloatVal.getValue<float>(), 3.145f);
    EXPECT_EQ(anotherDoubleVal.getValue<double>(), 3.145);
    EXPECT_EQ(anotherBoolVal.getValue<bool>(), true);

    EXPECT_EQ(anotherObjType.getValue<TestObj>().a, 1000);
    EXPECT_EQ(anotherObjType.getValue<TestObj>().b, 'B');

    EXPECT_EQ(anotherPtrType.getValue<TestObj*>()->a, 1000);
    EXPECT_EQ(anotherPtrType.getValue<TestObj*>()->b, 'B');

    EXPECT_THROW(anotherObjType.getValue<int>(), InvalidCast );
}


TEST(AnyType, Move) {
    
    AnyType intVal(599);

    AnyType v(std::move(intVal));

    EXPECT_EQ(v.getValue<int>(), 599);
    EXPECT_EQ((v == intVal), false);

    EXPECT_THROW(intVal.getValue<int>(), InvalidCast );
    EXPECT_THROW((int)intVal, InvalidCast );
    EXPECT_THROW(intVal.toString(), InvalidCast );

    try {
        intVal.getValue<int>();
    } catch(InvalidCast & ic) {
        ic.what();
    }
}

TEST(AnyType, cast) {

    AnyType intVal(599);
    AnyType charVal('Y');
    AnyType shortVal((short)199);
    AnyType floatVal(3.145f);
    AnyType doubleVal(3.145);
    AnyType boolVal(true);

    TestObj testObj{1000, 'B'};
    TestObj* testObjPtr = &testObj;

    AnyType objType(testObj);
    AnyType ptrType(testObjPtr);

    EXPECT_EQ((int)intVal, 599);
    EXPECT_EQ((char)charVal, 'Y');
    EXPECT_EQ((short)shortVal, (short)199);
    EXPECT_EQ((float)floatVal, 3.145f);
    EXPECT_EQ((double)doubleVal, 3.145);
    EXPECT_EQ((bool)boolVal, true);

    EXPECT_EQ(static_cast<TestObj>(objType).a, 1000);
    EXPECT_EQ(static_cast<TestObj>(objType).b, 'B');

    EXPECT_EQ(static_cast<TestObj*>(ptrType)->a, 1000);
    EXPECT_EQ(static_cast<TestObj*>(ptrType)->b, 'B');

    EXPECT_THROW((int)objType, InvalidCast );
}

TEST(AnyType, toString) {

    AnyType intVal(599);
    AnyType charVal('Y');
    AnyType shortVal((short)199);
    AnyType floatVal(3.145f);
    AnyType doubleVal(3.145);
    AnyType boolVal(true);

    TestObj testObj{1000, 'B'};
    TestObj* testObjPtr = &testObj;

    AnyType objType(testObj);
    AnyType ptrType(testObjPtr);

    EXPECT_EQ(intVal.toString(), "599");
    EXPECT_EQ(charVal.toString(), "Y");
    EXPECT_EQ(shortVal.toString(), "199");
    EXPECT_THAT(floatVal.toString(), StartsWith("3.145"));
    EXPECT_THAT(doubleVal.toString(), StartsWith("3.145"));
    EXPECT_EQ(boolVal.toString(), "true");

    EXPECT_THROW(objType.toString(), InvalidCast );
}

TEST(AnyType, equals) {
    AnyType intValA(599);
    AnyType intValB(599);

    EXPECT_EQ(intValA, intValB);
}

}}