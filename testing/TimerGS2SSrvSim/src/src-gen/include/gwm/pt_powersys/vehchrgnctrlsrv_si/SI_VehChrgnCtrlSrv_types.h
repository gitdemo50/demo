
/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/include/gwm/pt_powersys/vehchrgnctrlsrv_si/SI_VehChrgnCtrlSrv_types.h
 *        \brief  Input and output structures for methods, fields and application errors of service 'SI_VehChrgnCtrlSrv'
 *
 *      \details  Definition of common input-/output structs used for simplified argument / marshalling handling. For all elements like methods, events fields structs with the related in-/output arguments are generated.
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_socal
 *         Commit ID: 209b3c0234013e84e3170ee826c9a8c6022de194
 *********************************************************************************************************************/

#ifndef DENPENDENCYSRVEXE_INCLUDE_GWM_PT_POWERSYS_VEHCHRGNCTRLSRV_SI_SI_VEHCHRGNCTRLSRV_TYPES_H_
#define DENPENDENCYSRVEXE_INCLUDE_GWM_PT_POWERSYS_VEHCHRGNCTRLSRV_SI_SI_VEHCHRGNCTRLSRV_TYPES_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "amsr/socal/internal/handle_type.h"
#include "gwm/pt_powersys/vehchrgnctrlsrv_si/si_vehchrgnctrlsrv_common.h"

namespace gwm {
namespace pt_powersys {
namespace vehchrgnctrlsrv_si {

namespace internal {

/*!
 * \brief Proxy HandleType for the Service 'SI_VehChrgnCtrlSrv'.
 * \trace SPEC-4980259
 */
class SI_VehChrgnCtrlSrvHandleType final : public ::amsr::socal::internal::HandleType {
  public:
  /*!
   * \brief Inherit constructor.
   */
  using HandleType::HandleType;
};

namespace methods {

/*!
 * \brief A class for service method 'ChargingTypeSet' used for type definitions.
 */
class ChargingTypeSet final {
 public:
  /*!
   * \brief Struct representing all output arguments of the service method.
   */
  struct Output {
    /*!
     * \brief Reference of output argument 'Response' (/DataTypes/ImplementationDataTypes/ChargingTypeSet_Response_Enum_Idt)
     */
    ::gwm::pt_powersys::powersys_idt::ChargingTypeSet_Response_Enum_Idt Response;
  };

  /*!
   * \brief Struct representing all input arguments of the service method.
   */
  struct Input {
    /*!
     * \brief Reference of input argument 'ChrgMod' (/DataTypes/ImplementationDataTypes/ChargingTypeSet_ChrgMod_Enum_Idt)
     */
    ::gwm::pt_powersys::powersys_idt::ChargingTypeSet_ChrgMod_Enum_Idt ChrgMod;
  };
};

/*!
 * \brief A class for service method 'VehChrgnCtrl' used for type definitions.
 */
class VehChrgnCtrl final {
 public:
  /*!
   * \brief Struct representing all output arguments of the service method.
   */
  struct Output {
    /*!
     * \brief Reference of output argument 'Response' (/DataTypes/ImplementationDataTypes/VehChrgnCtrl_Response_Enum_Idt)
     */
    ::gwm::pt_powersys::powersys_idt::VehChrgnCtrl_Response_Enum_Idt Response;
  };

  /*!
   * \brief Struct representing all input arguments of the service method.
   */
  struct Input {
    /*!
     * \brief Reference of input argument 'ClntId' (/DataTypes/ImplementationDataTypes/VehChrgnCtrl_ClntId_Enum_Idt)
     */
    ::gwm::pt_powersys::powersys_idt::VehChrgnCtrl_ClntId_Enum_Idt ClntId;
    /*!
     * \brief Reference of input argument 'Command' (/DataTypes/ImplementationDataTypes/VehChrgnCtrl_Command_Enum_Idt)
     */
    ::gwm::pt_powersys::powersys_idt::VehChrgnCtrl_Command_Enum_Idt Command;
  };
};

}  // namespace methods

namespace fields {

}  // namespace fields
}  // namespace internal

}  //  namespace vehchrgnctrlsrv_si
}  //  namespace pt_powersys
}  //  namespace gwm

#endif  // DENPENDENCYSRVEXE_INCLUDE_GWM_PT_POWERSYS_VEHCHRGNCTRLSRV_SI_SI_VEHCHRGNCTRLSRV_TYPES_H_

