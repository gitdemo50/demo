#ifndef GWM_UTILS_WORKERTHREAD_HPP
#define GWM_UTILS_WORKERTHREAD_HPP

#include <atomic>
#include <condition_variable>
#include <mutex>
#include <functional>
#include <queue>
#include <memory>
#include <thread>


namespace gwm {
namespace utils {

class BaseEvent {

};

class WorkerThread
{
public:
    using TaskType = std::function<void()>;

    explicit WorkerThread(/* args */) : running(true) {
        thread = std::make_unique<std::thread>(std::bind(&WorkerThread::threadLoop,this));
    }

    WorkerThread(WorkerThread const &) =  delete;

    ~WorkerThread() {
        if(running)
            stop();
    }

    void schedule(TaskType&& task) {
        std::unique_lock<std::mutex> lock(mutex);
        taskList.push(task);
        lock.unlock();
        looper.notify_one();
    }

    void stop() {
        //std::unique_lock<std::mutex> lock(mutex);
        running = false;
        looper.notify_one();
        thread->join();
    }

protected:

    // TODO support for time schdulable tasks / timer tasks
    // struct TimedTask{
    //     TaskType task;
    //     int status;
    // };

    void threadLoop() {
        while(true) {
            std::unique_lock<std::mutex> lock(mutex);
            looper.wait(lock,[this]() {
                return !(taskList.empty() && running);
            });
            
            TaskType task;
            if(!taskList.empty()) {
                task = taskList.front();
                taskList.pop();
            }
            lock.unlock();

            if(task) {
                task();
            } else if(!running) {
                break;
            }
        }
    }

    std::atomic_bool running {false};

    std::condition_variable looper;
    std::mutex mutex;

    std::queue<TaskType> taskList;

    std::unique_ptr<std::thread> thread; 
};



}
}

#endif