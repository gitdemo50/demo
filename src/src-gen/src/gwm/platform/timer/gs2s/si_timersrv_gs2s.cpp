/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  TimerSrvExe/src/gwm/platform/timer/gs2s/si_timersrv_gs2s.cpp
 *        \brief  Header for service 'SI_TimerSrv_GS2S'.
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_socal
 *         Commit ID: 209b3c0234013e84e3170ee826c9a8c6022de194
 *********************************************************************************************************************/

#ifndef TIMERSRVEXE_SRC_GWM_PLATFORM_TIMER_GS2S_SI_TIMERSRV_GS2S_CPP_
#define TIMERSRVEXE_SRC_GWM_PLATFORM_TIMER_GS2S_SI_TIMERSRV_GS2S_CPP_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "gwm/platform/timer/gs2s/SI_TimerSrv_GS2S.h"

namespace gwm {
namespace platform {
namespace timer {
namespace gs2s {

/*!
 * \brief Service Identifier instance.
 */
constexpr ara::com::ServiceIdentifierType SI_TimerSrv_GS2S::ServiceIdentifier;

/*!
 * \brief Service shortname path instance.
 */
constexpr vac::container::CStringView SI_TimerSrv_GS2S::kServiceShortNamePath;

}  // namespace gs2s
}  // namespace timer
}  // namespace platform
}  // namespace gwm

#endif  // TIMERSRVEXE_SRC_GWM_PLATFORM_TIMER_GS2S_SI_TIMERSRV_GS2S_CPP_
