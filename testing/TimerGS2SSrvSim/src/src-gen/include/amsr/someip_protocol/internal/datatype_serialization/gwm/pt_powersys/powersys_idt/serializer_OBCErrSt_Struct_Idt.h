/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/include/amsr/someip_protocol/internal/datatype_serialization/gwm/pt_powersys/powersys_idt/serializer_OBCErrSt_Struct_Idt.h
 *        \brief  SOME/IP protocol serializer implementation for datatype 'OBCErrSt_Struct_Idt'
 *
 *      \details  /DataTypes/ImplementationDataTypes/OBCErrSt_Struct_Idt
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_someipprotocol
 *         Commit ID: 0e137b9e6356987a6d2839c8d79c286219b68d60
 *********************************************************************************************************************/

#ifndef DENPENDENCYSRVEXE_INCLUDE_AMSR_SOMEIP_PROTOCOL_INTERNAL_DATATYPE_SERIALIZATION_GWM_PT_POWERSYS_POWERSYS_IDT_SERIALIZER_OBCERRST_STRUCT_IDT_H_
#define DENPENDENCYSRVEXE_INCLUDE_AMSR_SOMEIP_PROTOCOL_INTERNAL_DATATYPE_SERIALIZATION_GWM_PT_POWERSYS_POWERSYS_IDT_SERIALIZER_OBCERRST_STRUCT_IDT_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "gwm/pt_powersys/powersys_idt/impl_type_obcerrst_struct_idt.h"
#include "someip-protocol/internal/serialization/ser_common.h"
#include "someip-protocol/internal/serialization/ser_forward.h"
#include "someip-protocol/internal/serialization/ser_sizing.h"
#include "someip-protocol/internal/serialization/writer.h"

namespace gwm {
namespace pt_powersys {
namespace powersys_idt {

/*!
 * \brief Serializer for datatype /DataTypes/ImplementationDataTypes/OBCErrSt_Struct_Idt.
 * \details Data type class: DataTypeStruct (CppImplDataTypeStruct).
 *          is TLV struct: false
 *
 * \tparam TpPack Transformation properties to be used for serialization.
 *
 * \param[in, out] writer Reference to byte stream writer.
 * \param[in] data Reference to the data object of type /DataTypes/ImplementationDataTypes/OBCErrSt_Struct_Idt to be serialized.
 * \pre The writer holds a memory buffer which is enough to store the serialized data.
 * \context Reactor|App
 * \threadsafe FALSE
 * \reentrant TRUE
 * \vprivate Vector component internal API.
 * \synchronous TRUE
 */
template <typename TpPack>
void SomeIpProtocolSerialize(amsr::someip_protocol::internal::serialization::Writer& writer, ::gwm::pt_powersys::powersys_idt::OBCErrSt_Struct_Idt const &data) noexcept {
  // Namespace alias for static serialization code
  namespace serialization = amsr::someip_protocol::internal::serialization;
  // Serialize non-TLV struct
    // Serialize struct member 'FailSt_Idt' of type /DataTypes/ImplementationDataTypes/OBCErrSt__FailSt_Enum_Idt
    serialization::SomeIpProtocolSerialize<
                        TpPack,
                        // Byte-order of primitive datatype (/DataTypes/ImplementationDataTypes/OBCErrSt__FailSt_Enum_Idt)
                        typename serialization::Tp<TpPack>::ByteOrder

                        >(writer, data.FailSt_Idt);
    // Serialize struct member 'FailCode_Idt' of type /AUTOSAR/StdTypes/uint16_t
    serialization::SomeIpProtocolSerialize<
                        TpPack,
                        // Byte-order of primitive datatype (/AUTOSAR/StdTypes/uint16_t)
                        typename serialization::Tp<TpPack>::ByteOrder

                        >(writer, data.FailCode_Idt);
}

/*!
 * \brief Calculates the required buffer size for datatype /DataTypes/ImplementationDataTypes/OBCErrSt_Struct_Idt.
 * \details Data type class: DataTypeStruct (CppImplDataTypeStruct).
 *          is TLV struct: false
 *
 * \tparam TpPack Transformation properties to be used for serialization.
 * \param[in] data Reference to the data object of type /DataTypes/ImplementationDataTypes/OBCErrSt_Struct_Idt.
 * \return Returns the required buffer size for the struct in bytes.
 * \pre -
 * \context Reactor|App
 * \threadsafe FALSE
 * \reentrant TRUE
 * \vprivate Vector component internal API.
 * \synchronous TRUE
 */
template <typename TpPack>
constexpr std::size_t GetRequiredBufferSize(::gwm::pt_powersys::powersys_idt::OBCErrSt_Struct_Idt const &data) noexcept {
  // Namespace alias for static serialization code
  namespace serialization = amsr::someip_protocol::internal::serialization;

  // Sum of required size
  std::size_t required_buffer_size{0};

  // Accumulate the static size of struct member 'FailSt_Idt' of type /DataTypes/ImplementationDataTypes/OBCErrSt__FailSt_Enum_Idt
  required_buffer_size += serialization::GetRequiredBufferSize<
                        TpPack,
                        // Byte-order of primitive datatype (/DataTypes/ImplementationDataTypes/OBCErrSt__FailSt_Enum_Idt)
                        typename serialization::Tp<TpPack>::ByteOrder

                        >(data.FailSt_Idt);
  // Accumulate the static size of struct member 'FailCode_Idt' of type /AUTOSAR/StdTypes/uint16_t
  required_buffer_size += serialization::GetRequiredBufferSize<
                        TpPack,
                        // Byte-order of primitive datatype (/AUTOSAR/StdTypes/uint16_t)
                        typename serialization::Tp<TpPack>::ByteOrder

                        >(data.FailCode_Idt);

  return required_buffer_size;
}

/*!
 * \brief Checks if datatype /DataTypes/ImplementationDataTypes/OBCErrSt_Struct_Idt is of static size.
 * \details Data type class: DataTypeStruct (CppImplDataTypeStruct).
 *          is TLV struct: false
 *
 * \tparam TpPack Transformation properties to be used for serialization.
 * \return True if datatype has static size, false otherwise.
 * \pre -
 * \context Reactor|App
 * \threadsafe FALSE
 * \reentrant TRUE
 * \vprivate Vector component internal API.
 * \synchronous TRUE
 */
template <typename TpPack>
constexpr bool IsStaticSize(amsr::someip_protocol::internal::serialization::SizeToken<::gwm::pt_powersys::powersys_idt::OBCErrSt_Struct_Idt>) noexcept {
  // Namespace alias for static serialization code
  namespace serialization = amsr::someip_protocol::internal::serialization;

  constexpr bool is_static_size{
  // Check static size status of struct member 'FailSt_Idt' of type /DataTypes/ImplementationDataTypes/OBCErrSt__FailSt_Enum_Idt
  serialization::IsStaticSize<
                      TpPack,
                      // Byte-order of primitive datatype (/DataTypes/ImplementationDataTypes/OBCErrSt__FailSt_Enum_Idt)
                      typename serialization::Tp<TpPack>::ByteOrder

                      >(serialization::SizeToken<::gwm::pt_powersys::powersys_idt::OBCErrSt__FailSt_Enum_Idt>{})  && 
  
  // Check static size status of struct member 'FailCode_Idt' of type /AUTOSAR/StdTypes/uint16_t
  serialization::IsStaticSize<
                      TpPack,
                      // Byte-order of primitive datatype (/AUTOSAR/StdTypes/uint16_t)
                      typename serialization::Tp<TpPack>::ByteOrder

                      >(serialization::SizeToken<::gwm::pt_drivingsys::drivingsys_idt::LRMcuFailSt_FailCode_Integer_Idt>{}) 
  };
    return is_static_size;
}

}  // namespace powersys_idt
}  // namespace pt_powersys
}  // namespace gwm

#endif  // DENPENDENCYSRVEXE_INCLUDE_AMSR_SOMEIP_PROTOCOL_INTERNAL_DATATYPE_SERIALIZATION_GWM_PT_POWERSYS_POWERSYS_IDT_SERIALIZER_OBCERRST_STRUCT_IDT_H_

