#ifndef GWM_PLATFORM_LOGGER_IMPL_GWM_LOGGER_HPP
#define GWM_PLATFORM_LOGGER_IMPL_GWM_LOGGER_HPP

#include "gwmlogstream.hpp"
#include "gwmloggertarget.hpp"

#include <vector>

namespace gwm {
namespace platform {
namespace impl {

enum GWMLogLevel : uint32_t {
    LogOff,
    LogFatal,
    LogError,
    LogWarn,
    LogInfo,
    LogDebug,
    LogVerbose
};

class GWMLogger {
public:
    GWMLogger(std::string name, GWMLogLevel logLevel);

    ~GWMLogger();

    GWMLogStream LogFatal() { return GWMLogStream(*this, GWMLogLevel::LogFatal); }
    GWMLogStream LogError() { return GWMLogStream(*this, GWMLogLevel::LogError); }
    GWMLogStream LogWarn() { return GWMLogStream(*this, GWMLogLevel::LogWarn); }
    GWMLogStream LogInfo() { return GWMLogStream(*this, GWMLogLevel::LogInfo); }
    GWMLogStream LogDebug() { return GWMLogStream(*this, GWMLogLevel::LogDebug); }
    GWMLogStream LogVerbose() { return GWMLogStream(*this, GWMLogLevel::LogVerbose); }

    bool isLogLevelEnabled(GWMLogLevel level);

    void log(std::stringstream& stream, GWMLogLevel logLevel);

private:
    std::string name;
    GWMLogLevel logLevel;
    int32_t nameLen;
    std::string filler;
    std::vector<GWMLoggerTarget*> targets;
};
}
}
}

#endif
