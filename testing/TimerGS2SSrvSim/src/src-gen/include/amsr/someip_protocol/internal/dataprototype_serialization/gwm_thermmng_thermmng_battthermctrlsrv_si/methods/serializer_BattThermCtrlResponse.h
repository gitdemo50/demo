/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/include/amsr/someip_protocol/internal/dataprototype_serialization/gwm_thermmng_thermmng_battthermctrlsrv_si/methods/serializer_BattThermCtrlResponse.h
 *        \brief  SOME/IP protocol serializer implementation for data prototype '/ServiceInterfaces/SI_BattThermCtrlSrv/BattThermCtrl/Response
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_someipprotocol
 *         Commit ID: 0e137b9e6356987a6d2839c8d79c286219b68d60
 *********************************************************************************************************************/

#ifndef DENPENDENCYSRVEXE_INCLUDE_AMSR_SOMEIP_PROTOCOL_INTERNAL_DATAPROTOTYPE_SERIALIZATION_GWM_THERMMNG_THERMMNG_BATTTHERMCTRLSRV_SI_METHODS_serializer_BattThermCtrlResponse_h_
#define DENPENDENCYSRVEXE_INCLUDE_AMSR_SOMEIP_PROTOCOL_INTERNAL_DATAPROTOTYPE_SERIALIZATION_GWM_THERMMNG_THERMMNG_BATTTHERMCTRLSRV_SI_METHODS_serializer_BattThermCtrlResponse_h_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "gwm/thermmng_thermmng/thermmng_idt/impl_type_battthermctrl_response_enum_idt.h"
#include "someip-protocol/internal/serialization/ser_common.h"
#include "someip-protocol/internal/serialization/ser_forward.h"
#include "someip-protocol/internal/serialization/ser_sizing.h"
#include "someip-protocol/internal/serialization/writer.h"

namespace amsr {
namespace someip_protocol {
namespace internal {
namespace dataprototype_serializer {
namespace gwm_thermmng_thermmng_battthermctrlsrv_si {
namespace methods {

/*!
 * \brief Serializer for method argument /ServiceInterfaces/SI_BattThermCtrlSrv/BattThermCtrl/Response
 *        of service interface /ServiceInterfaces/SI_BattThermCtrlSrv.
 * \details Top-Level data type: /DataTypes/ImplementationDataTypes/BattThermCtrl_Response_Enum_Idt
 *          Effective transformation properties of the DataPrototype:
 *          - ByteOrder:                    MOST-SIGNIFICANT-BYTE-FIRST (big-endian)
 *          - sizeOfArrayLengthField:       4
 *          - sizeOfVectorLengthField:      4
 *          - sizeOfMapLengthField:         4
 *          - sizeOfStringLengthField:      4
 *          - sizeOfStructLengthField:      0
 *          - sizeOfUnionLengthField:       4
 *          - sizeOfUnionTypeSelectorField: 4
 *          - isBomActive:                  true
 *          - isNullTerminationActive:      true
 *          - isDynamicLengthFieldSize:     false
 */
class SerializerBattThermCtrlResponse {
 public:
  /*!
   * \brief Returns the required buffer size for the data prototype method argument /ServiceInterfaces/SI_BattThermCtrlSrv/BattThermCtrl/Response.
   *
   * \param[in]   data Reference to data object of top-level data type /DataTypes/ImplementationDataTypes/BattThermCtrl_Response_Enum_Idt.
   *
   * \return      Calculated buffer size for serialization.
   * \pre         -
   * \context     Reactor
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  constexpr static std::size_t GetRequiredBufferSize(::gwm::thermmng_thermmng::thermmng_idt::BattThermCtrl_Response_Enum_Idt const &data) noexcept {
    return serialization::GetRequiredBufferSize<
      TpPackDataPrototype,
      // Byte-order of primitive datatype (/DataTypes/ImplementationDataTypes/BattThermCtrl_Response_Enum_Idt)
      typename serialization::Tp<TpPackDataPrototype>::ByteOrder

      >(data);
  }

  /*!
   * \brief Serialize the data prototype method argument /ServiceInterfaces/SI_BattThermCtrlSrv/BattThermCtrl/Response.
   *
   * \param[in,out] writer Reference to the byte stream writer.
   * \param[in]     data Reference to data object of top-level data type /DataTypes/ImplementationDataTypes/BattThermCtrl_Response_Enum_Idt
   *                , whose value will be serialized into the writer.
   *
   * \pre         -
   * \context     Reactor
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  static void Serialize(serialization::Writer &writer, ::gwm::thermmng_thermmng::thermmng_idt::BattThermCtrl_Response_Enum_Idt const &data);

 private:
  /*!
   * \brief Transformation properties of the data prototype.
   */
  using TpPackDataPrototype = serialization::TpPack<
      BigEndian,
      serialization::SizeOfArrayLengthField<4>,
      serialization::SizeOfVectorLengthField<4>,
      serialization::SizeOfMapLengthField<4>,
      serialization::SizeOfStringLengthField<4>,
      serialization::SizeOfStructLengthField<0>,
      serialization::SizeOfUnionLengthField<4>,
      serialization::SizeOfUnionTypeSelectorField<4>,
      serialization::StringBomActive,
      serialization::StringNullTerminationActive,
      serialization::DynamicLengthFieldSizeInactive>;

};

}  // namespace methods
}  // namespace gwm_thermmng_thermmng_battthermctrlsrv_si
}  // namespace dataprototype_serializer
}  // namespace internal
}  // namespace someip_protocol
}  // namespace amsr

#endif  // DENPENDENCYSRVEXE_INCLUDE_AMSR_SOMEIP_PROTOCOL_INTERNAL_DATAPROTOTYPE_SERIALIZATION_GWM_THERMMNG_THERMMNG_BATTTHERMCTRLSRV_SI_METHODS_serializer_BattThermCtrlResponse_h_

