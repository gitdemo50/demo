/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  TiMgrSrvExe/include/amsr/someip_protocol/internal/method_deserialization/gwm_composite_timermgr_timgrsrv_si/methods/deserializer_Request_AlrmClkReq.h
 *        \brief  SOME/IP packet deserializer of service 'SI_TiMgrSrv'.
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_someipprotocol
 *         Commit ID: 0e137b9e6356987a6d2839c8d79c286219b68d60
 *********************************************************************************************************************/

#ifndef TIMGRSRVEXE_INCLUDE_AMSR_SOMEIP_PROTOCOL_INTERNAL_METHOD_DESERIALIZATION_GWM_COMPOSITE_TIMERMGR_TIMGRSRV_SI_METHODS_deserializer_Request_AlrmClkReq_h_
#define TIMGRSRVEXE_INCLUDE_AMSR_SOMEIP_PROTOCOL_INTERNAL_METHOD_DESERIALIZATION_GWM_COMPOSITE_TIMERMGR_TIMGRSRV_SI_METHODS_deserializer_Request_AlrmClkReq_h_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "amsr/someip_protocol/internal/dataprototype_deserialization/gwm_composite_timermgr_timgrsrv_si/methods/deserializer_AlrmClkReqAlrmClkBgnTi.h"
#include "amsr/someip_protocol/internal/dataprototype_deserialization/gwm_composite_timermgr_timgrsrv_si/methods/deserializer_AlrmClkReqAlrmClkEndTi.h"
#include "amsr/someip_protocol/internal/dataprototype_deserialization/gwm_composite_timermgr_timgrsrv_si/methods/deserializer_AlrmClkReqAlrmClkFrq.h"
#include "amsr/someip_protocol/internal/dataprototype_deserialization/gwm_composite_timermgr_timgrsrv_si/methods/deserializer_AlrmClkReqClntID.h"
#include "someip-protocol/internal/deserialization/common.h"
#include "someip-protocol/internal/deserialization/reader.h"

namespace amsr {
namespace someip_protocol {
namespace internal {
namespace method_deserializer {
namespace gwm_composite_timermgr_timgrsrv_si {
namespace methods {

/*!
 * \brief Deserializer for a SOME/IP packet of method '/ServiceInterfaces/SI_TiMgrSrv/AlrmClkReq'
 *        of service interface '/ServiceInterfaces/SI_TiMgrSrv'.
 */
class DeserializerRequestAlrmClkReq final {
 public:
  /*!
   * \brief Deserialize the SOME/IP packet payload of method 'AlrmClkReq'.
   *
   * \tparam Output Type of data structure storing all deserialized in-/output arguments.
   *                Type must support member initializer lists. Typically a struct or a std::tuple is used.
   * \param[in,out] reader Reference to the byte stream reader.
   * \param[out]    output Parameter to store deserialized output.
   * \return        True if the deserialization is successful, false otherwise.
   * \pre           -
   * \context       Reactor|App
   * \threadsafe    FALSE
   * \reentrant     TRUE for different reader objects.
   * \synchronous   TRUE
   */
  template <typename Output>
  static bool Deserialize(deserialization::Reader& reader, Output & output) noexcept {    


    // Deserialize the argument 'ClntID' of type '/DataTypes/ImplementationDataTypes/AlrmClk_ClntID_Enum_Idt'

    // VECTOR NC AutosarC++17_10-A7.1.1: MD_SOMEIPPROTOCOL_AutosarC++17_10-A7.1.1_Immutable_Variable_Generation
    bool deserialization_ok{
        dataprototype_deserializer::gwm_composite_timermgr_timgrsrv_si::methods::DeserializerAlrmClkReqClntID::Deserialize(
            reader, output.ClntID)};

    // Deserialize the argument 'AlrmClkFrq' of type '/DataTypes/ImplementationDataTypes/AlrmClkFrq_Enum_Idt'

    // VECTOR NC AutosarC++17_10-A7.1.1: MD_SOMEIPPROTOCOL_AutosarC++17_10-A7.1.1_Immutable_Variable_Generation
    if(deserialization_ok) {
      deserialization_ok =
          dataprototype_deserializer::gwm_composite_timermgr_timgrsrv_si::methods::DeserializerAlrmClkReqAlrmClkFrq::Deserialize(
              reader, output.AlrmClkFrq);
    }

    // Deserialize the argument 'AlrmClkBgnTi' of type '/DataTypes/ImplementationDataTypes/AlrmClkBgnTi_Struct_Idt'

    // VECTOR NC AutosarC++17_10-A7.1.1: MD_SOMEIPPROTOCOL_AutosarC++17_10-A7.1.1_Immutable_Variable_Generation
    if(deserialization_ok) {
      deserialization_ok =
          dataprototype_deserializer::gwm_composite_timermgr_timgrsrv_si::methods::DeserializerAlrmClkReqAlrmClkBgnTi::Deserialize(
              reader, output.AlrmClkBgnTi);
    }

    // Deserialize the argument 'AlrmClkEndTi' of type '/DataTypes/ImplementationDataTypes/AlrmClkEndTi_Struct_Idt'

    // VECTOR NC AutosarC++17_10-A7.1.1: MD_SOMEIPPROTOCOL_AutosarC++17_10-A7.1.1_Immutable_Variable_Generation
    if(deserialization_ok) {
      deserialization_ok =
          dataprototype_deserializer::gwm_composite_timermgr_timgrsrv_si::methods::DeserializerAlrmClkReqAlrmClkEndTi::Deserialize(
              reader, output.AlrmClkEndTi);
    }
  // Return deserialization result
  return deserialization_ok;
  }
};

}  // namespace methods
}  // namespace gwm_composite_timermgr_timgrsrv_si
}  // namespace method_deserializer
}  // namespace internal
}  // namespace someip_protocol
}  // namespace amsr

#endif  // TIMGRSRVEXE_INCLUDE_AMSR_SOMEIP_PROTOCOL_INTERNAL_METHOD_DESERIALIZATION_GWM_COMPOSITE_TIMERMGR_TIMGRSRV_SI_METHODS_deserializer_Request_AlrmClkReq_h_

