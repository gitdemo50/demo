/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  TiMgrSrvExe/include/amsr/someip_protocol/internal/method_deserialization/gwm_composite_timermgr_timgrsrv_si/fields/deserializer_Request_AlrmClkWkeEveGet.h
 *        \brief  SOME/IP packet deserializer of service 'SI_TiMgrSrv'.
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_someipprotocol
 *         Commit ID: 0e137b9e6356987a6d2839c8d79c286219b68d60
 *********************************************************************************************************************/

#ifndef TIMGRSRVEXE_INCLUDE_AMSR_SOMEIP_PROTOCOL_INTERNAL_METHOD_DESERIALIZATION_GWM_COMPOSITE_TIMERMGR_TIMGRSRV_SI_FIELDS_deserializer_Request_AlrmClkWkeEveGet_h_
#define TIMGRSRVEXE_INCLUDE_AMSR_SOMEIP_PROTOCOL_INTERNAL_METHOD_DESERIALIZATION_GWM_COMPOSITE_TIMERMGR_TIMGRSRV_SI_FIELDS_deserializer_Request_AlrmClkWkeEveGet_h_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "someip-protocol/internal/deserialization/common.h"
#include "someip-protocol/internal/deserialization/reader.h"

namespace amsr {
namespace someip_protocol {
namespace internal {
namespace method_deserializer {
namespace gwm_composite_timermgr_timgrsrv_si {
namespace fields {

/*!
 * \brief Deserializer for a SOME/IP packet of method '/ServiceInterfaces/SI_TiMgrSrv/AlrmClkWkeEve'
 *        of service interface '/ServiceInterfaces/SI_TiMgrSrv'.
 */
class DeserializerRequestAlrmClkWkeEveGet final {
 public:
  /*!
   * \brief Deserialize the SOME/IP packet payload of method 'AlrmClkWkeEveGet'.
   *
   * \tparam Output Type of data structure storing all deserialized in-/output arguments.
   *                Type must support member initializer lists. Typically a struct or a std::tuple is used.
   * \param[in,out] reader Reference to the byte stream reader.
   * \param[out]    output Parameter to store deserialized output.
   * \return        True if the deserialization is successful, false otherwise.
   * \pre           -
   * \context       Reactor|App
   * \threadsafe    FALSE
   * \reentrant     TRUE for different reader objects.
   * \synchronous   TRUE
   */
  template <typename Output>
  static bool Deserialize(deserialization::Reader const& reader, Output  const& output) noexcept {    
    // No arguments to deserialize
    static_cast<void>(reader);
    static_cast<void>(output);

    // Return always true as no deserialization is necessary.
    return true;
  }
};

}  // namespace fields
}  // namespace gwm_composite_timermgr_timgrsrv_si
}  // namespace method_deserializer
}  // namespace internal
}  // namespace someip_protocol
}  // namespace amsr

#endif  // TIMGRSRVEXE_INCLUDE_AMSR_SOMEIP_PROTOCOL_INTERNAL_METHOD_DESERIALIZATION_GWM_COMPOSITE_TIMERMGR_TIMGRSRV_SI_FIELDS_deserializer_Request_AlrmClkWkeEveGet_h_

