/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/include/amsr/someip_protocol/internal/method_deserialization/gwm_thermmng_thermmng_battthermctrlsrv_si/methods/deserializer_Request_BattThermCtrl.h
 *        \brief  SOME/IP packet deserializer of service 'SI_BattThermCtrlSrv'.
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_someipprotocol
 *         Commit ID: 0e137b9e6356987a6d2839c8d79c286219b68d60
 *********************************************************************************************************************/

#ifndef DENPENDENCYSRVEXE_INCLUDE_AMSR_SOMEIP_PROTOCOL_INTERNAL_METHOD_DESERIALIZATION_GWM_THERMMNG_THERMMNG_BATTTHERMCTRLSRV_SI_METHODS_deserializer_Request_BattThermCtrl_h_
#define DENPENDENCYSRVEXE_INCLUDE_AMSR_SOMEIP_PROTOCOL_INTERNAL_METHOD_DESERIALIZATION_GWM_THERMMNG_THERMMNG_BATTTHERMCTRLSRV_SI_METHODS_deserializer_Request_BattThermCtrl_h_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "amsr/someip_protocol/internal/dataprototype_deserialization/gwm_thermmng_thermmng_battthermctrlsrv_si/methods/deserializer_BattThermCtrlClntId.h"
#include "amsr/someip_protocol/internal/dataprototype_deserialization/gwm_thermmng_thermmng_battthermctrlsrv_si/methods/deserializer_BattThermCtrlCommand.h"
#include "someip-protocol/internal/deserialization/common.h"
#include "someip-protocol/internal/deserialization/reader.h"

namespace amsr {
namespace someip_protocol {
namespace internal {
namespace method_deserializer {
namespace gwm_thermmng_thermmng_battthermctrlsrv_si {
namespace methods {

/*!
 * \brief Deserializer for a SOME/IP packet of method '/ServiceInterfaces/SI_BattThermCtrlSrv/BattThermCtrl'
 *        of service interface '/ServiceInterfaces/SI_BattThermCtrlSrv'.
 */
class DeserializerRequestBattThermCtrl final {
 public:
  /*!
   * \brief Deserialize the SOME/IP packet payload of method 'BattThermCtrl'.
   *
   * \tparam Output Type of data structure storing all deserialized in-/output arguments.
   *                Type must support member initializer lists. Typically a struct or a std::tuple is used.
   * \param[in,out] reader Reference to the byte stream reader.
   * \param[out]    output Parameter to store deserialized output.
   * \return        True if the deserialization is successful, false otherwise.
   * \pre           -
   * \context       Reactor|App
   * \threadsafe    FALSE
   * \reentrant     TRUE for different reader objects.
   * \synchronous   TRUE
   */
  template <typename Output>
  static bool Deserialize(deserialization::Reader& reader, Output & output) noexcept {    


    // Deserialize the argument 'ClntId' of type '/DataTypes/ImplementationDataTypes/BattThermCtrl_ClntId_Enum_Idt'

    // VECTOR NC AutosarC++17_10-A7.1.1: MD_SOMEIPPROTOCOL_AutosarC++17_10-A7.1.1_Immutable_Variable_Generation
    bool deserialization_ok{
        dataprototype_deserializer::gwm_thermmng_thermmng_battthermctrlsrv_si::methods::DeserializerBattThermCtrlClntId::Deserialize(
            reader, output.ClntId)};

    // Deserialize the argument 'Command' of type '/AUTOSAR/StdTypes/int8_t'

    // VECTOR NC AutosarC++17_10-A7.1.1: MD_SOMEIPPROTOCOL_AutosarC++17_10-A7.1.1_Immutable_Variable_Generation
    if(deserialization_ok) {
      deserialization_ok =
          dataprototype_deserializer::gwm_thermmng_thermmng_battthermctrlsrv_si::methods::DeserializerBattThermCtrlCommand::Deserialize(
              reader, output.Command);
    }
  // Return deserialization result
  return deserialization_ok;
  }
};

}  // namespace methods
}  // namespace gwm_thermmng_thermmng_battthermctrlsrv_si
}  // namespace method_deserializer
}  // namespace internal
}  // namespace someip_protocol
}  // namespace amsr

#endif  // DENPENDENCYSRVEXE_INCLUDE_AMSR_SOMEIP_PROTOCOL_INTERNAL_METHOD_DESERIALIZATION_GWM_THERMMNG_THERMMNG_BATTTHERMCTRLSRV_SI_METHODS_deserializer_Request_BattThermCtrl_h_

