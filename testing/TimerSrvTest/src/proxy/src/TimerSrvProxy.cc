
#include "TimerSrvProxy.hpp"

namespace gwm {
namespace services {
namespace timer {

namespace proxy {

TimerSrvService::TimerSrvService() : serviceSpec("TiMgrSrvExe/RootSwComponentPrototype/AdaptiveRequiredPortType_TimerSrv"),
    SensorProxy((gwm::platform::Logger("TimerSrvService"))) {

}

TimerSrvService::~TimerSrvService() {

}

void TimerSrvService::doSubscription() {
    logger.LogVerbose() << "TimerSrvService doSubscription " << proxy.get();
    ev_timerSt.subscribe();

    if(_availabilityNotifier) {
        _availabilityNotifier();
    }
}

void TimerSrvService::initProxyItems() {
    logger.LogVerbose() << "TimerSrvService initProxyItems ";
    
    timerReq.set(&proxy->TimerReq);
    timerCncl.set(&proxy->TimerCncl);
    ev_timerSt.set(&proxy->TimerSt);
}

}}}}