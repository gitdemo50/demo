/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/include/gwm/thermmng_thermmng/battthermctrlsrv_si/SI_BattThermCtrlSrv.h
 *        \brief  Header for service 'SI_BattThermCtrlSrv'.
 *
 *      \details  The service interface provides vehicle thermal control strategy level control
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_socal
 *         Commit ID: 209b3c0234013e84e3170ee826c9a8c6022de194
 *********************************************************************************************************************/

#ifndef DENPENDENCYSRVEXE_INCLUDE_GWM_THERMMNG_THERMMNG_BATTTHERMCTRLSRV_SI_SI_BATTTHERMCTRLSRV_H_
#define DENPENDENCYSRVEXE_INCLUDE_GWM_THERMMNG_THERMMNG_BATTTHERMCTRLSRV_SI_SI_BATTTHERMCTRLSRV_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "ara/com/service_identifier.h"
#include "vac/container/c_string_view.h"
#include "vac/container/string_literals.h"

namespace gwm {
namespace thermmng_thermmng {
namespace battthermctrlsrv_si {

// VECTOR NC AutosarC++17_10-M7.3.6: MD_SOCAL_AutosarC++17_10_M7.3.6_sv
/*!
 * \brief Literals namespace is needed for "_sv literal.
 */
using vac::container::literals::operator""_sv;  // NOLINT(build/namespaces)

/*!
 * \brief Representation of service 'SI_BattThermCtrlSrv'
 * \details Used for service-specific aspects:
 *  - Resource management
 *  - Service Identifier
 *
 * \trace SPEC-4980256
 */
class SI_BattThermCtrlSrv final {

 private:
 /*!
* \brief Type-alias for easy access to CStringView.
*/
 using CStringView = vac::container::CStringView;

  /*!
   * \brief String View for Service Identifier
   */
  static constexpr CStringView kServiceIdentifier{"SI_BattThermCtrlSrv"_sv};

 public:

  /*!
   * \brief Service Identifier.
   *
   * \trace SPEC-4980252, SPEC-4980256
   */
  static constexpr ara::com::ServiceIdentifierType ServiceIdentifier{kServiceIdentifier};

  /*!
   * \brief Service shortname path.
   */
  static constexpr CStringView kServiceShortNamePath{"/ServiceInterfaces/SI_BattThermCtrlSrv"_sv};
};

}  // namespace battthermctrlsrv_si
}  // namespace thermmng_thermmng
}  // namespace gwm

#endif  // DENPENDENCYSRVEXE_INCLUDE_GWM_THERMMNG_THERMMNG_BATTTHERMCTRLSRV_SI_SI_BATTTHERMCTRLSRV_H_

