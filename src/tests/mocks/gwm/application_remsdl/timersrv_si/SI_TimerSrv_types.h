
/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  TimerSrvExe/include/gwm/application_remsdl/timersrv_si/SI_TimerSrv_types.h
 *        \brief  Input and output structures for methods, fields and application errors of service 'SI_TimerSrv'
 *
 *      \details  Definition of common input-/output structs used for simplified argument / marshalling handling. For all elements like methods, events fields structs with the related in-/output arguments are generated.
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_socal
 *         Commit ID: 209b3c0234013e84e3170ee826c9a8c6022de194
 *********************************************************************************************************************/

#ifndef TIMERSRVEXE_INCLUDE_GWM_APPLICATION_REMSDL_TIMERSRV_SI_SI_TIMERSRV_TYPES_H_
#define TIMERSRVEXE_INCLUDE_GWM_APPLICATION_REMSDL_TIMERSRV_SI_SI_TIMERSRV_TYPES_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "amsr/socal/internal/handle_type.h"
#include "gwm/application_remsdl/timersrv_si/si_timersrv_common.h"

namespace gwm {
namespace application_remsdl {
namespace timersrv_si {

namespace internal {

/*!
 * \brief Proxy HandleType for the Service 'SI_TimerSrv'.
 * \trace SPEC-4980259
 */
// class SI_TimerSrvHandleType final : public ::amsr::socal::internal::HandleType {
//   public:
//   /*!
//    * \brief Inherit constructor.
//    */
//   using HandleType::HandleType;
// };

namespace methods {

/*!
 * \brief A class for service method 'TimerCncl' used for type definitions.
 */
class TimerCncl final {
 public:
  /*!
   * \brief Struct representing all output arguments of the service method.
   */
  struct Output {
    /*!
     * \brief Reference of output argument 'Response' (/DataTypes/ImplementationDataTypes/TimerSet_Response_Enum_Idt)
     */
    ::gwm::platform_timer::timer_idt::TimerSet_Response_Enum_Idt Response;
  };

  /*!
   * \brief Struct representing all input arguments of the service method.
   */
  struct Input {
  };
};

/*!
 * \brief A class for service method 'TimerReq' used for type definitions.
 */
class TimerReq final {
 public:
  /*!
   * \brief Struct representing all output arguments of the service method.
   */
  struct Output {
    /*!
     * \brief Reference of output argument 'Response' (/DataTypes/ImplementationDataTypes/TimerSet_Response_Enum_Idt)
     */
    ::gwm::platform_timer::timer_idt::TimerSet_Response_Enum_Idt Response;
  };

  /*!
   * \brief Struct representing all input arguments of the service method.
   */
  struct Input {
    /*!
     * \brief Reference of input argument 'TimerSet' (/DataTypes/ImplementationDataTypes/TimerSet_Integer_Idt)
     */
    ::gwm::platform_timer::timer_idt::TimerSet_Integer_Idt TimerSet;
  };
};

}  // namespace methods

namespace fields {

/*!
 * \brief Data class for service field 'TimerSt'.
 * \remark generated
 */
class TimerSt final {
 public:
  /*!
   * \brief Return/output parameters of service field 'TimerSt'
   */
  using Output = ::gwm::platform_timer::timer_idt::TimerSt_Struct_Idt;
};


  /*!
 * \brief A class for field method 'TimerSt'Get used for type definitions.
 */
class TimerStGet final {
 public:
  /*!
   * \brief Struct representing the field input data.
   */
  struct Input {};

  /*!
   * \brief Struct representing the field output value.
   */
  struct Output {
    /*!
     * \brief Reference of field value (/DataTypes/ImplementationDataTypes/TimerSt_Struct_Idt)
     */
    ::gwm::platform_timer::timer_idt::TimerSt_Struct_Idt out_val;
  };
};


}  // namespace fields
}  // namespace internal

}  //  namespace timersrv_si
}  //  namespace application_remsdl
}  //  namespace gwm

#endif  // TIMERSRVEXE_INCLUDE_GWM_APPLICATION_REMSDL_TIMERSRV_SI_SI_TIMERSRV_TYPES_H_

