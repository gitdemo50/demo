#pragma once

#include "shims.hpp"

#include "SI_TimerSrv_GS2S_types.h"

namespace gwm {
namespace platform {
namespace timer {
namespace gs2s {

namespace proxy {
namespace methods {

class TimerReq : gwm::services::MethodBase {
    public:
      using Output = gwm::platform::timer::gs2s::internal::methods::TimerReq::Output;
      MOCK_METHOD(datatypes::gs2s::timersrv_gs2s::TimerSet_Response_Enum,doTimerReq,(datatypes::gs2s::timersrv_gs2s::TimerSet_Integer,
          datatypes::gs2s::timersrv_gs2s::StartPoint_Enum, datatypes::gs2s::timersrv_gs2s::TimerType_Enum,
          datatypes::gs2s::timersrv_gs2s::SleepType_Enum ),());
      ara::core::Future<Output> operator () (datatypes::gs2s::timersrv_gs2s::TimerSet_Integer a1,
          datatypes::gs2s::timersrv_gs2s::StartPoint_Enum a2, datatypes::gs2s::timersrv_gs2s::TimerType_Enum a3,
          datatypes::gs2s::timersrv_gs2s::SleepType_Enum a4) {
          ara::core::Promise<Output> p;
          try {
            auto ret = doTimerReq(a1,a2,a3,a4);
            p.set_value(Output{ret});
          } catch(const std::exception& ex) {
            p.set_exception(std::make_exception_ptr(std::exception()));
          }
          return p.get_future();
      }
  };

}  // namespace methods

namespace fields {

    class FieldNotifierTimerSt : public gwm::services::EventBase<datatypes::gs2s::timersrv_gs2s::TimerSt_Struct> {

    };
}

class SI_TimerSrv_GS2SProxy : public gwm::services::ProxyBase {
public:

    SI_TimerSrv_GS2SProxy(gwm::services::ProxyBase::HandleType& h) {

    }

  methods::TimerReq TimerReq;
  fields::FieldNotifierTimerSt TimerSt;
};

}  // namespace proxy
}  // namespace gs2s
}  // namespace timer
}  // namespace platform
}  // namespace gwm
