#ifndef GWM_MANAGER_WINDOW_MANAGER_HPP
#define GWM_MANAGER_WINDOW_MANAGER_HPP

/*
*   COPY RIGHT NOTICE
*   
*
*
*/


#include<future>
#include<memory>
#include<functional>

#include "logger/logger.hpp"

#include "TimerBasicController.hpp"
#include "TimerS2SProxy.hpp"
#include "TimerSrvImpl.hpp"

namespace gwm {
namespace manager {

using TimerS2SService = gwm::services::timer::proxy::TimerS2SService;
using TimerSrvService = gwm::services::timer::skeleton::TimerSrvImpl;
using TimerBasicController = gwm::controller::TimerBasicController;

class TimerBasicManager {
public:
    static TimerBasicManager* getInstance() {
        static TimerBasicManager* intsance = new TimerBasicManager();
        return intsance;
    }

    void start();

protected:
    TimerBasicManager();
    TimerBasicManager(const TimerBasicManager& ) = delete;

    void startControllers();

    std::shared_ptr<TimerS2SService> _srv_TimerS2SService;
    std::shared_ptr<TimerBasicController> _controller;
    std::shared_ptr<TimerSrvService> _srv_TimerSrvService;

    gwm::platform::Logger logger;
};

}
}

#endif