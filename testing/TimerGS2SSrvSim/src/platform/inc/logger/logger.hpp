#ifndef GWM_PLATFORM_LOGGER_HPP
#define GWM_PLATFORM_LOGGER_HPP
/*
*   COPY RIGHT NOTICE
*   
*
*
*/

/*
*
*   Abstract logger interfacse.
*   To setup a logger use 'createLogger' method to get a context.
*   Once that is done can use any of the stream based logging methods
*
*/

#include <string>

namespace gwm {
namespace platform {

template<typename T>
T& createLogger(std::string name);

template<typename T>
class LoggerWrapper {
public:
    LoggerWrapper(std::string name) :  impl(createLogger<T>(name)) {
    }

    LoggerWrapper(const LoggerWrapper& o) = delete;

    LoggerWrapper(LoggerWrapper&& o) : impl(o.impl) {
    }

    auto LogFatal() { return impl.LogFatal(); }
    auto LogError() { return impl.LogError(); }
    auto LogWarn() { return impl.LogWarn(); }
    auto LogInfo() { return impl.LogInfo(); }
    auto LogDebug() { return impl.LogDebug(); }
    auto LogVerbose() { return impl.LogVerbose(); }

private:
    T& impl;
};

}
}

#include "loggercontext.hpp"

#endif
