/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/src/gwm/pt_powersys/obcstsrv_si/si_obcstsrv_skeleton.cpp
 *        \brief  Skeleton for service 'SI_OBCStSrv'.
 *
 *      \details  This service interface provides OBC working status information
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_socal
 *         Commit ID: 209b3c0234013e84e3170ee826c9a8c6022de194
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "gwm/pt_powersys/obcstsrv_si/si_obcstsrv_skeleton.h"
#include "amsr/socal/internal/instance_specifier_lookup_table.h"

/*!
 * \trace SPEC-4980240
 * \trace SPEC-4980241
 */
namespace gwm {
namespace pt_powersys {
namespace obcstsrv_si {
namespace skeleton {

/*!
 * \brief Static instance of service discovery.
 */
// VECTOR NC AutosarC++17_10-A3.3.2: MD_SOCAL_AutosarC++17_10-A3.3.2_StaticStorageDurationOfNonPODType
::amsr::generic::Singleton<SI_OBCStSrvSkeleton::ServiceDiscovery*> SI_OBCStSrvSkeleton::sd_;

SI_OBCStSrvSkeleton::ConstructionResult SI_OBCStSrvSkeleton::Preconstruct(
    ara::com::InstanceIdentifier instance_id, ara::com::MethodCallProcessingMode mode) noexcept {
return Base::Preconstruct(instance_id, mode);
}

SI_OBCStSrvSkeleton::ConstructionResult SI_OBCStSrvSkeleton::Preconstruct(
    ara::core::InstanceSpecifier instance, ara::com::MethodCallProcessingMode mode) noexcept {
return Base::Preconstruct(instance, mode);
}

SI_OBCStSrvSkeleton::ConstructionResult SI_OBCStSrvSkeleton::Preconstruct(
    ara::com::InstanceIdentifierContainer instance_identifiers, ara::com::MethodCallProcessingMode mode) noexcept {
return Base::Preconstruct(instance_identifiers, mode);
}

SI_OBCStSrvSkeleton::SI_OBCStSrvSkeleton(ConstructionToken&& token) noexcept
: Base{std::move(token)}
, OBCActT(this)
, OBCChrgLim(this)
, OBCDchrOutpI(this)
, OBCDchrOutpU(this)
, OBCInptAcI(this)
, OBCInptAcU(this)
, OBCOuptDcI(this)
, OBCOuptDcU(this)
, OBCChrgGunSt(this)
, OBCErrSt(this)
, OBCHVILSt(this)
, OBCSt(this)
, OBCV2XSt(this)
 {}

SI_OBCStSrvSkeleton::SI_OBCStSrvSkeleton(ara::com::InstanceIdentifier instance,
                                               ara::com::MethodCallProcessingMode mode) noexcept
     : SI_OBCStSrvSkeleton{Preconstruct(instance, mode).Value()} {}

SI_OBCStSrvSkeleton::SI_OBCStSrvSkeleton(ara::core::InstanceSpecifier instance,
                                               ara::com::MethodCallProcessingMode mode) noexcept
     : SI_OBCStSrvSkeleton{Preconstruct(instance, mode).Value()} {}

SI_OBCStSrvSkeleton::SI_OBCStSrvSkeleton(ara::com::InstanceIdentifierContainer instance_identifiers,
                                               ara::com::MethodCallProcessingMode mode) noexcept
     : SI_OBCStSrvSkeleton{Preconstruct(instance_identifiers, mode).Value()} {}

SI_OBCStSrvSkeleton::~SI_OBCStSrvSkeleton() noexcept {
  // Next line might block until all running method requests are done.
  StopOfferService();
}

void SI_OBCStSrvSkeleton::DoFieldInitializationChecks() noexcept {

    if (!OBCChrgGunSt.IsUpdated()) {
        logger_.LogError([this](ara::log::LogStream& s) {
                          s << "Failed to offer service 'SI_OBCStSrv' with instance id(s) '";
                          for (::amsr::socal::internal::InstanceSpecifierLookupTableEntry const& entry : offered_instances_) {
                              ara::core::StringView const instance_id_str{entry.GetInstanceIdentifier().ToString()};
                              s << " " << instance_id_str;
                          }
                          s << "' - No initial value has been set for field 'OBCChrgGunSt' (OBCChrgGunSt::Update(...) has never been called)."; },
                          __func__, __LINE__);
        ara::core::Abort("No initial value has been set for field 'OBCChrgGunSt' (OBCChrgGunSt::Update(...) has never been called).");
    }

    if (!OBCErrSt.IsUpdated()) {
        logger_.LogError([this](ara::log::LogStream& s) {
                          s << "Failed to offer service 'SI_OBCStSrv' with instance id(s) '";
                          for (::amsr::socal::internal::InstanceSpecifierLookupTableEntry const& entry : offered_instances_) {
                              ara::core::StringView const instance_id_str{entry.GetInstanceIdentifier().ToString()};
                              s << " " << instance_id_str;
                          }
                          s << "' - No initial value has been set for field 'OBCErrSt' (OBCErrSt::Update(...) has never been called)."; },
                          __func__, __LINE__);
        ara::core::Abort("No initial value has been set for field 'OBCErrSt' (OBCErrSt::Update(...) has never been called).");
    }

    if (!OBCHVILSt.IsUpdated()) {
        logger_.LogError([this](ara::log::LogStream& s) {
                          s << "Failed to offer service 'SI_OBCStSrv' with instance id(s) '";
                          for (::amsr::socal::internal::InstanceSpecifierLookupTableEntry const& entry : offered_instances_) {
                              ara::core::StringView const instance_id_str{entry.GetInstanceIdentifier().ToString()};
                              s << " " << instance_id_str;
                          }
                          s << "' - No initial value has been set for field 'OBCHVILSt' (OBCHVILSt::Update(...) has never been called)."; },
                          __func__, __LINE__);
        ara::core::Abort("No initial value has been set for field 'OBCHVILSt' (OBCHVILSt::Update(...) has never been called).");
    }

    if (!OBCSt.IsUpdated()) {
        logger_.LogError([this](ara::log::LogStream& s) {
                          s << "Failed to offer service 'SI_OBCStSrv' with instance id(s) '";
                          for (::amsr::socal::internal::InstanceSpecifierLookupTableEntry const& entry : offered_instances_) {
                              ara::core::StringView const instance_id_str{entry.GetInstanceIdentifier().ToString()};
                              s << " " << instance_id_str;
                          }
                          s << "' - No initial value has been set for field 'OBCSt' (OBCSt::Update(...) has never been called)."; },
                          __func__, __LINE__);
        ara::core::Abort("No initial value has been set for field 'OBCSt' (OBCSt::Update(...) has never been called).");
    }

    if (!OBCV2XSt.IsUpdated()) {
        logger_.LogError([this](ara::log::LogStream& s) {
                          s << "Failed to offer service 'SI_OBCStSrv' with instance id(s) '";
                          for (::amsr::socal::internal::InstanceSpecifierLookupTableEntry const& entry : offered_instances_) {
                              ara::core::StringView const instance_id_str{entry.GetInstanceIdentifier().ToString()};
                              s << " " << instance_id_str;
                          }
                          s << "' - No initial value has been set for field 'OBCV2XSt' (OBCV2XSt::Update(...) has never been called)."; },
                          __func__, __LINE__);
        ara::core::Abort("No initial value has been set for field 'OBCV2XSt' (OBCV2XSt::Update(...) has never been called).");
    }
}

void SI_OBCStSrvSkeleton::SendInitialFieldNotifications() noexcept {
  // Send initial field events for all fields with "hasNotifier = true"
  OBCChrgGunSt.SendInitialValue();
  OBCErrSt.SendInitialValue();
  OBCHVILSt.SendInitialValue();
  OBCSt.SendInitialValue();
  OBCV2XSt.SendInitialValue();
}

void SI_OBCStSrvSkeleton::OfferServiceInternal(::ara::com::InstanceIdentifier const& instance_id) noexcept {
  (*(sd_.GetAccess()))->OfferService(instance_id, this);
}

void SI_OBCStSrvSkeleton::StopOfferServiceInternal(::ara::com::InstanceIdentifier const& instance_id) noexcept {
  (*(sd_.GetAccess()))->StopOfferService(instance_id);
}

::amsr::generic::Singleton<SI_OBCStSrvSkeleton::ServiceDiscovery*>& SI_OBCStSrvSkeleton::GetServiceDiscovery() noexcept {
  return sd_;
}

void SI_OBCStSrvSkeleton::RegisterServiceDiscovery(SI_OBCStSrvSkeleton::ServiceDiscovery* service_discovery) noexcept {
  sd_.Create(service_discovery);
}

void SI_OBCStSrvSkeleton::DeRegisterServiceDiscovery() noexcept { sd_.Destroy(); }

}  // namespace skeleton
}  // namespace obcstsrv_si
}  // namespace pt_powersys
}  // namespace gwm

