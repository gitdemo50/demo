/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  TiMgrSrvExe/include/ara/per/key_value_storage_variant.h
 *        \brief
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_persistency_config
 *         Commit ID: 3c577be6a7ef0697cae45fabe324d59e826d2342
 *********************************************************************************************************************/

#ifndef TIMGRSRVEXE_INCLUDE_ARA_PER_KEY_VALUE_STORAGE_VARIANT_H_
#define TIMGRSRVEXE_INCLUDE_ARA_PER_KEY_VALUE_STORAGE_VARIANT_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include <cstdint>
#include <map>
#include <string>
#include <vector>

#include "ara/core/variant.h"
#include "ara/per/key_value_storage_data_types.h"
#include "vac/language/byte.h"

namespace ara {
namespace per {
namespace detail {

/*!
 * \brief Typedef for the variant that can hold all datatypes supported by ara::per.
 *
 * \trace SPEC-8071466
 * \trace SPEC-8071467
 */
using KeyValueStorageVariant =
  ara::core::Variant<NotSet,
                     float,
                     double,
                     bool,
                     std::int8_t,
                     std::int16_t,
                     std::int32_t,
                     std::int64_t,
                     std::uint8_t,
                     std::uint16_t,
                     std::uint32_t,
                     std::uint64_t,
                     std::string,
                     std::vector<vac::language::byte>
                    >;
}  // namespace detail
}  // namespace per
}  // namespace ara

#endif  // TIMGRSRVEXE_INCLUDE_ARA_PER_KEY_VALUE_STORAGE_VARIANT_H_
