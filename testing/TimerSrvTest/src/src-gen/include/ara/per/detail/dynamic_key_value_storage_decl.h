/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  TiMgrSrvExe/include/ara/per/detail/dynamic_key_value_storage_decl.h
 *        \brief
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_persistency_config
 *         Commit ID: 3c577be6a7ef0697cae45fabe324d59e826d2342
 *********************************************************************************************************************/

#ifndef TIMGRSRVEXE_INCLUDE_ARA_PER_DETAIL_DYNAMIC_KEY_VALUE_STORAGE_DECL_H_
#define TIMGRSRVEXE_INCLUDE_ARA_PER_DETAIL_DYNAMIC_KEY_VALUE_STORAGE_DECL_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include <cstdint>

#include "vac/container/c_string_view.h"
#include "vac/container/string_literals.h"

namespace ara {
namespace per {
namespace detail {

// VECTOR NC AutosarC++17_10-M7.3.6: MD_ARAPER_using_in_header
// VECTOR NC AutosarC++17_10-M7.3.4: MD_ARAPER_AutosarC++17_10-A7.3.4_using_shall_not_be_used
using namespace vac::container::literals;  // NOLINT(build/namespaces)

/*!
 * \brief Enum that holds the valid data types
 */
enum class ValueType : std::uint8_t {
  kNotSet,
  kFloat,
  kDouble,
  kBool,
  kI8,
  kI16,
  kI32,
  kI64,
  kUi8,
  kUi16,
  kUi32,
  kUi64,
  kString,
  kBin
};

namespace jsontypes {
}  // namespace jsontypes

}  // namespace detail
}  // namespace per
}  // namespace ara

#endif  // TIMGRSRVEXE_INCLUDE_ARA_PER_DETAIL_DYNAMIC_KEY_VALUE_STORAGE_DECL_H_
