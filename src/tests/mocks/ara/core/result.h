#pragma once

namespace ara {
namespace core {

template<typename T>
class Result {
public:
    Result(T&& t) : value(std::move(t)) {}

    T&& Value() { return std::move(value); }
private:
    T value;
};
}
}