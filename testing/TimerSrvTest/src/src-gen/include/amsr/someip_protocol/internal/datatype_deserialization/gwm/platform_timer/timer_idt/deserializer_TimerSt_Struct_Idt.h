/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  TiMgrSrvExe/include/amsr/someip_protocol/internal/datatype_deserialization/gwm/platform_timer/timer_idt/deserializer_TimerSt_Struct_Idt.h
 *        \brief  SOME/IP protocol deserializer implementation for datatype 'TimerSt_Struct_Idt'
 *
 *      \details  /DataTypes/ImplementationDataTypes/TimerSt_Struct_Idt
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_someipprotocol
 *         Commit ID: 0e137b9e6356987a6d2839c8d79c286219b68d60
 *********************************************************************************************************************/

#ifndef TIMGRSRVEXE_INCLUDE_AMSR_SOMEIP_PROTOCOL_INTERNAL_DATATYPE_DESERIALIZATION_GWM_PLATFORM_TIMER_TIMER_IDT_DESERIALIZER_TIMERST_STRUCT_IDT_H_
#define TIMGRSRVEXE_INCLUDE_AMSR_SOMEIP_PROTOCOL_INTERNAL_DATATYPE_DESERIALIZATION_GWM_PLATFORM_TIMER_TIMER_IDT_DESERIALIZER_TIMERST_STRUCT_IDT_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "gwm/platform_timer/timer_idt/impl_type_timerst_struct_idt.h"
#include "someip-protocol/internal/deserialization/deser_wrapper.h"

namespace gwm {
namespace platform_timer {
namespace timer_idt {

// VECTOR NC Metric-HIS.PATH: MD_SOMEIPPROTOCOL_Metric-HIS.PATH
/*!
 * \brief Deserializer for datatype /DataTypes/ImplementationDataTypes/TimerSt_Struct_Idt.
 * \details Data type class: DataTypeStruct (CppImplDataTypeStruct).
 *          is TLV struct: false
 *
 * \tparam TpPack Transformation properties to be used for deserialization.
 *
 * \param[in,out] reader Reference to byte stream reader.
 * \param[out]    data Reference to the data object of type /DataTypes/ImplementationDataTypes/TimerSt_Struct_Idt
 *                in which the deserialized value will be written.
 * \return        True if the deserialization is successful, false otherwise.
 * \pre           The static size provided by SomeIpProtocolGetStaticSize has been verified.
 * \context       Reactor|App
 * \threadsafe    FALSE
 * \reentrant     TRUE for different reader objects.
 * \synchronous   TRUE
 */
template <typename TpPack>
amsr::someip_protocol::internal::deserialization::Result SomeIpProtocolDeserialize(amsr::someip_protocol::internal::deserialization::Reader& reader, ::gwm::platform_timer::timer_idt::TimerSt_Struct_Idt &data) noexcept {
  // Namespace alias for static deserialization code
  namespace deserialization = amsr::someip_protocol::internal::deserialization;
  // Deserialize non-TLV struct
  deserialization::Result result{false};
  // Deserialize struct member 'WakeSrcexpired_Idt' of type /DataTypes/ImplementationDataTypes/TimerSt_WakeSrcexpired_Enum_Idt
  result = deserialization::SomeIpProtocolDeserialize<
          TpPack,
          // Byte-order of primitive datatype (/DataTypes/ImplementationDataTypes/TimerSt_WakeSrcexpired_Enum_Idt)
        typename deserialization::Tp<TpPack>::ByteOrder

          >(reader, data.WakeSrcexpired_Idt);


  // Deserialize struct member 'WakeSource_Idt' of type /DataTypes/ImplementationDataTypes/TimerSt_WakeSource_Enum_Idt
  // VECTOR NL AutosarC++17_10-M0.1.2: MD_SOMEIPPROTOCOL_AutosarC++17_10-M0.1.2_dead_branch
  if (result) {
    result = deserialization::SomeIpProtocolDeserialize<
        TpPack,
        // Byte-order of primitive datatype (/DataTypes/ImplementationDataTypes/TimerSt_WakeSource_Enum_Idt)
        typename deserialization::Tp<TpPack>::ByteOrder

        >(reader, data.WakeSource_Idt);
  }

  return result;
}

/*!
 * \brief Calculates the static size for datatype /DataTypes/ImplementationDataTypes/TimerSt_Struct_Idt.
 * \details Data type class: DataTypeStruct (CppImplDataTypeStruct).
 *          is TLV struct: false
 *
 * \tparam TpPack Transformation properties to be used for deserialization.
 * \return Returns the static size of the struct in bytes.
 * \pre -
 * \context Reactor|App
 * \threadsafe FALSE
 * \reentrant TRUE
 * \vprivate Vector component internal API.
 * \synchronous TRUE
 */
template <typename TpPack>
constexpr std::size_t SomeIpProtocolGetStaticSize(amsr::someip_protocol::internal::deserialization::SizeToken<::gwm::platform_timer::timer_idt::TimerSt_Struct_Idt>) noexcept {
  // Namespace alias for static deserialization code
  namespace deserialization = amsr::someip_protocol::internal::deserialization;

  // Sum of static size
 constexpr std::size_t static_size{
  // Accumulate the static size of struct member 'WakeSrcexpired_Idt' of type /DataTypes/ImplementationDataTypes/TimerSt_WakeSrcexpired_Enum_Idt
   SomeIpProtocolGetStaticSize<TpPack,
                                // Byte-order of primitive datatype (/DataTypes/ImplementationDataTypes/TimerSt_WakeSrcexpired_Enum_Idt)
                                typename deserialization::Tp<TpPack>::ByteOrder

                               >(deserialization::SizeToken<::gwm::platform_timer::timer_idt::TimerSt_WakeSrcexpired_Enum_Idt>{})  + 
  
  // Accumulate the static size of struct member 'WakeSource_Idt' of type /DataTypes/ImplementationDataTypes/TimerSt_WakeSource_Enum_Idt
   SomeIpProtocolGetStaticSize<TpPack,
                                // Byte-order of primitive datatype (/DataTypes/ImplementationDataTypes/TimerSt_WakeSource_Enum_Idt)
                                typename deserialization::Tp<TpPack>::ByteOrder

                               >(deserialization::SizeToken<::gwm::platform_timer::timer_idt::TimerSt_WakeSource_Enum_Idt>{}) 
  };
  return static_size;
}

}  // namespace timer_idt
}  // namespace platform_timer
}  // namespace gwm

#endif  // TIMGRSRVEXE_INCLUDE_AMSR_SOMEIP_PROTOCOL_INTERNAL_DATATYPE_DESERIALIZATION_GWM_PLATFORM_TIMER_TIMER_IDT_DESERIALIZER_TIMERST_STRUCT_IDT_H_

