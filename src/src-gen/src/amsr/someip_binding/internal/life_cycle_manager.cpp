// VECTOR Same Line AutosarC++17_10-A1.1.1: MD_SOMEIPBINDING_AutosarC++17_10-A1.1.1_external_identifiers
/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  TimerSrvExe/src/amsr/someip_binding/internal/life_cycle_manager.cpp
 *        \brief  Initialization/Deinitialization functions for someip_binding
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_someipbinding
 *         Commit ID: 43b33f0283d4d61385b98a3599688499abd86a75
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "amsr/someip_binding/internal/life_cycle_manager.h"
#include <map>
#include <utility>
#include "amsr/socal/internal/service_discovery/proxy_service_discovery.h"
#include "amsr/socal/internal/service_discovery/service_discovery.h"
#include "amsr/someip_binding_transformation_layer/internal/gwm/application_remsdl/timersrv_si/SI_TimerSrv_skeleton_someip_binding.h"
#include "amsr/someip_binding_transformation_layer/internal/gwm/application_remsdl/timersrv_si/SI_TimerSrv_skeleton_someip_binding_init.h"
#include "amsr/someip_binding_transformation_layer/internal/gwm/platform/timer/gs2s/SI_TimerSrv_GS2S_proxy_xf.h"
#include "someip_binding_transformation_layer/internal/service_discovery/service_discovery_proxy_xf.h"
#include "someip_binding_transformation_layer/internal/service_discovery/service_discovery_skeleton_xf.h"

namespace amsr {
namespace someip_binding {
namespace internal {

LifeCycleManager::LifeCycleManager(std::unique_ptr<RuntimeInterface>& runtime) noexcept : runtime_{runtime} {}

::ara::core::Result<void> LifeCycleManager::Initialize(amsr::someip_binding::internal::configuration::SomeIpBindingConfig& someip_binding_config) noexcept
{
  osabstraction::io::reactor1::Reactor1* const reactor{&runtime_->GetReactor()};
  std::function<void()> process_polling_function{runtime_->GetProcessPollingFunction()};
  aracom_someip_binding_.emplace(someip_binding_config, process_polling_function, reactor);


  // Register all service instances into SOME/IP Binding.
  InitializeRequiredServiceInstances();
  InitializeProvidedServiceInstances();

  // Create all ara::com / Binding Transformation objects.
  InitializeSkeletonSomeIpEventBackends();
  InitializeServiceDiscoveryProxyXfs();
  InitializeServiceDiscoverySkeletonXfs();

  // Registers all service instances into Socal.
  RegisterServiceInstances();

  return ::ara::core::Result<void> {};
}

::ara::core::Result<void> LifeCycleManager::Deinitialize() noexcept {
  CleanInstanceSpecifierToInstanceIdMapping();

  DeInitializeServiceDiscoveryProxyXfs();
  sd_skeleton_xfs_.clear();
  DeInitializeSkeletonSomeIpEventBackends();

  aracom_someip_binding_.reset();

  return ::ara::core::Result<void> {};
}

// VECTOR NC AutosarC++17_10-M9.3.3, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M9.3.3_Method_can_be_declared_const
// VECTOR NC AutosarC++17_10-A15.5.3: MD_SOMEIPBINDING_AutosarC++17_10-A15.4.2_A15.5.3_Exception_caught
// VECTOR NC AutosarC++17_10-A15.4.2: MD_SOMEIPBINDING_AutosarC++17_10-A15.4.2_A15.5.3_Exception_caught
void LifeCycleManager::RegisterServiceInstances() noexcept {
  {
    // ---- Register all known R-Port InstanceSpecifiers ----
    {
      // Map R-Port /Applications/AdaptiveApplicationSwComponents/TimerSrvSwc/AdaptiveRequiredPortType_GS2STimerSrv to instance /ServiceInstances/TimerSrv/SomeIpSI_TimerSrv_GS2S_interfaceDeploymentRequiredInstance 
      ::ara::core::InstanceSpecifier const instance_specifier{"TimerSrvExe/RootSwComponentPrototype/AdaptiveRequiredPortType_GS2STimerSrv"_sv};
      ::ara::com::InstanceIdentifier const instance_identifier{"SomeIp:1"_sv};

        runtime_->MapRequiredInstanceSpecifierToInstanceId(
            vac::container::CStringView{"SomeIp"_sv}, instance_specifier, instance_identifier,
            "/S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/SI_TimerSrv_GS2S"_sv);
    }
  }
  {
    // ---- Register all known P-Port InstanceSpecifiers ----
    {
      // Map P-Port /Applications/AdaptiveApplicationSwComponents/TimerSrvSwc/AdaptiveProvidedPortType_TimerSrv to instance /ServiceInstances/TimerSrv/SomeIpSI_TimerSrvProvidedInstance
      ::ara::core::InstanceSpecifier const instance_specifier{"TimerSrvExe/RootSwComponentPrototype/AdaptiveProvidedPortType_TimerSrv"_sv};
      ::ara::com::InstanceIdentifier const instance_identifier{"SomeIp:1"_sv};

      runtime_->MapProvidedInstanceSpecifierToInstanceId(
            vac::container::CStringView{"SomeIp"_sv}, instance_specifier, instance_identifier,
            "/ServiceInterfaces/SI_TimerSrv"_sv);
    }
  }
}

// VECTOR NC AutosarC++17_10-M9.3.3, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M9.3.3_Method_can_be_declared_const
void LifeCycleManager::InitializeSkeletonSomeIpEventBackends() noexcept {
  // Initialize skeleton event backends for ServiceInterface '/ServiceInterfaces/SI_TimerSrv'
  ::amsr::someip_binding_transformation_layer::internal::gwm::application_remsdl::timersrv_si::
      AraComSomeIpBindingInitializeSkeletonSomeIpEventBackendsSI_TimerSrv(aracom_someip_binding_->GetServerManager());
}

// VECTOR NC AutosarC++17_10-M9.3.3, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M9.3.3_Method_can_be_declared_const
void LifeCycleManager::DeInitializeSkeletonSomeIpEventBackends() noexcept {
  // Initialize skeleton event backends for ServiceInterface '/ServiceInterfaces/SI_TimerSrv'
  ::amsr::someip_binding_transformation_layer::internal::gwm::application_remsdl::timersrv_si::
      AraComSomeIpBindingDeInitializeSkeletonSomeIpEventBackendsSI_TimerSrv();
}

// VECTOR NC AutosarC++17_10-M9.3.3, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M9.3.3_Method_can_be_declared_static
// VECTOR NC AutosarC++17_10-M0.1.8, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M0.1.8_Void_function_has_no_external_side_effect
void LifeCycleManager::InitializeRequiredServiceInstances() noexcept {
  aracom_someip_binding_->EmplaceRequiredServiceInstance(::amsr::someip_protocol::internal::ServiceId{0x3AE7U},
                                                        ::amsr::someip_protocol::internal::MajorVersion{1U},
                                                        ::amsr::someip_protocol::internal::MinorVersion{4294967295U},
                                                        ::amsr::someip_protocol::internal::InstanceId{1U});
}

// VECTOR NC AutosarC++17_10-M9.3.3, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M9.3.3_Method_can_be_declared_const
// VECTOR NC AutosarC++17_10-M0.1.8, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M0.1.8_Void_function_has_no_external_side_effect
void LifeCycleManager::InitializeProvidedServiceInstances() noexcept {
    // Register provided service for instance /ServiceInstances/TimerSrv/SomeIpSI_TimerSrvProvidedInstance
    {
      ::amsr::someip_binding::internal::ProvidedServiceInstanceId const SI_TimerSrv_provided_service_instance_id{ static_cast<::amsr::someip_protocol::internal::ServiceId>(0x9201),
                                                                                                                                                        1, 0, 0x1 };
      aracom_someip_binding_->GetServerManager().RegisterProvidedServiceInstance(SI_TimerSrv_provided_service_instance_id);
    }
}

// VECTOR NC AutosarC++17_10-M9.3.3, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M9.3.3_Method_can_be_declared_const
// VECTOR NC AutosarC++17_10-M3.2.1, Linker-Return_Type_Mismatch: MD_SOMEIPBINDING_AutosarC++17_10-M3.2.1_Return_type_mismatch
LifeCycleManager::SomeIpBindingType& LifeCycleManager::GetBinding() noexcept {
  if (!aracom_someip_binding_.has_value()) {
    ::ara::core::Abort("SOME/IP Binding has never been initialized.");
  }
  return aracom_someip_binding_.value();
}

std::map<::amsr::someip_binding::internal::RequiredServiceInstanceId, LifeCycleManager::E2EPropsMap> LifeCycleManager::ReadE2EPropsMaps() noexcept {
  std::map<::amsr::someip_binding::internal::RequiredServiceInstanceId, E2EPropsMap> required_e2e_props_map{};
  {
    // Required Service Instance ID
    ::amsr::someip_protocol::internal::ServiceId const service_id{static_cast<::amsr::someip_protocol::internal::ServiceId>(0x3AE7)};
    ::amsr::someip_protocol::internal::MajorVersion const major_version{1};
    ::amsr::someip_protocol::internal::MinorVersion const minor_version{static_cast<::amsr::someip_protocol::internal::MinorVersion>(4294967295)};
    ::amsr::someip_protocol::internal::InstanceId const instance_id{1};
    ::amsr::someip_binding::internal::RequiredServiceInstanceId const required_si_id{service_id, major_version, minor_version, instance_id};

    // Create E2E props map. This map shall contain all E2E props for all events of this instance.
    // Key: event ID, Value: E2E protection properties.
    E2EPropsMap e2e_props_map{};

    SomeIpBindingXfInitializer::EmplaceRequiredServiceInstanceConfig(required_e2e_props_map, required_si_id, e2e_props_map);
  }

  return std::move(required_e2e_props_map);
}

void LifeCycleManager::InitializeServiceDiscoveryProxyXfs() noexcept {
  std::map<::amsr::someip_binding::internal::RequiredServiceInstanceId, E2EPropsMap> const required_e2e_props_map{
      ReadE2EPropsMaps()};

  {  // Instantiate ServiceDiscoveryProxyXfs for Service 0x3AE7
    // Service ID
    ::amsr::someip_protocol::internal::ServiceId const service_id{static_cast<::amsr::someip_protocol::internal::ServiceId>(0x3AE7)};
    ::amsr::someip_protocol::internal::MajorVersion const major_version{1};

    // Filter out related instances
    std::map<::amsr::someip_binding::internal::RequiredServiceInstanceId, E2EPropsMap> const
        service_deployment_required_e2e_props_map{SomeIpBindingXfInitializer::FilterServiceDeploymentConfig(
           required_e2e_props_map, service_id, major_version)};

    {
      // Create & initialize SD Proxy XF
      SomeIpBindingXfInitializer::InitializeSdProxyXfs<
          ::gwm::platform::timer::gs2s::proxy::SI_TimerSrv_GS2SProxy,
          ::gwm::platform::timer::gs2s::internal::SI_TimerSrv_GS2SProxyBackendInterface,
          ::amsr::someip_binding_transformation_layer::internal::gwm::platform::timer::gs2s::SI_TimerSrv_GS2SProxyXf>(
          sd_proxy_xfs_, service_deployment_required_e2e_props_map, aracom_someip_binding_->GetClientManager(),
          runtime_->GetThreadPoolInterface());
    }
  }
}

void LifeCycleManager::DeInitializeServiceDiscoveryProxyXfs() noexcept {
  // Deregister ServiceDiscoveryProxyXfs for instance /ServiceInstances/TimerSrv/SomeIpSI_TimerSrv_GS2S_interfaceDeploymentRequiredInstance
  {
    // Type definitions
    using ProxyBackendInterfaceType =
         ::gwm::platform::timer::gs2s::internal::SI_TimerSrv_GS2SProxyBackendInterface;
    using ProxyServiceDiscoveryType = ::amsr::socal::internal::service_discovery::ProxyServiceDiscovery<ProxyBackendInterfaceType>;

    // Required Service Instance ID
    ::ara::com::InstanceIdentifier const instance_identifier{"SomeIp:1"_sv};

    // Service Discovery singleton
    ::amsr::generic::Singleton<ProxyServiceDiscoveryType*>& service_discovery{::gwm::platform::timer::gs2s::proxy::SI_TimerSrv_GS2SProxy::GetServiceDiscovery()};

    // Deregister from Socal
    ::ara::core::Result<void> const result{(*(service_discovery.GetAccess()))->DeregisterBindingXfFactory(instance_identifier)};
    if (!result.HasValue()) {
      ara::core::Abort("Unable to deregister SOME/IP SdProxyXf from socal");
    }
  }

  // Destroy all SdProxyXf instances
  sd_proxy_xfs_.clear();
}


void LifeCycleManager::InitializeServiceDiscoverySkeletonXfs() noexcept {

  // Instantiate ServiceDiscoverySkeletonXfs for instance /ServiceInstances/TimerSrv/SomeIpSI_TimerSrvProvidedInstance
  {
    // Types.
    using SkeletonXfType =
        ::amsr::someip_binding_transformation_layer::internal::gwm::application_remsdl::timersrv_si::SI_TimerSrvSkeletonSomeIpBinding;
    using SkeletonBackendInterfaceType =
        ::gwm::application_remsdl::timersrv_si::skeleton::SI_TimerSrvSkeleton;
    using ServiceDiscoveryType =
        ::amsr::socal::internal::service_discovery::ServiceDiscovery<SkeletonBackendInterfaceType>;
    using SdSkeletonXfType =
        ::amsr::someip_binding_transformation_layer::internal::service_discovery::ServiceDiscoverySkeletonXf<
            SkeletonXfType,
            SkeletonBackendInterfaceType,
            ::amsr::someip_binding::internal::ServerManager<>,
            ServiceDiscoveryType>;

    // Variables.
    ::amsr::someip_binding::internal::ProvidedServiceInstanceId const provided_service_instance_id{static_cast<::amsr::someip_protocol::internal::ServiceId>(0x9201),
         1, 0, 0x1 };

    ::ara::com::InstanceIdentifier const instance_identifier{"SomeIp:1"_sv};

    ::amsr::someip_binding::internal::ServerManager<
        amsr::someip_daemon_client::internal::SkeletonSomeIpDaemonClient<
            amsr::someip_daemon_client::internal::SomeIpDaemonClientDefaultTemplateConfiguration>>& server_manager{
        aracom_someip_binding_->GetServerManager()};

    ::amsr::generic::Singleton<::amsr::socal::internal::service_discovery::ServiceDiscovery<
        ::gwm::application_remsdl::timersrv_si::skeleton::SI_TimerSrvSkeleton>*>& service_discovery {
            ::gwm::application_remsdl::timersrv_si::skeleton::SI_TimerSrvSkeleton::GetServiceDiscovery()};

    // Add the new entry to the container of XF's.
    sd_skeleton_xfs_.emplace_back(std::make_unique<SdSkeletonXfType>(
        provided_service_instance_id, instance_identifier, server_manager, service_discovery));
  }
}

// VECTOR NC AutosarC++17_10-M9.3.3, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M9.3.3_Method_can_be_declared_const
// VECTOR NC AutosarC++17_10-A15.5.3: MD_SOMEIPBINDING_AutosarC++17_10-A15.4.2_A15.5.3_Exception_caught
// VECTOR NC AutosarC++17_10-A15.4.2: MD_SOMEIPBINDING_AutosarC++17_10-A15.4.2_A15.5.3_Exception_caught
void LifeCycleManager::CleanInstanceSpecifierToInstanceIdMapping() noexcept {
  {
    // Remove mapping R-Port /Applications/AdaptiveApplicationSwComponents/TimerSrvSwc/AdaptiveRequiredPortType_GS2STimerSrv to instance /ServiceInstances/TimerSrv/SomeIpSI_TimerSrv_GS2S_interfaceDeploymentRequiredInstance 
    ::ara::core::InstanceSpecifier const instance_specifier{"TimerSrvExe/RootSwComponentPrototype/AdaptiveRequiredPortType_GS2STimerSrv"_sv};
    ::ara::com::InstanceIdentifier const instance_identifier{"SomeIp:1"_sv};

    runtime_->RemoveRequiredInstanceSpecifierEntry(instance_specifier, instance_identifier);
  }
  {
    // Remove mapping P-Port /Applications/AdaptiveApplicationSwComponents/TimerSrvSwc/AdaptiveProvidedPortType_TimerSrv to instance /ServiceInstances/TimerSrv/SomeIpSI_TimerSrvProvidedInstance
    ::ara::core::InstanceSpecifier const instance_specifier{"TimerSrvExe/RootSwComponentPrototype/AdaptiveProvidedPortType_TimerSrv"_sv};
    ::ara::com::InstanceIdentifier const instance_identifier{"SomeIp:1"_sv};

    runtime_->RemoveProvidedInstanceSpecifierEntry(instance_specifier, instance_identifier);
  }
}

}  // namespace internal
}  // namespace someip_binding
}  // namespace amsr

