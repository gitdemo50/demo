/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  TimerSrvExe/include/gwm/application_remsdl/timersrv_si/si_timersrv_common.h
 *        \brief  Header for service 'SI_TimerSrv'.
 *
 *      \details  This Service interface provides timer
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_socal
 *         Commit ID: 209b3c0234013e84e3170ee826c9a8c6022de194
 *********************************************************************************************************************/

#ifndef TIMERSRVEXE_INCLUDE_GWM_APPLICATION_REMSDL_TIMERSRV_SI_SI_TIMERSRV_COMMON_H_
#define TIMERSRVEXE_INCLUDE_GWM_APPLICATION_REMSDL_TIMERSRV_SI_SI_TIMERSRV_COMMON_H_

/*!
 * \trace SPEC-4980247, SPEC-4980248, SPEC-5951130, SPEC-4980251
 */
/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "ara/com/types.h"
#include "gwm/application_remsdl/timersrv_si/SI_TimerSrv.h"
#include "gwm/platform_timer/timer_idt/impl_type_timerset_integer_idt.h"
#include "gwm/platform_timer/timer_idt/impl_type_timerset_response_enum_idt.h"
#include "gwm/platform_timer/timer_idt/impl_type_timerst_struct_idt.h"

#endif  // TIMERSRVEXE_INCLUDE_GWM_APPLICATION_REMSDL_TIMERSRV_SI_SI_TIMERSRV_COMMON_H_
