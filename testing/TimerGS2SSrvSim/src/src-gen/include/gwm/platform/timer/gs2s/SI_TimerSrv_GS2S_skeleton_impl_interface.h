/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/include/gwm/platform/timer/gs2s/SI_TimerSrv_GS2S_skeleton_impl_interface.h
 *        \brief  Skeleton implementation interface of service 'SI_TimerSrv_GS2S'.
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_socal
 *         Commit ID: 209b3c0234013e84e3170ee826c9a8c6022de194
 *********************************************************************************************************************/

#ifndef DENPENDENCYSRVEXE_INCLUDE_GWM_PLATFORM_TIMER_GS2S_SI_TIMERSRV_GS2S_SKELETON_IMPL_INTERFACE_H_
#define DENPENDENCYSRVEXE_INCLUDE_GWM_PLATFORM_TIMER_GS2S_SI_TIMERSRV_GS2S_SKELETON_IMPL_INTERFACE_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "amsr/socal/internal/events/skeleton_event_manager_interface.h"
#include "gwm/platform/timer/gs2s/si_timersrv_gs2s_common.h"

namespace gwm {
namespace platform {
namespace timer {
namespace gs2s {
namespace internal {

/*!
 * \brief Skeleton implementation interface of service 'SI_TimerSrv_GS2S'
 */
class SI_TimerSrv_GS2SSkeletonImplInterface {
 public:
 /*!
   * \brief Define default constructor.
   * \pre -
   * \context App
   */
  SI_TimerSrv_GS2SSkeletonImplInterface() noexcept = default;

  /*!
   * \brief Use default destructor
   */
  virtual ~SI_TimerSrv_GS2SSkeletonImplInterface() noexcept = default;

 protected:

  /*!
   * \brief Use default move constructor
   * \pre -
   * \context App
   */
  SI_TimerSrv_GS2SSkeletonImplInterface(SI_TimerSrv_GS2SSkeletonImplInterface &&) noexcept = default;

  /*!
   * \brief Use default move assignment
   * \pre -
   * \context App
   */
  SI_TimerSrv_GS2SSkeletonImplInterface &operator=(SI_TimerSrv_GS2SSkeletonImplInterface &&) & noexcept = default;

  SI_TimerSrv_GS2SSkeletonImplInterface(SI_TimerSrv_GS2SSkeletonImplInterface const &) = delete;

  SI_TimerSrv_GS2SSkeletonImplInterface &operator=(SI_TimerSrv_GS2SSkeletonImplInterface const &) & = delete;

 public:

  // ---- Events ---------------------------------------------------------------------------------------------------

  // ---- Fields ---------------------------------------------------------------------------------------------------

  /*!
   * \brief Get the event manager object for the field notifier of field 'TimerSt'.
   * \details Field data type: ::datatypes::gs2s::timersrv_gs2s::TimerSt_Struct.
   * \return A binding-specific event management object/interface supporting field notifier updates.
   * \pre -
   * \context App
   */
   virtual ::amsr::socal::internal::events::SkeletonEventManagerInterface<::datatypes::gs2s::timersrv_gs2s::TimerSt_Struct>* GetFieldNotifierTimerSt() noexcept = 0;
};

} // namespace internal
}  // namespace gs2s
}  // namespace timer
}  // namespace platform
}  // namespace gwm

#endif  // DENPENDENCYSRVEXE_INCLUDE_GWM_PLATFORM_TIMER_GS2S_SI_TIMERSRV_GS2S_SKELETON_IMPL_INTERFACE_H_

