
/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  TiMgrSrvExe/include/gwm/composite_timermgr/timgrsrv_si/SI_TiMgrSrv_types.h
 *        \brief  Input and output structures for methods, fields and application errors of service 'SI_TiMgrSrv'
 *
 *      \details  Definition of common input-/output structs used for simplified argument / marshalling handling. For all elements like methods, events fields structs with the related in-/output arguments are generated.
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_socal
 *         Commit ID: 209b3c0234013e84e3170ee826c9a8c6022de194
 *********************************************************************************************************************/

#ifndef TIMGRSRVEXE_INCLUDE_GWM_COMPOSITE_TIMERMGR_TIMGRSRV_SI_SI_TIMGRSRV_TYPES_H_
#define TIMGRSRVEXE_INCLUDE_GWM_COMPOSITE_TIMERMGR_TIMGRSRV_SI_SI_TIMGRSRV_TYPES_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "amsr/socal/internal/handle_type.h"
#include "gwm/composite_timermgr/timgrsrv_si/si_timgrsrv_common.h"

namespace gwm {
namespace composite_timermgr {
namespace timgrsrv_si {

namespace internal {

/*!
 * \brief Proxy HandleType for the Service 'SI_TiMgrSrv'.
 * \trace SPEC-4980259
 */
class SI_TiMgrSrvHandleType final : public ::amsr::socal::internal::HandleType {
  public:
  /*!
   * \brief Inherit constructor.
   */
  using HandleType::HandleType;
};

namespace methods {

/*!
 * \brief A class for service method 'AlarmClkCncl' used for type definitions.
 */
class AlarmClkCncl final {
 public:
  /*!
   * \brief Struct representing all output arguments of the service method.
   */
  struct Output {
    /*!
     * \brief Reference of output argument 'Response' (/DataTypes/ImplementationDataTypes/AlrmClk_Response_Enum_Idt)
     */
    ::gwm::composite_timermgr::timermgr_idt::AlrmClk_Response_Enum_Idt Response;
  };

  /*!
   * \brief Struct representing all input arguments of the service method.
   */
  struct Input {
    /*!
     * \brief Reference of input argument 'ClntID' (/DataTypes/ImplementationDataTypes/AlrmClk_ClntID_Enum_Idt)
     */
    ::gwm::composite_timermgr::timermgr_idt::AlrmClk_ClntID_Enum_Idt ClntID;
  };
};

/*!
 * \brief A class for service method 'AlrmClkReq' used for type definitions.
 */
class AlrmClkReq final {
 public:
  /*!
   * \brief Struct representing all output arguments of the service method.
   */
  struct Output {
    /*!
     * \brief Reference of output argument 'Response' (/DataTypes/ImplementationDataTypes/AlrmClk_Response_Enum_Idt)
     */
    ::gwm::composite_timermgr::timermgr_idt::AlrmClk_Response_Enum_Idt Response;
  };

  /*!
   * \brief Struct representing all input arguments of the service method.
   */
  struct Input {
    /*!
     * \brief Reference of input argument 'ClntID' (/DataTypes/ImplementationDataTypes/AlrmClk_ClntID_Enum_Idt)
     */
    ::gwm::composite_timermgr::timermgr_idt::AlrmClk_ClntID_Enum_Idt ClntID;
    /*!
     * \brief Reference of input argument 'AlrmClkFrq' (/DataTypes/ImplementationDataTypes/AlrmClkFrq_Enum_Idt)
     */
    ::gwm::composite_timermgr::timermgr_idt::AlrmClkFrq_Enum_Idt AlrmClkFrq;
    /*!
     * \brief Reference of input argument 'AlrmClkBgnTi' (/DataTypes/ImplementationDataTypes/AlrmClkBgnTi_Struct_Idt)
     */
    ::gwm::composite_timermgr::timermgr_idt::AlrmClkBgnTi_Struct_Idt AlrmClkBgnTi;
    /*!
     * \brief Reference of input argument 'AlrmClkEndTi' (/DataTypes/ImplementationDataTypes/AlrmClkEndTi_Struct_Idt)
     */
    ::gwm::composite_timermgr::timermgr_idt::AlrmClkEndTi_Struct_Idt AlrmClkEndTi;
  };
};

}  // namespace methods

namespace fields {

/*!
 * \brief Data class for service field 'AlrmClkWkeEve'.
 * \remark generated
 */
class AlrmClkWkeEve final {
 public:
  /*!
   * \brief Return/output parameters of service field 'AlrmClkWkeEve'
   */
  using Output = ::gwm::composite_timermgr::timermgr_idt::AlrmClkWkeEve_Enum_Idt;
};


  /*!
 * \brief A class for field method 'AlrmClkWkeEve'Get used for type definitions.
 */
class AlrmClkWkeEveGet final {
 public:
  /*!
   * \brief Struct representing the field input data.
   */
  struct Input {};

  /*!
   * \brief Struct representing the field output value.
   */
  struct Output {
    /*!
     * \brief Reference of field value (/DataTypes/ImplementationDataTypes/AlrmClkWkeEve_Enum_Idt)
     */
    ::gwm::composite_timermgr::timermgr_idt::AlrmClkWkeEve_Enum_Idt out_val;
  };
};


}  // namespace fields
}  // namespace internal

}  //  namespace timgrsrv_si
}  //  namespace composite_timermgr
}  //  namespace gwm

#endif  // TIMGRSRVEXE_INCLUDE_GWM_COMPOSITE_TIMERMGR_TIMGRSRV_SI_SI_TIMGRSRV_TYPES_H_

