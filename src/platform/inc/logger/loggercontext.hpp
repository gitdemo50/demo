#ifndef GWM_PLATFORM_LOGGER_CONTEXT_HPP
#define GWM_PLATFORM_LOGGER_CONTEXT_HPP

template<typename T>
class LoggerWrapper;

#ifdef USE_ARA_LOGGER
#include "impl/araloggercontext.hpp"
#else
#include "impl/defaultloggercontext.hpp"
#endif

#endif