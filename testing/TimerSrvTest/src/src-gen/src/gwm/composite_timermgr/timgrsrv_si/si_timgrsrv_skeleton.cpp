/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  TiMgrSrvExe/src/gwm/composite_timermgr/timgrsrv_si/si_timgrsrv_skeleton.cpp
 *        \brief  Skeleton for service 'SI_TiMgrSrv'.
 *
 *      \details  This Service interface provides the timer manager request
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_socal
 *         Commit ID: 209b3c0234013e84e3170ee826c9a8c6022de194
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "gwm/composite_timermgr/timgrsrv_si/si_timgrsrv_skeleton.h"
#include "amsr/socal/internal/instance_specifier_lookup_table.h"

/*!
 * \trace SPEC-4980240
 * \trace SPEC-4980241
 */
namespace gwm {
namespace composite_timermgr {
namespace timgrsrv_si {
namespace skeleton {

/*!
 * \brief Static instance of service discovery.
 */
// VECTOR NC AutosarC++17_10-A3.3.2: MD_SOCAL_AutosarC++17_10-A3.3.2_StaticStorageDurationOfNonPODType
::amsr::generic::Singleton<SI_TiMgrSrvSkeleton::ServiceDiscovery*> SI_TiMgrSrvSkeleton::sd_;

SI_TiMgrSrvSkeleton::ConstructionResult SI_TiMgrSrvSkeleton::Preconstruct(
    ara::com::InstanceIdentifier instance_id, ara::com::MethodCallProcessingMode mode) noexcept {
return Base::Preconstruct(instance_id, mode);
}

SI_TiMgrSrvSkeleton::ConstructionResult SI_TiMgrSrvSkeleton::Preconstruct(
    ara::core::InstanceSpecifier instance, ara::com::MethodCallProcessingMode mode) noexcept {
return Base::Preconstruct(instance, mode);
}

SI_TiMgrSrvSkeleton::ConstructionResult SI_TiMgrSrvSkeleton::Preconstruct(
    ara::com::InstanceIdentifierContainer instance_identifiers, ara::com::MethodCallProcessingMode mode) noexcept {
return Base::Preconstruct(instance_identifiers, mode);
}

SI_TiMgrSrvSkeleton::SI_TiMgrSrvSkeleton(ConstructionToken&& token) noexcept
: Base{std::move(token)}
, AlrmClkWkeEve(this)
 {}

SI_TiMgrSrvSkeleton::SI_TiMgrSrvSkeleton(ara::com::InstanceIdentifier instance,
                                               ara::com::MethodCallProcessingMode mode) noexcept
     : SI_TiMgrSrvSkeleton{Preconstruct(instance, mode).Value()} {}

SI_TiMgrSrvSkeleton::SI_TiMgrSrvSkeleton(ara::core::InstanceSpecifier instance,
                                               ara::com::MethodCallProcessingMode mode) noexcept
     : SI_TiMgrSrvSkeleton{Preconstruct(instance, mode).Value()} {}

SI_TiMgrSrvSkeleton::SI_TiMgrSrvSkeleton(ara::com::InstanceIdentifierContainer instance_identifiers,
                                               ara::com::MethodCallProcessingMode mode) noexcept
     : SI_TiMgrSrvSkeleton{Preconstruct(instance_identifiers, mode).Value()} {}

SI_TiMgrSrvSkeleton::~SI_TiMgrSrvSkeleton() noexcept {
  // Next line might block until all running method requests are done.
  StopOfferService();
}

void SI_TiMgrSrvSkeleton::DoFieldInitializationChecks() noexcept {

    if (!AlrmClkWkeEve.IsUpdated()) {
        logger_.LogError([this](ara::log::LogStream& s) {
                          s << "Failed to offer service 'SI_TiMgrSrv' with instance id(s) '";
                          for (::amsr::socal::internal::InstanceSpecifierLookupTableEntry const& entry : offered_instances_) {
                              ara::core::StringView const instance_id_str{entry.GetInstanceIdentifier().ToString()};
                              s << " " << instance_id_str;
                          }
                          s << "' - No initial value has been set for field 'AlrmClkWkeEve' (AlrmClkWkeEve::Update(...) has never been called)."; },
                          __func__, __LINE__);
        ara::core::Abort("No initial value has been set for field 'AlrmClkWkeEve' (AlrmClkWkeEve::Update(...) has never been called).");
    }
}

void SI_TiMgrSrvSkeleton::SendInitialFieldNotifications() noexcept {
  // Send initial field events for all fields with "hasNotifier = true"
  AlrmClkWkeEve.SendInitialValue();
}

void SI_TiMgrSrvSkeleton::OfferServiceInternal(::ara::com::InstanceIdentifier const& instance_id) noexcept {
  (*(sd_.GetAccess()))->OfferService(instance_id, this);
}

void SI_TiMgrSrvSkeleton::StopOfferServiceInternal(::ara::com::InstanceIdentifier const& instance_id) noexcept {
  (*(sd_.GetAccess()))->StopOfferService(instance_id);
}

::amsr::generic::Singleton<SI_TiMgrSrvSkeleton::ServiceDiscovery*>& SI_TiMgrSrvSkeleton::GetServiceDiscovery() noexcept {
  return sd_;
}

void SI_TiMgrSrvSkeleton::RegisterServiceDiscovery(SI_TiMgrSrvSkeleton::ServiceDiscovery* service_discovery) noexcept {
  sd_.Create(service_discovery);
}

void SI_TiMgrSrvSkeleton::DeRegisterServiceDiscovery() noexcept { sd_.Destroy(); }

}  // namespace skeleton
}  // namespace timgrsrv_si
}  // namespace composite_timermgr
}  // namespace gwm

