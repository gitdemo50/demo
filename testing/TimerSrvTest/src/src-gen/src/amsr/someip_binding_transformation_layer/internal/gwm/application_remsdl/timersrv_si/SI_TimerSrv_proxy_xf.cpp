// VECTOR Same Line AutosarC++17_10-A1.1.1: MD_SOMEIPBINDING_AutosarC++17_10-A1.1.1_external_identifiers
/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  TiMgrSrvExe/src/amsr/someip_binding_transformation_layer/internal/gwm/application_remsdl/timersrv_si/SI_TimerSrv_proxy_xf.cpp
 *        \brief  SOME/IP proxy binding of service 'SI_TimerSrv'
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_someipbinding
 *         Commit ID: 43b33f0283d4d61385b98a3599688499abd86a75
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "amsr/someip_binding_transformation_layer/internal/gwm/application_remsdl/timersrv_si/SI_TimerSrv_proxy_xf.h"
#include <tuple>
#include <utility>
#include "gwm/application_remsdl/timersrv_si/si_timersrv_proxy.h"
#include "osabstraction/io/io_buffer.h"
#include "someip_binding/internal/client_manager_interface.h"
#include "vac/memory/memory_buffer.h"


namespace amsr {
namespace someip_binding_transformation_layer {
namespace internal {

namespace gwm {
namespace application_remsdl {
namespace timersrv_si {

// ---- Methods --------------------------------------------------------------------------------------------------
constexpr ::amsr::someip_protocol::internal::MethodId gwm::application_remsdl::timersrv_si::methods::timercncl::ProxyConfiguration::kMethodId;
constexpr ::ara::core::StringView gwm::application_remsdl::timersrv_si::methods::timercncl::ProxyConfiguration::kName;
constexpr ::amsr::someip_protocol::internal::MethodId gwm::application_remsdl::timersrv_si::methods::timerreq::ProxyConfiguration::kMethodId;
constexpr ::ara::core::StringView gwm::application_remsdl::timersrv_si::methods::timerreq::ProxyConfiguration::kName;

// ---- Fields ---------------------------------------------------------------------------------------------------
constexpr ::amsr::someip_protocol::internal::MethodId gwm::application_remsdl::timersrv_si::fields::timerstget::ProxyConfiguration::kMethodId;
constexpr ::ara::core::StringView gwm::application_remsdl::timersrv_si::fields::timerstget::ProxyConfiguration::kName;


SI_TimerSrvProxyXf::SI_TimerSrvProxyXf(
    ::amsr::someip_protocol::internal::ServiceId const service_id,
    ::amsr::someip_protocol::internal::MajorVersion const major_version,
    ::amsr::someip_protocol::internal::InstanceId const instance_id,
    ::amsr::someip_protocol::internal::ClientId const client_id,
    ::amsr::someip_binding::internal::ClientManagerInterface& client_manager,
    ::amsr::someip_binding::internal::ThreadPoolInterface& thread_pool_interface,
    E2EPropsMap const& e2e_props_map)
    : service_id_(service_id),
      major_version_(major_version),
      instance_id_(instance_id),
      client_id_(client_id),
      client_manager_(client_manager),
      thread_pool_interface_{thread_pool_interface},
      e2e_props_map_{e2e_props_map} {
   // Register this proxy binding for method responses and event notifications.
   ::amsr::someip_binding::internal::ProxyBindingIdentity const proxy_identity{
        std::make_tuple(service_id, instance_id, client_id)};

   client_manager_.RegisterProxyBinding(proxy_identity, client_);
}

SI_TimerSrvProxyXf::~SI_TimerSrvProxyXf() noexcept {
  client_manager_.DeRegisterProxyBinding( std::make_tuple(service_id_, instance_id_, client_id_));
}

void SI_TimerSrvProxyXf::HandleMethodResponse(
    ::amsr::someip_protocol::internal::SomeIpMessageHeader const& header,
    ::amsr::someip_protocol::internal::SomeIpMessage  packet) {
  // Get method ID from header
  ::amsr::someip_protocol::internal::MethodId const method_id{header.method_id_};

  // Based on the method id -> static dispatching to the method request/response manager
  switch (method_id) {
    case methods::timercncl::ProxyConfiguration::kMethodId: {
      method_TimerCncl_.HandleMethodResponse(header, std::move(packet));
      break;
    }
    case methods::timerreq::ProxyConfiguration::kMethodId: {
      method_TimerReq_.HandleMethodResponse(header, std::move(packet));
      break;
    }
    case fields::timerstget::ProxyConfiguration::kMethodId: {
      field_manager_TimerSt_get_.HandleMethodResponse(header, std::move(packet));
      break;
    }
    default: {
      logger_.LogError([&method_id](ara::log::LogStream& s) {
                        s << "Method response handling implementation for SOME/IP method ID " << method_id
                        << " is missing. Please re-configure."; },
                        __func__, __LINE__);
      break;
    }
  }
}

void SI_TimerSrvProxyXf::OnServiceDown() {
      method_TimerCncl_.OnServiceDown();
      method_TimerReq_.OnServiceDown();
}

}  // namespace internal
}  // namespace someip_binding_transformation_layer
}  // namespace amsr
}  // namespace timersrv_si
}  // namespace application_remsdl
}  // namespace gwm

