/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  TiMgrSrvExe/include/gwm/application_remsdl/timersrv_si/SI_TimerSrv_proxy_backend_interface.h
 *        \brief  Proxy implementation interface of service 'SI_TimerSrv'.
 *
 *      \details  This Service interface provides timer
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_socal
 *         Commit ID: 209b3c0234013e84e3170ee826c9a8c6022de194
 *********************************************************************************************************************/

#ifndef TIMGRSRVEXE_INCLUDE_GWM_APPLICATION_REMSDL_TIMERSRV_SI_SI_TIMERSRV_PROXY_BACKEND_INTERFACE_H_
#define TIMGRSRVEXE_INCLUDE_GWM_APPLICATION_REMSDL_TIMERSRV_SI_SI_TIMERSRV_PROXY_BACKEND_INTERFACE_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "amsr/socal/internal/events/proxy_event_backend_interface.h"
#include "ara/core/future.h"
#include "gwm/application_remsdl/timersrv_si/SI_TimerSrv_types.h"

namespace gwm {
namespace application_remsdl {
namespace timersrv_si {
namespace internal {

/*!
 * \brief Proxy backend interface for the Service 'SI_TimerSrv'
 * \remark generated
 */
class SI_TimerSrvProxyBackendInterface {
 public:

  /*!
   * \brief Define default constructor.
   * \pre -
   * \context App
   * \synchronus TRUE
   */
  SI_TimerSrvProxyBackendInterface() noexcept = default;

  /*!
   * \brief Define default destructor.
   * \pre -
   * \context App
   * \synchronus TRUE
   */
  virtual ~SI_TimerSrvProxyBackendInterface() noexcept = default;

  SI_TimerSrvProxyBackendInterface(SI_TimerSrvProxyBackendInterface const &) = delete;

  SI_TimerSrvProxyBackendInterface(SI_TimerSrvProxyBackendInterface &&) = delete;

  SI_TimerSrvProxyBackendInterface &operator=(SI_TimerSrvProxyBackendInterface const &) & = delete;

  SI_TimerSrvProxyBackendInterface &operator=(SI_TimerSrvProxyBackendInterface &&) & = delete;

  // ---- Methods --------------------------------------------------------------------------------------------------

  /*!
   * \brief Invokes request response method TimerCncl.
   * \return ara::core::Future with method output data element.
   * \pre -
   * \context       App
   * \synchronous   FALSE
   */
  virtual ara::core::Future<methods::TimerCncl::Output> HandleMethodTimerCncl() noexcept = 0;

  /*!
   * \brief Invokes request response method TimerReq.
    * \param[in] TimerSet IN parameter of type ::gwm::platform_timer::timer_idt::TimerSet_Integer_Idt
   * \return ara::core::Future with method output data element.
   * \pre -
   * \context       App
   * \synchronous   FALSE
   */
  virtual ara::core::Future<methods::TimerReq::Output> HandleMethodTimerReq(::gwm::platform_timer::timer_idt::TimerSet_Integer_Idt const& TimerSet) noexcept = 0;

  // ---- Events ---------------------------------------------------------------------------------------------------

  // ---- Fields ---------------------------------------------------------------------------------------------------

  // ---- Field 'TimerSt' ----
  /*!
   * \brief Get the field notifier object for the service field 'TimerSt'
   * \return A proxy event object supporting event sample and subscription.
   * \pre -
   * \context App | Reactor | Callback
   * \synchronus TRUE
   */
  virtual ::amsr::socal::internal::events::ProxyEventBackendInterface<::gwm::platform_timer::timer_idt::TimerSt_Struct_Idt>& GetFieldNotifierBackendTimerSt() noexcept = 0;

  /*!
   * \brief Getter for the field 'TimerSt'
   * \return ara::core::Future with Field data element.
   * \pre -
   * \context App
   * \synchronous FALSE
   */
  virtual ara::core::Future<fields::TimerSt::Output> HandleFieldTimerStMethodGet() noexcept = 0;

};

} //namespace internal
}  // namespace timersrv_si
}  // namespace application_remsdl
}  // namespace gwm

#endif  // TIMGRSRVEXE_INCLUDE_GWM_APPLICATION_REMSDL_TIMERSRV_SI_SI_TIMERSRV_PROXY_BACKEND_INTERFACE_H_

