#include "TimerSrvImpl.hpp"

namespace gwm {
namespace services {
namespace timer {

namespace skeleton {

using vac::container::operator""_sv;

TimerSrvImpl::TimerSrvImpl(std::shared_ptr<TimerBasicController> cntrl) : 
    gwm::skeleton::SkeletonMixin<TimerSrvSkeleton>(ara::core::InstanceSpecifier("TimerSrvExe/RootSwComponentPrototype/AdaptiveProvidedPortType_TimerSrv"_sv)),
    _controller(cntrl), _timerState{}, _logger(gwm::platform::Logger("TimerSrvImpl")) {

    _controller->setTimerNotifier([this](TimerBasicController::TimerState data) {

        _logger.LogInfo() << "Handling Timer Notification " << static_cast<int>(data);
        
        if(data == TimerBasicController::TimerState::TimerTrigger) {

            _timerState.WakeSrcexpired_Idt = gwm::platform_timer::timer_idt::TimerSt_Struct_Idt::WakeSrcexpired_Idt_generated_type::TimeOut;
        } else if(data == TimerBasicController::TimerState::TimerDefault) {

            _timerState.WakeSrcexpired_Idt = gwm::platform_timer::timer_idt::TimerSt_Struct_Idt::WakeSrcexpired_Idt_generated_type::NoTimer;
        } else {
            _timerState.WakeSrcexpired_Idt = gwm::platform_timer::timer_idt::TimerSt_Struct_Idt::WakeSrcexpired_Idt_generated_type::Timing;
        }
        
        this->TimerSt.Update(_timerState); 
    });

    ::gwm::platform_timer::timer_idt::TimerSt_Struct_Idt data;
    data.WakeSrcexpired_Idt = ::gwm::platform_timer::timer_idt::TimerSt_WakeSrcexpired_Enum_Idt::NoTimer;
    data.WakeSource_Idt = ::gwm::platform_timer::timer_idt::TimerSt_WakeSource_Enum_Idt::Default;
    TimerSt.Update(data);

    TimerSrvImpl::TimerSt.RegisterGetHandler([this]() -> ara::core::Future<TimerStFieldType> {return this->getTimerSt();});

    
}

ara::core::Future<methods::TimerCncl::Output> TimerSrvImpl::TimerCncl() {

    _logger.LogInfo() << "Handling TimerCncl in TimerSrvImpl";

    ara::core::Promise<methods::TimerCncl::Output> ret;
    auto res = _controller->cancelActiveTimer();
    _logger.LogInfo() << "Controller result is " << res;
    ret.set_value({transFormResult(res)});

    return ret.get_future();
}

ara::core::Future<methods::TimerReq::Output> TimerSrvImpl::TimerReq(::gwm::platform_timer::timer_idt::TimerSet_Integer_Idt const &TimerSet) {

    _logger.LogInfo() << "Handling TimerReq in TimerSrvImpl, value is " << static_cast<std::uint32_t>(TimerSet);

    ara::core::Promise<methods::TimerReq::Output> ret;
    if ( TimerSet > SECONDS_IN_WEEK || TimerSet < 1 ) {

        _logger.LogInfo() << "Invalid time range, the range is 1 - " << SECONDS_IN_WEEK;

        ret.set_value({transFormResult(1)});
    } else {
        auto res = _controller->requestTimer(TimerSet);
        _logger.LogInfo() << "Controller result is " << res;
        ret.set_value({transFormResult(res)});
    }

    return ret.get_future();
}

gwm::platform_timer::timer_idt::TimerSet_Response_Enum_Idt TimerSrvImpl::transFormResult(int ret)
{
    if (ret == 0){
        
        return gwm::platform_timer::timer_idt::TimerSet_Response_Enum_Idt::OK;             
    }

    return gwm::platform_timer::timer_idt::TimerSet_Response_Enum_Idt::NOK;
}

ara::core::Future<TimerStFieldType> TimerSrvImpl::getTimerSt(){

    ara::core::Promise<TimerStFieldType> prms;
    prms.set_value(_timerState);  
    return  prms.get_future();

}

}
}
}
}