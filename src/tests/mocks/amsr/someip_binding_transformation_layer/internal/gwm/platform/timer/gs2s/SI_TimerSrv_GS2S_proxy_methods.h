/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  TimerSrvExe/include/amsr/someip_binding_transformation_layer/internal/gwm/platform/timer/gs2s/SI_TimerSrv_GS2S_proxy_methods.h
 *        \brief  SOME/IP proxy method de- /serialization handling for methods and field methods of service 'SI_TimerSrv_GS2S'
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_someipbinding
 *         Commit ID: 43b33f0283d4d61385b98a3599688499abd86a75
 *********************************************************************************************************************/

#ifndef TIMERSRVEXE_INCLUDE_AMSR_SOMEIP_BINDING_TRANSFORMATION_LAYER_INTERNAL_GWM_PLATFORM_TIMER_GS2S_SI_TIMERSRV_GS2S_PROXY_METHODS_H_
#define TIMERSRVEXE_INCLUDE_AMSR_SOMEIP_BINDING_TRANSFORMATION_LAYER_INTERNAL_GWM_PLATFORM_TIMER_GS2S_SI_TIMERSRV_GS2S_PROXY_METHODS_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "amsr/iostream/stateful_output_stream.h"
#include "amsr/iostream/stringstream/output_string_stream.h"
#include "amsr/someip_protocol/internal/method_deserialization/gwm_platform_timer_gs2s/fields/deserializer_Response_TimerStGet.h"
#include "amsr/someip_protocol/internal/method_deserialization/gwm_platform_timer_gs2s/methods/deserializer_Response_TimerReq.h"
#include "amsr/someip_protocol/internal/method_serialization/gwm_platform_timer_gs2s/fields/serializer_Request_TimerStGet.h"
#include "amsr/someip_protocol/internal/method_serialization/gwm_platform_timer_gs2s/methods/serializer_Request_TimerReq.h"
#include "ara/com/com_error_domain.h"
#include "gwm/platform/timer/gs2s/SI_TimerSrv_GS2S_types.h"
#include "someip-protocol/internal/someip_message.h"
#include "someip_binding_transformation_layer/internal/methods/proxy_method_xf.h"
#include "vac/memory/memory_buffer_allocator.h"

namespace amsr {
namespace someip_binding_transformation_layer {
namespace internal {

namespace gwm {
namespace platform {
namespace timer {
namespace gs2s {


namespace methods {

namespace timerreq {

// VECTOR NC AutosarC++17_10-M7.3.6, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.6_using_directive
// VECTOR NC AutosarC++17_10-M7.3.4, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.4_using_directive
using namespace vac::container::literals;  // NOLINT(build/namespaces)

/*!
 * \brief Builder to create an \c ara::core::ErrorCode from a specific error domain.
 */
class LocalErrorCodeBuilder {
 public:
  /*!
   * \brief Constructor.
   * \param[in] logger Passed in by the method to log errors and debug messages.
   * \pre         -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  explicit LocalErrorCodeBuilder(::amsr::someip_binding::internal::logging::AraComLogger& logger) : logger_{logger} {}

  /*!
   * \brief   Builder to create an \c ara::core::ErrorCode from a specific error domain.
   * \details The builder supports all error domains that are referenced by errors in \c
   *          ClientServerOperation.possibleApError and \c ClientServerOperation.possibleApErrorSet.
   *          If an error domain id is passed that is not supported, an \c ComErrc::network_binding_failure error code
   *          is created.
   *
   * \param[in] error_domain_id The unique error domain identifier.
   * \param[in] error_code_value  The value of the error code.
   * \param[in] support_data The support data.
   * \param[in] user_message  The user message.
   *
   * \trace SWS_CM_10440 If the error domain id or the error code value are invalid an \c ErrorCode(ComErrc::network_binding_failure) is returned.
   *
   * \return The error code.
   * \pre         -
   * \context     Callback
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  ara::core::ErrorCode CreateErrorCode(::ara::core::ErrorDomain::CodeType const error_code_value,
                                       ::ara::core::ErrorDomain::IdType const error_domain_id,
                                       ::ara::core::ErrorDomain::SupportDataType const support_data,
                                       ::ara::core::ErrorDomain::StringType const user_message) const {
    logger_.LogDebug(
        [&error_code_value, &error_domain_id, &support_data, &user_message](::ara::log::LogStream& s) {
          s << "Create ErrorCode with error code value ='" << error_code_value << "', domain id '" << error_domain_id
            << "', support data '" << support_data << "', user message '" << user_message << "'";
        },
        __func__, __LINE__);

    ara::core::ErrorCode retval{::ara::com::ComErrc::network_binding_failure, ""};

    // clang-format off
    // VECTOR NC AutosarC++17_10-M6.4.3, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M6.4.3_Switch_statement_with_no_case_clause
    // VECTOR NC AutosarC++17_10-A6.4.1, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-A6.4.1_Switch_statement_with_no_case_clause
    // clang-format on
    switch (error_domain_id) {
      default: {
        // Create an error code
        // invalid error domain id
        logger_.LogError([&error_domain_id](ara::log::LogStream& s) {
                          constexpr std::size_t kBufferSize{50};
                          ara::core::Array<char, kBufferSize> buffer;
                          ::amsr::stream::stringstream::OutputStringStream unknown_error_message{ara::core::Span<char>{buffer}};
                          ::amsr::stream::StatefulOutputStream unknown_error_message_wrapper{unknown_error_message};
                          unknown_error_message_wrapper << ara::core::StringView{"Unknown error domain id: "};
                          unknown_error_message_wrapper << error_domain_id;
                          s << unknown_error_message.AsStringView().ToString(); },
                         __func__, __LINE__);
        retval = ara::core::ErrorCode{::ara::com::ComErrc::network_binding_failure};
        break;
      }
    }
    return retval;
  }

 private:


  /*!
   * \brief Reference to the logger of the builder.
   */
  ::amsr::someip_binding::internal::logging::AraComLogger& logger_;
};


/*!
 * \brief Struct combining all required types/values for creating the method manager.
 *
 * \remark generated
 */
struct ProxyConfiguration {

/*!
 * \brief Request serializer.
 */
using RequestSerializer = ::amsr::someip_protocol::internal::method_serializer::gwm_platform_timer_gs2s::methods::SerializerRequestTimerReq;

/*!
 * \brief Method name string.
 */
static constexpr ::ara::core::StringView kName{"TimerReq"_sv};

/*!
 * \brief Method Id.
 */
static constexpr ::amsr::someip_protocol::internal::MethodId kMethodId{0x1U};

/*!
 * \brief Builder to create an \c ara::core::ErrorCode from a specific error domain.
 */
using ErrorCodeBuilder = LocalErrorCodeBuilder;

/*!
 * \brief Method Input struct.
 */
using Input = ::gwm::platform::timer::gs2s::internal::methods::TimerReq::Input;

/*!
 * \brief Method Output struct.
 */
using Output = ::gwm::platform::timer::gs2s::internal::methods::TimerReq::Output;

/*!
 * \brief OK Response deserializer.
 */
using ResponseOkDeserializer = ::amsr::someip_protocol::internal::method_deserializer::gwm_platform_timer_gs2s::methods::DeserializerResponseOkTimerReq;

};


/*!
 * \brief Method manager configuration class.
 *
 * \remark generated
 */
using ProxyTimerReqConfiguration = ::amsr::someip_binding_transformation_layer::internal::methods::ProxyMethodXfConfiguration<ProxyConfiguration::ErrorCodeBuilder, ProxyConfiguration::Input, ProxyConfiguration::RequestSerializer, ProxyConfiguration::Output, ProxyConfiguration::ResponseOkDeserializer>;

}  // namespace timerreq

/*!
 * \brief SOME/IP Proxy method class for 'TimerReq'.
 * \details Handles SOME/IP de-/serialization.
 *
 * \remark generated
 */
using ProxyTimerReq = ::amsr::someip_binding_transformation_layer::internal::methods::ProxyMethodXf<timerreq::ProxyTimerReqConfiguration>;

}  // namespace methods

namespace fields {

namespace timerstget {

// VECTOR NC AutosarC++17_10-M7.3.6, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.6_using_directive
// VECTOR NC AutosarC++17_10-M7.3.4, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.4_using_directive
using namespace vac::container::literals;  // NOLINT(build/namespaces)

/*!
 * \brief Builder to create an \c ara::core::ErrorCode from a specific error domain.
 */
class LocalErrorCodeBuilder {
 public:
  /*!
   * \brief Constructor.
   * \param[in] logger Passed in by the method to log errors and debug messages.
   * \pre         -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  explicit LocalErrorCodeBuilder(::amsr::someip_binding::internal::logging::AraComLogger& logger) : logger_{logger} {}

  /*!
   * \brief   Builder to create an \c ara::core::ErrorCode from a specific error domain.
   * \details The builder supports all error domains that are referenced by errors in \c
   *          ClientServerOperation.possibleApError and \c ClientServerOperation.possibleApErrorSet.
   *          If an error domain id is passed that is not supported, an \c ComErrc::network_binding_failure error code
   *          is created.
   *
   * \param[in] error_domain_id The unique error domain identifier.
   * \param[in] error_code_value  The value of the error code.
   * \param[in] support_data The support data.
   * \param[in] user_message  The user message.
   *
   * \trace SWS_CM_10440 If the error domain id or the error code value are invalid an \c ErrorCode(ComErrc::network_binding_failure) is returned.
   *
   * \return The error code.
   * \pre         -
   * \context     Callback
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  ara::core::ErrorCode CreateErrorCode(::ara::core::ErrorDomain::CodeType const error_code_value,
                                       ::ara::core::ErrorDomain::IdType const error_domain_id,
                                       ::ara::core::ErrorDomain::SupportDataType const support_data,
                                       ::ara::core::ErrorDomain::StringType const user_message) const {
    logger_.LogDebug(
        [&error_code_value, &error_domain_id, &support_data, &user_message](::ara::log::LogStream& s) {
          s << "Create ErrorCode with error code value ='" << error_code_value << "', domain id '" << error_domain_id
            << "', support data '" << support_data << "', user message '" << user_message << "'";
        },
        __func__, __LINE__);

    ara::core::ErrorCode retval{::ara::com::ComErrc::network_binding_failure, ""};

    // clang-format off
    // VECTOR NC AutosarC++17_10-M6.4.3, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M6.4.3_Switch_statement_with_no_case_clause
    // VECTOR NC AutosarC++17_10-A6.4.1, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-A6.4.1_Switch_statement_with_no_case_clause
    // clang-format on
    switch (error_domain_id) {
      default: {
        // Create an error code
        // invalid error domain id
        logger_.LogError([&error_domain_id](ara::log::LogStream& s) {
                          constexpr std::size_t kBufferSize{50};
                          ara::core::Array<char, kBufferSize> buffer;
                          ::amsr::stream::stringstream::OutputStringStream unknown_error_message{ara::core::Span<char>{buffer}};
                          ::amsr::stream::StatefulOutputStream unknown_error_message_wrapper{unknown_error_message};
                          unknown_error_message_wrapper << ara::core::StringView{"Unknown error domain id: "};
                          unknown_error_message_wrapper << error_domain_id;
                          s << unknown_error_message.AsStringView().ToString(); },
                         __func__, __LINE__);
        retval = ara::core::ErrorCode{::ara::com::ComErrc::network_binding_failure};
        break;
      }
    }
    return retval;
  }

 private:


  /*!
   * \brief Reference to the logger of the builder.
   */
  ::amsr::someip_binding::internal::logging::AraComLogger& logger_;
};


/*!
 * \brief Struct combining all required types/values for creating the method manager.
 *
 * \remark generated
 */
struct ProxyConfiguration {

/*!
 * \brief Request serializer.
 */
using RequestSerializer = ::amsr::someip_protocol::internal::method_serializer::gwm_platform_timer_gs2s::fields::SerializerRequestTimerStGet;

/*!
 * \brief Method name string.
 */
static constexpr ::ara::core::StringView kName{"TimerStGet"_sv};

/*!
 * \brief Method Id.
 */
static constexpr ::amsr::someip_protocol::internal::MethodId kMethodId{0x4001U};

/*!
 * \brief Builder to create an \c ara::core::ErrorCode from a specific error domain.
 */
using ErrorCodeBuilder = LocalErrorCodeBuilder;

/*!
 * \brief Method Input struct.
 */
using Input = ::gwm::platform::timer::gs2s::internal::fields::TimerStGet::Input;

/*!
 * \brief Method Output struct.
 */
using Output = ::gwm::platform::timer::gs2s::internal::fields::TimerSt::Output;

/*!
 * \brief OK Response deserializer.
 */
using ResponseOkDeserializer = ::amsr::someip_protocol::internal::method_deserializer::gwm_platform_timer_gs2s::fields::DeserializerResponseOkTimerStGet;

};


/*!
 * \brief Method manager configuration class.
 *
 * \remark generated
 */
using ProxyTimerStGetConfiguration = ::amsr::someip_binding_transformation_layer::internal::methods::ProxyMethodXfConfiguration<ProxyConfiguration::ErrorCodeBuilder, ProxyConfiguration::Input, ProxyConfiguration::RequestSerializer, ProxyConfiguration::Output, ProxyConfiguration::ResponseOkDeserializer>;

}  // namespace timerstget

/*!
 * \brief SOME/IP Proxy method class for 'TimerStGet'.
 * \details Handles SOME/IP de-/serialization.
 *
 * \remark generated
 */
using ProxyTimerStGet = ::amsr::someip_binding_transformation_layer::internal::methods::ProxyMethodXf<timerstget::ProxyTimerStGetConfiguration>;

}  // namespace fields

}  // namespace internal
}  // namespace someip_binding_transformation_layer
}  // namespace amsr
}  // namespace gs2s
}  // namespace timer
}  // namespace platform
}  // namespace gwm

#endif  // TIMERSRVEXE_INCLUDE_AMSR_SOMEIP_BINDING_TRANSFORMATION_LAYER_INTERNAL_GWM_PLATFORM_TIMER_GS2S_SI_TIMERSRV_GS2S_PROXY_METHODS_H_

