/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/include/amsr/someip_binding_transformation_layer/internal/gwm/pt_batterypacksys/bmsstsrv_si/SI_BmsStSrv_skeleton_someip_binding.h
 *        \brief  SOME/IP binding of service skeleton for service 'SI_BmsStSrv'
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_someipbinding
 *         Commit ID: 43b33f0283d4d61385b98a3599688499abd86a75
 *********************************************************************************************************************/

#ifndef DENPENDENCYSRVEXE_INCLUDE_AMSR_SOMEIP_BINDING_TRANSFORMATION_LAYER_INTERNAL_GWM_PT_BATTERYPACKSYS_BMSSTSRV_SI_SI_BMSSTSRV_SKELETON_SOMEIP_BINDING_H_
#define DENPENDENCYSRVEXE_INCLUDE_AMSR_SOMEIP_BINDING_TRANSFORMATION_LAYER_INTERNAL_GWM_PT_BATTERYPACKSYS_BMSSTSRV_SI_SI_BMSSTSRV_SKELETON_SOMEIP_BINDING_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "amsr/someip_binding_transformation_layer/internal/gwm/pt_batterypacksys/bmsstsrv_si/SI_BmsStSrv_skeleton_someip_event_manager.h"
#include "amsr/someip_binding_transformation_layer/internal/gwm/pt_batterypacksys/bmsstsrv_si/SI_BmsStSrv_skeleton_someip_methods.h"
#include "gwm/pt_batterypacksys/bmsstsrv_si/SI_BmsStSrv_skeleton_impl_interface.h"
#include "gwm/pt_batterypacksys/bmsstsrv_si/si_bmsstsrv_skeleton.h"
#include "someip_binding/internal/server_interface.h"
#include "someip_binding/internal/server_manager_interface.h"

namespace amsr {
namespace someip_binding_transformation_layer {
namespace internal {

namespace gwm {
namespace pt_batterypacksys {
namespace bmsstsrv_si {

/*!
 * \brief SOME/IP skeleton binding of Service 'SI_BmsStSrv'.
 * \details Handles serialization and deserialization of all method calls, events, etc.
 * \remark generated
 */
class SI_BmsStSrvSkeletonSomeIpBinding final :
    public ::gwm::pt_batterypacksys::bmsstsrv_si::internal::SI_BmsStSrvSkeletonImplInterface,
    public ::amsr::someip_binding::internal::ServerInterface {
 public:
  /*!
   * \brief Type alias for used skeleton frontend.
   */
  using SkeletonFrontend = ::gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton;
  /*!
   * \brief Generated SOME/IP related service ID.
   */
  static constexpr ::amsr::someip_protocol::internal::ServiceId kServiceId{0x4008U};

  /*!
   * \brief SOME/IP major version of this service interface deployment.
   */
  static constexpr ::amsr::someip_protocol::internal::MajorVersion kMajorVersion{1U};

  /*!
   * \brief Constructor of SI_BmsStSrvSkeletonSomeIpBinding.
   * \details Constructed when the service is offered.
   *
   * \param[in] instance_id                    SOME/IP service instance ID offered by this skeleton.
   * \param[in] someip_binding_server_manager  Reference to the related SOME/IP transport binding manager.
   * \param[in] skeleton                       Reference to the related frontend skeleton.
   * \pre         -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  SI_BmsStSrvSkeletonSomeIpBinding(::amsr::someip_protocol::internal::InstanceId const instance_id,
                                                         ::amsr::someip_binding::internal::ServerManagerInterface& someip_binding_server_manager,
                                                         ::gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton& skeleton);

  /*!
   * \brief Default destructor must de-register the implementation from the SomeIpBinding.
   * \details Destroyed when the service is stopped.
   * \pre         -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR NC AutosarC++17_10-A15.5.1: MD_SOMEIPBINDING_AutosarC++17_10-A15.4.2_A15.5.1_A15.5.2_A15.5.3_GoogleTest
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  ~SI_BmsStSrvSkeletonSomeIpBinding() noexcept override  = default;

  /*!
   * \brief Getter for accessing the related frontend skeleton.
   * \pre         -
   * \context     Callback
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR Next Line AutosarC++17_10-M9.3.3: MD_SOMEIPBINDING_AutosarC++17_10-M9.3.3_returns_nonconst
  SkeletonFrontend& GetSkeleton() noexcept { return skeleton_; }

  /*!
   * \brief Receive handler for method requests.
   *
   * \param[in] header Deserialized SOME/IP message header.
   * \param[in] packet Serialized SOME/IP Method Request [SOME/IP header + Payload]
   * \pre         -
   * \context     Reactor
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  void HandleMethodRequest(::amsr::someip_protocol::internal::SomeIpMessageHeader const& header,
      ::amsr::someip_protocol::internal::SomeIpMessage packet) override;

  /*!
   * \brief Forward a prepared packet to the SomeIpBinding.
   * \param[in] packet the serialized response packet.
   * \pre         -
   * \context     Callback
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  void SendMethodResponse(::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet);

  /*!
   * \brief Send a SOME/IP error response packet. Used in case of:
   *  - Infrastructural checks failed (protocol version, return code, message type).
   *  - Deserialization failed.
   *  - Serialization of method responses failed.
   *  - Service / Method is not implemented.
   *
   * \tparam ReturnCode The error return code as template parameter to make a compile-time check.
   * \param[in] request_header The unmodified SOME/IP header.
   * \pre         -
   * \context     Reactor
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  template <typename ::amsr::someip_protocol::internal::ReturnCode ReturnCode>
  void SendErrorResponse(::amsr::someip_protocol::internal::SomeIpMessageHeader const& request_header) noexcept {
    static_assert((static_cast<::amsr::someip_protocol::internal::SomeIpReturnCode>(ReturnCode) >= ::amsr::someip_protocol::internal::SomeIpReturnCode::kNotOk) &&
                      (static_cast<::amsr::someip_protocol::internal::SomeIpReturnCode>(ReturnCode) <= ::amsr::someip_protocol::internal::SomeIpReturnCode::kRangeServiceErrorsEnd),
                  "Return code for errors must be in the range of 0x01 .. 0x5E.");
    someip_binding_server_manager_.SendErrorResponse(ReturnCode, instance_id_, request_header);
  }

  /*!
   * \brief Send an error response if the SOME/IP request is malformed.
   * \details This method is invoked when the return code of the error response is only known during runtime.
   * \param[in] return_code    The return code to send.
   * \param[in] request_header The header taken from the request.
   * \pre         -
   * \context     Reactor
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  void SendErrorResponse(::amsr::someip_protocol::internal::ReturnCode const return_code,
                         ::amsr::someip_protocol::internal::SomeIpMessageHeader const& request_header) {
    someip_binding_server_manager_.SendErrorResponse(return_code, instance_id_, request_header);
  }

  // ---- Events --------------------------------------------------------------------------------------------------- */

  /*!
   * \brief Get the event manager object for the SOME/IP binding service event 'BMSChrgTi'.
   * \details Event sample type: ::gwm::pt_batterypacksys::batterypacksys_idt::BMSChrgTi_Integer_Idt.
   * \return The SOME/IP binding specific event management object/interface supporting event transmission.
   *
   * \pre         -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  SI_BmsStSrvSkeletonSomeIpEventManagerBMSChrgTi* GetEventManagerBMSChrgTi() noexcept override;

  /*!
   * \brief Get the event manager object for the SOME/IP binding service event 'BMSConDchrMaxPwr'.
   * \details Event sample type: ::gwm::pt_batterypacksys::batterypacksys_idt::BMSConDchrMaxPwr_Float_Idt.
   * \return The SOME/IP binding specific event management object/interface supporting event transmission.
   *
   * \pre         -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  SI_BmsStSrvSkeletonSomeIpEventManagerBMSConDchrMaxPwr* GetEventManagerBMSConDchrMaxPwr() noexcept override;

  /*!
   * \brief Get the event manager object for the SOME/IP binding service event 'BMSConFBMaxPwr'.
   * \details Event sample type: ::gwm::pt_batterypacksys::batterypacksys_idt::BMSConFBMaxPwr_Float_Idt.
   * \return The SOME/IP binding specific event management object/interface supporting event transmission.
   *
   * \pre         -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  SI_BmsStSrvSkeletonSomeIpEventManagerBMSConFBMaxPwr* GetEventManagerBMSConFBMaxPwr() noexcept override;

  /*!
   * \brief Get the event manager object for the SOME/IP binding service event 'BMSPlsDchrMaxPwr'.
   * \details Event sample type: ::gwm::pt_batterypacksys::batterypacksys_idt::BMSPlsDchrMaxPwr_Float_Idt.
   * \return The SOME/IP binding specific event management object/interface supporting event transmission.
   *
   * \pre         -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  SI_BmsStSrvSkeletonSomeIpEventManagerBMSPlsDchrMaxPwr* GetEventManagerBMSPlsDchrMaxPwr() noexcept override;

  /*!
   * \brief Get the event manager object for the SOME/IP binding service event 'BMSPlsFBMaxPwr'.
   * \details Event sample type: ::gwm::pt_batterypacksys::batterypacksys_idt::BMSPlsFBMaxPwr_Float_Idt.
   * \return The SOME/IP binding specific event management object/interface supporting event transmission.
   *
   * \pre         -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  SI_BmsStSrvSkeletonSomeIpEventManagerBMSPlsFBMaxPwr* GetEventManagerBMSPlsFBMaxPwr() noexcept override;

  /*!
   * \brief Get the event manager object for the SOME/IP binding service event 'BmsInletCooltT'.
   * \details Event sample type: ::gwm::pt_batterypacksys::batterypacksys_idt::BmsInletCooltT_Integer_Idt.
   * \return The SOME/IP binding specific event management object/interface supporting event transmission.
   *
   * \pre         -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  SI_BmsStSrvSkeletonSomeIpEventManagerBmsInletCooltT* GetEventManagerBmsInletCooltT() noexcept override;

  /*!
   * \brief Get the event manager object for the SOME/IP binding service event 'BmsIsoR'.
   * \details Event sample type: ::gwm::pt_batterypacksys::batterypacksys_idt::BmsIsoR_Integer_Idt.
   * \return The SOME/IP binding specific event management object/interface supporting event transmission.
   *
   * \pre         -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  SI_BmsStSrvSkeletonSomeIpEventManagerBmsIsoR* GetEventManagerBmsIsoR() noexcept override;

  /*!
   * \brief Get the event manager object for the SOME/IP binding service event 'BmsOutletCooltT'.
   * \details Event sample type: ::gwm::pt_batterypacksys::batterypacksys_idt::BmsOutletCooltT_Integer_Idt.
   * \return The SOME/IP binding specific event management object/interface supporting event transmission.
   *
   * \pre         -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  SI_BmsStSrvSkeletonSomeIpEventManagerBmsOutletCooltT* GetEventManagerBmsOutletCooltT() noexcept override;

  /*!
   * \brief Get the event manager object for the SOME/IP binding service event 'CellMaxT'.
   * \details Event sample type: ::gwm::pt_batterypacksys::batterypacksys_idt::CellMaxT_Struct_Idt.
   * \return The SOME/IP binding specific event management object/interface supporting event transmission.
   *
   * \pre         -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  SI_BmsStSrvSkeletonSomeIpEventManagerCellMaxT* GetEventManagerCellMaxT() noexcept override;

  /*!
   * \brief Get the event manager object for the SOME/IP binding service event 'CellMaxU'.
   * \details Event sample type: ::gwm::pt_batterypacksys::batterypacksys_idt::CellMaxU_Struct_Idt.
   * \return The SOME/IP binding specific event management object/interface supporting event transmission.
   *
   * \pre         -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  SI_BmsStSrvSkeletonSomeIpEventManagerCellMaxU* GetEventManagerCellMaxU() noexcept override;

  /*!
   * \brief Get the event manager object for the SOME/IP binding service event 'CellMinT'.
   * \details Event sample type: ::gwm::pt_batterypacksys::batterypacksys_idt::CellMinT_Struct_Idt.
   * \return The SOME/IP binding specific event management object/interface supporting event transmission.
   *
   * \pre         -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  SI_BmsStSrvSkeletonSomeIpEventManagerCellMinT* GetEventManagerCellMinT() noexcept override;

  /*!
   * \brief Get the event manager object for the SOME/IP binding service event 'CellMinU'.
   * \details Event sample type: ::gwm::pt_batterypacksys::batterypacksys_idt::CellMinU_Struct_Idt.
   * \return The SOME/IP binding specific event management object/interface supporting event transmission.
   *
   * \pre         -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  SI_BmsStSrvSkeletonSomeIpEventManagerCellMinU* GetEventManagerCellMinU() noexcept override;

  /*!
   * \brief Get the event manager object for the SOME/IP binding service event 'HVBattDispySoc'.
   * \details Event sample type: ::gwm::pt_batterypacksys::batterypacksys_idt::HVBattDispySoc_Float_Idt.
   * \return The SOME/IP binding specific event management object/interface supporting event transmission.
   *
   * \pre         -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  SI_BmsStSrvSkeletonSomeIpEventManagerHVBattDispySoc* GetEventManagerHVBattDispySoc() noexcept override;

  /*!
   * \brief Get the event manager object for the SOME/IP binding service event 'HVBattI'.
   * \details Event sample type: ::gwm::pt_batterypacksys::batterypacksys_idt::HVBattI_Float_Idt.
   * \return The SOME/IP binding specific event management object/interface supporting event transmission.
   *
   * \pre         -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  SI_BmsStSrvSkeletonSomeIpEventManagerHVBattI* GetEventManagerHVBattI() noexcept override;

  /*!
   * \brief Get the event manager object for the SOME/IP binding service event 'HVBattInnerSoc'.
   * \details Event sample type: ::gwm::pt_batterypacksys::batterypacksys_idt::HVBattInnerSoc_Float_Idt.
   * \return The SOME/IP binding specific event management object/interface supporting event transmission.
   *
   * \pre         -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  SI_BmsStSrvSkeletonSomeIpEventManagerHVBattInnerSoc* GetEventManagerHVBattInnerSoc() noexcept override;

  /*!
   * \brief Get the event manager object for the SOME/IP binding service event 'HVBattSoh'.
   * \details Event sample type: ::gwm::pt_batterypacksys::batterypacksys_idt::HVBattSoh_Float_Idt.
   * \return The SOME/IP binding specific event management object/interface supporting event transmission.
   *
   * \pre         -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  SI_BmsStSrvSkeletonSomeIpEventManagerHVBattSoh* GetEventManagerHVBattSoh() noexcept override;

  /*!
   * \brief Get the event manager object for the SOME/IP binding service event 'HVBattTotEgy'.
   * \details Event sample type: ::gwm::pt_batterypacksys::batterypacksys_idt::HVBattTotEgy_Float_Idt.
   * \return The SOME/IP binding specific event management object/interface supporting event transmission.
   *
   * \pre         -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  SI_BmsStSrvSkeletonSomeIpEventManagerHVBattTotEgy* GetEventManagerHVBattTotEgy() noexcept override;

  /*!
   * \brief Get the event manager object for the SOME/IP binding service event 'HVBattU'.
   * \details Event sample type: ::gwm::pt_batterypacksys::batterypacksys_idt::HVBattU_Float_Idt.
   * \return The SOME/IP binding specific event management object/interface supporting event transmission.
   *
   * \pre         -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  SI_BmsStSrvSkeletonSomeIpEventManagerHVBattU* GetEventManagerHVBattU() noexcept override;

  /*!
   * \brief Get the event manager object for the SOME/IP binding service event 'ModulAvrgT'.
   * \details Event sample type: ::gwm::pt_batterypacksys::batterypacksys_idt::ModulAvrgT_Integer_Idt.
   * \return The SOME/IP binding specific event management object/interface supporting event transmission.
   *
   * \pre         -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  SI_BmsStSrvSkeletonSomeIpEventManagerModulAvrgT* GetEventManagerModulAvrgT() noexcept override;

  // ---- Fields ---------------------------------------------------------------------------------------------------

  /*!
   * \brief Get the event manager object for the SOME/IP binding field notifier of field 'BMSChrgSt'.
   * \details Field data type: ::gwm::pt_batterypacksys::batterypacksys_idt::BMSChrgSt_Enum_Idt.
   * \return The SOME/IP binding specific event management object/interface supporting field notifier updates.
   *
   * \pre         -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSChrgSt* GetFieldNotifierBMSChrgSt() noexcept override;

  /*!
   * \brief Get the event manager object for the SOME/IP binding field notifier of field 'BMSDCChrgCnctSt'.
   * \details Field data type: ::gwm::pt_batterypacksys::batterypacksys_idt::BMSDCChrgCnctSt_Enum_Idt.
   * \return The SOME/IP binding specific event management object/interface supporting field notifier updates.
   *
   * \pre         -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSDCChrgCnctSt* GetFieldNotifierBMSDCChrgCnctSt() noexcept override;

  /*!
   * \brief Get the event manager object for the SOME/IP binding field notifier of field 'BMSDCChrgEndReas'.
   * \details Field data type: ::gwm::pt_batterypacksys::batterypacksys_idt::BMSDCChrgEndReas_Enum_Idt.
   * \return The SOME/IP binding specific event management object/interface supporting field notifier updates.
   *
   * \pre         -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSDCChrgEndReas* GetFieldNotifierBMSDCChrgEndReas() noexcept override;

  /*!
   * \brief Get the event manager object for the SOME/IP binding field notifier of field 'BMSDchrgPrmsSt'.
   * \details Field data type: ::gwm::pt_batterypacksys::batterypacksys_idt::BMSDchrgPrmsSt_Enum_Idt.
   * \return The SOME/IP binding specific event management object/interface supporting field notifier updates.
   *
   * \pre         -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSDchrgPrmsSt* GetFieldNotifierBMSDchrgPrmsSt() noexcept override;

  /*!
   * \brief Get the event manager object for the SOME/IP binding field notifier of field 'BMSHeatRunawaySt'.
   * \details Field data type: ::gwm::pt_batterypacksys::batterypacksys_idt::BMSHeatRunawaySt_Enum_Idt.
   * \return The SOME/IP binding specific event management object/interface supporting field notifier updates.
   *
   * \pre         -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSHeatRunawaySt* GetFieldNotifierBMSHeatRunawaySt() noexcept override;

  /*!
   * \brief Get the event manager object for the SOME/IP binding field notifier of field 'BMSInnerSOCSt'.
   * \details Field data type: ::gwm::pt_batterypacksys::batterypacksys_idt::BMSInnerSOCSt_Struct_Idt.
   * \return The SOME/IP binding specific event management object/interface supporting field notifier updates.
   *
   * \pre         -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSInnerSOCSt* GetFieldNotifierBMSInnerSOCSt() noexcept override;

  /*!
   * \brief Get the event manager object for the SOME/IP binding field notifier of field 'BMSIntelTempMgtSt'.
   * \details Field data type: ::gwm::pt_batterypacksys::batterypacksys_idt::BMSIntelTempMgtSt_Enum_Idt.
   * \return The SOME/IP binding specific event management object/interface supporting field notifier updates.
   *
   * \pre         -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSIntelTempMgtSt* GetFieldNotifierBMSIntelTempMgtSt() noexcept override;

  /*!
   * \brief Get the event manager object for the SOME/IP binding field notifier of field 'BMSLowTempSt'.
   * \details Field data type: ::gwm::pt_batterypacksys::batterypacksys_idt::BMSLowTempSt_Struct_Idt.
   * \return The SOME/IP binding specific event management object/interface supporting field notifier updates.
   *
   * \pre         -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSLowTempSt* GetFieldNotifierBMSLowTempSt() noexcept override;

  /*!
   * \brief Get the event manager object for the SOME/IP binding field notifier of field 'BMSRmtPreHeatSt'.
   * \details Field data type: ::gwm::pt_batterypacksys::batterypacksys_idt::BMSRmtPreHeatSt_Enum_Idt.
   * \return The SOME/IP binding specific event management object/interface supporting field notifier updates.
   *
   * \pre         -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSRmtPreHeatSt* GetFieldNotifierBMSRmtPreHeatSt() noexcept override;

  /*!
   * \brief Get the event manager object for the SOME/IP binding field notifier of field 'BMSSafCnctSt'.
   * \details Field data type: ::gwm::pt_batterypacksys::batterypacksys_idt::BMSSafCnctSt_Enum_Idt.
   * \return The SOME/IP binding specific event management object/interface supporting field notifier updates.
   *
   * \pre         -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSSafCnctSt* GetFieldNotifierBMSSafCnctSt() noexcept override;

  /*!
   * \brief Get the event manager object for the SOME/IP binding field notifier of field 'BmsErrSt'.
   * \details Field data type: ::gwm::pt_batterypacksys::batterypacksys_idt::BmsErrSt_Struct_Idt.
   * \return The SOME/IP binding specific event management object/interface supporting field notifier updates.
   *
   * \pre         -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  SI_BmsStSrvSkeletonSomeIpFieldNotifierBmsErrSt* GetFieldNotifierBmsErrSt() noexcept override;

  /*!
   * \brief Get the event manager object for the SOME/IP binding field notifier of field 'BmsHVILSt'.
   * \details Field data type: ::gwm::pt_batterypacksys::batterypacksys_idt::BmsHVILSt_Enum_Idt.
   * \return The SOME/IP binding specific event management object/interface supporting field notifier updates.
   *
   * \pre         -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  SI_BmsStSrvSkeletonSomeIpFieldNotifierBmsHVILSt* GetFieldNotifierBmsHVILSt() noexcept override;

  /*!
   * \brief Get the event manager object for the SOME/IP binding field notifier of field 'BmsIsoMeasSt'.
   * \details Field data type: ::gwm::pt_batterypacksys::batterypacksys_idt::BmsIsoMeasSt_Enum_Idt.
   * \return The SOME/IP binding specific event management object/interface supporting field notifier updates.
   *
   * \pre         -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  SI_BmsStSrvSkeletonSomeIpFieldNotifierBmsIsoMeasSt* GetFieldNotifierBmsIsoMeasSt() noexcept override;

  /*!
   * \brief Get the event manager object for the SOME/IP binding field notifier of field 'BmsSt'.
   * \details Field data type: ::gwm::pt_batterypacksys::batterypacksys_idt::BmsSt_Enum_Idt.
   * \return The SOME/IP binding specific event management object/interface supporting field notifier updates.
   *
   * \pre         -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  SI_BmsStSrvSkeletonSomeIpFieldNotifierBmsSt* GetFieldNotifierBmsSt() noexcept override;

  /*!
   * \brief Get the event manager object for the SOME/IP binding field notifier of field 'HVBattSOCLim'.
   * \details Field data type: ::gwm::pt_batterypacksys::batterypacksys_idt::HVBattSOCLim_Enum_Idt.
   * \return The SOME/IP binding specific event management object/interface supporting field notifier updates.
   *
   * \pre         -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  SI_BmsStSrvSkeletonSomeIpFieldNotifierHVBattSOCLim* GetFieldNotifierHVBattSOCLim() noexcept override;

  /*!
   * \brief Get the event manager object for the SOME/IP binding field notifier of field 'HVMaiSwtSt'.
   * \details Field data type: ::gwm::pt_batterypacksys::batterypacksys_idt::HVMaiSwtSt_Struct_Idt.
   * \return The SOME/IP binding specific event management object/interface supporting field notifier updates.
   *
   * \pre         -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  // VECTOR NC AutosarC++17_10-A10.3.3: MD_SOMEIPBINDING_AutosarC++17_10-A10.3.3_no_virtual_functions_in_final_class
  SI_BmsStSrvSkeletonSomeIpFieldNotifierHVMaiSwtSt* GetFieldNotifierHVMaiSwtSt() noexcept override;

 private:
  // SOME/IP instance ID offered by this skeleton
  ::amsr::someip_protocol::internal::InstanceId instance_id_;

  // Related SOME/IP binding
  ::amsr::someip_binding::internal::ServerManagerInterface& someip_binding_server_manager_;

  // Reference to the related frontend skeleton
  ::gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton& skeleton_;

  // ---- Methods ------------------------------------------------------------------------------------------

  // ---- Event manager --------------------------------------------------------------------------------------------

  // Event manager for skeleton event 'BMSChrgTi'
  SI_BmsStSrvSkeletonSomeIpEventManagerBMSChrgTi event_manager_BMSChrgTi_;

  // Event manager for skeleton event 'BMSConDchrMaxPwr'
  SI_BmsStSrvSkeletonSomeIpEventManagerBMSConDchrMaxPwr event_manager_BMSConDchrMaxPwr_;

  // Event manager for skeleton event 'BMSConFBMaxPwr'
  SI_BmsStSrvSkeletonSomeIpEventManagerBMSConFBMaxPwr event_manager_BMSConFBMaxPwr_;

  // Event manager for skeleton event 'BMSPlsDchrMaxPwr'
  SI_BmsStSrvSkeletonSomeIpEventManagerBMSPlsDchrMaxPwr event_manager_BMSPlsDchrMaxPwr_;

  // Event manager for skeleton event 'BMSPlsFBMaxPwr'
  SI_BmsStSrvSkeletonSomeIpEventManagerBMSPlsFBMaxPwr event_manager_BMSPlsFBMaxPwr_;

  // Event manager for skeleton event 'BmsInletCooltT'
  SI_BmsStSrvSkeletonSomeIpEventManagerBmsInletCooltT event_manager_BmsInletCooltT_;

  // Event manager for skeleton event 'BmsIsoR'
  SI_BmsStSrvSkeletonSomeIpEventManagerBmsIsoR event_manager_BmsIsoR_;

  // Event manager for skeleton event 'BmsOutletCooltT'
  SI_BmsStSrvSkeletonSomeIpEventManagerBmsOutletCooltT event_manager_BmsOutletCooltT_;

  // Event manager for skeleton event 'CellMaxT'
  SI_BmsStSrvSkeletonSomeIpEventManagerCellMaxT event_manager_CellMaxT_;

  // Event manager for skeleton event 'CellMaxU'
  SI_BmsStSrvSkeletonSomeIpEventManagerCellMaxU event_manager_CellMaxU_;

  // Event manager for skeleton event 'CellMinT'
  SI_BmsStSrvSkeletonSomeIpEventManagerCellMinT event_manager_CellMinT_;

  // Event manager for skeleton event 'CellMinU'
  SI_BmsStSrvSkeletonSomeIpEventManagerCellMinU event_manager_CellMinU_;

  // Event manager for skeleton event 'HVBattDispySoc'
  SI_BmsStSrvSkeletonSomeIpEventManagerHVBattDispySoc event_manager_HVBattDispySoc_;

  // Event manager for skeleton event 'HVBattI'
  SI_BmsStSrvSkeletonSomeIpEventManagerHVBattI event_manager_HVBattI_;

  // Event manager for skeleton event 'HVBattInnerSoc'
  SI_BmsStSrvSkeletonSomeIpEventManagerHVBattInnerSoc event_manager_HVBattInnerSoc_;

  // Event manager for skeleton event 'HVBattSoh'
  SI_BmsStSrvSkeletonSomeIpEventManagerHVBattSoh event_manager_HVBattSoh_;

  // Event manager for skeleton event 'HVBattTotEgy'
  SI_BmsStSrvSkeletonSomeIpEventManagerHVBattTotEgy event_manager_HVBattTotEgy_;

  // Event manager for skeleton event 'HVBattU'
  SI_BmsStSrvSkeletonSomeIpEventManagerHVBattU event_manager_HVBattU_;

  // Event manager for skeleton event 'ModulAvrgT'
  SI_BmsStSrvSkeletonSomeIpEventManagerModulAvrgT event_manager_ModulAvrgT_;

  // ---- Field manager --------------------------------------------------------------------------------------------

  // ---- Field 'BMSChrgSt' ----

  // Field notifier for the the skeleton field 'BMSChrgSt'
  SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSChrgSt field_notifier_BMSChrgSt_;

  // Field method manager for method Get of skeleton field 'BMSChrgSt'
  fields::SkeletonBMSChrgStGet field_manager_BMSChrgSt_get_;

  // ---- Field 'BMSDCChrgCnctSt' ----

  // Field notifier for the the skeleton field 'BMSDCChrgCnctSt'
  SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSDCChrgCnctSt field_notifier_BMSDCChrgCnctSt_;

  // Field method manager for method Get of skeleton field 'BMSDCChrgCnctSt'
  fields::SkeletonBMSDCChrgCnctStGet field_manager_BMSDCChrgCnctSt_get_;

  // ---- Field 'BMSDCChrgEndReas' ----

  // Field notifier for the the skeleton field 'BMSDCChrgEndReas'
  SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSDCChrgEndReas field_notifier_BMSDCChrgEndReas_;

  // Field method manager for method Get of skeleton field 'BMSDCChrgEndReas'
  fields::SkeletonBMSDCChrgEndReasGet field_manager_BMSDCChrgEndReas_get_;

  // ---- Field 'BMSDchrgPrmsSt' ----

  // Field notifier for the the skeleton field 'BMSDchrgPrmsSt'
  SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSDchrgPrmsSt field_notifier_BMSDchrgPrmsSt_;

  // Field method manager for method Get of skeleton field 'BMSDchrgPrmsSt'
  fields::SkeletonBMSDchrgPrmsStGet field_manager_BMSDchrgPrmsSt_get_;

  // ---- Field 'BMSHeatRunawaySt' ----

  // Field notifier for the the skeleton field 'BMSHeatRunawaySt'
  SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSHeatRunawaySt field_notifier_BMSHeatRunawaySt_;

  // Field method manager for method Get of skeleton field 'BMSHeatRunawaySt'
  fields::SkeletonBMSHeatRunawayStGet field_manager_BMSHeatRunawaySt_get_;

  // ---- Field 'BMSInnerSOCSt' ----

  // Field notifier for the the skeleton field 'BMSInnerSOCSt'
  SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSInnerSOCSt field_notifier_BMSInnerSOCSt_;

  // Field method manager for method Get of skeleton field 'BMSInnerSOCSt'
  fields::SkeletonBMSInnerSOCStGet field_manager_BMSInnerSOCSt_get_;

  // ---- Field 'BMSIntelTempMgtSt' ----

  // Field notifier for the the skeleton field 'BMSIntelTempMgtSt'
  SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSIntelTempMgtSt field_notifier_BMSIntelTempMgtSt_;

  // Field method manager for method Get of skeleton field 'BMSIntelTempMgtSt'
  fields::SkeletonBMSIntelTempMgtStGet field_manager_BMSIntelTempMgtSt_get_;

  // ---- Field 'BMSLowTempSt' ----

  // Field notifier for the the skeleton field 'BMSLowTempSt'
  SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSLowTempSt field_notifier_BMSLowTempSt_;

  // Field method manager for method Get of skeleton field 'BMSLowTempSt'
  fields::SkeletonBMSLowTempStGet field_manager_BMSLowTempSt_get_;

  // ---- Field 'BMSRmtPreHeatSt' ----

  // Field notifier for the the skeleton field 'BMSRmtPreHeatSt'
  SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSRmtPreHeatSt field_notifier_BMSRmtPreHeatSt_;

  // Field method manager for method Get of skeleton field 'BMSRmtPreHeatSt'
  fields::SkeletonBMSRmtPreHeatStGet field_manager_BMSRmtPreHeatSt_get_;

  // ---- Field 'BMSSafCnctSt' ----

  // Field notifier for the the skeleton field 'BMSSafCnctSt'
  SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSSafCnctSt field_notifier_BMSSafCnctSt_;

  // Field method manager for method Get of skeleton field 'BMSSafCnctSt'
  fields::SkeletonBMSSafCnctStGet field_manager_BMSSafCnctSt_get_;

  // ---- Field 'BmsErrSt' ----

  // Field notifier for the the skeleton field 'BmsErrSt'
  SI_BmsStSrvSkeletonSomeIpFieldNotifierBmsErrSt field_notifier_BmsErrSt_;

  // Field method manager for method Get of skeleton field 'BmsErrSt'
  fields::SkeletonBmsErrStGet field_manager_BmsErrSt_get_;

  // ---- Field 'BmsHVILSt' ----

  // Field notifier for the the skeleton field 'BmsHVILSt'
  SI_BmsStSrvSkeletonSomeIpFieldNotifierBmsHVILSt field_notifier_BmsHVILSt_;

  // Field method manager for method Get of skeleton field 'BmsHVILSt'
  fields::SkeletonBmsHVILStGet field_manager_BmsHVILSt_get_;

  // ---- Field 'BmsIsoMeasSt' ----

  // Field notifier for the the skeleton field 'BmsIsoMeasSt'
  SI_BmsStSrvSkeletonSomeIpFieldNotifierBmsIsoMeasSt field_notifier_BmsIsoMeasSt_;

  // Field method manager for method Get of skeleton field 'BmsIsoMeasSt'
  fields::SkeletonBmsIsoMeasStGet field_manager_BmsIsoMeasSt_get_;

  // ---- Field 'BmsSt' ----

  // Field notifier for the the skeleton field 'BmsSt'
  SI_BmsStSrvSkeletonSomeIpFieldNotifierBmsSt field_notifier_BmsSt_;

  // Field method manager for method Get of skeleton field 'BmsSt'
  fields::SkeletonBmsStGet field_manager_BmsSt_get_;

  // ---- Field 'HVBattSOCLim' ----

  // Field notifier for the the skeleton field 'HVBattSOCLim'
  SI_BmsStSrvSkeletonSomeIpFieldNotifierHVBattSOCLim field_notifier_HVBattSOCLim_;

  // Field method manager for method Get of skeleton field 'HVBattSOCLim'
  fields::SkeletonHVBattSOCLimGet field_manager_HVBattSOCLim_get_;

  // ---- Field 'HVMaiSwtSt' ----

  // Field notifier for the the skeleton field 'HVMaiSwtSt'
  SI_BmsStSrvSkeletonSomeIpFieldNotifierHVMaiSwtSt field_notifier_HVMaiSwtSt_;

  // Field method manager for method Get of skeleton field 'HVMaiSwtSt'
  fields::SkeletonHVMaiSwtStGet field_manager_HVMaiSwtSt_get_;
};

}  // namespace internal
}  // namespace someip_binding_transformation_layer
}  // namespace amsr
}  // namespace bmsstsrv_si
}  // namespace pt_batterypacksys
}  // namespace gwm

#endif  // DENPENDENCYSRVEXE_INCLUDE_AMSR_SOMEIP_BINDING_TRANSFORMATION_LAYER_INTERNAL_GWM_PT_BATTERYPACKSYS_BMSSTSRV_SI_SI_BMSSTSRV_SKELETON_SOMEIP_BINDING_H_

