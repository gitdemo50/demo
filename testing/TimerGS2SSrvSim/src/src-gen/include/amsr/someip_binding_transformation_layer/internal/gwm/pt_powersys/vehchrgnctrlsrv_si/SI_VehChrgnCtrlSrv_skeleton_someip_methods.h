/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/include/amsr/someip_binding_transformation_layer/internal/gwm/pt_powersys/vehchrgnctrlsrv_si/SI_VehChrgnCtrlSrv_skeleton_someip_methods.h
 *        \brief  SOME/IP skeleton method de- /serialization handling for methods and field methods of service 'SI_VehChrgnCtrlSrv'
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_someipbinding
 *         Commit ID: 43b33f0283d4d61385b98a3599688499abd86a75
 *********************************************************************************************************************/

#ifndef DENPENDENCYSRVEXE_INCLUDE_AMSR_SOMEIP_BINDING_TRANSFORMATION_LAYER_INTERNAL_GWM_PT_POWERSYS_VEHCHRGNCTRLSRV_SI_SI_VEHCHRGNCTRLSRV_SKELETON_SOMEIP_METHODS_H_
#define DENPENDENCYSRVEXE_INCLUDE_AMSR_SOMEIP_BINDING_TRANSFORMATION_LAYER_INTERNAL_GWM_PT_POWERSYS_VEHCHRGNCTRLSRV_SI_SI_VEHCHRGNCTRLSRV_SKELETON_SOMEIP_METHODS_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include <memory>
#include "amsr/someip_protocol/internal/method_deserialization/gwm_pt_powersys_vehchrgnctrlsrv_si/methods/deserializer_Request_ChargingTypeSet.h"
#include "amsr/someip_protocol/internal/method_deserialization/gwm_pt_powersys_vehchrgnctrlsrv_si/methods/deserializer_Request_VehChrgnCtrl.h"
#include "amsr/someip_protocol/internal/method_serialization/gwm_pt_powersys_vehchrgnctrlsrv_si/methods/serializer_Response_ChargingTypeSet.h"
#include "amsr/someip_protocol/internal/method_serialization/gwm_pt_powersys_vehchrgnctrlsrv_si/methods/serializer_Response_VehChrgnCtrl.h"
#include "ara/core/result.h"
#include "gwm/pt_powersys/vehchrgnctrlsrv_si/SI_VehChrgnCtrlSrv_types.h"
#include "gwm/pt_powersys/vehchrgnctrlsrv_si/si_vehchrgnctrlsrv_skeleton.h"
#include "osabstraction/io/io_buffer.h"
#include "someip-protocol/internal/message.h"
#include "someip_binding_transformation_layer/internal/methods/skeleton_method_xf.h"
#include "someip_binding_transformation_layer/internal/methods/skeleton_response_handler.h"
#include "vac/container/c_string_view.h"

namespace amsr {
namespace someip_binding_transformation_layer {
namespace internal {
namespace gwm {
namespace pt_powersys {
namespace vehchrgnctrlsrv_si {

// Forward-declaration for back-reference
class SI_VehChrgnCtrlSrvSkeletonSomeIpBinding;


namespace methods {

/*!
 * \brief Forward-declaration for back-reference
 */
class SkeletonChargingTypeSetAsyncRequest;

namespace chargingtypeset {

// VECTOR NC AutosarC++17_10-M7.3.6, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.6_using_directive
// VECTOR NC AutosarC++17_10-M7.3.4, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.4_using_directive
using namespace vac::container::literals;  // NOLINT(build/namespaces)

/*!
 * \brief Struct combining all required types/values for creating the method manager.
 *
 * \remark generated
 */
struct SkeletonConfiguration final {

/*!
 * \brief Method name string.
 */
static constexpr vac::container::CStringView kName{"ChargingTypeSet"_sv};

/*!
 * \brief Method Id.
 */
static constexpr ::amsr::someip_protocol::internal::MethodId kMethodId{0x2U};

/*!
 * \brief Skeleton SomeIp Binding.
 */
using SkeletonSomeIpBinding = SI_VehChrgnCtrlSrvSkeletonSomeIpBinding;

/*!
 * \brief Method Input struct.
 */
using Input = ::gwm::pt_powersys::vehchrgnctrlsrv_si::internal::methods::ChargingTypeSet::Input;

/*!
 * \brief Async request type.
 */
using AsyncRequest = SkeletonChargingTypeSetAsyncRequest;

/*!
 * \brief Method Request deserializer.
 */
using MethodRequestDeserializer = ::amsr::someip_protocol::internal::method_deserializer::gwm_pt_powersys_vehchrgnctrlsrv_si::methods::DeserializerRequestChargingTypeSet;

/*!
 * \brief Method response serializer.
 */
using MethodResponseSerializer = ::amsr::someip_protocol::internal::method_serializer::gwm_pt_powersys_vehchrgnctrlsrv_si::methods::SerializerResponseOkChargingTypeSet;
};

/*!
 * \brief Method manager configuration class.
 *
 * \remark generated
 */
using SkeletonChargingTypeSetConfiguration = ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonMethodXfConfiguration<SkeletonConfiguration::kMethodId, SkeletonConfiguration::SkeletonSomeIpBinding, SkeletonConfiguration::Input, SkeletonConfiguration::AsyncRequest, SkeletonConfiguration::MethodRequestDeserializer>;

} // namespace chargingtypeset

/*!
 * \brief SOME/IP Skeleton method class for method 'ChargingTypeSet'.
 * \details Handles SOME/IP de-/serialization.
 * \remark generated
 */
using SkeletonChargingTypeSet = ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonMethodXf<chargingtypeset::SkeletonChargingTypeSetConfiguration>;

/*!
 * \brief For each method request a task of this type is assembled and is passed to the skeleton's frontend,
 *        where it is being executed asynchronously according to the chosen processing mode.
 */
class SkeletonChargingTypeSetAsyncRequest : public ::amsr::socal::internal::PendingRequest {
 public:
  /*!
   * \brief At initialization store all relevant information, to be able to invoke the method call
   *        and route the response to the binding-related part again.
   * \param[in] skeleton A pointer to the skeleton frontend for calling the concrete method asynchronously in a separate
   *                     worker thread.
   * \param[in] skeleton_method A pointer to the skeleton method class needed to send the response after processing the
   *                            method request.
   * \param[in] header The SOME/IP header which must be stored for a possible response
   *                   (in case this method is not fire & forget).
   * \param[in] packet Serialized Method Request [SOME/IP Header + Payload].
   * \pre -
   * \context     Reactor
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  SkeletonChargingTypeSetAsyncRequest(::gwm::pt_powersys::vehchrgnctrlsrv_si::skeleton::SI_VehChrgnCtrlSrvSkeleton* skeleton,
      SkeletonChargingTypeSet& skeleton_method,
      ::amsr::someip_protocol::internal::SomeIpMessageHeader const& header,
      ::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet)
      : ::amsr::socal::internal::PendingRequest{skeleton},
        skeleton_{skeleton},
        response_handler_{skeleton_method},
        header_{header},
        packet_{std::move(packet)} {}

  /*!
   * \brief   Operator gets called when method invocation is planned in the frontend.
   * \details It shall be called only once for each instance.
   * \pre -
   * \context     Callback
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   *
   */
  void operator()() override {
    // VECTOR Next Line AutosarC++17_10-A18.5.8: MD_SOMEIPBINDING_AutosarC++17_10-A18.5.8_Local_object_allocated_in_the_heap
    std::unique_ptr<Input> const input{std::make_unique<Input>()};
    bool const deserialization_ok{DeserializeInput(packet_.get(), *input)};
    if (deserialization_ok) {
    ::gwm::pt_powersys::powersys_idt::ChargingTypeSet_ChrgMod_Enum_Idt const& arg_ChrgMod{input->ChrgMod};

    ara::core::Result<::gwm::pt_powersys::vehchrgnctrlsrv_si::internal::methods::ChargingTypeSet::Output> result{skeleton_->ChargingTypeSet(arg_ChrgMod).GetResult()};
    if (result.HasValue()) {
      response_handler_.SerializeAndSendMethodResponse<Serializer>(header_, result.Value());
    } else {
      response_handler_.SerializeAndSendApplicationErrorMethodResponse(header_, result.Error());
    }
    } else { // Deserialization failed
      response_handler_.SendErrorResponse(header_,
                                          static_cast<::amsr::someip_protocol::internal::ReturnCode>(::amsr::someip_protocol::internal::SomeIpReturnCode::kMalformedMessage));
    }
  }

 private:
  /*!
   * \brief Serialization class of the method response.
   */
  using Serializer = chargingtypeset::SkeletonConfiguration::MethodResponseSerializer;

  /*!
   * \brief Pointer to the skeleton to invoke the method request call.
   */
  using Input = chargingtypeset::SkeletonConfiguration::Input;

  /*!
   * \brief Deserialization class of the method request.
   */
  using Deserializer = chargingtypeset::SkeletonConfiguration::MethodRequestDeserializer;

  /*!
   * \brief Deserialize the given method request.
   * \param[in]  serialized_sample  Serialized SOME/IP Method Request [SOME/IP header + Payload].
   * \param[out] input              The deserialized method request arguments will be written into this param.
   * \return     true               If the deserialization succeeded.
   *             false              If an error occurred during deserialization.
   * \pre -
   * \context     Callback
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  inline static bool DeserializeInput(::vac::memory::MemoryBuffer<osabstraction::io::MutableIOBuffer> const* serialized_sample,
                                      Input& input) {
    // Prepare Reader
    ::vac::memory::MemoryBuffer<osabstraction::io::MutableIOBuffer>::MemoryBufferView packet_view{serialized_sample->GetView(0U)};
    ::amsr::someip_protocol::internal::deserialization::BufferView const body_view{
        // VECTOR Next Line AutosarC++17_10-M5.2.8:MD_SOMEIPBINDING_AutosarC++17_10-M5.2.8_conv_from_voidp
        static_cast<std::uint8_t*>(packet_view[0U].base_pointer), serialized_sample->size()};

    // Skip the header
    ::amsr::someip_protocol::internal::deserialization::BufferView const buffer_view{body_view.subspan(
        ::amsr::someip_protocol::internal::kHeaderSize, body_view.size() - ::amsr::someip_protocol::internal::kHeaderSize)};

    // Deserialize Payload
    ::amsr::someip_protocol::internal::deserialization::Reader reader{buffer_view};
    return Deserializer::Deserialize(reader, input);
  }

  /*!
   * \brief Pointer to the skeleton to invoke the method request call.
   */
  ::gwm::pt_powersys::vehchrgnctrlsrv_si::skeleton::SI_VehChrgnCtrlSrvSkeleton* skeleton_;

  /*!
   * \brief Binding-related handler to serialize and send a response right after the method call.
   */
  ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonResponseHandler<SkeletonChargingTypeSet> response_handler_;

  /*!
   * \brief Buffer the SOME/IP header from the request for the response with the corresponding client ID and session ID.
   */
  ::amsr::someip_protocol::internal::SomeIpMessageHeader const header_;

  /*!
   * \brief Serialized Method Request [SOME/IP Header + Payload].
   */
  ::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet_;
};


}  // namespace methods


namespace methods {

/*!
 * \brief Forward-declaration for back-reference
 */
class SkeletonVehChrgnCtrlAsyncRequest;

namespace vehchrgnctrl {

// VECTOR NC AutosarC++17_10-M7.3.6, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.6_using_directive
// VECTOR NC AutosarC++17_10-M7.3.4, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.4_using_directive
using namespace vac::container::literals;  // NOLINT(build/namespaces)

/*!
 * \brief Struct combining all required types/values for creating the method manager.
 *
 * \remark generated
 */
struct SkeletonConfiguration final {

/*!
 * \brief Method name string.
 */
static constexpr vac::container::CStringView kName{"VehChrgnCtrl"_sv};

/*!
 * \brief Method Id.
 */
static constexpr ::amsr::someip_protocol::internal::MethodId kMethodId{0x1U};

/*!
 * \brief Skeleton SomeIp Binding.
 */
using SkeletonSomeIpBinding = SI_VehChrgnCtrlSrvSkeletonSomeIpBinding;

/*!
 * \brief Method Input struct.
 */
using Input = ::gwm::pt_powersys::vehchrgnctrlsrv_si::internal::methods::VehChrgnCtrl::Input;

/*!
 * \brief Async request type.
 */
using AsyncRequest = SkeletonVehChrgnCtrlAsyncRequest;

/*!
 * \brief Method Request deserializer.
 */
using MethodRequestDeserializer = ::amsr::someip_protocol::internal::method_deserializer::gwm_pt_powersys_vehchrgnctrlsrv_si::methods::DeserializerRequestVehChrgnCtrl;

/*!
 * \brief Method response serializer.
 */
using MethodResponseSerializer = ::amsr::someip_protocol::internal::method_serializer::gwm_pt_powersys_vehchrgnctrlsrv_si::methods::SerializerResponseOkVehChrgnCtrl;
};

/*!
 * \brief Method manager configuration class.
 *
 * \remark generated
 */
using SkeletonVehChrgnCtrlConfiguration = ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonMethodXfConfiguration<SkeletonConfiguration::kMethodId, SkeletonConfiguration::SkeletonSomeIpBinding, SkeletonConfiguration::Input, SkeletonConfiguration::AsyncRequest, SkeletonConfiguration::MethodRequestDeserializer>;

} // namespace vehchrgnctrl

/*!
 * \brief SOME/IP Skeleton method class for method 'VehChrgnCtrl'.
 * \details Handles SOME/IP de-/serialization.
 * \remark generated
 */
using SkeletonVehChrgnCtrl = ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonMethodXf<vehchrgnctrl::SkeletonVehChrgnCtrlConfiguration>;

/*!
 * \brief For each method request a task of this type is assembled and is passed to the skeleton's frontend,
 *        where it is being executed asynchronously according to the chosen processing mode.
 */
class SkeletonVehChrgnCtrlAsyncRequest : public ::amsr::socal::internal::PendingRequest {
 public:
  /*!
   * \brief At initialization store all relevant information, to be able to invoke the method call
   *        and route the response to the binding-related part again.
   * \param[in] skeleton A pointer to the skeleton frontend for calling the concrete method asynchronously in a separate
   *                     worker thread.
   * \param[in] skeleton_method A pointer to the skeleton method class needed to send the response after processing the
   *                            method request.
   * \param[in] header The SOME/IP header which must be stored for a possible response
   *                   (in case this method is not fire & forget).
   * \param[in] packet Serialized Method Request [SOME/IP Header + Payload].
   * \pre -
   * \context     Reactor
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  SkeletonVehChrgnCtrlAsyncRequest(::gwm::pt_powersys::vehchrgnctrlsrv_si::skeleton::SI_VehChrgnCtrlSrvSkeleton* skeleton,
      SkeletonVehChrgnCtrl& skeleton_method,
      ::amsr::someip_protocol::internal::SomeIpMessageHeader const& header,
      ::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet)
      : ::amsr::socal::internal::PendingRequest{skeleton},
        skeleton_{skeleton},
        response_handler_{skeleton_method},
        header_{header},
        packet_{std::move(packet)} {}

  /*!
   * \brief   Operator gets called when method invocation is planned in the frontend.
   * \details It shall be called only once for each instance.
   * \pre -
   * \context     Callback
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   *
   */
  void operator()() override {
    // VECTOR Next Line AutosarC++17_10-A18.5.8: MD_SOMEIPBINDING_AutosarC++17_10-A18.5.8_Local_object_allocated_in_the_heap
    std::unique_ptr<Input> const input{std::make_unique<Input>()};
    bool const deserialization_ok{DeserializeInput(packet_.get(), *input)};
    if (deserialization_ok) {
    ::gwm::pt_powersys::powersys_idt::VehChrgnCtrl_ClntId_Enum_Idt const& arg_ClntId{input->ClntId};
    ::gwm::pt_powersys::powersys_idt::VehChrgnCtrl_Command_Enum_Idt const& arg_Command{input->Command};

    ara::core::Result<::gwm::pt_powersys::vehchrgnctrlsrv_si::internal::methods::VehChrgnCtrl::Output> result{skeleton_->VehChrgnCtrl(arg_ClntId,arg_Command).GetResult()};
    if (result.HasValue()) {
      response_handler_.SerializeAndSendMethodResponse<Serializer>(header_, result.Value());
    } else {
      response_handler_.SerializeAndSendApplicationErrorMethodResponse(header_, result.Error());
    }
    } else { // Deserialization failed
      response_handler_.SendErrorResponse(header_,
                                          static_cast<::amsr::someip_protocol::internal::ReturnCode>(::amsr::someip_protocol::internal::SomeIpReturnCode::kMalformedMessage));
    }
  }

 private:
  /*!
   * \brief Serialization class of the method response.
   */
  using Serializer = vehchrgnctrl::SkeletonConfiguration::MethodResponseSerializer;

  /*!
   * \brief Pointer to the skeleton to invoke the method request call.
   */
  using Input = vehchrgnctrl::SkeletonConfiguration::Input;

  /*!
   * \brief Deserialization class of the method request.
   */
  using Deserializer = vehchrgnctrl::SkeletonConfiguration::MethodRequestDeserializer;

  /*!
   * \brief Deserialize the given method request.
   * \param[in]  serialized_sample  Serialized SOME/IP Method Request [SOME/IP header + Payload].
   * \param[out] input              The deserialized method request arguments will be written into this param.
   * \return     true               If the deserialization succeeded.
   *             false              If an error occurred during deserialization.
   * \pre -
   * \context     Callback
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  inline static bool DeserializeInput(::vac::memory::MemoryBuffer<osabstraction::io::MutableIOBuffer> const* serialized_sample,
                                      Input& input) {
    // Prepare Reader
    ::vac::memory::MemoryBuffer<osabstraction::io::MutableIOBuffer>::MemoryBufferView packet_view{serialized_sample->GetView(0U)};
    ::amsr::someip_protocol::internal::deserialization::BufferView const body_view{
        // VECTOR Next Line AutosarC++17_10-M5.2.8:MD_SOMEIPBINDING_AutosarC++17_10-M5.2.8_conv_from_voidp
        static_cast<std::uint8_t*>(packet_view[0U].base_pointer), serialized_sample->size()};

    // Skip the header
    ::amsr::someip_protocol::internal::deserialization::BufferView const buffer_view{body_view.subspan(
        ::amsr::someip_protocol::internal::kHeaderSize, body_view.size() - ::amsr::someip_protocol::internal::kHeaderSize)};

    // Deserialize Payload
    ::amsr::someip_protocol::internal::deserialization::Reader reader{buffer_view};
    return Deserializer::Deserialize(reader, input);
  }

  /*!
   * \brief Pointer to the skeleton to invoke the method request call.
   */
  ::gwm::pt_powersys::vehchrgnctrlsrv_si::skeleton::SI_VehChrgnCtrlSrvSkeleton* skeleton_;

  /*!
   * \brief Binding-related handler to serialize and send a response right after the method call.
   */
  ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonResponseHandler<SkeletonVehChrgnCtrl> response_handler_;

  /*!
   * \brief Buffer the SOME/IP header from the request for the response with the corresponding client ID and session ID.
   */
  ::amsr::someip_protocol::internal::SomeIpMessageHeader const header_;

  /*!
   * \brief Serialized Method Request [SOME/IP Header + Payload].
   */
  ::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet_;
};


}  // namespace methods


}  // namespace internal
}  // namespace someip_binding_transformation_layer
}  // namespace amsr
}  // namespace vehchrgnctrlsrv_si
}  // namespace pt_powersys
}  // namespace gwm

#endif  // DENPENDENCYSRVEXE_INCLUDE_AMSR_SOMEIP_BINDING_TRANSFORMATION_LAYER_INTERNAL_GWM_PT_POWERSYS_VEHCHRGNCTRLSRV_SI_SI_VEHCHRGNCTRLSRV_SKELETON_SOMEIP_METHODS_H_

