#ifndef GWM_MANAGER_WINDOW_MANAGER_HPP
#define GWM_MANAGER_WINDOW_MANAGER_HPP

/*
*   COPY RIGHT NOTICE
*   
*
*
*/


#include<future>
#include<memory>
#include<functional>

#include "logger/logger.hpp"

#include "TimerSrvProxy.hpp"

#include "workerthread.hpp"

namespace gwm {
namespace manager {

using TimerSrvService = gwm::services::timer::proxy::TimerSrvService;

class TimerManager {
public:
    static TimerManager* getInstance() {
        static TimerManager* intsance = new TimerManager();
        return intsance;
    }

    void start();
    void test(int a);

protected:
    TimerManager();
    TimerManager(const TimerManager& ) = delete;

    void initControllers();
    void startControllers();
    
    std::shared_ptr<TimerSrvService> srv_TimerSrvService;

    gwm::platform::Logger logger;
private:
    std::unique_ptr<gwm::utils::WorkerThread> worker_test;
};

}
}

#endif