/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  TiMgrSrvExe/include/gwm/application_remsdl/timersrv_si/SI_TimerSrv_proxy_update_manager.h
 *        \brief  Proxy update manager of service 'SI_TimerSrv'.
 *
 *      \details  This Service interface provides timer
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_socal
 *         Commit ID: 209b3c0234013e84e3170ee826c9a8c6022de194
 *********************************************************************************************************************/

#ifndef TIMGRSRVEXE_INCLUDE_GWM_APPLICATION_REMSDL_TIMERSRV_SI_SI_TIMERSRV_PROXY_UPDATE_MANAGER_H_
#define TIMGRSRVEXE_INCLUDE_GWM_APPLICATION_REMSDL_TIMERSRV_SI_SI_TIMERSRV_PROXY_UPDATE_MANAGER_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "amsr/socal/internal/events/proxy_event_backend_interface.h"
#include "amsr/socal/internal/proxy_update_manager.h"
#include "ara/core/future.h"
#include "gwm/application_remsdl/timersrv_si/SI_TimerSrv_proxy_backend_interface.h"
#include "gwm/application_remsdl/timersrv_si/SI_TimerSrv_types.h"

namespace gwm {
namespace application_remsdl {
namespace timersrv_si {
namespace internal {

/*!
 * \brief Proxy update manager for the Service 'SI_TimerSrv'
 * \remark generated
 */
class SI_TimerSrvProxyUpdateManager : public SI_TimerSrvProxyBackendInterface,
                                                            public ::amsr::socal::internal::ProxyUpdateManager<SI_TimerSrvProxyBackendInterface>{
 public:
  using BackendInterfaceType = SI_TimerSrvProxyBackendInterface;
  using Base = ::amsr::socal::internal::ProxyUpdateManager<SI_TimerSrvProxyBackendInterface>;

  SI_TimerSrvProxyUpdateManager() = delete;

  SI_TimerSrvProxyUpdateManager(SI_TimerSrvProxyUpdateManager const &) = delete;

  SI_TimerSrvProxyUpdateManager(SI_TimerSrvProxyUpdateManager &&) = delete;

  SI_TimerSrvProxyUpdateManager &operator=(SI_TimerSrvProxyUpdateManager const &) & = delete;

  SI_TimerSrvProxyUpdateManager &operator=(SI_TimerSrvProxyUpdateManager &&) & = delete;

  /*!
   * \brief Destroys the proxy update manager.
   * \pre -
   * \context   App
   */
   ~SI_TimerSrvProxyUpdateManager() noexcept override = default;

  /*!
   * \brief Initializes the ProxyUpdateManager and starts listening to the service discovery.
   * \param[in] id The instance identifier.
   * \param[in] service_discovery The proxy service discovery singleton instance.
   * \param[in] proxy_backend The proxy backend to communicate with the bindings.
   * \pre -
   * \context   App
   */
  SI_TimerSrvProxyUpdateManager(ara::com::InstanceIdentifier const& id,
    ::amsr::generic::Singleton<ServiceDiscovery*>& service_discovery,
    BackendInterfaceType& proxy_backend) noexcept
    : ProxyUpdateManager{id, service_discovery, proxy_backend} {
      StartListening();
    }

  // ---- Methods --------------------------------------------------------------------------------------------------

  /*!
   * \brief Invokes request response method TimerCncl.
   * \return ara::core::Future with method output data element.
   * \pre -
   * \context       App
   * \synchronous   FALSE
   */
  ara::core::Future<methods::TimerCncl::Output> HandleMethodTimerCncl() noexcept override {    
    MethodHandler<> const methodHandler{GetBackend()};
    return methodHandler.RequestResponseMethod<methods::TimerCncl::Output, 
            &BackendInterfaceType::HandleMethodTimerCncl>();
  }

  /*!
   * \brief Invokes request response method TimerReq.
    * \param[in] TimerSet IN parameter of type ::gwm::platform_timer::timer_idt::TimerSet_Integer_Idt
   * \return ara::core::Future with method output data element.
   * \pre -
   * \context       App
   * \synchronous   FALSE
   */
  ara::core::Future<methods::TimerReq::Output> HandleMethodTimerReq(::gwm::platform_timer::timer_idt::TimerSet_Integer_Idt const& TimerSet) noexcept override {    
    MethodHandler<::gwm::platform_timer::timer_idt::TimerSet_Integer_Idt const&> const methodHandler{GetBackend()};
    return methodHandler.RequestResponseMethod<methods::TimerReq::Output, 
            &BackendInterfaceType::HandleMethodTimerReq>(TimerSet);
  }

  // ---- Events ---------------------------------------------------------------------------------------------------

  // ---- Fields ---------------------------------------------------------------------------------------------------

  // ---- Field 'TimerSt' ----
  /*!
   * \brief Get the field notifier object for the service field 'TimerSt'
   * \return A proxy event object supporting event sample and subscription.
   * \pre -
   * \context App
   */
    ::amsr::socal::internal::events::ProxyEventBackendInterface<::gwm::platform_timer::timer_idt::TimerSt_Struct_Idt>& GetFieldNotifierBackendTimerSt() noexcept override {
      return GetBackend().GetFieldNotifierBackendTimerSt();
    }

  /*!
   * \brief Getter for the field 'TimerSt'
   * \return ara::core::Future with Field data element.
   * \pre -
   * \context App
   */
    ara::core::Future<fields::TimerSt::Output> HandleFieldTimerStMethodGet() noexcept override {
      return this->FieldMethodGet<fields::TimerSt::Output, &BackendInterfaceType::HandleFieldTimerStMethodGet>();
    }

};

} //namespace internal
}  // namespace timersrv_si
}  // namespace application_remsdl
}  // namespace gwm

#endif  // TIMGRSRVEXE_INCLUDE_GWM_APPLICATION_REMSDL_TIMERSRV_SI_SI_TIMERSRV_PROXY_UPDATE_MANAGER_H_

