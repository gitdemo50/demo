/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/include/amsr/someip_protocol/internal/dataprototype_deserialization/gwm_thermmng_thermmng_battthermctrlsrv_si/methods/deserializer_BattThermCtrlCommand.h
 *        \brief  SOME/IP protocol deserializer implementation for data prototype '/ServiceInterfaces/SI_BattThermCtrlSrv/BattThermCtrl/Command
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_someipprotocol
 *         Commit ID: 0e137b9e6356987a6d2839c8d79c286219b68d60
 *********************************************************************************************************************/

#ifndef DENPENDENCYSRVEXE_INCLUDE_AMSR_SOMEIP_PROTOCOL_INTERNAL_DATAPROTOTYPE_DESERIALIZATION_GWM_THERMMNG_THERMMNG_BATTTHERMCTRLSRV_SI_METHODS_deserializer_BattThermCtrlCommand_h_
#define DENPENDENCYSRVEXE_INCLUDE_AMSR_SOMEIP_PROTOCOL_INTERNAL_DATAPROTOTYPE_DESERIALIZATION_GWM_THERMMNG_THERMMNG_BATTTHERMCTRLSRV_SI_METHODS_deserializer_BattThermCtrlCommand_h_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include <cstdint>
#include "someip-protocol/internal/deserialization/common.h"
#include "someip-protocol/internal/deserialization/reader.h"

namespace amsr {
namespace someip_protocol {
namespace internal {
namespace dataprototype_deserializer {
namespace gwm_thermmng_thermmng_battthermctrlsrv_si {
namespace methods {

/*!
 * \brief Deserializer for method argument /ServiceInterfaces/SI_BattThermCtrlSrv/BattThermCtrl/Command
 *        of service interface /ServiceInterfaces/SI_BattThermCtrlSrv.
 * \details Top-Level data type: /AUTOSAR/StdTypes/int8_t
 *          Effective transformation properties of the DataPrototype:
 *          - ByteOrder:                    MOST-SIGNIFICANT-BYTE-FIRST (big-endian)
 *          - sizeOfArrayLengthField:       0
 *          - sizeOfVectorLengthField:      4
 *          - sizeOfMapLengthField:         4
 *          - sizeOfStringLengthField:      4
 *          - sizeOfStructLengthField:      0
 *          - sizeOfUnionLengthField:       4
 *          - sizeOfUnionTypeSelectorField: 4
 *          - isBomActive:                  true
 *          - isNullTerminationActive:      true
 *          - isDynamicLengthFieldSize:     false
 */
class DeserializerBattThermCtrlCommand {
 public:
  /*!
   * \brief Deserialize the data prototype method argument /ServiceInterfaces/SI_BattThermCtrlSrv/BattThermCtrl/Command.
   *
   * \param[in,out] reader Reference to the byte stream reader.
   * \param[out]    data Reference to data object of top-level data type
   *                /AUTOSAR/StdTypes/int8_t
   *                in which the deserialized value will be written.
   *
   * \pre           -
   * \context       Reactor|App
   * \threadsafe    FALSE
   * \reentrant     TRUE for different reader objects.
   * \return        True if the deserialization is successful, false otherwise.
   * \synchronous   TRUE
   */
  static deserialization::Result Deserialize(deserialization::Reader &reader, std::int8_t &data);
};

}  // namespace methods
}  // namespace gwm_thermmng_thermmng_battthermctrlsrv_si
}  // namespace dataprototype_deserializer
}  // namespace internal
}  // namespace someip_protocol
}  // namespace amsr

#endif  // DENPENDENCYSRVEXE_INCLUDE_AMSR_SOMEIP_PROTOCOL_INTERNAL_DATAPROTOTYPE_DESERIALIZATION_GWM_THERMMNG_THERMMNG_BATTTHERMCTRLSRV_SI_METHODS_deserializer_BattThermCtrlCommand_h_

