/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/include/gwm/pt_powersys/obcstsrv_si/si_obcstsrv_skeleton.h
 *        \brief  Skeleton for service 'SI_OBCStSrv'.
 *
 *      \details  This service interface provides OBC working status information
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_socal
 *         Commit ID: 209b3c0234013e84e3170ee826c9a8c6022de194
 *********************************************************************************************************************/

#ifndef DENPENDENCYSRVEXE_INCLUDE_GWM_PT_POWERSYS_OBCSTSRV_SI_SI_OBCSTSRV_SKELETON_H_
#define DENPENDENCYSRVEXE_INCLUDE_GWM_PT_POWERSYS_OBCSTSRV_SI_SI_OBCSTSRV_SKELETON_H_

/*!
 * \trace SPEC-4980239
 */
/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "amsr/generic/singleton_wrapper.h"
#include "amsr/socal/events/skeleton_event.h"
#include "amsr/socal/fields/skeleton_field.h"
#include "amsr/socal/internal/events/skeleton_event_manager_interface.h"
#include "amsr/socal/internal/service_discovery/service_discovery.h"
#include "amsr/socal/skeleton.h"
#include "ara/core/future.h"
#include "ara/core/instance_specifier.h"
#include "gwm/pt_powersys/obcstsrv_si/SI_OBCStSrv_skeleton_impl_interface.h"
#include "gwm/pt_powersys/obcstsrv_si/SI_OBCStSrv_types.h"

/*!
 * \trace SPEC-4980240
 */
namespace gwm {
namespace pt_powersys {
namespace obcstsrv_si {
/*!
 * \trace SPEC-4980241
 */
namespace skeleton {
/*!
 * \brief Forward declaration for inserting as a type into the template class SkeletonEvent
 */
class SI_OBCStSrvSkeleton;

/*!
 * \trace SPEC-4980244
 */
namespace methods {

}  // namespace methods

/*!
 * \trace SPEC-4980243
 */
namespace events {

/*!
 * \brief Type alias for service event 'OBCActT', that is part of the skeleton.
 *
 * \trace SPEC-4980342
 */
using OBCActT = ::amsr::socal::events::SkeletonEvent<
    gwm::pt_powersys::obcstsrv_si::skeleton::SI_OBCStSrvSkeleton,
    ::gwm::pt_powersys::powersys_idt::OBCActT_Float_Idt,
    gwm::pt_powersys::obcstsrv_si::internal::SI_OBCStSrvSkeletonImplInterface,
    ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_powersys::powersys_idt::OBCActT_Float_Idt>,
    &gwm::pt_powersys::obcstsrv_si::internal::SI_OBCStSrvSkeletonImplInterface::GetEventManagerOBCActT>;

/*!
 * \brief Type alias for service event 'OBCChrgLim', that is part of the skeleton.
 *
 * \trace SPEC-4980342
 */
using OBCChrgLim = ::amsr::socal::events::SkeletonEvent<
    gwm::pt_powersys::obcstsrv_si::skeleton::SI_OBCStSrvSkeleton,
    ::gwm::pt_powersys::powersys_idt::OBCChrgLim_Struct_Idt,
    gwm::pt_powersys::obcstsrv_si::internal::SI_OBCStSrvSkeletonImplInterface,
    ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_powersys::powersys_idt::OBCChrgLim_Struct_Idt>,
    &gwm::pt_powersys::obcstsrv_si::internal::SI_OBCStSrvSkeletonImplInterface::GetEventManagerOBCChrgLim>;

/*!
 * \brief Type alias for service event 'OBCDchrOutpI', that is part of the skeleton.
 *
 * \trace SPEC-4980342
 */
using OBCDchrOutpI = ::amsr::socal::events::SkeletonEvent<
    gwm::pt_powersys::obcstsrv_si::skeleton::SI_OBCStSrvSkeleton,
    ::gwm::pt_powersys::powersys_idt::OBCDchrOutpI_Float_Idt,
    gwm::pt_powersys::obcstsrv_si::internal::SI_OBCStSrvSkeletonImplInterface,
    ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_powersys::powersys_idt::OBCDchrOutpI_Float_Idt>,
    &gwm::pt_powersys::obcstsrv_si::internal::SI_OBCStSrvSkeletonImplInterface::GetEventManagerOBCDchrOutpI>;

/*!
 * \brief Type alias for service event 'OBCDchrOutpU', that is part of the skeleton.
 *
 * \trace SPEC-4980342
 */
using OBCDchrOutpU = ::amsr::socal::events::SkeletonEvent<
    gwm::pt_powersys::obcstsrv_si::skeleton::SI_OBCStSrvSkeleton,
    ::gwm::pt_powersys::powersys_idt::OBCDchrOutpU_Float_Idt,
    gwm::pt_powersys::obcstsrv_si::internal::SI_OBCStSrvSkeletonImplInterface,
    ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_powersys::powersys_idt::OBCDchrOutpU_Float_Idt>,
    &gwm::pt_powersys::obcstsrv_si::internal::SI_OBCStSrvSkeletonImplInterface::GetEventManagerOBCDchrOutpU>;

/*!
 * \brief Type alias for service event 'OBCInptAcI', that is part of the skeleton.
 *
 * \trace SPEC-4980342
 */
using OBCInptAcI = ::amsr::socal::events::SkeletonEvent<
    gwm::pt_powersys::obcstsrv_si::skeleton::SI_OBCStSrvSkeleton,
    ::gwm::pt_powersys::powersys_idt::OBCInptAcI_Float_Idt,
    gwm::pt_powersys::obcstsrv_si::internal::SI_OBCStSrvSkeletonImplInterface,
    ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_powersys::powersys_idt::OBCInptAcI_Float_Idt>,
    &gwm::pt_powersys::obcstsrv_si::internal::SI_OBCStSrvSkeletonImplInterface::GetEventManagerOBCInptAcI>;

/*!
 * \brief Type alias for service event 'OBCInptAcU', that is part of the skeleton.
 *
 * \trace SPEC-4980342
 */
using OBCInptAcU = ::amsr::socal::events::SkeletonEvent<
    gwm::pt_powersys::obcstsrv_si::skeleton::SI_OBCStSrvSkeleton,
    ::gwm::pt_powersys::powersys_idt::OBCInptAcU_Float_Idt,
    gwm::pt_powersys::obcstsrv_si::internal::SI_OBCStSrvSkeletonImplInterface,
    ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_powersys::powersys_idt::OBCInptAcU_Float_Idt>,
    &gwm::pt_powersys::obcstsrv_si::internal::SI_OBCStSrvSkeletonImplInterface::GetEventManagerOBCInptAcU>;

/*!
 * \brief Type alias for service event 'OBCOuptDcI', that is part of the skeleton.
 *
 * \trace SPEC-4980342
 */
using OBCOuptDcI = ::amsr::socal::events::SkeletonEvent<
    gwm::pt_powersys::obcstsrv_si::skeleton::SI_OBCStSrvSkeleton,
    ::gwm::pt_powersys::powersys_idt::OBCOuptDcI_Float_Idt,
    gwm::pt_powersys::obcstsrv_si::internal::SI_OBCStSrvSkeletonImplInterface,
    ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_powersys::powersys_idt::OBCOuptDcI_Float_Idt>,
    &gwm::pt_powersys::obcstsrv_si::internal::SI_OBCStSrvSkeletonImplInterface::GetEventManagerOBCOuptDcI>;

/*!
 * \brief Type alias for service event 'OBCOuptDcU', that is part of the skeleton.
 *
 * \trace SPEC-4980342
 */
using OBCOuptDcU = ::amsr::socal::events::SkeletonEvent<
    gwm::pt_powersys::obcstsrv_si::skeleton::SI_OBCStSrvSkeleton,
    ::gwm::pt_powersys::powersys_idt::OBCOuptDcU_Float_Idt,
    gwm::pt_powersys::obcstsrv_si::internal::SI_OBCStSrvSkeletonImplInterface,
    ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_powersys::powersys_idt::OBCOuptDcU_Float_Idt>,
    &gwm::pt_powersys::obcstsrv_si::internal::SI_OBCStSrvSkeletonImplInterface::GetEventManagerOBCOuptDcU>;

}  // namespace events

/*!
 * \trace SPEC-4980245
 */
namespace fields {
/*!
 * \brief Type alias for the notification of field 'OBCChrgGunSt'.
 */
using FieldNotifierOBCChrgGunSt = ::amsr::socal::events::SkeletonEvent<
  gwm::pt_powersys::obcstsrv_si::skeleton::SI_OBCStSrvSkeleton,
  ::gwm::pt_powersys::powersys_idt::OBCChrgGunSt_Struct_Idt,
  gwm::pt_powersys::obcstsrv_si::internal::SI_OBCStSrvSkeletonImplInterface,
  ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_powersys::powersys_idt::OBCChrgGunSt_Struct_Idt>,
  &gwm::pt_powersys::obcstsrv_si::internal::SI_OBCStSrvSkeletonImplInterface::GetFieldNotifierOBCChrgGunSt>;

/*!
 * \brief Type alias for the notifier configuration of field 'OBCChrgGunSt'.
 */
using FieldNotifierConfigOBCChrgGunSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasNotifier<true, FieldNotifierOBCChrgGunSt>;

/*!
 * \brief Type alias for the getter configuration of field 'OBCChrgGunSt'.
 */
using FieldGetterConfigOBCChrgGunSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasGetter<true>;

/*!
 * \brief Type alias for the setter configuration of field 'OBCChrgGunSt'.
 */
using FieldSetterConfigOBCChrgGunSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasSetter<false>;

/*!
 * \brief Field name.
 */
struct OBCChrgGunStName {
  /*!
   * \brief Field name string.
   */
  static constexpr vac::container::CStringView Name{"OBCChrgGunSt"_sv};
};

/*!
 * \brief Type alias for the parameterization of field 'OBCChrgGunSt'.
 */
using FieldConfigOBCChrgGunSt = ::amsr::socal::internal::fields::SkeletonFieldConfig<gwm::pt_powersys::obcstsrv_si::skeleton::SI_OBCStSrvSkeleton, FieldNotifierConfigOBCChrgGunSt, FieldGetterConfigOBCChrgGunSt, FieldSetterConfigOBCChrgGunSt, OBCChrgGunStName>;

/*!
 * \brief Type alias for service field 'OBCChrgGunSt', that is part of the skeleton.
 *
 * \trace SPEC-4980343
 */
using OBCChrgGunSt = ::amsr::socal::fields::SkeletonField<::gwm::pt_powersys::powersys_idt::OBCChrgGunSt_Struct_Idt, FieldConfigOBCChrgGunSt>;

/*!
 * \brief Type alias for the notification of field 'OBCErrSt'.
 */
using FieldNotifierOBCErrSt = ::amsr::socal::events::SkeletonEvent<
  gwm::pt_powersys::obcstsrv_si::skeleton::SI_OBCStSrvSkeleton,
  ::gwm::pt_powersys::powersys_idt::OBCErrSt_Struct_Idt,
  gwm::pt_powersys::obcstsrv_si::internal::SI_OBCStSrvSkeletonImplInterface,
  ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_powersys::powersys_idt::OBCErrSt_Struct_Idt>,
  &gwm::pt_powersys::obcstsrv_si::internal::SI_OBCStSrvSkeletonImplInterface::GetFieldNotifierOBCErrSt>;

/*!
 * \brief Type alias for the notifier configuration of field 'OBCErrSt'.
 */
using FieldNotifierConfigOBCErrSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasNotifier<true, FieldNotifierOBCErrSt>;

/*!
 * \brief Type alias for the getter configuration of field 'OBCErrSt'.
 */
using FieldGetterConfigOBCErrSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasGetter<true>;

/*!
 * \brief Type alias for the setter configuration of field 'OBCErrSt'.
 */
using FieldSetterConfigOBCErrSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasSetter<false>;

/*!
 * \brief Field name.
 */
struct OBCErrStName {
  /*!
   * \brief Field name string.
   */
  static constexpr vac::container::CStringView Name{"OBCErrSt"_sv};
};

/*!
 * \brief Type alias for the parameterization of field 'OBCErrSt'.
 */
using FieldConfigOBCErrSt = ::amsr::socal::internal::fields::SkeletonFieldConfig<gwm::pt_powersys::obcstsrv_si::skeleton::SI_OBCStSrvSkeleton, FieldNotifierConfigOBCErrSt, FieldGetterConfigOBCErrSt, FieldSetterConfigOBCErrSt, OBCErrStName>;

/*!
 * \brief Type alias for service field 'OBCErrSt', that is part of the skeleton.
 *
 * \trace SPEC-4980343
 */
using OBCErrSt = ::amsr::socal::fields::SkeletonField<::gwm::pt_powersys::powersys_idt::OBCErrSt_Struct_Idt, FieldConfigOBCErrSt>;

/*!
 * \brief Type alias for the notification of field 'OBCHVILSt'.
 */
using FieldNotifierOBCHVILSt = ::amsr::socal::events::SkeletonEvent<
  gwm::pt_powersys::obcstsrv_si::skeleton::SI_OBCStSrvSkeleton,
  ::gwm::pt_powersys::powersys_idt::OBCHVILSt_Enum_Idt,
  gwm::pt_powersys::obcstsrv_si::internal::SI_OBCStSrvSkeletonImplInterface,
  ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_powersys::powersys_idt::OBCHVILSt_Enum_Idt>,
  &gwm::pt_powersys::obcstsrv_si::internal::SI_OBCStSrvSkeletonImplInterface::GetFieldNotifierOBCHVILSt>;

/*!
 * \brief Type alias for the notifier configuration of field 'OBCHVILSt'.
 */
using FieldNotifierConfigOBCHVILSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasNotifier<true, FieldNotifierOBCHVILSt>;

/*!
 * \brief Type alias for the getter configuration of field 'OBCHVILSt'.
 */
using FieldGetterConfigOBCHVILSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasGetter<true>;

/*!
 * \brief Type alias for the setter configuration of field 'OBCHVILSt'.
 */
using FieldSetterConfigOBCHVILSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasSetter<false>;

/*!
 * \brief Field name.
 */
struct OBCHVILStName {
  /*!
   * \brief Field name string.
   */
  static constexpr vac::container::CStringView Name{"OBCHVILSt"_sv};
};

/*!
 * \brief Type alias for the parameterization of field 'OBCHVILSt'.
 */
using FieldConfigOBCHVILSt = ::amsr::socal::internal::fields::SkeletonFieldConfig<gwm::pt_powersys::obcstsrv_si::skeleton::SI_OBCStSrvSkeleton, FieldNotifierConfigOBCHVILSt, FieldGetterConfigOBCHVILSt, FieldSetterConfigOBCHVILSt, OBCHVILStName>;

/*!
 * \brief Type alias for service field 'OBCHVILSt', that is part of the skeleton.
 *
 * \trace SPEC-4980343
 */
using OBCHVILSt = ::amsr::socal::fields::SkeletonField<::gwm::pt_powersys::powersys_idt::OBCHVILSt_Enum_Idt, FieldConfigOBCHVILSt>;

/*!
 * \brief Type alias for the notification of field 'OBCSt'.
 */
using FieldNotifierOBCSt = ::amsr::socal::events::SkeletonEvent<
  gwm::pt_powersys::obcstsrv_si::skeleton::SI_OBCStSrvSkeleton,
  ::gwm::pt_powersys::powersys_idt::OBCSt_Enum_Idt,
  gwm::pt_powersys::obcstsrv_si::internal::SI_OBCStSrvSkeletonImplInterface,
  ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_powersys::powersys_idt::OBCSt_Enum_Idt>,
  &gwm::pt_powersys::obcstsrv_si::internal::SI_OBCStSrvSkeletonImplInterface::GetFieldNotifierOBCSt>;

/*!
 * \brief Type alias for the notifier configuration of field 'OBCSt'.
 */
using FieldNotifierConfigOBCSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasNotifier<true, FieldNotifierOBCSt>;

/*!
 * \brief Type alias for the getter configuration of field 'OBCSt'.
 */
using FieldGetterConfigOBCSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasGetter<true>;

/*!
 * \brief Type alias for the setter configuration of field 'OBCSt'.
 */
using FieldSetterConfigOBCSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasSetter<false>;

/*!
 * \brief Field name.
 */
struct OBCStName {
  /*!
   * \brief Field name string.
   */
  static constexpr vac::container::CStringView Name{"OBCSt"_sv};
};

/*!
 * \brief Type alias for the parameterization of field 'OBCSt'.
 */
using FieldConfigOBCSt = ::amsr::socal::internal::fields::SkeletonFieldConfig<gwm::pt_powersys::obcstsrv_si::skeleton::SI_OBCStSrvSkeleton, FieldNotifierConfigOBCSt, FieldGetterConfigOBCSt, FieldSetterConfigOBCSt, OBCStName>;

/*!
 * \brief Type alias for service field 'OBCSt', that is part of the skeleton.
 *
 * \trace SPEC-4980343
 */
using OBCSt = ::amsr::socal::fields::SkeletonField<::gwm::pt_powersys::powersys_idt::OBCSt_Enum_Idt, FieldConfigOBCSt>;

/*!
 * \brief Type alias for the notification of field 'OBCV2XSt'.
 */
using FieldNotifierOBCV2XSt = ::amsr::socal::events::SkeletonEvent<
  gwm::pt_powersys::obcstsrv_si::skeleton::SI_OBCStSrvSkeleton,
  ::gwm::pt_powersys::powersys_idt::OBCV2XSt_Enum_Idt,
  gwm::pt_powersys::obcstsrv_si::internal::SI_OBCStSrvSkeletonImplInterface,
  ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_powersys::powersys_idt::OBCV2XSt_Enum_Idt>,
  &gwm::pt_powersys::obcstsrv_si::internal::SI_OBCStSrvSkeletonImplInterface::GetFieldNotifierOBCV2XSt>;

/*!
 * \brief Type alias for the notifier configuration of field 'OBCV2XSt'.
 */
using FieldNotifierConfigOBCV2XSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasNotifier<true, FieldNotifierOBCV2XSt>;

/*!
 * \brief Type alias for the getter configuration of field 'OBCV2XSt'.
 */
using FieldGetterConfigOBCV2XSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasGetter<true>;

/*!
 * \brief Type alias for the setter configuration of field 'OBCV2XSt'.
 */
using FieldSetterConfigOBCV2XSt = ::amsr::socal::internal::fields::SkeletonFieldParams::HasSetter<false>;

/*!
 * \brief Field name.
 */
struct OBCV2XStName {
  /*!
   * \brief Field name string.
   */
  static constexpr vac::container::CStringView Name{"OBCV2XSt"_sv};
};

/*!
 * \brief Type alias for the parameterization of field 'OBCV2XSt'.
 */
using FieldConfigOBCV2XSt = ::amsr::socal::internal::fields::SkeletonFieldConfig<gwm::pt_powersys::obcstsrv_si::skeleton::SI_OBCStSrvSkeleton, FieldNotifierConfigOBCV2XSt, FieldGetterConfigOBCV2XSt, FieldSetterConfigOBCV2XSt, OBCV2XStName>;

/*!
 * \brief Type alias for service field 'OBCV2XSt', that is part of the skeleton.
 *
 * \trace SPEC-4980343
 */
using OBCV2XSt = ::amsr::socal::fields::SkeletonField<::gwm::pt_powersys::powersys_idt::OBCV2XSt_Enum_Idt, FieldConfigOBCV2XSt>;


}  // namespace fields

/*!
 * \brief Skeleton interface class for the service 'SI_OBCStSrv'.
 * \details This service interface provides OBC working status information
 *
 * \vpublic
 * \trace SPEC-4980341
 */
class SI_OBCStSrvSkeleton
    : public ::amsr::socal::Skeleton<gwm::pt_powersys::obcstsrv_si::SI_OBCStSrv,
                                          gwm::pt_powersys::obcstsrv_si::internal::SI_OBCStSrvSkeletonImplInterface> {
 public:
// ---- Constructors / Destructors -----------------------------------------------------------------------------------
/*!
 * \brief Exception-less pre-construction of SI_OBCStSrv.
 *
 * \param[in] instance The InstanceIdentifier of the service instance to be created.
 *                     Expected format: "<Binding type/prefix>:<binding specific instance ID>".
 *                     The InstanceIdentifier must fulfill the following preconditions:
 *                     - Must be configured in the ARXML model.
 *                     - Must belong to the service interface.
 * \param[in] mode The mode of the service implementation for processing service method invocations.
 *                 Default: Event-driven processing.
 *                 Preconditions to be fulfilled:
 *                 - If the mode 'kEventSingleThread' is used, a possible user-defined ThreadPool must have exactly one
 *                   worker thread configured.
 * \return Result<ConstructionToken> Result containing construction token from which a skeleton object can be
 *         constructed.
 *
 * \pre No other service skeleton for the same instance must exist concurrently. After destruction of an already
 *      created skeleton object, the instantiation for the same service instance will be possible.
 * \context App
 * \threadsafe FALSE
 * \reentrant FALSE
 * \vpublic
 * \synchronous TRUE
 * \trace SPEC-8053551
 * \trace SPEC-8053550
 */
static ConstructionResult Preconstruct(
    ara::com::InstanceIdentifier instance_id,
    ara::com::MethodCallProcessingMode const mode = ara::com::MethodCallProcessingMode::kEvent) noexcept;

/*!
 * \brief Exception-less pre-construction of SI_OBCStSrvSkeleton.
 *
 * \param[in] instance InstanceSpecifier of this service.
 *                     The provided InstanceSpecifier must fulfill the following preconditions:
 *                     - Must be configured in the ARXML model.
 *                     - Must belong to the service interface.
 * \param[in] mode The mode of the service implementation for processing service method invocations.
 *                 Default: Event-driven processing.
 *                 Preconditions to be fulfilled:
 *                 - If the mode 'kEventSingleThread' is used, a possible user-defined ThreadPool must have exactly one
 *                   worker thread configured.
 * \return Result<ConstructionToken> Result containing construction token from which a skeleton object can be
 *         constructed.
 *
 * \pre No other service skeleton for the same instance must exist concurrently. After destruction of an already
 *      created skeleton object, the instantation for the same service instance will be possible.
 * \context App
 * \threadsafe FALSE
 * \reentrant FALSE
 * \vpublic
 * \synchronous TRUE
 * \trace SPEC-8053553
 * \trace SPEC-8053550
 */
static ConstructionResult Preconstruct(
    ara::core::InstanceSpecifier instance,
    ara::com::MethodCallProcessingMode const mode = ara::com::MethodCallProcessingMode::kEvent) noexcept;

/*!
 * \brief Exception-less pre-construction of SI_OBCStSrvSkeleton.
 *
 * \param[in] instance_identifiers The container of instances of a service, each instance element needed to distinguish
 *                                 different instances of exactly the same service in the system.
 *                                 The provided InstanceIdentifierContainer must fulfill the following preconditions:
 *                                 - Every InstanceIdentifier of the container must be configured in the ARXML model.
 *                                 - Every InstanceIdentifier of the container must belong to the service interface to be
 *                                   instantiated.
 *                                 - The container must not be empty.
 *                                 - All elements of the container must be unique (no duplicates).
 * \param[in] mode The mode of the service implementation for processing service method invocations.
 *                 Default: Event-driven processing.
 *                 Preconditions to be fulfilled:
 *                 - If the mode 'kEventSingleThread' is used, a possible user-defined ThreadPool must have exactly one
 *                   worker thread configured.
 * \return Result<ConstructionToken> Result containing construction token from which a skeleton object can be
 *         constructed.
 *
 * \pre No other service skeleton for the same instance must exist concurrently. After destruction of an already
 *      created skeleton object, the instantation for the same service instance will be possible.
 * \context App
 * \threadsafe FALSE
 * \reentrant FALSE
 * \vpublic
 * \synchronous TRUE
 * \trace SPEC-8053555
 * \trace SPEC-8053550
 */
static ConstructionResult Preconstruct(
    ara::com::InstanceIdentifierContainer instance_identifiers,
    ara::com::MethodCallProcessingMode const mode = ara::com::MethodCallProcessingMode::kEvent) noexcept;

/*!
 * \brief Exception-less constructor of SI_OBCStSrvSkeleton.
 * \details Because of internal resource management strategy, all created skeletons shall be released before the
 *          Runtime is destroyed; i.e. they cannot not be stored in variables with longer life period than the
 *          application's main(). If not followed, it's not guaranteed that the communication middleware is shut down
 *          properly and may lead to segmentation fault.
 *
 * \param[in] token ConstructionToken created with Preconstruct() API.
 * \context App
 * \threadsafe FALSE
 * \reentrant FALSE
 * \vpublic
 * \synchronous TRUE
 * \trace SPEC-8053551
 * \trace SPEC-8053553
 * \trace SPEC-8053555
 */
explicit SI_OBCStSrvSkeleton(ConstructionToken&& token) noexcept;

  /*!
   * \brief Delete default constructor.
   */
  SI_OBCStSrvSkeleton() = delete;
  /*!
   * \brief Delete copy constructor.
   */
  SI_OBCStSrvSkeleton(SI_OBCStSrvSkeleton const &) = delete;
  /*!
   * \brief Delete move constructor.
   */
  SI_OBCStSrvSkeleton(SI_OBCStSrvSkeleton &&) = delete;
  /*!
   * \brief Delete copy assignment.
   */
  SI_OBCStSrvSkeleton &operator=(SI_OBCStSrvSkeleton const &) & = delete;
  /*!
   * \brief Delete move assignment.
   */
  SI_OBCStSrvSkeleton &operator=(SI_OBCStSrvSkeleton &&) & = delete;

  /*!
   * \brief Constructor of SI_OBCStSrvSkeleton.
   *
   * \remark Because of internal resource management strategy, all created skeletons shall be released before the Runtime
   * is destroyed; i.e. they cannot not be stored in variables with longer life period than the application's main().
   * If not followed, it's not guaranteed that the communication middleware is shut down properly and may lead to
   * segmentation fault.
   *
   * \param[in] instance The identifier of a specific instance of a service, needed to distinguish different instances of
   *                     exactly the same service in the system.
   *
   * \param[in] mode The mode of the service implpementation for processing service method invocations.
   *                 Default: Event-driven processing
   *
   * \pre No other service skeleton for the same instance must exist concurrently. After destruction of an already
   *      created skeleton object, the instantiation for the same service instance will be possible.
   *
   * \context App
   * \threadsafe FALSE
   * \reentrant FALSE
   * \vprivate Vector component internal API.
   * \synchronous TRUE
   * \trace SPEC-4980351
   * \trace SPEC-4980356
   */
   explicit SI_OBCStSrvSkeleton(
      ara::com::InstanceIdentifier instance,
      ara::com::MethodCallProcessingMode mode = ara::com::MethodCallProcessingMode::kEvent) noexcept;

  /*!
   * \brief Constructor of SI_OBCStSrvSkeleton.
   *
   * \remark Because of internal resource management strategy, all created skeletons shall be released before the Runtime
   * is destroyed; i.e. they cannot not be stored in variables with longer life period than the application's main().
   * If not followed, it's not guaranteed that the communication middleware is shut down properly and may lead to
   * segmentation fault.
   *
   * \param[in] instance The InstanceSpecifier of a specific service instance, needed to distinguish different instances
   *                     of exactly the same service in the system.
   *
   * \param[in] mode The mode of the service implementation for processing service method invocations.
   *                 Default: Event-driven processing
   *
   * \pre No other service skeleton for the same instance must exist concurrently. After destruction of an already
   *      created skeleton object, the instantiation for the same service instance will be possible.
   *
   * \context App
   * \threadsafe FALSE
   * \reentrant FALSE
   * \vprivate Vector component internal API.
   * \synchronous TRUE
   * \trace SPEC-8053552
   * \trace SPEC-4980356
   */
   explicit SI_OBCStSrvSkeleton(
      ara::core::InstanceSpecifier instance,
      ara::com::MethodCallProcessingMode mode = ara::com::MethodCallProcessingMode::kEvent) noexcept;

  /*!
   * \brief Constructor of SI_OBCStSrvSkeleton.
   *
   * \remark Because of internal resource management strategy, all created skeletons shall be released before the Runtime
   * is destroyed; i.e. they cannot not be stored in variables with longer life period than the application's main().
   * If not followed, it's not guaranteed that the communication middleware is shut down properly and may lead to
   * segmentation fault.
   *
   * \param[in] instance_identifiers The container of instances of a service, each instance element needed to
   *                                 distinguish different instances of exactly the same service in the system.
   *
   * \param[in] mode The mode of the service implementation for processing service method invocations.
   *                 Default: Event-driven processing
   *
   * \pre No other service skeleton for the same instance must exist concurrently. After destruction of an already
   *      created skeleton object, the instantiation for the same service instance will be possible.
   *
   * \context App
   * \threadsafe FALSE
   * \reentrant FALSE
   * \vprivate Vector component internal API.
   * \synchronous TRUE
   * \trace SPEC-8053554
   * \trace SPEC-4980356
   */
   explicit SI_OBCStSrvSkeleton(
      ara::com::InstanceIdentifierContainer instance_identifiers,
      ara::com::MethodCallProcessingMode mode = ara::com::MethodCallProcessingMode::kEvent) noexcept;


  /*!
   * \brief Stops the service if it is currently offered.
   * \details This call will be blocked until all current method requests are finished/canceled.
   * \pre -
   * \context App
   * \vpublic
   * \synchronous TRUE
   * \trace SPEC-4980351
   */
  ~SI_OBCStSrvSkeleton() noexcept override;

  /*!
   * \brief Type alias for ServiceDiscovery.
   */
  using ServiceDiscovery = ::amsr::socal::internal::service_discovery::ServiceDiscovery<SI_OBCStSrvSkeleton>;

  /*!
   * \brief       Returns the service discovery singleton.
   * \return      Reference to service discovery singleton.
   * \pre         Service discovery has been registered via RegisterServiceDiscovery.
   * \context     ANY
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \vprivate
   * \synchronous TRUE
   */
  static ::amsr::generic::Singleton<ServiceDiscovery*>& GetServiceDiscovery() noexcept;

  /*!
   * \brief       Registers the service discovery.
   * \param[in]   service_discovery Pointer to service discovery.
   * \pre         -
   * \context     Init
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \vprivate
   * \synchronous TRUE
   */
  static void RegisterServiceDiscovery(ServiceDiscovery* service_discovery) noexcept;

  /*!
   * \brief       Deregisters the service discovery.
   * \pre         RegisterServiceDiscovery has been called.
   * \context     Shutdown
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \vprivate
   * \synchronous TRUE
   */
  static void DeRegisterServiceDiscovery() noexcept;

  // ---- Methods --------------------------------------------------------------------------------------------------

  // ---- Events ---------------------------------------------------------------------------------------------------

  // VECTOR NC VectorC++-V11.0.2: MD_SOCAL_V11-0-2_public_member_in_generated_code
  /*!
   * \brief The skeleton event 'OBCActT' which can be used by application developer.
   * \details 
   * Data of type ::gwm::pt_powersys::powersys_idt::OBCActT_Float_Idt
   * \vpublic
   */
  events::OBCActT OBCActT;

  // VECTOR NC VectorC++-V11.0.2: MD_SOCAL_V11-0-2_public_member_in_generated_code
  /*!
   * \brief The skeleton event 'OBCChrgLim' which can be used by application developer.
   * \details 
   * Data of type ::gwm::pt_powersys::powersys_idt::OBCChrgLim_Struct_Idt
   * \vpublic
   */
  events::OBCChrgLim OBCChrgLim;

  // VECTOR NC VectorC++-V11.0.2: MD_SOCAL_V11-0-2_public_member_in_generated_code
  /*!
   * \brief The skeleton event 'OBCDchrOutpI' which can be used by application developer.
   * \details 
   * Data of type ::gwm::pt_powersys::powersys_idt::OBCDchrOutpI_Float_Idt
   * \vpublic
   */
  events::OBCDchrOutpI OBCDchrOutpI;

  // VECTOR NC VectorC++-V11.0.2: MD_SOCAL_V11-0-2_public_member_in_generated_code
  /*!
   * \brief The skeleton event 'OBCDchrOutpU' which can be used by application developer.
   * \details 
   * Data of type ::gwm::pt_powersys::powersys_idt::OBCDchrOutpU_Float_Idt
   * \vpublic
   */
  events::OBCDchrOutpU OBCDchrOutpU;

  // VECTOR NC VectorC++-V11.0.2: MD_SOCAL_V11-0-2_public_member_in_generated_code
  /*!
   * \brief The skeleton event 'OBCInptAcI' which can be used by application developer.
   * \details 
   * Data of type ::gwm::pt_powersys::powersys_idt::OBCInptAcI_Float_Idt
   * \vpublic
   */
  events::OBCInptAcI OBCInptAcI;

  // VECTOR NC VectorC++-V11.0.2: MD_SOCAL_V11-0-2_public_member_in_generated_code
  /*!
   * \brief The skeleton event 'OBCInptAcU' which can be used by application developer.
   * \details 
   * Data of type ::gwm::pt_powersys::powersys_idt::OBCInptAcU_Float_Idt
   * \vpublic
   */
  events::OBCInptAcU OBCInptAcU;

  // VECTOR NC VectorC++-V11.0.2: MD_SOCAL_V11-0-2_public_member_in_generated_code
  /*!
   * \brief The skeleton event 'OBCOuptDcI' which can be used by application developer.
   * \details 
   * Data of type ::gwm::pt_powersys::powersys_idt::OBCOuptDcI_Float_Idt
   * \vpublic
   */
  events::OBCOuptDcI OBCOuptDcI;

  // VECTOR NC VectorC++-V11.0.2: MD_SOCAL_V11-0-2_public_member_in_generated_code
  /*!
   * \brief The skeleton event 'OBCOuptDcU' which can be used by application developer.
   * \details 
   * Data of type ::gwm::pt_powersys::powersys_idt::OBCOuptDcU_Float_Idt
   * \vpublic
   */
  events::OBCOuptDcU OBCOuptDcU;

  // ---- Fields ---------------------------------------------------------------------------------------------------

  // VECTOR NC VectorC++-V11.0.2: MD_SOCAL_V11-0-2_public_member_in_generated_code
  /*!
   * \brief The skeleton field 'OBCChrgGunSt' which can be used by application developer.
   * \details 
   * Data of type ::gwm::pt_powersys::powersys_idt::OBCChrgGunSt_Struct_Idt
   * \vpublic
   */
  fields::OBCChrgGunSt OBCChrgGunSt;

  // VECTOR NC VectorC++-V11.0.2: MD_SOCAL_V11-0-2_public_member_in_generated_code
  /*!
   * \brief The skeleton field 'OBCErrSt' which can be used by application developer.
   * \details 
   * Data of type ::gwm::pt_powersys::powersys_idt::OBCErrSt_Struct_Idt
   * \vpublic
   */
  fields::OBCErrSt OBCErrSt;

  // VECTOR NC VectorC++-V11.0.2: MD_SOCAL_V11-0-2_public_member_in_generated_code
  /*!
   * \brief The skeleton field 'OBCHVILSt' which can be used by application developer.
   * \details 
   * Data of type ::gwm::pt_powersys::powersys_idt::OBCHVILSt_Enum_Idt
   * \vpublic
   */
  fields::OBCHVILSt OBCHVILSt;

  // VECTOR NC VectorC++-V11.0.2: MD_SOCAL_V11-0-2_public_member_in_generated_code
  /*!
   * \brief The skeleton field 'OBCSt' which can be used by application developer.
   * \details 
   * Data of type ::gwm::pt_powersys::powersys_idt::OBCSt_Enum_Idt
   * \vpublic
   */
  fields::OBCSt OBCSt;

  // VECTOR NC VectorC++-V11.0.2: MD_SOCAL_V11-0-2_public_member_in_generated_code
  /*!
   * \brief The skeleton field 'OBCV2XSt' which can be used by application developer.
   * \details 
   * Data of type ::gwm::pt_powersys::powersys_idt::OBCV2XSt_Enum_Idt
   * \vpublic
   */
  fields::OBCV2XSt OBCV2XSt;

 private:
  /*!
   * \brief Type alias for the base class.
   */
  using Base = ::amsr::socal::Skeleton<gwm::pt_powersys::obcstsrv_si::SI_OBCStSrv, gwm::pt_powersys::obcstsrv_si::internal::SI_OBCStSrvSkeletonImplInterface>;

  /*!
   * \copydoc amsr::socal::Skeleton<gwm::pt_powersys::obcstsrv_si::SI_OBCStSrv,gwm::pt_powersys::obcstsrv_si::internal::SI_OBCStSrvSkeletonImplInterface>::DoFieldInitializationChecks()
   */
  void DoFieldInitializationChecks() noexcept override;

  /*!
   * \copydoc amsr::socal::Skeleton<gwm::pt_powersys::obcstsrv_si::SI_OBCStSrv,gwm::pt_powersys::obcstsrv_si::internal::SI_OBCStSrvSkeletonImplInterface>::SendInitialFieldNotifications()
   */
  void SendInitialFieldNotifications() noexcept override;

  /*!
   * \copydoc amsr::socal::Skeleton<gwm::pt_powersys::obcstsrv_si::SI_OBCStSrv,gwm::pt_powersys::obcstsrv_si::internal::SI_OBCStSrvSkeletonImplInterface>::OfferServiceInternal()
   */
  void OfferServiceInternal(::ara::com::InstanceIdentifier const& instance_id) noexcept override;

  /*!
   * \copydoc amsr::socal::Skeleton<gwm::pt_powersys::obcstsrv_si::SI_OBCStSrv,gwm::pt_powersys::obcstsrv_si::internal::SI_OBCStSrvSkeletonImplInterface>::StopOfferServiceInternal()
   */
  void StopOfferServiceInternal(::ara::com::InstanceIdentifier const& instance_id) noexcept override;

  /*!
   * \brief The service discovery instance.
   */
  // VECTOR NC AutosarC++17_10-A3.3.2: MD_SOCAL_AutosarC++17_10-A3.3.2_StaticStorageDurationOfNonPODType
  static ::amsr::generic::Singleton<ServiceDiscovery*> sd_;
};  // class SI_OBCStSrvSkeleton

}  // namespace skeleton
}  // namespace obcstsrv_si
}  // namespace pt_powersys
}  // namespace gwm

#endif  // DENPENDENCYSRVEXE_INCLUDE_GWM_PT_POWERSYS_OBCSTSRV_SI_SI_OBCSTSRV_SKELETON_H_

