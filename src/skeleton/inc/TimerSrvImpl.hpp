#ifndef GWM_SKELETON_TIMER_SRV_HPP
#define GWM_SKELETON_TIMER_SRV_HPP

#include <memory>

#include "logger/logger.hpp"
#include "TimerBasicController.hpp"
#include "gwm/application_remsdl/timersrv_si/si_timersrv_skeleton.h"

#include "skeletonmixin.hpp"

namespace gwm {
namespace services {
namespace timer {
   
namespace skeleton {
     
using TimerSrvSkeleton = gwm::application_remsdl::timersrv_si::skeleton::SI_TimerSrvSkeleton;
using TimerBasicController = gwm::controller::TimerBasicController;
using TimerStFieldType = gwm::platform_timer::timer_idt::TimerSt_Struct_Idt;

namespace methods = gwm::application_remsdl::timersrv_si::skeleton::methods;

class TimerSrvImpl : public gwm::skeleton::SkeletonMixin<TimerSrvSkeleton> {
public:
    TimerSrvImpl(std::shared_ptr<TimerBasicController> cntrl);
      
    ara::core::Future<methods::TimerCncl::Output> TimerCncl() override;
    ara::core::Future<methods::TimerReq::Output> TimerReq(::gwm::platform_timer::timer_idt::TimerSet_Integer_Idt const &TimerSet) override;
    ara::core::Future<TimerStFieldType> getTimerSt();

private:
  
    const int32_t SECONDS_IN_MIN = 60;
    const int32_t SECONDS_IN_HR = SECONDS_IN_MIN * 60;
    const int32_t SECONDS_IN_DAY = 24 * SECONDS_IN_HR;
    const int32_t SECONDS_IN_WEEK = 7 * SECONDS_IN_DAY;
        
    gwm::platform_timer::timer_idt::TimerSet_Response_Enum_Idt transFormResult(int ret);
         
    std::shared_ptr<TimerBasicController> _controller;
          
    gwm::platform_timer::timer_idt::TimerSt_Struct_Idt _timerState;
    gwm::platform::Logger _logger;
};
          
}
}
}
}

#endif
