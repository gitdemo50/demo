/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  TiMgrSrvExe/include/gwm/composite_timermgr/timgrsrv_si/si_timgrsrv_common.h
 *        \brief  Header for service 'SI_TiMgrSrv'.
 *
 *      \details  This Service interface provides the timer manager request
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_socal
 *         Commit ID: 209b3c0234013e84e3170ee826c9a8c6022de194
 *********************************************************************************************************************/

#ifndef TIMGRSRVEXE_INCLUDE_GWM_COMPOSITE_TIMERMGR_TIMGRSRV_SI_SI_TIMGRSRV_COMMON_H_
#define TIMGRSRVEXE_INCLUDE_GWM_COMPOSITE_TIMERMGR_TIMGRSRV_SI_SI_TIMGRSRV_COMMON_H_

/*!
 * \trace SPEC-4980247, SPEC-4980248, SPEC-5951130, SPEC-4980251
 */
/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "ara/com/types.h"
#include "gwm/composite_timermgr/timermgr_idt/impl_type_alrmclk_clntid_enum_idt.h"
#include "gwm/composite_timermgr/timermgr_idt/impl_type_alrmclk_hours_integer_idt.h"
#include "gwm/composite_timermgr/timermgr_idt/impl_type_alrmclk_minutes_integer_idt.h"
#include "gwm/composite_timermgr/timermgr_idt/impl_type_alrmclk_response_enum_idt.h"
#include "gwm/composite_timermgr/timermgr_idt/impl_type_alrmclk_seconds_integer_idt.h"
#include "gwm/composite_timermgr/timermgr_idt/impl_type_alrmclkbgnti_struct_idt.h"
#include "gwm/composite_timermgr/timermgr_idt/impl_type_alrmclkendti_struct_idt.h"
#include "gwm/composite_timermgr/timermgr_idt/impl_type_alrmclkfrq_enum_idt.h"
#include "gwm/composite_timermgr/timermgr_idt/impl_type_alrmclkwkeeve_enum_idt.h"
#include "gwm/composite_timermgr/timgrsrv_si/SI_TiMgrSrv.h"

#endif  // TIMGRSRVEXE_INCLUDE_GWM_COMPOSITE_TIMERMGR_TIMGRSRV_SI_SI_TIMGRSRV_COMMON_H_
