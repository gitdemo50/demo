/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/include/gwm/pt_powersys/powersys_idt/impl_type_obcchrggunst_struct_idt.h
 *        \brief
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_modelleddatatypes_api
 *         Commit ID: d9332226824a4a6c4616e2d948d41c862d192343
 *********************************************************************************************************************/

#ifndef DENPENDENCYSRVEXE_INCLUDE_GWM_PT_POWERSYS_POWERSYS_IDT_IMPL_TYPE_OBCCHRGGUNST_STRUCT_IDT_H_
#define DENPENDENCYSRVEXE_INCLUDE_GWM_PT_POWERSYS_POWERSYS_IDT_IMPL_TYPE_OBCCHRGGUNST_STRUCT_IDT_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include <array>
#include <cstdint>
#include <map>
#include <string>
#include <vector>
#include "gwm/pt_powersys/powersys_idt/impl_type_obcchrggunst_acinletsnsrst_enum_idt.h"
#include "gwm/pt_powersys/powersys_idt/impl_type_obcchrggunst_acinlettemperr_enum_idt.h"
#include "gwm/pt_powersys/powersys_idt/impl_type_obcchrggunst_cclinest_enum_idt.h"
#include "gwm/pt_powersys/powersys_idt/impl_type_obcchrggunst_chrgguncurrst_enum_idt.h"
#include "gwm/pt_powersys/powersys_idt/impl_type_obcchrggunst_connectst_enum_idt.h"
#include "gwm/pt_powersys/powersys_idt/impl_type_obcchrggunst_cplinest_enum_idt.h"
#include "gwm/pt_powersys/powersys_idt/impl_type_obcchrggunst_lockst_enum_idt.h"

namespace gwm {
namespace pt_powersys {
namespace powersys_idt {

// VECTOR Disable AutosarC++17_10-A12.6.1: MD_MDTG_A12.6.1_GeneratedStructUninitializedMembers
// VECTOR Disable AutosarC++17_10-M8.5.1: MD_MDTG_M8.5.1_GeneratedStructUninitializedMembers
/*!
 * \brief Type OBCChrgGunSt_Struct_Idt.
 * \remark generated
 * \trace SPEC-5951372
 *
 * IMPLEMENTATION-DATA-TYPE /DataTypes/ImplementationDataTypes/OBCChrgGunSt_Struct_Idt
 */
struct OBCChrgGunSt_Struct_Idt {
  using ConnectSt_Idt_generated_type = gwm::pt_powersys::powersys_idt::OBCChrgGunSt_ConnectSt_Enum_Idt;
  using ChrgGunCurrSt_Idt_generated_type = gwm::pt_powersys::powersys_idt::OBCChrgGunSt_ChrgGunCurrSt_Enum_Idt;
  using CPLineSt_Idt_generated_type = gwm::pt_powersys::powersys_idt::OBCChrgGunSt_CPLineSt_Enum_Idt;
  using CCLineSt_Idt_generated_type = gwm::pt_powersys::powersys_idt::OBCChrgGunSt_CCLineSt_Enum_Idt;
  using ACInletTempErr_Idt_generated_type = gwm::pt_powersys::powersys_idt::OBCChrgGunSt_ACInletTempErr_Enum_Idt;
  using LockSt_Idt_generated_type = gwm::pt_powersys::powersys_idt::OBCChrgGunSt_LockSt_Enum_Idt;
  using ACInletSnsrSt_Idt_generated_type = gwm::pt_powersys::powersys_idt::OBCChrgGunSt_ACInletSnsrSt_Enum_Idt;

  ConnectSt_Idt_generated_type ConnectSt_Idt;
  ChrgGunCurrSt_Idt_generated_type ChrgGunCurrSt_Idt;
  CPLineSt_Idt_generated_type CPLineSt_Idt;
  CCLineSt_Idt_generated_type CCLineSt_Idt;
  ACInletTempErr_Idt_generated_type ACInletTempErr_Idt;
  LockSt_Idt_generated_type LockSt_Idt;
  ACInletSnsrSt_Idt_generated_type ACInletSnsrSt_Idt;
};
// VECTOR Enable AutosarC++17_10-A12.6.1
// VECTOR Enable AutosarC++17_10-M8.5.1

/*!
 * \brief Compare for equality with another OBCChrgGunSt_Struct_Idt instance.
 */
inline bool operator==(OBCChrgGunSt_Struct_Idt const& l,
                       OBCChrgGunSt_Struct_Idt const& r) noexcept {
  return (&l == &r) || ((l.ConnectSt_Idt == r.ConnectSt_Idt)
                         && (l.ChrgGunCurrSt_Idt == r.ChrgGunCurrSt_Idt)
                         && (l.CPLineSt_Idt == r.CPLineSt_Idt)
                         && (l.CCLineSt_Idt == r.CCLineSt_Idt)
                         && (l.ACInletTempErr_Idt == r.ACInletTempErr_Idt)
                         && (l.LockSt_Idt == r.LockSt_Idt)
                         && (l.ACInletSnsrSt_Idt == r.ACInletSnsrSt_Idt)
  );
}

/*!
 * \brief Compare for inequality with another OBCChrgGunSt_Struct_Idt instance.
 */
inline bool operator!=(OBCChrgGunSt_Struct_Idt const& l,
                       OBCChrgGunSt_Struct_Idt const& r) noexcept {
  return !(l == r);
}

}  // namespace powersys_idt
}  // namespace pt_powersys
}  // namespace gwm

#endif  // DENPENDENCYSRVEXE_INCLUDE_GWM_PT_POWERSYS_POWERSYS_IDT_IMPL_TYPE_OBCCHRGGUNST_STRUCT_IDT_H_
