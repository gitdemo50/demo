#ifndef GWM_SKELETON_MIXIN_HPP
#define GWM_SKELETON_MIXIN_HPP

#include<memory>

#include "ara/com/types.h"

namespace gwm {
namespace skeleton {

template<typename T>
class SkeletonMixin : public T {
public:
    
    template<typename U>
    SkeletonMixin(U p) : T(p) {}

    SkeletonMixin(ara::core::InstanceSpecifier iSpec) : T(T::Preconstruct(ara::core::InstanceSpecifier(iSpec)).Value()) {}
    
    virtual ~SkeletonMixin() { }
    void start() {
        T::OfferService();
    }

    void stop() {
        T::StopOfferService();
    }

private:
};

}
}
#endif