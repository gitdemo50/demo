#ifndef GWM_SERVICES_TIMER_GS2S_PROXY_HPP
#define GWM_SERVICES_TIMER_GS2S_PROXY_HPP

#include "ara/com/runtime.h"
#include "ara/com/types.h"
#include "ara/core/future.h"
#include "ara/log/logging.h"
#include "vac/container/string_literals.h"
#include "gwm/platform/timer/gs2s/si_timersrv_gs2s_proxy.h"

#include "SensorProxy.hpp"

namespace gwm {
namespace services {
namespace timer {

namespace proxy {

using TimerS2SProxy = gwm::platform::timer::gs2s::proxy::SI_TimerSrv_GS2SProxy;

using vac::container::literals::operator""_sv;

class TimerS2SService : public SensorProxy<TimerS2SProxy> {
   
public:
    TimerS2SService();

    virtual ~TimerS2SService();

    void start() {
        SensorProxy::start(serviceSpec);
        logger.LogInfo() << "TimerS2SProxy Starting ";
    }

    MethodWrapper<gwm::platform::timer::gs2s::proxy::methods::TimerReq> timerReq {&proxy->TimerReq};
    EventWrapper<gwm::platform::timer::gs2s::proxy::fields::FieldNotifierTimerSt> ev_timerSt {*this, &proxy->TimerSt};

protected:
    void doSubscription() override;
    void initProxyItems() override;

    ara::core::StringView serviceSpec;
};

}
}
}
}

#endif