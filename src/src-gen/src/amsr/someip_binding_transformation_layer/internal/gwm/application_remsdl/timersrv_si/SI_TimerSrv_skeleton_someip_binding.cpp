// VECTOR Same Line AutosarC++17_10-A1.1.1: MD_SOMEIPBINDING_AutosarC++17_10-A1.1.1_external_identifiers
/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  TimerSrvExe/src/amsr/someip_binding_transformation_layer/internal/gwm/application_remsdl/timersrv_si/SI_TimerSrv_skeleton_someip_binding.cpp
 *        \brief  SOME/IP skeleton binding of service 'SI_TimerSrv'
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_someipbinding
 *         Commit ID: 43b33f0283d4d61385b98a3599688499abd86a75
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "amsr/someip_binding_transformation_layer/internal/gwm/application_remsdl/timersrv_si/SI_TimerSrv_skeleton_someip_binding.h"
#include <utility>

namespace amsr {
namespace someip_binding_transformation_layer {
namespace internal {

namespace gwm {
namespace application_remsdl {
namespace timersrv_si {

/*!
 * \brief Generated SOME/IP related service ID.
 */
::amsr::someip_protocol::internal::ServiceId constexpr SI_TimerSrvSkeletonSomeIpBinding::kServiceId;

/*!
 * \brief SOME/IP major version of this service interface deployment.
 */
constexpr ::amsr::someip_protocol::internal::MajorVersion SI_TimerSrvSkeletonSomeIpBinding::kMajorVersion;

SI_TimerSrvSkeletonSomeIpBinding::SI_TimerSrvSkeletonSomeIpBinding(
    ::amsr::someip_protocol::internal::InstanceId const instance_id,
    ::amsr::someip_binding::internal::ServerManagerInterface& someip_binding_server_manager,
    ::gwm::application_remsdl::timersrv_si::skeleton::SI_TimerSrvSkeleton& skeleton)
    : instance_id_(instance_id),
      someip_binding_server_manager_(someip_binding_server_manager),
      skeleton_(skeleton),
      methods_TimerCncl_(*this, "TimerCncl" ),
      methods_TimerReq_(*this, "TimerReq" ) ,
      field_notifier_TimerSt_(instance_id),
      field_manager_TimerSt_get_(*this, "TimerSt" ){
}

void SI_TimerSrvSkeletonSomeIpBinding::HandleMethodRequest(
  ::amsr::someip_protocol::internal::SomeIpMessageHeader const& header,
  ::amsr::someip_protocol::internal::SomeIpMessage  packet) {
  // Based on the method id -> static dispatching to the method request/response manager
  switch (header.method_id_) {
    case methods::SkeletonTimerCncl::kMethodId: {
      methods_TimerCncl_.HandleMethodRequest(header, std::move(packet));
      break;
    }
    case methods::SkeletonTimerReq::kMethodId: {
      methods_TimerReq_.HandleMethodRequest(header, std::move(packet));
      break;
    }
    case fields::SkeletonTimerStGet::kMethodId: {
      field_manager_TimerSt_get_.HandleMethodRequest(header, std::move(packet));
      break;
    }
    default: {
      // Method implementation is missing.
      SendErrorResponse<static_cast<::amsr::someip_protocol::internal::ReturnCode>(::amsr::someip_protocol::internal::SomeIpReturnCode::kUnknownMethod)>(header);
      break;
    }
  }
}

void SI_TimerSrvSkeletonSomeIpBinding::SendMethodResponse(::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet) {
  someip_binding_server_manager_.SendMethodResponse(instance_id_, std::move(packet));
}

// ---- Events -----------------------------------------------------------------------------------------------------

// ---- Fields -----------------------------------------------------------------------------------------------------

// Field 'TimerSt'
SI_TimerSrvSkeletonSomeIpFieldNotifierTimerSt* SI_TimerSrvSkeletonSomeIpBinding::GetFieldNotifierTimerSt() noexcept {
  return &field_notifier_TimerSt_;
}

}  // namespace internal
}  // namespace someip_binding_transformation_layer
}  // namespace amsr
}  // namespace timersrv_si
}  // namespace application_remsdl
}  // namespace gwm

