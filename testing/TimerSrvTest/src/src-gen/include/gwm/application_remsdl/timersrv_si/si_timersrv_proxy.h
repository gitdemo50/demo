/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  TiMgrSrvExe/include/gwm/application_remsdl/timersrv_si/si_timersrv_proxy.h
 *        \brief  Proxy for service 'SI_TimerSrv'.
 *
 *      \details  This Service interface provides timer
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_socal
 *         Commit ID: 209b3c0234013e84e3170ee826c9a8c6022de194
 *********************************************************************************************************************/

#ifndef TIMGRSRVEXE_INCLUDE_GWM_APPLICATION_REMSDL_TIMERSRV_SI_SI_TIMERSRV_PROXY_H_
#define TIMGRSRVEXE_INCLUDE_GWM_APPLICATION_REMSDL_TIMERSRV_SI_SI_TIMERSRV_PROXY_H_

/*!
 * \trace SPEC-4980240
 */
/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "amsr/socal/events/proxy_event.h"
#include "amsr/socal/fields/proxy_field.h"
#include "amsr/socal/internal/events/proxy_event_backend_interface.h"
#include "amsr/socal/internal/logging/ara_com_logger.h"
#include "amsr/socal/methods/proxy_method.h"
#include "amsr/socal/proxy.h"
#include "gwm/application_remsdl/timersrv_si/SI_TimerSrv_proxy_backend_interface.h"
#include "gwm/application_remsdl/timersrv_si/SI_TimerSrv_proxy_update_manager.h"
#include "gwm/application_remsdl/timersrv_si/SI_TimerSrv_types.h"

/*!
 * \trace SPEC-4980240
 */
namespace gwm {
namespace application_remsdl {
namespace timersrv_si {

/*!
 * \trace SPEC-4980242
 */
namespace proxy {

/*!
 * \trace SPEC-4980244
 */
namespace methods {


/*!
 * \brief Type alias for service method 'TimerCncl', that is part of the proxy.
 *
 * \trace SPEC-4980346
 */
using TimerCncl = ::amsr::socal::methods::MethodParameters<void>::
        ProxyMethod<gwm::application_remsdl::timersrv_si::internal::SI_TimerSrvProxyBackendInterface,
                    gwm::application_remsdl::timersrv_si::internal::methods::TimerCncl,
                    &gwm::application_remsdl::timersrv_si::internal::SI_TimerSrvProxyBackendInterface::HandleMethodTimerCncl>;


/*!
 * \brief Type alias for service method 'TimerReq', that is part of the proxy.
 *
 * \trace SPEC-4980346
 */
using TimerReq = ::amsr::socal::methods::MethodParameters<
    ::gwm::platform_timer::timer_idt::TimerSet_Integer_Idt const&>::
        ProxyMethod<gwm::application_remsdl::timersrv_si::internal::SI_TimerSrvProxyBackendInterface,
                    gwm::application_remsdl::timersrv_si::internal::methods::TimerReq,
                    &gwm::application_remsdl::timersrv_si::internal::SI_TimerSrvProxyBackendInterface::HandleMethodTimerReq>;

}  // namespace methods

/*!
 * \trace SPEC-4980243
 */
namespace events {

}  // namespace events

/*!
 * \trace SPEC-4980245
 */
namespace fields {
// ---- Type aliases for field 'TimerSt' ----

/*!
 * \brief Type alias for the notifier of service field 'TimerSt'.
 */
using FieldNotifierTimerSt =
    ::amsr::socal::events::ProxyEvent<::gwm::platform_timer::timer_idt::TimerSt_Struct_Idt>;
/*!
 * \brief Type alias for the notifier configuration of field 'TimerSt'.
 */
using FieldNotifierConfigTimerSt = ::amsr::socal::internal::fields::ProxyFieldParams::HasNotifier<true, FieldNotifierTimerSt>;
/*!
 * \brief Type alias for the getter method of service field 'TimerSt'.
 */
using FieldMethodGetTimerSt = ::amsr::socal::methods::MethodParameters<void>::ProxyMethod<  
  gwm::application_remsdl::timersrv_si::internal::SI_TimerSrvProxyBackendInterface,
  gwm::application_remsdl::timersrv_si::internal::fields::TimerSt,
  &gwm::application_remsdl::timersrv_si::internal::SI_TimerSrvProxyBackendInterface::HandleFieldTimerStMethodGet>;

/*!
 * \brief Type alias for the getter configuration of field 'TimerSt'.
 */
using FieldGetterConfigTimerSt = ::amsr::socal::internal::fields::ProxyFieldParams::HasGetter<true, FieldMethodGetTimerSt>;

/*!
 * \brief Type alias for the setter configuration of field 'TimerSt'.
 */
using FieldSetterConfigTimerSt = ::amsr::socal::internal::fields::ProxyFieldParams::HasSetter<false>;

/*!
 * \brief Field name.
 */
struct TimerStName {
  /*!
   * \brief Field name string.
   */
  static constexpr vac::container::CStringView Name{"TimerSt"_sv};
};

/*!
 * \brief Type alias for the parameterization of field 'TimerSt'.
 */
using FieldConfigTimerSt = ::amsr::socal::internal::fields::ProxyFieldConfig<gwm::application_remsdl::timersrv_si::internal::SI_TimerSrvProxyBackendInterface,
                               FieldNotifierConfigTimerSt, FieldGetterConfigTimerSt, FieldSetterConfigTimerSt, TimerStName,
                               ::gwm::platform_timer::timer_idt::TimerSt_Struct_Idt>;

/*!
 * \brief Type alias for the field 'TimerSt'.
 *
 * \trace SPEC-5951150
 */
using TimerSt =
    ::amsr::socal::fields::ProxyField<FieldConfigTimerSt>;

}  // namespace fields

/*!
 * \brief Proxy class for the service 'SI_TimerSrv'.
 *
 * \details This Service interface provides timer
 * \vpublic
 *
 * \trace SPEC-4980344
 */
class SI_TimerSrvProxy : public ::amsr::socal::Proxy<gwm::application_remsdl::timersrv_si::SI_TimerSrv, gwm::application_remsdl::timersrv_si::internal::SI_TimerSrvProxyBackendInterface, gwm::application_remsdl::timersrv_si::internal::SI_TimerSrvHandleType> {
 public:

  /*!
   * \brief Type alias for the ProxyBackendInterface.
   */
  using ProxyBackendInterface = gwm::application_remsdl::timersrv_si::internal::SI_TimerSrvProxyBackendInterface;

  // ==================== Constructors / Destructors (start) ====================

  /*!
  * \brief Exception-less pre-construction of SI_TimerSrvProxy.
  *
  * \param[in] handle The identification of the service the proxy should represent. Handles are generated by synchronous
  * SI_TimerSrvProxy::FindService or asynchronous SI_TimerSrvProxy::StartFindService.
  * The handle object passed as a parameter shall be a handle returned by a FindService call for this specific service.
  * Passing handles from different services is currently possible, but is detected. An exception is raised as documented below.
  *
  * \return Result<ConstructionToken> Result containing construction token from which a proxy object can be
  *                                   constructed.
  * \pre "Runtime" must be initialized.
  * \pre The instance identifier of the \p handle must be known.
  * \pre The proxy backend must be spawned.
  * \pre The provided \p handle must match the handle of the service.
  *
  * \context App
  * \threadsafe FALSE
  * \reentrant FALSE
  * \vpublic
  * \synchronous TRUE
  * \trace SPEC-8053550
  * \trace SPEC-8053560
  */
  static ConstructionResult Preconstruct(HandleType const& handle) noexcept;

  /*!
  * \brief Exception-less constructor of SI_TimerSrvProxy.
  * \details Handles exception-less construction of a proxy based on the availability of
  *          a valid token.
  *
  * \param[in] token ConstructionToken created with "Preconstruct()" API.
  *
  * \pre No other service proxy for the same instance identifier must exist concurrently. After destruction of an
  *      already created proxy object, the instantation for the same service instance will be possible.
  * \pre IAM access must be granted.
  * \context App
  * \threadsafe FALSE
  * \reentrant FALSE
  * \vpublic
  * \synchronous TRUE
  * \trace SPEC-8053550
  */
  explicit SI_TimerSrvProxy(ConstructionToken&& token) noexcept;

  /*!
  * \brief Constructor of SI_TimerSrvProxy. Constructor takes a handle returned by
  * SI_TimerSrvProxy::FindService() method or provided as parameter to the callback handler of
  * SI_TimerSrvProxy::StartFindService().
  *
  * \details Because of internal resource management strategy, all created proxies shall be released before the Runtime
  * is destroyed; i.e. they cannot not be stored in variables with longer life period than the application's main().
  * If not followed, it's not guaranteed that the communication middleware is shut down properly and may lead to
  * segmentation fault.
  *
  * \param[in] handle The identification of the service the proxy should represent. Handles are generated by synchronous
  * SI_TimerSrvProxy::FindService or asynchronous SI_TimerSrvProxy::StartFindService.
  * The handle object passed as a parameter shall be a handle returned by a FindService call for this specific service.
  * Passing handles from different services is currently possible, but is detected. An exception is raised as documented below.
  *
  * \pre "Runtime" must be initialized.
  * \pre The instance identifier of the \p handle must be known.
  * \pre The proxy backend must be spawned.
  * \pre The provided \p handle must match the handle of the service.
  * \pre No other service proxy for the same instance identifier must exist concurrently. After destruction of an
  *      already created proxy object, the instantation for the same service instance will be possible.
  *
  * \pre IAM access must be granted.
  * \context App
  * \threadsafe FALSE
  * \reentrant FALSE
  * \vprivate Vector component internal API.
  * \synchronous TRUE
  * \trace SPEC-4980371
  */
  explicit SI_TimerSrvProxy(HandleType const& handle) noexcept;

  /*!
   * \brief Perform cleanup of proxy instance.
   *
   * \details Unsubscribes all event and field notifications and unsets event receive and subscription state change
   * handlers. Finally, unregisters the proxy from the communication middleware.
   * This call will be blocked until all current events/methods' callbacks are finished/canceled.
   *
   * \pre -
   * \context     App
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \vpublic
   * \synchronous TRUE
   */
  ~SI_TimerSrvProxy() noexcept override;

  // ===================== Constructors / Destructors (end) =====================

 private:
  /*!
   * \brief Type alias for the base class.
   */
  using Base = ::amsr::socal::Proxy<gwm::application_remsdl::timersrv_si::SI_TimerSrv, ProxyBackendInterface, gwm::application_remsdl::timersrv_si::internal::SI_TimerSrvHandleType>;

  /*!
   * \brief Logger for tracing and debugging
   */
  amsr::socal::internal::logging::AraComLogger logger_;
  
  /*!
   * \brief The proxy update manager as proxy backend.
   */
  gwm::application_remsdl::timersrv_si::internal::SI_TimerSrvProxyUpdateManager proxy_update_manager_;

 public:

  /*!
   * \brief       Poll the current state of the service backend.
   *
   * \return      A ServiceState indicating if the service backend is up, down or restarted.
   * \pre         -
   * \context     App | Callback
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \vpublic
   * \synchronous TRUE
   */
  ::amsr::socal::ServiceState ReadServiceState() noexcept; 

  // ---- Methods --------------------------------------------------------------------------------------------------

    // VECTOR NC VectorC++-V11.0.2: MD_SOCAL_V11-0-2_public_member_in_generated_code
    /*!
     * \brief Call of service method 'TimerCncl'.
     * \vpublic
     */
  methods::TimerCncl TimerCncl;

    // VECTOR NC VectorC++-V11.0.2: MD_SOCAL_V11-0-2_public_member_in_generated_code
    /*!
     * \brief Call of service method 'TimerReq'.
     * \vpublic
     */
  methods::TimerReq TimerReq;

  // ---- Events ---------------------------------------------------------------------------------------------------

  // ---- Fields ---------------------------------------------------------------------------------------------------

  // VECTOR NC VectorC++-V11.0.2: MD_SOCAL_V11-0-2_public_member_in_generated_code
  /*!
   * \brief Field 'TimerSt' which can be used by application developer.
   * \details
   * Data of type ::gwm::platform_timer::timer_idt::TimerSt_Struct_Idt 
   * \vpublic
   */
  fields::TimerSt TimerSt;
};

}  // namespace proxy
}  // namespace timersrv_si
}  // namespace application_remsdl
}  // namespace gwm

#endif  // TIMGRSRVEXE_INCLUDE_GWM_APPLICATION_REMSDL_TIMERSRV_SI_SI_TIMERSRV_PROXY_H_

