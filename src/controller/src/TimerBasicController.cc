
#include "TimerBasicController.hpp"

namespace gwm {
namespace controller {

TimerBasicController::TimerBasicController(std::shared_ptr<TimerS2SService> service) : _timerS2S(service), _notifier(nullptr),
    _logger(gwm::platform::Logger("TimerBasicController")) {
          
    _timerS2S->ev_timerSt.setEventHandler([this](datatypes::gs2s::timersrv_gs2s::TimerSt_Struct &v) {

        _logger.LogInfo() << "received timerS2S event, "
                             "WakeSrcexpired is " << static_cast<int> (v.WakeSrcexpired) << 
                             ", WakeSource is " << static_cast<int> (v.WakeSource);
                             
        if(_notifier) {
            if(v.WakeSrcexpired == datatypes::gs2s::timersrv_gs2s::TimerSt_WakeSrcexpired_Enum::WakeupSourceExpired) {

                _notifier(TimerState::TimerTrigger);
            } else if(v.WakeSrcexpired == datatypes::gs2s::timersrv_gs2s::TimerSt_WakeSrcexpired_Enum::NoWakeupSourceExpired){

                _notifier(TimerState::Timing);
            }else{

                _notifier(TimerState::TimerDefault);
            }
        } 
    });
}

int TimerBasicController::requestTimer(int time) {

    if (!_timerS2S->isServiceAvailable()) {

        _logger.LogInfo() << "_timerS2S not found.";

        return 1;
    }

    auto ret = _timerS2S->timerReq(time, datatypes::gs2s::timersrv_gs2s::StartPoint_Enum::CountFromTriggered,
                                   datatypes::gs2s::timersrv_gs2s::TimerType_Enum::DurationTime,
                                   datatypes::gs2s::timersrv_gs2s::SleepType_Enum::NormalSleep);
                  
    try {
        return transformResult(ret.get().Response);
    }
    catch (const std::exception &ex) {

       _logger.LogInfo() <<"timerReq get response failed.";
    }

    return 1;
}

int TimerBasicController::cancelActiveTimer() {
    
    const int CANCEL_TIME_VALUE = 0xFFFFFFFF;
    return requestTimer(CANCEL_TIME_VALUE);
}
          
void TimerBasicController::setTimerNotifier(NotifierFn notifier) {
    _notifier = notifier;
}

int TimerBasicController::transformResult(::datatypes::gs2s::timersrv_gs2s::TimerSet_Response_Enum res) {
    if(res == datatypes::gs2s::timersrv_gs2s::TimerSet_Response_Enum::OK) {
        return 0;
    }
    return 1;
}

}
}