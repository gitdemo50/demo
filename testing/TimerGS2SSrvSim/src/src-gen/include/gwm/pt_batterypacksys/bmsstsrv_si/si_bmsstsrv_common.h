/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/include/gwm/pt_batterypacksys/bmsstsrv_si/si_bmsstsrv_common.h
 *        \brief  Header for service 'SI_BmsStSrv'.
 *
 *      \details  The service interface provides HV battery real time current information
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_socal
 *         Commit ID: 209b3c0234013e84e3170ee826c9a8c6022de194
 *********************************************************************************************************************/

#ifndef DENPENDENCYSRVEXE_INCLUDE_GWM_PT_BATTERYPACKSYS_BMSSTSRV_SI_SI_BMSSTSRV_COMMON_H_
#define DENPENDENCYSRVEXE_INCLUDE_GWM_PT_BATTERYPACKSYS_BMSSTSRV_SI_SI_BMSSTSRV_COMMON_H_

/*!
 * \trace SPEC-4980247, SPEC-4980248, SPEC-5951130, SPEC-4980251
 */
/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "ara/com/types.h"
#include "gwm/pt_batterypacksys/batterypacksys_idt/impl_type_bmschrgst_enum_idt.h"
#include "gwm/pt_batterypacksys/batterypacksys_idt/impl_type_bmschrgti_integer_idt.h"
#include "gwm/pt_batterypacksys/batterypacksys_idt/impl_type_bmscondchrmaxpwr_float_idt.h"
#include "gwm/pt_batterypacksys/batterypacksys_idt/impl_type_bmsconfbmaxpwr_float_idt.h"
#include "gwm/pt_batterypacksys/batterypacksys_idt/impl_type_bmsdcchrgcnctst_enum_idt.h"
#include "gwm/pt_batterypacksys/batterypacksys_idt/impl_type_bmsdcchrgendreas_enum_idt.h"
#include "gwm/pt_batterypacksys/batterypacksys_idt/impl_type_bmsdchrgprmsst_enum_idt.h"
#include "gwm/pt_batterypacksys/batterypacksys_idt/impl_type_bmserrst_struct_idt.h"
#include "gwm/pt_batterypacksys/batterypacksys_idt/impl_type_bmsheatrunawayst_enum_idt.h"
#include "gwm/pt_batterypacksys/batterypacksys_idt/impl_type_bmshvilst_enum_idt.h"
#include "gwm/pt_batterypacksys/batterypacksys_idt/impl_type_bmsinletcooltt_integer_idt.h"
#include "gwm/pt_batterypacksys/batterypacksys_idt/impl_type_bmsinnersocst_struct_idt.h"
#include "gwm/pt_batterypacksys/batterypacksys_idt/impl_type_bmsinteltempmgtst_enum_idt.h"
#include "gwm/pt_batterypacksys/batterypacksys_idt/impl_type_bmsisomeasst_enum_idt.h"
#include "gwm/pt_batterypacksys/batterypacksys_idt/impl_type_bmsisor_integer_idt.h"
#include "gwm/pt_batterypacksys/batterypacksys_idt/impl_type_bmslowtempst_struct_idt.h"
#include "gwm/pt_batterypacksys/batterypacksys_idt/impl_type_bmsoutletcooltt_integer_idt.h"
#include "gwm/pt_batterypacksys/batterypacksys_idt/impl_type_bmsplsdchrmaxpwr_float_idt.h"
#include "gwm/pt_batterypacksys/batterypacksys_idt/impl_type_bmsplsfbmaxpwr_float_idt.h"
#include "gwm/pt_batterypacksys/batterypacksys_idt/impl_type_bmsrmtpreheatst_enum_idt.h"
#include "gwm/pt_batterypacksys/batterypacksys_idt/impl_type_bmssafcnctst_enum_idt.h"
#include "gwm/pt_batterypacksys/batterypacksys_idt/impl_type_bmsst_enum_idt.h"
#include "gwm/pt_batterypacksys/batterypacksys_idt/impl_type_cellmaxt_struct_idt.h"
#include "gwm/pt_batterypacksys/batterypacksys_idt/impl_type_cellmaxu_struct_idt.h"
#include "gwm/pt_batterypacksys/batterypacksys_idt/impl_type_cellmint_struct_idt.h"
#include "gwm/pt_batterypacksys/batterypacksys_idt/impl_type_cellminu_struct_idt.h"
#include "gwm/pt_batterypacksys/batterypacksys_idt/impl_type_hvbattdispysoc_float_idt.h"
#include "gwm/pt_batterypacksys/batterypacksys_idt/impl_type_hvbatti_float_idt.h"
#include "gwm/pt_batterypacksys/batterypacksys_idt/impl_type_hvbattinnersoc_float_idt.h"
#include "gwm/pt_batterypacksys/batterypacksys_idt/impl_type_hvbattsoclim_enum_idt.h"
#include "gwm/pt_batterypacksys/batterypacksys_idt/impl_type_hvbattsoh_float_idt.h"
#include "gwm/pt_batterypacksys/batterypacksys_idt/impl_type_hvbatttotegy_float_idt.h"
#include "gwm/pt_batterypacksys/batterypacksys_idt/impl_type_hvbattu_float_idt.h"
#include "gwm/pt_batterypacksys/batterypacksys_idt/impl_type_hvmaiswtst_struct_idt.h"
#include "gwm/pt_batterypacksys/batterypacksys_idt/impl_type_modulavrgt_integer_idt.h"
#include "gwm/pt_batterypacksys/bmsstsrv_si/SI_BmsStSrv.h"

#endif  // DENPENDENCYSRVEXE_INCLUDE_GWM_PT_BATTERYPACKSYS_BMSSTSRV_SI_SI_BMSSTSRV_COMMON_H_
