/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/src/gwm/pt_batterypacksys/bmsstsrv_si/si_bmsstsrv_skeleton.cpp
 *        \brief  Skeleton for service 'SI_BmsStSrv'.
 *
 *      \details  The service interface provides HV battery real time current information
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_socal
 *         Commit ID: 209b3c0234013e84e3170ee826c9a8c6022de194
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "gwm/pt_batterypacksys/bmsstsrv_si/si_bmsstsrv_skeleton.h"
#include "amsr/socal/internal/instance_specifier_lookup_table.h"

/*!
 * \trace SPEC-4980240
 * \trace SPEC-4980241
 */
namespace gwm {
namespace pt_batterypacksys {
namespace bmsstsrv_si {
namespace skeleton {

/*!
 * \brief Static instance of service discovery.
 */
// VECTOR NC AutosarC++17_10-A3.3.2: MD_SOCAL_AutosarC++17_10-A3.3.2_StaticStorageDurationOfNonPODType
::amsr::generic::Singleton<SI_BmsStSrvSkeleton::ServiceDiscovery*> SI_BmsStSrvSkeleton::sd_;

SI_BmsStSrvSkeleton::ConstructionResult SI_BmsStSrvSkeleton::Preconstruct(
    ara::com::InstanceIdentifier instance_id, ara::com::MethodCallProcessingMode mode) noexcept {
return Base::Preconstruct(instance_id, mode);
}

SI_BmsStSrvSkeleton::ConstructionResult SI_BmsStSrvSkeleton::Preconstruct(
    ara::core::InstanceSpecifier instance, ara::com::MethodCallProcessingMode mode) noexcept {
return Base::Preconstruct(instance, mode);
}

SI_BmsStSrvSkeleton::ConstructionResult SI_BmsStSrvSkeleton::Preconstruct(
    ara::com::InstanceIdentifierContainer instance_identifiers, ara::com::MethodCallProcessingMode mode) noexcept {
return Base::Preconstruct(instance_identifiers, mode);
}

SI_BmsStSrvSkeleton::SI_BmsStSrvSkeleton(ConstructionToken&& token) noexcept
: Base{std::move(token)}
, BMSChrgTi(this)
, BMSConDchrMaxPwr(this)
, BMSConFBMaxPwr(this)
, BMSPlsDchrMaxPwr(this)
, BMSPlsFBMaxPwr(this)
, BmsInletCooltT(this)
, BmsIsoR(this)
, BmsOutletCooltT(this)
, CellMaxT(this)
, CellMaxU(this)
, CellMinT(this)
, CellMinU(this)
, HVBattDispySoc(this)
, HVBattI(this)
, HVBattInnerSoc(this)
, HVBattSoh(this)
, HVBattTotEgy(this)
, HVBattU(this)
, ModulAvrgT(this)
, BMSChrgSt(this)
, BMSDCChrgCnctSt(this)
, BMSDCChrgEndReas(this)
, BMSDchrgPrmsSt(this)
, BMSHeatRunawaySt(this)
, BMSInnerSOCSt(this)
, BMSIntelTempMgtSt(this)
, BMSLowTempSt(this)
, BMSRmtPreHeatSt(this)
, BMSSafCnctSt(this)
, BmsErrSt(this)
, BmsHVILSt(this)
, BmsIsoMeasSt(this)
, BmsSt(this)
, HVBattSOCLim(this)
, HVMaiSwtSt(this)
 {}

SI_BmsStSrvSkeleton::SI_BmsStSrvSkeleton(ara::com::InstanceIdentifier instance,
                                               ara::com::MethodCallProcessingMode mode) noexcept
     : SI_BmsStSrvSkeleton{Preconstruct(instance, mode).Value()} {}

SI_BmsStSrvSkeleton::SI_BmsStSrvSkeleton(ara::core::InstanceSpecifier instance,
                                               ara::com::MethodCallProcessingMode mode) noexcept
     : SI_BmsStSrvSkeleton{Preconstruct(instance, mode).Value()} {}

SI_BmsStSrvSkeleton::SI_BmsStSrvSkeleton(ara::com::InstanceIdentifierContainer instance_identifiers,
                                               ara::com::MethodCallProcessingMode mode) noexcept
     : SI_BmsStSrvSkeleton{Preconstruct(instance_identifiers, mode).Value()} {}

SI_BmsStSrvSkeleton::~SI_BmsStSrvSkeleton() noexcept {
  // Next line might block until all running method requests are done.
  StopOfferService();
}

void SI_BmsStSrvSkeleton::DoFieldInitializationChecks() noexcept {

    if (!BMSChrgSt.IsUpdated()) {
        logger_.LogError([this](ara::log::LogStream& s) {
                          s << "Failed to offer service 'SI_BmsStSrv' with instance id(s) '";
                          for (::amsr::socal::internal::InstanceSpecifierLookupTableEntry const& entry : offered_instances_) {
                              ara::core::StringView const instance_id_str{entry.GetInstanceIdentifier().ToString()};
                              s << " " << instance_id_str;
                          }
                          s << "' - No initial value has been set for field 'BMSChrgSt' (BMSChrgSt::Update(...) has never been called)."; },
                          __func__, __LINE__);
        ara::core::Abort("No initial value has been set for field 'BMSChrgSt' (BMSChrgSt::Update(...) has never been called).");
    }

    if (!BMSDCChrgCnctSt.IsUpdated()) {
        logger_.LogError([this](ara::log::LogStream& s) {
                          s << "Failed to offer service 'SI_BmsStSrv' with instance id(s) '";
                          for (::amsr::socal::internal::InstanceSpecifierLookupTableEntry const& entry : offered_instances_) {
                              ara::core::StringView const instance_id_str{entry.GetInstanceIdentifier().ToString()};
                              s << " " << instance_id_str;
                          }
                          s << "' - No initial value has been set for field 'BMSDCChrgCnctSt' (BMSDCChrgCnctSt::Update(...) has never been called)."; },
                          __func__, __LINE__);
        ara::core::Abort("No initial value has been set for field 'BMSDCChrgCnctSt' (BMSDCChrgCnctSt::Update(...) has never been called).");
    }

    if (!BMSDCChrgEndReas.IsUpdated()) {
        logger_.LogError([this](ara::log::LogStream& s) {
                          s << "Failed to offer service 'SI_BmsStSrv' with instance id(s) '";
                          for (::amsr::socal::internal::InstanceSpecifierLookupTableEntry const& entry : offered_instances_) {
                              ara::core::StringView const instance_id_str{entry.GetInstanceIdentifier().ToString()};
                              s << " " << instance_id_str;
                          }
                          s << "' - No initial value has been set for field 'BMSDCChrgEndReas' (BMSDCChrgEndReas::Update(...) has never been called)."; },
                          __func__, __LINE__);
        ara::core::Abort("No initial value has been set for field 'BMSDCChrgEndReas' (BMSDCChrgEndReas::Update(...) has never been called).");
    }

    if (!BMSDchrgPrmsSt.IsUpdated()) {
        logger_.LogError([this](ara::log::LogStream& s) {
                          s << "Failed to offer service 'SI_BmsStSrv' with instance id(s) '";
                          for (::amsr::socal::internal::InstanceSpecifierLookupTableEntry const& entry : offered_instances_) {
                              ara::core::StringView const instance_id_str{entry.GetInstanceIdentifier().ToString()};
                              s << " " << instance_id_str;
                          }
                          s << "' - No initial value has been set for field 'BMSDchrgPrmsSt' (BMSDchrgPrmsSt::Update(...) has never been called)."; },
                          __func__, __LINE__);
        ara::core::Abort("No initial value has been set for field 'BMSDchrgPrmsSt' (BMSDchrgPrmsSt::Update(...) has never been called).");
    }

    if (!BMSHeatRunawaySt.IsUpdated()) {
        logger_.LogError([this](ara::log::LogStream& s) {
                          s << "Failed to offer service 'SI_BmsStSrv' with instance id(s) '";
                          for (::amsr::socal::internal::InstanceSpecifierLookupTableEntry const& entry : offered_instances_) {
                              ara::core::StringView const instance_id_str{entry.GetInstanceIdentifier().ToString()};
                              s << " " << instance_id_str;
                          }
                          s << "' - No initial value has been set for field 'BMSHeatRunawaySt' (BMSHeatRunawaySt::Update(...) has never been called)."; },
                          __func__, __LINE__);
        ara::core::Abort("No initial value has been set for field 'BMSHeatRunawaySt' (BMSHeatRunawaySt::Update(...) has never been called).");
    }

    if (!BMSInnerSOCSt.IsUpdated()) {
        logger_.LogError([this](ara::log::LogStream& s) {
                          s << "Failed to offer service 'SI_BmsStSrv' with instance id(s) '";
                          for (::amsr::socal::internal::InstanceSpecifierLookupTableEntry const& entry : offered_instances_) {
                              ara::core::StringView const instance_id_str{entry.GetInstanceIdentifier().ToString()};
                              s << " " << instance_id_str;
                          }
                          s << "' - No initial value has been set for field 'BMSInnerSOCSt' (BMSInnerSOCSt::Update(...) has never been called)."; },
                          __func__, __LINE__);
        ara::core::Abort("No initial value has been set for field 'BMSInnerSOCSt' (BMSInnerSOCSt::Update(...) has never been called).");
    }

    if (!BMSIntelTempMgtSt.IsUpdated()) {
        logger_.LogError([this](ara::log::LogStream& s) {
                          s << "Failed to offer service 'SI_BmsStSrv' with instance id(s) '";
                          for (::amsr::socal::internal::InstanceSpecifierLookupTableEntry const& entry : offered_instances_) {
                              ara::core::StringView const instance_id_str{entry.GetInstanceIdentifier().ToString()};
                              s << " " << instance_id_str;
                          }
                          s << "' - No initial value has been set for field 'BMSIntelTempMgtSt' (BMSIntelTempMgtSt::Update(...) has never been called)."; },
                          __func__, __LINE__);
        ara::core::Abort("No initial value has been set for field 'BMSIntelTempMgtSt' (BMSIntelTempMgtSt::Update(...) has never been called).");
    }

    if (!BMSLowTempSt.IsUpdated()) {
        logger_.LogError([this](ara::log::LogStream& s) {
                          s << "Failed to offer service 'SI_BmsStSrv' with instance id(s) '";
                          for (::amsr::socal::internal::InstanceSpecifierLookupTableEntry const& entry : offered_instances_) {
                              ara::core::StringView const instance_id_str{entry.GetInstanceIdentifier().ToString()};
                              s << " " << instance_id_str;
                          }
                          s << "' - No initial value has been set for field 'BMSLowTempSt' (BMSLowTempSt::Update(...) has never been called)."; },
                          __func__, __LINE__);
        ara::core::Abort("No initial value has been set for field 'BMSLowTempSt' (BMSLowTempSt::Update(...) has never been called).");
    }

    if (!BMSRmtPreHeatSt.IsUpdated()) {
        logger_.LogError([this](ara::log::LogStream& s) {
                          s << "Failed to offer service 'SI_BmsStSrv' with instance id(s) '";
                          for (::amsr::socal::internal::InstanceSpecifierLookupTableEntry const& entry : offered_instances_) {
                              ara::core::StringView const instance_id_str{entry.GetInstanceIdentifier().ToString()};
                              s << " " << instance_id_str;
                          }
                          s << "' - No initial value has been set for field 'BMSRmtPreHeatSt' (BMSRmtPreHeatSt::Update(...) has never been called)."; },
                          __func__, __LINE__);
        ara::core::Abort("No initial value has been set for field 'BMSRmtPreHeatSt' (BMSRmtPreHeatSt::Update(...) has never been called).");
    }

    if (!BMSSafCnctSt.IsUpdated()) {
        logger_.LogError([this](ara::log::LogStream& s) {
                          s << "Failed to offer service 'SI_BmsStSrv' with instance id(s) '";
                          for (::amsr::socal::internal::InstanceSpecifierLookupTableEntry const& entry : offered_instances_) {
                              ara::core::StringView const instance_id_str{entry.GetInstanceIdentifier().ToString()};
                              s << " " << instance_id_str;
                          }
                          s << "' - No initial value has been set for field 'BMSSafCnctSt' (BMSSafCnctSt::Update(...) has never been called)."; },
                          __func__, __LINE__);
        ara::core::Abort("No initial value has been set for field 'BMSSafCnctSt' (BMSSafCnctSt::Update(...) has never been called).");
    }

    if (!BmsErrSt.IsUpdated()) {
        logger_.LogError([this](ara::log::LogStream& s) {
                          s << "Failed to offer service 'SI_BmsStSrv' with instance id(s) '";
                          for (::amsr::socal::internal::InstanceSpecifierLookupTableEntry const& entry : offered_instances_) {
                              ara::core::StringView const instance_id_str{entry.GetInstanceIdentifier().ToString()};
                              s << " " << instance_id_str;
                          }
                          s << "' - No initial value has been set for field 'BmsErrSt' (BmsErrSt::Update(...) has never been called)."; },
                          __func__, __LINE__);
        ara::core::Abort("No initial value has been set for field 'BmsErrSt' (BmsErrSt::Update(...) has never been called).");
    }

    if (!BmsHVILSt.IsUpdated()) {
        logger_.LogError([this](ara::log::LogStream& s) {
                          s << "Failed to offer service 'SI_BmsStSrv' with instance id(s) '";
                          for (::amsr::socal::internal::InstanceSpecifierLookupTableEntry const& entry : offered_instances_) {
                              ara::core::StringView const instance_id_str{entry.GetInstanceIdentifier().ToString()};
                              s << " " << instance_id_str;
                          }
                          s << "' - No initial value has been set for field 'BmsHVILSt' (BmsHVILSt::Update(...) has never been called)."; },
                          __func__, __LINE__);
        ara::core::Abort("No initial value has been set for field 'BmsHVILSt' (BmsHVILSt::Update(...) has never been called).");
    }

    if (!BmsIsoMeasSt.IsUpdated()) {
        logger_.LogError([this](ara::log::LogStream& s) {
                          s << "Failed to offer service 'SI_BmsStSrv' with instance id(s) '";
                          for (::amsr::socal::internal::InstanceSpecifierLookupTableEntry const& entry : offered_instances_) {
                              ara::core::StringView const instance_id_str{entry.GetInstanceIdentifier().ToString()};
                              s << " " << instance_id_str;
                          }
                          s << "' - No initial value has been set for field 'BmsIsoMeasSt' (BmsIsoMeasSt::Update(...) has never been called)."; },
                          __func__, __LINE__);
        ara::core::Abort("No initial value has been set for field 'BmsIsoMeasSt' (BmsIsoMeasSt::Update(...) has never been called).");
    }

    if (!BmsSt.IsUpdated()) {
        logger_.LogError([this](ara::log::LogStream& s) {
                          s << "Failed to offer service 'SI_BmsStSrv' with instance id(s) '";
                          for (::amsr::socal::internal::InstanceSpecifierLookupTableEntry const& entry : offered_instances_) {
                              ara::core::StringView const instance_id_str{entry.GetInstanceIdentifier().ToString()};
                              s << " " << instance_id_str;
                          }
                          s << "' - No initial value has been set for field 'BmsSt' (BmsSt::Update(...) has never been called)."; },
                          __func__, __LINE__);
        ara::core::Abort("No initial value has been set for field 'BmsSt' (BmsSt::Update(...) has never been called).");
    }

    if (!HVBattSOCLim.IsUpdated()) {
        logger_.LogError([this](ara::log::LogStream& s) {
                          s << "Failed to offer service 'SI_BmsStSrv' with instance id(s) '";
                          for (::amsr::socal::internal::InstanceSpecifierLookupTableEntry const& entry : offered_instances_) {
                              ara::core::StringView const instance_id_str{entry.GetInstanceIdentifier().ToString()};
                              s << " " << instance_id_str;
                          }
                          s << "' - No initial value has been set for field 'HVBattSOCLim' (HVBattSOCLim::Update(...) has never been called)."; },
                          __func__, __LINE__);
        ara::core::Abort("No initial value has been set for field 'HVBattSOCLim' (HVBattSOCLim::Update(...) has never been called).");
    }

    if (!HVMaiSwtSt.IsUpdated()) {
        logger_.LogError([this](ara::log::LogStream& s) {
                          s << "Failed to offer service 'SI_BmsStSrv' with instance id(s) '";
                          for (::amsr::socal::internal::InstanceSpecifierLookupTableEntry const& entry : offered_instances_) {
                              ara::core::StringView const instance_id_str{entry.GetInstanceIdentifier().ToString()};
                              s << " " << instance_id_str;
                          }
                          s << "' - No initial value has been set for field 'HVMaiSwtSt' (HVMaiSwtSt::Update(...) has never been called)."; },
                          __func__, __LINE__);
        ara::core::Abort("No initial value has been set for field 'HVMaiSwtSt' (HVMaiSwtSt::Update(...) has never been called).");
    }
}

void SI_BmsStSrvSkeleton::SendInitialFieldNotifications() noexcept {
  // Send initial field events for all fields with "hasNotifier = true"
  BMSChrgSt.SendInitialValue();
  BMSDCChrgCnctSt.SendInitialValue();
  BMSDCChrgEndReas.SendInitialValue();
  BMSDchrgPrmsSt.SendInitialValue();
  BMSHeatRunawaySt.SendInitialValue();
  BMSInnerSOCSt.SendInitialValue();
  BMSIntelTempMgtSt.SendInitialValue();
  BMSLowTempSt.SendInitialValue();
  BMSRmtPreHeatSt.SendInitialValue();
  BMSSafCnctSt.SendInitialValue();
  BmsErrSt.SendInitialValue();
  BmsHVILSt.SendInitialValue();
  BmsIsoMeasSt.SendInitialValue();
  BmsSt.SendInitialValue();
  HVBattSOCLim.SendInitialValue();
  HVMaiSwtSt.SendInitialValue();
}

void SI_BmsStSrvSkeleton::OfferServiceInternal(::ara::com::InstanceIdentifier const& instance_id) noexcept {
  (*(sd_.GetAccess()))->OfferService(instance_id, this);
}

void SI_BmsStSrvSkeleton::StopOfferServiceInternal(::ara::com::InstanceIdentifier const& instance_id) noexcept {
  (*(sd_.GetAccess()))->StopOfferService(instance_id);
}

::amsr::generic::Singleton<SI_BmsStSrvSkeleton::ServiceDiscovery*>& SI_BmsStSrvSkeleton::GetServiceDiscovery() noexcept {
  return sd_;
}

void SI_BmsStSrvSkeleton::RegisterServiceDiscovery(SI_BmsStSrvSkeleton::ServiceDiscovery* service_discovery) noexcept {
  sd_.Create(service_discovery);
}

void SI_BmsStSrvSkeleton::DeRegisterServiceDiscovery() noexcept { sd_.Destroy(); }

}  // namespace skeleton
}  // namespace bmsstsrv_si
}  // namespace pt_batterypacksys
}  // namespace gwm

