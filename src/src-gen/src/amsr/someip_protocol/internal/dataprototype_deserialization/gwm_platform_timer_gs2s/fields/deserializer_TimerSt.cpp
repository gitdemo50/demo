/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  TimerSrvExe/src/amsr/someip_protocol/internal/dataprototype_deserialization/gwm_platform_timer_gs2s/fields/deserializer_TimerSt.cpp
 *        \brief  SOME/IP protocol deserializer implementation for data prototype '/S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/SI_TimerSrv_GS2S/TimerSt
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_someipprotocol
 *         Commit ID: 0e137b9e6356987a6d2839c8d79c286219b68d60
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "someip-protocol/internal/deserialization/deser_wrapper.h"
#include "amsr/someip_protocol/internal/dataprototype_deserialization/gwm_platform_timer_gs2s/fields/deserializer_TimerSt.h"
#include "amsr/someip_protocol/internal/datatype_deserialization/datatypes/gs2s/timersrv_gs2s/deserializer_TimerSt_Struct.h"

namespace amsr {
namespace someip_protocol {
namespace internal {
namespace dataprototype_deserializer {
namespace gwm_platform_timer_gs2s {
namespace fields {

deserialization::Result DeserializerTimerSt::Deserialize(deserialization::Reader &reader, ::datatypes::gs2s::timersrv_gs2s::TimerSt_Struct &data) {
  // Transformation properties parameter pack for data prototype /S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/SI_TimerSrv_GS2S/TimerSt
    using TpPackAlias = deserialization::TpPack<
        BigEndian,
        deserialization::SizeOfArrayLengthField<0>, 
        deserialization::SizeOfVectorLengthField<4>,
        deserialization::SizeOfMapLengthField<4>,
        deserialization::SizeOfStringLengthField<4>,
        deserialization::SizeOfStructLengthField<0>,
        deserialization::SizeOfUnionLengthField<4>,
        deserialization::SizeOfUnionTypeSelectorField<4>,
        deserialization::StringBomActive,
        deserialization::StringNullTerminationActive>;


  // Verify static size
  constexpr std::size_t static_size{deserialization::SomeIpProtocolGetStaticSize<
      TpPackAlias,
        // Config of struct length field (/S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/TimerSt_Struct)
      deserialization::LengthSize<deserialization::Tp<TpPackAlias>::kSizeOfStructLengthField, typename deserialization::Tp<TpPackAlias>::ByteOrder>

      >(deserialization::SizeToken<::datatypes::gs2s::timersrv_gs2s::TimerSt_Struct>{})};

  deserialization::Result result{reader.VerifySize(static_size)};
  if (result) {
    // Deserialize byte stream
    result = deserialization::SomeIpProtocolDeserialize<
      TpPackAlias,
      // Config of struct length field (/S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/TimerSt_Struct)
      deserialization::LengthSize<deserialization::Tp<TpPackAlias>::kSizeOfStructLengthField, typename deserialization::Tp<TpPackAlias>::ByteOrder>

      >(reader, data);
  }

  return result;
}

}  // namespace fields
}  // namespace gwm_platform_timer_gs2s
}  // namespace dataprototype_deserializer
}  // namespace internal
}  // namespace someip_protocol
}  // namespace amsr

