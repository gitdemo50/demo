// VECTOR Same Line AutosarC++17_10-A1.1.1: MD_SOMEIPBINDING_AutosarC++17_10-A1.1.1_external_identifiers
/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/src/amsr/someip_binding/internal/life_cycle_manager.cpp
 *        \brief  Initialization/Deinitialization functions for someip_binding
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_someipbinding
 *         Commit ID: 43b33f0283d4d61385b98a3599688499abd86a75
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "amsr/someip_binding/internal/life_cycle_manager.h"
#include <map>
#include <utility>
#include "amsr/socal/internal/service_discovery/proxy_service_discovery.h"
#include "amsr/socal/internal/service_discovery/service_discovery.h"
#include "amsr/someip_binding_transformation_layer/internal/gwm/platform/timer/gs2s/SI_TimerSrv_GS2S_skeleton_someip_binding.h"
#include "amsr/someip_binding_transformation_layer/internal/gwm/platform/timer/gs2s/SI_TimerSrv_GS2S_skeleton_someip_binding_init.h"
#include "amsr/someip_binding_transformation_layer/internal/gwm/pt_batterypacksys/bmsstsrv_si/SI_BmsStSrv_skeleton_someip_binding.h"
#include "amsr/someip_binding_transformation_layer/internal/gwm/pt_batterypacksys/bmsstsrv_si/SI_BmsStSrv_skeleton_someip_binding_init.h"
#include "amsr/someip_binding_transformation_layer/internal/gwm/pt_powersys/obcstsrv_si/SI_OBCStSrv_skeleton_someip_binding.h"
#include "amsr/someip_binding_transformation_layer/internal/gwm/pt_powersys/obcstsrv_si/SI_OBCStSrv_skeleton_someip_binding_init.h"
#include "amsr/someip_binding_transformation_layer/internal/gwm/pt_powersys/vehchrgnctrlsrv_si/SI_VehChrgnCtrlSrv_skeleton_someip_binding.h"
#include "amsr/someip_binding_transformation_layer/internal/gwm/pt_powersys/vehchrgnctrlsrv_si/SI_VehChrgnCtrlSrv_skeleton_someip_binding_init.h"
#include "amsr/someip_binding_transformation_layer/internal/gwm/thermmng_thermmng/battthermctrlsrv_si/SI_BattThermCtrlSrv_skeleton_someip_binding.h"
#include "amsr/someip_binding_transformation_layer/internal/gwm/thermmng_thermmng/battthermctrlsrv_si/SI_BattThermCtrlSrv_skeleton_someip_binding_init.h"
#include "someip_binding_transformation_layer/internal/service_discovery/service_discovery_proxy_xf.h"
#include "someip_binding_transformation_layer/internal/service_discovery/service_discovery_skeleton_xf.h"

namespace amsr {
namespace someip_binding {
namespace internal {

LifeCycleManager::LifeCycleManager(std::unique_ptr<RuntimeInterface>& runtime) noexcept : runtime_{runtime} {}

::ara::core::Result<void> LifeCycleManager::Initialize(amsr::someip_binding::internal::configuration::SomeIpBindingConfig& someip_binding_config) noexcept
{
  osabstraction::io::reactor1::Reactor1* const reactor{&runtime_->GetReactor()};
  std::function<void()> process_polling_function{runtime_->GetProcessPollingFunction()};
  aracom_someip_binding_.emplace(someip_binding_config, process_polling_function, reactor);


  // Register all service instances into SOME/IP Binding.
  InitializeRequiredServiceInstances();
  InitializeProvidedServiceInstances();

  // Create all ara::com / Binding Transformation objects.
  InitializeSkeletonSomeIpEventBackends();
  InitializeServiceDiscoveryProxyXfs();
  InitializeServiceDiscoverySkeletonXfs();

  // Registers all service instances into Socal.
  RegisterServiceInstances();

  return ::ara::core::Result<void> {};
}

::ara::core::Result<void> LifeCycleManager::Deinitialize() noexcept {
  CleanInstanceSpecifierToInstanceIdMapping();

  DeInitializeServiceDiscoveryProxyXfs();
  sd_skeleton_xfs_.clear();
  DeInitializeSkeletonSomeIpEventBackends();

  aracom_someip_binding_.reset();

  return ::ara::core::Result<void> {};
}

// VECTOR NC AutosarC++17_10-M9.3.3, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M9.3.3_Method_can_be_declared_const
// VECTOR NC AutosarC++17_10-A15.5.3: MD_SOMEIPBINDING_AutosarC++17_10-A15.4.2_A15.5.3_Exception_caught
// VECTOR NC AutosarC++17_10-A15.4.2: MD_SOMEIPBINDING_AutosarC++17_10-A15.4.2_A15.5.3_Exception_caught
void LifeCycleManager::RegisterServiceInstances() noexcept {
  {
    // No R-Ports configured
  }
  {
    // ---- Register all known P-Port InstanceSpecifiers ----
    {
      // Map P-Port /Applications/AdaptiveApplicationSwComponents/DenpendencySrvSwc/AdaptiveProvidedPortType_BattThermCtrlSrv to instance /ServiceInstances/DependencySrv/SomeIpSI_BattThermCtrlSrv_ProvidedInstance
      ::ara::core::InstanceSpecifier const instance_specifier{"DenpendencySrvExe/RootSwComponentPrototype/AdaptiveProvidedPortType_BattThermCtrlSrv"_sv};
      ::ara::com::InstanceIdentifier const instance_identifier{"SomeIp:1"_sv};

      runtime_->MapProvidedInstanceSpecifierToInstanceId(
            vac::container::CStringView{"SomeIp"_sv}, instance_specifier, instance_identifier,
            "/ServiceInterfaces/SI_BattThermCtrlSrv"_sv);
    }
    {
      // Map P-Port /Applications/AdaptiveApplicationSwComponents/DenpendencySrvSwc/AdaptiveProvidedPortType_BmsStSrv to instance /ServiceInstances/DependencySrv/SomeIpSI_BmsStSrv_ProvidedInstance
      ::ara::core::InstanceSpecifier const instance_specifier{"DenpendencySrvExe/RootSwComponentPrototype/AdaptiveProvidedPortType_BmsStSrv"_sv};
      ::ara::com::InstanceIdentifier const instance_identifier{"SomeIp:1"_sv};

      runtime_->MapProvidedInstanceSpecifierToInstanceId(
            vac::container::CStringView{"SomeIp"_sv}, instance_specifier, instance_identifier,
            "/ServiceInterfaces/SI_BmsStSrv"_sv);
    }
    {
      // Map P-Port /Applications/AdaptiveApplicationSwComponents/DenpendencySrvSwc/AdaptiveProvidedPortType_OBCStSrv to instance /ServiceInstances/DependencySrv/SomeIpSI_OBCStSrv_ProvidedInstance
      ::ara::core::InstanceSpecifier const instance_specifier{"DenpendencySrvExe/RootSwComponentPrototype/AdaptiveProvidedPortType_OBCStSrv"_sv};
      ::ara::com::InstanceIdentifier const instance_identifier{"SomeIp:1"_sv};

      runtime_->MapProvidedInstanceSpecifierToInstanceId(
            vac::container::CStringView{"SomeIp"_sv}, instance_specifier, instance_identifier,
            "/ServiceInterfaces/SI_OBCStSrv"_sv);
    }
    {
      // Map P-Port /Applications/AdaptiveApplicationSwComponents/DenpendencySrvSwc/AdaptiveProvidedPortType_Timer_GS2SSrv to instance /ServiceInstances/DependencySrv/SomeIpSI_TimerSrv_GS2S_ProvidedInstance
      ::ara::core::InstanceSpecifier const instance_specifier{"DenpendencySrvExe/RootSwComponentPrototype/AdaptiveProvidedPortType_Timer_GS2SSrv"_sv};
      ::ara::com::InstanceIdentifier const instance_identifier{"SomeIp:1"_sv};

      runtime_->MapProvidedInstanceSpecifierToInstanceId(
            vac::container::CStringView{"SomeIp"_sv}, instance_specifier, instance_identifier,
            "/S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/SI_TimerSrv_GS2S"_sv);
    }
    {
      // Map P-Port /Applications/AdaptiveApplicationSwComponents/DenpendencySrvSwc/AdaptiveProvidedPortType_VehChrgnCtrlSrv to instance /ServiceInstances/DependencySrv/SomeIpSI_VehChrgnCtrlSrv_ProvidedInstance
      ::ara::core::InstanceSpecifier const instance_specifier{"DenpendencySrvExe/RootSwComponentPrototype/AdaptiveProvidedPortType_VehChrgnCtrlSrv"_sv};
      ::ara::com::InstanceIdentifier const instance_identifier{"SomeIp:1"_sv};

      runtime_->MapProvidedInstanceSpecifierToInstanceId(
            vac::container::CStringView{"SomeIp"_sv}, instance_specifier, instance_identifier,
            "/ServiceInterfaces/SI_VehChrgnCtrlSrv"_sv);
    }
  }
}

// VECTOR NC AutosarC++17_10-M9.3.3, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M9.3.3_Method_can_be_declared_const
void LifeCycleManager::InitializeSkeletonSomeIpEventBackends() noexcept {
  // Initialize skeleton event backends for ServiceInterface '/S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/SI_TimerSrv_GS2S'
  ::amsr::someip_binding_transformation_layer::internal::gwm::platform::timer::gs2s::
      AraComSomeIpBindingInitializeSkeletonSomeIpEventBackendsSI_TimerSrv_GS2S(aracom_someip_binding_->GetServerManager());
  // Initialize skeleton event backends for ServiceInterface '/ServiceInterfaces/SI_BattThermCtrlSrv'
  ::amsr::someip_binding_transformation_layer::internal::gwm::thermmng_thermmng::battthermctrlsrv_si::
      AraComSomeIpBindingInitializeSkeletonSomeIpEventBackendsSI_BattThermCtrlSrv(aracom_someip_binding_->GetServerManager());
  // Initialize skeleton event backends for ServiceInterface '/ServiceInterfaces/SI_BmsStSrv'
  ::amsr::someip_binding_transformation_layer::internal::gwm::pt_batterypacksys::bmsstsrv_si::
      AraComSomeIpBindingInitializeSkeletonSomeIpEventBackendsSI_BmsStSrv(aracom_someip_binding_->GetServerManager());
  // Initialize skeleton event backends for ServiceInterface '/ServiceInterfaces/SI_OBCStSrv'
  ::amsr::someip_binding_transformation_layer::internal::gwm::pt_powersys::obcstsrv_si::
      AraComSomeIpBindingInitializeSkeletonSomeIpEventBackendsSI_OBCStSrv(aracom_someip_binding_->GetServerManager());
  // Initialize skeleton event backends for ServiceInterface '/ServiceInterfaces/SI_VehChrgnCtrlSrv'
  ::amsr::someip_binding_transformation_layer::internal::gwm::pt_powersys::vehchrgnctrlsrv_si::
      AraComSomeIpBindingInitializeSkeletonSomeIpEventBackendsSI_VehChrgnCtrlSrv(aracom_someip_binding_->GetServerManager());
}

// VECTOR NC AutosarC++17_10-M9.3.3, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M9.3.3_Method_can_be_declared_const
void LifeCycleManager::DeInitializeSkeletonSomeIpEventBackends() noexcept {
  // Initialize skeleton event backends for ServiceInterface '/S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/SI_TimerSrv_GS2S'
  ::amsr::someip_binding_transformation_layer::internal::gwm::platform::timer::gs2s::
      AraComSomeIpBindingDeInitializeSkeletonSomeIpEventBackendsSI_TimerSrv_GS2S();
  // Initialize skeleton event backends for ServiceInterface '/ServiceInterfaces/SI_BattThermCtrlSrv'
  ::amsr::someip_binding_transformation_layer::internal::gwm::thermmng_thermmng::battthermctrlsrv_si::
      AraComSomeIpBindingDeInitializeSkeletonSomeIpEventBackendsSI_BattThermCtrlSrv();
  // Initialize skeleton event backends for ServiceInterface '/ServiceInterfaces/SI_BmsStSrv'
  ::amsr::someip_binding_transformation_layer::internal::gwm::pt_batterypacksys::bmsstsrv_si::
      AraComSomeIpBindingDeInitializeSkeletonSomeIpEventBackendsSI_BmsStSrv();
  // Initialize skeleton event backends for ServiceInterface '/ServiceInterfaces/SI_OBCStSrv'
  ::amsr::someip_binding_transformation_layer::internal::gwm::pt_powersys::obcstsrv_si::
      AraComSomeIpBindingDeInitializeSkeletonSomeIpEventBackendsSI_OBCStSrv();
  // Initialize skeleton event backends for ServiceInterface '/ServiceInterfaces/SI_VehChrgnCtrlSrv'
  ::amsr::someip_binding_transformation_layer::internal::gwm::pt_powersys::vehchrgnctrlsrv_si::
      AraComSomeIpBindingDeInitializeSkeletonSomeIpEventBackendsSI_VehChrgnCtrlSrv();
}

// VECTOR NC AutosarC++17_10-M9.3.3, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M9.3.3_Method_can_be_declared_static
// VECTOR NC AutosarC++17_10-M0.1.8, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M0.1.8_Void_function_has_no_external_side_effect
void LifeCycleManager::InitializeRequiredServiceInstances() noexcept {
}

// VECTOR NC AutosarC++17_10-M9.3.3, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M9.3.3_Method_can_be_declared_const
// VECTOR NC AutosarC++17_10-M0.1.8, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M0.1.8_Void_function_has_no_external_side_effect
void LifeCycleManager::InitializeProvidedServiceInstances() noexcept {
    // Register provided service for instance /ServiceInstances/DependencySrv/SomeIpSI_BattThermCtrlSrv_ProvidedInstance
    {
      ::amsr::someip_binding::internal::ProvidedServiceInstanceId const SI_BattThermCtrlSrv_provided_service_instance_id{ static_cast<::amsr::someip_protocol::internal::ServiceId>(0x3506),
                                                                                                                                                        1, 0, 0x1 };
      aracom_someip_binding_->GetServerManager().RegisterProvidedServiceInstance(SI_BattThermCtrlSrv_provided_service_instance_id);
    }
    // Register provided service for instance /ServiceInstances/DependencySrv/SomeIpSI_BmsStSrv_ProvidedInstance
    {
      ::amsr::someip_binding::internal::ProvidedServiceInstanceId const SI_BmsStSrv_provided_service_instance_id{ static_cast<::amsr::someip_protocol::internal::ServiceId>(0x4008),
                                                                                                                                                        1, 0, 0x1 };
      aracom_someip_binding_->GetServerManager().RegisterProvidedServiceInstance(SI_BmsStSrv_provided_service_instance_id);
    }
    // Register provided service for instance /ServiceInstances/DependencySrv/SomeIpSI_OBCStSrv_ProvidedInstance
    {
      ::amsr::someip_binding::internal::ProvidedServiceInstanceId const SI_OBCStSrv_provided_service_instance_id{ static_cast<::amsr::someip_protocol::internal::ServiceId>(0x4004),
                                                                                                                                                        1, 0, 0x1 };
      aracom_someip_binding_->GetServerManager().RegisterProvidedServiceInstance(SI_OBCStSrv_provided_service_instance_id);
    }
    // Register provided service for instance /ServiceInstances/DependencySrv/SomeIpSI_TimerSrv_GS2S_ProvidedInstance
    {
      ::amsr::someip_binding::internal::ProvidedServiceInstanceId const SI_TimerSrv_GS2S_provided_service_instance_id{ static_cast<::amsr::someip_protocol::internal::ServiceId>(0x3AE7),
                                                                                                                                                        1, 0, 0x1 };
      aracom_someip_binding_->GetServerManager().RegisterProvidedServiceInstance(SI_TimerSrv_GS2S_provided_service_instance_id);
    }
    // Register provided service for instance /ServiceInstances/DependencySrv/SomeIpSI_VehChrgnCtrlSrv_ProvidedInstance
    {
      ::amsr::someip_binding::internal::ProvidedServiceInstanceId const SI_VehChrgnCtrlSrv_provided_service_instance_id{ static_cast<::amsr::someip_protocol::internal::ServiceId>(0x4012),
                                                                                                                                                        1, 0, 0x1 };
      aracom_someip_binding_->GetServerManager().RegisterProvidedServiceInstance(SI_VehChrgnCtrlSrv_provided_service_instance_id);
    }
}

// VECTOR NC AutosarC++17_10-M9.3.3, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M9.3.3_Method_can_be_declared_const
// VECTOR NC AutosarC++17_10-M3.2.1, Linker-Return_Type_Mismatch: MD_SOMEIPBINDING_AutosarC++17_10-M3.2.1_Return_type_mismatch
LifeCycleManager::SomeIpBindingType& LifeCycleManager::GetBinding() noexcept {
  if (!aracom_someip_binding_.has_value()) {
    ::ara::core::Abort("SOME/IP Binding has never been initialized.");
  }
  return aracom_someip_binding_.value();
}

std::map<::amsr::someip_binding::internal::RequiredServiceInstanceId, LifeCycleManager::E2EPropsMap> LifeCycleManager::ReadE2EPropsMaps() noexcept {
  std::map<::amsr::someip_binding::internal::RequiredServiceInstanceId, E2EPropsMap> required_e2e_props_map{};

  return std::move(required_e2e_props_map);
}

void LifeCycleManager::InitializeServiceDiscoveryProxyXfs() noexcept {
  std::map<::amsr::someip_binding::internal::RequiredServiceInstanceId, E2EPropsMap> const required_e2e_props_map{
      ReadE2EPropsMaps()};
}

void LifeCycleManager::DeInitializeServiceDiscoveryProxyXfs() noexcept {

  // Destroy all SdProxyXf instances
  sd_proxy_xfs_.clear();
}


void LifeCycleManager::InitializeServiceDiscoverySkeletonXfs() noexcept {

  // Instantiate ServiceDiscoverySkeletonXfs for instance /ServiceInstances/DependencySrv/SomeIpSI_BattThermCtrlSrv_ProvidedInstance
  {
    // Types.
    using SkeletonXfType =
        ::amsr::someip_binding_transformation_layer::internal::gwm::thermmng_thermmng::battthermctrlsrv_si::SI_BattThermCtrlSrvSkeletonSomeIpBinding;
    using SkeletonBackendInterfaceType =
        ::gwm::thermmng_thermmng::battthermctrlsrv_si::skeleton::SI_BattThermCtrlSrvSkeleton;
    using ServiceDiscoveryType =
        ::amsr::socal::internal::service_discovery::ServiceDiscovery<SkeletonBackendInterfaceType>;
    using SdSkeletonXfType =
        ::amsr::someip_binding_transformation_layer::internal::service_discovery::ServiceDiscoverySkeletonXf<
            SkeletonXfType,
            SkeletonBackendInterfaceType,
            ::amsr::someip_binding::internal::ServerManager<>,
            ServiceDiscoveryType>;

    // Variables.
    ::amsr::someip_binding::internal::ProvidedServiceInstanceId const provided_service_instance_id{static_cast<::amsr::someip_protocol::internal::ServiceId>(0x3506),
         1, 0, 0x1 };

    ::ara::com::InstanceIdentifier const instance_identifier{"SomeIp:1"_sv};

    ::amsr::someip_binding::internal::ServerManager<
        amsr::someip_daemon_client::internal::SkeletonSomeIpDaemonClient<
            amsr::someip_daemon_client::internal::SomeIpDaemonClientDefaultTemplateConfiguration>>& server_manager{
        aracom_someip_binding_->GetServerManager()};

    ::amsr::generic::Singleton<::amsr::socal::internal::service_discovery::ServiceDiscovery<
        ::gwm::thermmng_thermmng::battthermctrlsrv_si::skeleton::SI_BattThermCtrlSrvSkeleton>*>& service_discovery {
            ::gwm::thermmng_thermmng::battthermctrlsrv_si::skeleton::SI_BattThermCtrlSrvSkeleton::GetServiceDiscovery()};

    // Add the new entry to the container of XF's.
    sd_skeleton_xfs_.emplace_back(std::make_unique<SdSkeletonXfType>(
        provided_service_instance_id, instance_identifier, server_manager, service_discovery));
  }

  // Instantiate ServiceDiscoverySkeletonXfs for instance /ServiceInstances/DependencySrv/SomeIpSI_BmsStSrv_ProvidedInstance
  {
    // Types.
    using SkeletonXfType =
        ::amsr::someip_binding_transformation_layer::internal::gwm::pt_batterypacksys::bmsstsrv_si::SI_BmsStSrvSkeletonSomeIpBinding;
    using SkeletonBackendInterfaceType =
        ::gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton;
    using ServiceDiscoveryType =
        ::amsr::socal::internal::service_discovery::ServiceDiscovery<SkeletonBackendInterfaceType>;
    using SdSkeletonXfType =
        ::amsr::someip_binding_transformation_layer::internal::service_discovery::ServiceDiscoverySkeletonXf<
            SkeletonXfType,
            SkeletonBackendInterfaceType,
            ::amsr::someip_binding::internal::ServerManager<>,
            ServiceDiscoveryType>;

    // Variables.
    ::amsr::someip_binding::internal::ProvidedServiceInstanceId const provided_service_instance_id{static_cast<::amsr::someip_protocol::internal::ServiceId>(0x4008),
         1, 0, 0x1 };

    ::ara::com::InstanceIdentifier const instance_identifier{"SomeIp:1"_sv};

    ::amsr::someip_binding::internal::ServerManager<
        amsr::someip_daemon_client::internal::SkeletonSomeIpDaemonClient<
            amsr::someip_daemon_client::internal::SomeIpDaemonClientDefaultTemplateConfiguration>>& server_manager{
        aracom_someip_binding_->GetServerManager()};

    ::amsr::generic::Singleton<::amsr::socal::internal::service_discovery::ServiceDiscovery<
        ::gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton>*>& service_discovery {
            ::gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton::GetServiceDiscovery()};

    // Add the new entry to the container of XF's.
    sd_skeleton_xfs_.emplace_back(std::make_unique<SdSkeletonXfType>(
        provided_service_instance_id, instance_identifier, server_manager, service_discovery));
  }

  // Instantiate ServiceDiscoverySkeletonXfs for instance /ServiceInstances/DependencySrv/SomeIpSI_OBCStSrv_ProvidedInstance
  {
    // Types.
    using SkeletonXfType =
        ::amsr::someip_binding_transformation_layer::internal::gwm::pt_powersys::obcstsrv_si::SI_OBCStSrvSkeletonSomeIpBinding;
    using SkeletonBackendInterfaceType =
        ::gwm::pt_powersys::obcstsrv_si::skeleton::SI_OBCStSrvSkeleton;
    using ServiceDiscoveryType =
        ::amsr::socal::internal::service_discovery::ServiceDiscovery<SkeletonBackendInterfaceType>;
    using SdSkeletonXfType =
        ::amsr::someip_binding_transformation_layer::internal::service_discovery::ServiceDiscoverySkeletonXf<
            SkeletonXfType,
            SkeletonBackendInterfaceType,
            ::amsr::someip_binding::internal::ServerManager<>,
            ServiceDiscoveryType>;

    // Variables.
    ::amsr::someip_binding::internal::ProvidedServiceInstanceId const provided_service_instance_id{static_cast<::amsr::someip_protocol::internal::ServiceId>(0x4004),
         1, 0, 0x1 };

    ::ara::com::InstanceIdentifier const instance_identifier{"SomeIp:1"_sv};

    ::amsr::someip_binding::internal::ServerManager<
        amsr::someip_daemon_client::internal::SkeletonSomeIpDaemonClient<
            amsr::someip_daemon_client::internal::SomeIpDaemonClientDefaultTemplateConfiguration>>& server_manager{
        aracom_someip_binding_->GetServerManager()};

    ::amsr::generic::Singleton<::amsr::socal::internal::service_discovery::ServiceDiscovery<
        ::gwm::pt_powersys::obcstsrv_si::skeleton::SI_OBCStSrvSkeleton>*>& service_discovery {
            ::gwm::pt_powersys::obcstsrv_si::skeleton::SI_OBCStSrvSkeleton::GetServiceDiscovery()};

    // Add the new entry to the container of XF's.
    sd_skeleton_xfs_.emplace_back(std::make_unique<SdSkeletonXfType>(
        provided_service_instance_id, instance_identifier, server_manager, service_discovery));
  }

  // Instantiate ServiceDiscoverySkeletonXfs for instance /ServiceInstances/DependencySrv/SomeIpSI_TimerSrv_GS2S_ProvidedInstance
  {
    // Types.
    using SkeletonXfType =
        ::amsr::someip_binding_transformation_layer::internal::gwm::platform::timer::gs2s::SI_TimerSrv_GS2SSkeletonSomeIpBinding;
    using SkeletonBackendInterfaceType =
        ::gwm::platform::timer::gs2s::skeleton::SI_TimerSrv_GS2SSkeleton;
    using ServiceDiscoveryType =
        ::amsr::socal::internal::service_discovery::ServiceDiscovery<SkeletonBackendInterfaceType>;
    using SdSkeletonXfType =
        ::amsr::someip_binding_transformation_layer::internal::service_discovery::ServiceDiscoverySkeletonXf<
            SkeletonXfType,
            SkeletonBackendInterfaceType,
            ::amsr::someip_binding::internal::ServerManager<>,
            ServiceDiscoveryType>;

    // Variables.
    ::amsr::someip_binding::internal::ProvidedServiceInstanceId const provided_service_instance_id{static_cast<::amsr::someip_protocol::internal::ServiceId>(0x3AE7),
         1, 0, 0x1 };

    ::ara::com::InstanceIdentifier const instance_identifier{"SomeIp:1"_sv};

    ::amsr::someip_binding::internal::ServerManager<
        amsr::someip_daemon_client::internal::SkeletonSomeIpDaemonClient<
            amsr::someip_daemon_client::internal::SomeIpDaemonClientDefaultTemplateConfiguration>>& server_manager{
        aracom_someip_binding_->GetServerManager()};

    ::amsr::generic::Singleton<::amsr::socal::internal::service_discovery::ServiceDiscovery<
        ::gwm::platform::timer::gs2s::skeleton::SI_TimerSrv_GS2SSkeleton>*>& service_discovery {
            ::gwm::platform::timer::gs2s::skeleton::SI_TimerSrv_GS2SSkeleton::GetServiceDiscovery()};

    // Add the new entry to the container of XF's.
    sd_skeleton_xfs_.emplace_back(std::make_unique<SdSkeletonXfType>(
        provided_service_instance_id, instance_identifier, server_manager, service_discovery));
  }

  // Instantiate ServiceDiscoverySkeletonXfs for instance /ServiceInstances/DependencySrv/SomeIpSI_VehChrgnCtrlSrv_ProvidedInstance
  {
    // Types.
    using SkeletonXfType =
        ::amsr::someip_binding_transformation_layer::internal::gwm::pt_powersys::vehchrgnctrlsrv_si::SI_VehChrgnCtrlSrvSkeletonSomeIpBinding;
    using SkeletonBackendInterfaceType =
        ::gwm::pt_powersys::vehchrgnctrlsrv_si::skeleton::SI_VehChrgnCtrlSrvSkeleton;
    using ServiceDiscoveryType =
        ::amsr::socal::internal::service_discovery::ServiceDiscovery<SkeletonBackendInterfaceType>;
    using SdSkeletonXfType =
        ::amsr::someip_binding_transformation_layer::internal::service_discovery::ServiceDiscoverySkeletonXf<
            SkeletonXfType,
            SkeletonBackendInterfaceType,
            ::amsr::someip_binding::internal::ServerManager<>,
            ServiceDiscoveryType>;

    // Variables.
    ::amsr::someip_binding::internal::ProvidedServiceInstanceId const provided_service_instance_id{static_cast<::amsr::someip_protocol::internal::ServiceId>(0x4012),
         1, 0, 0x1 };

    ::ara::com::InstanceIdentifier const instance_identifier{"SomeIp:1"_sv};

    ::amsr::someip_binding::internal::ServerManager<
        amsr::someip_daemon_client::internal::SkeletonSomeIpDaemonClient<
            amsr::someip_daemon_client::internal::SomeIpDaemonClientDefaultTemplateConfiguration>>& server_manager{
        aracom_someip_binding_->GetServerManager()};

    ::amsr::generic::Singleton<::amsr::socal::internal::service_discovery::ServiceDiscovery<
        ::gwm::pt_powersys::vehchrgnctrlsrv_si::skeleton::SI_VehChrgnCtrlSrvSkeleton>*>& service_discovery {
            ::gwm::pt_powersys::vehchrgnctrlsrv_si::skeleton::SI_VehChrgnCtrlSrvSkeleton::GetServiceDiscovery()};

    // Add the new entry to the container of XF's.
    sd_skeleton_xfs_.emplace_back(std::make_unique<SdSkeletonXfType>(
        provided_service_instance_id, instance_identifier, server_manager, service_discovery));
  }
}

// VECTOR NC AutosarC++17_10-M9.3.3, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M9.3.3_Method_can_be_declared_const
// VECTOR NC AutosarC++17_10-A15.5.3: MD_SOMEIPBINDING_AutosarC++17_10-A15.4.2_A15.5.3_Exception_caught
// VECTOR NC AutosarC++17_10-A15.4.2: MD_SOMEIPBINDING_AutosarC++17_10-A15.4.2_A15.5.3_Exception_caught
void LifeCycleManager::CleanInstanceSpecifierToInstanceIdMapping() noexcept {
  {
    // Remove mapping P-Port /Applications/AdaptiveApplicationSwComponents/DenpendencySrvSwc/AdaptiveProvidedPortType_BattThermCtrlSrv to instance /ServiceInstances/DependencySrv/SomeIpSI_BattThermCtrlSrv_ProvidedInstance
    ::ara::core::InstanceSpecifier const instance_specifier{"DenpendencySrvExe/RootSwComponentPrototype/AdaptiveProvidedPortType_BattThermCtrlSrv"_sv};
    ::ara::com::InstanceIdentifier const instance_identifier{"SomeIp:1"_sv};

    runtime_->RemoveProvidedInstanceSpecifierEntry(instance_specifier, instance_identifier);
  }
  {
    // Remove mapping P-Port /Applications/AdaptiveApplicationSwComponents/DenpendencySrvSwc/AdaptiveProvidedPortType_BmsStSrv to instance /ServiceInstances/DependencySrv/SomeIpSI_BmsStSrv_ProvidedInstance
    ::ara::core::InstanceSpecifier const instance_specifier{"DenpendencySrvExe/RootSwComponentPrototype/AdaptiveProvidedPortType_BmsStSrv"_sv};
    ::ara::com::InstanceIdentifier const instance_identifier{"SomeIp:1"_sv};

    runtime_->RemoveProvidedInstanceSpecifierEntry(instance_specifier, instance_identifier);
  }
  {
    // Remove mapping P-Port /Applications/AdaptiveApplicationSwComponents/DenpendencySrvSwc/AdaptiveProvidedPortType_OBCStSrv to instance /ServiceInstances/DependencySrv/SomeIpSI_OBCStSrv_ProvidedInstance
    ::ara::core::InstanceSpecifier const instance_specifier{"DenpendencySrvExe/RootSwComponentPrototype/AdaptiveProvidedPortType_OBCStSrv"_sv};
    ::ara::com::InstanceIdentifier const instance_identifier{"SomeIp:1"_sv};

    runtime_->RemoveProvidedInstanceSpecifierEntry(instance_specifier, instance_identifier);
  }
  {
    // Remove mapping P-Port /Applications/AdaptiveApplicationSwComponents/DenpendencySrvSwc/AdaptiveProvidedPortType_Timer_GS2SSrv to instance /ServiceInstances/DependencySrv/SomeIpSI_TimerSrv_GS2S_ProvidedInstance
    ::ara::core::InstanceSpecifier const instance_specifier{"DenpendencySrvExe/RootSwComponentPrototype/AdaptiveProvidedPortType_Timer_GS2SSrv"_sv};
    ::ara::com::InstanceIdentifier const instance_identifier{"SomeIp:1"_sv};

    runtime_->RemoveProvidedInstanceSpecifierEntry(instance_specifier, instance_identifier);
  }
  {
    // Remove mapping P-Port /Applications/AdaptiveApplicationSwComponents/DenpendencySrvSwc/AdaptiveProvidedPortType_VehChrgnCtrlSrv to instance /ServiceInstances/DependencySrv/SomeIpSI_VehChrgnCtrlSrv_ProvidedInstance
    ::ara::core::InstanceSpecifier const instance_specifier{"DenpendencySrvExe/RootSwComponentPrototype/AdaptiveProvidedPortType_VehChrgnCtrlSrv"_sv};
    ::ara::com::InstanceIdentifier const instance_identifier{"SomeIp:1"_sv};

    runtime_->RemoveProvidedInstanceSpecifierEntry(instance_specifier, instance_identifier);
  }
}

}  // namespace internal
}  // namespace someip_binding
}  // namespace amsr

