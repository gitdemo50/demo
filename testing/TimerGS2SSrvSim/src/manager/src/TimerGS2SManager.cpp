#include <memory>

#include "TimerGS2SManager.h"

namespace gwm {
namespace manager {

TimerGS2SManager::TimerGS2SManager() : srv_TimerSrvService(std::make_shared<TimerSrvService>()),
        logger(gwm::platform::Logger("TimerGS2S")) 
{
    logger.LogInfo() << "TimerGS2SManager manager ready";
}

void TimerGS2SManager::start() 
{
    logger.LogInfo() << "Starting TimerGS2SManager Manager";
    initWindowController();
    startControllers();
    startHealthMonitors();
}

void TimerGS2SManager::startControllers() 
{
    srv_TimerSrvService->start();
}

void TimerGS2SManager::startHealthMonitors() 
{

}

void TimerGS2SManager::initWindowController() 
{
    
}

}
}