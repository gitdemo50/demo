/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  TimerSrvExe/src/gwm/platform/timer/gs2s/si_timersrv_gs2s_proxy.cpp
 *        \brief  Proxy for service 'SI_TimerSrv_GS2S'.
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_socal
 *         Commit ID: 209b3c0234013e84e3170ee826c9a8c6022de194
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "gwm/platform/timer/gs2s/si_timersrv_gs2s_proxy.h"

/*!
 * \trace SPEC-4980240
 * \trace SPEC-4980242
 */
namespace gwm {
namespace platform {
namespace timer {
namespace gs2s {
namespace proxy {

 /*!
  * \trace SPEC-8053550
  */
// ============ Proxy preconstructor (returns a token or an error) ============
SI_TimerSrv_GS2SProxy::ConstructionResult SI_TimerSrv_GS2SProxy::Preconstruct(
    SI_TimerSrv_GS2SProxy::HandleType const& handle) noexcept {
    return Base::Preconstruct(handle.GetInstanceId());
}

/*!
 * \trace SPEC-8053550
 */
// ====================== Proxy constructor (token based) =====================
SI_TimerSrv_GS2SProxy::SI_TimerSrv_GS2SProxy(ConstructionToken&& token) noexcept
  : Base{std::move(token)},
    logger_(amsr::socal::internal::logging::kAraComLoggerContextId, amsr::socal::internal::logging::kAraComLoggerContextDescription,
            "SI_TimerSrv_GS2SProxy"),
    proxy_update_manager_{Base::GetInstanceId(), Base::GetServiceDiscovery(), Base::GetProxyBackend()},
    TimerReq{proxy_update_manager_},
// VECTOR Disable VectorC++-V5.0.1: MD_SOCAL_VectorC++-V5.0.1_UnsequencedFunctionCalls
    TimerSt{&(proxy_update_manager_.GetFieldNotifierBackendTimerSt()), proxy_update_manager_, Base::GetRuntime(), "SI_TimerSrv_GS2S", "TimerSt", Base::GetInstanceId()}
    
// VECTOR Enable VectorC++-V5.0.1
    {}

// ====================== Proxy constructor ======================
SI_TimerSrv_GS2SProxy::SI_TimerSrv_GS2SProxy(
  SI_TimerSrv_GS2SProxy::HandleType const& handle) noexcept
  : SI_TimerSrv_GS2SProxy{Preconstruct(handle).Value()} {}

// ============================= Proxy destructor =============================
SI_TimerSrv_GS2SProxy::~SI_TimerSrv_GS2SProxy() noexcept {
  // Start cleanup of proxy by unsubscribing all event and field notifications
  // Events

  // Fields
  
  TimerSt.UnsetReceiveHandler();
  TimerSt.UnsetSubscriptionStateHandler();
  TimerSt.Unsubscribe();
}

::amsr::socal::ServiceState SI_TimerSrv_GS2SProxy::ReadServiceState() noexcept {
  return proxy_update_manager_.ReadServiceState();
}

}  // namespace proxy
}  // namespace gs2s
}  // namespace timer
}  // namespace platform
}  // namespace gwm

