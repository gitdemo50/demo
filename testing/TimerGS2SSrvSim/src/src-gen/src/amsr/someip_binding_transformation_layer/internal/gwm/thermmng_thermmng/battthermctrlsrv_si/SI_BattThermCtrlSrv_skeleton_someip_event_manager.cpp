/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/src/amsr/someip_binding_transformation_layer/internal/gwm/thermmng_thermmng/battthermctrlsrv_si/SI_BattThermCtrlSrv_skeleton_someip_event_manager.cpp
 *        \brief  SOME/IP skeleton event handling for events and field notifications of service 'SI_BattThermCtrlSrv'
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_someipbinding
 *         Commit ID: 43b33f0283d4d61385b98a3599688499abd86a75
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "amsr/someip_binding_transformation_layer/internal/gwm/thermmng_thermmng/battthermctrlsrv_si/SI_BattThermCtrlSrv_skeleton_someip_event_manager.h"

namespace amsr {
namespace someip_binding_transformation_layer {
namespace internal {
namespace gwm {
namespace thermmng_thermmng {
namespace battthermctrlsrv_si {


}  // namespace internal
}  // namespace someip_binding_transformation_layer
}  // namespace amsr
}  // namespace battthermctrlsrv_si
}  // namespace thermmng_thermmng
}  // namespace gwm

