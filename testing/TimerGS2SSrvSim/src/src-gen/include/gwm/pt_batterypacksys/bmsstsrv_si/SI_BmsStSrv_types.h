
/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/include/gwm/pt_batterypacksys/bmsstsrv_si/SI_BmsStSrv_types.h
 *        \brief  Input and output structures for methods, fields and application errors of service 'SI_BmsStSrv'
 *
 *      \details  Definition of common input-/output structs used for simplified argument / marshalling handling. For all elements like methods, events fields structs with the related in-/output arguments are generated.
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_socal
 *         Commit ID: 209b3c0234013e84e3170ee826c9a8c6022de194
 *********************************************************************************************************************/

#ifndef DENPENDENCYSRVEXE_INCLUDE_GWM_PT_BATTERYPACKSYS_BMSSTSRV_SI_SI_BMSSTSRV_TYPES_H_
#define DENPENDENCYSRVEXE_INCLUDE_GWM_PT_BATTERYPACKSYS_BMSSTSRV_SI_SI_BMSSTSRV_TYPES_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "amsr/socal/internal/handle_type.h"
#include "gwm/pt_batterypacksys/bmsstsrv_si/si_bmsstsrv_common.h"

namespace gwm {
namespace pt_batterypacksys {
namespace bmsstsrv_si {

namespace internal {

/*!
 * \brief Proxy HandleType for the Service 'SI_BmsStSrv'.
 * \trace SPEC-4980259
 */
class SI_BmsStSrvHandleType final : public ::amsr::socal::internal::HandleType {
  public:
  /*!
   * \brief Inherit constructor.
   */
  using HandleType::HandleType;
};

namespace methods {

}  // namespace methods

namespace fields {

/*!
 * \brief Data class for service field 'BMSChrgSt'.
 * \remark generated
 */
class BMSChrgSt final {
 public:
  /*!
   * \brief Return/output parameters of service field 'BMSChrgSt'
   */
  using Output = ::gwm::pt_batterypacksys::batterypacksys_idt::BMSChrgSt_Enum_Idt;
};


  /*!
 * \brief A class for field method 'BMSChrgSt'Get used for type definitions.
 */
class BMSChrgStGet final {
 public:
  /*!
   * \brief Struct representing the field input data.
   */
  struct Input {};

  /*!
   * \brief Struct representing the field output value.
   */
  struct Output {
    /*!
     * \brief Reference of field value (/DataTypes/ImplementationDataTypes/BMSChrgSt_Enum_Idt)
     */
    ::gwm::pt_batterypacksys::batterypacksys_idt::BMSChrgSt_Enum_Idt out_val;
  };
};


/*!
 * \brief Data class for service field 'BMSDCChrgCnctSt'.
 * \remark generated
 */
class BMSDCChrgCnctSt final {
 public:
  /*!
   * \brief Return/output parameters of service field 'BMSDCChrgCnctSt'
   */
  using Output = ::gwm::pt_batterypacksys::batterypacksys_idt::BMSDCChrgCnctSt_Enum_Idt;
};


  /*!
 * \brief A class for field method 'BMSDCChrgCnctSt'Get used for type definitions.
 */
class BMSDCChrgCnctStGet final {
 public:
  /*!
   * \brief Struct representing the field input data.
   */
  struct Input {};

  /*!
   * \brief Struct representing the field output value.
   */
  struct Output {
    /*!
     * \brief Reference of field value (/DataTypes/ImplementationDataTypes/BMSDCChrgCnctSt_Enum_Idt)
     */
    ::gwm::pt_batterypacksys::batterypacksys_idt::BMSDCChrgCnctSt_Enum_Idt out_val;
  };
};


/*!
 * \brief Data class for service field 'BMSDCChrgEndReas'.
 * \remark generated
 */
class BMSDCChrgEndReas final {
 public:
  /*!
   * \brief Return/output parameters of service field 'BMSDCChrgEndReas'
   */
  using Output = ::gwm::pt_batterypacksys::batterypacksys_idt::BMSDCChrgEndReas_Enum_Idt;
};


  /*!
 * \brief A class for field method 'BMSDCChrgEndReas'Get used for type definitions.
 */
class BMSDCChrgEndReasGet final {
 public:
  /*!
   * \brief Struct representing the field input data.
   */
  struct Input {};

  /*!
   * \brief Struct representing the field output value.
   */
  struct Output {
    /*!
     * \brief Reference of field value (/DataTypes/ImplementationDataTypes/BMSDCChrgEndReas_Enum_Idt)
     */
    ::gwm::pt_batterypacksys::batterypacksys_idt::BMSDCChrgEndReas_Enum_Idt out_val;
  };
};


/*!
 * \brief Data class for service field 'BMSDchrgPrmsSt'.
 * \remark generated
 */
class BMSDchrgPrmsSt final {
 public:
  /*!
   * \brief Return/output parameters of service field 'BMSDchrgPrmsSt'
   */
  using Output = ::gwm::pt_batterypacksys::batterypacksys_idt::BMSDchrgPrmsSt_Enum_Idt;
};


  /*!
 * \brief A class for field method 'BMSDchrgPrmsSt'Get used for type definitions.
 */
class BMSDchrgPrmsStGet final {
 public:
  /*!
   * \brief Struct representing the field input data.
   */
  struct Input {};

  /*!
   * \brief Struct representing the field output value.
   */
  struct Output {
    /*!
     * \brief Reference of field value (/DataTypes/ImplementationDataTypes/BMSDchrgPrmsSt_Enum_Idt)
     */
    ::gwm::pt_batterypacksys::batterypacksys_idt::BMSDchrgPrmsSt_Enum_Idt out_val;
  };
};


/*!
 * \brief Data class for service field 'BMSHeatRunawaySt'.
 * \remark generated
 */
class BMSHeatRunawaySt final {
 public:
  /*!
   * \brief Return/output parameters of service field 'BMSHeatRunawaySt'
   */
  using Output = ::gwm::pt_batterypacksys::batterypacksys_idt::BMSHeatRunawaySt_Enum_Idt;
};


  /*!
 * \brief A class for field method 'BMSHeatRunawaySt'Get used for type definitions.
 */
class BMSHeatRunawayStGet final {
 public:
  /*!
   * \brief Struct representing the field input data.
   */
  struct Input {};

  /*!
   * \brief Struct representing the field output value.
   */
  struct Output {
    /*!
     * \brief Reference of field value (/DataTypes/ImplementationDataTypes/BMSHeatRunawaySt_Enum_Idt)
     */
    ::gwm::pt_batterypacksys::batterypacksys_idt::BMSHeatRunawaySt_Enum_Idt out_val;
  };
};


/*!
 * \brief Data class for service field 'BMSInnerSOCSt'.
 * \remark generated
 */
class BMSInnerSOCSt final {
 public:
  /*!
   * \brief Return/output parameters of service field 'BMSInnerSOCSt'
   */
  using Output = ::gwm::pt_batterypacksys::batterypacksys_idt::BMSInnerSOCSt_Struct_Idt;
};


  /*!
 * \brief A class for field method 'BMSInnerSOCSt'Get used for type definitions.
 */
class BMSInnerSOCStGet final {
 public:
  /*!
   * \brief Struct representing the field input data.
   */
  struct Input {};

  /*!
   * \brief Struct representing the field output value.
   */
  struct Output {
    /*!
     * \brief Reference of field value (/DataTypes/ImplementationDataTypes/BMSInnerSOCSt_Struct_Idt)
     */
    ::gwm::pt_batterypacksys::batterypacksys_idt::BMSInnerSOCSt_Struct_Idt out_val;
  };
};


/*!
 * \brief Data class for service field 'BMSIntelTempMgtSt'.
 * \remark generated
 */
class BMSIntelTempMgtSt final {
 public:
  /*!
   * \brief Return/output parameters of service field 'BMSIntelTempMgtSt'
   */
  using Output = ::gwm::pt_batterypacksys::batterypacksys_idt::BMSIntelTempMgtSt_Enum_Idt;
};


  /*!
 * \brief A class for field method 'BMSIntelTempMgtSt'Get used for type definitions.
 */
class BMSIntelTempMgtStGet final {
 public:
  /*!
   * \brief Struct representing the field input data.
   */
  struct Input {};

  /*!
   * \brief Struct representing the field output value.
   */
  struct Output {
    /*!
     * \brief Reference of field value (/DataTypes/ImplementationDataTypes/BMSIntelTempMgtSt_Enum_Idt)
     */
    ::gwm::pt_batterypacksys::batterypacksys_idt::BMSIntelTempMgtSt_Enum_Idt out_val;
  };
};


/*!
 * \brief Data class for service field 'BMSLowTempSt'.
 * \remark generated
 */
class BMSLowTempSt final {
 public:
  /*!
   * \brief Return/output parameters of service field 'BMSLowTempSt'
   */
  using Output = ::gwm::pt_batterypacksys::batterypacksys_idt::BMSLowTempSt_Struct_Idt;
};


  /*!
 * \brief A class for field method 'BMSLowTempSt'Get used for type definitions.
 */
class BMSLowTempStGet final {
 public:
  /*!
   * \brief Struct representing the field input data.
   */
  struct Input {};

  /*!
   * \brief Struct representing the field output value.
   */
  struct Output {
    /*!
     * \brief Reference of field value (/DataTypes/ImplementationDataTypes/BMSLowTempSt_Struct_Idt)
     */
    ::gwm::pt_batterypacksys::batterypacksys_idt::BMSLowTempSt_Struct_Idt out_val;
  };
};


/*!
 * \brief Data class for service field 'BMSRmtPreHeatSt'.
 * \remark generated
 */
class BMSRmtPreHeatSt final {
 public:
  /*!
   * \brief Return/output parameters of service field 'BMSRmtPreHeatSt'
   */
  using Output = ::gwm::pt_batterypacksys::batterypacksys_idt::BMSRmtPreHeatSt_Enum_Idt;
};


  /*!
 * \brief A class for field method 'BMSRmtPreHeatSt'Get used for type definitions.
 */
class BMSRmtPreHeatStGet final {
 public:
  /*!
   * \brief Struct representing the field input data.
   */
  struct Input {};

  /*!
   * \brief Struct representing the field output value.
   */
  struct Output {
    /*!
     * \brief Reference of field value (/DataTypes/ImplementationDataTypes/BMSRmtPreHeatSt_Enum_Idt)
     */
    ::gwm::pt_batterypacksys::batterypacksys_idt::BMSRmtPreHeatSt_Enum_Idt out_val;
  };
};


/*!
 * \brief Data class for service field 'BMSSafCnctSt'.
 * \remark generated
 */
class BMSSafCnctSt final {
 public:
  /*!
   * \brief Return/output parameters of service field 'BMSSafCnctSt'
   */
  using Output = ::gwm::pt_batterypacksys::batterypacksys_idt::BMSSafCnctSt_Enum_Idt;
};


  /*!
 * \brief A class for field method 'BMSSafCnctSt'Get used for type definitions.
 */
class BMSSafCnctStGet final {
 public:
  /*!
   * \brief Struct representing the field input data.
   */
  struct Input {};

  /*!
   * \brief Struct representing the field output value.
   */
  struct Output {
    /*!
     * \brief Reference of field value (/DataTypes/ImplementationDataTypes/BMSSafCnctSt_Enum_Idt)
     */
    ::gwm::pt_batterypacksys::batterypacksys_idt::BMSSafCnctSt_Enum_Idt out_val;
  };
};


/*!
 * \brief Data class for service field 'BmsErrSt'.
 * \remark generated
 */
class BmsErrSt final {
 public:
  /*!
   * \brief Return/output parameters of service field 'BmsErrSt'
   */
  using Output = ::gwm::pt_batterypacksys::batterypacksys_idt::BmsErrSt_Struct_Idt;
};


  /*!
 * \brief A class for field method 'BmsErrSt'Get used for type definitions.
 */
class BmsErrStGet final {
 public:
  /*!
   * \brief Struct representing the field input data.
   */
  struct Input {};

  /*!
   * \brief Struct representing the field output value.
   */
  struct Output {
    /*!
     * \brief Reference of field value (/DataTypes/ImplementationDataTypes/BmsErrSt_Struct_Idt)
     */
    ::gwm::pt_batterypacksys::batterypacksys_idt::BmsErrSt_Struct_Idt out_val;
  };
};


/*!
 * \brief Data class for service field 'BmsHVILSt'.
 * \remark generated
 */
class BmsHVILSt final {
 public:
  /*!
   * \brief Return/output parameters of service field 'BmsHVILSt'
   */
  using Output = ::gwm::pt_batterypacksys::batterypacksys_idt::BmsHVILSt_Enum_Idt;
};


  /*!
 * \brief A class for field method 'BmsHVILSt'Get used for type definitions.
 */
class BmsHVILStGet final {
 public:
  /*!
   * \brief Struct representing the field input data.
   */
  struct Input {};

  /*!
   * \brief Struct representing the field output value.
   */
  struct Output {
    /*!
     * \brief Reference of field value (/DataTypes/ImplementationDataTypes/BmsHVILSt_Enum_Idt)
     */
    ::gwm::pt_batterypacksys::batterypacksys_idt::BmsHVILSt_Enum_Idt out_val;
  };
};


/*!
 * \brief Data class for service field 'BmsIsoMeasSt'.
 * \remark generated
 */
class BmsIsoMeasSt final {
 public:
  /*!
   * \brief Return/output parameters of service field 'BmsIsoMeasSt'
   */
  using Output = ::gwm::pt_batterypacksys::batterypacksys_idt::BmsIsoMeasSt_Enum_Idt;
};


  /*!
 * \brief A class for field method 'BmsIsoMeasSt'Get used for type definitions.
 */
class BmsIsoMeasStGet final {
 public:
  /*!
   * \brief Struct representing the field input data.
   */
  struct Input {};

  /*!
   * \brief Struct representing the field output value.
   */
  struct Output {
    /*!
     * \brief Reference of field value (/DataTypes/ImplementationDataTypes/BmsIsoMeasSt_Enum_Idt)
     */
    ::gwm::pt_batterypacksys::batterypacksys_idt::BmsIsoMeasSt_Enum_Idt out_val;
  };
};


/*!
 * \brief Data class for service field 'BmsSt'.
 * \remark generated
 */
class BmsSt final {
 public:
  /*!
   * \brief Return/output parameters of service field 'BmsSt'
   */
  using Output = ::gwm::pt_batterypacksys::batterypacksys_idt::BmsSt_Enum_Idt;
};


  /*!
 * \brief A class for field method 'BmsSt'Get used for type definitions.
 */
class BmsStGet final {
 public:
  /*!
   * \brief Struct representing the field input data.
   */
  struct Input {};

  /*!
   * \brief Struct representing the field output value.
   */
  struct Output {
    /*!
     * \brief Reference of field value (/DataTypes/ImplementationDataTypes/BmsSt_Enum_Idt)
     */
    ::gwm::pt_batterypacksys::batterypacksys_idt::BmsSt_Enum_Idt out_val;
  };
};


/*!
 * \brief Data class for service field 'HVBattSOCLim'.
 * \remark generated
 */
class HVBattSOCLim final {
 public:
  /*!
   * \brief Return/output parameters of service field 'HVBattSOCLim'
   */
  using Output = ::gwm::pt_batterypacksys::batterypacksys_idt::HVBattSOCLim_Enum_Idt;
};


  /*!
 * \brief A class for field method 'HVBattSOCLim'Get used for type definitions.
 */
class HVBattSOCLimGet final {
 public:
  /*!
   * \brief Struct representing the field input data.
   */
  struct Input {};

  /*!
   * \brief Struct representing the field output value.
   */
  struct Output {
    /*!
     * \brief Reference of field value (/DataTypes/ImplementationDataTypes/HVBattSOCLim_Enum_Idt)
     */
    ::gwm::pt_batterypacksys::batterypacksys_idt::HVBattSOCLim_Enum_Idt out_val;
  };
};


/*!
 * \brief Data class for service field 'HVMaiSwtSt'.
 * \remark generated
 */
class HVMaiSwtSt final {
 public:
  /*!
   * \brief Return/output parameters of service field 'HVMaiSwtSt'
   */
  using Output = ::gwm::pt_batterypacksys::batterypacksys_idt::HVMaiSwtSt_Struct_Idt;
};


  /*!
 * \brief A class for field method 'HVMaiSwtSt'Get used for type definitions.
 */
class HVMaiSwtStGet final {
 public:
  /*!
   * \brief Struct representing the field input data.
   */
  struct Input {};

  /*!
   * \brief Struct representing the field output value.
   */
  struct Output {
    /*!
     * \brief Reference of field value (/DataTypes/ImplementationDataTypes/HVMaiSwtSt_Struct_Idt)
     */
    ::gwm::pt_batterypacksys::batterypacksys_idt::HVMaiSwtSt_Struct_Idt out_val;
  };
};


}  // namespace fields
}  // namespace internal

}  //  namespace bmsstsrv_si
}  //  namespace pt_batterypacksys
}  //  namespace gwm

#endif  // DENPENDENCYSRVEXE_INCLUDE_GWM_PT_BATTERYPACKSYS_BMSSTSRV_SI_SI_BMSSTSRV_TYPES_H_

