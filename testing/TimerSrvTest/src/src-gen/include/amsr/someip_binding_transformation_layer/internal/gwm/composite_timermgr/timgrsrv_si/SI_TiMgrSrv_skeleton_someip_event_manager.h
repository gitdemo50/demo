/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  TiMgrSrvExe/include/amsr/someip_binding_transformation_layer/internal/gwm/composite_timermgr/timgrsrv_si/SI_TiMgrSrv_skeleton_someip_event_manager.h
 *        \brief  SOME/IP skeleton event handling for events and field notifications of service 'SI_TiMgrSrv'
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_someipbinding
 *         Commit ID: 43b33f0283d4d61385b98a3599688499abd86a75
 *********************************************************************************************************************/

#ifndef TIMGRSRVEXE_INCLUDE_AMSR_SOMEIP_BINDING_TRANSFORMATION_LAYER_INTERNAL_GWM_COMPOSITE_TIMERMGR_TIMGRSRV_SI_SI_TIMGRSRV_SKELETON_SOMEIP_EVENT_MANAGER_H_
#define TIMGRSRVEXE_INCLUDE_AMSR_SOMEIP_BINDING_TRANSFORMATION_LAYER_INTERNAL_GWM_COMPOSITE_TIMERMGR_TIMGRSRV_SI_SI_TIMGRSRV_SKELETON_SOMEIP_EVENT_MANAGER_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "amsr/someip_protocol/internal/dataprototype_serialization/gwm_composite_timermgr_timgrsrv_si/fields/serializer_AlrmClkWkeEve.h"
#include "gwm/composite_timermgr/timgrsrv_si/SI_TiMgrSrv_types.h"
#include "someip_binding/internal/e2e/e2e_wrapper.h"
#include "someip_binding/internal/inactive_session_handler.h"
#include "someip_binding/internal/session_handler.h"
#include "someip_binding_transformation_layer/internal/skeleton_event_xf.h"

namespace amsr {
namespace someip_binding_transformation_layer {
namespace internal {

// VECTOR NC AutosarC++17_10-M7.3.6, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.6_using_directive
// VECTOR NC AutosarC++17_10-M7.3.4, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.4_using_directive
using namespace vac::container::literals;  // NOLINT(build/namespaces)

namespace gwm {
namespace composite_timermgr {
namespace timgrsrv_si {


// ---- Field notifier 'AlrmClkWkeEve' -------------------------------------------

/*!
 * \brief Skeleton-side configuration struct for event 'AlrmClkWkeEve'.
 */
struct SI_TiMgrSrvSkeletonSomeIpEventConfigurationAlrmClkWkeEve {
  /*!
   * \brief SOME/IP service ID.
   */
  static constexpr ::amsr::someip_protocol::internal::ServiceId kServiceId{0x9202};

  /*!
   * \brief SOME/IP major version.
   */
  static constexpr ::amsr::someip_protocol::internal::InterfaceVersion kMajorVersion{1};

  /*!
   * \brief SOME/IP event ID.
   */
  static constexpr ::amsr::someip_protocol::internal::EventId kEventId{0x9001};
  
  /*!
   * \brief Deserializer type for this event
   */
  static constexpr  ::amsr::someipd_app_protocol::internal::MessageType kMessageType{::amsr::someipd_app_protocol::internal::MessageType::kSomeIp};

  /*!
   * \brief Datatype of the event sample.
   * \details AsrPath: /DataTypes/ImplementationDataTypes/AlrmClkWkeEve_Enum_Idt
   */
  using SampleType = ::gwm::composite_timermgr::timermgr_idt::AlrmClkWkeEve_Enum_Idt;

  /*!
   * \brief Serializer for event payload.
   */
  using PayloadSerializer = ::amsr::someip_protocol::internal::dataprototype_serializer::gwm_composite_timermgr_timgrsrv_si::fields::SerializerAlrmClkWkeEve;

  /*!
   * \brief Used session handler.
   * \details Session handling is enabled for the event.
   * \trace CREQ-SomeIpBinding-SessionHandling
   */
  using SessionHandler = ::amsr::someip_binding::internal::SessionHandler;

  /*!
   * \brief E2E profile configuration.
   * \details No E2E protection configured for the event.
   */
  using E2eProfileConfig = void;
};

/*!
 * \brief SOME/IP Skeleton event manager type for event 'AlrmClkWkeEve'.
 */
using SI_TiMgrSrvSkeletonSomeIpFieldNotifierAlrmClkWkeEve =
    ::amsr::someip_binding_transformation_layer::internal::SkeletonEventXf<SI_TiMgrSrvSkeletonSomeIpEventConfigurationAlrmClkWkeEve>;


}  // namespace internal
}  // namespace someip_binding_transformation_layer
}  // namespace amsr
}  // namespace timgrsrv_si
}  // namespace composite_timermgr
}  // namespace gwm

#endif  // TIMGRSRVEXE_INCLUDE_AMSR_SOMEIP_BINDING_TRANSFORMATION_LAYER_INTERNAL_GWM_COMPOSITE_TIMERMGR_TIMGRSRV_SI_SI_TIMGRSRV_SKELETON_SOMEIP_EVENT_MANAGER_H_

