
/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/include/gwm/platform/timer/gs2s/SI_TimerSrv_GS2S_types.h
 *        \brief  Input and output structures for methods, fields and application errors of service 'SI_TimerSrv_GS2S'
 *
 *      \details  Definition of common input-/output structs used for simplified argument / marshalling handling. For all elements like methods, events fields structs with the related in-/output arguments are generated.
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_socal
 *         Commit ID: 209b3c0234013e84e3170ee826c9a8c6022de194
 *********************************************************************************************************************/

#ifndef DENPENDENCYSRVEXE_INCLUDE_GWM_PLATFORM_TIMER_GS2S_SI_TIMERSRV_GS2S_TYPES_H_
#define DENPENDENCYSRVEXE_INCLUDE_GWM_PLATFORM_TIMER_GS2S_SI_TIMERSRV_GS2S_TYPES_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "amsr/socal/internal/handle_type.h"
#include "gwm/platform/timer/gs2s/si_timersrv_gs2s_common.h"

namespace gwm {
namespace platform {
namespace timer {
namespace gs2s {

namespace internal {

/*!
 * \brief Proxy HandleType for the Service 'SI_TimerSrv_GS2S'.
 * \trace SPEC-4980259
 */
class SI_TimerSrv_GS2SHandleType final : public ::amsr::socal::internal::HandleType {
  public:
  /*!
   * \brief Inherit constructor.
   */
  using HandleType::HandleType;
};

namespace methods {

/*!
 * \brief A class for service method 'TimerReq' used for type definitions.
 */
class TimerReq final {
 public:
  /*!
   * \brief Struct representing all output arguments of the service method.
   */
  struct Output {
    /*!
     * \brief Reference of output argument 'Response' (/S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/TimerSet_Response_Enum)
     */
    ::datatypes::gs2s::timersrv_gs2s::TimerSet_Response_Enum Response;
  };

  /*!
   * \brief Struct representing all input arguments of the service method.
   */
  struct Input {
    /*!
     * \brief Reference of input argument 'TimerSet' (/S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/TimerSet_Integer)
     */
    ::datatypes::gs2s::timersrv_gs2s::TimerSet_Integer TimerSet;
    /*!
     * \brief Reference of input argument 'StartPoint' (/S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/StartPoint_Enum)
     */
    ::datatypes::gs2s::timersrv_gs2s::StartPoint_Enum StartPoint;
    /*!
     * \brief Reference of input argument 'TimerType' (/S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/TimerType_Enum)
     */
    ::datatypes::gs2s::timersrv_gs2s::TimerType_Enum TimerType;
    /*!
     * \brief Reference of input argument 'SleepType' (/S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/SleepType_Enum)
     */
    ::datatypes::gs2s::timersrv_gs2s::SleepType_Enum SleepType;
  };
};

}  // namespace methods

namespace fields {

/*!
 * \brief Data class for service field 'TimerSt'.
 * \remark generated
 */
class TimerSt final {
 public:
  /*!
   * \brief Return/output parameters of service field 'TimerSt'
   */
  using Output = ::datatypes::gs2s::timersrv_gs2s::TimerSt_Struct;
};


  /*!
 * \brief A class for field method 'TimerSt'Get used for type definitions.
 */
class TimerStGet final {
 public:
  /*!
   * \brief Struct representing the field input data.
   */
  struct Input {};

  /*!
   * \brief Struct representing the field output value.
   */
  struct Output {
    /*!
     * \brief Reference of field value (/S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/TimerSt_Struct)
     */
    ::datatypes::gs2s::timersrv_gs2s::TimerSt_Struct out_val;
  };
};


}  // namespace fields
}  // namespace internal

}  //  namespace gs2s
}  //  namespace timer
}  //  namespace platform
}  //  namespace gwm

#endif  // DENPENDENCYSRVEXE_INCLUDE_GWM_PLATFORM_TIMER_GS2S_SI_TIMERSRV_GS2S_TYPES_H_

