/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  TimerSrvExe/include/amsr/someip_binding_transformation_layer/internal/gwm/platform/timer/gs2s/SI_TimerSrv_GS2S_proxy_someip_event_manager.h
 *        \brief  SOME/IP proxy event handling for events and field notifications of service 'SI_TimerSrv_GS2S'
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_someipbinding
 *         Commit ID: 43b33f0283d4d61385b98a3599688499abd86a75
 *********************************************************************************************************************/

#ifndef TIMERSRVEXE_INCLUDE_AMSR_SOMEIP_BINDING_TRANSFORMATION_LAYER_INTERNAL_GWM_PLATFORM_TIMER_GS2S_SI_TIMERSRV_GS2S_PROXY_SOMEIP_EVENT_MANAGER_H_
#define TIMERSRVEXE_INCLUDE_AMSR_SOMEIP_BINDING_TRANSFORMATION_LAYER_INTERNAL_GWM_PLATFORM_TIMER_GS2S_SI_TIMERSRV_GS2S_PROXY_SOMEIP_EVENT_MANAGER_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "amsr/someip_protocol/internal/dataprototype_deserialization/gwm_platform_timer_gs2s/fields/deserializer_TimerSt.h"
#include "gwm/platform/timer/gs2s/si_timersrv_gs2s_common.h"
#include "someip-protocol/internal/marshalling.h"
#include "someip_binding/internal/e2e/e2e_wrapper.h"
#include "someip_binding_transformation_layer/internal/proxy_event_xf.h"

namespace amsr {
namespace someip_binding_transformation_layer {
namespace internal {

// VECTOR NC AutosarC++17_10-M7.3.6, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.6_using_directive
// VECTOR NC AutosarC++17_10-M7.3.4, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.4_using_directive
using namespace vac::container::literals;  // NOLINT(build/namespaces)

namespace gwm {
namespace platform {
namespace timer {
namespace gs2s {

/*!
 * \brief Configuration for Event 'TimerSt'.
 */
struct SI_TimerSrv_GS2SProxySomeIpEventConfigurationTimerSt {
  /*!
   * \brief Event Short name.
   */
  static constexpr vac::container::CStringView kEventName{"TimerSt"_sv};

  /*!
   * \brief Generated SOME/IP related service ID.
   */
  static constexpr ::amsr::someip_protocol::internal::ServiceId kServiceId{0x3AE7U};

  /*!
   * \brief SOME/IP event identifier for this event.
   */
  static constexpr ::amsr::someip_protocol::internal::EventId kEventId{0x9001U};
  
  /*!
   * \brief Message type for this event
   */
  static constexpr  ::amsr::someipd_app_protocol::internal::MessageType kMessageType{::amsr::someipd_app_protocol::internal::MessageType::kSomeIp};

  /*!
   * \brief true if this event corresponds to Field notifier, false otherwise.
   */
  static constexpr bool kIsFieldEvent{true};

  /*!
   * \brief Datatype of the event sample.
   */
  using SampleType = ::datatypes::gs2s::timersrv_gs2s::TimerSt_Struct;

  /*!
   * \brief Deserializer for event payload.
   */
  using PayloadDeserializerType = ::amsr::someip_protocol::internal::dataprototype_deserializer::gwm_platform_timer_gs2s::fields::DeserializerTimerSt;

  /*!
   * \brief E2E profile configuration
   */
  using E2eProfileConfig = void; // No E2E

  /*!
   * \brief Flag to indicate if E2E check shall be done.
   *        true = NO E2E Check (skip E2E header), false (default) = Do E2E check.
   */
  static bool constexpr kE2eCheckDisabled{false};
};

/*!
 * \brief Alias for templated ProxyEventBackend used by this event
 */
using SI_TimerSrv_GS2SProxySomeIpEventBackendTimerSt = ::amsr::someip_binding_transformation_layer::internal::ProxyEventXf<
              SI_TimerSrv_GS2SProxySomeIpEventConfigurationTimerSt>;


}  // namespace internal
}  // namespace someip_binding_transformation_layer
}  // namespace amsr
}  // namespace gs2s
}  // namespace timer
}  // namespace platform
}  // namespace gwm


#endif  // TIMERSRVEXE_INCLUDE_AMSR_SOMEIP_BINDING_TRANSFORMATION_LAYER_INTERNAL_GWM_PLATFORM_TIMER_GS2S_SI_TIMERSRV_GS2S_PROXY_SOMEIP_EVENT_MANAGER_H_

