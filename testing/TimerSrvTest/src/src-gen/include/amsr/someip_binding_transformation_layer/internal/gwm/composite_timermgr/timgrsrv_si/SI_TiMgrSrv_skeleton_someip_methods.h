/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  TiMgrSrvExe/include/amsr/someip_binding_transformation_layer/internal/gwm/composite_timermgr/timgrsrv_si/SI_TiMgrSrv_skeleton_someip_methods.h
 *        \brief  SOME/IP skeleton method de- /serialization handling for methods and field methods of service 'SI_TiMgrSrv'
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_someipbinding
 *         Commit ID: 43b33f0283d4d61385b98a3599688499abd86a75
 *********************************************************************************************************************/

#ifndef TIMGRSRVEXE_INCLUDE_AMSR_SOMEIP_BINDING_TRANSFORMATION_LAYER_INTERNAL_GWM_COMPOSITE_TIMERMGR_TIMGRSRV_SI_SI_TIMGRSRV_SKELETON_SOMEIP_METHODS_H_
#define TIMGRSRVEXE_INCLUDE_AMSR_SOMEIP_BINDING_TRANSFORMATION_LAYER_INTERNAL_GWM_COMPOSITE_TIMERMGR_TIMGRSRV_SI_SI_TIMGRSRV_SKELETON_SOMEIP_METHODS_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include <memory>
#include "amsr/someip_protocol/internal/method_deserialization/gwm_composite_timermgr_timgrsrv_si/fields/deserializer_Request_AlrmClkWkeEveGet.h"
#include "amsr/someip_protocol/internal/method_deserialization/gwm_composite_timermgr_timgrsrv_si/methods/deserializer_Request_AlarmClkCncl.h"
#include "amsr/someip_protocol/internal/method_deserialization/gwm_composite_timermgr_timgrsrv_si/methods/deserializer_Request_AlrmClkReq.h"
#include "amsr/someip_protocol/internal/method_serialization/gwm_composite_timermgr_timgrsrv_si/fields/serializer_Response_AlrmClkWkeEveGet.h"
#include "amsr/someip_protocol/internal/method_serialization/gwm_composite_timermgr_timgrsrv_si/methods/serializer_Response_AlarmClkCncl.h"
#include "amsr/someip_protocol/internal/method_serialization/gwm_composite_timermgr_timgrsrv_si/methods/serializer_Response_AlrmClkReq.h"
#include "ara/core/result.h"
#include "gwm/composite_timermgr/timgrsrv_si/SI_TiMgrSrv_types.h"
#include "gwm/composite_timermgr/timgrsrv_si/si_timgrsrv_skeleton.h"
#include "osabstraction/io/io_buffer.h"
#include "someip-protocol/internal/message.h"
#include "someip_binding_transformation_layer/internal/methods/skeleton_method_xf.h"
#include "someip_binding_transformation_layer/internal/methods/skeleton_response_handler.h"
#include "vac/container/c_string_view.h"

namespace amsr {
namespace someip_binding_transformation_layer {
namespace internal {
namespace gwm {
namespace composite_timermgr {
namespace timgrsrv_si {

// Forward-declaration for back-reference
class SI_TiMgrSrvSkeletonSomeIpBinding;


namespace methods {

/*!
 * \brief Forward-declaration for back-reference
 */
class SkeletonAlarmClkCnclAsyncRequest;

namespace alarmclkcncl {

// VECTOR NC AutosarC++17_10-M7.3.6, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.6_using_directive
// VECTOR NC AutosarC++17_10-M7.3.4, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.4_using_directive
using namespace vac::container::literals;  // NOLINT(build/namespaces)

/*!
 * \brief Struct combining all required types/values for creating the method manager.
 *
 * \remark generated
 */
struct SkeletonConfiguration final {

/*!
 * \brief Method name string.
 */
static constexpr vac::container::CStringView kName{"AlarmClkCncl"_sv};

/*!
 * \brief Method Id.
 */
static constexpr ::amsr::someip_protocol::internal::MethodId kMethodId{0x2U};

/*!
 * \brief Skeleton SomeIp Binding.
 */
using SkeletonSomeIpBinding = SI_TiMgrSrvSkeletonSomeIpBinding;

/*!
 * \brief Method Input struct.
 */
using Input = ::gwm::composite_timermgr::timgrsrv_si::internal::methods::AlarmClkCncl::Input;

/*!
 * \brief Async request type.
 */
using AsyncRequest = SkeletonAlarmClkCnclAsyncRequest;

/*!
 * \brief Method Request deserializer.
 */
using MethodRequestDeserializer = ::amsr::someip_protocol::internal::method_deserializer::gwm_composite_timermgr_timgrsrv_si::methods::DeserializerRequestAlarmClkCncl;

/*!
 * \brief Method response serializer.
 */
using MethodResponseSerializer = ::amsr::someip_protocol::internal::method_serializer::gwm_composite_timermgr_timgrsrv_si::methods::SerializerResponseOkAlarmClkCncl;
};

/*!
 * \brief Method manager configuration class.
 *
 * \remark generated
 */
using SkeletonAlarmClkCnclConfiguration = ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonMethodXfConfiguration<SkeletonConfiguration::kMethodId, SkeletonConfiguration::SkeletonSomeIpBinding, SkeletonConfiguration::Input, SkeletonConfiguration::AsyncRequest, SkeletonConfiguration::MethodRequestDeserializer>;

} // namespace alarmclkcncl

/*!
 * \brief SOME/IP Skeleton method class for method 'AlarmClkCncl'.
 * \details Handles SOME/IP de-/serialization.
 * \remark generated
 */
using SkeletonAlarmClkCncl = ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonMethodXf<alarmclkcncl::SkeletonAlarmClkCnclConfiguration>;

/*!
 * \brief For each method request a task of this type is assembled and is passed to the skeleton's frontend,
 *        where it is being executed asynchronously according to the chosen processing mode.
 */
class SkeletonAlarmClkCnclAsyncRequest : public ::amsr::socal::internal::PendingRequest {
 public:
  /*!
   * \brief At initialization store all relevant information, to be able to invoke the method call
   *        and route the response to the binding-related part again.
   * \param[in] skeleton A pointer to the skeleton frontend for calling the concrete method asynchronously in a separate
   *                     worker thread.
   * \param[in] skeleton_method A pointer to the skeleton method class needed to send the response after processing the
   *                            method request.
   * \param[in] header The SOME/IP header which must be stored for a possible response
   *                   (in case this method is not fire & forget).
   * \param[in] packet Serialized Method Request [SOME/IP Header + Payload].
   * \pre -
   * \context     Reactor
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  SkeletonAlarmClkCnclAsyncRequest(::gwm::composite_timermgr::timgrsrv_si::skeleton::SI_TiMgrSrvSkeleton* skeleton,
      SkeletonAlarmClkCncl& skeleton_method,
      ::amsr::someip_protocol::internal::SomeIpMessageHeader const& header,
      ::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet)
      : ::amsr::socal::internal::PendingRequest{skeleton},
        skeleton_{skeleton},
        response_handler_{skeleton_method},
        header_{header},
        packet_{std::move(packet)} {}

  /*!
   * \brief   Operator gets called when method invocation is planned in the frontend.
   * \details It shall be called only once for each instance.
   * \pre -
   * \context     Callback
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   *
   */
  void operator()() override {
    // VECTOR Next Line AutosarC++17_10-A18.5.8: MD_SOMEIPBINDING_AutosarC++17_10-A18.5.8_Local_object_allocated_in_the_heap
    std::unique_ptr<Input> const input{std::make_unique<Input>()};
    bool const deserialization_ok{DeserializeInput(packet_.get(), *input)};
    if (deserialization_ok) {
    ::gwm::composite_timermgr::timermgr_idt::AlrmClk_ClntID_Enum_Idt const& arg_ClntID{input->ClntID};

    ara::core::Result<::gwm::composite_timermgr::timgrsrv_si::internal::methods::AlarmClkCncl::Output> result{skeleton_->AlarmClkCncl(arg_ClntID).GetResult()};
    if (result.HasValue()) {
      response_handler_.SerializeAndSendMethodResponse<Serializer>(header_, result.Value());
    } else {
      response_handler_.SerializeAndSendApplicationErrorMethodResponse(header_, result.Error());
    }
    } else { // Deserialization failed
      response_handler_.SendErrorResponse(header_,
                                          static_cast<::amsr::someip_protocol::internal::ReturnCode>(::amsr::someip_protocol::internal::SomeIpReturnCode::kMalformedMessage));
    }
  }

 private:
  /*!
   * \brief Serialization class of the method response.
   */
  using Serializer = alarmclkcncl::SkeletonConfiguration::MethodResponseSerializer;

  /*!
   * \brief Pointer to the skeleton to invoke the method request call.
   */
  using Input = alarmclkcncl::SkeletonConfiguration::Input;

  /*!
   * \brief Deserialization class of the method request.
   */
  using Deserializer = alarmclkcncl::SkeletonConfiguration::MethodRequestDeserializer;

  /*!
   * \brief Deserialize the given method request.
   * \param[in]  serialized_sample  Serialized SOME/IP Method Request [SOME/IP header + Payload].
   * \param[out] input              The deserialized method request arguments will be written into this param.
   * \return     true               If the deserialization succeeded.
   *             false              If an error occurred during deserialization.
   * \pre -
   * \context     Callback
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  inline static bool DeserializeInput(::vac::memory::MemoryBuffer<osabstraction::io::MutableIOBuffer> const* serialized_sample,
                                      Input& input) {
    // Prepare Reader
    ::vac::memory::MemoryBuffer<osabstraction::io::MutableIOBuffer>::MemoryBufferView packet_view{serialized_sample->GetView(0U)};
    ::amsr::someip_protocol::internal::deserialization::BufferView const body_view{
        // VECTOR Next Line AutosarC++17_10-M5.2.8:MD_SOMEIPBINDING_AutosarC++17_10-M5.2.8_conv_from_voidp
        static_cast<std::uint8_t*>(packet_view[0U].base_pointer), serialized_sample->size()};

    // Skip the header
    ::amsr::someip_protocol::internal::deserialization::BufferView const buffer_view{body_view.subspan(
        ::amsr::someip_protocol::internal::kHeaderSize, body_view.size() - ::amsr::someip_protocol::internal::kHeaderSize)};

    // Deserialize Payload
    ::amsr::someip_protocol::internal::deserialization::Reader reader{buffer_view};
    return Deserializer::Deserialize(reader, input);
  }

  /*!
   * \brief Pointer to the skeleton to invoke the method request call.
   */
  ::gwm::composite_timermgr::timgrsrv_si::skeleton::SI_TiMgrSrvSkeleton* skeleton_;

  /*!
   * \brief Binding-related handler to serialize and send a response right after the method call.
   */
  ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonResponseHandler<SkeletonAlarmClkCncl> response_handler_;

  /*!
   * \brief Buffer the SOME/IP header from the request for the response with the corresponding client ID and session ID.
   */
  ::amsr::someip_protocol::internal::SomeIpMessageHeader const header_;

  /*!
   * \brief Serialized Method Request [SOME/IP Header + Payload].
   */
  ::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet_;
};


}  // namespace methods


namespace methods {

/*!
 * \brief Forward-declaration for back-reference
 */
class SkeletonAlrmClkReqAsyncRequest;

namespace alrmclkreq {

// VECTOR NC AutosarC++17_10-M7.3.6, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.6_using_directive
// VECTOR NC AutosarC++17_10-M7.3.4, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.4_using_directive
using namespace vac::container::literals;  // NOLINT(build/namespaces)

/*!
 * \brief Struct combining all required types/values for creating the method manager.
 *
 * \remark generated
 */
struct SkeletonConfiguration final {

/*!
 * \brief Method name string.
 */
static constexpr vac::container::CStringView kName{"AlrmClkReq"_sv};

/*!
 * \brief Method Id.
 */
static constexpr ::amsr::someip_protocol::internal::MethodId kMethodId{0x1U};

/*!
 * \brief Skeleton SomeIp Binding.
 */
using SkeletonSomeIpBinding = SI_TiMgrSrvSkeletonSomeIpBinding;

/*!
 * \brief Method Input struct.
 */
using Input = ::gwm::composite_timermgr::timgrsrv_si::internal::methods::AlrmClkReq::Input;

/*!
 * \brief Async request type.
 */
using AsyncRequest = SkeletonAlrmClkReqAsyncRequest;

/*!
 * \brief Method Request deserializer.
 */
using MethodRequestDeserializer = ::amsr::someip_protocol::internal::method_deserializer::gwm_composite_timermgr_timgrsrv_si::methods::DeserializerRequestAlrmClkReq;

/*!
 * \brief Method response serializer.
 */
using MethodResponseSerializer = ::amsr::someip_protocol::internal::method_serializer::gwm_composite_timermgr_timgrsrv_si::methods::SerializerResponseOkAlrmClkReq;
};

/*!
 * \brief Method manager configuration class.
 *
 * \remark generated
 */
using SkeletonAlrmClkReqConfiguration = ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonMethodXfConfiguration<SkeletonConfiguration::kMethodId, SkeletonConfiguration::SkeletonSomeIpBinding, SkeletonConfiguration::Input, SkeletonConfiguration::AsyncRequest, SkeletonConfiguration::MethodRequestDeserializer>;

} // namespace alrmclkreq

/*!
 * \brief SOME/IP Skeleton method class for method 'AlrmClkReq'.
 * \details Handles SOME/IP de-/serialization.
 * \remark generated
 */
using SkeletonAlrmClkReq = ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonMethodXf<alrmclkreq::SkeletonAlrmClkReqConfiguration>;

/*!
 * \brief For each method request a task of this type is assembled and is passed to the skeleton's frontend,
 *        where it is being executed asynchronously according to the chosen processing mode.
 */
class SkeletonAlrmClkReqAsyncRequest : public ::amsr::socal::internal::PendingRequest {
 public:
  /*!
   * \brief At initialization store all relevant information, to be able to invoke the method call
   *        and route the response to the binding-related part again.
   * \param[in] skeleton A pointer to the skeleton frontend for calling the concrete method asynchronously in a separate
   *                     worker thread.
   * \param[in] skeleton_method A pointer to the skeleton method class needed to send the response after processing the
   *                            method request.
   * \param[in] header The SOME/IP header which must be stored for a possible response
   *                   (in case this method is not fire & forget).
   * \param[in] packet Serialized Method Request [SOME/IP Header + Payload].
   * \pre -
   * \context     Reactor
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  SkeletonAlrmClkReqAsyncRequest(::gwm::composite_timermgr::timgrsrv_si::skeleton::SI_TiMgrSrvSkeleton* skeleton,
      SkeletonAlrmClkReq& skeleton_method,
      ::amsr::someip_protocol::internal::SomeIpMessageHeader const& header,
      ::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet)
      : ::amsr::socal::internal::PendingRequest{skeleton},
        skeleton_{skeleton},
        response_handler_{skeleton_method},
        header_{header},
        packet_{std::move(packet)} {}

  /*!
   * \brief   Operator gets called when method invocation is planned in the frontend.
   * \details It shall be called only once for each instance.
   * \pre -
   * \context     Callback
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   *
   */
  void operator()() override {
    // VECTOR Next Line AutosarC++17_10-A18.5.8: MD_SOMEIPBINDING_AutosarC++17_10-A18.5.8_Local_object_allocated_in_the_heap
    std::unique_ptr<Input> const input{std::make_unique<Input>()};
    bool const deserialization_ok{DeserializeInput(packet_.get(), *input)};
    if (deserialization_ok) {
    ::gwm::composite_timermgr::timermgr_idt::AlrmClk_ClntID_Enum_Idt const& arg_ClntID{input->ClntID};
    ::gwm::composite_timermgr::timermgr_idt::AlrmClkFrq_Enum_Idt const& arg_AlrmClkFrq{input->AlrmClkFrq};
    ::gwm::composite_timermgr::timermgr_idt::AlrmClkBgnTi_Struct_Idt const& arg_AlrmClkBgnTi{input->AlrmClkBgnTi};
    ::gwm::composite_timermgr::timermgr_idt::AlrmClkEndTi_Struct_Idt const& arg_AlrmClkEndTi{input->AlrmClkEndTi};

    ara::core::Result<::gwm::composite_timermgr::timgrsrv_si::internal::methods::AlrmClkReq::Output> result{skeleton_->AlrmClkReq(arg_ClntID,arg_AlrmClkFrq,arg_AlrmClkBgnTi,arg_AlrmClkEndTi).GetResult()};
    if (result.HasValue()) {
      response_handler_.SerializeAndSendMethodResponse<Serializer>(header_, result.Value());
    } else {
      response_handler_.SerializeAndSendApplicationErrorMethodResponse(header_, result.Error());
    }
    } else { // Deserialization failed
      response_handler_.SendErrorResponse(header_,
                                          static_cast<::amsr::someip_protocol::internal::ReturnCode>(::amsr::someip_protocol::internal::SomeIpReturnCode::kMalformedMessage));
    }
  }

 private:
  /*!
   * \brief Serialization class of the method response.
   */
  using Serializer = alrmclkreq::SkeletonConfiguration::MethodResponseSerializer;

  /*!
   * \brief Pointer to the skeleton to invoke the method request call.
   */
  using Input = alrmclkreq::SkeletonConfiguration::Input;

  /*!
   * \brief Deserialization class of the method request.
   */
  using Deserializer = alrmclkreq::SkeletonConfiguration::MethodRequestDeserializer;

  /*!
   * \brief Deserialize the given method request.
   * \param[in]  serialized_sample  Serialized SOME/IP Method Request [SOME/IP header + Payload].
   * \param[out] input              The deserialized method request arguments will be written into this param.
   * \return     true               If the deserialization succeeded.
   *             false              If an error occurred during deserialization.
   * \pre -
   * \context     Callback
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  inline static bool DeserializeInput(::vac::memory::MemoryBuffer<osabstraction::io::MutableIOBuffer> const* serialized_sample,
                                      Input& input) {
    // Prepare Reader
    ::vac::memory::MemoryBuffer<osabstraction::io::MutableIOBuffer>::MemoryBufferView packet_view{serialized_sample->GetView(0U)};
    ::amsr::someip_protocol::internal::deserialization::BufferView const body_view{
        // VECTOR Next Line AutosarC++17_10-M5.2.8:MD_SOMEIPBINDING_AutosarC++17_10-M5.2.8_conv_from_voidp
        static_cast<std::uint8_t*>(packet_view[0U].base_pointer), serialized_sample->size()};

    // Skip the header
    ::amsr::someip_protocol::internal::deserialization::BufferView const buffer_view{body_view.subspan(
        ::amsr::someip_protocol::internal::kHeaderSize, body_view.size() - ::amsr::someip_protocol::internal::kHeaderSize)};

    // Deserialize Payload
    ::amsr::someip_protocol::internal::deserialization::Reader reader{buffer_view};
    return Deserializer::Deserialize(reader, input);
  }

  /*!
   * \brief Pointer to the skeleton to invoke the method request call.
   */
  ::gwm::composite_timermgr::timgrsrv_si::skeleton::SI_TiMgrSrvSkeleton* skeleton_;

  /*!
   * \brief Binding-related handler to serialize and send a response right after the method call.
   */
  ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonResponseHandler<SkeletonAlrmClkReq> response_handler_;

  /*!
   * \brief Buffer the SOME/IP header from the request for the response with the corresponding client ID and session ID.
   */
  ::amsr::someip_protocol::internal::SomeIpMessageHeader const header_;

  /*!
   * \brief Serialized Method Request [SOME/IP Header + Payload].
   */
  ::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet_;
};


}  // namespace methods


namespace fields {

/*!
 * \brief Forward-declaration for back-reference
 */
class SkeletonAlrmClkWkeEveGetAsyncRequest;

namespace alrmclkwkeeveget {

// VECTOR NC AutosarC++17_10-M7.3.6, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.6_using_directive
// VECTOR NC AutosarC++17_10-M7.3.4, VectorC++-V5.0.1: MD_SOMEIPBINDING_AutosarC++17_10-M7.3.4_using_directive
using namespace vac::container::literals;  // NOLINT(build/namespaces)

/*!
 * \brief Struct combining all required types/values for creating the method manager.
 *
 * \remark generated
 */
struct SkeletonConfiguration final {

/*!
 * \brief Method name string.
 */
static constexpr vac::container::CStringView kName{"AlrmClkWkeEveGet"_sv};

/*!
 * \brief Method Id.
 */
static constexpr ::amsr::someip_protocol::internal::MethodId kMethodId{0x4001U};

/*!
 * \brief Skeleton SomeIp Binding.
 */
using SkeletonSomeIpBinding = SI_TiMgrSrvSkeletonSomeIpBinding;

/*!
 * \brief Method Input struct.
 */
using Input = ::gwm::composite_timermgr::timgrsrv_si::internal::fields::AlrmClkWkeEveGet::Input;

/*!
 * \brief Async request type.
 */
using AsyncRequest = SkeletonAlrmClkWkeEveGetAsyncRequest;

/*!
 * \brief Method Request deserializer.
 */
using MethodRequestDeserializer = ::amsr::someip_protocol::internal::method_deserializer::gwm_composite_timermgr_timgrsrv_si::fields::DeserializerRequestAlrmClkWkeEveGet;

/*!
 * \brief Method response serializer.
 */
using MethodResponseSerializer = ::amsr::someip_protocol::internal::method_serializer::gwm_composite_timermgr_timgrsrv_si::fields::SerializerResponseOkAlrmClkWkeEveGet;
};

/*!
 * \brief Method manager configuration class.
 *
 * \remark generated
 */
using SkeletonAlrmClkWkeEveGetConfiguration = ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonMethodXfConfiguration<SkeletonConfiguration::kMethodId, SkeletonConfiguration::SkeletonSomeIpBinding, SkeletonConfiguration::Input, SkeletonConfiguration::AsyncRequest, SkeletonConfiguration::MethodRequestDeserializer>;

} // namespace alrmclkwkeeveget

/*!
 * \brief SOME/IP Skeleton method class for method 'AlrmClkWkeEveGet'.
 * \details Handles SOME/IP de-/serialization.
 * \remark generated
 */
using SkeletonAlrmClkWkeEveGet = ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonMethodXf<alrmclkwkeeveget::SkeletonAlrmClkWkeEveGetConfiguration>;

/*!
 * \brief For each method request a task of this type is assembled and is passed to the skeleton's frontend,
 *        where it is being executed asynchronously according to the chosen processing mode.
 */
class SkeletonAlrmClkWkeEveGetAsyncRequest : public ::amsr::socal::internal::PendingRequest {
 public:
  /*!
   * \brief At initialization store all relevant information, to be able to invoke the method call
   *        and route the response to the binding-related part again.
   * \param[in] skeleton A pointer to the skeleton frontend for calling the concrete method asynchronously in a separate
   *                     worker thread.
   * \param[in] skeleton_method A pointer to the skeleton method class needed to send the response after processing the
   *                            method request.
   * \param[in] header The SOME/IP header which must be stored for a possible response
   *                   (in case this method is not fire & forget).
   * \param[in] packet Serialized Method Request [SOME/IP Header + Payload].
   * \pre -
   * \context     Reactor
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   */
  SkeletonAlrmClkWkeEveGetAsyncRequest(::gwm::composite_timermgr::timgrsrv_si::skeleton::SI_TiMgrSrvSkeleton* skeleton,
      SkeletonAlrmClkWkeEveGet& skeleton_method,
      ::amsr::someip_protocol::internal::SomeIpMessageHeader const& header,
      ::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet)
      : ::amsr::socal::internal::PendingRequest{skeleton},
        skeleton_{skeleton},
        response_handler_{skeleton_method},
        header_{header},
        packet_{std::move(packet)} {}

  /*!
   * \brief   Operator gets called when method invocation is planned in the frontend.
   * \details It shall be called only once for each instance.
   * \pre -
   * \context     Callback
   * \threadsafe  FALSE
   * \reentrant   FALSE
   * \synchronous TRUE
   *
   */
  void operator()() override {

    ara::core::Result<::gwm::composite_timermgr::timgrsrv_si::internal::fields::AlrmClkWkeEve::Output> result{skeleton_->AlrmClkWkeEve.Get().GetResult()};
    if (result.HasValue()) {
      response_handler_.SerializeAndSendMethodResponse<Serializer>(header_, result.Value());
    } else {
      response_handler_.SerializeAndSendApplicationErrorMethodResponse(header_, result.Error());
    }
  }

 private:
  /*!
   * \brief Serialization class of the method response.
   */
  using Serializer = alrmclkwkeeveget::SkeletonConfiguration::MethodResponseSerializer;


  /*!
   * \brief Pointer to the skeleton to invoke the method request call.
   */
  ::gwm::composite_timermgr::timgrsrv_si::skeleton::SI_TiMgrSrvSkeleton* skeleton_;

  /*!
   * \brief Binding-related handler to serialize and send a response right after the method call.
   */
  ::amsr::someip_binding_transformation_layer::internal::methods::SkeletonResponseHandler<SkeletonAlrmClkWkeEveGet> response_handler_;

  /*!
   * \brief Buffer the SOME/IP header from the request for the response with the corresponding client ID and session ID.
   */
  ::amsr::someip_protocol::internal::SomeIpMessageHeader const header_;

  /*!
   * \brief Serialized Method Request [SOME/IP Header + Payload].
   */
  ::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet_;
};


}  // namespace fields


}  // namespace internal
}  // namespace someip_binding_transformation_layer
}  // namespace amsr
}  // namespace timgrsrv_si
}  // namespace composite_timermgr
}  // namespace gwm

#endif  // TIMGRSRVEXE_INCLUDE_AMSR_SOMEIP_BINDING_TRANSFORMATION_LAYER_INTERNAL_GWM_COMPOSITE_TIMERMGR_TIMGRSRV_SI_SI_TIMGRSRV_SKELETON_SOMEIP_METHODS_H_

