#include <memory>
#include <condition_variable>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "TimerBasicController.hpp"
#include "TimerSrvImpl.hpp"

namespace gwm {
namespace services {
namespace timer {
namespace skeleton {

using TimerS2SService = gwm::services::timer::proxy::TimerS2SService;

class MockTimerS2SService : public TimerS2SService {
public:
    void raiseTimer(datatypes::gs2s::timersrv_gs2s::TimerSt_Struct val) {
        proxy->TimerSt.raiseEvent(val);
    }

    void setUp() {
        TimerS2SService::doSubscription();
    }
    MOCK_METHOD(void, doSubscription, (), (override));
};

class MockController : public gwm::controller::TimerBasicController {
public:
    MockController(std::shared_ptr<TimerS2SService> service) : TimerBasicController(service) {}

    MOCK_METHOD(void,setTimerNotifier,(NotifierFn),(override));
    MOCK_METHOD(int,cancelActiveTimer,(),(override));
    MOCK_METHOD(int,requestTimer,(int),(override));
};

TEST(TimerSrvImpl, constr) {
    std::condition_variable cv;
    std::mutex cv_m;


    std::shared_ptr<MockTimerS2SService> timerS2SSvc = std::make_shared<MockTimerS2SService>();
    std::shared_ptr<MockController> cntrl = std::make_shared<MockController>(timerS2SSvc);

    EXPECT_CALL(*cntrl,setTimerNotifier(_))
        .Times(1);

    TimerSrvImpl timerSrvr{cntrl};

}

TEST(TimerSrvImpl, timerTrigger) {
    std::condition_variable cv;
    std::mutex cv_m;


    std::shared_ptr<MockTimerS2SService> timerS2SSvc = std::make_shared<MockTimerS2SService>();
    std::shared_ptr<MockController> cntrl = std::make_shared<MockController>(timerS2SSvc);

    EXPECT_CALL(*cntrl,setTimerNotifier(_))
        .WillRepeatedly([&cntrl](MockController::NotifierFn fn) {
            (*cntrl).TimerBasicController::setTimerNotifier(fn);
        });

    EXPECT_CALL((*timerS2SSvc),doSubscription())
        .WillOnce([&cv,&timerS2SSvc] {
            timerS2SSvc->setUp();
            cv.notify_all(); 
        });
    
    timerS2SSvc->start();

    TimerSrvImpl timerSrvr{cntrl};

    std::unique_lock<std::mutex> lk(cv_m);
    cv.wait_for(lk,std::chrono::seconds(1));

    datatypes::gs2s::timersrv_gs2s::TimerSt_Struct data;
    data.WakeSrcexpired = datatypes::gs2s::timersrv_gs2s::TimerSt_WakeSrcexpired_Enum::NoWakeupSourceExpired;
    timerS2SSvc->raiseTimer(data);

    ASSERT_EQ(timerSrvr.TimerSt.Get().get().WakeSrcexpired_Idt , gwm::platform_timer::timer_idt::TimerSt_Struct_Idt::WakeSrcexpired_Idt_generated_type::Timing);

    data.WakeSrcexpired = datatypes::gs2s::timersrv_gs2s::TimerSt_WakeSrcexpired_Enum::DefaultValue;
    timerS2SSvc->raiseTimer(data);

    ASSERT_EQ(timerSrvr.TimerSt.Get().get().WakeSrcexpired_Idt , gwm::platform_timer::timer_idt::TimerSt_Struct_Idt::WakeSrcexpired_Idt_generated_type::NoTimer);
     
    data.WakeSrcexpired = datatypes::gs2s::timersrv_gs2s::TimerSt_WakeSrcexpired_Enum::WakeupSourceExpired;
    timerS2SSvc->raiseTimer(data);

    ASSERT_EQ(timerSrvr.TimerSt.Get().get().WakeSrcexpired_Idt , gwm::platform_timer::timer_idt::TimerSt_Struct_Idt::WakeSrcexpired_Idt_generated_type::TimeOut);
}

TEST(TimerSrvImpl,TimerCncl) {
    std::shared_ptr<MockTimerS2SService> timerS2SSvc = std::make_shared<MockTimerS2SService>();
    std::shared_ptr<MockController> cntrl = std::make_shared<MockController>(timerS2SSvc);

    EXPECT_CALL(*cntrl,setTimerNotifier(_))
        .Times(1);

    EXPECT_CALL(*cntrl,cancelActiveTimer())
        .WillOnce(Return(0))
        .WillRepeatedly(Return(1));

    TimerSrvImpl timerSrvr{cntrl};

    timerSrvr.TimerCncl();
    timerSrvr.TimerCncl();
}

TEST(TimerSrvImpl,TimerReq) {
    std::shared_ptr<MockTimerS2SService> timerS2SSvc = std::make_shared<MockTimerS2SService>();
    std::shared_ptr<MockController> cntrl = std::make_shared<MockController>(timerS2SSvc);

    int timeVal = 100;

    EXPECT_CALL(*cntrl,setTimerNotifier(_))
        .Times(1);

    EXPECT_CALL(*cntrl,requestTimer(timeVal))
        .WillRepeatedly(Return(0));

    TimerSrvImpl timerSrvr{cntrl};

    timerSrvr.TimerReq(timeVal);
}

TEST(TimerSrvImpl,TimerReqInvalid0) {
    std::shared_ptr<MockTimerS2SService> timerS2SSvc = std::make_shared<MockTimerS2SService>();
    std::shared_ptr<MockController> cntrl = std::make_shared<MockController>(timerS2SSvc);

    int timeVal = 0;

    EXPECT_CALL(*cntrl,setTimerNotifier(_))
        .Times(1);

    EXPECT_CALL(*cntrl,requestTimer(timeVal))
        .WillRepeatedly(Return(1));

    TimerSrvImpl timerSrvr{cntrl};

    timerSrvr.TimerReq(timeVal);
}

TEST(TimerSrvImpl,TimerReqInvalidMax) {
    std::shared_ptr<MockTimerS2SService> timerS2SSvc = std::make_shared<MockTimerS2SService>();
    std::shared_ptr<MockController> cntrl = std::make_shared<MockController>(timerS2SSvc);

    int timeVal = 604801;

    EXPECT_CALL(*cntrl,setTimerNotifier(_))
        .Times(1);

    EXPECT_CALL(*cntrl,requestTimer(timeVal))
        .WillRepeatedly(Return(1));

    TimerSrvImpl timerSrvr{cntrl};

    timerSrvr.TimerReq(timeVal);
}

}}}}