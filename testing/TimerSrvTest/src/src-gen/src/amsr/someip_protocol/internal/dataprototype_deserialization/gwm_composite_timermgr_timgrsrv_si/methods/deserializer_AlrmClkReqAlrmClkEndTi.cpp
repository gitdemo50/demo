/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  TiMgrSrvExe/src/amsr/someip_protocol/internal/dataprototype_deserialization/gwm_composite_timermgr_timgrsrv_si/methods/deserializer_AlrmClkReqAlrmClkEndTi.cpp
 *        \brief  SOME/IP protocol deserializer implementation for data prototype '/ServiceInterfaces/SI_TiMgrSrv/AlrmClkReq/AlrmClkEndTi
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_someipprotocol
 *         Commit ID: 0e137b9e6356987a6d2839c8d79c286219b68d60
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "someip-protocol/internal/deserialization/deser_wrapper.h"
#include "amsr/someip_protocol/internal/dataprototype_deserialization/gwm_composite_timermgr_timgrsrv_si/methods/deserializer_AlrmClkReqAlrmClkEndTi.h"
#include "amsr/someip_protocol/internal/datatype_deserialization/gwm/composite_timermgr/timermgr_idt/deserializer_AlrmClkEndTi_Struct_Idt.h"

namespace amsr {
namespace someip_protocol {
namespace internal {
namespace dataprototype_deserializer {
namespace gwm_composite_timermgr_timgrsrv_si {
namespace methods {

deserialization::Result DeserializerAlrmClkReqAlrmClkEndTi::Deserialize(deserialization::Reader &reader, ::gwm::composite_timermgr::timermgr_idt::AlrmClkEndTi_Struct_Idt &data) {
  // Transformation properties parameter pack for data prototype /ServiceInterfaces/SI_TiMgrSrv/AlrmClkReq/AlrmClkEndTi
    using TpPackAlias = deserialization::TpPack<
        BigEndian,
        deserialization::SizeOfArrayLengthField<0>, 
        deserialization::SizeOfVectorLengthField<4>,
        deserialization::SizeOfMapLengthField<4>,
        deserialization::SizeOfStringLengthField<4>,
        deserialization::SizeOfStructLengthField<0>,
        deserialization::SizeOfUnionLengthField<4>,
        deserialization::SizeOfUnionTypeSelectorField<4>,
        deserialization::StringBomActive,
        deserialization::StringNullTerminationActive>;


  // Verify static size
  constexpr std::size_t static_size{deserialization::SomeIpProtocolGetStaticSize<
      TpPackAlias,
        // Config of struct length field (/DataTypes/ImplementationDataTypes/AlrmClkEndTi_Struct_Idt)
      deserialization::LengthSize<deserialization::Tp<TpPackAlias>::kSizeOfStructLengthField, typename deserialization::Tp<TpPackAlias>::ByteOrder>

      >(deserialization::SizeToken<::gwm::composite_timermgr::timermgr_idt::AlrmClkEndTi_Struct_Idt>{})};

  deserialization::Result result{reader.VerifySize(static_size)};
  if (result) {
    // Deserialize byte stream
    result = deserialization::SomeIpProtocolDeserialize<
      TpPackAlias,
      // Config of struct length field (/DataTypes/ImplementationDataTypes/AlrmClkEndTi_Struct_Idt)
      deserialization::LengthSize<deserialization::Tp<TpPackAlias>::kSizeOfStructLengthField, typename deserialization::Tp<TpPackAlias>::ByteOrder>

      >(reader, data);
  }

  return result;
}

}  // namespace methods
}  // namespace gwm_composite_timermgr_timgrsrv_si
}  // namespace dataprototype_deserializer
}  // namespace internal
}  // namespace someip_protocol
}  // namespace amsr

