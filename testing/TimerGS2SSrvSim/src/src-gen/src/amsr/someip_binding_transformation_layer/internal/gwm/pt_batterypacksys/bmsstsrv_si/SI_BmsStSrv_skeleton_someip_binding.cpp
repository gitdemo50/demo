// VECTOR Same Line AutosarC++17_10-A1.1.1: MD_SOMEIPBINDING_AutosarC++17_10-A1.1.1_external_identifiers
/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/src/amsr/someip_binding_transformation_layer/internal/gwm/pt_batterypacksys/bmsstsrv_si/SI_BmsStSrv_skeleton_someip_binding.cpp
 *        \brief  SOME/IP skeleton binding of service 'SI_BmsStSrv'
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_someipbinding
 *         Commit ID: 43b33f0283d4d61385b98a3599688499abd86a75
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "amsr/someip_binding_transformation_layer/internal/gwm/pt_batterypacksys/bmsstsrv_si/SI_BmsStSrv_skeleton_someip_binding.h"
#include <utility>

namespace amsr {
namespace someip_binding_transformation_layer {
namespace internal {

namespace gwm {
namespace pt_batterypacksys {
namespace bmsstsrv_si {

/*!
 * \brief Generated SOME/IP related service ID.
 */
::amsr::someip_protocol::internal::ServiceId constexpr SI_BmsStSrvSkeletonSomeIpBinding::kServiceId;

/*!
 * \brief SOME/IP major version of this service interface deployment.
 */
constexpr ::amsr::someip_protocol::internal::MajorVersion SI_BmsStSrvSkeletonSomeIpBinding::kMajorVersion;

SI_BmsStSrvSkeletonSomeIpBinding::SI_BmsStSrvSkeletonSomeIpBinding(
    ::amsr::someip_protocol::internal::InstanceId const instance_id,
    ::amsr::someip_binding::internal::ServerManagerInterface& someip_binding_server_manager,
    ::gwm::pt_batterypacksys::bmsstsrv_si::skeleton::SI_BmsStSrvSkeleton& skeleton)
    : instance_id_(instance_id),
      someip_binding_server_manager_(someip_binding_server_manager),
      skeleton_(skeleton) ,
      event_manager_BMSChrgTi_(instance_id),
      event_manager_BMSConDchrMaxPwr_(instance_id),
      event_manager_BMSConFBMaxPwr_(instance_id),
      event_manager_BMSPlsDchrMaxPwr_(instance_id),
      event_manager_BMSPlsFBMaxPwr_(instance_id),
      event_manager_BmsInletCooltT_(instance_id),
      event_manager_BmsIsoR_(instance_id),
      event_manager_BmsOutletCooltT_(instance_id),
      event_manager_CellMaxT_(instance_id),
      event_manager_CellMaxU_(instance_id),
      event_manager_CellMinT_(instance_id),
      event_manager_CellMinU_(instance_id),
      event_manager_HVBattDispySoc_(instance_id),
      event_manager_HVBattI_(instance_id),
      event_manager_HVBattInnerSoc_(instance_id),
      event_manager_HVBattSoh_(instance_id),
      event_manager_HVBattTotEgy_(instance_id),
      event_manager_HVBattU_(instance_id),
      event_manager_ModulAvrgT_(instance_id),
      field_notifier_BMSChrgSt_(instance_id),
      field_manager_BMSChrgSt_get_(*this, "BMSChrgSt" ),
      field_notifier_BMSDCChrgCnctSt_(instance_id),
      field_manager_BMSDCChrgCnctSt_get_(*this, "BMSDCChrgCnctSt" ),
      field_notifier_BMSDCChrgEndReas_(instance_id),
      field_manager_BMSDCChrgEndReas_get_(*this, "BMSDCChrgEndReas" ),
      field_notifier_BMSDchrgPrmsSt_(instance_id),
      field_manager_BMSDchrgPrmsSt_get_(*this, "BMSDchrgPrmsSt" ),
      field_notifier_BMSHeatRunawaySt_(instance_id),
      field_manager_BMSHeatRunawaySt_get_(*this, "BMSHeatRunawaySt" ),
      field_notifier_BMSInnerSOCSt_(instance_id),
      field_manager_BMSInnerSOCSt_get_(*this, "BMSInnerSOCSt" ),
      field_notifier_BMSIntelTempMgtSt_(instance_id),
      field_manager_BMSIntelTempMgtSt_get_(*this, "BMSIntelTempMgtSt" ),
      field_notifier_BMSLowTempSt_(instance_id),
      field_manager_BMSLowTempSt_get_(*this, "BMSLowTempSt" ),
      field_notifier_BMSRmtPreHeatSt_(instance_id),
      field_manager_BMSRmtPreHeatSt_get_(*this, "BMSRmtPreHeatSt" ),
      field_notifier_BMSSafCnctSt_(instance_id),
      field_manager_BMSSafCnctSt_get_(*this, "BMSSafCnctSt" ),
      field_notifier_BmsErrSt_(instance_id),
      field_manager_BmsErrSt_get_(*this, "BmsErrSt" ),
      field_notifier_BmsHVILSt_(instance_id),
      field_manager_BmsHVILSt_get_(*this, "BmsHVILSt" ),
      field_notifier_BmsIsoMeasSt_(instance_id),
      field_manager_BmsIsoMeasSt_get_(*this, "BmsIsoMeasSt" ),
      field_notifier_BmsSt_(instance_id),
      field_manager_BmsSt_get_(*this, "BmsSt" ),
      field_notifier_HVBattSOCLim_(instance_id),
      field_manager_HVBattSOCLim_get_(*this, "HVBattSOCLim" ),
      field_notifier_HVMaiSwtSt_(instance_id),
      field_manager_HVMaiSwtSt_get_(*this, "HVMaiSwtSt" ){
}

void SI_BmsStSrvSkeletonSomeIpBinding::HandleMethodRequest(
  ::amsr::someip_protocol::internal::SomeIpMessageHeader const& header,
  ::amsr::someip_protocol::internal::SomeIpMessage  packet) {
  // Based on the method id -> static dispatching to the method request/response manager
  switch (header.method_id_) {
    case fields::SkeletonBMSChrgStGet::kMethodId: {
      field_manager_BMSChrgSt_get_.HandleMethodRequest(header, std::move(packet));
      break;
    }
    case fields::SkeletonBMSDCChrgCnctStGet::kMethodId: {
      field_manager_BMSDCChrgCnctSt_get_.HandleMethodRequest(header, std::move(packet));
      break;
    }
    case fields::SkeletonBMSDCChrgEndReasGet::kMethodId: {
      field_manager_BMSDCChrgEndReas_get_.HandleMethodRequest(header, std::move(packet));
      break;
    }
    case fields::SkeletonBMSDchrgPrmsStGet::kMethodId: {
      field_manager_BMSDchrgPrmsSt_get_.HandleMethodRequest(header, std::move(packet));
      break;
    }
    case fields::SkeletonBMSHeatRunawayStGet::kMethodId: {
      field_manager_BMSHeatRunawaySt_get_.HandleMethodRequest(header, std::move(packet));
      break;
    }
    case fields::SkeletonBMSInnerSOCStGet::kMethodId: {
      field_manager_BMSInnerSOCSt_get_.HandleMethodRequest(header, std::move(packet));
      break;
    }
    case fields::SkeletonBMSIntelTempMgtStGet::kMethodId: {
      field_manager_BMSIntelTempMgtSt_get_.HandleMethodRequest(header, std::move(packet));
      break;
    }
    case fields::SkeletonBMSLowTempStGet::kMethodId: {
      field_manager_BMSLowTempSt_get_.HandleMethodRequest(header, std::move(packet));
      break;
    }
    case fields::SkeletonBMSRmtPreHeatStGet::kMethodId: {
      field_manager_BMSRmtPreHeatSt_get_.HandleMethodRequest(header, std::move(packet));
      break;
    }
    case fields::SkeletonBMSSafCnctStGet::kMethodId: {
      field_manager_BMSSafCnctSt_get_.HandleMethodRequest(header, std::move(packet));
      break;
    }
    case fields::SkeletonBmsErrStGet::kMethodId: {
      field_manager_BmsErrSt_get_.HandleMethodRequest(header, std::move(packet));
      break;
    }
    case fields::SkeletonBmsHVILStGet::kMethodId: {
      field_manager_BmsHVILSt_get_.HandleMethodRequest(header, std::move(packet));
      break;
    }
    case fields::SkeletonBmsIsoMeasStGet::kMethodId: {
      field_manager_BmsIsoMeasSt_get_.HandleMethodRequest(header, std::move(packet));
      break;
    }
    case fields::SkeletonBmsStGet::kMethodId: {
      field_manager_BmsSt_get_.HandleMethodRequest(header, std::move(packet));
      break;
    }
    case fields::SkeletonHVBattSOCLimGet::kMethodId: {
      field_manager_HVBattSOCLim_get_.HandleMethodRequest(header, std::move(packet));
      break;
    }
    case fields::SkeletonHVMaiSwtStGet::kMethodId: {
      field_manager_HVMaiSwtSt_get_.HandleMethodRequest(header, std::move(packet));
      break;
    }
    default: {
      // Method implementation is missing.
      SendErrorResponse<static_cast<::amsr::someip_protocol::internal::ReturnCode>(::amsr::someip_protocol::internal::SomeIpReturnCode::kUnknownMethod)>(header);
      break;
    }
  }
}

void SI_BmsStSrvSkeletonSomeIpBinding::SendMethodResponse(::vac::memory::MemoryBufferPtr<osabstraction::io::MutableIOBuffer> packet) {
  someip_binding_server_manager_.SendMethodResponse(instance_id_, std::move(packet));
}

// ---- Events -----------------------------------------------------------------------------------------------------

// Event 'BMSChrgTi'
SI_BmsStSrvSkeletonSomeIpEventManagerBMSChrgTi* SI_BmsStSrvSkeletonSomeIpBinding::GetEventManagerBMSChrgTi() noexcept {
  return &event_manager_BMSChrgTi_;
}

// Event 'BMSConDchrMaxPwr'
SI_BmsStSrvSkeletonSomeIpEventManagerBMSConDchrMaxPwr* SI_BmsStSrvSkeletonSomeIpBinding::GetEventManagerBMSConDchrMaxPwr() noexcept {
  return &event_manager_BMSConDchrMaxPwr_;
}

// Event 'BMSConFBMaxPwr'
SI_BmsStSrvSkeletonSomeIpEventManagerBMSConFBMaxPwr* SI_BmsStSrvSkeletonSomeIpBinding::GetEventManagerBMSConFBMaxPwr() noexcept {
  return &event_manager_BMSConFBMaxPwr_;
}

// Event 'BMSPlsDchrMaxPwr'
SI_BmsStSrvSkeletonSomeIpEventManagerBMSPlsDchrMaxPwr* SI_BmsStSrvSkeletonSomeIpBinding::GetEventManagerBMSPlsDchrMaxPwr() noexcept {
  return &event_manager_BMSPlsDchrMaxPwr_;
}

// Event 'BMSPlsFBMaxPwr'
SI_BmsStSrvSkeletonSomeIpEventManagerBMSPlsFBMaxPwr* SI_BmsStSrvSkeletonSomeIpBinding::GetEventManagerBMSPlsFBMaxPwr() noexcept {
  return &event_manager_BMSPlsFBMaxPwr_;
}

// Event 'BmsInletCooltT'
SI_BmsStSrvSkeletonSomeIpEventManagerBmsInletCooltT* SI_BmsStSrvSkeletonSomeIpBinding::GetEventManagerBmsInletCooltT() noexcept {
  return &event_manager_BmsInletCooltT_;
}

// Event 'BmsIsoR'
SI_BmsStSrvSkeletonSomeIpEventManagerBmsIsoR* SI_BmsStSrvSkeletonSomeIpBinding::GetEventManagerBmsIsoR() noexcept {
  return &event_manager_BmsIsoR_;
}

// Event 'BmsOutletCooltT'
SI_BmsStSrvSkeletonSomeIpEventManagerBmsOutletCooltT* SI_BmsStSrvSkeletonSomeIpBinding::GetEventManagerBmsOutletCooltT() noexcept {
  return &event_manager_BmsOutletCooltT_;
}

// Event 'CellMaxT'
SI_BmsStSrvSkeletonSomeIpEventManagerCellMaxT* SI_BmsStSrvSkeletonSomeIpBinding::GetEventManagerCellMaxT() noexcept {
  return &event_manager_CellMaxT_;
}

// Event 'CellMaxU'
SI_BmsStSrvSkeletonSomeIpEventManagerCellMaxU* SI_BmsStSrvSkeletonSomeIpBinding::GetEventManagerCellMaxU() noexcept {
  return &event_manager_CellMaxU_;
}

// Event 'CellMinT'
SI_BmsStSrvSkeletonSomeIpEventManagerCellMinT* SI_BmsStSrvSkeletonSomeIpBinding::GetEventManagerCellMinT() noexcept {
  return &event_manager_CellMinT_;
}

// Event 'CellMinU'
SI_BmsStSrvSkeletonSomeIpEventManagerCellMinU* SI_BmsStSrvSkeletonSomeIpBinding::GetEventManagerCellMinU() noexcept {
  return &event_manager_CellMinU_;
}

// Event 'HVBattDispySoc'
SI_BmsStSrvSkeletonSomeIpEventManagerHVBattDispySoc* SI_BmsStSrvSkeletonSomeIpBinding::GetEventManagerHVBattDispySoc() noexcept {
  return &event_manager_HVBattDispySoc_;
}

// Event 'HVBattI'
SI_BmsStSrvSkeletonSomeIpEventManagerHVBattI* SI_BmsStSrvSkeletonSomeIpBinding::GetEventManagerHVBattI() noexcept {
  return &event_manager_HVBattI_;
}

// Event 'HVBattInnerSoc'
SI_BmsStSrvSkeletonSomeIpEventManagerHVBattInnerSoc* SI_BmsStSrvSkeletonSomeIpBinding::GetEventManagerHVBattInnerSoc() noexcept {
  return &event_manager_HVBattInnerSoc_;
}

// Event 'HVBattSoh'
SI_BmsStSrvSkeletonSomeIpEventManagerHVBattSoh* SI_BmsStSrvSkeletonSomeIpBinding::GetEventManagerHVBattSoh() noexcept {
  return &event_manager_HVBattSoh_;
}

// Event 'HVBattTotEgy'
SI_BmsStSrvSkeletonSomeIpEventManagerHVBattTotEgy* SI_BmsStSrvSkeletonSomeIpBinding::GetEventManagerHVBattTotEgy() noexcept {
  return &event_manager_HVBattTotEgy_;
}

// Event 'HVBattU'
SI_BmsStSrvSkeletonSomeIpEventManagerHVBattU* SI_BmsStSrvSkeletonSomeIpBinding::GetEventManagerHVBattU() noexcept {
  return &event_manager_HVBattU_;
}

// Event 'ModulAvrgT'
SI_BmsStSrvSkeletonSomeIpEventManagerModulAvrgT* SI_BmsStSrvSkeletonSomeIpBinding::GetEventManagerModulAvrgT() noexcept {
  return &event_manager_ModulAvrgT_;
}

// ---- Fields -----------------------------------------------------------------------------------------------------

// Field 'BMSChrgSt'
SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSChrgSt* SI_BmsStSrvSkeletonSomeIpBinding::GetFieldNotifierBMSChrgSt() noexcept {
  return &field_notifier_BMSChrgSt_;
}

// Field 'BMSDCChrgCnctSt'
SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSDCChrgCnctSt* SI_BmsStSrvSkeletonSomeIpBinding::GetFieldNotifierBMSDCChrgCnctSt() noexcept {
  return &field_notifier_BMSDCChrgCnctSt_;
}

// Field 'BMSDCChrgEndReas'
SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSDCChrgEndReas* SI_BmsStSrvSkeletonSomeIpBinding::GetFieldNotifierBMSDCChrgEndReas() noexcept {
  return &field_notifier_BMSDCChrgEndReas_;
}

// Field 'BMSDchrgPrmsSt'
SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSDchrgPrmsSt* SI_BmsStSrvSkeletonSomeIpBinding::GetFieldNotifierBMSDchrgPrmsSt() noexcept {
  return &field_notifier_BMSDchrgPrmsSt_;
}

// Field 'BMSHeatRunawaySt'
SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSHeatRunawaySt* SI_BmsStSrvSkeletonSomeIpBinding::GetFieldNotifierBMSHeatRunawaySt() noexcept {
  return &field_notifier_BMSHeatRunawaySt_;
}

// Field 'BMSInnerSOCSt'
SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSInnerSOCSt* SI_BmsStSrvSkeletonSomeIpBinding::GetFieldNotifierBMSInnerSOCSt() noexcept {
  return &field_notifier_BMSInnerSOCSt_;
}

// Field 'BMSIntelTempMgtSt'
SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSIntelTempMgtSt* SI_BmsStSrvSkeletonSomeIpBinding::GetFieldNotifierBMSIntelTempMgtSt() noexcept {
  return &field_notifier_BMSIntelTempMgtSt_;
}

// Field 'BMSLowTempSt'
SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSLowTempSt* SI_BmsStSrvSkeletonSomeIpBinding::GetFieldNotifierBMSLowTempSt() noexcept {
  return &field_notifier_BMSLowTempSt_;
}

// Field 'BMSRmtPreHeatSt'
SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSRmtPreHeatSt* SI_BmsStSrvSkeletonSomeIpBinding::GetFieldNotifierBMSRmtPreHeatSt() noexcept {
  return &field_notifier_BMSRmtPreHeatSt_;
}

// Field 'BMSSafCnctSt'
SI_BmsStSrvSkeletonSomeIpFieldNotifierBMSSafCnctSt* SI_BmsStSrvSkeletonSomeIpBinding::GetFieldNotifierBMSSafCnctSt() noexcept {
  return &field_notifier_BMSSafCnctSt_;
}

// Field 'BmsErrSt'
SI_BmsStSrvSkeletonSomeIpFieldNotifierBmsErrSt* SI_BmsStSrvSkeletonSomeIpBinding::GetFieldNotifierBmsErrSt() noexcept {
  return &field_notifier_BmsErrSt_;
}

// Field 'BmsHVILSt'
SI_BmsStSrvSkeletonSomeIpFieldNotifierBmsHVILSt* SI_BmsStSrvSkeletonSomeIpBinding::GetFieldNotifierBmsHVILSt() noexcept {
  return &field_notifier_BmsHVILSt_;
}

// Field 'BmsIsoMeasSt'
SI_BmsStSrvSkeletonSomeIpFieldNotifierBmsIsoMeasSt* SI_BmsStSrvSkeletonSomeIpBinding::GetFieldNotifierBmsIsoMeasSt() noexcept {
  return &field_notifier_BmsIsoMeasSt_;
}

// Field 'BmsSt'
SI_BmsStSrvSkeletonSomeIpFieldNotifierBmsSt* SI_BmsStSrvSkeletonSomeIpBinding::GetFieldNotifierBmsSt() noexcept {
  return &field_notifier_BmsSt_;
}

// Field 'HVBattSOCLim'
SI_BmsStSrvSkeletonSomeIpFieldNotifierHVBattSOCLim* SI_BmsStSrvSkeletonSomeIpBinding::GetFieldNotifierHVBattSOCLim() noexcept {
  return &field_notifier_HVBattSOCLim_;
}

// Field 'HVMaiSwtSt'
SI_BmsStSrvSkeletonSomeIpFieldNotifierHVMaiSwtSt* SI_BmsStSrvSkeletonSomeIpBinding::GetFieldNotifierHVMaiSwtSt() noexcept {
  return &field_notifier_HVMaiSwtSt_;
}

}  // namespace internal
}  // namespace someip_binding_transformation_layer
}  // namespace amsr
}  // namespace bmsstsrv_si
}  // namespace pt_batterypacksys
}  // namespace gwm

