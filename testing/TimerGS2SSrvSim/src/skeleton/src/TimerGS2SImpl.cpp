
#include "TimerGS2SImpl.h"
#include "iostream"

namespace gwm {
namespace services {
namespace timersrv_gs2s {

namespace skeleton {

using vac::container::operator""_sv;

TimerSrvGS2SImpl::TimerSrvGS2SImpl() : 
    gwm::skeleton::SkeletonMixin<TimerSrvGS2SSkeleton>(ara::core::InstanceSpecifier("DenpendencySrvExe/RootSwComponentPrototype/AdaptiveProvidedPortType_Timer_GS2SSrv"_sv)),
    m_uiTimerSet(0), m_enStartPoint(StartPointFieldType::CountFromTriggered),
    m_enTimerType(TimerTypeFieldType::DurationTime), m_enSleepType(SleepTypeFieldType::NormalSleep),
    m_uiTimer(0), m_bTiming(false)
{
    m_sctTimerSt.WakeSrcexpired = ::datatypes::gs2s::timersrv_gs2s::TimerSt_WakeSrcexpired_Enum::DefaultValue;
    m_sctTimerSt.WakeSource = ::datatypes::gs2s::timersrv_gs2s::TimerSt_WakeSource_Enum::DefaultValue;

    TimerSt.RegisterGetHandler([this]() -> ara::core::Future<TimerStFieldType> {return this->getTimerSt();});
    
    this->TimerSt.Update(m_sctTimerSt);

    worker = std::make_unique<gwm::utils::WorkerThread>();
}

TimerSrvGS2SImpl::~TimerSrvGS2SImpl()
{
    if(worker)
        worker->stop();
}

inline void TimerSrvGS2SImpl::ontimer(bool timing) {
    uint32_t lastTime = time(NULL);
    uint32_t diffTime = 0;

    uint32_t timer = m_uiTimer;
    while(timing) {
        if (timer != m_uiTimer) 
        {
            lastTime = time(NULL);
            timer = m_uiTimer;
        }
        uint32_t nowTime = time(NULL);
        if (abs(nowTime - lastTime) >= timer) {
            break;
        }
        if (abs(nowTime - lastTime) != diffTime)
        {
            diffTime = abs(nowTime - lastTime);
            std::cout<<"time second: " << nowTime << " timer: " << timer << " Count down: "<< timer - diffTime << std::endl;
        }
        std::this_thread::sleep_for(std::chrono::milliseconds{500});
    }

    if (m_bTiming) {
        m_bTiming = false;
        m_sctTimerSt.WakeSrcexpired = ::datatypes::gs2s::timersrv_gs2s::TimerSt_WakeSrcexpired_Enum::WakeupSourceExpired ;
        m_sctTimerSt.WakeSource = ::datatypes::gs2s::timersrv_gs2s::TimerSt_WakeSource_Enum::CurrentSourceTriggered;
    } else {
        m_sctTimerSt.WakeSrcexpired = ::datatypes::gs2s::timersrv_gs2s::TimerSt_WakeSrcexpired_Enum::NoWakeupSourceExpired ;
        m_sctTimerSt.WakeSource = ::datatypes::gs2s::timersrv_gs2s::TimerSt_WakeSource_Enum::NotCurrentSourceTriggered;
    }
    
    this->TimerSt.Update(m_sctTimerSt);
}

ara::core::Future<methods::TimerReq::Output> 
    TimerSrvGS2SImpl::TimerReq(const ::datatypes::gs2s::timersrv_gs2s::TimerSet_Integer& TimerSet, 
    const ::datatypes::gs2s::timersrv_gs2s::StartPoint_Enum& StartPoint, 
    const ::datatypes::gs2s::timersrv_gs2s::TimerType_Enum& TimerType, 
    const ::datatypes::gs2s::timersrv_gs2s::SleepType_Enum& SleepType)
{
    ara::core::Promise<methods::TimerReq::Output> ret;
    ret.set_value({::datatypes::gs2s::timersrv_gs2s::TimerSet_Response_Enum::NOK});

    uint32_t uiTimer = static_cast<uint32_t>(TimerSet);
    if (uiTimer == 0xFFFFFFFF) {
        m_uiTimer = 0;
        m_bTiming = false;

        m_sctTimerSt.WakeSrcexpired = ::datatypes::gs2s::timersrv_gs2s::TimerSt_WakeSrcexpired_Enum::NoWakeupSourceExpired ;
        m_sctTimerSt.WakeSource = ::datatypes::gs2s::timersrv_gs2s::TimerSt_WakeSource_Enum::NotCurrentSourceTriggered;
    } else {
        m_uiTimer = uiTimer;
        if (!m_bTiming) {
            m_bTiming = true;
            worker->schedule([this] {
                this->ontimer(this->m_bTiming);
            });

            m_sctTimerSt.WakeSrcexpired = ::datatypes::gs2s::timersrv_gs2s::TimerSt_WakeSrcexpired_Enum::NoWakeupSourceExpired ;
            m_sctTimerSt.WakeSource = ::datatypes::gs2s::timersrv_gs2s::TimerSt_WakeSource_Enum::CurrentSourceTriggered;
        }
    }
    this->TimerSt.Update(m_sctTimerSt);
    ara::core::Promise<methods::TimerReq::Output> OKret;
    OKret.set_value({::datatypes::gs2s::timersrv_gs2s::TimerSet_Response_Enum::OK});
    return OKret.get_future();
}

void TimerSrvGS2SImpl::SetTimerSt(TimerStFieldType timerSt)
{
    std::cout << "Setting the TimerSt State..." << std::endl;

    this->m_sctTimerSt = timerSt;

    this->TimerSt.Update(this->m_sctTimerSt);
}

ara::core::Future<TimerStFieldType> TimerSrvGS2SImpl::getTimerSt()
{
    std::cout << "Getting the TimerSt State WakeSrcexpired: " << static_cast<int>(m_sctTimerSt.WakeSrcexpired) 
              << " WakeSource: " << static_cast<int>(m_sctTimerSt.WakeSource) << std::endl;

    ara::core::Promise<TimerStFieldType> prms;
    prms.set_value(m_sctTimerSt);
    ara::core::Future<TimerStFieldType> modFuture = prms.get_future();
    return modFuture;
}



}}}}