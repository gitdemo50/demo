/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/include/gwm/pt_batterypacksys/bmsstsrv_si/SI_BmsStSrv_skeleton_impl_interface.h
 *        \brief  Skeleton implementation interface of service 'SI_BmsStSrv'.
 *
 *      \details  The service interface provides HV battery real time current information
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_socal
 *         Commit ID: 209b3c0234013e84e3170ee826c9a8c6022de194
 *********************************************************************************************************************/

#ifndef DENPENDENCYSRVEXE_INCLUDE_GWM_PT_BATTERYPACKSYS_BMSSTSRV_SI_SI_BMSSTSRV_SKELETON_IMPL_INTERFACE_H_
#define DENPENDENCYSRVEXE_INCLUDE_GWM_PT_BATTERYPACKSYS_BMSSTSRV_SI_SI_BMSSTSRV_SKELETON_IMPL_INTERFACE_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "amsr/socal/internal/events/skeleton_event_manager_interface.h"
#include "gwm/pt_batterypacksys/bmsstsrv_si/si_bmsstsrv_common.h"

namespace gwm {
namespace pt_batterypacksys {
namespace bmsstsrv_si {
namespace internal {

/*!
 * \brief Skeleton implementation interface of service 'SI_BmsStSrv'
 */
class SI_BmsStSrvSkeletonImplInterface {
 public:
 /*!
   * \brief Define default constructor.
   * \pre -
   * \context App
   */
  SI_BmsStSrvSkeletonImplInterface() noexcept = default;

  /*!
   * \brief Use default destructor
   */
  virtual ~SI_BmsStSrvSkeletonImplInterface() noexcept = default;

 protected:

  /*!
   * \brief Use default move constructor
   * \pre -
   * \context App
   */
  SI_BmsStSrvSkeletonImplInterface(SI_BmsStSrvSkeletonImplInterface &&) noexcept = default;

  /*!
   * \brief Use default move assignment
   * \pre -
   * \context App
   */
  SI_BmsStSrvSkeletonImplInterface &operator=(SI_BmsStSrvSkeletonImplInterface &&) & noexcept = default;

  SI_BmsStSrvSkeletonImplInterface(SI_BmsStSrvSkeletonImplInterface const &) = delete;

  SI_BmsStSrvSkeletonImplInterface &operator=(SI_BmsStSrvSkeletonImplInterface const &) & = delete;

 public:

  // ---- Events ---------------------------------------------------------------------------------------------------

  /*!
   * \brief Get the event manager object for the service event 'BMSChrgTi'.
   * \details Event sample type: ::gwm::pt_batterypacksys::batterypacksys_idt::BMSChrgTi_Integer_Idt.
   * \return A binding-specific event management object/interface supporting event transmission.
   * \pre -
   * \context App
   */
  virtual ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::BMSChrgTi_Integer_Idt>* GetEventManagerBMSChrgTi() noexcept = 0;

  /*!
   * \brief Get the event manager object for the service event 'BMSConDchrMaxPwr'.
   * \details Event sample type: ::gwm::pt_batterypacksys::batterypacksys_idt::BMSConDchrMaxPwr_Float_Idt.
   * \return A binding-specific event management object/interface supporting event transmission.
   * \pre -
   * \context App
   */
  virtual ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::BMSConDchrMaxPwr_Float_Idt>* GetEventManagerBMSConDchrMaxPwr() noexcept = 0;

  /*!
   * \brief Get the event manager object for the service event 'BMSConFBMaxPwr'.
   * \details Event sample type: ::gwm::pt_batterypacksys::batterypacksys_idt::BMSConFBMaxPwr_Float_Idt.
   * \return A binding-specific event management object/interface supporting event transmission.
   * \pre -
   * \context App
   */
  virtual ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::BMSConFBMaxPwr_Float_Idt>* GetEventManagerBMSConFBMaxPwr() noexcept = 0;

  /*!
   * \brief Get the event manager object for the service event 'BMSPlsDchrMaxPwr'.
   * \details Event sample type: ::gwm::pt_batterypacksys::batterypacksys_idt::BMSPlsDchrMaxPwr_Float_Idt.
   * \return A binding-specific event management object/interface supporting event transmission.
   * \pre -
   * \context App
   */
  virtual ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::BMSPlsDchrMaxPwr_Float_Idt>* GetEventManagerBMSPlsDchrMaxPwr() noexcept = 0;

  /*!
   * \brief Get the event manager object for the service event 'BMSPlsFBMaxPwr'.
   * \details Event sample type: ::gwm::pt_batterypacksys::batterypacksys_idt::BMSPlsFBMaxPwr_Float_Idt.
   * \return A binding-specific event management object/interface supporting event transmission.
   * \pre -
   * \context App
   */
  virtual ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::BMSPlsFBMaxPwr_Float_Idt>* GetEventManagerBMSPlsFBMaxPwr() noexcept = 0;

  /*!
   * \brief Get the event manager object for the service event 'BmsInletCooltT'.
   * \details Event sample type: ::gwm::pt_batterypacksys::batterypacksys_idt::BmsInletCooltT_Integer_Idt.
   * \return A binding-specific event management object/interface supporting event transmission.
   * \pre -
   * \context App
   */
  virtual ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::BmsInletCooltT_Integer_Idt>* GetEventManagerBmsInletCooltT() noexcept = 0;

  /*!
   * \brief Get the event manager object for the service event 'BmsIsoR'.
   * \details Event sample type: ::gwm::pt_batterypacksys::batterypacksys_idt::BmsIsoR_Integer_Idt.
   * \return A binding-specific event management object/interface supporting event transmission.
   * \pre -
   * \context App
   */
  virtual ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::BmsIsoR_Integer_Idt>* GetEventManagerBmsIsoR() noexcept = 0;

  /*!
   * \brief Get the event manager object for the service event 'BmsOutletCooltT'.
   * \details Event sample type: ::gwm::pt_batterypacksys::batterypacksys_idt::BmsOutletCooltT_Integer_Idt.
   * \return A binding-specific event management object/interface supporting event transmission.
   * \pre -
   * \context App
   */
  virtual ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::BmsOutletCooltT_Integer_Idt>* GetEventManagerBmsOutletCooltT() noexcept = 0;

  /*!
   * \brief Get the event manager object for the service event 'CellMaxT'.
   * \details Event sample type: ::gwm::pt_batterypacksys::batterypacksys_idt::CellMaxT_Struct_Idt.
   * \return A binding-specific event management object/interface supporting event transmission.
   * \pre -
   * \context App
   */
  virtual ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::CellMaxT_Struct_Idt>* GetEventManagerCellMaxT() noexcept = 0;

  /*!
   * \brief Get the event manager object for the service event 'CellMaxU'.
   * \details Event sample type: ::gwm::pt_batterypacksys::batterypacksys_idt::CellMaxU_Struct_Idt.
   * \return A binding-specific event management object/interface supporting event transmission.
   * \pre -
   * \context App
   */
  virtual ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::CellMaxU_Struct_Idt>* GetEventManagerCellMaxU() noexcept = 0;

  /*!
   * \brief Get the event manager object for the service event 'CellMinT'.
   * \details Event sample type: ::gwm::pt_batterypacksys::batterypacksys_idt::CellMinT_Struct_Idt.
   * \return A binding-specific event management object/interface supporting event transmission.
   * \pre -
   * \context App
   */
  virtual ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::CellMinT_Struct_Idt>* GetEventManagerCellMinT() noexcept = 0;

  /*!
   * \brief Get the event manager object for the service event 'CellMinU'.
   * \details Event sample type: ::gwm::pt_batterypacksys::batterypacksys_idt::CellMinU_Struct_Idt.
   * \return A binding-specific event management object/interface supporting event transmission.
   * \pre -
   * \context App
   */
  virtual ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::CellMinU_Struct_Idt>* GetEventManagerCellMinU() noexcept = 0;

  /*!
   * \brief Get the event manager object for the service event 'HVBattDispySoc'.
   * \details Event sample type: ::gwm::pt_batterypacksys::batterypacksys_idt::HVBattDispySoc_Float_Idt.
   * \return A binding-specific event management object/interface supporting event transmission.
   * \pre -
   * \context App
   */
  virtual ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::HVBattDispySoc_Float_Idt>* GetEventManagerHVBattDispySoc() noexcept = 0;

  /*!
   * \brief Get the event manager object for the service event 'HVBattI'.
   * \details Event sample type: ::gwm::pt_batterypacksys::batterypacksys_idt::HVBattI_Float_Idt.
   * \return A binding-specific event management object/interface supporting event transmission.
   * \pre -
   * \context App
   */
  virtual ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::HVBattI_Float_Idt>* GetEventManagerHVBattI() noexcept = 0;

  /*!
   * \brief Get the event manager object for the service event 'HVBattInnerSoc'.
   * \details Event sample type: ::gwm::pt_batterypacksys::batterypacksys_idt::HVBattInnerSoc_Float_Idt.
   * \return A binding-specific event management object/interface supporting event transmission.
   * \pre -
   * \context App
   */
  virtual ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::HVBattInnerSoc_Float_Idt>* GetEventManagerHVBattInnerSoc() noexcept = 0;

  /*!
   * \brief Get the event manager object for the service event 'HVBattSoh'.
   * \details Event sample type: ::gwm::pt_batterypacksys::batterypacksys_idt::HVBattSoh_Float_Idt.
   * \return A binding-specific event management object/interface supporting event transmission.
   * \pre -
   * \context App
   */
  virtual ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::HVBattSoh_Float_Idt>* GetEventManagerHVBattSoh() noexcept = 0;

  /*!
   * \brief Get the event manager object for the service event 'HVBattTotEgy'.
   * \details Event sample type: ::gwm::pt_batterypacksys::batterypacksys_idt::HVBattTotEgy_Float_Idt.
   * \return A binding-specific event management object/interface supporting event transmission.
   * \pre -
   * \context App
   */
  virtual ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::HVBattTotEgy_Float_Idt>* GetEventManagerHVBattTotEgy() noexcept = 0;

  /*!
   * \brief Get the event manager object for the service event 'HVBattU'.
   * \details Event sample type: ::gwm::pt_batterypacksys::batterypacksys_idt::HVBattU_Float_Idt.
   * \return A binding-specific event management object/interface supporting event transmission.
   * \pre -
   * \context App
   */
  virtual ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::HVBattU_Float_Idt>* GetEventManagerHVBattU() noexcept = 0;

  /*!
   * \brief Get the event manager object for the service event 'ModulAvrgT'.
   * \details Event sample type: ::gwm::pt_batterypacksys::batterypacksys_idt::ModulAvrgT_Integer_Idt.
   * \return A binding-specific event management object/interface supporting event transmission.
   * \pre -
   * \context App
   */
  virtual ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::ModulAvrgT_Integer_Idt>* GetEventManagerModulAvrgT() noexcept = 0;

  // ---- Fields ---------------------------------------------------------------------------------------------------

  /*!
   * \brief Get the event manager object for the field notifier of field 'BMSChrgSt'.
   * \details Field data type: ::gwm::pt_batterypacksys::batterypacksys_idt::BMSChrgSt_Enum_Idt.
   * \return A binding-specific event management object/interface supporting field notifier updates.
   * \pre -
   * \context App
   */
   virtual ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::BMSChrgSt_Enum_Idt>* GetFieldNotifierBMSChrgSt() noexcept = 0;

  /*!
   * \brief Get the event manager object for the field notifier of field 'BMSDCChrgCnctSt'.
   * \details Field data type: ::gwm::pt_batterypacksys::batterypacksys_idt::BMSDCChrgCnctSt_Enum_Idt.
   * \return A binding-specific event management object/interface supporting field notifier updates.
   * \pre -
   * \context App
   */
   virtual ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::BMSDCChrgCnctSt_Enum_Idt>* GetFieldNotifierBMSDCChrgCnctSt() noexcept = 0;

  /*!
   * \brief Get the event manager object for the field notifier of field 'BMSDCChrgEndReas'.
   * \details Field data type: ::gwm::pt_batterypacksys::batterypacksys_idt::BMSDCChrgEndReas_Enum_Idt.
   * \return A binding-specific event management object/interface supporting field notifier updates.
   * \pre -
   * \context App
   */
   virtual ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::BMSDCChrgEndReas_Enum_Idt>* GetFieldNotifierBMSDCChrgEndReas() noexcept = 0;

  /*!
   * \brief Get the event manager object for the field notifier of field 'BMSDchrgPrmsSt'.
   * \details Field data type: ::gwm::pt_batterypacksys::batterypacksys_idt::BMSDchrgPrmsSt_Enum_Idt.
   * \return A binding-specific event management object/interface supporting field notifier updates.
   * \pre -
   * \context App
   */
   virtual ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::BMSDchrgPrmsSt_Enum_Idt>* GetFieldNotifierBMSDchrgPrmsSt() noexcept = 0;

  /*!
   * \brief Get the event manager object for the field notifier of field 'BMSHeatRunawaySt'.
   * \details Field data type: ::gwm::pt_batterypacksys::batterypacksys_idt::BMSHeatRunawaySt_Enum_Idt.
   * \return A binding-specific event management object/interface supporting field notifier updates.
   * \pre -
   * \context App
   */
   virtual ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::BMSHeatRunawaySt_Enum_Idt>* GetFieldNotifierBMSHeatRunawaySt() noexcept = 0;

  /*!
   * \brief Get the event manager object for the field notifier of field 'BMSInnerSOCSt'.
   * \details Field data type: ::gwm::pt_batterypacksys::batterypacksys_idt::BMSInnerSOCSt_Struct_Idt.
   * \return A binding-specific event management object/interface supporting field notifier updates.
   * \pre -
   * \context App
   */
   virtual ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::BMSInnerSOCSt_Struct_Idt>* GetFieldNotifierBMSInnerSOCSt() noexcept = 0;

  /*!
   * \brief Get the event manager object for the field notifier of field 'BMSIntelTempMgtSt'.
   * \details Field data type: ::gwm::pt_batterypacksys::batterypacksys_idt::BMSIntelTempMgtSt_Enum_Idt.
   * \return A binding-specific event management object/interface supporting field notifier updates.
   * \pre -
   * \context App
   */
   virtual ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::BMSIntelTempMgtSt_Enum_Idt>* GetFieldNotifierBMSIntelTempMgtSt() noexcept = 0;

  /*!
   * \brief Get the event manager object for the field notifier of field 'BMSLowTempSt'.
   * \details Field data type: ::gwm::pt_batterypacksys::batterypacksys_idt::BMSLowTempSt_Struct_Idt.
   * \return A binding-specific event management object/interface supporting field notifier updates.
   * \pre -
   * \context App
   */
   virtual ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::BMSLowTempSt_Struct_Idt>* GetFieldNotifierBMSLowTempSt() noexcept = 0;

  /*!
   * \brief Get the event manager object for the field notifier of field 'BMSRmtPreHeatSt'.
   * \details Field data type: ::gwm::pt_batterypacksys::batterypacksys_idt::BMSRmtPreHeatSt_Enum_Idt.
   * \return A binding-specific event management object/interface supporting field notifier updates.
   * \pre -
   * \context App
   */
   virtual ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::BMSRmtPreHeatSt_Enum_Idt>* GetFieldNotifierBMSRmtPreHeatSt() noexcept = 0;

  /*!
   * \brief Get the event manager object for the field notifier of field 'BMSSafCnctSt'.
   * \details Field data type: ::gwm::pt_batterypacksys::batterypacksys_idt::BMSSafCnctSt_Enum_Idt.
   * \return A binding-specific event management object/interface supporting field notifier updates.
   * \pre -
   * \context App
   */
   virtual ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::BMSSafCnctSt_Enum_Idt>* GetFieldNotifierBMSSafCnctSt() noexcept = 0;

  /*!
   * \brief Get the event manager object for the field notifier of field 'BmsErrSt'.
   * \details Field data type: ::gwm::pt_batterypacksys::batterypacksys_idt::BmsErrSt_Struct_Idt.
   * \return A binding-specific event management object/interface supporting field notifier updates.
   * \pre -
   * \context App
   */
   virtual ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::BmsErrSt_Struct_Idt>* GetFieldNotifierBmsErrSt() noexcept = 0;

  /*!
   * \brief Get the event manager object for the field notifier of field 'BmsHVILSt'.
   * \details Field data type: ::gwm::pt_batterypacksys::batterypacksys_idt::BmsHVILSt_Enum_Idt.
   * \return A binding-specific event management object/interface supporting field notifier updates.
   * \pre -
   * \context App
   */
   virtual ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::BmsHVILSt_Enum_Idt>* GetFieldNotifierBmsHVILSt() noexcept = 0;

  /*!
   * \brief Get the event manager object for the field notifier of field 'BmsIsoMeasSt'.
   * \details Field data type: ::gwm::pt_batterypacksys::batterypacksys_idt::BmsIsoMeasSt_Enum_Idt.
   * \return A binding-specific event management object/interface supporting field notifier updates.
   * \pre -
   * \context App
   */
   virtual ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::BmsIsoMeasSt_Enum_Idt>* GetFieldNotifierBmsIsoMeasSt() noexcept = 0;

  /*!
   * \brief Get the event manager object for the field notifier of field 'BmsSt'.
   * \details Field data type: ::gwm::pt_batterypacksys::batterypacksys_idt::BmsSt_Enum_Idt.
   * \return A binding-specific event management object/interface supporting field notifier updates.
   * \pre -
   * \context App
   */
   virtual ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::BmsSt_Enum_Idt>* GetFieldNotifierBmsSt() noexcept = 0;

  /*!
   * \brief Get the event manager object for the field notifier of field 'HVBattSOCLim'.
   * \details Field data type: ::gwm::pt_batterypacksys::batterypacksys_idt::HVBattSOCLim_Enum_Idt.
   * \return A binding-specific event management object/interface supporting field notifier updates.
   * \pre -
   * \context App
   */
   virtual ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::HVBattSOCLim_Enum_Idt>* GetFieldNotifierHVBattSOCLim() noexcept = 0;

  /*!
   * \brief Get the event manager object for the field notifier of field 'HVMaiSwtSt'.
   * \details Field data type: ::gwm::pt_batterypacksys::batterypacksys_idt::HVMaiSwtSt_Struct_Idt.
   * \return A binding-specific event management object/interface supporting field notifier updates.
   * \pre -
   * \context App
   */
   virtual ::amsr::socal::internal::events::SkeletonEventManagerInterface<::gwm::pt_batterypacksys::batterypacksys_idt::HVMaiSwtSt_Struct_Idt>* GetFieldNotifierHVMaiSwtSt() noexcept = 0;
};

} // namespace internal
}  // namespace bmsstsrv_si
}  // namespace pt_batterypacksys
}  // namespace gwm

#endif  // DENPENDENCYSRVEXE_INCLUDE_GWM_PT_BATTERYPACKSYS_BMSSTSRV_SI_SI_BMSSTSRV_SKELETON_IMPL_INTERFACE_H_

