#include <condition_variable>
#include <chrono>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "ara/com/runtime.h"
#include "ara/com/types.h"
#include "ara/core/future.h"

#include "SensorProxy.hpp"
#include "mocks/proxyImpl_mock.hpp"

namespace gwm{
namespace services {
namespace timer {
namespace proxy {

using ::testing::Return;
using ::testing::ReturnRef;
using ::testing::MockFunction;

class MockSensorProxy : public SensorProxy<ProxyImpl> {
public:

    MockSensorProxy(MockEvent& m) : SensorProxy((gwm::platform::Logger("MockSensorProxy"))), eventWrapper(*this,&m) {
        proxy = std::make_unique<ProxyImpl>();
      //  eventWrapper.subscribe();
    }

    void raiseEvent(int v) {
        this->proxy->mockEvent.raiseEvent(v);
    }

    MOCK_METHOD(void, doSubscription, (), (override));
    MOCK_METHOD(void, initProxyItems, (), (override));

    EventWrapper<MockEvent> eventWrapper;
};

TEST(SensorProxy, SensorProxy) {
    std::condition_variable cv;
    std::mutex cv_m;

    MockEvent mockEvent;
    MockSensorProxy mockSensorProxy(mockEvent);

    MockEvent::CacheType cache;

    MockFunction<void(MockEvent::SampleType&)> mock_function;

    EXPECT_CALL(mock_function,Call)
        .WillOnce([] (MockEvent::SampleType &d) {
            ASSERT_EQ(d,199);
        });

    EXPECT_CALL(mockSensorProxy,initProxyItems())
        .Times(2);
    EXPECT_CALL(mockSensorProxy,doSubscription())
        .Times(2)
        .WillRepeatedly([&cv] { cv.notify_all(); });

    EXPECT_CALL(mockEvent,Subscribe)
        .Times(4);

    EXPECT_CALL(mockEvent,SetReceiveHandler)
        .Times(4)
        .WillRepeatedly([] (MockEvent::ReceiveHandler cb) { cb();} );

    EXPECT_CALL(mockEvent,Update())
        .Times(4)
        .WillOnce(Return(false))
        .WillOnce(Return(true))
        .WillOnce(Return(true))
        .WillOnce(Return(true));

    EXPECT_CALL(mockEvent,GetCachedSamples())
        .Times(3)
        .WillOnce(ReturnRef(cache))
        .WillOnce(ReturnRef(cache))
        .WillOnce(ReturnRef(cache));

    mockSensorProxy.start("MockSpec");
    MockEvent::SampleType val = 199;
    mockSensorProxy.eventWrapper.subscribe();
    mockSensorProxy.eventWrapper.subscribe();
    cache.push_back(std::make_shared<int>(val));
    mockSensorProxy.eventWrapper.subscribe();
    //mockSensorProxy.raiseEvent(10);
    mockSensorProxy.eventWrapper.setEventHandler(mock_function.AsStdFunction());
    
    mockSensorProxy.eventWrapper.subscribe();
    ASSERT_EQ(mockSensorProxy.isServiceAvailable(), true);
    mockSensorProxy.start("MockSpec");
    std::unique_lock<std::mutex> lk(cv_m);
    cv.wait_for(lk,std::chrono::seconds(1));
}

}}}}