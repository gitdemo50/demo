/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/include/gwm/pt_batterypacksys/batterypacksys_idt/impl_type_bmsinnersocst_struct_idt.h
 *        \brief
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_modelleddatatypes_api
 *         Commit ID: d9332226824a4a6c4616e2d948d41c862d192343
 *********************************************************************************************************************/

#ifndef DENPENDENCYSRVEXE_INCLUDE_GWM_PT_BATTERYPACKSYS_BATTERYPACKSYS_IDT_IMPL_TYPE_BMSINNERSOCST_STRUCT_IDT_H_
#define DENPENDENCYSRVEXE_INCLUDE_GWM_PT_BATTERYPACKSYS_BATTERYPACKSYS_IDT_IMPL_TYPE_BMSINNERSOCST_STRUCT_IDT_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include <array>
#include <cstdint>
#include <map>
#include <string>
#include <vector>
#include "gwm/pt_batterypacksys/batterypacksys_idt/impl_type_bmsinnersocst_maxsoc_float_idt.h"
#include "gwm/pt_batterypacksys/batterypacksys_idt/impl_type_bmsinnersocst_minsoc_float_idt.h"

namespace gwm {
namespace pt_batterypacksys {
namespace batterypacksys_idt {

// VECTOR Disable AutosarC++17_10-A12.6.1: MD_MDTG_A12.6.1_GeneratedStructUninitializedMembers
// VECTOR Disable AutosarC++17_10-M8.5.1: MD_MDTG_M8.5.1_GeneratedStructUninitializedMembers
/*!
 * \brief Type BMSInnerSOCSt_Struct_Idt.
 * \remark generated
 * \trace SPEC-5951372
 *
 * IMPLEMENTATION-DATA-TYPE /DataTypes/ImplementationDataTypes/BMSInnerSOCSt_Struct_Idt
 */
struct BMSInnerSOCSt_Struct_Idt {
  using MaxSOC_Idt_generated_type = gwm::pt_batterypacksys::batterypacksys_idt::BMSInnerSOCSt_MaxSOC_Float_Idt;
  using MinSOC_Idt_generated_type = gwm::pt_batterypacksys::batterypacksys_idt::BMSInnerSOCSt_MinSOC_Float_Idt;

  MaxSOC_Idt_generated_type MaxSOC_Idt;
  MinSOC_Idt_generated_type MinSOC_Idt;
};
// VECTOR Enable AutosarC++17_10-A12.6.1
// VECTOR Enable AutosarC++17_10-M8.5.1

/*!
 * \brief Compare for equality with another BMSInnerSOCSt_Struct_Idt instance.
 */
inline bool operator==(BMSInnerSOCSt_Struct_Idt const& l,
                       BMSInnerSOCSt_Struct_Idt const& r) noexcept {
  return (&l == &r) || ((l.MaxSOC_Idt == r.MaxSOC_Idt)
                         && (l.MinSOC_Idt == r.MinSOC_Idt)
  );
}

/*!
 * \brief Compare for inequality with another BMSInnerSOCSt_Struct_Idt instance.
 */
inline bool operator!=(BMSInnerSOCSt_Struct_Idt const& l,
                       BMSInnerSOCSt_Struct_Idt const& r) noexcept {
  return !(l == r);
}

}  // namespace batterypacksys_idt
}  // namespace pt_batterypacksys
}  // namespace gwm

#endif  // DENPENDENCYSRVEXE_INCLUDE_GWM_PT_BATTERYPACKSYS_BATTERYPACKSYS_IDT_IMPL_TYPE_BMSINNERSOCST_STRUCT_IDT_H_
