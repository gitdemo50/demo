/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  TimerSrvExe/include/datatypes/gs2s/timersrv_gs2s/impl_type_timerst_struct.h
 *        \brief
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_modelleddatatypes_api
 *         Commit ID: d9332226824a4a6c4616e2d948d41c862d192343
 *********************************************************************************************************************/

#ifndef TIMERSRVEXE_INCLUDE_DATATYPES_GS2S_TIMERSRV_GS2S_IMPL_TYPE_TIMERST_STRUCT_H_
#define TIMERSRVEXE_INCLUDE_DATATYPES_GS2S_TIMERSRV_GS2S_IMPL_TYPE_TIMERST_STRUCT_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include <array>
#include <cstdint>
#include <map>
#include <string>
#include <vector>
#include "datatypes/gs2s/timersrv_gs2s/impl_type_timerst_wakesource_enum.h"
#include "datatypes/gs2s/timersrv_gs2s/impl_type_timerst_wakesrcexpired_enum.h"

namespace datatypes {
namespace gs2s {
namespace timersrv_gs2s {

// VECTOR Disable AutosarC++17_10-A12.6.1: MD_MDTG_A12.6.1_GeneratedStructUninitializedMembers
// VECTOR Disable AutosarC++17_10-M8.5.1: MD_MDTG_M8.5.1_GeneratedStructUninitializedMembers
/*!
 * \brief Type TimerSt_Struct.
 * \remark generated
 * \trace SPEC-5951372
 *
 * IMPLEMENTATION-DATA-TYPE /S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/TimerSt_Struct
 */
struct TimerSt_Struct {
  using WakeSrcexpired_generated_type = datatypes::gs2s::timersrv_gs2s::TimerSt_WakeSrcexpired_Enum;
  using WakeSource_generated_type = datatypes::gs2s::timersrv_gs2s::TimerSt_WakeSource_Enum;

  WakeSrcexpired_generated_type WakeSrcexpired;
  WakeSource_generated_type WakeSource;
};
// VECTOR Enable AutosarC++17_10-A12.6.1
// VECTOR Enable AutosarC++17_10-M8.5.1

/*!
 * \brief Compare for equality with another TimerSt_Struct instance.
 */
inline bool operator==(TimerSt_Struct const& l,
                       TimerSt_Struct const& r) noexcept {
  return (&l == &r) || ((l.WakeSrcexpired == r.WakeSrcexpired)
                         && (l.WakeSource == r.WakeSource)
  );
}

/*!
 * \brief Compare for inequality with another TimerSt_Struct instance.
 */
inline bool operator!=(TimerSt_Struct const& l,
                       TimerSt_Struct const& r) noexcept {
  return !(l == r);
}

}  // namespace timersrv_gs2s
}  // namespace gs2s
}  // namespace datatypes

#endif  // TIMERSRVEXE_INCLUDE_DATATYPES_GS2S_TIMERSRV_GS2S_IMPL_TYPE_TIMERST_STRUCT_H_
