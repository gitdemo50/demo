/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  TiMgrSrvExe/include/gwm/composite_timermgr/timermgr_idt/impl_type_alrmclk_minutes_integer_idt.h
 *        \brief
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_modelleddatatypes_api
 *         Commit ID: d9332226824a4a6c4616e2d948d41c862d192343
 *********************************************************************************************************************/

#ifndef TIMGRSRVEXE_INCLUDE_GWM_COMPOSITE_TIMERMGR_TIMERMGR_IDT_IMPL_TYPE_ALRMCLK_MINUTES_INTEGER_IDT_H_
#define TIMGRSRVEXE_INCLUDE_GWM_COMPOSITE_TIMERMGR_TIMERMGR_IDT_IMPL_TYPE_ALRMCLK_MINUTES_INTEGER_IDT_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include <array>
#include <cstdint>
#include <map>
#include <string>
#include <vector>

namespace gwm {
namespace composite_timermgr {
namespace timermgr_idt {

/*!
 * \brief Type AlrmClk_Minutes_Integer_Idt.
 * \remark generated
 * \trace SPEC-5951372
 *
 * IMPLEMENTATION-DATA-TYPE /DataTypes/ImplementationDataTypes/AlrmClk_Minutes_Integer_Idt
 */
using AlrmClk_Minutes_Integer_Idt = std::uint8_t;

}  // namespace timermgr_idt
}  // namespace composite_timermgr
}  // namespace gwm

#endif  // TIMGRSRVEXE_INCLUDE_GWM_COMPOSITE_TIMERMGR_TIMERMGR_IDT_IMPL_TYPE_ALRMCLK_MINUTES_INTEGER_IDT_H_
