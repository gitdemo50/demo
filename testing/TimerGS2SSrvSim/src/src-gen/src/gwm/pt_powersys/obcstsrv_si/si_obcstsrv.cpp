/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/src/gwm/pt_powersys/obcstsrv_si/si_obcstsrv.cpp
 *        \brief  Header for service 'SI_OBCStSrv'.
 *
 *      \details  This service interface provides OBC working status information
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_socal
 *         Commit ID: 209b3c0234013e84e3170ee826c9a8c6022de194
 *********************************************************************************************************************/

#ifndef DENPENDENCYSRVEXE_SRC_GWM_PT_POWERSYS_OBCSTSRV_SI_SI_OBCSTSRV_CPP_
#define DENPENDENCYSRVEXE_SRC_GWM_PT_POWERSYS_OBCSTSRV_SI_SI_OBCSTSRV_CPP_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "gwm/pt_powersys/obcstsrv_si/SI_OBCStSrv.h"

namespace gwm {
namespace pt_powersys {
namespace obcstsrv_si {

/*!
 * \brief Service Identifier instance.
 */
constexpr ara::com::ServiceIdentifierType SI_OBCStSrv::ServiceIdentifier;

/*!
 * \brief Service shortname path instance.
 */
constexpr vac::container::CStringView SI_OBCStSrv::kServiceShortNamePath;

}  // namespace obcstsrv_si
}  // namespace pt_powersys
}  // namespace gwm

#endif  // DENPENDENCYSRVEXE_SRC_GWM_PT_POWERSYS_OBCSTSRV_SI_SI_OBCSTSRV_CPP_
