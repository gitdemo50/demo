/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  TimerSrvExe/include/gwm/platform/timer/gs2s/SI_TimerSrv_GS2S_proxy_backend_interface.h
 *        \brief  Proxy implementation interface of service 'SI_TimerSrv_GS2S'.
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_socal
 *         Commit ID: 209b3c0234013e84e3170ee826c9a8c6022de194
 *********************************************************************************************************************/

#ifndef TIMERSRVEXE_INCLUDE_GWM_PLATFORM_TIMER_GS2S_SI_TIMERSRV_GS2S_PROXY_BACKEND_INTERFACE_H_
#define TIMERSRVEXE_INCLUDE_GWM_PLATFORM_TIMER_GS2S_SI_TIMERSRV_GS2S_PROXY_BACKEND_INTERFACE_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "amsr/socal/internal/events/proxy_event_backend_interface.h"
#include "ara/core/future.h"
#include "gwm/platform/timer/gs2s/SI_TimerSrv_GS2S_types.h"

namespace gwm {
namespace platform {
namespace timer {
namespace gs2s {
namespace internal {

/*!
 * \brief Proxy backend interface for the Service 'SI_TimerSrv_GS2S'
 * \remark generated
 */
class SI_TimerSrv_GS2SProxyBackendInterface {
 public:

  /*!
   * \brief Define default constructor.
   * \pre -
   * \context App
   * \synchronus TRUE
   */
  SI_TimerSrv_GS2SProxyBackendInterface() noexcept = default;

  /*!
   * \brief Define default destructor.
   * \pre -
   * \context App
   * \synchronus TRUE
   */
  virtual ~SI_TimerSrv_GS2SProxyBackendInterface() noexcept = default;

  SI_TimerSrv_GS2SProxyBackendInterface(SI_TimerSrv_GS2SProxyBackendInterface const &) = delete;

  SI_TimerSrv_GS2SProxyBackendInterface(SI_TimerSrv_GS2SProxyBackendInterface &&) = delete;

  SI_TimerSrv_GS2SProxyBackendInterface &operator=(SI_TimerSrv_GS2SProxyBackendInterface const &) & = delete;

  SI_TimerSrv_GS2SProxyBackendInterface &operator=(SI_TimerSrv_GS2SProxyBackendInterface &&) & = delete;

  // ---- Methods --------------------------------------------------------------------------------------------------

  /*!
   * \brief Invokes request response method TimerReq.
    * \param[in] TimerSet IN parameter of type ::datatypes::gs2s::timersrv_gs2s::TimerSet_Integer
    * \param[in] StartPoint IN parameter of type ::datatypes::gs2s::timersrv_gs2s::StartPoint_Enum
    * \param[in] TimerType IN parameter of type ::datatypes::gs2s::timersrv_gs2s::TimerType_Enum
    * \param[in] SleepType IN parameter of type ::datatypes::gs2s::timersrv_gs2s::SleepType_Enum
   * \return ara::core::Future with method output data element.
   * \pre -
   * \context       App
   * \synchronous   FALSE
   */
  virtual ara::core::Future<methods::TimerReq::Output> HandleMethodTimerReq(::datatypes::gs2s::timersrv_gs2s::TimerSet_Integer const& TimerSet, ::datatypes::gs2s::timersrv_gs2s::StartPoint_Enum const& StartPoint, ::datatypes::gs2s::timersrv_gs2s::TimerType_Enum const& TimerType, ::datatypes::gs2s::timersrv_gs2s::SleepType_Enum const& SleepType) noexcept = 0;

  // ---- Events ---------------------------------------------------------------------------------------------------

  // ---- Fields ---------------------------------------------------------------------------------------------------

  // ---- Field 'TimerSt' ----
  /*!
   * \brief Get the field notifier object for the service field 'TimerSt'
   * \return A proxy event object supporting event sample and subscription.
   * \pre -
   * \context App | Reactor | Callback
   * \synchronus TRUE
   */
  virtual ::amsr::socal::internal::events::ProxyEventBackendInterface<::datatypes::gs2s::timersrv_gs2s::TimerSt_Struct>& GetFieldNotifierBackendTimerSt() noexcept = 0;

  /*!
   * \brief Getter for the field 'TimerSt'
   * \return ara::core::Future with Field data element.
   * \pre -
   * \context App
   * \synchronous FALSE
   */
  virtual ara::core::Future<fields::TimerSt::Output> HandleFieldTimerStMethodGet() noexcept = 0;

};

} //namespace internal
}  // namespace gs2s
}  // namespace timer
}  // namespace platform
}  // namespace gwm

#endif  // TIMERSRVEXE_INCLUDE_GWM_PLATFORM_TIMER_GS2S_SI_TIMERSRV_GS2S_PROXY_BACKEND_INTERFACE_H_

