/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/include/amsr/someip_protocol/internal/method_deserialization/gwm_platform_timer_gs2s/methods/deserializer_Request_TimerReq.h
 *        \brief  SOME/IP packet deserializer of service 'SI_TimerSrv_GS2S'.
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_someipprotocol
 *         Commit ID: 0e137b9e6356987a6d2839c8d79c286219b68d60
 *********************************************************************************************************************/

#ifndef DENPENDENCYSRVEXE_INCLUDE_AMSR_SOMEIP_PROTOCOL_INTERNAL_METHOD_DESERIALIZATION_GWM_PLATFORM_TIMER_GS2S_METHODS_deserializer_Request_TimerReq_h_
#define DENPENDENCYSRVEXE_INCLUDE_AMSR_SOMEIP_PROTOCOL_INTERNAL_METHOD_DESERIALIZATION_GWM_PLATFORM_TIMER_GS2S_METHODS_deserializer_Request_TimerReq_h_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "amsr/someip_protocol/internal/dataprototype_deserialization/gwm_platform_timer_gs2s/methods/deserializer_TimerReqSleepType.h"
#include "amsr/someip_protocol/internal/dataprototype_deserialization/gwm_platform_timer_gs2s/methods/deserializer_TimerReqStartPoint.h"
#include "amsr/someip_protocol/internal/dataprototype_deserialization/gwm_platform_timer_gs2s/methods/deserializer_TimerReqTimerSet.h"
#include "amsr/someip_protocol/internal/dataprototype_deserialization/gwm_platform_timer_gs2s/methods/deserializer_TimerReqTimerType.h"
#include "someip-protocol/internal/deserialization/common.h"
#include "someip-protocol/internal/deserialization/reader.h"

namespace amsr {
namespace someip_protocol {
namespace internal {
namespace method_deserializer {
namespace gwm_platform_timer_gs2s {
namespace methods {

/*!
 * \brief Deserializer for a SOME/IP packet of method '/S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/SI_TimerSrv_GS2S/TimerReq'
 *        of service interface '/S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/SI_TimerSrv_GS2S'.
 */
class DeserializerRequestTimerReq final {
 public:
  /*!
   * \brief Deserialize the SOME/IP packet payload of method 'TimerReq'.
   *
   * \tparam Output Type of data structure storing all deserialized in-/output arguments.
   *                Type must support member initializer lists. Typically a struct or a std::tuple is used.
   * \param[in,out] reader Reference to the byte stream reader.
   * \param[out]    output Parameter to store deserialized output.
   * \return        True if the deserialization is successful, false otherwise.
   * \pre           -
   * \context       Reactor|App
   * \threadsafe    FALSE
   * \reentrant     TRUE for different reader objects.
   * \synchronous   TRUE
   */
  template <typename Output>
  static bool Deserialize(deserialization::Reader& reader, Output & output) noexcept {    


    // Deserialize the argument 'TimerSet' of type '/AUTOSAR/StdTypes/uint32_t'

    // VECTOR NC AutosarC++17_10-A7.1.1: MD_SOMEIPPROTOCOL_AutosarC++17_10-A7.1.1_Immutable_Variable_Generation
    bool deserialization_ok{
        dataprototype_deserializer::gwm_platform_timer_gs2s::methods::DeserializerTimerReqTimerSet::Deserialize(
            reader, output.TimerSet)};

    // Deserialize the argument 'StartPoint' of type '/S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/StartPoint_Enum'

    // VECTOR NC AutosarC++17_10-A7.1.1: MD_SOMEIPPROTOCOL_AutosarC++17_10-A7.1.1_Immutable_Variable_Generation
    if(deserialization_ok) {
      deserialization_ok =
          dataprototype_deserializer::gwm_platform_timer_gs2s::methods::DeserializerTimerReqStartPoint::Deserialize(
              reader, output.StartPoint);
    }

    // Deserialize the argument 'TimerType' of type '/S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/TimerType_Enum'

    // VECTOR NC AutosarC++17_10-A7.1.1: MD_SOMEIPPROTOCOL_AutosarC++17_10-A7.1.1_Immutable_Variable_Generation
    if(deserialization_ok) {
      deserialization_ok =
          dataprototype_deserializer::gwm_platform_timer_gs2s::methods::DeserializerTimerReqTimerType::Deserialize(
              reader, output.TimerType);
    }

    // Deserialize the argument 'SleepType' of type '/S2S/Platform/Timer/GS2S/GwmTimerSrv_GS2SInterfaces/SleepType_Enum'

    // VECTOR NC AutosarC++17_10-A7.1.1: MD_SOMEIPPROTOCOL_AutosarC++17_10-A7.1.1_Immutable_Variable_Generation
    if(deserialization_ok) {
      deserialization_ok =
          dataprototype_deserializer::gwm_platform_timer_gs2s::methods::DeserializerTimerReqSleepType::Deserialize(
              reader, output.SleepType);
    }
  // Return deserialization result
  return deserialization_ok;
  }
};

}  // namespace methods
}  // namespace gwm_platform_timer_gs2s
}  // namespace method_deserializer
}  // namespace internal
}  // namespace someip_protocol
}  // namespace amsr

#endif  // DENPENDENCYSRVEXE_INCLUDE_AMSR_SOMEIP_PROTOCOL_INTERNAL_METHOD_DESERIALIZATION_GWM_PLATFORM_TIMER_GS2S_METHODS_deserializer_Request_TimerReq_h_

