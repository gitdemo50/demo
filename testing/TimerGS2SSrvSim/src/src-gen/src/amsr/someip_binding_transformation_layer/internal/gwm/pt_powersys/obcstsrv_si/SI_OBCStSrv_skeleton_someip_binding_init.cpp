// VECTOR Same Line AutosarC++17_10-A1.1.1: MD_SOMEIPBINDING_AutosarC++17_10-A1.1.1_external_identifiers
/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/src/amsr/someip_binding_transformation_layer/internal/gwm/pt_powersys/obcstsrv_si/SI_OBCStSrv_skeleton_someip_binding_init.cpp
 *        \brief  Skeleton-side ara::com SOME/IP binding initialization for ServiceInterface 'SI_OBCStSrv'
 *
 *      \details  Full ServiceInterface path: '/ServiceInterfaces/SI_OBCStSrv'
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_someipbinding
 *         Commit ID: 43b33f0283d4d61385b98a3599688499abd86a75
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "amsr/someip_binding_transformation_layer/internal/gwm/pt_powersys/obcstsrv_si/SI_OBCStSrv_skeleton_someip_binding_init.h"
#include "amsr/someip_binding_transformation_layer/internal/gwm/pt_powersys/obcstsrv_si/SI_OBCStSrv_skeleton_someip_binding.h"
#include "ara/core/optional.h"

namespace amsr {
namespace someip_binding_transformation_layer {
namespace internal {

namespace gwm {
namespace pt_powersys {
namespace obcstsrv_si {

void AraComSomeIpBindingInitializeSkeletonSomeIpEventBackendsSI_OBCStSrv(
    AraComSomeIpBindingSpecializationSkeleton::ServerManager& server_manager) {
  { // ServiceInstance: 0x1
    { // Event: OBCActT

      // SOME/IP Skeleton event backend type for event 'OBCActT'.
      using SI_OBCStSrvSkeletonSomeIpEventBackendOBCActT = ::amsr::someip_binding_transformation_layer::internal::SomeIpSkeletonEventBackend<SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCActT>;

      std::unique_ptr<SI_OBCStSrvSkeletonSomeIpEventBackendOBCActT> event_backend{
        std::make_unique<SI_OBCStSrvSkeletonSomeIpEventBackendOBCActT>(1U, server_manager)};
      gwm::pt_powersys::obcstsrv_si::SI_OBCStSrvSkeletonSomeIpEventManagerOBCActT::EmplaceBackend(1U, std::move(event_backend));
    }
    { // Event: OBCChrgLim

      // SOME/IP Skeleton event backend type for event 'OBCChrgLim'.
      using SI_OBCStSrvSkeletonSomeIpEventBackendOBCChrgLim = ::amsr::someip_binding_transformation_layer::internal::SomeIpSkeletonEventBackend<SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCChrgLim>;

      std::unique_ptr<SI_OBCStSrvSkeletonSomeIpEventBackendOBCChrgLim> event_backend{
        std::make_unique<SI_OBCStSrvSkeletonSomeIpEventBackendOBCChrgLim>(1U, server_manager)};
      gwm::pt_powersys::obcstsrv_si::SI_OBCStSrvSkeletonSomeIpEventManagerOBCChrgLim::EmplaceBackend(1U, std::move(event_backend));
    }
    { // Event: OBCDchrOutpI

      // SOME/IP Skeleton event backend type for event 'OBCDchrOutpI'.
      using SI_OBCStSrvSkeletonSomeIpEventBackendOBCDchrOutpI = ::amsr::someip_binding_transformation_layer::internal::SomeIpSkeletonEventBackend<SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCDchrOutpI>;

      std::unique_ptr<SI_OBCStSrvSkeletonSomeIpEventBackendOBCDchrOutpI> event_backend{
        std::make_unique<SI_OBCStSrvSkeletonSomeIpEventBackendOBCDchrOutpI>(1U, server_manager)};
      gwm::pt_powersys::obcstsrv_si::SI_OBCStSrvSkeletonSomeIpEventManagerOBCDchrOutpI::EmplaceBackend(1U, std::move(event_backend));
    }
    { // Event: OBCDchrOutpU

      // SOME/IP Skeleton event backend type for event 'OBCDchrOutpU'.
      using SI_OBCStSrvSkeletonSomeIpEventBackendOBCDchrOutpU = ::amsr::someip_binding_transformation_layer::internal::SomeIpSkeletonEventBackend<SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCDchrOutpU>;

      std::unique_ptr<SI_OBCStSrvSkeletonSomeIpEventBackendOBCDchrOutpU> event_backend{
        std::make_unique<SI_OBCStSrvSkeletonSomeIpEventBackendOBCDchrOutpU>(1U, server_manager)};
      gwm::pt_powersys::obcstsrv_si::SI_OBCStSrvSkeletonSomeIpEventManagerOBCDchrOutpU::EmplaceBackend(1U, std::move(event_backend));
    }
    { // Event: OBCInptAcI

      // SOME/IP Skeleton event backend type for event 'OBCInptAcI'.
      using SI_OBCStSrvSkeletonSomeIpEventBackendOBCInptAcI = ::amsr::someip_binding_transformation_layer::internal::SomeIpSkeletonEventBackend<SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCInptAcI>;

      std::unique_ptr<SI_OBCStSrvSkeletonSomeIpEventBackendOBCInptAcI> event_backend{
        std::make_unique<SI_OBCStSrvSkeletonSomeIpEventBackendOBCInptAcI>(1U, server_manager)};
      gwm::pt_powersys::obcstsrv_si::SI_OBCStSrvSkeletonSomeIpEventManagerOBCInptAcI::EmplaceBackend(1U, std::move(event_backend));
    }
    { // Event: OBCInptAcU

      // SOME/IP Skeleton event backend type for event 'OBCInptAcU'.
      using SI_OBCStSrvSkeletonSomeIpEventBackendOBCInptAcU = ::amsr::someip_binding_transformation_layer::internal::SomeIpSkeletonEventBackend<SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCInptAcU>;

      std::unique_ptr<SI_OBCStSrvSkeletonSomeIpEventBackendOBCInptAcU> event_backend{
        std::make_unique<SI_OBCStSrvSkeletonSomeIpEventBackendOBCInptAcU>(1U, server_manager)};
      gwm::pt_powersys::obcstsrv_si::SI_OBCStSrvSkeletonSomeIpEventManagerOBCInptAcU::EmplaceBackend(1U, std::move(event_backend));
    }
    { // Event: OBCOuptDcI

      // SOME/IP Skeleton event backend type for event 'OBCOuptDcI'.
      using SI_OBCStSrvSkeletonSomeIpEventBackendOBCOuptDcI = ::amsr::someip_binding_transformation_layer::internal::SomeIpSkeletonEventBackend<SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCOuptDcI>;

      std::unique_ptr<SI_OBCStSrvSkeletonSomeIpEventBackendOBCOuptDcI> event_backend{
        std::make_unique<SI_OBCStSrvSkeletonSomeIpEventBackendOBCOuptDcI>(1U, server_manager)};
      gwm::pt_powersys::obcstsrv_si::SI_OBCStSrvSkeletonSomeIpEventManagerOBCOuptDcI::EmplaceBackend(1U, std::move(event_backend));
    }
    { // Event: OBCOuptDcU

      // SOME/IP Skeleton event backend type for event 'OBCOuptDcU'.
      using SI_OBCStSrvSkeletonSomeIpEventBackendOBCOuptDcU = ::amsr::someip_binding_transformation_layer::internal::SomeIpSkeletonEventBackend<SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCOuptDcU>;

      std::unique_ptr<SI_OBCStSrvSkeletonSomeIpEventBackendOBCOuptDcU> event_backend{
        std::make_unique<SI_OBCStSrvSkeletonSomeIpEventBackendOBCOuptDcU>(1U, server_manager)};
      gwm::pt_powersys::obcstsrv_si::SI_OBCStSrvSkeletonSomeIpEventManagerOBCOuptDcU::EmplaceBackend(1U, std::move(event_backend));
    }

    { // Field notifier: OBCChrgGunSt

      // SOME/IP Skeleton event backend type for field 'OBCChrgGunSt'.
      using SI_OBCStSrvSkeletonSomeIpEventBackendOBCChrgGunSt = ::amsr::someip_binding_transformation_layer::internal::SomeIpSkeletonEventBackend<SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCChrgGunSt>;

      std::unique_ptr<SI_OBCStSrvSkeletonSomeIpEventBackendOBCChrgGunSt> event_backend{
        std::make_unique<SI_OBCStSrvSkeletonSomeIpEventBackendOBCChrgGunSt>(1U, server_manager)};
      gwm::pt_powersys::obcstsrv_si::SI_OBCStSrvSkeletonSomeIpFieldNotifierOBCChrgGunSt::EmplaceBackend(1U, std::move(event_backend));
    }
    { // Field notifier: OBCErrSt

      // SOME/IP Skeleton event backend type for field 'OBCErrSt'.
      using SI_OBCStSrvSkeletonSomeIpEventBackendOBCErrSt = ::amsr::someip_binding_transformation_layer::internal::SomeIpSkeletonEventBackend<SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCErrSt>;

      std::unique_ptr<SI_OBCStSrvSkeletonSomeIpEventBackendOBCErrSt> event_backend{
        std::make_unique<SI_OBCStSrvSkeletonSomeIpEventBackendOBCErrSt>(1U, server_manager)};
      gwm::pt_powersys::obcstsrv_si::SI_OBCStSrvSkeletonSomeIpFieldNotifierOBCErrSt::EmplaceBackend(1U, std::move(event_backend));
    }
    { // Field notifier: OBCHVILSt

      // SOME/IP Skeleton event backend type for field 'OBCHVILSt'.
      using SI_OBCStSrvSkeletonSomeIpEventBackendOBCHVILSt = ::amsr::someip_binding_transformation_layer::internal::SomeIpSkeletonEventBackend<SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCHVILSt>;

      std::unique_ptr<SI_OBCStSrvSkeletonSomeIpEventBackendOBCHVILSt> event_backend{
        std::make_unique<SI_OBCStSrvSkeletonSomeIpEventBackendOBCHVILSt>(1U, server_manager)};
      gwm::pt_powersys::obcstsrv_si::SI_OBCStSrvSkeletonSomeIpFieldNotifierOBCHVILSt::EmplaceBackend(1U, std::move(event_backend));
    }
    { // Field notifier: OBCSt

      // SOME/IP Skeleton event backend type for field 'OBCSt'.
      using SI_OBCStSrvSkeletonSomeIpEventBackendOBCSt = ::amsr::someip_binding_transformation_layer::internal::SomeIpSkeletonEventBackend<SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCSt>;

      std::unique_ptr<SI_OBCStSrvSkeletonSomeIpEventBackendOBCSt> event_backend{
        std::make_unique<SI_OBCStSrvSkeletonSomeIpEventBackendOBCSt>(1U, server_manager)};
      gwm::pt_powersys::obcstsrv_si::SI_OBCStSrvSkeletonSomeIpFieldNotifierOBCSt::EmplaceBackend(1U, std::move(event_backend));
    }
    { // Field notifier: OBCV2XSt

      // SOME/IP Skeleton event backend type for field 'OBCV2XSt'.
      using SI_OBCStSrvSkeletonSomeIpEventBackendOBCV2XSt = ::amsr::someip_binding_transformation_layer::internal::SomeIpSkeletonEventBackend<SI_OBCStSrvSkeletonSomeIpEventConfigurationOBCV2XSt>;

      std::unique_ptr<SI_OBCStSrvSkeletonSomeIpEventBackendOBCV2XSt> event_backend{
        std::make_unique<SI_OBCStSrvSkeletonSomeIpEventBackendOBCV2XSt>(1U, server_manager)};
      gwm::pt_powersys::obcstsrv_si::SI_OBCStSrvSkeletonSomeIpFieldNotifierOBCV2XSt::EmplaceBackend(1U, std::move(event_backend));
    }
  }
}

void AraComSomeIpBindingDeInitializeSkeletonSomeIpEventBackendsSI_OBCStSrv() {
  // Event: OBCActT
  gwm::pt_powersys::obcstsrv_si::SI_OBCStSrvSkeletonSomeIpEventManagerOBCActT::ClearBackendList();
  // Event: OBCChrgLim
  gwm::pt_powersys::obcstsrv_si::SI_OBCStSrvSkeletonSomeIpEventManagerOBCChrgLim::ClearBackendList();
  // Event: OBCDchrOutpI
  gwm::pt_powersys::obcstsrv_si::SI_OBCStSrvSkeletonSomeIpEventManagerOBCDchrOutpI::ClearBackendList();
  // Event: OBCDchrOutpU
  gwm::pt_powersys::obcstsrv_si::SI_OBCStSrvSkeletonSomeIpEventManagerOBCDchrOutpU::ClearBackendList();
  // Event: OBCInptAcI
  gwm::pt_powersys::obcstsrv_si::SI_OBCStSrvSkeletonSomeIpEventManagerOBCInptAcI::ClearBackendList();
  // Event: OBCInptAcU
  gwm::pt_powersys::obcstsrv_si::SI_OBCStSrvSkeletonSomeIpEventManagerOBCInptAcU::ClearBackendList();
  // Event: OBCOuptDcI
  gwm::pt_powersys::obcstsrv_si::SI_OBCStSrvSkeletonSomeIpEventManagerOBCOuptDcI::ClearBackendList();
  // Event: OBCOuptDcU
  gwm::pt_powersys::obcstsrv_si::SI_OBCStSrvSkeletonSomeIpEventManagerOBCOuptDcU::ClearBackendList();

  // Field notifier: OBCChrgGunSt
  gwm::pt_powersys::obcstsrv_si::SI_OBCStSrvSkeletonSomeIpFieldNotifierOBCChrgGunSt::ClearBackendList();

  // Field notifier: OBCErrSt
  gwm::pt_powersys::obcstsrv_si::SI_OBCStSrvSkeletonSomeIpFieldNotifierOBCErrSt::ClearBackendList();

  // Field notifier: OBCHVILSt
  gwm::pt_powersys::obcstsrv_si::SI_OBCStSrvSkeletonSomeIpFieldNotifierOBCHVILSt::ClearBackendList();

  // Field notifier: OBCSt
  gwm::pt_powersys::obcstsrv_si::SI_OBCStSrvSkeletonSomeIpFieldNotifierOBCSt::ClearBackendList();

  // Field notifier: OBCV2XSt
  gwm::pt_powersys::obcstsrv_si::SI_OBCStSrvSkeletonSomeIpFieldNotifierOBCV2XSt::ClearBackendList();

}


}  // namespace obcstsrv_si
}  // namespace pt_powersys
}  // namespace gwm

}  // namespace internal
}  // namespace someip_binding_transformation_layer
}  // namespace amsr

