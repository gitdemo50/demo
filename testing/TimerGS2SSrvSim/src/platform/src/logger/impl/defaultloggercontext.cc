
#include "logger/logger.hpp"

namespace gwm {
namespace platform {

template<>
impl::GWMLogger& createLogger<impl::GWMLogger>(std::string name) {
    return LoggerContext::getInstance().createLogger(name);
}

}
}
