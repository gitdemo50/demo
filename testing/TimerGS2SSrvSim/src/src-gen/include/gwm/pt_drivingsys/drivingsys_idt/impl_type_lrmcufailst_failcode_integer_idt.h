/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *  Copyright (c) 2022 by Vector Informatik GmbH. All rights reserved.
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -----------------------------------------------------------------------------------------------------------------*/
/**        \file  DenpendencySrvExe/include/gwm/pt_drivingsys/drivingsys_idt/impl_type_lrmcufailst_failcode_integer_idt.h
 *        \brief
 *
 *      \details
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  GENERATOR INFORMATION
 *  -------------------------------------------------------------------------------------------------------------------
 *    Generator Name: amsr_modelleddatatypes_api
 *         Commit ID: d9332226824a4a6c4616e2d948d41c862d192343
 *********************************************************************************************************************/

#ifndef DENPENDENCYSRVEXE_INCLUDE_GWM_PT_DRIVINGSYS_DRIVINGSYS_IDT_IMPL_TYPE_LRMCUFAILST_FAILCODE_INTEGER_IDT_H_
#define DENPENDENCYSRVEXE_INCLUDE_GWM_PT_DRIVINGSYS_DRIVINGSYS_IDT_IMPL_TYPE_LRMCUFAILST_FAILCODE_INTEGER_IDT_H_

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include <array>
#include <cstdint>
#include <map>
#include <string>
#include <vector>

namespace gwm {
namespace pt_drivingsys {
namespace drivingsys_idt {

/*!
 * \brief Type LRMcuFailSt_FailCode_Integer_Idt.
 * \remark generated
 * \trace SPEC-5951372
 *
 * IMPLEMENTATION-DATA-TYPE /DataTypes/ImplementationDataTypes/LRMcuFailSt_FailCode_Integer_Idt
 */
using LRMcuFailSt_FailCode_Integer_Idt = std::uint16_t;

}  // namespace drivingsys_idt
}  // namespace pt_drivingsys
}  // namespace gwm

#endif  // DENPENDENCYSRVEXE_INCLUDE_GWM_PT_DRIVINGSYS_DRIVINGSYS_IDT_IMPL_TYPE_LRMCUFAILST_FAILCODE_INTEGER_IDT_H_
