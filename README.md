# GWM Adaptive Basic Services
111
222
## TimerSrvExe

### Build Step

#### For executable

```sh 
mkdir build && cd build
cmake -DARA_INSTALL_PATH=<MICROSAR_INSTALL_PATH> -DARA_LIB_PATH=<MICROSAR_LIB_PATH> -DUSE_ARA_LOGGER=ON ..
make TimerSrvExe
```
**ARA_INSTALL_PATH**: Path to the MICROSAR install location. Expects <ARA_INSTALL_PATH>/include to contain ARA include files
**ARA_LIB_PATH**: Path to the folder where all the MICROSAR libraries are installed

For example: 
cmake -DARA_INSTALL_PATH=/home/gwm/AP_R2011 -DARA_LIB_PATH=/home/gwm/AP_R2011/lib -DUSE_ARA_LOGGER=ON ..

#### For Running Unit test and CodeCoverage report

```sh 
mkdir build && cd build
cmake -DARA_INSTALL_PATH=<MICROSAR_INSTALL_PATH> -DARA_LIB_PATH=<MICROSAR_LIB_PATH> -DUSE_ARA_LOGGER=OFF ..
make TimerSrvExe_coverage
```

GCOV coverage output will be at `build/TimerSrvExe_coverage/index.html`
![codeCoverageReport](./codeCoverageReport.jpg "codeCoverageReport")

#### For Testing Application

```sh 
cd testing
cd TimerGS2SSrvSim
mkdir build && cd build
cmake -DARA_INSTALL_PATH=<MICROSAR_INSTALL_PATH> -DARA_LIB_PATH=<MICROSAR_LIB_PATH> ..
make
```

```sh 
cd testing
cd TimerSrvTest
mkdir build && cd build
cmake -DARA_INSTALL_PATH=<MICROSAR_INSTALL_PATH> -DARA_LIB_PATH=<MICROSAR_LIB_PATH> ..
make
```

![TestingApp](./testingapp.gif "TestingApp")
